#!/bin/bash
if [ $# = 1 ]; then
	NAME="$1"
else
    NAME="cts"
fi

MY_HOSTNAME="$NAME:latest"
export MY_HOSTNAME

#let's run it
docker run -it -d --name cts -e JAVA_OPTS='-Xmx20g -XX:MaxPermSize=4096m' -v //$(pwd)/access-log://var/log/nginx -p 80:8080 $MY_HOSTNAME
