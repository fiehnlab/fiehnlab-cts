#!/bin/bash

echo "starting nginx..."
service nginx start
echo `netstat -anpt | grep nginx | grep LISTEN`

####
# JETTY PART
###

cd /opt/jetty

#java -Xmx4048m -jar start.jar "jetty.home=/opt/jetty"
echo "Starting jetty..."

COMMAND=`/opt/jetty/bin/jetty.sh check | grep RUN_CMD | awk '{n=split($0,final,"  =  "); print final[2]}'`
echo "Start Jetty: $COMMAND"

eval "$COMMAND"
