#!/bin/bash

cp ../cts/target/cts*.war ./root.war

set -o pipefail

VERSION="2.6.15"
if [ -z "$1" ]; then
	NAME="cts"
else
	NAME=$1
fi

IMAGE="eros.fiehnlab.ucdavis.edu/$NAME"

docker build -t ${IMAGE} --rm . | tee build.log || exit 1
ID=$(tail -1 build.log | awk '{print $3;}')

echo "built image id:$ID (${IMAGE})"

docker tag $ID ${IMAGE}:${VERSION}
docker tag $ID ${NAME}:${VERSION}
docker tag $ID ${NAME}:latest

rm root.war

if [ "$2" == "push" ]; then
	docker push $IMAGE
else
	echo "pushing disabled!"
fi
