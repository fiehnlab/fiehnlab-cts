package edu.ucdavis.fiehnlab.cts

import static org.junit.Assert.*

import org.junit.*

import grails.test.mixin.*
import grails.test.mixin.support.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Compound, Synonym, ExternalId])
class SynonymControllerUnitTests {

    void testGoodData() {
		Synonym s = new Synonym(name: "banana", type: "Synonyms")
		
		assertEquals "banana", s.name
		assertEquals "Synonyms", s.type
    }		

	void testBadData() {
		Synonym s = new Synonym(type: "i dont care", name: "bad synonym")
		assertFalse "there should be errors", s.validate()
		assertEquals "type", s.errors.getFieldError("type").field
		assertEquals "not.inList", s.errors.getFieldError("type").code

		s = new Synonym(type: "Synonyms")
		assertFalse "there should be errors", s.validate()
		assertEquals "name", s.errors.getFieldError("name").field
		assertEquals "nullable", s.errors.getFieldError("name").code
		
	}
}
