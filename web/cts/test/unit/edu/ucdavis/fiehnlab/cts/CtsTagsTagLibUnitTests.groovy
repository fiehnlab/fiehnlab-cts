package edu.ucdavis.fiehnlab.cts

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*;

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(CtsTagsTagLib)
@TestMixin(GrailsUnitTestMixin)
class CtsTagsTagLibUnitTests {
	def c
	
	@Before
	void setup() {
		def syns = new HashSet<Synonym>()
		syns.add(new Synonym(inchiKey:'abcdefghijklmnopqrstuvwxy-z',
										type:'IUPAC Name (Preferred)',
										name:'polvo magico'))
		syns.add(new Synonym(inchiKey:'abcdefghijklmnopqrstuvwxy-z',
			type:'Synonym',
			name:'ethanol'))

		def ids = new HashSet<ExternalId>()
		ids.add(new ExternalId(inchiKey:'abcdefghijklmnopqrstuvwxy-z',
								name:'first id',
								value:'1'))
		ids.add(new ExternalId(inchiKey:'abcdefghijklmnopqrstuvwxy-z',
								name:'second id',
								value:'2d42'))
		
		c = new Compound(inchiKey:'abcdefghijklmnopqrstuvwxy-z', 
							inchiCode:'no/tengo-idea-como[hacer]-esto', 
							molWeight: 10.1,
							synonyms: syns,
							extIds: ids)
	}
	
	@Test
	@Ignore
    void testCompoundRender() {
		String exp = "<div class='compound'>\n\
	<p><span class='compound.label'>Compound:</span> <span class='compound.value'>polvo magico</span></p>\n\
	<p class='compound.details'>\n\
		<span class='compound.label'>InChI Key:</span> <span class='compound.value'>abcdefghijklmnopqrstuvwxy-z<br/></span>\n\
		<span class='compound.label'>InChI Code:</span> <span class='compound.value'>no/tengo-idea-como[hacer]-esto<br/></span>\n\
		<span class='compound.label'>Molecular weight:</span> <span class='compound.value'>10.1</span>\n\
	</p>\n\
</div>\n"

		def res = applyTemplate('<cts:compound compound="${comp}" />', [comp:c])
		res = res.replaceAll("(?m)^[ \t]*\r?\n", "")

        assertEquals(exp, res)
    }
}
