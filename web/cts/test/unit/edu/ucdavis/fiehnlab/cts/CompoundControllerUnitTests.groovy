package edu.ucdavis.fiehnlab.cts

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class CompoundControllerUnitTests {

	@Before
    void setUp() {
        // Setup logic here
    }

	@After
    void tearDown() {
        // Tear down logic here
    }
	
	@Ignore("unimplemented") //TODO add more tests
	@Test
    void testIndex() {
        fail "Implement me"
    }
}
