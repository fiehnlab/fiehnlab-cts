package edu.ucdavis.fiehnlab.cts

import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(IdName)
class IdNameUnitTests {
	IdName name1, name2
	
	@Before
	public void setUp() {
		name1 = new IdName(idName: 'el nombre')
		name2 = new IdName()
	}
	
    void testGetIdName() {
		assertEquals('el nombre', name1.idName)
    }
	
	void testSetIdName() {
		assert name2.idName == null
		
		name2.idName = 'otro nombre'
		assertEquals('otro nombre', name2.idName)
	}
}
