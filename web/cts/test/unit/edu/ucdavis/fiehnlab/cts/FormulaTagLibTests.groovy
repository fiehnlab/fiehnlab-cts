package edu.ucdavis.fiehnlab.cts
import cts.FormulaTagLib
import grails.test.mixin.TestFor
/**
 * Created by diego on 1/17/14.
 */
@TestFor(FormulaTagLib)
public class FormulaTagLibTests {

	void testCreateImage() {
		def id = "LFQSCWFLJHTTHZ-JVVVGQRLSA-N"
		def code = "InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3/i2-1"

		def image = applyTemplate('<g:structureMedium3D id="${id}" inchi="${code}"/>', [id: id, code: code])

		assertFalse image.isEmpty()

	}
}