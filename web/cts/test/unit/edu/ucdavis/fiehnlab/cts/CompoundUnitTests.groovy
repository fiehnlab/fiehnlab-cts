package edu.ucdavis.fiehnlab.cts

import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Compound, Synonym, ExternalId])
class CompoundUnitTests {
	private static Logger logger = Logger.getLogger(CompoundUnitTests.class)
	Compound c

	@Before
	void setUp() {
		c = new Compound(inchiKey: 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
				inchiCode: 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
				molWeight: 380.2286,
				exactMass: 378.2286,
				formula: "C16H15Cl2N5O2",
				sourceFile: "test",
				synonyms:[],
				extIds:[]
		)
	}

	@After
	void tearDown() {
		c = null
	}

	@Test
	void testCompound() {
		assertEquals "RYXHPKXJBBAPCB-UHFFFAOYSA-N", c.inchiKey
		assertEquals "InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)", c.inchiCode
		assertEquals 380.2286, c.molWeight, 0.00001
		assertEquals 378.2286, c.exactMass, 0.00001
		assertEquals "C16H15Cl2N5O2", c.formula
		assertEquals "test", c.sourceFile
		assertEquals 0, c.dataSourcesCount
		assertEquals 0, c.referencesCount
		assertEquals 0, c.pubmedHits
	}

	@Test
	void testCompoundSave() {
//		Synonym s = null;
//		ExternalId eId = new ExternalId(name:"PubChem", url:"https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi", value:"21104443")

		Compound c = new Compound(inchiKey: "ZVEYXXGLPNSLIT-UHFFFAOYSA-N",
				molWeight: 362.42168,
				exactMass: 361.42168795,
				formula: "C22H22N2O3",
				inchiCode: "InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)",
				sourceFile: "test-1",
				dataSourcesCount: 1,
				referencesCount: 2,
				pubmedHits: 3
		)
//				.addToExtIds(eId)

//		s = new Synonym(type:"IUPAC Name (CAS-like Style)", name: "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-3-quinolizinecarboxylic acid")
//		c.addToSynonyms(s)

//		s = new Synonym(type:"IUPAC Name (Traditional)", name: "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid")
//		c.addToSynonyms(s)

//		s = new Synonym(type:"IUPAC Name (Systematic)", name: "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxidanylidene-quinolizine-3-carboxylic acid")
//		c.addToSynonyms(s)

//		s = new Synonym(type:"IUPAC Name (Preferred)", name: "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxoquinolizine-3-carboxylic acid")
//		c.addToSynonyms(s)

//		s = new Synonym(type:"IUPAC Name (Allowed)", name:"1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-quinolizine-3-carboxylic acid")
//		c.addToSynonyms(s)
//		c.addToSynonyms(s)

		c.save(flush: true)

		def cc = Compound.get("ZVEYXXGLPNSLIT-UHFFFAOYSA-N")

		assertEquals c.inchiKey, cc.inchiKey
		assertEquals c.inchiCode, cc.inchiCode
		assertEquals c.molWeight, cc.molWeight, 0.00001
		assertEquals c.exactMass, cc.exactMass, 0.00001
		assertEquals c.formula, cc.formula
		assertEquals c.sourceFile, cc.sourceFile
		assertEquals c.dataSourcesCount, cc.dataSourcesCount
		assertEquals c.referencesCount, cc.referencesCount
		assertEquals c.pubmedHits, cc.pubmedHits

//		assertEquals 5, c.synonyms.collect().size()
//		assertEquals "IUPAC Name (Preferred)", c.synonyms.collect().get(3).getType()

//		assertEquals 1, c.extIds.collect().size()
//		assertEquals "21104443", c.extIds.collect().get(0).getValue()
	}

	@Test
	void testBadInchiKey() {
		Compound c = new Compound(inchiKey: 'too-short')
		assertFalse "there should be errors", c.validate()
		assertEquals "inchiKey", c.errors.getFieldError("inchiKey").field
		assertEquals "size.toosmall", c.errors.getFieldError("inchiKey").code

		c = new Compound(inchiKey: 'this-is-going-to-be-a-long-string-for-inchiKey')
		assertFalse "there should be errors", c.validate()
		assertEquals "size.toobig", c.errors.getFieldError("inchiKey").code
	}

	@Test
	void testToString() {
		assertEquals "RYXHPKXJBBAPCB-UHFFFAOYSA-N", c.toString()
	}

	@Test
	void testHashCode() {
		Compound c1 = new Compound(inchiKey: "RYXHPKXJBBAPCB-UHFFFAOYSA-N",
				molWeight: 380.2286,
				formula: "C16H15Cl2N5O2",
				exactMass: 378.2286,
				inchiCode: "InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)")
		assert c.hashCode() == c1.hashCode()
	}

	@Test
	void testBadHashCode() {
		Compound c2 = new Compound(inchiKey: "adsad", molWeight: 12.1, inchiCode: '')
		assert c.hashCode() != c2.hashCode()
	}

	@Test
	void testEquals() {
		Compound c1 = new Compound(inchiKey: 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
				inchiCode: 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
				molWeight: 380.2286,
				exactMass: 378.2286,
				formula: "C16H15Cl2N5O2"
		)

		assert c.equals(c1)
		assert c1.equals(c)
	}

	@Test
	void testNotEquals() {
		Compound c2 = new Compound(inchiKey: "adsad", molWeight: 362.42168, inchiCode: 'InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)')
		assert !c.equals(c2)

		c2.inchiKey = "adsad"
		c2.molWeight = 362.42168
		c2.inchiCode = 'InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)'
		assert !c.equals(c2)
		assert !c2.equals(c)

		c2 = new Compound(inchiKey: "adsad", molWeight: 362.42168, inchiCode: 'InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)')
		assert !c.equals(c2)
		assert !c2.equals(c)
	}
}
