package edu.ucdavis.fiehnlab.cts

import grails.test.mixin.*

import org.junit.Ignore

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ConversionController)
@Mock([ExternalId, ConversionService])
class ConversionControllerUnitTests {
	
	@Ignore("rewrite as Unit test")
	void testGetIdNames() {

		def expected = ["PubChem CID", "ChEMBL", "ABI Chem"]		
		def query = ExternalId.where{}.projections {distinct 'name'}
		def model = controller.getIdNames()
		
		model.each { println it }
		assert model != null
		assert model.size > 0
		assert model.containsAll(expected)
	}

	@Ignore("rewrite as Unit test")
	void testIndex() {
		controller.index()

		assert "/conversion/convertSimpleForm" == view
		
	}
	
	@Ignore("rewrite as Unit test")
	void testConvert() {
		params.from = "Pubchem CID"
		params.to = "InChIKey"

		controller.convert()

		assert "i will cdk from Pubchem CID to InChIKey" == response.text
	}
}
