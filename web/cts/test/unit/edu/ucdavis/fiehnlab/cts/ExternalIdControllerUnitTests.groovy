package edu.ucdavis.fiehnlab.cts

import static org.junit.Assert.*

import org.junit.*

import grails.test.mixin.*
import grails.test.mixin.support.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Compound, Synonym, ExternalId])
class ExternalIdControllerUnitTests {

	@Ignore("not implemented")
    void testExternalId() {
		// TODO create tests
	}
}
