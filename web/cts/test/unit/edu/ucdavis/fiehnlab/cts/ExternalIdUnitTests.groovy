package edu.ucdavis.fiehnlab.cts

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Compound, Synonym, ExternalId])
class ExternalIdUnitTests {
	ExternalId exid
	
	@Before
    void setUp() {
		exid = new ExternalId(name:"pubchem", value:"1234-dd", url:'http://localhost.com')
    }

	@After
    void tearDown() {
        // Tear down logic here
    }

	@Test
    void testExternalId() {
		
		assertEquals "pubchem", exid.name
		assertEquals "1234-dd", exid.value
		assertEquals "http://localhost.com", exid.url
    }
	
	@Test
	void testBadInput() {
		ExternalId id = new ExternalId()
		assertFalse "errors... there must be", id.validate()
		assertEquals "name", id.errors.getFieldError("name").field
		assertEquals "nullable", id.errors.getFieldError("name").code
		assertEquals "value", id.errors.getFieldError("value").field
		assertEquals "nullable", id.errors.getFieldError("value").code
	}

	@Test
	void testBadUrl() {
		ExternalId id = new ExternalId(name:'name', value:'value', url:'not a url')
		
		assertFalse 'should have errors', id.validate()
		assertEquals 'url', id.errors.getFieldError('url').field
		assertEquals 'url.invalid', id.errors.getFieldError('url').code
	}
		
	@Test
	void testToString() {
		assertEquals "pubchem:1234-dd (http://localhost.com)", exid.toString()
	}
	
	@Test
	void testHashCode() {
		ExternalId id1 = new ExternalId(name:'pubchem', value:'1234-dd', url:'http://localhost.com')
		ExternalId id2 = new ExternalId(name:'yourname', value:'1234-dd', url:'http://localhost.com')
		
		assert exid.hashCode() == id1.hashCode()
		assert exid.hashCode() != id2.hashCode()
	}
	
	@Test
	void testEquals() {
		ExternalId id1 = new ExternalId(name:'pubchem', value:'1234-dd', url:'http://localhost.com')
		assertEquals exid, id1
	}
	
	@Test
	void testNotEquals() {
		assert !exid.equals(null)
		assert !exid.equals(new Synonym())

		ExternalId id1 = new ExternalId(name:'yourname', value:'1234-dd', url:'http://localhost.com')
		assert !exid.equals(id1)

		ExternalId id2 = new ExternalId(name:'pubchem', value:'dd-12gf', url:'http://localhost.com')
		assert !exid.equals(id2)

		ExternalId id3 = new ExternalId(name:'pubchem', value:'1234-dd', url:'http://localhost')
		assert !exid.equals(id3)
	}
}
