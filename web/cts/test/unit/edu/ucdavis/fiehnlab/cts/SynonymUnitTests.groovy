package edu.ucdavis.fiehnlab.cts

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class SynonymUnitTests {
	Synonym s;

	@Before
    void setUp() {
        s = new Synonym(type:'PubChem', name:'synon1')
    }

	@After
    void tearDown() {
        // Tear down logic here
    }

	@Test
    void testToString() {
		assert "PubChem: synon1" == s.toString()
    }
	
	@Test
	void testHashCode() {
		Synonym s1 = new Synonym(type:'PubChem', name:'synon1')
		Synonym s2 = new Synonym(type:'PubChem', name:'synon2')

		assert s.hashCode() == s1.hashCode()
		assert s.hashCode() != s2.hashCode()
	}
	
	@Test
	void testEquals() {
		Synonym s1 = new Synonym(type:'PubChem', name:'synon1')
		
		assert s.equals(s1)
	}
	
	@Test
	void testNotEquals() {
		Synonym s1 = new Synonym(type:'chem', name:'synon1')
		Synonym s2 = new Synonym(type:'PubChem', name:'synon2')
		
		assert !s.equals(s1)
		assert !s.equals(s2)
	}
}
