import edu.ucdavis.fiehnlab.cts.ServiceController
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/3/13
 * Time: 10:23 AM
 * To change this template use File | Settings | File Templates.
 */
@TestFor(UrlMappings)
@Mock([ServiceController])
class UrlMappingsUnitTests {
	Logger logger = Logger.getLogger(this.getClass())

	@Before
	void setUp() {
		logger.setLevel(Level.DEBUG)
	}

	@After
	void tearDown() {
		logger.setLevel(Level.INFO)
	}

	// testing compound action mapping with inchikey
	@Test
	void testCompoundWithInchikey() {
		webRequest.currentRequest.setMethod("GET")

		assertUrlMapping("/service/compound/RYXHPKXJBBAPCB-UHFFFAOYSA-N", controller: "service", action: "compound") {
			inchiKey = 'RYXHPKXJBBAPCB-UHFFFAOYSA-N'
		}
	}

	// testing compound action mapping without inchikey
	@Test
	void testCompoundWithoutInchikey() {
		webRequest.currentRequest.setMethod("GET")

		assertUrlMapping("/service/compound", controller: "service", action: "compound") {
			inchiKey = null
		}
	}

	// testing convertion mapping
	@Test
	@Ignore
	void testConversion() {

		assertUrlMapping('/service/convert/AbiChem/InChIKey/150003', controller: 'service', action: 'convert') {
			from = 'AbiChem'
			to = 'InChIKey'
			value = '150003'
		}
	}

	// testing from conversion values list mapping
	@Test
	@Ignore
	void testConversionFromValues() {

		assertUrlMapping("/service/convert/fromValues", controller: "service", action: "fromValues")
	}

	// testing to conversion values list mapping
	@Test
	@Ignore
	void testConversionToValues() {

		assertUrlMapping("/service/convert/toValues", controller: "service", action: "toValues")
	}

	@Test//(expected = junit.framework.ComparisonFailure)
	void testMissingMapping() {

		shouldFail {
			assertUrlMapping('/service/delete', controller: 'service', accion: 'delete')
		}
	}

	@Test
	void testSourceNameURL() {

		assertForwardUrlMapping('/service/sourceName/708', controller: 'service', action: 'sourceName')
	}

	@Test
	void testSourceNameBadURL() {

		shouldFail {
			assertUrlMapping('/service/sourceName/pcid', controller: 'service', action: 'sourceName')
		}
	}

	@Test
	void testPubchemDepositorURL() {

		assertForwardUrlMapping('/service/pubchemDepositor/substance/708', controller: 'service', action: 'pubchemDepositor')
	}
}
