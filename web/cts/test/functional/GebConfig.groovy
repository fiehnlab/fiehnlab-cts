import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver

baseUrl = 'http://localhost:8080/cts/'

// default driver...
driver = {new FirefoxDriver()}

environments {
	// specify environment via -Dgeb.env=ie

	//we don't care for IE
//	'ie' {
//		def ieDriver = new File('src/test/resources/IEDriverServer.exe')
//		System.setProperty('webdriver.ie.driver', ieDriver.absolutePath)
//		driver = { new InternetExplorerDriver() }
//	}
	// or safari
//	'safari' {
//		driver = { new SafariDriver() }
//	}

	'chrome' {
		def chromeDriver = new File('src/test/resources/chromedriver.exe')
		System.setProperty('webdriver.chrome.driver', chromeDriver.absolutePath)
		driver = {new ChromeDriver()}
	}

	'ff' {
		driver = {new FirefoxDriver()}
		//driver.manage().window().setSize(new Dimension(1028, 768))
	}

}

reportsDir = "target/test-reports"

waiting {
	timeout = 6
	retryInterval = 0.5
	slow {timeout = 12}
	reallyslow {timeout = 24}
}
