package edu.ucdavis.fiehnlab.cts.restapi

import geb.spock.GebReportingSpec
import org.apache.log4j.Logger
import org.junit.Ignore

@Ignore
class RestAPIFunctionalSpec extends GebReportingSpec {
	private static Logger logger = Logger.getLogger(this.class.name)

	void testServiceCount() {
        get('/service/count/QNAYBMKLOCPYGJ-UHFFFAOYSA-N') {
            accept("application/json")
        }

        assertStatus 200
        assertContentContains 'datasource_count'
	}

	void testServiceCountBadInchi() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/count/<inchikey>"

        get('/service/count/badinchi') {
            accept("application/json")
        }

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceCountNoInchi() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/count/<inchikey>"

        get('/service/count') {
            accept("application/json")
        }

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceBioCount() {
        get('/service/countBiological/QNAYBMKLOCPYGJ-UHFFFAOYSA-N') {
            accept("application/json")
        }

        assertStatus 200
        assertContentContains '\"Human Metabolome Database\": 1,'
        assertContentContains '\"KEGG\": 1,'
        assertContentContains '\"total\": 2'
	}

	void testServiceBioCountBadInchi() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/countBiological/<inchikey>"

        get('/service/countBiological/badinchi') {
            accept("application/json")
        }

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceBioCountNoInchi() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/countBiological/<inchikey>"

        get('/service/countBiological') {
            accept("application/json")
        }

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceSynonyms() {
        get("/service/synonyms/QNAYBMKLOCPYGJ-UHFFFAOYSA-N")

        assertStatus 200

        assertContentContains 'Alanine'
	}

	void testServiceSynonymsBadInchikey() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>"

        get("/service/synonyms/blah-inchi key")

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceSynonymsNoInchikey() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>"

        get("/service/synonyms")

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceCompound() {
        get("/service/compound/QNAYBMKLOCPYGJ-UHFFFAOYSA-N")

        assertStatus 200

        assertEquals "QNAYBMKLOCPYGJ-UHFFFAOYSA-N", JSON.inchikey
        assertEquals "inchiCode", JSON.inchicode
        assertEquals 1, JSON.molweight
        assertEquals 1.00001, JSON.exactmass
        assertEquals "abc", JSON.formula

        assert JSON.synonyms.contains([name: "Alanine", score: 0, type: "Synonym"])
        assert JSON.externalIds.contains([name: "KEGG", value: "BC1290", url: "http://no.url.com"])
	}

	void testServiceCompoundBadInchikey() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>"

        get("/service/compound/not an inchikey")

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceCompoundNoInchikey() {
        def exp = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>"

        get("/service/compound")

        assertStatus 400
        assertEquals exp, JSON[0]
	}

	void testServiceConvert() {
        get('/service/convert/kegg/inchikey/C00041') {
            accept("text/html")
        }

        logger.debug "JSON: " + JSON
        logger.debug "JSON: " + content
        logger.debug "JSON: " + contentType
        logger.debug "JSON: " + contentTypeForJSON

        assertStatus 200
        assertContentContains("QNAYBMKLOCPYGJ-REOHCLBHSA-N")
        assertContentContains("\"fromIdentifier\": \"kegg\",")
        assertContentContains("\"searchTerm\": \"C00041\",")
        assertContentContains("\"toIdentifier\": \"inchikey\",")
        assertContentContains("\"result\": [\"QNAYBMKLOCPYGJ-REOHCLBHSA-N\"]")
	}

	void testServiceNoParams() {
        get("/service/convert")

        logger.debug "JSON: " + JSON

        assertStatus 400
        assertContentContains("\"error\": \"Missing parameters.")
        assertContentContains("\"toIdentifier\": null,")
        assertContentContains("\"fromIdentifier\": null,")
        assertContentContains("\"searchTerm\": null")
	}

	void testServiceBadFromParam() {
        get('/service/convert/badfrom/cas/myvalue')

        logger.debug "JSON: " + JSON

        assertStatus 400
        assertContentContains("\"error\": \"Parameter 'from' is invalid.")
        assertContentContains("\"toIdentifier\": \"cas\"")
        assertContentContains("\"fromIdentifier\": \"badfrom\"")
        assertContentContains("\"searchTerm\":\"myvalue\"")
	}

	void testServiceBadToParam() {
        get("/service/convert/KEGG/badto/C00041")

        logger.debug "JSON: " + JSON

        assertStatus 400
        assertContentContains("\"error\": \"Parameter 'to' is invalid.")
        assertContentContains("\"toIdentifier\": \"badto\",")
        assertContentContains("\"fromIdentifier\": \"KEGG\",")
        assertContentContains("\"searchTerm\": \"C00041\"")
	}

	void testMolToInchiService() {
        def data = post('/service/moltoinchi') {
            accept("application/json")
            contentType("application/x-www-form-urlencoded")
            body {
                """mol=977
  -OEChem-07071413282D

  2  1  0     0  0  0  0  0  0999 V2000
    2.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0  0  0  0
M  END"""
            }
        }

        assertContent '{\n' +
              '  "inchikey": "MYMOFIZGZYHOMD-UHFFFAOYSA-N",\n' +
              '  "inchicode": "InChI=1S/O2/c1-2"\n' +
              '}'
        assertStatus 200
        assertContentContains 'InChI Code'
        assertContentContains 'InChIKey'
	}
}
