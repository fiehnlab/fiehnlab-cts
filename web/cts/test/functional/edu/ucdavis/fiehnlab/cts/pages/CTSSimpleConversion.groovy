package edu.ucdavis.fiehnlab.cts.pages

import geb.Page

/**
 * Created by Diego on 10/5/2015.
 */
class CTSSimpleConversion extends Page {
	static url = 'conversion'

	static at = {
		$("h2").text() == "Simple Conversion"
	}

}
