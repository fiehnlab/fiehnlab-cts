package edu.ucdavis.fiehnlab.cts

import grails.plugin.spock.IntegrationSpec
import org.apache.log4j.Logger
import spock.lang.Shared

/**
 * Created by Diego on 12/1/2015.
 */
class InfoControllerSpec extends IntegrationSpec {
	private static Logger logger = Logger.getLogger(InfoControllerSpec.class)
	def infoService

	@Shared
	def infoController = new InfoController()

	void setup() {
		infoController.infoService = infoService
	}

	def "GetExternalIdScore"() {
		setup:
		def id = "KEGG"
		def count = 4

		when:
		infoController.params.name = id

		def res = infoController.getExternalIdScore()
		logger.debug "RES: ${infoController.response.json}"

		then:
		infoController.response.status == 200
		def content = infoController.response.json
		content.external_id == id
		content.count == count
	}

	def "GetBadExternalIdScore"() {
		setup:
		def id = "this is not a valid id name"
		def count = 0
		def error = "External id name doesn't exist"

		when:
		infoController.params.name = id

		def res = infoController.getExternalIdScore()
		logger.debug "RES: ${infoController.response.json}"

		then:
		infoController.response.status == 400
		def content = infoController.response.json
		content.external_id == id
		content.count == count
		content.error == error
	}

	def "GetNullExternalIdScore"() {
		setup:
		def id = null
		def count = 0
		def error = "parameter 'name' cannot be null or empty."

		when:
		infoController.params.name = null

		infoController.getExternalIdScore()
		logger.debug "RES: ${infoController.response.json}"

		then:
		infoController.response.status == 400
		def content = infoController.response.json
		content.external_id == "invalid"
		content.count == count
		content.error == error
	}

	def "GetEmptyExternalIdScore"() {
		setup:
		def id = ""
		def count = 0
		def error = "parameter 'name' cannot be null or empty."

		when:
		infoController.params.name = id

		def res = infoController.getExternalIdScore()
		logger.debug "RES: ${infoController.response.json}"

		then:
		infoController.response.status == 400
		def content = infoController.response.json
		content.external_id == "invalid"
		content.count == count
		content.error == error
	}


	def "GetAllExtIdScores"() {
		when:
		infoController.getAllExtIdScores()

		then:
		infoController.response.status == 200

		def jsonresp = infoController.response.json
		jsonresp != null
		jsonresp['KEGG'] != null
		jsonresp['KEGG'] == 4

		logger.debug(jsonresp.toString())
	}
}
