package edu.ucdavis.fiehnlab.cts.pages

import geb.Page

/**
 * Created by Diego on 10/5/2015.
 */
class CTSHomePage extends Page {
	static url = "http://localhost:8080"

	static at = {
		$("h2").text() == "Hello"
	}

	static content = {
		heading {$("h2").text()}
	}
}
