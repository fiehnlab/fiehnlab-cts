package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.cts.pages.CTSHomePage
import geb.spock.GebReportingSpec
import org.apache.log4j.Logger

/**
 * Created by Diego on 10/5/2015.
 */
class WebsiteFunctionalSpec extends GebReportingSpec {
	private static Logger logger = Logger.getLogger(WebsiteFunctionalSpec.class)

	public def "test homepage"() {
		when:
		to CTSHomePage
		logger.debug "TITLE= $title"

		then:
		assert title == "Chemical Translation Service"

	}

	public def "simple conversion page"() {
		when:
		go "convertion"

		then:
		assert heading == "Simple Conversion"
	}
}
