package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger


class InfoServiceTests extends GroovyTestCase {
	private static Logger logger = Logger.getLogger(InfoServiceTests.class)
	def infoService

	void testGetExtIdScore() {
		def id = "KEGG"

		def res = infoService.getExternalIdScore(id)
		logger.debug "result: $res"

		assert res != null
		assert id == res.external_id
		assert 4 == res.count
	}

	void testBadExtId() {
		def id = "this should not exist"

		def res = infoService.getExternalIdScore(id)
		logger.debug "result: $res"

		assert res != null
		assert 'inexistent' == res.external_id
		assert 0 == res.count
	}

	void testNullExtId() {

		def res = infoService.getExternalIdScore(null)
		logger.debug "result: $res"

		assert res != null
		assert 'inexistent' == res.external_id
		assert 0 == res.count
	}
}
