package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.chemify.Chemify
import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Identify
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit
import edu.ucdavis.fiehnlab.chemify.identify.TestIdentifyMapper
import edu.ucdavis.fiehnlab.chemify.identify.cts.CTSConvertIdentify
import edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
class ChemifyServiceTests extends GroovyTestCase {
	Logger logger = Logger.getLogger(this.class)

	ChemifyService chemifyService
	Chemify chemifyBean

	@Before
	void setUp() {
		chemifyService = new ChemifyService()
		chemifyService.chemifyBean = chemifyBean
	}

	@Test
	void testIndex() {
		def props = "{\n" +
				"  \"class\": \"edu.ucdavis.fiehnlab.chemify.configuration.SpringConfiguration\",\n" +
				"  \"searchTermMinQuality\": 0.25,\n"
		def filters = "  \"filters\": [\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.filter.TopNFilter\",\n" +
				"      \"priority\": \"MEDIUM\",\n" +
				"      \"description\": \"this filter returns the Top 5 hits, ordered by score\"\n" +
				"    }\n" +
				"  ],\n"
		def algorithms = "  \"algorithms\": [\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cdk.InchiCodeConverter\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the CDK tools to convert a search term to a valid InChI Code\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.fiehnlab.LipidIdentifyFile\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"identifies the search term by using the Fiehnlab lipid/lcms database based on sodium citrate. Please be aware that it only has a subset of InChI Keys\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.CLStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.FAStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.GLStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.GPStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.LMStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.SPStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.STStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.SummarizedTGLipidIdentify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSCasIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSHMDBIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSInChIKeyIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSKeggIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"      \"priority\": \"EXACT_SEARCH_BASED\",\n" +
				"      \"description\": \"this method utilizes the PubChem webservice to identify a search term\"\n" +
				"    },\n" +
//				"    {\n" +
//				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.chemspider.ChemSpiderSearchIdentify\",\n" +
//				"      \"priority\": \"WEB_SERVICE_BASED_EXTERNAL\",\n" +
//				"      \"description\": \"this method utilizes the Chemspider webservice to identify a search term\"\n" +
//				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSConvertIdentify\",\n" +
				"      \"priority\": \"LOW\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
//				"    {\n" +
//				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSChemSpiderToInChIKeyIdentify\",\n" +
//				"      \"priority\": \"VERY_LOW\",\n" +
//				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
//				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll\",\n" +
				"      \"priority\": \"NO_RESULT_POSSIBLE\",\n" +
				"      \"description\": \"this declares that there was no identification possible\"\n" +
				"    }\n" +
				"  ],\n"
		def scoring = "  \"scoring\": [\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone\",\n" +
				"      \"priority\": \"NO_RESULT_POSSIBLE\",\n" +
				"      \"description\": \"this method does not do any scoring fo the result term\"\n" +
				"    }\n" +
				"  ]"

		def res = chemifyService.getConfiguration()

		assertNotNull res
		assertFalse res.isEmpty()

		assert res.contains(props)
		assert res.contains(filters)
		assert res.contains(algorithms)
		assert res.contains(scoring)
	}

	@Test
	void testIdentifyGoodValue() {
		def exp = ["QNAYBMKLOCPYGJ-REOHCLBHSA-N", "QNAYBMKLOCPYGJ-UHFFFAOYSA-N", "QNAYBMKLOCPYGJ-UWTATZPHSA-N"]
		def exp1 = "\"result\": \"QNAYBMKLOCPYGJ-REOHCLBHSA-N\",\n" +
				"    \"query\": \"alanine\",\n" +
				"    \"algorithm\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"    \"score\": 1.0,\n" +
				"    \"scoring_algorithm\": \"edu.ucdavis.fiehnlab.chemify.scoring.cts.ScoreByBiologicalCount\",\n"

		def exp2 = "    \"result\": \"QNAYBMKLOCPYGJ-UHFFFAOYSA-N\",\n" +
				"    \"query\": \"alanine\",\n" +
				"    \"algorithm\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"    \"score\": 0.85,\n" +
				"    \"scoring_algorithm\": \"edu.ucdavis.fiehnlab.chemify.scoring.cts.ScoreByBiologicalCount\",\n"

		def exp3 = "    \"result\": \"QNAYBMKLOCPYGJ-UWTATZPHSA-N\",\n" +
				"    \"query\": \"alanine\",\n" +
				"    \"algorithm\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"    \"score\": 0.0,\n" +
				"    \"scoring_algorithm\": \"edu.ucdavis.fiehnlab.chemify.scoring.cts.ScoreByBiologicalCount\",\n"

		List<? extends Hit> res = chemifyService.identify("alanine")

		def inchis = res['inchiKey'].sort()

		assert inchis.containsAll(exp.sort())
	}

	@Test
	void testIdentifyEmptyValue() {

		def msg = shouldFail(IllegalArgumentException) {
			chemifyService.identify("")
		}

		assert "you need to provide a name!", msg
	}

	@Test
	void testIdentifyNullValue() {

		def msg = shouldFail(IllegalArgumentException) {
			chemifyService.identify("")
		}

		assert "you need to provide a name!", msg
	}

	@Test
	void testIdentifyInexistentValue() {
		String badName = "this should not exist"
		def exp = [new ScoredHit(new HitImpl("nothing found", new TestIdentifyMapper(), badName, badName), new NoScoringDone(), 0.0)]

		def identifier
		for (Identify algo : chemifyBean.defaultConfiguration.algorithms) {
			if (algo instanceof CTSConvertIdentify) {
				identifier = algo
			}
		}

		List<? extends Hit> res = chemifyService.identify(badName)

		assertEquals exp.inchiKey, res.inchiKey
	}
}
