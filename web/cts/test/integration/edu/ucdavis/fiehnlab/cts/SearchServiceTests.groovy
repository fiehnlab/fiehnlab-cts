package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.junit.*

class SearchServiceTests extends GroovyTestCase {
	SearchService searchService
	Compound testComp
	Logger logger = Logger.getLogger(getClass())

	@Ignore
	@Before
	void setUp() {

		logger.setLevel(Level.DEBUG)
		testComp = new Compound(inchiKey: 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
				inchiCode: 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
				molWeight: 380.2286)
	}

	@Ignore
	@Test
	void testSearchByName() {
		def vals = [query:'carbamico']
		List comps = searchService.search(vals)

		logger.debug(comps)

		assertEquals testComp, comps.get(0)
	}

	@Ignore
	@Test
	void testSearchById() {
		def vals = [query:'150001']
		List comps = searchService.search(vals)
		assertEquals testComp, comps.get(0)
	}

	@Ignore
	@Test
	void testBadParamSearchWithoutType() {
		def vals = [query:'non existing value']
		List comps = searchService.search(vals)
		assertEquals 0, comps.size()
	}

	@Ignore
	@Test
	void testBadParamSearchWrongTypes() {
		def vals = [query:'150001', max:'hola', offset:'chau']
		List comps = searchService.search(vals)
		assertEquals testComp, comps.get(0)
	}

	@Ignore
	@Test
	void testSearchMultiResults() {
		def testComps = []

		testComps.add(new Compound(inchiKey: 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
			inchiCode: 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
			molWeight: 380.2286))
		testComps.add(new Compound(inchiKey: 'QACVMOHIHXGYLO-UHFFFAOYSA-N',
			inchiCode: 'InChI=1S/C17H19N5O3/c1-3-25-17(23)22-14-8-12-15(16(18)21-14)20-13(9-19-12)10-5-4-6-11(7-10)24-2/h4-8,19H,3,9H2,1-2H3,(H3,18,21,22,23)',
			molWeight: 341.36446))
		testComps.add(new Compound(inchiKey: 'OYHTZOLNNSQJLH-UHFFFAOYSA-N',
			inchiCode: '',
			molWeight: 326.35312))
		testComps.add(new Compound(inchiKey: 'DNVPMVJYQWFBIK-UHFFFAOYSA-N',
			inchiCode: '',
			molWeight: 341.36446))
		testComps.add(new Compound(inchiKey: 'BWJOJAFDVLMQIH-UHFFFAOYSA-N',
			inchiCode: '',
			molWeight: 401.41642))

		def vals=[query:'carbamate', max:'10', offset:'0']
		def comps = searchService.search(vals)
		assertEquals 1, comps.size()
		assertTrue testComps.containsAll(comps)
	}
}
