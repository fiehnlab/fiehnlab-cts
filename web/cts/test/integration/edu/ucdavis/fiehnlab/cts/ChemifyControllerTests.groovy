package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
class ChemifyControllerTests extends GroovyTestCase {
	Logger logger = Logger.getLogger(this.class)
	ChemifyController chemifyController

	@Before
	void setUp() {
		chemifyController = new ChemifyController()
	}

	@Test
	void testIndex() {
		def props = "{\n" +
				"  \"class\": \"edu.ucdavis.fiehnlab.chemify.configuration.SpringConfiguration\",\n" +
				"  \"searchTermMinQuality\": 0.25,\n"
		def filters = "  \"filters\": [\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.filter.TopNFilter\",\n" +
				"      \"priority\": \"MEDIUM\",\n" +
				"      \"description\": \"this filter returns the Top 5 hits, ordered by score\"\n" +
				"    }\n" +
				"  ],\n"
		def algorithms = "  \"algorithms\": [\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cdk.InchiCodeConverter\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the CDK tools to convert a search term to a valid InChI Code\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.fiehnlab.LipidIdentifyFile\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"identifies the search term by using the Fiehnlab lipid/lcms database based on sodium citrate. Please be aware that it only has a subset of InChI Keys\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.CLStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.FAStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.GLStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.GPStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.LMStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.SPStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.STStrGenLipify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.SummarizedTGLipidIdentify\",\n" +
				"      \"priority\": \"CALCULATION_BASED\",\n" +
				"      \"description\": \"this method utilizes the Lipid Maps tools to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSCasIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSHMDBIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSInChIKeyIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSKeggIdentify\",\n" +
				"      \"priority\": \"PATTERN_BASED_APPROACH\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"      \"priority\": \"EXACT_SEARCH_BASED\",\n" +
				"      \"description\": \"this method utilizes the PubChem webservice to identify a search term\"\n" +
				"    },\n" +
//				"    {\n" +
//				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.chemspider.ChemSpiderSearchIdentify\",\n" +
//				"      \"priority\": \"WEB_SERVICE_BASED_EXTERNAL\",\n" +
//				"      \"description\": \"this method utilizes the Chemspider webservice to identify a search term\"\n" +
//				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSConvertIdentify\",\n" +
				"      \"priority\": \"LOW\",\n" +
				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
				"    },\n" +
//				"    {\n" +
//				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.cts.CTSChemSpiderToInChIKeyIdentify\",\n" +
//				"      \"priority\": \"VERY_LOW\",\n" +
//				"      \"description\": \"this method utilizes the CTS Webservice to identify a search term\"\n" +
//				"    },\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll\",\n" +
				"      \"priority\": \"NO_RESULT_POSSIBLE\",\n" +
				"      \"description\": \"this declares that there was no identification possible\"\n" +
				"    }\n" +
				"  ],\n"
		def scoring = "  \"scoring\": [\n" +
				"    {\n" +
				"      \"class\": \"edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone\",\n" +
				"      \"priority\": \"NO_RESULT_POSSIBLE\",\n" +
				"      \"description\": \"this method does not do any scoring fo the result term\"\n" +
				"    }\n" +
				"  ]"

		chemifyController.index()

		def res = chemifyController.response.contentAsString

		assertNotNull res
		assertFalse res.isEmpty()

		assert res.contains(props)
		assert res.contains(filters)
		assert res.contains(algorithms)
		assert res.contains(scoring)
	}

	@Test
	void testIdentifyGoodValue() {
		def exp1 = "\"result\": \"QNAYBMKLOCPYGJ-REOHCLBHSA-N\",\n" +
				"    \"query\": \"alanine\",\n" +
				"    \"algorithm\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"    \"score\": 0.0,\n" +
				"    \"scoring_algorithm\": \"edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone\",\n"

		def exp2 = "    \"result\": \"QNAYBMKLOCPYGJ-UHFFFAOYSA-N\",\n" +
				"    \"query\": \"alanine\",\n" +
				"    \"algorithm\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"    \"score\": 0.0,\n" +
				"    \"scoring_algorithm\": \"edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone\",\n"

		def exp3 = "    \"result\": \"QNAYBMKLOCPYGJ-UWTATZPHSA-N\",\n" +
				"    \"query\": \"alanine\",\n" +
				"    \"algorithm\": \"edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify\",\n" +
				"    \"score\": 0.0,\n" +
				"    \"scoring_algorithm\": \"edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone\",\n"

		chemifyController.request.json = '{"value":"alanine"}'
		chemifyController.identify()

		def res = chemifyController.response.contentAsString
		def jsonres = chemifyController.response.json

		println jsonres

		assertTrue res.contains(exp1)
		assertTrue res.contains(exp2)
		assertTrue res.contains(exp3)
	}

	@Test
	void testIdentifyEmptyValue() {
		def exp = "Can't search for a null or empty value. Please call the service like: 'http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/<value>' or 'http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/query?value=<search term>'."

		chemifyController.params.value = ""
		chemifyController.identify()

		def res = chemifyController.response.contentAsString

		assert res.contains(exp)
	}

	@Test
	void testIdentifyNullValue() {
		def exp = "Can't search for a null or empty value. Please call the service like: 'http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/<value>' or 'http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/query?value=<search term>'."

		chemifyController.params.value = null
		chemifyController.identify()

		def res = chemifyController.response.contentAsString
		logger.error(chemifyController.response.json)
		assert res.contains(exp)
	}

	@Test
	void testIdentifyInexistentValue() {
		def exp = [result           : "RQFUZUMFPRMVDX-UHFFFAOYSA-N",
		           query            : "3-bromopropanol",
		           algorithm        : "edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify",
		           score            : 0.0,
		           scoring_algorithm: "edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone"]

		chemifyController.params.value = "3 bromopropanol"
		chemifyController.identify()

		def res = JSON.parse(chemifyController.response.contentAsString)[0]

		assertEquals exp.result, res.result
		assertEquals exp.query, res.query
		assertEquals exp.scoring_algorithm, res.scoring_algorithm
		assert res.algorithm.matches(/.*(?:PCNameIdentify|ChemSpiderSearchIdentify)/)
	}
}
