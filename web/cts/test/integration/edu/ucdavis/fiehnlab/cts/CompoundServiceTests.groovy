package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit
import edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify
import edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone
import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * Created by diego on 7/29/15.
 */
class CompoundServiceTests extends GroovyTestCase {
	private static Logger logger = Logger.getLogger(this.class)
	def compoundService

	@Before
	void setUp() {
		Compound.withNewTransaction {
			Compound.findByInchiKey('ZPUCINDJVBIVPJ-LJISPDSOSA-N')?.delete(flush: true, failOnError: true)
			Compound.findByInchiKey('ZPUCINDJVBIVPJ-LJISPDSOSA-O')?.delete(flush: true, failOnError: true)
		}
	}

	@After
	void tearDown() {
		Compound.withNewTransaction {
			Compound.findByInchiKey('ZPUCINDJVBIVPJ-LJISPDSOSA-N')?.delete(flush: true, failOnError: true)
			Compound.findByInchiKey('ZPUCINDJVBIVPJ-LJISPDSOSA-O')?.delete(flush: true, failOnError: true)
		}
	}

	@Test
	void testGetExpandedFormula() {
		def formula = [:]

		formula["H"] = "H"
		formula["N2"] = "NN"
		formula["CO2"] = "COO"
		formula["NaCl"] = "ClNa"
		formula["H2O"] = "HHO"
		formula["OH2"] = "HHO"
		formula["C6H12"] = "CCCCCCHHHHHHHHHHHH"
		formula["C8H10N4O2"] = "CCCCCCCCHHHHHHHHHHNNNNOO"
		formula["C8H10O2N4"] = "CCCCCCCCHHHHHHHHHHNNNNOO"
		formula["CH3CH2CH2CH3"] = "CCCCHHHHHHHHHH"

		formula.entrySet().each { Map.Entry<String, String> entry ->
			long start = System.nanoTime()
			def res = compoundService.getExpandedFormula(entry.key)
			def diff = (System.nanoTime() - start) / 1000000000
			logger.info("${entry.key} expanded to ${res} in ${diff}ms")
			assertEquals(entry.value, res)
		}
	}

    @Test
    void testAddCompoundFromHit() {
	    def term = "Cocaine"
        def count = Compound.count

	    def hit = new ScoredHit(new HitImpl("ZPUCINDJVBIVPJ-LJISPDSOSA-O", new PCNameIdentify(), term, term), new NoScoringDone(), 0.0)
	    compoundService.addCompoundFromHit(hit)

        assert(count == Compound.count - 1)

	    def c = Compound.findByInchiKey("ZPUCINDJVBIVPJ-LJISPDSOSA-O")
        assertEquals ("InChI=1S/C17H21NO4/c1-18-12-8-9-13(18)15(17(20)21-2)14(10-12)22-16(19)11-6-4-3-5-7-11/h3-7,12-15H,8-10H2,1-2H3/p+1/t12-,13+,14-,15+/m0/s1", c?.inchiCode)
    }

	@Test
	void testAddCompoundListFromHits() {
		def count = Compound.count
		def hits = []

		hits.add(new ScoredHit(new HitImpl("ZPUCINDJVBIVPJ-LJISPDSOSA-N", new PCNameIdentify(), "Cocaine", "Cocaine"), new NoScoringDone(), 0.0))
		hits.add(new ScoredHit(new HitImpl("ZPUCINDJVBIVPJ-LJISPDSOSA-O", new PCNameIdentify(), "Cocaine", "Cocaine"), new NoScoringDone(), 0.0))
		compoundService.addCompounds(hits)

		def nowct = Compound.count
		assert (count == nowct - hits.size())

		def c = Compound.findByInchiKey("ZPUCINDJVBIVPJ-LJISPDSOSA-N")
		assertEquals("InChI=1S/C17H21NO4/c1-18-12-8-9-13(18)15(17(20)21-2)14(10-12)22-16(19)11-6-4-3-5-7-11/h3-7,12-15H,8-10H2,1-2H3/t12-,13+,14-,15+/m0/s1", c?.inchiCode)
	}
}
