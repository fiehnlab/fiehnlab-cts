package edu.ucdavis.fiehnlab.cts
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
/**
 * test  similarity scoringController  functions
 * @author narendra
 */
@RunWith(JUnit4)
class SimilarityServiceTests extends GroovyTestCase {
	/**
	 * creating object of respective object
	 */
	SimilarityService similarityService
	def dataSource
	public static final List<String> bioDbList = ['KEGG', 'Human Metabolome Database', 'BioCyc']

	Logger logger = Logger.getLogger(getClass())

	@Before
	void setUp() {
		similarityService = new SimilarityService()
		similarityService.dataSource = dataSource
	}

	@Test
	void testExternalIdCount() {
		def exp = ['datasource_count': 3]
		def res = similarityService.externalIdCount("RYXHPKXJBBAPCB-UHFFFAOYSA-N")
		assertEquals exp, res
	}

	@Test
	void testExternalIdCountFailed() {
		def exp = ['datasource_count':"Please provide a valid InchiKey"]
		def res = similarityService.externalIdCount("RYXHPKXJBBAPCB")
		assertEquals exp, res

	}

	@Test
	void testExternalIdCountFailedSecond() {
		def exp = ['datasource_count':"Please provide a valid InchiKey"]
		def res = similarityService.externalIdCount("")
		assertEquals exp, res
	}

	@Test
	void testBioIdCount() {
		def res = similarityService.bioIdCount("QNAYBMKLOCPYGJ-REOHCLBHSA-N", bioDbList)
		def exp = ["KEGG": 2L, "Human Metabolome Database": 1L, "total": 3L]

		assertEquals 3, res.size()
		assertEquals(exp, res)  //Map content comparison
	}

	@Test
	void testBioIdCount2() {
		def res = similarityService.bioIdCount("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", bioDbList)
		def exp = ["Human Metabolome Database":1L, "KEGG": 1L, "total": 2L]

		assertEquals 3, res.size()
		assertEquals(exp, res)
	}

	@Test
	void testBioIdCountThreonicAcid() {
		def res = similarityService.bioIdCount("JPIJQSOTBSSVTP-GBXIJSLDSA-N", bioDbList)
		def exp = ["Human Metabolome Database":1L, "total": 1L]

		assertEquals 2, res.size()
		assertEquals(exp, res)  //Map content comparison

		def res2 = similarityService.bioIdCount("JPIJQSOTBSSVTP-STHAYSLISA-N", bioDbList)
		def exp2 = ["KEGG": 1L, "total": 1L]

		assertEquals 2, res2.size()
		assertEquals(exp2, res2)  //Map content comparison

		def res3 = similarityService.bioIdCount("JPIJQSOTBSSVTP-UHFFFAOYSA-N", bioDbList)
		def exp3 = ["total": 0L]

		assertEquals 1, res3.size()
		assertEquals(exp3, res3)  //Map content comparison
	}

	@Test
	void testNoBioIdCount() {
		Map res = similarityService.bioIdCount("ZZZZMKLOCPYGJZ-REOHCLBHSA-N", bioDbList)
		Map exp = ["total":0]

		assertEquals 1, res.size()
		assertEquals(exp, res)
	}

	@Test
	void testSynonymsForInchikey() {
		def exp = new HashSet<String>()
		exp.add("Alanine")
		exp.add("ALANINE")

		def res = similarityService.synonymsForInchikey("QNAYBMKLOCPYGJ-REOHCLBHSA-N")

		assert res.size() > 0
		assertArrayEquals(exp.toArray(), res.toArray())
	}

//	@Test
//	void testBadInput() {
//		Map res = similarityService.bioIdCount("bad inchi", bioDbList)
//		Map exp = ["error":"Please provide a valid InchiKey"]
//
//		assertEquals 1, res.size()
//		assertEquals(exp, res)
//	}
}
