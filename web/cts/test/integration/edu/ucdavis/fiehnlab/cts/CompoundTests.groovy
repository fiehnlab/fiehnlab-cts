package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 6/20/13
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
class CompoundTests {
	Logger logger = Logger.getLogger(getClass())
	Compound c

	@Before
	void setUp() {
//		logger.setLevel(Level.DEBUG)
		c = new Compound(inchiKey: 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
				inchiCode: 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
				molWeight: 380.2286,
				exactMass: 378.2286,
				formula: "C16H15Cl2N5O2",
				synonyms: [],
				extIds: []
		)
	}

	@After
	void tearDown() {
		c = null
	}

	@Test
	void testToJson() {
		JSON.use('deep')
		def exp
		exp = ['inchikey'        : 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
		       'inchicode'       : 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
		       'molweight'       : 380.2286,
		       'exactmass'       : 378.2286,
		       'formula'         : 'C16H15Cl2N5O2',
		       'synonyms'        : [],
		       'externalIds'     : [],
		       'dataSourcesCount': 0,
		       'referencesCount' : 0,
		       'pubmedHits'      : 0
		] as JSON

		exp = exp.target
		def res = c.toJson().target

		assert exp.equals(res)
	}
}
