package edu.ucdavis.fiehnlab.cts.filter;

public abstract class AbstractResultFilterTest {


	protected abstract String getTestTerm()

	protected abstract String getExpectedTerm()

	public abstract getFilter()
}