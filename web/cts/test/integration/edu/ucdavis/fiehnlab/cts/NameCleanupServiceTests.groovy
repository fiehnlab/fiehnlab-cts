package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.chemify.exception.InvalidSearchTermException
import org.apache.log4j.Logger
import org.junit.Test

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */

class NameCleanupServiceTests extends GroovyTestCase {
	private static Logger logger = Logger.getLogger(NameCleanupServiceTests.class)

	def nameCleanupService

	void testCleanName() {
		def names = [
				"<good =name>\t\n\t\t",
				"&alan/ine%",
				"1,2 |diol",
				"~my super! \\name`",
				"&lt;html ^encoded&gt;",
				"A-&alpha; C (2-Amino@ 9H pyrido[2,3 b]indole)",
				"<i>i can't</i> even copy paste without HTML tags",
				"&lt;i&gt;trans&lt;/i&gt;-1,2-dihydrobenzene-1,2-diol"
		]

		def cleanNames = []
		names.each { cleanNames.add(nameCleanupService.cleanName(it)) };

		def exp = [
				"good name",
				"alanine",
				"1,2 diol",
				"my super name",
				"html encoded",
				"A-alpha C (2-Amino 9H pyrido[2,3 b]indole)",
				"i can\'t even copy paste without HTML tags",
				"trans-1,2-dihydrobenzene-1,2-diol"
		]

		logger.debug "exp: ${exp.toListString()}\nres: ${cleanNames.toListString()}"

		assertArrayEquals exp.toArray(), cleanNames.toArray()
	}

	void testCleanupTaggedName() {
		def name = "&lt;i&gt;N&lt;/i&gt;-acetylmuramoyl-L-alanyl-D-isoglutaminyl-N-(&beta;-D-asparatyl)-L-lysyl-D-alanyl-D-alanine-diphosphoundecaprenyl-&lt;i&gt;N&lt;/i&gt;-acetylglucosamine"
		def clean = "N-acetylmuramoyl-L-alanyl-D-isoglutaminyl-N-(beta-D-asparatyl)-L-lysyl-D-alanyl-D-alanine-diphosphoundecaprenyl-N-acetylglucosamine"
		assertEquals clean, nameCleanupService.cleanName(name)

	}

	@Test(expected = InvalidSearchTermException.class)
	void testCleanupNullName() {
		nameCleanupService.cleanName(null)
	}

	@Test(expected = InvalidSearchTermException.class)
	void testCleanupNameResultingInEmptyName() {
		nameCleanupService.cleanName("<>><<>")
	}

}
