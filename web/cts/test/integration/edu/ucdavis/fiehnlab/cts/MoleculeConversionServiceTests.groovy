package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.cts.exceptions.InvalidInchiException
import edu.ucdavis.fiehnlab.cts.exceptions.UnsupportedFormatException
import org.apache.log4j.Logger
import org.junit.*
import org.openscience.cdk.exception.CDKException

import static org.junit.Assert.*

public class MoleculeConversionServiceTests extends GroovyTestCase {

	MoleculeConversionService service
	Logger logger = Logger.getRootLogger()
	final shouldFail = new GroovyTestCase().&shouldFail

	@Before
	public void setUp() throws Exception {
		service = new MoleculeConversionService()
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testMolToInchikey() {
		String exp = "{inchikey=QNAYBMKLOCPYGJ-REOHCLBHSA-N, inchicode=InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1}"

		String mol = ""
		Reader file = new FileReader(new File("test/resources/singleSubstance.sdf"))

		try {
			mol = file.text
		} catch (IOException e) {
			logger.error("Error reading file", e)
		} finally {
			file.close()
		}

		String res = service.molToInchi(mol)

		assertEquals(exp, res)
	}

	@Test
	public void testBadMolToInchikey() {
		String exp = "{inchikey=QNAYBMKLOCPYGJ-REOHCLBHSA-N, inchicode=InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1}"

		String mol = ""
		Reader file = new FileReader(new File("test/resources/badmol.dat"))

		try {
			mol = file.text
		} catch (IOException e) {
			logger.error("Error reading file", e)
		} finally {
			file.close()
		}

		shouldFail(UnsupportedFormatException) {
			service.molToInchi(mol)
		}
	}

	@Test
	public void testNullMolToInchikey() {

		shouldFail(UnsupportedFormatException) {
			service.molToInchi(null)
		}
	}

	@Test
	public void testInchiToMol() {
		def exp = " 13 12  0  0  0  0  0  0  0  0999 V2000\n" +
				"    1.2990   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.5981   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.5623    1.1491    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -2.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.1962   -0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    1.6339    1.1491    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.0000    0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.3349   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.2632   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.0492    2.5586    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.0395    0.8886    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    6.4952   -0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"  2  1  1  0  0  0  0 \n" +
				"  3  2  1  0  0  0  0 \n" +
				"  4  2  1  0  0  0  0 \n" +
				"  5  3  2  0  0  0  0 \n" +
				"  6  3  1  0  0  0  0 \n" +
				"  2  7  1  1  0  0  0 \n" +
				"  1  8  1  0  0  0  0 \n" +
				"  1  9  1  0  0  0  0 \n" +
				"  1 10  1  0  0  0  0 \n" +
				"  4 11  1  0  0  0  0 \n" +
				"  4 12  1  0  0  0  0 \n" +
				"  6 13  1  0  0  0  0 \n" +
				"M  END"
		String inchi = "InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1"

		def res

		try {
			res = service.inchiToMol(inchi)
		} catch (InvalidInchiException e) {
			logger.error(e.message)
		} catch (CDKException e) {
			logger.error(e.message)
		}

		assertNotNull("res shouldn't be null", res)
		logger.info("res:\n$res")
		assertNotNull("molecule shouldn't be null", res.molecule)
		logger.info("Molecule:\n${res.molecule}")
		assertTrue res.molecule.replaceAll("\\r?\\n", "\n").contains(exp)
		assertNull res.message
	}

	@Test
	public void testBadInchiToMol() {
		shouldFail(InvalidInchiException) {
			service.inchiToMol("very wrong inchi code")
		}
	}

	@Test
	public void testInchiToInchiKey() {
		String inchicode = "InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1"
		def exp = ["inchicode": "InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1", "inchikey": "QNAYBMKLOCPYGJ-REOHCLBHSA-N"]

		def res = service.inchiToInchiKey(inchicode)

		assertEquals exp, res
	}

	@Test
	public void testNullInchiToMol() {
		shouldFail(InvalidInchiException) {
			service.inchiToMol(null)
		}
	}

	@Test
	public void testInchiKeyToMol() {
		String inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
		def exp = " 13 12  0  0  0  0  0  0  0  0999 V2000\n" +
				"    1.2990   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.5981   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.5623    1.1491    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -2.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.1962   -0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    1.6339    1.1491    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.0000    0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.3349   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.2632   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.0492    2.5586    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.0395    0.8886    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    6.4952   -0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"  2  1  1  0  0  0  0 \n" +
				"  3  2  1  0  0  0  0 \n" +
				"  4  2  1  0  0  0  0 \n" +
				"  5  3  2  0  0  0  0 \n" +
				"  6  3  1  0  0  0  0 \n" +
				"  2  7  1  1  0  0  0 \n" +
				"  1  8  1  0  0  0  0 \n" +
				"  1  9  1  0  0  0  0 \n" +
				"  1 10  1  0  0  0  0 \n" +
				"  4 11  1  0  0  0  0 \n" +
				"  4 12  1  0  0  0  0 \n" +
				"  6 13  1  0  0  0  0 \n" +
				"M  END"
		def res = ""

		try {
			res = service.inchiKeyToMol(inchikey)
		} catch (InvalidInchiException e) {
			logger.error(e.message)
		} catch (CDKException e) {
			logger.error(e.message)
		}

		assertNotNull("res shouldn't be null", res)
		logger.info("res:\n$res")
		assertNotNull("molecule shouldn't be null", res.molecule)
		logger.info("Molecule:\n${res.molecule}")
		assertTrue res.molecule.replaceAll("\\r?\\n", "\n").contains(exp)
		assertNull res.message
	}

	@Test
	public void testBadInchiKeyToMol() {
		shouldFail(InvalidInchiException) {
			service.inchiKeyToMol("bad inchi key")
		}
	}

	@Test
	public void testBadInchiKeyToMol2() {
		def exp = "Couldn't find a compound for that InChIKey."
		def res = service.inchiKeyToMol("QNAYBMKLOCPYGJ-REOHCLBHSA-W")

		assertEquals exp, res.message
	}

	@Test
	public void testNullInchiKeyToMol() {
		shouldFail(InvalidInchiException) {
			service.inchiKeyToMol(null)
		}
	}

	@Test
	@Ignore
	public void testBuggyInchiCode() {
		def exp = ""
		def inchicode = "InChI=1S/2C42H80NO10P/c1-3-5-7-9-11-13-15-17-18-19-20-22-24-26-28-30-32-34-41(45)53-38(36-51-54(48,49)52-37-39(43)42(46)47)35-50-40(44)33-31-29-27-25-23-21-16-14-12-10-8-6-4-2;1-3-5-7-9-11-13-15-17-19-21-23-25-27-29-31-33-40(44)50-35-38(36-51-54(48,49)52-37-39(43)42(46)47)53-41(45)34-32-30-28-26-24-22-20-18-16-14-12-10-8-6-4-2/h28,30,38-39H,3-27,29,31-37,43H2,1-2H3,(H,46,47)(H,48,49);27,29,38-39H,3-26,28,30-37,43H2,1-2H3,(H,46,47)(H,48,49)/b30-28-;29-27-"

		def res = service.inchiToMol(inchicode)

		logger.error(res)

		assertEquals exp, res
	}

	@Test
	public void testInchicodeToMolBug() {
		def inchicode = 'InChI=1S/C12H22O11/c13-1-4-6(16)8(18)9(19)11(21-4)23-12(3-15)10(20)7(17)5(2-14)22-12/h4-11,13-20H,1-3H2/t4-,5-,6-,7-,8+,9-,10+,11-,12+/m1/s1'
		def exp = " 45 46  0  0  0  0  0  0  0  0999 V2000\n" +
		"    6.3807    3.8535    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -1.4961    2.4812    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.2386   -0.7398    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    5.2657    2.8502    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -1.1840    1.0140    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    6.4474    1.9263    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -1.1840   -0.4860    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    6.2381    0.4410    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.8472   -0.1205    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    0.2430   -0.9490    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.6655    0.8034    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    1.1240    0.2640    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    7.8071    3.3896    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -2.9227    2.9445    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.6653   -0.2764    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    7.8738    1.4623    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -2.6758   -0.3294    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    6.5496   -1.0263    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.7322   -1.1238    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -0.3666   -2.3195    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.8747    2.2887    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    0.2430    1.4780    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.2390    1.2673    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.4712    4.1225    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -2.6758    0.8571    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    7.1520    3.2505    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -1.4955   -1.9533    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    7.7373    0.4929    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    5.6417   -1.3928    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    1.5422   -1.6987    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.9608   -0.5208    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    7.0853    5.1777    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    5.1990    4.7774    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    0.0030    2.5337    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -1.2875    3.9666    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    1.0566   -1.6632    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.9427   -2.0643    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    8.9222    4.3929    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -3.2348    4.4117    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.7799   -1.2801    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    8.9888    2.4657    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -3.5573   -1.5431    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    7.9760   -1.4903    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.0436   -2.5912    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"   -1.8584   -2.4768    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"  4  1  1  0  0  0  0 \n" +
		"  5  2  1  0  0  0  0 \n" +
		"  6  4  1  0  0  0  0 \n" +
		"  7  5  1  0  0  0  0 \n" +
		"  8  6  1  0  0  0  0 \n" +
		"  9  8  1  0  0  0  0 \n" +
		" 10  7  1  0  0  0  0 \n" +
		" 11  9  1  0  0  0  0 \n" +
		" 12  3  1  0  0  0  0 \n" +
		" 12 10  1  0  0  0  0 \n" +
		" 13  1  1  0  0  0  0 \n" +
		" 14  2  1  0  0  0  0 \n" +
		" 15  3  1  0  0  0  0 \n" +
		" 16  6  1  0  0  0  0 \n" +
		" 17  7  1  0  0  0  0 \n" +
		" 18  8  1  0  0  0  0 \n" +
		" 19  9  1  0  0  0  0 \n" +
		" 20 10  1  0  0  0  0 \n" +
		" 21  4  1  0  0  0  0 \n" +
		" 21 11  1  0  0  0  0 \n" +
		" 22  5  1  0  0  0  0 \n" +
		" 22 12  1  0  0  0  0 \n" +
		" 23 11  1  0  0  0  0 \n" +
		" 12 23  1  1  0  0  0 \n" +
		"  4 24  1  1  0  0  0 \n" +
		"  5 25  1  6  0  0  0 \n" +
		"  6 26  1  6  0  0  0 \n" +
		"  7 27  1  1  0  0  0 \n" +
		"  8 28  1  1  0  0  0 \n" +
		"  9 29  1  6  0  0  0 \n" +
		" 10 30  1  6  0  0  0 \n" +
		" 11 31  1  6  0  0  0 \n" +
		"  1 32  1  0  0  0  0 \n" +
		"  1 33  1  0  0  0  0 \n" +
		"  2 34  1  0  0  0  0 \n" +
		"  2 35  1  0  0  0  0 \n" +
		"  3 36  1  0  0  0  0 \n" +
		"  3 37  1  0  0  0  0 \n" +
		" 13 38  1  0  0  0  0 \n" +
		" 14 39  1  0  0  0  0 \n" +
		" 15 40  1  0  0  0  0 \n" +
		" 16 41  1  0  0  0  0 \n" +
		" 17 42  1  0  0  0  0 \n" +
		" 18 43  1  0  0  0  0 \n" +
		" 19 44  1  0  0  0  0 \n" +
		" 20 45  1  0  0  0  0 \n" +
		"M  END\n"

		def res = service.inchiToMol(inchicode)

		assert res.molecule.replaceAll("\\r?\\n", "\n").contains(exp)
	}
}
