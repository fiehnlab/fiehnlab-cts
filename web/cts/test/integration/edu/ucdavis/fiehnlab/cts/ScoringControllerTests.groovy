package edu.ucdavis.fiehnlab.cts

import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

class ScoringControllerTests {
	ScoringController scoringController

	@Before
	void setup() {
		scoringController = new ScoringController()
	}

	@Test
	void testScoreFromAnyId() {
		def exp = """{"searchTerm":"AC1L43G0","from":"Abi Chem","result":[{"InChIKey":"RYXHPKXJBBAPCB-UHFFFAOYSA-N","score":1}]}"""

		scoringController.params.from = "Abi Chem"
		scoringController.params.value = "AC1L43G0"
		scoringController.params.algorithm = ConstantHelper.SCORING_COUNT
		scoringController.score()

		def res = scoringController.response.contentAsString

		assertNotNull res
		assertEquals exp, res
	}

	@Test
	void testScoreFromName() {
		def exp = """{"searchTerm":"alanine","from":"chemical name","result":[{"InChIKey":"QNAYBMKLOCPYGJ-REOHCLBHSA-N","score":1},{"InChIKey":"QNAYBMKLOCPYGJ-UHFFFAOYSA-N","score":1},{"InChIKey":"QNAYBMKLOCPYGJ-UWTATZPHSA-N","score":0}]}"""

		scoringController.params.from = "chemical name"
		scoringController.params.value = "alanine"
		scoringController.params.algorithm = ConstantHelper.SCORING_COUNT
		scoringController.score()

		def res = scoringController.response.contentAsString

		assertNotNull res
		assertEquals exp[0], res[0]
		assertEquals exp[1], res[1]
		assertEquals exp[2], res[2]

	}

	@Test
	void testScoreFromInexistentName() {
		def exp = """{"searchTerm":"3 bromopropanol","from":"chemical name","result":[{"InChIKey":"RQFUZUMFPRMVDX-UHFFFAOYSA-N","score":1}]}"""

		scoringController.params.from = "chemical name"
		scoringController.params.value = "3 bromopropanol"
		scoringController.params.algorithm = ConstantHelper.SCORING_COUNT
		scoringController.score()

		def res = scoringController.response.contentAsString

		assertNotNull res
		assertEquals exp, res
	}

}
