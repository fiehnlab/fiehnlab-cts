package edu.ucdavis.fiehnlab.cts

import org.junit.Test

import static edu.ucdavis.fiehnlab.cts.ConstantHelper.NAMES_INCHI_KEY

/**
 * Created by diego on 10/21/2014.
 */
class SimpleTests extends GroovyTestCase {
	ConversionService conversionService

// ------- from name ----------------------------------
	@Test
	void 'testCC-name2key'() {
		def exp = [["fChemical Name": "alanine", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0D),
		                                                      new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.85D),
		                                                      new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.0D)]]]
		def res = conversionService.convert("Chemical Name", "alanine", "InChIKey")

		assertFalse res.isEmpty()
		assertEquals exp[NAMES_INCHI_KEY][0].value[0..2], res[NAMES_INCHI_KEY][0].value[0..2]
	}


	@Test
	void 'testCC-name2name'() {
		def exp = [["fChemical Name": "alanine", "Chemical Name": [new ScoredValue("L-Alanine", 1.0),
		                                                           new ScoredValue("Alanine", 0.74),
		                                                           new ScoredValue("D-Alanine", 0.2324823196550647)]]]
		def res = conversionService.convert("Chemical Name", "alanine", "Chemical Name")

		assertFalse(res.isEmpty())
		assert res.value.containsAll(exp.value)
	}


	@Test
	void 'testCC-name2code'() {
		def exp = [["fChemical Name": "alanine", "InChI Code": [new ScoredValue("InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1", 1.0),
                                                                new ScoredValue("inchiCode", 1.0),
                                                                new ScoredValue("InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m1/s1", 1.0)]]]
		def res = conversionService.convert("Chemical Name", "alanine", "InChI Code")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}


	@Test
	void 'testCC-name2eid'() {
		def exp = [["fChemical Name": "alanine", "KEGG": [new ScoredValue("C00041", 1.0), new ScoredValue("D00012", 1.0), new ScoredValue("BC1290", 0.7500708247323061)]]]
		def res = conversionService.convert("Chemical Name", "alanine", "KEGG")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}

// ------- from inchikey ----------------------------------

	@Test
	void 'testCC-key2key'() {
		def exp = [["fInChIKey": "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0)]]]
		def res = conversionService.convert("InChIKey", "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "inchikey")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}


	@Test
	void 'testCC-key2name'() {
        def exp = [["fInChIKey": "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "Chemical Name": [new ScoredValue("ALANINE", 1.0D), new ScoredValue("Alanine", 1.0D)]]]
		def res = conversionService.convert("InChIKey", "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "Chemical Name")

		assertFalse(res.isEmpty())
		assertEquals(exp[0].fInChIKey, res[0].fInChIKey)

		def sexp = exp[0]["Chemical Name"].sort()
		def sres = res[0]["Chemical Name"].sort()
		assertEquals(sexp, sres)
	}


	@Test
	void 'testCC-key2code'() {
		def exp = [["fInChIKey": "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "InChI Code": [new ScoredValue("InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1", 1.0D)]]]
		def res = conversionService.convert("InChIKey", "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "InChI Code")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}


	@Test
	void 'testCC-key2eid'() {
		def exp = [["fInChIKey": "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "KEGG": [new ScoredValue("C00041", 1.0D), new ScoredValue("D00012", 1.0D)]]]
		def res = conversionService.convert("InChIKey", "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "KEGG")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}

// ------- from extId ----------------------------------

	@Test
	void 'testCC-eid2key'() {
		def exp = [["fKEGG": "C00041", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0D)]]]
		def res = conversionService.convert("KEGG", "C00041", "InChIKey")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}


	@Test
	void 'testCC-eid2name'() {
        def exp = [["fKEGG": "C00041", "Chemical Name": [new ScoredValue("ALANINE", 1.0D), new ScoredValue("Alanine", 1.0D)]]]
		def res = conversionService.convert("KEGG", "C00041", "Chemical Name")

		assertFalse(res.isEmpty())
		assertEquals(exp[0].fKEGG, res[0].fKEGG)

		def sexp = exp[0]["Chemical Name"].sort()
		def sres = res[0]["Chemical Name"].sort()

		assertEquals(sexp, sres)
	}


	@Test
	void 'testCC-eid2code'() {
		def exp = [["fKEGG": "C00041", "InChI Code": [new ScoredValue("InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1", 1.0D)]]]
		def res = conversionService.convert("kegg", "C00041", "InChI Code")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}


	@Test
	void 'testCC-eid2eid'() {
		def exp = [["fKEGG": "C00041", "Human Metabolome Database": [new ScoredValue("HMDB00161", 1.0D)]]]
		def res = conversionService.convert("kEgG", "C00041", "Human Metabolome Database")

		assertFalse(res.isEmpty())
		assertEquals(exp, res)
	}
}
