package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test

/**
 * Created by Diego on 9/24/2015.
 */
class BigBatchTest extends GroovyTestCase {
	def searchService
	def conversionService
	ConversionController controller
	private static Logger logger = Logger.getLogger(BigBatchTest.class)

	private def data = /"N-Acetyl-L-glutamate 5-semialdehyde\n
 Pantothenate\n
 Pantothenate\n
 5-Hydroxy-N-formylkynurenine\n
 Xanthurenic acid\n
 L-Formylkynurenine\n
 Triaziquone\n
 4-(2-Aminophenyl)-2,4-dioxobutanoate\n
 3-Hydroxykynurenine\n
 N-(Acetyloxy)benzenamine\n
 3-Hydroxykynurenine\n
 2-Aminophenol\n
 4,8-Dihydroxyquinoline\n
 Triaziquone\n
 4-Hydroxy-2-quinolinecarboxylic acid\n
 Bis(1-aziridinyl)morpholinophosphine sulfide\n
 4-Hydroxy-2-quinolinecarboxylic acid\n
 8-Oxodeoxycoformycin\n
 Sphingosine\n
 Acrovestone\n
 Sphingosine\n
 L-Albizziine\n
 sn-Glycero-3-phosphoethanolamine\n
 7-Mercaptoheptanoic acid\n
 5-Hydroxyindoleacetate\n
 Hypoxantine\n
 Inosine\n
 2-Phenylacetamide\n
 Phenylpyruvate\n
 L-Kynurenine\n
 (3S,5S)-Carbapenam-3-carboxylic acid\n
 L-Tyrosine\n
 Coumarin\n
 Octadecanamide\n
 N-Acetyl-L-glutamate 5-semialdehyde\n
 L-Lysine"/

	@Before
	void setup() {
		controller = new ConversionController()
		conversionService.searchService = searchService
		controller.conversionService = conversionService
	}

	@Test
	void testBigSingleConversion() {
		def sdata = data

		controller.params.from = "Chemical Name"
		controller.params.to = "InChIKey"
		controller.params.values = sdata
		controller.convert()

		assert controller.params.values.size() < sdata.size()
		assertEquals 200, controller.response.status

		def modelView = controller.modelAndView

		assertEquals "Query too long, using the first ${controller.SINGLE_QUERY_MAX_LENGTH} characters.", controller.flash.message
		assertEquals sdata[0..<controller.SINGLE_QUERY_MAX_LENGTH], modelView.model.values
		assertEquals 1, modelView.model.results.size()
	}

	@Test
	void testBigSingleExport() {
		def sdata = data

		controller.params.from = "Chemical Name"
		controller.params.to = "InChIKey"
		controller.params.value = sdata
		controller.params.format = "excel"
		controller.params.max = "10"
		controller.params.offset = "0"
		controller.params.total = "10"
		controller.response.setHeader("Content-disposition", "attachment; filename=test.xls")
		controller.exportTo()

		def response = controller.response

		assertEquals 302, response.status
	}

	@Test
	void testBigBatchConversion() {
		def bdata = data * 5

		controller.params.from = "Chemical Name"
		controller.params.to = ["InChIKey"]
		controller.params.value = bdata


		controller.batchConvert()

		assertEquals 200, controller.response.status

		def modelView = controller.modelAndView
		def results = modelView.model.results

		assertEquals bdata.split("\\r?\\n+")[0..<controller.BATCH_QUERY_MAX_TOKENS].size(), modelView.model.values.size()
		assertEquals 9, results.size()  //first page
	}

	@Test
	void testBigBatchExport() {
		def bdata = data * 5

		controller.params.from = "Chemical Name"
		controller.params.to = ["InChIKey"]
		controller.params.values = bdata
		controller.params.format = "excel"
		controller.response.setHeader("Content-disposition", "attachment; filename=test.xls")
		controller.batchExport()

		def response = controller.response

		assertEquals 302, response.status
	}
}
