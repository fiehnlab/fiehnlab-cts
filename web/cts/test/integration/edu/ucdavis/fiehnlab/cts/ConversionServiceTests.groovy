package edu.ucdavis.fiehnlab.cts

import groovyx.gpars.GParsPool
import org.apache.log4j.Logger
import org.codehaus.groovy.runtime.metaclass.MethodSelectionException
import org.junit.After
import org.junit.Before
import org.junit.Test

class ConversionServiceTests extends GroovyTestCase {
	static final String CHEMICAL_NAME = "Chemical Name"
	static final String INCHI_KEY = "InChIKey"
	static final String INCHI_CODE = "InChI Code"

    ConversionService conversionService
//	def averageCombinedScore

	def c1
	Set<Synonym> syns
	Set<ExternalId> ids
	Logger logger = Logger.getLogger(this.getClass())

	@Before
	public void setUp() {
		c1 = new Compound(inchiKey: 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
				inchiCode: 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
				molWeight: 380.2286)

		syns = new HashSet<Synonym>()
		syns.add(new Synonym(name: 'N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (CAS-like Style)'))
		syns.add(new Synonym(name: 'N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (Traditional)'))
		syns.add(new Synonym(name: 'ethyl N-[5-azanyl-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Systematic)'))
		syns.add(new Synonym(name: 'ethyl N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Preferred)'))
		syns.add(new Synonym(name: 'ethyl N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Allowed)'))
		syns.add(new Synonym(name: 'carbamico', type: 'Synonym'))

		ids = new HashSet<ExternalId>()
		ids.add(new ExternalId(name: 'PubChem CID', url: 'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', value: '150001'))
		ids.add(new ExternalId(name: 'ABI Chem', url: 'http://www.abichem.com', value: 'AC1L43G0'))
		ids.add(new ExternalId(name: 'ChEMBL', url: 'https://www.ebi.ac.uk/chembldb', value: 'CHEMBL9517'))

		c1.synonyms = syns
		c1.extIds = ids

//		conversionService.averageCombinedScore = averageCombinedScore
	}

	@After
	public void tearDown() {
	}

	@Test
	void convertFromIdToId() {
		//expected result is: inchi_key, from value, from name, to value, to name
		def sv = new ScoredValue('AC1L43G0', 0.0)
		def exp = [['fPubChem CID': '150001', 'ABI Chem': [sv]]]

		def res = conversionService.convert('PubChem CID', '150001', 'ABI Chem')

		assertEquals exp[0], res[0]
	}

	@Test
	void convertFromIdToIdCaseInsensitive() {
		//expected result is: inchi_key, from value, from name, to value, to name

        def exp = [['fPubChem CID': '150001', 'ABI Chem': [new ScoredValue('AC1L43G0', 0.0)]]]

		def res = conversionService.convert('pubchem cid', '150001', 'abi Chem')

		assert exp.containsAll(res)
		assert res.containsAll(exp)
	}

	@Test
	void convertFromIdToSynonym() {
		//expected result is: inchi_key, from value, from name, to value, to name
        def exp = [['fPubChem CID': '150001', (CHEMICAL_NAME): [new ScoredValue('N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', 0.0),
                                                                new ScoredValue('carbamico', 0.0),
                                                                new ScoredValue('ethyl N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                                                new ScoredValue('ethyl N-[5-azanyl-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0)]]]

		def res = conversionService.convert('PubChem CID', '150001', CHEMICAL_NAME)

		assertEquals exp[CHEMICAL_NAME][0].sort(), res[CHEMICAL_NAME][0].sort()
	}

	@Test
	void convertFromInchiKeyToSynonym() {
		def exp = ['fChemical Name': 'alanine', (INCHI_KEY): [new ScoredValue('QNAYBMKLOCPYGJ-REOHCLBHSA-N', 0.0), new ScoredValue('QNAYBMKLOCPYGJ-UWTATZPHSA-N', 0.0), new ScoredValue('QNAYBMKLOCPYGJ-UHFFFAOYSA-N', 0.0)]]
		def res = conversionService.convert(INCHI_KEY, 'QNAYBMKLOCPYGJ-REOHCLBHSA-N', INCHI_KEY)[0]

		assert res[INCHI_KEY][0].value.contains(exp[INCHI_KEY][0].value[0])
		assert res[INCHI_KEY][0].value.contains(exp[INCHI_KEY][0].value[1])
		assert res[INCHI_KEY][0].value.contains(exp[INCHI_KEY][0].value[2])
	}

	@Test
	void convertFromSynonymToInchiKey() {
		def exp = ['fChemical Name': 'alanine', (INCHI_KEY): [new ScoredValue('QNAYBMKLOCPYGJ-REOHCLBHSA-N', 0.0), new ScoredValue('QNAYBMKLOCPYGJ-UWTATZPHSA-N', 0.0), new ScoredValue('QNAYBMKLOCPYGJ-UHFFFAOYSA-N', 0.0)]]
		def res = conversionService.convert(CHEMICAL_NAME, 'alanine', INCHI_KEY)[0]

		assert res[INCHI_KEY][0].value.contains(exp[INCHI_KEY][0].value[0])
		assert res[INCHI_KEY][0].value.contains(exp[INCHI_KEY][0].value[1])
		assert res[INCHI_KEY][0].value.contains(exp[INCHI_KEY][0].value[2])
	}

	@Test
	void convertFromSynonymToId() {
        def exp = [['fChemical Name': 'alanine', 'KEGG': [new ScoredValue('C00041', 1.0), new ScoredValue('D00012', 1.0), new ScoredValue('BC1290', 1.0)]]]
		def res = conversionService.convert(CHEMICAL_NAME, 'alanine', 'KEGG')

		assert exp["fChemicalName"].equals(res["fChemicalName"])
		assertEquals exp["KEGG"][0].value.sort(), res["KEGG"][0].value.sort()

		assertEquals exp["KEGG"][0].score.sort().size(), res["KEGG"][0].score.sort().size()

		res["KEGG"][0].score.eachWithIndex {Double score, int i ->
			assertEquals exp['KEGG'][0][i].score, score, 0.01
		}

	}

	@Test
	void convertFromSynonymToSynonym() {
		def exp = [['fChemical Name': 'alanine',
                    (CHEMICAL_NAME) : [new ScoredValue("ALANINE", 1.0),
                                       new ScoredValue("Alanine", 1.0)]
		           ]]

		def res = conversionService.convert(CHEMICAL_NAME, 'alanine', CHEMICAL_NAME)

		assert res["fChemical Name"].equals(exp["fChemical Name"])

		def expNames = exp[CHEMICAL_NAME].sort()
		def resNames = res[CHEMICAL_NAME].sort()

		assertEquals(expNames, resNames)
	}

	@Test(expected = MethodSelectionException.class)
	void emptyConvert() {
		def res = conversionService.convert()
	}

	@Test
	void getFromFields() {
		List<String> exp = ["BioCyc", "CAS", "ChEBI", "ChemSpider", "Human Metabolome Database", "InChIKey", "KEGG", "LMSD", "LipidMAPS", "Chemical Name", "PubChem CID", "ChEMBL", "ABI Chem", "MOLI"]
		exp = exp.sort()

		ArrayList<String> res = conversionService.getFromFields().sort()

		assertEquals exp, res
	}

	@Test
	void getToFields() {
		List<String> exp = ["BioCyc", "CAS", "ChEBI", "ChemSpider", "Human Metabolome Database", "InChI Code", "InChIKey", "KEGG", "LMSD", "LipidMAPS", "Chemical Name", "PubChem CID", "ChEMBL", "ABI Chem", "MOLI"]
		exp = exp.sort()

		ArrayList<String> res = conversionService.getToFields().sort()

		assertEquals exp, res
	}

	//tests for batchConversions
	@Test(expected = MethodSelectionException.class)
	void emptyBatchConvert() {
		def res = conversionService.batchConvert()
	}

	@Test
	void batchConvertSynonymToAll() {
		def from = CHEMICAL_NAME
		def value = ["alanine", "1,3-dimethoxybutane"]
		def to = ["PubChem CID", INCHI_KEY, CHEMICAL_NAME, INCHI_CODE]
		def params = [max: 100, offset: 0, end: -1]

		def exp = []
		exp.addAll(['fChemical Name': "alanine",
                    'PubChem CID'   : [],
                    (INCHI_KEY)     : [new ScoredValue('QNAYBMKLOCPYGJ-REOHCLBHSA-N', 1.0),
                                       new ScoredValue('QNAYBMKLOCPYGJ-UHFFFAOYSA-N', 1.0),
                                       new ScoredValue('QNAYBMKLOCPYGJ-UWTATZPHSA-N', 0.0)],
                    (CHEMICAL_NAME) : [new ScoredValue('ALANINE', 1.0),
                                       new ScoredValue('Alanine', 1.0)],
                    (INCHI_CODE)    : [new ScoredValue('InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1', 1.0),
                                       new ScoredValue('inchiCode', 1.0),
                                       new ScoredValue('InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m1/s1', 1.0)]],
				['fChemical Name': "1,3-dimethoxybutane",
				 'PubChem CID'   : [new ScoredValue('25011', 0.0)],
				 (INCHI_KEY)     : [new ScoredValue('JCRQEORWPZVZJP-UHFFFAOYSA-N', 0.0)],
				 (CHEMICAL_NAME) : [new ScoredValue('1,3-dimethoxybutane', 0.0)],
				 (INCHI_CODE)    : [new ScoredValue('InChI=1S/C6H14O2/c1-6(8-3)4-5-7-2/h6H,4-5H2,1-3H3', 0.5)]])

		def res = conversionService.batchConvert(from, value, to, params)

		assert res.size() == 2
        GParsPool.withPool(4) {
            exp.eachWithIndexParallel { Map e, int idx ->
                assertEquals(e, res.get(idx))
            }
        }
	}

	@Test
	void batchConvertInchikeyToAll() {
		def from = INCHI_KEY
		def value = ["BWJOJAFDVLMQIH-UHFFFAOYSA-N", "DNVPMVJYQWFBIK-UHFFFAOYSA-N"]
		def to = ["PubChem CID", "ChEMBL", CHEMICAL_NAME, INCHI_CODE]
		def params = [max: 100, offset: 0, end: -1]

		def exp = []
		exp.addAll(['fInChIKey'    : 'BWJOJAFDVLMQIH-UHFFFAOYSA-N',
                    'PubChem CID'  : [new ScoredValue('150004', 0.0)],
                    'ChEMBL'       : [new ScoredValue('CHEMBL110407', 0.0)],
                    (CHEMICAL_NAME): [new ScoredValue('carbamoto', 0.0),
                                      new ScoredValue('N-[5-amino-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', 0.0),
                                      new ScoredValue('ethyl N-[5-azanyl-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                      new ScoredValue('ethyl N-[5-amino-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0)],
                    (INCHI_CODE)   : [new ScoredValue('InChI de BWJOJAFDVLMQIH-UHFFFAOYSA-N', 0.0)]],
				['fInChIKey'    : 'DNVPMVJYQWFBIK-UHFFFAOYSA-N',
                 'PubChem CID'  : [new ScoredValue('150003', 0.0)],
                 'ChEMBL'       : [new ScoredValue('CHEMBL9740', 0.0)],
                 (CHEMICAL_NAME): [new ScoredValue('N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', 0.0),
                                   new ScoredValue('ethyl N-[5-azanyl-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                   new ScoredValue('ethyl N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                   new ScoredValue('carbameso', 0.0)],
                 (INCHI_CODE)   : [new ScoredValue('InChI de DNVPMVJYQWFBIK-UHFFFAOYSA-N', 0.0)]])

        List<Map<String, String>> res = conversionService.batchConvert(from, value, to, params)

		assert res.size() == 2
        GParsPool.withPool(2) {
            exp.eachWithIndex { Map e, int idx ->
                assertEquals(e, res.get(idx))
            }
        }
	}

	@Test
	void batchConvertExtIdToAll() {
		def from = "PubChem CID"
		def value = ["150002", "150003", "150005"]
		def to = ["ABI Chem", INCHI_KEY, CHEMICAL_NAME, INCHI_CODE]
		def params = [max: 100, offset: 0, end: -1]

		def exp = []
		exp.addAll(['fPubChem CID' : '150002',
                    'ABI Chem'     : [new ScoredValue('AC1L43G3', 0.0)],
                    (INCHI_KEY)    : [new ScoredValue('QACVMOHIHXGYLO-UHFFFAOYSA-N', 0.0)],
                    (CHEMICAL_NAME): [new ScoredValue('N-[5-amino-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', 0.0),
                                      new ScoredValue('ethyl N-[5-azanyl-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                      new ScoredValue('ethyl N-[5-amino-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                      new ScoredValue('carbamato', 0.0)],
                    (INCHI_CODE)   : [new ScoredValue('InChI=1S/C17H19N5O3/c1-3-25-17(23)22-14-8-12-15(16(18)21-14)20-13(9-19-12)10-5-4-6-11(7-10)24-2/h4-8,19H,3,9H2,1-2H3,(H3,18,21,22,23)', 0.0)]],
				['fPubChem CID' : '150003',
                 'ABI Chem'     : [new ScoredValue('AC1L43G6', 0.0)],
                 (INCHI_KEY)    : [new ScoredValue('DNVPMVJYQWFBIK-UHFFFAOYSA-N', 0.0)],
                 (CHEMICAL_NAME): [new ScoredValue('N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', 0.0),
                                   new ScoredValue('ethyl N-[5-azanyl-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                   new ScoredValue('ethyl N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                   new ScoredValue('carbameso', 0.0)],
                 (INCHI_CODE)   : [new ScoredValue('InChI de DNVPMVJYQWFBIK-UHFFFAOYSA-N', 0.0)]],
				['fPubChem CID' : '150005',
                 'ABI Chem'     : [new ScoredValue('AC1L43GC', 0.0)],
                 (INCHI_KEY)    : [new ScoredValue('OYHTZOLNNSQJLH-UHFFFAOYSA-N', 0.0)],
                 (CHEMICAL_NAME): [new ScoredValue('N-[5-amino-3-(4-aminophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', 0.0),
                                   new ScoredValue('ethyl N-[3-(4-aminophenyl)-5-azanyl-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                   new ScoredValue('ethyl N-[5-amino-3-(4-aminophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', 0.0),
                                   new ScoredValue('carbamistico', 0.0)],
                 (INCHI_CODE)   : [new ScoredValue('InChI=1S/C16H18N6O2/c1-2-24-16(23)22-13-7-11-14(15(18)21-13)20-12(8-19-11)9-3-5-10(17)6-4-9/h3-7,19H,2,8,17H2,1H3,(H3,18,21,22,23)', 0.0)]])

		def res = conversionService.batchConvert(from, value, to, params)

		assert res.size() == 3
        GParsPool.withPool(4) {
            exp.eachWithIndexParallel { Map e, int idx ->
                assertEquals(e, res.get(idx))
            }
        }
	}

	@Test
	void testBatchBadStrings() {
		def from = CHEMICAL_NAME
		def value = ["5'-wrong, stuff", "this should-not exist", "alanine"]
		def to = ["KEGG", INCHI_KEY]
		def params = [max: 100, offset: 0, end: -1]

		def exp = []
		exp.add('fChemical Name': "5'-wrong, stuff",
				(INCHI_KEY): [],
				'KEGG': [])
		exp.add('fChemical Name': "this should-not exist",
				(INCHI_KEY): [],
				'KEGG': [])
		exp.add('fChemical Name': "alanine",
				(INCHI_KEY): [new ScoredValue('QNAYBMKLOCPYGJ-REOHCLBHSA-N', 1.0), new ScoredValue('QNAYBMKLOCPYGJ-UHFFFAOYSA-N', 0.7500708247323061), new ScoredValue('QNAYBMKLOCPYGJ-UWTATZPHSA-N', 0.2324823196550647)],
				'KEGG': [new ScoredValue('C00041', 1.0), new ScoredValue('D00012', 1.0), new ScoredValue('BC1290', 0.7500708247323061),])

		def res = conversionService.batchConvert(from, value, to, params)


		assertFalse res.isEmpty()
		assertEquals exp['fChemical Name'].sort(), res['fChemical Name'].sort()
		assertEquals exp['KEGG'][0], res['KEGG'][0]
		assertEquals exp['KEGG'][1], res['KEGG'][1]
		assertEquals exp['KEGG'][2].value, res['KEGG'][2].value
		assertEquals exp[INCHI_KEY][0].value[0], res[INCHI_KEY][0].value[0]
		assertEquals exp[INCHI_KEY][0].value[1], res[INCHI_KEY][0].value[1]
		assertEquals exp[INCHI_KEY][0].value[2], res[INCHI_KEY][0].value[2]
	}

	@Test
	void testBatchSanitizedStrings() {
		def from = CHEMICAL_NAME
		def value = ["1,1'-Bicyclobutyl"]
		def to = [INCHI_KEY]
		def params = [max: 100, offset: 0, end: -1, prequery: false]

		def exp = []
		exp.add('fChemical Name': "1,1'-Bicyclobutyl",
				(INCHI_KEY): [new ScoredValue('DENCHDMOLAALHN-UHFFFAOYSA-N', 0.0)])

		def res = conversionService.batchConvert(from, value, to, params)

		assertEquals exp, res
	}

	@Test
	void testBromopropanol() {
		def from = CHEMICAL_NAME
		def value = ["bromopropanol"]
		def to = [INCHI_KEY]

		//chemspider finds 1-bromopropanol
		def exp = [['fChemical Name': 'bromopropanol', (INCHI_KEY): [new ScoredValue("JCERKCRUSDOWLT-UHFFFAOYSA-N", 0.0)]]]
//		def exp = [['fChemical Name': 'bromopropanol', (INCHI_KEY): [new ScoredValue("RQFUZUMFPRMVDX-UHFFFAOYSA-N", 0.75)]]]

		def res = conversionService.batchConvert(from, value, to)

		assertEquals exp['fChemical Name'], res['fChemical Name']
		assertEquals exp[INCHI_KEY].value, res[INCHI_KEY].value
	}

	@Test
	void testChemSpiderConversionIssue() {
		def from = "chemspider"
		def value = "582"
		def to = INCHI_KEY

		def exp = [['fChemSpider': '582', (INCHI_KEY): [new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 1.0)]]]

		def res = conversionService.convert(from, value, to)

		assertEquals exp, res
	}
}
