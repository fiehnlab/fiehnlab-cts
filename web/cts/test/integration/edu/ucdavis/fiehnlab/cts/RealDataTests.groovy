package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger
import org.junit.Ignore
import org.junit.Test

/**
 * Created by diego on 11/5/14.
 */
class RealDataTests extends GroovyTestCase {

	Logger logger = Logger.getLogger(this.class.simpleName)
	def conversionService

	@Test
	@Ignore("not fully implemented")
	void testRealData() {
		def from = ConstantHelper.NAMES_CHEMICAL_NAME
		def to = [ConstantHelper.NAMES_INCHI_KEY]
		def values = ['glucose',
		              'phosphoric acid',
		              'leucine',
		              'tryptophan',
		              'citric acid',
		              'serine',
		              'proline',
		              '2-hydroxybutanoic acid',
		              'phenylalanine',
		              'inositol myo-',
					  'glutamic acid',
		              'N-methylalanine',
		              'beta-mannosylglycerate',
		              'iminodiacetic acid',
		              '3-hydroxybutanoic acid',
		              'glyceric acid',
		              '2-ketoisocaproic acid',
		              'asparagine']

		def res = conversionService.batchConvert(from, values, to)

		logger.debug("results: $res")
	}
}
