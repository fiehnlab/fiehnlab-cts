package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test
/**
 * Created by diego on 10/21/2014.
 */
class BatchTests extends GroovyTestCase {
	private static Logger logger = Logger.getLogger(BatchTests.class)

	def conversionService

	@Before
	void setUp() {
	}

// ------- from name ----------------------------------
	@Test
	void 'testCC-name2key'() {
		def exp = [["fChemical Name": "alanine", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0),
													           new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.7500708247323061),
													           new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.2324823196550647)]]]
		def res = conversionService.batchConvert("Chemical Name", ["alanine"], ["InChIKey"])

		logger.debug("result: ${res}\n")
		assertFalse(res.isEmpty())
		assertEquals(exp['fChemical Name'][0], res['fChemical Name'][0])
		assertEquals(exp['InChIKey'][0][0].value, res['InChIKey'][0][0].value)
		assertEquals(exp['InChIKey'][0][1].value, res['InChIKey'][0][1].value)
		assertEquals(exp['InChIKey'][0][2].value, res['InChIKey'][0][2].value)
	}

	@Test
	void 'testCC-namelist2key'() {
		def exp = [["fChemical Name": "alanine", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0),
													           new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.7500708247323061),
													           new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.2324823196550647)]],
					["fChemical Name": "ethanol", "InChIKey": [new ScoredValue("LFQSCWFLJHTTHZ-UHFFFAOYSA-N", 0.75)]]]
		def res = conversionService.batchConvert("Chemical Name", ["alanine", "ethanol"], ["InChIKey"])

		logger.debug("result: ${res}\n")
		assertFalse(res.isEmpty())
		assertEquals(exp['fChemical Name'][0], res['fChemical Name'][0])
		assertEquals(exp['fChemical Name'][1], res['fChemical Name'][1])
		assertEquals(exp['InChIKey'][0][0].value, res['InChIKey'][0][0].value)
		assertEquals(exp['InChIKey'][0][1].value, res['InChIKey'][0][1].value)
		assertEquals(exp['InChIKey'][0][2].value, res['InChIKey'][0][2].value)
		assertEquals(exp['InChIKey'][1].value, res['InChIKey'][1].value)
	}

	@Test
	void 'testCC-name2keylist'() {
		def exp = [["fChemical Name": "alanine", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0),
//		                                                      new ScoredValue("USUKZANDWKHTGZ-UHFFFAOYSA-M", 0.0069)],
													           new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.7500708247323061),
													           new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.2324823196550647)],
		                                         "Human Metabolome Database" : [new ScoredValue("HMDB00161", 1.0), new ScoredValue("HMDB00016", 0.7500708247323061)]]]
//		                                         "Human Metabolome Database" : [new ScoredValue("HMDB00161", 1.0)]]]
		def res = conversionService.batchConvert("Chemical Name", ["alanine"], ["inchikey", "Human Metabolome Database"])

		logger.debug("result: ${res}\n")
		assertFalse(res.isEmpty())
		assertEquals(exp['fChemical Name'], res['fChemical Name'])
		assertEquals(exp['InChIKey'][0][0].value, res['InChIKey'][0][0].value)
		assertEquals(exp['InChIKey'][0][1].value, res['InChIKey'][0][1].value)
		assertEquals(exp['InChIKey'][0][2].value, res['InChIKey'][0][2].value)
		assertEquals(exp['Human Metabolome Database'][0].value, res['Human Metabolome Database'][0].value)
	}

	@Test
	void 'testCC-namelist2keylist'() {
		def exp = [["fChemical Name": "alanine", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0),
													           new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.7500708247323061),
													           new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.2324823196550647)],
		                                         "Human Metabolome Database" : [new ScoredValue("HMDB00161", 1.0), new ScoredValue("HMDB00016", 0.7500708247323061)]],
//		                                         "Human Metabolome Database" : [new ScoredValue("HMDB00161", 1.0)]],
					["fChemical Name": "ethanol", "InChIKey": [new ScoredValue("LFQSCWFLJHTTHZ-UHFFFAOYSA-N", 0.75)],
												 "Human Metabolome Database" : []]]
		def res = conversionService.batchConvert("Chemical Name", ["alanine","ethanol"], ["inchikey", "Human Metabolome Database"])

		logger.debug("result: ${res}\n")
		assertFalse(res.isEmpty())
		assertEquals(exp['fChemical Name'][0], res['fChemical Name'][0])
		assertEquals(exp['fChemical Name'][1], res['fChemical Name'][1])
		assertEquals(exp['InChIKey'][0][0].value, res['InChIKey'][0][0].value)
		assertEquals(exp['InChIKey'][0][1].value, res['InChIKey'][0][1].value)
		assertEquals(exp['InChIKey'][0][2].value, res['InChIKey'][0][2].value)
		assertEquals(exp['InChIKey'][1].value, res['InChIKey'][1].value)
		assertEquals(exp['Human Metabolome Database'][0].value, res['Human Metabolome Database'][0].value)
		assertEquals(exp['Human Metabolome Database'][1].value, res['Human Metabolome Database'][1].value)
	}

	@Test
	void 'testCC-dupenamelist2keylist'() {
		def exp = [["fChemical Name": "ethanol", "InChIKey": [new ScoredValue("LFQSCWFLJHTTHZ-UHFFFAOYSA-N", 0.0)],
					"Human Metabolome Database" : []],
					["fChemical Name": "alanine", "InChIKey": [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0),
													           new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.7500708247323061),
													           new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.2324823196550647)],
												"Human Metabolome Database" : [new ScoredValue("HMDB00161", 1.0), new ScoredValue("HMDB00016", 0.74)]],
//												"Human Metabolome Database" : [new ScoredValue("HMDB00161", 1.0)]],
					["fChemical Name": "ethanol", "InChIKey": [new ScoredValue("LFQSCWFLJHTTHZ-UHFFFAOYSA-N", 0.75)],
												"Human Metabolome Database" : []]]
		def res = conversionService.batchConvert("Chemical Name", ["ethanol", "alanine", "ethanol"], ["inchikey", "Human Metabolome Database"])

		logger.debug("result: ${res}\n")
		assertFalse(res.isEmpty())
		assertEquals(exp['fChemical Name'], res['fChemical Name'])
		assertEquals(exp['InChIKey'][0].value, res['InChIKey'][0].value)
		assertEquals(exp['InChIKey'][1][0].value, res['InChIKey'][1][0].value)
		assertEquals(exp['InChIKey'][1][1].value, res['InChIKey'][1][1].value)
		assertEquals(exp['InChIKey'][1][2].value, res['InChIKey'][1][2].value)
		assertEquals(exp['InChIKey'][2].value, res['InChIKey'][2].value)
		assertEquals(exp['Human Metabolome Database'][0].value, res['Human Metabolome Database'][0].value)
		assertEquals(exp['Human Metabolome Database'][1].value, res['Human Metabolome Database'][1].value)
		assertEquals(exp['Human Metabolome Database'][2].value, res['Human Metabolome Database'][2].value)
	}

	@Test(expected = MissingMethodException.class)
	void testBatchBadParameterTypeForValues() {
		conversionService.batchConvert("right", "this should not be a string", ["right"])
	}

	@Test(expected = MissingMethodException.class)
	void testBatchBadParameterTypeForToNames() {
		conversionService.batchConvert("right", ["right"], "this should not be a string")
	}
}
