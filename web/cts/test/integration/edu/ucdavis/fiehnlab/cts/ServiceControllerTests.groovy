package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test

import java.text.DecimalFormat

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
class ServiceControllerTests extends GroovyTestCase {
	ServiceController serviceController

	/**
	 * 	this is supposed to do the job
	 */
	ConversionService conversionService
	MoleculeConversionService moleculeConversionService
	CompoundService compoundService
	SimilarityService similarityService
	NameCleanupService nameCleanupService

	Logger logger = Logger.getLogger(getClass())

	@Before
	public void setup() {
		serviceController = new ServiceController()
		serviceController.conversionService = conversionService
		serviceController.moleculeConversionService = moleculeConversionService
		serviceController.compoundService = compoundService
		serviceController.similarityService = similarityService
		serviceController.nameCleanupService = nameCleanupService
	}

	@Test
	void testIdCount() {
		def exp = "{\"datasource_count\":0}"

		serviceController.params.inchikey = "HQMLIDZJXVVKCW-REOHCLBHSA-O"
		serviceController.idcount()

		def res = serviceController.response.contentAsString

		assertEquals exp, res
	}

	@Test
	void testConvert() {
		def exp = "[{\"fromIdentifier\":\"PubChem CID\",\"searchTerm\":\"150003\",\"toIdentifier\":\"InChIKey\",\"result\":[\"DNVPMVJYQWFBIK-UHFFFAOYSA-N\"]}]"

		serviceController.params.from = "PubChem CID"
		serviceController.params.to = "InChIKey"
		serviceController.params.value = "150003"
		serviceController.convert()

		def res = serviceController.response.contentAsString

		assert exp.equals(res)
	}

	@Test
	void testConvertDmitry() {
		def exp = "[{\"fromIdentifier\":\"InChIKey\",\"searchTerm\":\"QNAYBMKLOCPYGJ-UHFFFAOYSA-N\",\"toIdentifier\":\"KEGG\",\"result\":[\"BC1290\"]}]"

		serviceController.params.from = "InChIKey"
		serviceController.params.to = "KEGG"
		serviceController.params.value = "QNAYBMKLOCPYGJ-UHFFFAOYSA-N"
//		serviceController.params.scoring = false
		serviceController.convert()

		def res = serviceController.response.contentAsString

		assertEquals exp, res
	}

	@Test
	void testConvertNoResults() {
		def exp = "[{\"fromIdentifier\":\"PubChem CID\",\"searchTerm\":\"199003\",\"toIdentifier\":\"InChIKey\",\"result\":[]}]"

		serviceController.params.from = "PubChem CID"
		serviceController.params.to = "InChIKey"
		serviceController.params.value = "199003"
		serviceController.convert()

		def res = serviceController.response.contentAsString

		assert exp.equals(res)
	}

	@Test
	void testConvertWithoutParams() {
		def exp = "[{\"fromIdentifier\":null,\"searchTerm\":null,\"toIdentifier\":null,\"error\":\"Missing parameters. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/convert/<fromidtype>/<toidtype>/<searchterm>\"}]"
		serviceController.params.from = null
		serviceController.params.to = null
		serviceController.params.value = null
		serviceController.convert()

		def res = serviceController.response.contentAsString

		assert (exp).equals(res)
	}

	@Test
	void testCompound() {
		def exp = "{\"inchikey\":\"RYXHPKXJBBAPCB-UHFFFAOYSA-N\",\"inchicode\":\"InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)\",\"molweight\":380.2286,\"exactmass\":null,\"formula\":\"C16H15Cl2N5O2\",\"synonyms\":[{\"type\":\"IUPAC Name (Allowed)\",\"name\":\"ethyl N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate\",\"score\":0},{\"type\":\"IUPAC Name (Traditional)\",\"name\":\"N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester\",\"score\":0},{\"type\":\"IUPAC Name (CAS-like Style)\",\"name\":\"N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester\",\"score\":0},{\"type\":\"IUPAC Name (Systematic)\",\"name\":\"ethyl N-[5-azanyl-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate\",\"score\":0},{\"type\":\"Synonym\",\"name\":\"carbamico\",\"score\":0},{\"type\":\"IUPAC Name (Preferred)\",\"name\":\"ethyl N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate\",\"score\":0}],\"externalIds\":[{\"name\":\"ABI Chem\",\"value\":\"AC1L43G0\",\"url\":\"http://www.abichem.com\"},{\"name\":\"ChEMBL\",\"value\":\"CHEMBL9517\",\"url\":\"https://www.ebi.ac.uk/chembldb\"},{\"name\":\"PubChem CID\",\"value\":\"150001\",\"url\":\"https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi\"}],\"dataSourcesCount\":9,\"referencesCount\":9,\"pubmedHits\":0}"

		serviceController.params.inchiKey = "RYXHPKXJBBAPCB-UHFFFAOYSA-N"
		serviceController.compound()

		def res = serviceController.response.contentAsString

		assert serviceController.response.json.inchikey.equals('RYXHPKXJBBAPCB-UHFFFAOYSA-N')
		assert serviceController.params.inchiKey.equals('RYXHPKXJBBAPCB-UHFFFAOYSA-N')
		assertEquals exp, res
	}

	@Test
	void testCompoundNoParams() {
		def exp = "[\"You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>\"]"

		serviceController.params.inchiKey = null
		serviceController.compound()

		def res = serviceController.response.contentAsString

		assert serviceController.params.inchiKey.equals(null)
		assertEquals exp, res
	}

	@Test
	void testCompoundBadParams() {
		def exp = "[\"You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>\"]"

		serviceController.params.inchiKey = "a bad inchi"
		serviceController.compound()

		def res = serviceController.response.contentAsString

		assert serviceController.params.inchiKey.equals('a bad inchi')
		assertEquals exp, res
	}

	@Test
	void testCompoundNoResults() {
		def exp = "[\"Sorry, we couldn't find any matching results\"]"

		serviceController.params.inchiKey = 'QVBOTMGSBYXVJQ-LLVKDONJSA-O'
		serviceController.compound()

		def res = serviceController.response.contentAsString
		assert serviceController.params.inchiKey.equals('QVBOTMGSBYXVJQ-LLVKDONJSA-O')
		assert exp.equals(res)
	}

	@Test
	void testSourceName1() {
		def exp = "ABI Chem"

		serviceController.params.pcid = "708"
		serviceController.sourceName()

		def res = serviceController.response.text
//		logger.debug("\nres: ${serviceController.response.text}\n")

		assertEquals 200, serviceController.response.status
		assertEquals exp, res
	}

	@Test
	void testSourceName2() {
		def exp = "Human Metabolome Database"

		serviceController.params.pcid = "811"
		serviceController.sourceName()

		assertEquals 200, serviceController.response.status
		def res = serviceController.response.text
//		logger.debug("\nres: ${serviceController.response.text}\n")

		assertEquals exp, res
	}

	@Test
	void testSourceNameBadParams() {
		def exp = ""

		serviceController.params.pcid = "badId"
		serviceController.sourceName()

		def res = serviceController.response.contentAsString
//		logger.debug("\nres: ${serviceController.response.contentAsString}\n")

		assertEquals 400, serviceController.response.status
		assertEquals exp, res
	}

	@Test
	void testSourceNameNoParams() {
		def exp = ""

		serviceController.params.pcid = ""
		serviceController.sourceName()

		def res = serviceController.response.contentAsString
//		logger.debug("\nres: ${serviceController.response.contentAsString}\n")

		assertEquals 400, serviceController.response.status
		assertEquals exp, res
	}

	@Test
	void testPubchemDepositor() {
		def exp = "Human Metabolome Database (HMDB)"

		serviceController.params.type = "substance"
		serviceController.params.sid = "126531493"
		serviceController.pubchemDepositor()

		def res = serviceController.response.text
		logger.debug("\nres: ${serviceController.response.text}\n")

		assertEquals exp, res
	}

	@Test
	void testPubchemDepositorInexistentId() {
		def exp = "Human Metabolome Database (HMDB)"

		serviceController.params.type = "substance"
		serviceController.params.sid = "126531493"
		serviceController.params.dsid = "811"
		serviceController.pubchemDepositor()

		def res = serviceController.response.text
		logger.debug("\nres: ${serviceController.response.text}\n")

		assertEquals exp, res
	}

	@Test
	void testPubchemDepositorBadParams() {
		def exp = ""

		// good type -- bad sid
		serviceController.params.type = "substance"
		serviceController.params.sid = "dfasf"
		serviceController.pubchemDepositor()

		def res = serviceController.response.contentAsString
//		logger.debug("\nres: ${serviceController.response.contentAsString}\n")

		assertEquals exp, res

		// bad type -- good sid
		serviceController.params.type = "34t652"
		serviceController.params.sid = "708"
		serviceController.pubchemDepositor()

		res = serviceController.response.contentAsString
//		logger.debug("\nres: ${serviceController.response.contentAsString}\n")

		assertEquals exp, res

		// bad type -- bad sid
		serviceController.params.type = "31245"
		serviceController.params.sid = "dfasf"
		serviceController.pubchemDepositor()

		res = serviceController.response.contentAsString
//		logger.debug("\nres: ${serviceController.response.contentAsString}\n")

		assertEquals exp, res

		// empty params
		serviceController.params.type = ""
		serviceController.params.sid = ""
		serviceController.pubchemDepositor()

		res = serviceController.response.contentAsString
//		logger.debug("\nres: ${serviceController.response.contentAsString}\n")

		assertEquals exp, res
	}

	@Test
	void testBioCount() {
		def exp = "{\"KEGG\":2,\"Human Metabolome Database\":1,\"total\":3}"

		serviceController.params.inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
		serviceController.bioCount()

		def res = serviceController.response.contentAsString

		assertNotNull(res)
		assertEquals exp, res
	}

	@Test
	void testBioCountBadInchi() {
		def exp = "[\"You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/countBiological/<inchikey>\"]"

		serviceController.params.inchikey = "a bad inchi"
		serviceController.bioCount()

		def res = serviceController.response.contentAsString

		assertNotNull(res)
		assertEquals(exp, res)
	}

	@Test
	void testBioCountNoResult() {
		def exp = "{\"total\":0}"

		serviceController.params.inchikey = "ZZZZZZZZZZZZZZ-REOHCLBHSA-X"
		serviceController.bioCount()

		def res = serviceController.response.contentAsString

		assertNotNull(res)
		assertEquals(exp, res)
	}

	@Test
	void testBioCountNoParams() {
		def exp = "[\"You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/countBiological/<inchikey>\"]"

		serviceController.params.inchikey = null
		serviceController.bioCount()

		def res = serviceController.response.contentAsString

		assertNotNull(res)
		assertEquals(exp, res)
	}

	@Test
	void testSynonymsForInchikey() {
		serviceController.params.inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
		serviceController.synonymsForInchikey()

		def res = serviceController.response.contentAsString

		assertNotNull(res)
		assert res.contains("ALANINE")
		// "Alanine"
	}

	@Test
	void testConvertBioWithScoring() {
		def exp = [fromIdentifier: "chemical name", searchTerm: "alanine", toIdentifier: "inchikey",
		           result        : [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0),
									new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 1.0),
									new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.0)]]

		serviceController.params.from = "chemical name"
		serviceController.params.to = "inchikey"
		serviceController.params.value = "alanine"
		serviceController.params.scoring = "biological"
		serviceController.convert()

		def res = serviceController.response.json[0]

		def comparator = {a,b -> a.score <=> b.score}

		assertNotNull(res)
		assertEquals exp.fromIdentifier, res.fromIdentifier
		assertEquals exp.searchTerm, res.searchTerm
		assertEquals exp.toIdentifier, res.toIdentifier
		assertEquals exp.result[0..2].sort(comparator).value, res.result[0..2].sort(comparator).value

		for(int i=0; i < exp.result.size(); i++) {
			Double dExp = new DecimalFormat("0.00##").format(exp.result.get(i).score).toDouble()
			Double dRes = new DecimalFormat("0.00##").format(res.result.get(i).score).toDouble()

			assertEquals dExp, dRes, 0.01
		}
	}

	@Test
	void testConvertCountWithScoring() {
		def exp = [fromIdentifier: "chemical name", searchTerm: "alanine", toIdentifier: "inchikey",
		           result        : [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0),
									new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 1.0),
									new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.0)]]

		serviceController.params.from = "chemical name"
		serviceController.params.to = "inchikey"
		serviceController.params.value = "alanine"
		serviceController.params.scoring = "popularity"
		serviceController.convert()

		def res = serviceController.response.json[0]

		def comparator = {a,b -> a.score <=> b.score }

		assertNotNull(res)
		assertEquals exp.fromIdentifier, res.fromIdentifier
		assertEquals exp.searchTerm, res.searchTerm
		assertEquals exp.toIdentifier, res.toIdentifier
		assertEquals exp.result.sort(comparator).reverse().value[0..2], res.result.sort(comparator).reverse().value[0..2]
	}

	@Test
	void testConvertBioWithoutScoring() {
		def exp = [fromIdentifier: "chemical name", searchTerm: "alanine", toIdentifier: "inchikey", result: ["QNAYBMKLOCPYGJ-REOHCLBHSA-N", "QNAYBMKLOCPYGJ-UWTATZPHSA-N", "QNAYBMKLOCPYGJ-UHFFFAOYSA-N"]]

		serviceController.params.from = "chemical name"
		serviceController.params.to = "inchikey"
		serviceController.params.value = "alanine"
		serviceController.convert()

		def res = JSON.parse(serviceController.response.contentAsString)[0]

		def comparator = {a,b -> a <=> b }

		assertNotNull(res)
		assertEquals exp.fromIdentifier, res.fromIdentifier
		assertEquals exp.searchTerm, res.searchTerm
		assertEquals exp.result[0..2].sort(comparator), res.result[0..2].sort(comparator)
		assertEquals exp.toIdentifier, res.toIdentifier

	}

	@Test
	void testConvertCountWithoutScoring() {
		def exp = [fromIdentifier: "chemical name", searchTerm: "alanine", toIdentifier: "inchikey", result: ["QNAYBMKLOCPYGJ-REOHCLBHSA-N", "QNAYBMKLOCPYGJ-UWTATZPHSA-N", "QNAYBMKLOCPYGJ-UHFFFAOYSA-N"]]

		serviceController.params.from = "chemical name"
		serviceController.params.to = "inchikey"
		serviceController.params.value = "alanine"
		serviceController.convert()

		def res = JSON.parse(serviceController.response.contentAsString)[0]

		def comparator = {a,b -> a <=> b}

		assertNotNull(res)
		assertEquals exp.fromIdentifier, res.fromIdentifier
		assertEquals exp.searchTerm, res.searchTerm
		assertEquals exp.result[0..2].sort(comparator), res.result[0..2].sort(comparator)
		assertEquals exp.toIdentifier, res.toIdentifier
	}

	@Test
	void testMolToInchi() {
		def exp = "{\"inchikey\":\"QNAYBMKLOCPYGJ-REOHCLBHSA-N\",\"inchicode\":\"InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1\"}"
		Reader file = new FileReader(new File("test/resources/singleSubstance.sdf"))

		serviceController.params.mol = file.text
		serviceController.molToInchi()

		def res = serviceController.response.contentAsString

		assertNotNull res
		assertEquals exp, res
	}

	@Test
	void testBadMolToInchi() {
		def exp = "{\"error\":\"Invalid molecule definition. The only format supported is MDL.\"}"

		Reader file = new FileReader(new File("test/resources/badmol.dat"))
		String mol = file.text
		file.close()

		serviceController.params.mol = mol
		serviceController.molToInchi()

		def res = serviceController.response.contentAsString

		assertNotNull res
		assertEquals 400, serviceController.response.status
		assertEquals exp, res
	}

	@Test
	void testInchiToMol() {
		def inchicode = "InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1"
		def exp = " 13 12  0  0  0  0  0  0  0  0999 V2000\n" +
				"    1.2990   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.5981   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.5623    1.1491    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -2.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.1962   -0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    1.6339    1.1491    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.0000    0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.3349   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.2632   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.0492    2.5586    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.0395    0.8886    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    6.4952   -0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"  2  1  1  0  0  0  0 \n" +
				"  3  2  1  0  0  0  0 \n" +
				"  4  2  1  0  0  0  0 \n" +
				"  5  3  2  0  0  0  0 \n" +
				"  6  3  1  0  0  0  0 \n" +
				"  2  7  1  1  0  0  0 \n" +
				"  1  8  1  0  0  0  0 \n" +
				"  1  9  1  0  0  0  0 \n" +
				"  1 10  1  0  0  0  0 \n" +
				"  4 11  1  0  0  0  0 \n" +
				"  4 12  1  0  0  0  0 \n" +
				"  6 13  1  0  0  0  0 \n" +
				"M  END\n"

		serviceController.params.inchicode = inchicode
		serviceController.inchiToMol()

		def res = serviceController.response.contentAsString.replaceAll("\\\\r?\\\\n", "\n")

		assertNotNull res
		assert res.contains(exp)
		assert res.contains("message\":null")
	}

	@Test
	void testBadInchiToMol() {
		def inchicode = "bad inchi code after a party"
		def exp = "{\"error\":\"Invalid InChI code. I recieved a malformed InChI code: 'bad inchi code after a party'.\"}"

		serviceController.params.inchicode = inchicode
		serviceController.inchiToMol()

		def res = serviceController.response.contentAsString

		assertNotNull res
		assertEquals 400, serviceController.response.status
		assertEquals exp, res
	}

	@Test
	void testInchiKeyToMol() {
		def inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
		def exp = " 13 12  0  0  0  0  0  0  0  0999 V2000\n" +
				"    1.2990   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.5981   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.5623    1.1491    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.8971   -2.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.1962   -0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    1.6339    1.1491    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.0000    0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    0.3349   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    2.2632   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    3.0492    2.5586    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    5.0395    0.8886    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"    6.4952   -0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
				"  2  1  1  0  0  0  0 \n" +
				"  3  2  1  0  0  0  0 \n" +
				"  4  2  1  0  0  0  0 \n" +
				"  5  3  2  0  0  0  0 \n" +
				"  6  3  1  0  0  0  0 \n" +
				"  2  7  1  1  0  0  0 \n" +
				"  1  8  1  0  0  0  0 \n" +
				"  1  9  1  0  0  0  0 \n" +
				"  1 10  1  0  0  0  0 \n" +
				"  4 11  1  0  0  0  0 \n" +
				"  4 12  1  0  0  0  0 \n" +
				"  6 13  1  0  0  0  0 \n" +
				"M  END\n"

		serviceController.params.inchikey = inchikey
		serviceController.inchiKeyToMol()

		def res = serviceController.response.contentAsString.replaceAll("\\\\r?\\\\n", "\n")

		assertNotNull res
		assert res.contains(exp)
		assert res.contains("message\":null")
	}

	@Test
	void testBadInchiKeyToMol() {
		def inchikey = "inchikey i typed while drunk"
		def exp = "{\"error\":\"Invalid InChIKey. I recieved a malformed InChIKey 'inchikey i typed while drunk'.\"}"

		serviceController.params.inchikey = inchikey
		serviceController.inchiKeyToMol()

		def res = serviceController.response.contentAsString

		assertNotNull res
		assertEquals 400, serviceController.response.status
		assertEquals exp, res
	}

	@Test
	void testInchiCodeToInchiKey() {
		def inchiCode = "InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1"
		def exp = """{"inchicode":"InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1","inchikey":"QNAYBMKLOCPYGJ-REOHCLBHSA-N"}"""

		serviceController.params.inchicode = inchiCode
		serviceController.inchiCodeToInchiKey()

		def res = serviceController.response.contentAsString

		assertNotNull res
		assertEquals 200, serviceController.response.status
		assertEquals exp, res
	}

	@Test
	void testInchiToMolBug() {
		def inchicode = 'InChI=1S/C12H22O11/c13-1-4-6(16)8(18)9(19)11(21-4)23-12(3-15)10(20)7(17)5(2-14)22-12/h4-11,13-20H,1-3H2/t4-,5-,6-,7-,8+,9-,10+,11-,12+/m1/s1'
		def exp = " 45 46  0  0  0  0  0  0  0  0999 V2000\n" +
			"    6.3807    3.8535    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -1.4961    2.4812    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    2.2386   -0.7398    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    5.2657    2.8502    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -1.1840    1.0140    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    6.4474    1.9263    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -1.1840   -0.4860    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    6.2381    0.4410    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    4.8472   -0.1205    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    0.2430   -0.9490    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    3.6655    0.8034    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    1.1240    0.2640    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    7.8071    3.3896    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -2.9227    2.9445    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    3.6653   -0.2764    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    7.8738    1.4623    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -2.6758   -0.3294    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    6.5496   -1.0263    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    3.7322   -1.1238    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -0.3666   -2.3195    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    3.8747    2.2887    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    0.2430    1.4780    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    2.2390    1.2673    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    4.4712    4.1225    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -2.6758    0.8571    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    7.1520    3.2505    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -1.4955   -1.9533    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    7.7373    0.4929    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    5.6417   -1.3928    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    1.5422   -1.6987    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    2.9608   -0.5208    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    7.0853    5.1777    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    5.1990    4.7774    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    0.0030    2.5337    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -1.2875    3.9666    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    1.0566   -1.6632    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    2.9427   -2.0643    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    8.9222    4.3929    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -3.2348    4.4117    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    4.7799   -1.2801    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    8.9888    2.4657    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -3.5573   -1.5431    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    7.9760   -1.4903    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"    4.0436   -2.5912    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"   -1.8584   -2.4768    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
			"  4  1  1  0  0  0  0 \n" +
			"  5  2  1  0  0  0  0 \n" +
			"  6  4  1  0  0  0  0 \n" +
			"  7  5  1  0  0  0  0 \n" +
			"  8  6  1  0  0  0  0 \n" +
			"  9  8  1  0  0  0  0 \n" +
			" 10  7  1  0  0  0  0 \n" +
			" 11  9  1  0  0  0  0 \n" +
			" 12  3  1  0  0  0  0 \n" +
			" 12 10  1  0  0  0  0 \n" +
			" 13  1  1  0  0  0  0 \n" +
			" 14  2  1  0  0  0  0 \n" +
			" 15  3  1  0  0  0  0 \n" +
			" 16  6  1  0  0  0  0 \n" +
			" 17  7  1  0  0  0  0 \n" +
			" 18  8  1  0  0  0  0 \n" +
			" 19  9  1  0  0  0  0 \n" +
			" 20 10  1  0  0  0  0 \n" +
			" 21  4  1  0  0  0  0 \n" +
			" 21 11  1  0  0  0  0 \n" +
			" 22  5  1  0  0  0  0 \n" +
			" 22 12  1  0  0  0  0 \n" +
			" 23 11  1  0  0  0  0 \n" +
			" 12 23  1  1  0  0  0 \n" +
			"  4 24  1  1  0  0  0 \n" +
			"  5 25  1  6  0  0  0 \n" +
			"  6 26  1  6  0  0  0 \n" +
			"  7 27  1  1  0  0  0 \n" +
			"  8 28  1  1  0  0  0 \n" +
			"  9 29  1  6  0  0  0 \n" +
			" 10 30  1  6  0  0  0 \n" +
			" 11 31  1  6  0  0  0 \n" +
			"  1 32  1  0  0  0  0 \n" +
			"  1 33  1  0  0  0  0 \n" +
			"  2 34  1  0  0  0  0 \n" +
			"  2 35  1  0  0  0  0 \n" +
			"  3 36  1  0  0  0  0 \n" +
			"  3 37  1  0  0  0  0 \n" +
			" 13 38  1  0  0  0  0 \n" +
			" 14 39  1  0  0  0  0 \n" +
			" 15 40  1  0  0  0  0 \n" +
			" 16 41  1  0  0  0  0 \n" +
			" 17 42  1  0  0  0  0 \n" +
			" 18 43  1  0  0  0  0 \n" +
			" 19 44  1  0  0  0  0 \n" +
			" 20 45  1  0  0  0  0 \n" +
			"M  END\n"

		serviceController.params.inchicode = inchicode
		serviceController.inchiToMol()

		def res = serviceController.response.contentAsString.replaceAll("\\\\r?\\\\n","\n")

		assertNotNull res
		assertEquals 200, serviceController.response.status
		assert res.contains(exp)
	}

	@Test
	public void testExpandGoodFormula() {
		def formula = [:]

		formula["H"] = "H"
		formula["N2"] = "NN"
		formula["CO2"] = "COO"
		formula["NaCl"] = "ClNa"
		formula["H2O"] = "HHO"
		formula["OH2"] = "HHO"
		formula["C6H12"] = "CCCCCCHHHHHHHHHHHH"
		formula["C8H10N4O2"] = "CCCCCCCCHHHHHHHHHHNNNNOO"
		formula["C8H10O2N4"] = "CCCCCCCCHHHHHHHHHHNNNNOO"
		formula["CH3CH2CH2CH3"] = "CCCCHHHHHHHHHH"

		formula.entrySet().each { Map.Entry<String, String> entry ->
			serviceController.response.reset()
			serviceController.params.formula = entry.key
			serviceController.expandFormula()

			def res = serviceController.response.json

			assertEquals 200, serviceController.response.status
			log.info("${entry.key} expanded to ${res.result}")
			assertEquals(entry.value, res.result)
		}

	}

	@Test
	public void testExpandBadFormula() {
		def formula = [:]

		formula[""] = "It would be nice to create something out of nothing. Please provide a valid molecular formula."

		formula.entrySet().each { Map.Entry<String, String> entry ->
			serviceController.params.formula = entry.key
			serviceController.expandFormula()

			def res = serviceController.response.json

			assertEquals 400, serviceController.response.status
			assertEquals(entry.value, res.error)
		}
	}

	@Test
	public void testHTMLEncodedTaggedNames() {
		def name = "&lt;i&gt;N&lt;/i&gt;-acetylmuramoyl-L-alanyl-D-isoglutaminyl-N-(&beta;-D-asparatyl)-L-lysyl-D-alanyl-D-alanine-diphosphoundecaprenyl-&lt;i&gt;N&lt;/i&gt;-acetylglucosamine"
		def clean = "N-acetylmuramoyl-L-alanyl-D-isoglutaminyl-N-(beta-D-asparatyl)-L-lysyl-D-alanyl-D-alanine-diphosphoundecaprenyl-N-acetylglucosamine"

		def exp = "[{\"fromIdentifier\":\"Chemical Name\",\"searchTerm\":\"$clean\",\"toIdentifier\":\"InChIKey\",\"result\":[]}]"

		serviceController.params.from = "Chemical Name"
		serviceController.params.to = "InChIKey"
		serviceController.params.value = name

		serviceController.convert()
		logger.debug "request: ${serviceController.response.text}"
		def res = serviceController.response.contentAsString

		assertNotNull res
		assertEquals exp, res
	}

	@Test
	public void testHTMLTaggedNames() {
		def name = "<i>N</i>-acetylmuramoyl-L-alanyl-D-isoglutaminyl-N-(&beta;-D-asparatyl)-L-lysyl-D-alanyl-D-alanine-diphosphoundecaprenyl-<i>N</i>-acetylglucosamine"
		def clean = "N-acetylmuramoyl-L-alanyl-D-isoglutaminyl-N-(beta-D-asparatyl)-L-lysyl-D-alanyl-D-alanine-diphosphoundecaprenyl-N-acetylglucosamine"

		def exp = "[{\"fromIdentifier\":\"Chemical Name\",\"searchTerm\":\"$clean\",\"toIdentifier\":\"InChIKey\",\"result\":[]}]"

		serviceController.params.from = "Chemical Name"
		serviceController.params.to = "InChIKey"
		serviceController.params.value = name

		serviceController.convert()
		logger.debug "request: ${serviceController.response.text}"
		def res = serviceController.response.contentAsString

		assertNotNull res
		assertEquals exp, res
	}
}
