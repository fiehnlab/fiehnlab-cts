package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test

class ConversionControllerTests extends GroovyTestCase {
	private static Logger logger = Logger.getLogger(ConversionControllerTests.class)
	SearchService searchService
	ConversionService conversionService
	ConversionController conversionController

	@Before
	void setup() {
		conversionController = new ConversionController()
		conversionService.searchService = searchService
		conversionController.conversionService = conversionService
	}

	@Test
	void testIndex() {
		conversionController.index()

		assert conversionController.modelAndView.model.fromNames?.size() == 14
		assert conversionController.modelAndView.model.toNames?.size() == 15
		assert "/conversion/simpleConversion" == conversionController.modelAndView.getViewName()
	}

	@Test
	void testBatch() {
		conversionController.batch()

		assert conversionController.modelAndView.model.fromNames?.size() == 14
		assert conversionController.modelAndView.model.toNames?.size() == 15
		assert "/conversion/batchConversion" == conversionController.modelAndView.getViewName()
	}

	@Test
	void testConvert() {
		conversionController.params.from = "PubChem CID"
		conversionController.params.to = "InChIKey"
		conversionController.params.values = "150003"

		conversionController.convert()
		def expList = [['fPubChem CID': '150003', 'InChIKey': [new ScoredValue('DNVPMVJYQWFBIK-UHFFFAOYSA-N', 0.0)]]]

		def modelAndView = conversionController.modelAndView
		assert "PubChem CID" == modelAndView.model.from
		assert "InChIKey" == modelAndView.model.to
		assert "150003" == modelAndView.model.values
		assert modelAndView.model.results.containsAll(expList)
		assert expList.containsAll(modelAndView.model.results)
		assert "/conversion/simpleConversion" == modelAndView.viewName
	}

	@Test
	void testConvertNoResults() {
		def exp = [['fABI Chem': 'inexistent value', 'LMSD': []]]
		conversionController.params.from = "ABI Chem"
		conversionController.params.to = "lMsD"
		conversionController.params.values = "inexistent value"

		conversionController.convert()

		def mav = conversionController.modelAndView

		assertEquals exp, mav.model.results
		assertEquals mav.viewName, "/conversion/simpleConversion"
	}

	@Test
	void testBatchConvert() {
		conversionController.params.from = "PubChem CID"
		conversionController.params.to = ["InChIKey"]
		conversionController.params.values = "150002\n150003\n150004"

		conversionController.batchConvert()

		def modelAndView = conversionController.modelAndView
		def exp = [:]
		exp.values = ["150002","150003","150004"]
		def expList = [['fPubChem CID': '150002', InChIKey: [new ScoredValue('QACVMOHIHXGYLO-UHFFFAOYSA-N', 0.0)]],
					   ['fPubChem CID': '150003', InChIKey: [new ScoredValue('DNVPMVJYQWFBIK-UHFFFAOYSA-N', 0.0)]],
					   ['fPubChem CID': '150004', InChIKey: [new ScoredValue('BWJOJAFDVLMQIH-UHFFFAOYSA-N', 0.0)]]]

		assert "PubChem CID" == modelAndView.model.from
		assert ["InChIKey"] == modelAndView.model.to
		assert exp.values.equals(modelAndView.model.values)
		assert expList.containsAll(modelAndView.model.results)
		assert modelAndView.model.results.containsAll(expList)
		assert "/conversion/batchConversion" == modelAndView.viewName
	}

	@Test
	void testBatchConvertWithEmptyLines() {
		conversionController.params.from = "PubChem CID"
		conversionController.params.to = ["InChIKey"]
		conversionController.params.values = "150002\n\t150003\t\n\n150004"

		conversionController.batchConvert()

		def modelAndView = conversionController.modelAndView
		def exp = [:]
		exp.values = ["150002","150003","Missing input","150004"]
		def expList = [['fPubChem CID': '150002', InChIKey: [new ScoredValue('QACVMOHIHXGYLO-UHFFFAOYSA-N', 0.0)]],
					   ['fPubChem CID': '150003', InChIKey: [new ScoredValue('DNVPMVJYQWFBIK-UHFFFAOYSA-N', 0.0)]],
					   ['fPubChem CID': 'Missing input', InChIKey: []],
					   ['fPubChem CID': '150004', InChIKey: [new ScoredValue('BWJOJAFDVLMQIH-UHFFFAOYSA-N', 0.0)]]]

		assert "PubChem CID" == modelAndView.model.from
		assert ["InChIKey"] == modelAndView.model.to
		assert exp.values.equals(modelAndView.model.values)
		assert expList.containsAll(modelAndView.model.results)
		assert modelAndView.model.results.containsAll(expList)
		assert "/conversion/batchConversion" == modelAndView.viewName
	}

	@Test
	void testBatchConvertWithInvisibleChars() {
		conversionController.params.from = "PubChem CID"
		conversionController.params.to = ["InChIKey"]
		conversionController.params.values = "150002\n  150003\n\t  \t\t\n150004"

		conversionController.batchConvert()

		def modelAndView = conversionController.modelAndView
		def exp = [:]
		exp.values = ["150002","150003","Missing input","150004"]
		def expList = [['fPubChem CID': '150002', InChIKey: [new ScoredValue('QACVMOHIHXGYLO-UHFFFAOYSA-N', 0.0)]],
					   ['fPubChem CID': '150003', InChIKey: [new ScoredValue('DNVPMVJYQWFBIK-UHFFFAOYSA-N', 0.0)]],
					   ['fPubChem CID': 'Missing input', InChIKey: []],
					   ['fPubChem CID': '150004', InChIKey: [new ScoredValue('BWJOJAFDVLMQIH-UHFFFAOYSA-N', 0.0)]]]

		assert "PubChem CID" == modelAndView.model.from
		assert ["InChIKey"] == modelAndView.model.to
		assert exp.values.equals(modelAndView.model.values)
		assert expList.containsAll(modelAndView.model.results)
		assert modelAndView.model.results.containsAll(expList)
		assert "/conversion/batchConversion" == modelAndView.viewName
	}

	@Test
	void testConvertAlanine() {
		conversionController.params.from = "chemical name"
		conversionController.params.to = "InChIKey"
		conversionController.params.values = "alanine"

		conversionController.convert()
		def expList = [['fChemical Name': 'alanine', 'InChIKey': [new ScoredValue('QNAYBMKLOCPYGJ-REOHCLBHSA-N', 1.0),
		                                                          new ScoredValue('QNAYBMKLOCPYGJ-UHFFFAOYSA-N', 0.746),
		                                                          new ScoredValue('QNAYBMKLOCPYGJ-UWTATZPHSA-N', 0.2409)]]]

		def results = conversionController.modelAndView.model.results
		def model = conversionController.modelAndView.model
		assert "chemical name" == model.from
		assert "InChIKey" == model.to
		assert "alanine" == model.values

		assertEquals expList[0]["fChemical Name"], results[0]["fChemical Name"]

		def comparator = {a, b -> a.score <=> b.score}
		List<ScoredValue> rinchis = results[0].InChIKey.sort(comparator)
		List<ScoredValue> einchis = expList[0].InChIKey.sort(comparator)

		assert rinchis.value.contains(einchis.value[0])
		assert rinchis.value.contains(einchis.value[1])
		assert rinchis.value.contains(einchis.value[2])

		assert "/conversion/simpleConversion" == conversionController.modelAndView.viewName
	}


}
