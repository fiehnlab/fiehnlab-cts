package edu.ucdavis.fiehnlab.cts

import grails.plugin.spock.IntegrationSpec
import org.apache.log4j.Logger
import spock.lang.Ignore

/**
 * Created by Diego on 10/12/2015.
 */
@Ignore
public class UpdateJobSpec extends IntegrationSpec {
	private static Logger logger = Logger.getLogger(UpdateJobSpec.class)

	def grailsApplication
	UpdateJob updateJob
	def wascalled = false

	void setup() {
		updateJob = new UpdateJob()
	}

	@Ignore
	def "Execute"() {
		when:
		updateJob.execute(grailsApplication.mainContext)

		then:
		wascalled
	}

	@Ignore
	def "GetConcurrent"() {
		when:
		def res = updateJob.concurrent
		logger.debug "concurrent: $res"

		then:
		true
	}

	@Ignore
	def "GetGroup"() {
		when:
		def group = updateJob.group
		logger.debug "Group: $group"

		then:
		group == "update"
	}

	@Ignore
	def "GetTriggers"() {
		when:
		def triggers = updateJob.triggers
		logger.debug "Triggers: $triggers"

		then:
		triggers.size() == 1

	}
}
