package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */

class UpdateControllerTests extends GroovyTestCase {
	UpdateController controller
	UpdateService updateService
	Logger logger = Logger.getLogger(this.class)

	@Before
	void setup() {
		controller = new UpdateController()
		controller.updateService = updateService
		Compound.findByInchiKey("SOTOZGCOYKZRGH-UHFFFAOYSA-N")?.delete(flush:true)
	}

	@After
	void teardown() {
//		Compound.findByInchiKey("SOTOZGCOYKZRGH-UHFFFAOYSA-N")?.delete(flush:true)
	}

	@Test
	@Ignore("Not fully implemented")
    void testRegisterInchi() {
		def inchi = "InChI=1S/C23H17ClN4O4/c24-19-12-11-17(14-21(19)28(31)32)15-25-27-23(30)20(13-16-7-3-1-4-8-16)26-22(29)18-9-5-2-6-10-18/h1-15H,(H,26,29)(H,27,30)"
		def inchikey = "SOTOZGCOYKZRGH-UHFFFAOYSA-N"
		def name = "N-[(Z)-3-[(2E)-2-[(4-chloro-3-nitrophenyl)methylidene]hydrazinyl]-3-oxo-1-phenylprop-1-en-2-yl]benzamide"

		String jsonhit = """{"inchikey":"$inchikey", "searchTerm":"$name"}"""

		controller.request.json = jsonhit
		controller.registerInchi()

		def status = controller.response.status
		def res = controller.response.contentAsString

		assertEquals 204, controller.response.status
		assertEquals "Compound registered: SOTOZGCOYKZRGH-UHFFFAOYSA-N", res
		assertNotNull Compound.findByInchiKey("SOTOZGCOYKZRGH-UHFFFAOYSA-N")
	}

	@Test
	@Ignore("Not fully implemented")
	void testExistingRegisterInchi() {
		def inchi = "InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1"
		def inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
		def name = "alanina"

		String jsonhit = """{"inchikey":"$inchikey", "searchTerm":"$name"}"""

		controller.request.json = jsonhit
		controller.registerInchi()

		def status = controller.response.status
		def res = controller.response.contentAsString

		assertEquals 204, controller.response.status
		assertEquals "Compound not registered: QNAYBMKLOCPYGJ-REOHCLBHSA-N", res
	}

	@Test
	@Ignore("Not fully implemented")
	void testAddInchikeyEmptyParams() {
		def exp = [status:400, message:""]//"The required parameter was null or empty.\nPlease provide a valid inchikey and a name in a json string like\n{"inchikey":"XXXXXXXXXXXXXX-XXXXXXXXXX-X", "searchTerm":"compound x"}"""]

		controller.request.json = []
		controller.registerInchi()

		def res = [status:controller.response.status, message: ""]

		assertEquals exp, res
	}
	@Test
	@Ignore("Not fully implemented")
	void testAddInchikeyNullParams() {
		def exp = [status:400, message:""]//"The required parameter was null or empty.\nPlease provide a valid inchikey and a name in a json string like\n{"inchikey":"XXXXXXXXXXXXXX-XXXXXXXXXX-X", "searchTerm":"compound x"}"""]

		controller.registerInchi()

		def res = [status:controller.response.status, message: ""]

		assertEquals exp, res
	}
}
