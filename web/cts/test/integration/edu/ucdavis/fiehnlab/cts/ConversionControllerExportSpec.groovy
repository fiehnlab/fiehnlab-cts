package edu.ucdavis.fiehnlab.cts

import de.andreasschmitt.export.ExportService
import grails.plugin.spock.IntegrationSpec
import org.apache.log4j.Logger

/**
 * Created by Diego on 11/30/2015.
 */
class ConversionControllerExportSpec extends IntegrationSpec {
	private static Logger logger = Logger.getLogger(ConversionControllerExportSpec.class)
	def conversionController = new ConversionController()
	def exportService = Stub(ExportService)
	def conversionService

	public void testExport() {
		setup:
		conversionController.exportService = exportService
		conversionController.conversionService = conversionService

		when:
		conversionController.params.from = "PubChem CID"
		conversionController.params.to = "InChIKey"
		conversionController.params.value = "150003"
		conversionController.params.format = "excel"
		conversionController.params.max = "10"
		conversionController.params.offset = "0"
		conversionController.params.total = "10"
		conversionController.params.hits = "10"
		conversionController.params.filename = "/target/testExport"

		conversionController.exportTo()

		then:
		conversionController.response.status == 302
//			1 * exportService.export(_) >> null
	}
}
