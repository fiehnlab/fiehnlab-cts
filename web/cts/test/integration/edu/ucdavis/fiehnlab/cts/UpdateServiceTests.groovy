package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger
import org.junit.After
import org.junit.Ignore
import org.junit.Test
/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
class UpdateServiceTests extends GroovyTestCase {
	UpdateService updateService
	Logger logger = Logger.getLogger(this.class)

	@After
	void teardown() {
		Compound.findByInchiKey("SOTOZGCOYKZRGH-UHFFFAOYSA-N")?.delete()
	}

	@Test
	public void testRegisterCompound() {
		def name = "AGN-PC-02GC0X"
		def inchikey = "SOTOZGCOYKZRGH-UHFFFAOYSA-N"

		def res = updateService.registerCompound(inchikey, name)

		assertNotNull res
	}

	@Test
	public void testExistingRegisterCompound() {
		def name = "alanine"
		def inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"

		def res = updateService.registerCompound(inchikey, name)

		assertEquals "inchikey '$inchikey' already exists", res.result.message
	}

	@Test
	public void testInchiKeyToProperties() {
		def inchikey = "SOTOZGCOYKZRGH-UHFFFAOYSA-N"

		def res = updateService.inchikeyToProperties(inchikey)

		assertNotNull res
		assertEquals "C23H17ClN4O4", res.formula[0]
		assertEquals "InChI=1S/C23H17ClN4O4/c24-19-12-11-17(14-21(19)28(31)32)15-25-27-23(30)20(13-16-7-3-1-4-8-16)26-22(29)18-9-5-2-6-10-18/h1-15H,(H,26,29)(H,27,30)", res.inchiCode[0]
        assertEquals 448.863D, res.molWeight[0], 0.05D
        assertEquals 448.094D, res.exactMass[0], 0.05D
		assertTrue res.name[0].equals("N-[3-[2-[(4-chloro-3-nitrophenyl)methylidene]hydrazinyl]-3-oxo-1-phenylprop-1-en-2-yl]benzamide")
	}

	@Test
	@Ignore("Not fully implemented")
	public void testRegisterInchiCode() {
		def inchiCode = "InChI=1S/C23H17ClN4O4/c24-19-12-11-17(14-21(19)28(31)32)15-25-27-23(30)20(13-16-7-3-1-4-8-16)26-22(29)18-9-5-2-6-10-18/h1-15H,(H,26,29)(H,27,30)"
		def name = "N-[3-[2-[(4-chloro-3-nitrophenyl)methylidene]hydrazinyl]-3-oxo-1-phenylprop-1-en-2-yl]benzamide"

		def res = updateService.registerInchiCode(inchiCode, name).result

		assertNotNull res.compound
		assertEquals "SOTOZGCOYKZRGH-UHFFFAOYSA-N", res.compound.inchiKey
		assertEquals "C23H17ClN4O4", res.compound.formula
		assertEquals 448.85848, res.compound.molWeight, 0.005
		assertEquals 448.093833, res.compound.exactMass, 0.00005
		assertEquals "PubChem search", res.compound.sourceFile
	}

	@Test
	public void testBadProps() {
		def inchikey = "SSSSSSSSSSSSSS-SSSSSSSSSS-S"

		def res = updateService.inchikeyToProperties(inchikey)

		assertEquals "No CID found; (404)", res.error
	}
}
