package edu.ucdavis.fiehnlab.cts

import grails.test.mixin.*
import org.junit.*


class IndexControllerTests extends GroovyTestCase {
	def indexController

	@Before
	void setup() {
		indexController = new IndexController()
	}

	void testIndex() {
		indexController.index()

		assert "/index" == indexController.modelAndView.getViewName()
	}
}
