package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Created by diego on 4/24/14.
 */
@RunWith(JUnit4)
class ScoringServiceTests extends GroovyTestCase {
	ScoringService scoringService

	Logger logger = Logger.getLogger(this.getClass())

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	void testMainScoreBio() {
		def keyList = ["QNAYBMKLOCPYGJ-UHFFFAOYSA-N", "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "QNAYBMKLOCPYGJ-UWTATZPHSA-N"]

		def exp = [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0), new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.85), new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.0)]
		def res = scoringService.score(ConstantHelper.SCORING_BIO, keyList)

		assertFalse res.isEmpty()
		assertEquals(exp, res)
	}

	@Test
	void testMainScoreCount() {
		def keyList = ["QNAYBMKLOCPYGJ-UHFFFAOYSA-N", "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "QNAYBMKLOCPYGJ-UWTATZPHSA-N"]

		def exp = [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0), new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 1.0), new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.0)]
//		def exp = [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0), new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.33), new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.0)]
		def res = scoringService.score(ConstantHelper.SCORING_COUNT, keyList);

		assertFalse res.isEmpty()
		assertEquals(exp.sort(), res.sort())
	}

	@Test
	void testBioCountAlanine() {
		def keyList = ["QNAYBMKLOCPYGJ-UHFFFAOYSA-N", "QNAYBMKLOCPYGJ-REOHCLBHSA-N", "QNAYBMKLOCPYGJ-UWTATZPHSA-N"]

		def exp = [new ScoredValue("QNAYBMKLOCPYGJ-REOHCLBHSA-N", 1.0), new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 0.85), new ScoredValue("QNAYBMKLOCPYGJ-UWTATZPHSA-N", 0.0)]
		def res = scoringService.scoreByBiologicalCount(keyList)

		assertFalse res.isEmpty()
		assertEquals(exp, res)
	}

	@Test
	void testBioCountThreonicAcid() {
		def key2 = ["JPIJQSOTBSSVTP-GBXIJSLDSA-N", "JPIJQSOTBSSVTP-STHAYSLISA-N", "JPIJQSOTBSSVTP-UHFFFAOYSA-N"]

		def res = scoringService.scoreByBiologicalCount(key2)
		def exp = [new ScoredValue("JPIJQSOTBSSVTP-STHAYSLISA-N", 1.0), new ScoredValue("JPIJQSOTBSSVTP-GBXIJSLDSA-N", 0.09), new ScoredValue("JPIJQSOTBSSVTP-UHFFFAOYSA-N", 0.0)]

		assertFalse res.isEmpty()
		assertEquals(exp, res)
	}

	@Test
	void testScoreByDatabaseCount() throws Exception {
		def exp = [new ScoredValue("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", 1.0)]
		def res = scoringService.scoreByDatabaseCount(["QNAYBMKLOCPYGJ-UHFFFAOYSA-N"]);

		assertFalse res.isEmpty()
		assertEquals(exp, res)
	}

	@Test
	void testInvalidDatabaseCountScore() throws Exception {
		def searchTerm = "no such inchi"
		List<ScoredValue> exp = new ArrayList<ScoredValue>()
		exp.add(new ScoredValue('no such inchi', 0.0))

		List<ScoredValue> res = (ArrayList<ScoredValue>) scoringService.scoreByDatabaseCount([searchTerm])

		assertFalse res.isEmpty()
		assertEquals(exp[0], res[0])
	}

	@Test
	void testInvalidBiologicalCountScore() throws Exception {
		def searchTerm = "no such inchi"
		List<ScoredValue> exp = new ArrayList<ScoredValue>()
		exp.add(new ScoredValue('no such inchi', 0.0))

		List<ScoredValue> res = scoringService.scoreByBiologicalCount([searchTerm])

		assertFalse res.isEmpty()
		assertEquals(exp[0], res[0])
	}
}
