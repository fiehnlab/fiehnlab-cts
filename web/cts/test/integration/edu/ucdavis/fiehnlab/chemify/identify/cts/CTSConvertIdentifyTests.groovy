package edu.ucdavis.fiehnlab.chemify.identify.cts
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTests
/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 7:43 PM
 * <p/>
 * Tests the CTS convert function
 */
public class CTSConvertIdentifyTests extends AbstractIdentifyTests {
	def grailsApplication

	@Override
	protected String getTestTerm() {
		return "Alanine"
	}

	@Override
	public AbstractIdentify getIdentify() {
		return new CTSConvertIdentify(grailsApplication.mainContext)
	}
}
