package edu.ucdavis.fiehnlab.chemify.scoring.cts
import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Scored
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.identify.TestIdentifyMapper
import org.apache.log4j.Logger
import org.junit.Test

import static org.junit.Assert.assertEquals
/**
 * Created by diego on 10/29/14.
 */
public class ScoreByAverageCombinedScoreTest extends GroovyTestCase {
	Logger logger = Logger.getLogger(this.class)

	def averageCombinedScore

	@Test
	void testScore() {
		List<? extends Hit> input = new ArrayList<HitImpl>()
		input.add(new HitImpl("QNAYBMKLOCPYGJ-REOHCLBHSA-N", new TestIdentifyMapper(), "alanine", "alanine"))
		input.add(new HitImpl("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", new TestIdentifyMapper(), "alanine", "alanine"))
		input.add(new HitImpl("QNAYBMKLOCPYGJ-UWTATZPHSA-N", new TestIdentifyMapper(), "alanine", "alanine"))

		//without kegg scorer
//		List<Double> scores = [0.2324823196550647D, 0.7125708247323062D, 1.0D]

		//with kegg scorer
		List<Double> scores = [1.0D, 1.0D, 0.02D]

		List<Scored> scResult = averageCombinedScore.score(input, "alanine")

		scResult.each{ s -> logger.debug("${s.inchiKey} => ${s.score}")}

		assertFalse(scResult.isEmpty())
		assertEquals(input.inchiKey, scResult.inchiKey)
		scores.eachWithIndex { double entry, int i ->
			assertEquals(entry, scResult.score[i], 0.05)
		}
	}
}
