package edu.ucdavis.fiehnlab.chemify

import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit
import edu.ucdavis.fiehnlab.chemify.identify.cts.CTSInverseKeggIdentify
import edu.ucdavis.fiehnlab.chemify.io.SpringConfiguredChemify
import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Test

import javax.ws.rs.ServerErrorException

import static org.junit.Assert.assertEquals

/**
 * Created by diego on 4/14/14.
 * <p/>
 * Expected conversions:
 * Carnitine	C00318
 * catechin	C06562
 * threonine	C00188
 * threonic acid	C01620
 * phenylethylamine	C05332
 * phenylalanine	C00079
 * N-acetyl-L-ornithine	C00437
 * mannitol	C00392
 * glutathione	C00051
 * threitol	C16884
 * glutamic acid	C00025
 * alpha ketoglutaric acid	C00026
 * alanine	C00041
 * succinic acid	C00042
 * UDP GlcNAc	C00043
 * lysine	C00047
 * aspartic acid	C00049
 * glutamine	C00064
 * serine	C00065
 * methionine	C00073
 * ornithine	C00077
 * tryptophan	C00078
 * Phenylalanine	C00079
 * tyrosine	C00082
 */
public class DmitryTestSuit {
	protected Chemify chemify;
	protected Logger logger = Logger.getLogger(this.getClass());
	protected CTSInverseKeggIdentify cts;

	@Before
	public void setUp() {
		this.chemify = this.getChemify();
		logger.debug("Configuration: " + chemify.getDefaultConfiguration());
		cts = new CTSInverseKeggIdentify();
	}

	@After
	public void tearDown() {
		this.cts = null;
		this.chemify = null;
	}

	@Test
	public void testCarnitine() {
		assertEquals("C00318", validate("Carnitine"));
	}

	@Test
	public void testCatechin() {
		assertEquals("C06562", validate("catechin"));
	}

	@Test
	public void testThreonine() {
		assertEquals("C00188", validate("threonine"));
	}

	@Test
	public void testThreonicAcid() {
		assertEquals("C01620", validate("threonic acid"));
	}

	@Test
	public void testPhenylethylamine() {
		assertEquals("C05332", validate("phenylethylamine"));
	}

	@Test
	public void testPhenylalanine() {
		assertEquals("C00079", validate("phenylalanine"));
		assertEquals("C00079", validate("Phenylalanine"));
	}

	@Test
	public void testNAcetylLOrnithine() {
		assertEquals("C00437", validate("N-acetyl-L-ornithine"));
	}

	@Test
	public void testMannitol() {
		assertEquals("C00392", validate("mannitol"));
	}

	@Test
	public void testGlutathione() {
		assertEquals("C00051", validate("glutathione"));
	}

	@Test
	public void testThreitol() {
		assertEquals("C16884", validate("threitol"));
	}

	@Test
	public void testGlutamicAcid() {
		assertEquals("C00025", validate("glutamic acid"));
	}

	@Test
	public void testAlphaKetoglutaricAcid() {
		assertEquals("C00026", validate("alpha ketoglutaric acid"));
	}

	@Test
	public void testAlanine() {
		assertEquals("C00041", validate("alanine"));
	}

	@Test
	public void testSuccinicAcid() {
		assertEquals("C00042", validate("succinic acid"));
	}

	@Test
	public void testUDPGlcNAc() {
		assertEquals("C00043", validate("UDP GlcNAc"));
	}

	@Test
	public void testLysine() {
		assertEquals("C00047", validate("lysine"));
	}

	@Test
	public void testAsparticAcid() {
		assertEquals("C00049", validate("aspartic acid"));
	}

	@Test
	public void testGlutamine() {
		assertEquals("C00064", validate("glutamine"));
	}

	@Test
	public void testSerine() {
		assertEquals("C00065", validate("serine"));
	}

	@Test
	public void testMethionine() {
		assertEquals("C00073", validate("methionine"));
	}

	@Test
	public void testOrnithine() {
		assertEquals("C00077", validate("ornithine"));
	}

	@Test
	public void testTryptophan() {
		assertEquals("C00078", validate("tryptophan"));
	}

	@Test
	public void testTyrosine() {
		assertEquals("C00082", validate("tyrosine"));
	}

	/**
	 * helper method to automate the identification and conversion of names to Keggs for Dmitry's test suit.
	 *
	 * @param searchTerm a string with the Chemical name to be transformed/tested.
	 * @return a string with the KEGG ID
	 */

	private String validate(String searchTerm) {
		List<? extends Hit> tmp = chemify.identify(searchTerm);
		List<? extends Hit> results = new ArrayList<Scored>();

		for (Hit hit : tmp) {
			logger.debug("inchis: " + (ScoredHit) hit);
			logger.debug("inchis: " + ((ScoredHit) hit).getInchiKey());
			logger.debug("class: " + hit.getClass());
		}

		String inchiKey = tmp.get(0).getInchiKey();
		logger.info("First step (" + searchTerm + " to inchiKey): " + inchiKey);

		try {
			results = cts.identify(inchiKey, inchiKey);
		} catch (ServerErrorException e) {
		}

		Collections.sort(results);

		logger.info("Results for " + searchTerm);
		for (Hit i : results) {
			logger.info("===== " + i.toString() + " =====");
		}

		logger.info("FINAL RESULT (" + searchTerm + " to KEGG): " + results);
		if (results.size() > 0)
			return results.get(0).getInchiKey();
		else
			return "no kegg found";
	}

	/**
	 * returns our chemify instance to be used
	 *
	 * @return
	 */
	protected Chemify getChemify() {
		return new SpringConfiguredChemify("grails-app/conf/spring/defaultConf.xml");
	}
}
