package edu.ucdavis.fiehnlab.chemify.scoring.cts

import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Scored
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll
import edu.ucdavis.fiehnlab.chemify.identify.TestIdentifyMapper
import org.apache.log4j.Logger
import org.junit.Test
/**
 * Created with IntelliJ IDEA.
 * <p/>
 * Tests our scoring algorithm
 */
public class ScoreByBiologicalCountTest extends GroovyTestCase {
	Logger logger = Logger.getLogger(this.getClass())

	ScoreByBiologicalCount scoreByBiologicalCount

	@Test
	public void testScore() throws Exception {

		List<Hit> input = new ArrayList<Hit>()
		input.add(new HitImpl("QNAYBMKLOCPYGJ-REOHCLBHSA-N", new TestIdentifyMapper(), "alanine", "alanine"))
		input.add(new HitImpl("QNAYBMKLOCPYGJ-UWTATZPHSA-N", new TestIdentifyMapper(), "alanine", "alanine"))
		input.add(new HitImpl("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", new TestIdentifyMapper(), "alanine", "alanine"))

		List<Scored> scResult = scoreByBiologicalCount.score(input, "alanine")

		assertFalse(scResult.isEmpty())
		assertEquals("QNAYBMKLOCPYGJ-REOHCLBHSA-N", scResult.get(0).getInchiKey())
		assertEquals(1.0, scResult.get(0).getScore(), 0.1)
		assertEquals("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", scResult.get(1).getInchiKey())
		assertEquals(0.85, scResult.get(1).getScore(), 0.1)
		assertEquals("QNAYBMKLOCPYGJ-UWTATZPHSA-N", scResult.get(2).getInchiKey())
		assertEquals(0.0, scResult.get(2).getScore(), 0.1)
	}

	@Test
	public void testNotExistingInchiScore() throws Exception {

		List<Hit> data = new Vector<Hit>()
		data.add(new HitImpl("bad inchi", new NothingIdentifiedAtAll(), "nothing found", "bad inchi"))

		List<Scored> result2 = scoreByBiologicalCount.score(data, "bad inchi")

		assertFalse(result2.isEmpty())
		assertEquals("bad inchi", result2.get(0).getInchiKey())
		assertEquals(0.0, result2.get(0).getScore(), 0.1)
	}

//	@Test
//	public void testScoreAlanine() {
//
//		List<Scored> exp = new ArrayList<Scored>()
//
//		exp.add(new ScoredHit("QNAYBMKLOCPYGJ-REOHCLBHSA-N", new TestIdentifyMapper(), "alanine", new ScoreByBiologicalCount(), 1.0, "alanine"))
//		exp.add(new ScoredHit("QNAYBMKLOCPYGJ-UWTATZPHSA-N", new TestIdentifyMapper(), "alanine", new ScoreByBiologicalCount(), 0.85, "alanine"))
//		exp.add(new ScoredHit("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", new TestIdentifyMapper(), "alanine", new ScoreByBiologicalCount(), 0.0, "alanine"))
//
//		List<Scored> res = (List<Scored>) chemifyBean.identify("alanine")
//
//		assertEquals(exp.inchiKey.sort(), res.inchiKey.sort())
//		assertEquals(exp.score.sort(), res.score.sort())
//		assertEquals(exp.searchTerm.sort(), res.searchTerm.sort())
//	}
}
