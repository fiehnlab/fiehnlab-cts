package edu.ucdavis.fiehnlab.chemify.identify

import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Priority
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/9/13
 * Time: 2:13 PM
 * <p/>
 * just for testing and mocking nothing else
 */
public class TestIdentifyMapper extends AbstractIdentify {
	/**
	 * sets the internal priority of this identity object
	 */
	public TestIdentifyMapper() {
		super(Priority.NO_RESULT_POSSIBLE);
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {
		return Collections.EMPTY_LIST;
	}

	public String getDescription() {
		return "this method is used for testing only";
	}
}
