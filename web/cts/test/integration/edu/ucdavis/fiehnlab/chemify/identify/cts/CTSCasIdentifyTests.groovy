package edu.ucdavis.fiehnlab.chemify.identify.cts

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTests

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 2:25 PM
 */
public class CTSCasIdentifyTests extends AbstractIdentifyTests {
	@Override
	protected String getTestTerm() {
		return "149591-34-4"
	}

	@Override
	public AbstractIdentify getIdentify() {
		return new CTSCasIdentify()
	}
}
