package edu.ucdavis.fiehnlab.chemify.identify
import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.configuration.SimpleConfiguration
import edu.ucdavis.fiehnlab.cts.PatternHelper
import org.apache.log4j.Logger
import org.junit.*
/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 7:29 PM
 * <p/>
 * Abstract test class to simplify testing for identify tests
 */
public abstract class AbstractIdentifyTests extends GroovyTestCase {
	Logger logger = Logger.getLogger(getClass());

	def identify
	def conversionService

	@Before
	public void setUp() {
		identify = getIdentify();
		identify.conversionService = conversionService
	}

	@After
	public void tearDown() {
		identify = null
	}

	@Test
	public void testSetConfiguration() throws Exception {
		testHasConfiguration();
	}

	@Test
	public void testGetConfiguration() throws Exception {
		testHasConfiguration();
	}

	@Test
	public void testHasConfiguration() throws Exception {
		identify.setConfiguration(new SimpleConfiguration());

		assertTrue(identify.hasConfiguration() == true);
		assertTrue(identify.getConfiguration() != null);

		identify = getIdentify();
		identify.setConfiguration(null);

		assertTrue(identify.hasConfiguration() == false);
		assertTrue(identify.getConfiguration() == null);

	}

	@Test
	public void testIdentifyFailed() {
		String term = "this should not exist" + new Random().nextLong();
		List<Hit> result = identify.identify(term, term);

		logger.error("results: $result")
		logger.error("size: ${result.size()}")

		assertTrue(result.size() == 0);
	}

	@Test
	public void testIdentify() throws Exception {

		for (String s : this.getTestTerms()) {
			List<Hit> result = identify.identify(s, s);

//			logger.debug("identify result: " + result);

			assertTrue(result.isEmpty() == false);
			assertTrue(result.get(0) instanceof NothingIdentifiedAtAll.NoHit == false);
		}
	}

	@Test
	public void testIdentifyResultIsInChiKey() {

		for (String s : this.getTestTerms()) {

			List<Hit> result = identify.identify(s, s);

			assertTrue(result.isEmpty() == false);
			assertTrue(result.get(0).getInchiKey().matches(PatternHelper.STD_INCHI_KEY_PATTERN));
		}
	}


	@Test
	public void testIdentifyResultHasAlgorithmImplementationSet() {

		for (String s : this.getTestTerms()) {

			List<Hit> result = identify.identify(s, s);

			assertTrue(result.size() > 0);
			assertTrue(result.get(0).getUsedAlgorithm() != null);
			assertTrue(result.get(0).getUsedAlgorithm() instanceof AbstractIdentify);
			assertTrue(result.get(0).getUsedAlgorithm().getClass().getName().equals(this.getIdentify().getClass().getName()));
		}
	}

	/**
	 * returns the actual term to be tested
	 *
	 * @return
	 */
	protected String getTestTerm() {
		throw new NoSuchMethodError("you need to overwrite the getTestTerm or getTestTerms method to make this test work!");
	}

	protected String[] getTestTerms() {
		return [getTestTerm()];
	}

	/**
	 * implmementation needs to provide us with a new identify object
	 *
	 * @return
	 */
	public abstract AbstractIdentify getIdentify();
}
