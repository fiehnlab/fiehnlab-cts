package edu.ucdavis.fiehnlab.chemify.identify.cts

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTests

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 2:34 PM
 * <p/>
 *
 */
public class CTSHMDBIdentifyTests extends AbstractIdentifyTests {
	@Override
	protected String getTestTerm() {
		return "HMDB00161";
	}

	@Override
	public AbstractIdentify getIdentify() {
		return new CTSHMDBIdentify();
	}
}
