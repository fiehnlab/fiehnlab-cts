package edu.ucdavis.fiehnlab.chemify.identify.cts

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTests

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 2:04 PM
 *
 * simple web service based query to identify inchi keys
 */
public class CTSInChIKeyIdentifyTests extends AbstractIdentifyTests {
	@Override
	protected String getTestTerm() {
		return "QNAYBMKLOCPYGJ-UHFFFAOYSA-N";
	}

	@Override
	public AbstractIdentify getIdentify() {
		return new CTSInChIKeyIdentify();
	}
}
