package edu.ucdavis.fiehnlab.chemify.scoring.kegg
import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Scored
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.identify.TestIdentifyMapper
import org.apache.log4j.Logger
import org.junit.Test

public class ScoreByCTSKeggPresenceTest extends GroovyTestCase {
	Logger logger = Logger.getLogger(this.class);

	def scoreByCTSKeggPresence

	@Test
	public void testScore() throws Exception {
		List<Hit> hits = [new HitImpl("JPIJQSOTBSSVTP-STHAYSLISA-N", new TestIdentifyMapper(), "alanine", "alanine")]

		List<Scored> res = scoreByCTSKeggPresence.score(hits, "alanine")
//logger.debug("res: $res")

		assertTrue(res[0].score == 1)
	}

	@Test
	public void testNoScore() throws Exception {
		List<Hit> hits = [new HitImpl("OYHTZOLNNSQJLH-UHFFFAOYSA-N", new TestIdentifyMapper(), "carbamistico", "carbamistico")]

		List<Scored> res = scoreByCTSKeggPresence.score(hits, "carbamistico")
//logger.debug("res: $res")

		assertTrue(res[0].score == 0)
	}
}