DROP INDEX synonym_inchikey_index;
DROP INDEX synonym_name_index;
DROP INDEX synonym_name_lower_index;
DROP INDEX synonym_name_type_index;
DROP INDEX synonym_gin_tsv_index;
GO

CREATE INDEX synonym_inchikey_index ON public.synonym (inchi_key);
CREATE INDEX synonym_name_index ON public.synonym (name);
CREATE INDEX synonym_lower_name_index ON public.synonym (lower(name));
CREATE INDEX synonym_name_type_index ON public.synonym (name, type DESC);
CREATE INDEX synonym_gin_tsv_index ON synonym USING GIN (tsv_syns);
GO