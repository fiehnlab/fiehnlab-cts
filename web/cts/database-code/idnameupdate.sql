--updates the id_name table with the latest values
insert into id_name
select * from
 ( select nextval('hibernate_sequence') as id) a,
 ( select distinct name from external_id where name not in (select distinct name from id_name)) b
