DROP INDEX externalid_value_index;
DROP INDEX externalid_value_lower_index;
DROP INDEX externalid_name_lower_index;
DROP INDEX externalid_name_value_index;
DROP INDEX externalid_name_index;
DROP INDEX externalid_inchikey_index;
DROP INDEX externalid_value_name_inchikey_index;
GO

CREATE INDEX externalid_inchikey_index ON public.external_id(inchi_key)
CREATE INDEX externalid_lower_name_index ON public.external_id (lower(value));
CREATE INDEX externalid_lower_name_value_index ON public.external_id (lower(name), lower(value));
CREATE INDEX externalid_lower_value_index ON public.external_id (lower(value));
CREATE INDEX externalid_name_index ON public.external_id(name);
CREATE INDEX externalid_name_value_index ON public.external_id (name, value);
CREATE INDEX externalid_value_index ON public.external_id (value);
CREATE UNIQUE INDEX externalid_value_name_inchikey_index ON public.external_id(value, name, inchi_key)
GO

