explain
select this_.id as id2_0_, this_.exact_mass as exact2_2_0_, this_.formula as formula2_0_, this_.inchi_code as inchi4_2_0_, this_.mol_weight as mol5_2_0_ 
from compound this_ 
where this_.id='QNAYBMKLOCPYGJ-REOHCLBHSA-N'
go

explain
select synonyms0_.inchi_key as inchi2_2_1_, synonyms0_.id as id1_, synonyms0_.id as id1_0_, synonyms0_.inchi_key as inchi2_1_0_, synonyms0_.name as name1_0_, synonyms0_.score as score1_0_, synonyms0_.type as type1_0_ 
from synonym synonyms0_ 
where synonyms0_.inchi_key='QNAYBMKLOCPYGJ-REOHCLBHSA-N'
go

explain
select extids0_.inchi_key as inchi2_2_1_, extids0_.id as id1_, extids0_.id as id0_0_, extids0_.inchi_key as inchi2_0_0_, extids0_.name as name0_0_, extids0_.url as url0_0_, extids0_.value as value0_0_ 
from external_id extids0_ 
where extids0_.inchi_key='QNAYBMKLOCPYGJ-REOHCLBHSA-N'
go

explain
select this_.id as id0_0_, this_.inchi_key as inchi2_0_0_, this_.name as name0_0_, this_.url as url0_0_, this_.value as value0_0_ 
from external_id this_ 
where this_.inchi_key='QNAYBMKLOCPYGJ-REOHCLBHSA-N' and this_.name='KEGG'
go
