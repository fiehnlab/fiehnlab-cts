SELECT 'compounds' as table_name, reltuples as hits_count FROM pg_class WHERE relname = 'compound'
union
SELECT 'synonyms' as table_name, reltuples as hits_count FROM pg_class WHERE relname = 'synonym'
union
SELECT 'external ids' as table_name, reltuples as hits_count FROM pg_class WHERE relname = 'external_id';
