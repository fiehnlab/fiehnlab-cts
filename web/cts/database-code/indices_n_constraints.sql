CREATE INDEX  "synonym_name_index" 
	ON "public"."synonym"("name") tablespace indexes

GO
CREATE INDEX  "synonym_name_lower_index" 
	ON "public"."synonym"(lower("name")) tablespace indexes

GO
CREATE INDEX  "externalid_name_index" 
	ON "public"."external_id"("name") tablespace indexes

GO
CREATE INDEX  "externalid_name_lower_index" 
	ON "public"."external_id"(lower("name")) tablespace indexes

GO
CREATE INDEX  "externalid_value_index" 
	ON "public"."external_id"("value") tablespace indexes

GO
CREATE INDEX  "externalid_value_lower_index" 
	ON "public"."external_id"(lower("value")) tablespace indexes

GO
CREATE INDEX  "compound_mass_index" 
	ON "public"."compound"("mol_weight") tablespace indexes

GO

CREATE INDEX  "synonym_inchikey_index" 
	ON "public"."synonym"("inchi_key") tablespace indexes

GO
CREATE INDEX  "externalid_inchikey_index" 
	ON "public"."external_id"("inchi_key") tablespace indexes

GO

 ALTER TABLE synonym ADD COLUMN tsv_syns TSVECTOR

GO

 CREATE INDEX synonym_gist_tsv_index ON synonym USING GIST (tsv_syns)
