﻿SELECT c.id, c.inchi_code, c.mol_weight
FROM compound AS c LEFT OUTER JOIN external_id AS e ON c.id = e.inchi_key
WHERE c.id = 'ala' OR (e.value = 'ala' AND e.name = 'Chemical Name')
UNION
SELECT c.id, c.inchi_code, c.mol_weight
FROM compound AS c LEFT OUTER JOIN synonym AS s ON (c.id = s.inchi_key)
WHERE s.name LIKE '%ala%'
ORDER BY id;