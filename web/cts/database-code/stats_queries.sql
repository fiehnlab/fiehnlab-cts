select count(*) from compound where source_file is null
go

SELECT relnamespace as ns, relname as table, reltuples::bigint AS rows FROM pg_class where relname in ('compound', 'external_id', 'synonym'); 
go

SELECT nspname AS dbname, relname as table, reltuples::bigint as rows
FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE 
  nspname NOT IN ('pg_catalog', 'information_schema') AND
  relkind='r' 
ORDER BY reltuples DESC;
go

SELECT schemaname, relname, n_live_tup
FROM pg_stat_user_tables 
ORDER BY n_live_tup DESC;
go