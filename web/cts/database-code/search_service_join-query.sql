﻿SELECT DISTINCT c.id, c.inchi_code, c.mol_weight, e.name, e.value, e.url, s.name, s.type
FROM (compound AS c LEFT OUTER JOIN external_id AS e ON c.id = e.inchi_key) 
	LEFT OUTER JOIN synonym AS s ON (c.id = s.inchi_key)
WHERE c.id = 'alanine'
	OR (e.value = 'alanine' AND e.name = 'Chemical Name')
	OR s.name ILIKE '%alanine%'
ORDER BY 1

