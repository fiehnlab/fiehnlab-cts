##############
## Postgres ##
##############

-- Function: insert_compound(character varying, text, double precision)

-- DROP FUNCTION insert_compound(character varying, text, double precision);

CREATE OR REPLACE FUNCTION insert_compound(inchikey character varying, inchicode text, molweight double precision)
  RETURNS void AS
$BODY$
BEGIN
	INSERT INTO compound(id,inchi_code,mol_weight) VALUES (inchikey,inchicode,molweight);
		RETURN;
	EXCEPTION WHEN unique_violation THEN
	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;



-- Function: insert_synonym(character varying, text, character varying)

-- DROP FUNCTION insert_synonym(character varying, text, character varying);

CREATE OR REPLACE FUNCTION insert_synonym(inchikey character varying, namec text, typec character varying)
  RETURNS void AS
$BODY$
BEGIN
	INSERT INTO synonym(id,inchi_key,name,type) SELECT nextval('hibernate_sequence'),inchikey,namec,typec,source
	WHERE NOT EXISTS (SELECT inchi_key FROM external_id e WHERE inchi_key = inchikey AND value = namec);
		RETURN;
	EXCEPTION WHEN unique_violation THEN
		RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;



-- Function: insert_external_id(character varying, text, character varying, character varying)

-- DROP FUNCTION insert_external_id(character varying, text, character varying, character varying);

CREATE OR REPLACE FUNCTION insert_external_id(inchikey character varying, namec text, urlc character varying, valuec character varying)
  RETURNS void AS
$BODY$
BEGIN
    INSERT INTO external_id(id,inchi_key,name,url,value) VALUES (nextval('hibernate_sequence'),inchikey,namec,urlc,valuec);
           RETURN;
    EXCEPTION WHEN unique_violation THEN
           RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
