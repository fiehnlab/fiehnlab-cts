//SELECT *
SELECT pg_terminate_backend( procpid )
FROM pg_stat_activity
WHERE procpid <> pg_backend_pid( )
    AND datname in ('cts', 'cts_fixing');

alter database cts rename to cts_alt;
alter database cts_fixing rename to cts-alt;
go


SELECT *
FROM pg_stat_activity
WHERE procpid <> pg_backend_pid( )
    AND datname in ('cts', 'cts_fixing')
order by procpid;
go


SELECT relname as table, reltuples::bigint AS rows FROM pg_class where relname in ('compound', 'external_id', 'synonym'); 
go


SELECT pg_terminate_backend( procpid )
FROM pg_stat_activity
WHERE procpid <> pg_backend_pid( )
    AND datname in ('cts_fixing')
order by procpid;

alter database cts_fixing rename to "cts-alt";
go


explain
UPDATE compound SET data_sources_count = '45', 
    exact_mass = '105.042590000000004', 
    formula = 'C3H7NO3', 
    inchi_code = 'InChI=1S/C3H7NO3/c1-2-7-3(5)4-6/h6H,2H2,1H3,(H,4,5)', 
    mol_weight = '105.092709999999997', 
    pubmed_hits = '0', 
    references_count = '105', 
    source_file = 'Compound_000000001_000025000.sdf.gz' 
WHERE id = 'VGEWEGHHYWGXGG-UHFFFAOYSA-N';
go

explain
alter table compound rename column data_source_count to data_sources_count;
go