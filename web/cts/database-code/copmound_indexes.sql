DROP INDEX compound_weight_index;
DROP INDEX compound_mass_index;
DROP INDEX compound_formula_index;
GO

CREATE INDEX compound_mass_index ON public.compound(mol_weight);
CREATE INDEX compound_weight_index ON public.compound(exact_mass);
CREATE INDEX compound_formula_index ON public.compound(formula);
GO
