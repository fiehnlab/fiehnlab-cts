//-------------------index-------------------
select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_


//-------------------regular search --- the limit can be 10, 25 or 50 as set in the form's select-------------------
select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct compound0_.id as id1_, compound0_.inchi_code as inchi2_1_, compound0_.mol_weight as mol3_1_ from compound compound0_ inner join external_id extids1_ on compound0_.id=extids1_.inchi_key inner join synonym synonyms2_ on compound0_.id=synonyms2_.inchi_key where compound0_.id='a*' or extids1_.value='a*' or lower(synonyms2_.name) like 'a*' limit 10


select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='QGZKDVFQNNGYKY-UHFFFAOYSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='NLHHRLWOUZZQLW-UHFFFAOYSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='QWAGALBSTFENEE-UHFFFAOYSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='WMOVHXAZOJBABW-UHFFFAOYSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='IADUEWIQBXOCDZ-UHFFFAOYSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='FQMDCEJHLOLKLI-KQYNXXCUSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='PZWDHVKNXVLHOV-UHFFFAOYSA-K'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='SVANEUDZNISLJD-UHFFFAOYSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='XEKOWRVHYACXOJ-UHFFFAOYSA-N'
select synonyms0_.inchi_key as inchi2_1_1_, synonyms0_.id as id1_, synonyms0_.id as id2_0_, synonyms0_.inchi_key as inchi2_2_0_, synonyms0_.name as name2_0_, synonyms0_.type as type2_0_ from synonym synonyms0_ where synonyms0_.inchi_key='HBMXBCHRTAJZIH-UHFFFAOYSA-N'


//-------------------conv data for select boxes-------------------
select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

//conv name 2 name
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct synonym0_.inchi_key as col_0_0_, synonym0_.name as col_1_0_, 'Chemical Name' as col_2_0_, 'a*' as col_3_0_, 'Chemical Name' as col_4_0_, compound2_.id as id1_, compound2_.inchi_code as inchi2_1_, compound2_.mol_weight as mol3_1_ from synonym synonym0_ inner join compound compound2_ on synonym0_.inchi_key=compound2_.id cross join synonym synonym1_ where (lower(synonym1_.name) like a%) and synonym0_.inchi_key=synonym1_.inchi_key limit 10

//-------------------conv name 2 inchikey-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct synonym0_.inchi_key as col_0_0_, synonym0_.inchi_key as col_1_0_, 'InChIKey' as col_2_0_, 'a*' as col_3_0_, 'Chemical Name' as col_4_0_, compound1_.id as id1_0_, compound2_.id as id1_1_, compound1_.inchi_code as inchi2_1_0_, compound1_.mol_weight as mol3_1_0_, compound2_.inchi_code as inchi2_1_1_, compound2_.mol_weight as mol3_1_1_ from synonym synonym0_ inner join compound compound1_ on synonym0_.inchi_key=compound1_.id inner join compound compound2_ on synonym0_.inchi_key=compound2_.id where lower(synonym0_.name) like 'a%' limit 10

//-------------------conv name 2 inchi code-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select synonym0_.inchi_key as col_0_0_, compound1_.inchi_code as col_1_0_, 'InChI Code' as col_2_0_, 'a*' as col_3_0_, 'Chemical Name' as col_4_0_, compound2_.id as id1_, compound2_.inchi_code as inchi2_1_, compound2_.mol_weight as mol3_1_ from synonym synonym0_ inner join compound compound2_ on synonym0_.inchi_key=compound2_.id cross join compound compound1_ where synonym0_.inchi_key=compound1_.id and (lower(synonym0_.name) like 'a%') limit 10

//-------------------conv name 2 id-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct synonym0_.inchi_key as col_0_0_, externalid1_.value as col_1_0_, 'Pubchem SID' as col_2_0_, 'a*' as col_3_0_, 'Chemical Name' as col_4_0_, compound2_.id as id1_, compound2_.inchi_code as inchi2_1_, compound2_.mol_weight as mol3_1_ from synonym synonym0_ inner join compound compound2_ on synonym0_.inchi_key=compound2_.id cross join external_id externalid1_ where synonym0_.inchi_key=externalid1_.inchi_key and (lower(synonym0_.name) like 'a%') and externalid1_.name='Pubchem SID' limit 10


//-------------------conv inchikey 2 name-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct compound0_.id as col_0_0_, synonym1_.name as col_1_0_, 'Chemical Name' as col_2_0_, 'PVKSNHVPLWYQGJ-KQYNXXCUSA-N' as col_3_0_, 'InChIKey' as col_4_0_ from compound compound0_ cross join synonym synonym1_ where compound0_.id='PVKSNHVPLWYQGJ-KQYNXXCUSA-N' and compound0_.id=synonym1_.inchi_key limit 10

//-------------------conv inchikey 2 inchikey ---> any sense?-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct compound0_.id as col_0_0_, compound0_.id as col_1_0_, 'InChIKey' as col_2_0_, 'PVKSNHVPLWYQGJ-KQYNXXCUSA-N' as col_3_0_, 'InChIKey' as col_4_0_ from compound compound0_ where compound0_.id='PVKSNHVPLWYQGJ-KQYNXXCUSA-N' limit 10

//-------------------conv inchikey 2 inchi code-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct compound0_.id as col_0_0_, compound0_.inchi_code as col_1_0_, 'InChI Code' as col_2_0_, 'PVKSNHVPLWYQGJ-KQYNXXCUSA-N' as col_3_0_, 'InChIKey' as col_4_0_ from compound compound0_ where compound0_.id='PVKSNHVPLWYQGJ-KQYNXXCUSA-N' limit 10

//-------------------conv inchikey 2 id-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct compound0_.id as col_0_0_, externalid1_.value as col_1_0_, 'Pubchem SID' as col_2_0_, 'PVKSNHVPLWYQGJ-KQYNXXCUSA-N' as col_3_0_, 'InChIKey' as col_4_0_ from compound compound0_ cross join external_id externalid1_ where compound0_.id='PVKSNHVPLWYQGJ-KQYNXXCUSA-N' and externalid1_.name='Pubchem SID' and compound0_.id=externalid1_.inchi_key limit 10


//-------------------conv id to name-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct externalid0_.inchi_key as col_0_0_, synonym1_.name as col_1_0_, 'Chemical Name' as col_2_0_, '160650082' as col_3_0_, 'Pubchem SID' as col_4_0_, compound2_.id as id1_, compound2_.inchi_code as inchi2_1_, compound2_.mol_weight as mol3_1_ from external_id externalid0_ inner join compound compound2_ on externalid0_.inchi_key=compound2_.id cross join synonym synonym1_ where externalid0_.value='160650082' and externalid0_.name='Pubchem SID' and externalid0_.inchi_key=synonym1_.inchi_key limit 10

//-------------------conv id to inchikey-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct externalid0_.inchi_key as col_0_0_, compound1_.id as col_1_0_, 'InChIKey' as col_2_0_, '160650082' as col_3_0_, 'Pubchem SID' as col_4_0_, compound2_.id as id1_, compound2_.inchi_code as inchi2_1_, compound2_.mol_weight as mol3_1_ from external_id externalid0_ inner join compound compound2_ on externalid0_.inchi_key=compound2_.id cross join compound compound1_ where externalid0_.value='160650082' and externalid0_.name='Pubchem SID' and externalid0_.inchi_key=compound1_.id limit 10

//-------------------conv id 2 inchi code-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct externalid0_.inchi_key as col_0_0_, compound1_.inchi_code as col_1_0_, 'InChI Code' as col_2_0_, '160650082' as col_3_0_, 'Pubchem SID' as col_4_0_, compound2_.id as id1_, compound2_.inchi_code as inchi2_1_, compound2_.mol_weight as mol3_1_ from external_id externalid0_ inner join compound compound2_ on externalid0_.inchi_key=compound2_.id cross join compound compound1_ where externalid0_.value='160650082' and externalid0_.name='Pubchem SID' and externalid0_.inchi_key=compound1_.id limit 10

//-------------------conv id 2 id-------------------
select distinct this_.name as y0_ from external_id this_
select distinct this_.name as y0_ from external_id this_

select count(*) as y0_ from compound this_
select count(*) as y0_ from synonym this_
select count(*) as y0_ from external_id this_

select distinct externalid0_.inchi_key as col_0_0_, externalid1_.value as col_1_0_, 'MMDB' as col_2_0_, '160650082' as col_3_0_, 'Pubchem SID' as col_4_0_, compound2_.id as id1_, compound2_.inchi_code as inchi2_1_, compound2_.mol_weight as mol3_1_ from external_id externalid0_ inner join compound compound2_ on externalid0_.inchi_key=compound2_.id cross join external_id externalid1_ where externalid0_.inchi_key=externalid1_.inchi_key and externalid0_.value='160650082' and externalid0_.name='Pubchem SID' and externalid1_.name='MMDB' limit 10

