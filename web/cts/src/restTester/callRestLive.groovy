//http-client dependency
@Grab(group = 'org.codehaus.groovy.modules.http-builder', module = 'http-builder', version = '0.7.2')

import groovyx.net.http.*

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.POST

//variable definitions
def path = ""
def http = new HTTPBuilder('http://cts.fiehnlab.ucdavis.edu/')
def postBody = [inchicode: 'InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1'] // will be url-encoded
def inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
def molDef = [mol: "\n" +
		"5950\n" +
		"  -OEChem-06031415072D\n" +
		"\n" +
		" 13 12  0     1  0  0  0  0  0999 V2000\n" +
		"    5.1350   -0.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.2690    1.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.5369    0.2500    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030   -0.2500    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030   -1.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.2690    0.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030    0.3700    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.7830   -1.2500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030   -1.8700    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.0230   -1.2500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.0000   -0.0600    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.5369    0.8700    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    5.6720    0.0600    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"  1  6  1  0  0  0  0\n" +
		"  1 13  1  0  0  0  0\n" +
		"  2  6  2  0  0  0  0\n" +
		"  4  3  1  6  0  0  0\n" +
		"  3 11  1  0  0  0  0\n" +
		"  3 12  1  0  0  0  0\n" +
		"  4  5  1  0  0  0  0\n" +
		"  4  6  1  0  0  0  0\n" +
		"  4  7  1  0  0  0  0\n" +
		"  5  8  1  0  0  0  0\n" +
		"  5  9  1  0  0  0  0\n" +
		"  5 10  1  0  0  0  0\n" +
		"M  END\n"]

//Testing POST: inchiToinchikey...
println "Testing POST: inchiCodeToInchiKey..."
path = "service/inchicodetoinchikey"
http.request(POST) {
	uri.path = path
	requestContentType = JSON
	body = [inchicode: 'InChI=1S/C20H32O3/c1-2-3-4-5-9-12-15-18-19(23-18)16-13-10-7-6-8-11-14-17-20(21)22/h6,8-10,12-13,18-19H,2-5,7,11,14-17H2,1H3,(H,21,22)/b8-6+,12-9+,13-10+']

	response.success = {resp, json ->
		println "request URL: ${http.uri}$path"
		println "POST response status: ${resp.statusLine}"
		println "data:\n$json\n"

		printHeaders(resp)
	}
}
println "END POST inchiToinchikey\n\n"

//Testing POST: inchiToMol...
println "Testing POST: inchiToMol..."
path = "service/inchitomol"
http.request(POST) {
	uri.path = path
	requestContentType = JSON
	body = postBody

	response.success = {resp, json ->
		println "request URL: ${http.uri}$path"
		println "POST response status: ${resp.statusLine}"
		println "data:\n$json\n"

		printHeaders(resp)
	}
}
println "END POST inchiToMol\n\n"

//Testing POST: molToInchi...
println "Testing POST: molToInchi..."
path = "service/moltoinchi"
http.request(POST) {
	uri.path = path
	requestContentType = JSON
	body = molDef

	response.success = {resp, json ->
		println "request URL: ${http.uri}$path"
		println "POST response status: ${resp.statusLine}"
		println "data:\n$json\n"

		printHeaders(resp)
	}
}
println "END POST molToInchi\n\n"

//Testing GET: inchiKeyToMol...
println "Testing GET: inchiKeyToMol..."
path = "service/inchikeytomol/$inchikey"
http.get(path: path,
		contentType: JSON) {resp, reader ->
	println "request URL: ${http.uri}$path"
	println "response status: ${resp.statusLine}"
	println "response data: \n $reader\n"

	printHeaders(resp)
}
println "END GET inchiKeyToMol\n\n"

//Testing GET: inchiKeyToMol...
println "Testing GET: fromValues..."
path = "service/conversion/fromValues"
http.get(path: path,
		contentType: JSON) {resp, reader ->
	println "request URL: ${http.uri}$path"
	println "response status: ${resp.statusLine}"
	println "response data: \n $reader\n"

	printHeaders(resp)
}
println "END GET fromValues\n\n"

//Testing GET: inchiKeyToMol...
println "Testing GET: toValues..."
path = "service/conversion/toValues"
http.get(path: path,
		contentType: JSON) {resp, reader ->
	println "request URL: ${http.uri}$path"
	println "response status: ${resp.statusLine}"
	println "response data: \n $reader\n"

	printHeaders(resp)
}
println "END GET toValues\n\n"

//Testing GET: convert...
println "Testing GET: convert..."
path = "service/convert/chemical name/inchikey/alanine"
http.get(path: path,
		contentType: JSON) {resp, reader ->
	println "request URL: ${http.uri}$path"
	println "response status: ${resp.statusLine}"
	println "response data: \n $reader\n"

	printHeaders(resp)
}
println "END GET convert\n\n"

//Testing GET: convert bad...
println "Testing GET: convert bad value..."
path = "service/convert/chemical name/inchikey/&lt;i&gt;N&lt;/i&gt;-acetylmuramoyl-L-alanyl-D-isoglutaminyl-N-(&beta;-D-asparatyl)-L-lysyl-D-alanyl-D-alanine-diphosphoundecaprenyl-&lt;i&gt;N&lt;/i&gt;-acetylglucosamine"
http.get(path: path,
		contentType: JSON) {resp, reader ->
	println "request URL: ${http.uri}$path"
	println "response status: ${resp.statusLine}"
	println "response data: \n $reader\n"

	printHeaders(resp)
}
println "END GET convert bad value\n\n"

def printHeaders(HttpResponseDecorator resp) {
	println "HEADERS:\n"
	resp.headers.each {h ->
		println h
	}
}
