import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import edu.ucdavis.fiehnlab.chemify.Configuration
import edu.ucdavis.fiehnlab.chemify.configuration.SpringConfiguration
@Grab(group = 'org.codehaus.groovy.modules.http-builder', module = 'http-builder', version = '0.7.2')

import groovyx.net.http.*

import static groovyx.net.http.ContentType.JSON
import static org.junit.Assert.assertEquals

def http = new HTTPBuilder('http://localhost:8080/cts/')
def http2 = new HTTPBuilder('http://cts.fiehnlab.ucdavis.edu/')

println "Checking GET: identify config..."
String local = "";
String live = "";
http.get(path: "chemify/rest/identify",
		contentType: JSON) { resp, reader ->
	println "response status: ${resp.statusLine}"
	println "headers: \n${resp.headers.each { println " -\t ${it.name}:${it.value}" }}"
	println "response data: \n $reader\n"

	local = reader
}

http2.get(path: "chemify/rest/identify",
		contentType: JSON) { resp, reader ->
	println "response status: ${resp.statusLine}"
	println "headers: \n${resp.headers.each { println " -\t ${it.name}:${it.value}" }}"
	println "response data: \n $reader\n"

	live = reader
}

Gson gson = new Gson();
JsonReader reader = new JsonReader(new StringReader(local));
reader.setLenient(true);
Configuration o1 = gson.fromJson(reader, SpringConfiguration);

reader = new JsonReader(new StringReader(live));
reader.setLenient(true);
Configuration o2 = gson.fromJson(reader, SpringConfiguration);

assertEquals("Equals? ", o1, o2);

println "END GET identify config\n\n"
