@Grab(group = 'org.codehaus.groovy.modules.http-builder', module = 'http-builder', version = '0.7.2')

import groovyx.net.http.*

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.POST

final String BASE_URL="http://localhost:8888/"

def http = new HTTPBuilder(BASE_URL)
def postBody = [inchicode: 'InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)/t2-/m0/s1'] // will be url-encoded
def inchikey = "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
def molDef = [mol: "\n" +
		"5950\n" +
		"  -OEChem-06031415072D\n" +
		"\n" +
		" 13 12  0     1  0  0  0  0  0999 V2000\n" +
		"    5.1350   -0.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.2690    1.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.5369    0.2500    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030   -0.2500    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030   -1.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.2690    0.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030    0.3700    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.7830   -1.2500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    3.4030   -1.8700    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    4.0230   -1.2500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.0000   -0.0600    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    2.5369    0.8700    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"    5.6720    0.0600    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n" +
		"  1  6  1  0  0  0  0\n" +
		"  1 13  1  0  0  0  0\n" +
		"  2  6  2  0  0  0  0\n" +
		"  4  3  1  6  0  0  0\n" +
		"  3 11  1  0  0  0  0\n" +
		"  3 12  1  0  0  0  0\n" +
		"  4  5  1  0  0  0  0\n" +
		"  4  6  1  0  0  0  0\n" +
		"  4  7  1  0  0  0  0\n" +
		"  5  8  1  0  0  0  0\n" +
		"  5  9  1  0  0  0  0\n" +
		"  5 10  1  0  0  0  0\n" +
		"M  END\n"]

println "Checking GET: inchiKeyToMol..."
http.get(path: "service/inchikeytomol/$inchikey",
		contentType: JSON) { resp, reader ->
	println "response status: ${resp.statusLine}"
	println "headers: \n${resp.headers.each { println " -\t ${it.name}:${it.value}" }}"
	println "response data: \n $reader\n"
}
println "END GET inchiKeyToMol\n\n"

println "Testing POST: inchiToMol..."
http.request(POST) {
	uri.path = "service/inchitomol"
	requestContentType = JSON
	body = postBody

	response.success = { resp, json ->
		println "POST response status: ${resp.statusLine}"

		println "data:\n$json\n"
	}
}
println "END POST inchiToMol\n\n"

println "Testing POST: molToInchi..."
http.request(POST) {
	uri.path = "service/moltoinchi"
	requestContentType = JSON
	body = molDef

	response.success = { resp, json ->
		println "POST response status: ${resp.statusLine}"

		println "data:\n$json\n"
	}
}
println "END POST molToInchi\n\n"
