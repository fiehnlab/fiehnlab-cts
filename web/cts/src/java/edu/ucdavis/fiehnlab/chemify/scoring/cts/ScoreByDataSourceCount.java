package edu.ucdavis.fiehnlab.chemify.scoring.cts;

import edu.ucdavis.fiehnlab.chemify.Prioritize;
import edu.ucdavis.fiehnlab.chemify.Priority;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * <p/>
 * This class scores by the data source count, as defined in the cts
 */
public class ScoreByDataSourceCount extends AbstractScorer {

	public ScoreByDataSourceCount() {
		super();
	}

	public ScoreByDataSourceCount(ApplicationContext context) {
		super(context);
	}

	public String getDescription() {
		return "this method scores the result by how often has it been found in a cts datasource";
	}

	public Priority getPriority() {
		return Priority.MEDIUM;
	}

	public int compareTo(Prioritize prioritize) {
		return getPriority().compareTo(prioritize.getPriority());
	}

	public String getScoringMethod() {
		return "popularity";
	}
}
