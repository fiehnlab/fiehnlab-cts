package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 2:33 PM
 */
public class CTSHMDBIdentify extends AbstractCTSConvertIdentify implements Patternized {

	public CTSHMDBIdentify() {
		super(Priority.PATTERN_BASED_APPROACH);
	}

	@Override
	protected String getFromIdentifier() {
		return "Human Metabolome Database";
	}

	public String getPattern() {
		return CommonPatterns.HMDB_PATTERN;
	}
}
