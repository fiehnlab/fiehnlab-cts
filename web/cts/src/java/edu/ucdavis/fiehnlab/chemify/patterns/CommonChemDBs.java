package edu.ucdavis.fiehnlab.chemify.patterns;

public class CommonChemDBs {

	/**
	 * url for cas
	 */
	public final static String CAS_URL = "http://www.cas.org/";
	public final static String CAS_NAME = "CAS";

	/**
	 * url for kegg
	 */
	public final static String KEGG_URL = "http://www.genome.jp/kegg/";
	public final static String KEGG_NAME = "KEGG";

	/**
	 * url for lipid maps
	 */
	public final static String LIPID_MAPS_URL = "http://www.lipidmaps.org/data/structure/LMSDSearch.php";
	public final static String LIPID_MAP_NAME = "LMSD";

	/**
	 * url for hmdb
	 */
	public final static String HMDB_URL = "http://www.hmdb.ca/";
	public final static String HMDB_NAME = "HMDB";

	/**
	 * url for pubchem
	 */
	public final static String PUBCHEM_URL = "http://pubchem.ncbi.nlm.nih.gov/";
	public final static String PUBCHEM_SID_NAME = "PubChem SID";
	public final static String PUBCHEM_CID_NAME = "PubChem CID";

	/**
	 * url for NCGC
	 */
	public final static String NCGC_URL = "http://ncgc.nih.gov";
	public final static String NCGC_NAME = "NCGC";

	/**
	 * url for aldrich
	 */
	public final static String ALDRICH_URL = "http://www.sigmaaldrich.com";
	public final static String ALDRICH_NAME = "ALDRICH";

	/**
	 * url for sigma
	 */
	public final static String SIGMA_URL = "http://www.sigmaaldrich.com";
	public final static String SIGMA_NAME = "SIGMA";

	/**
	 * url for fluka
	 */
	public final static String FLUKA_URL = "http://www.sigmaaldrich.com";
	public final static String FLUKA_NAME = "FLUKA";

	/**
	 * url for riedel
	 */
	public final static String RIEDEL_URL = "http://www.riedeldehaen.com/int/products.html";
	public final static String RIEDEL_NAME = "RIEDEL";

	/**
	 * url for chebi
	 */
	public final static String CHEBI_URL = "http://www.ebi.ac.uk/chebi/";
	public final static String CHEBI_NAME = "CHEBI";

	/**
	 * url for hsdb
	 */
	public final static String HSDB_URL = "http://toxnet.nlm.nih.gov/";
	public final static String HSDB_NAME = "HSDB";
	
	/**
	 * url for CHEMBL
	 */
	public final static String CHEMBL_URL = "";
	public final static String CHEMBL_NAME = "CHEMBL";

	/**
	 * url for CHEMBL
	 */
	public final static String CHEMSPIDER_URL = "http://www.chemspider.com/Chemical-Structure.@@ID@@.html";
	public final static String CHEMSPIDER_NAME = "ChemSpider";
}
