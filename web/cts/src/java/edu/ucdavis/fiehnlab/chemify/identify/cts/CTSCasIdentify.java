package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import edu.ucdavis.fiehnlab.cts.PatternHelper;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 2:23 PM
 */
public class CTSCasIdentify extends AbstractCTSConvertIdentify implements Patternized {
	@Override
	protected String getFromIdentifier() {
		return "CAS";
	}

	public String getPattern() {
		return PatternHelper.CAS_PATTERN;
	}

	public CTSCasIdentify() {
		super(Priority.PATTERN_BASED_APPROACH);
	}
}
