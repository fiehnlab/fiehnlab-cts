package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import edu.ucdavis.fiehnlab.cts.ConstantHelper;
import edu.ucdavis.fiehnlab.cts.PatternHelper;

/**
 * Created by diego on 4/14/14.
 */
public class CTSInverseKeggIdentify extends AbstractCTSConvertIdentify implements Patternized {

	@Override
	protected final String getToIdentifier() {
		return "KEGG";
	}

	@Override
	protected String getFromIdentifier() {
		return ConstantHelper.NAMES_INCHI_KEY;
	}

	@Override
	public String getPattern() {
		return PatternHelper.KEGG_PATTERN;
	}

	public CTSInverseKeggIdentify() {
		super(Priority.PATTERN_BASED_APPROACH);
	}
}
