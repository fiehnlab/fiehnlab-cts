package edu.ucdavis.fiehnlab.chemify.scoring.kegg;

import edu.ucdavis.fiehnlab.chemify.Prioritize;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.scoring.cts.AbstractScorer;
import edu.ucdavis.fiehnlab.cts.ConstantHelper;
import org.springframework.context.ApplicationContext;

/**
 * Created by diego on 10/17/14.
 */
public class ScoreByCTSKeggPresence extends AbstractScorer {

	public ScoreByCTSKeggPresence() {
		super();
	}

	public ScoreByCTSKeggPresence(ApplicationContext context) {
		super(context);
	}

	@Override
	public String getScoringMethod() {
		return ConstantHelper.SCORING_KEGG_PRESENCE;
	}

	/**
	 * am arbitrary number. The higher the value the higher the priority
	 *
	 * @return
	 */
	@Override
	public Priority getPriority() {
		return Priority.HIGH;
	}

	@Override
	public int compareTo(Prioritize prioritize) {
		return this.getPriority().compareTo(prioritize.getPriority());
	}

	/**
	 * the description of this object
	 *
	 * @return
	 */
	@Override
	public String getDescription() {
		return "Scores the hit by looking for a kegg id, if its found the score is 1 if not the score is 0";
	}
}
