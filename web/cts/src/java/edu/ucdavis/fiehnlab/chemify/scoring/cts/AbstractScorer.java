package edu.ucdavis.fiehnlab.chemify.scoring.cts;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Scored;
import edu.ucdavis.fiehnlab.chemify.Scoring;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone;
import edu.ucdavis.fiehnlab.cts.ScoredValue;
import edu.ucdavis.fiehnlab.cts.ScoringService;
import org.apache.log4j.Logger;
import org.codehaus.groovy.grails.commons.spring.GrailsApplicationContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diego on 5/15/14.
 */
public abstract class AbstractScorer implements Scoring, ApplicationContextAware {
	protected Logger logger = Logger.getLogger(this.getClass());

	ScoringService scoringService;
	protected GrailsApplicationContext grailsApplicationContext;

	protected AbstractScorer() {
	}

	protected AbstractScorer(ApplicationContext context) {
		this.scoringService = (ScoringService) context.getBean("scoringService");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.grailsApplicationContext = (GrailsApplicationContext) applicationContext;
		this.scoringService = (ScoringService) grailsApplicationContext.getBean("scoringService");
	}

	public void setScoringService(ScoringService service) {
		this.scoringService = service;
	}

	/**
	 * connect to the cts and calculate the relative score for each hit. Return a sorted list by highest score. The value for scoring should be between 0 and 1.0
	 *
	 * @param input
	 * @return
	 */
	public List<Scored> score(List<? extends Hit> input, String searchTerm) {
		ArrayList<Scored> scoredResult = new ArrayList<Scored>();
		List<String> inchis = new ArrayList<String>();

		for (Hit hit : input) {
			inchis.add(hit.getInchiKey());
		}

		List<ScoredValue> scoredValues = scoringService.score(this.getScoringMethod(), inchis);

		// create a scoredHit with hit data from input and score data from scoredValues
		for (ScoredValue value : scoredValues) {
			for (Hit hit : input) {
				if (value.getValue().equals("'" + hit.getInchiKey() + "' is not a valid inchikey")) {
					scoredResult.add(new ScoredHit(new HitImpl(value.getValue(), hit.getUsedAlgorithm(), hit.getSearchTerm(), hit.getOriginalTerm()), new NoScoringDone(), 0.0D));
				} else if (hit.getInchiKey().equals(value.getValue())) {
					scoredResult.add(new ScoredHit(hit, this, value.getScore()));
				}
			}
		}

		//done
		return scoredResult;
	}

	public abstract String getScoringMethod();

	public abstract String getDescription();


	class Data {
		private String searchTerm;
		private String from;
		List<ScoredValue> result;

		public String toString() {
			return this.searchTerm + ", " + this.from + " [" + result.toString() + "]";
		}

		class ScoredValue {
			private String InChIKey;
			private Double score;

			public String getInChIKey() {
				return this.InChIKey;
			}

			public Double getScore() {
				return this.score;
			}

			public String toString() {
				return this.InChIKey + "(" + this.score + ")";
			}
		}
	}
}
