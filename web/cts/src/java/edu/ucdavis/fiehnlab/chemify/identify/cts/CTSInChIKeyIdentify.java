package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import edu.ucdavis.fiehnlab.cts.ConstantHelper;
import edu.ucdavis.fiehnlab.cts.PatternHelper;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 2:02 PM
 * <p/>
 * basically builds a hit against the system, uses the inchi key as search term
 */
public class CTSInChIKeyIdentify extends AbstractCTSConvertIdentify implements Patternized {
	@Override
	protected String getFromIdentifier() {
		return ConstantHelper.NAMES_INCHI_KEY;
	}

	/**
	 * this only checks for inchi keys and nothing else
	 *
	 * @return
	 */
	public String getPattern() {
		return PatternHelper.STD_INCHI_KEY_PATTERN;
	}


	public CTSInChIKeyIdentify() {
		super(Priority.PATTERN_BASED_APPROACH);
	}
}
