package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 7:19 PM
 * <p/>
 * uses the cts cdk service to identify compounds
 */
public class CTSConvertIdentify extends AbstractCTSConvertIdentify implements Patternized {

	public CTSConvertIdentify() {
		super(Priority.LOW);
	}

	public CTSConvertIdentify(ApplicationContext context) {
		super(context);
	}

	@Override
	protected String getFromIdentifier() {
		return "Chemical Name";
	}

	public String getPattern() {
		return ".*";
	}
}
