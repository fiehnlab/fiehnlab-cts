package edu.ucdavis.fiehnlab.chemify.scoring.cts;

import edu.ucdavis.fiehnlab.chemify.Prioritize;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.cts.ConstantHelper;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * <p/>
 * This class scores by the data source count, as defined in the cts
 */
public class ScoreByBiologicalCount extends AbstractScorer {

	public ScoreByBiologicalCount() {
		super();
	}

	public ScoreByBiologicalCount(ApplicationContext context) {
		super(context);
	}

	public String getDescription() {
		return "this method scores the result by how often has it been found in a cts datasource with biological meaning";
	}

	public Priority getPriority() {
		return Priority.HIGH;
	}

	public int compareTo(Prioritize prioritize) {
		return getPriority().compareTo(prioritize.getPriority());
	}

	public String getScoringMethod() {
		return ConstantHelper.SCORING_BIO;
	}
}
