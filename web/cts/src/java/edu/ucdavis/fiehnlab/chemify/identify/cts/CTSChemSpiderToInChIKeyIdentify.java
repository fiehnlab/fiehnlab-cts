package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;

/**
 * Created with IntelliJ IDEA.
 * User: jennifervannier
 * Date: 10/9/13
 * Time: 3:11 PM
 */
public class CTSChemSpiderToInChIKeyIdentify extends AbstractCTSConvertIdentify implements Patternized {

	@Override
	protected String getFromIdentifier() {
		return "ChemSpider";
	}

	public CTSChemSpiderToInChIKeyIdentify() {
		super(Priority.VERY_LOW);
	}

	/**
	 * we only allow numbers for this class
	 *
	 * @return
	 */
	public String getPattern() {
		return "[0-9]*";
	}
}
