package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyAlgorithmWrongImplementationException;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.cts.ConstantHelper;
import edu.ucdavis.fiehnlab.cts.ConversionService;
import edu.ucdavis.fiehnlab.cts.ScoredValue;
import edu.ucdavis.fiehnlab.cts.exceptions.NullCTSServiceException;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/25/13
 * Time: 12:16 AM
 * <p/>
 * basic helper class to support more than just one to one conversion. Sub classes will specify what we actually want to cdk
 */
public abstract class AbstractCTSConvertIdentify extends AbstractIdentify {

	ConversionService conversionService;
	ApplicationContext context;

	protected AbstractCTSConvertIdentify() {
		super(Priority.LOW);
	}

	protected AbstractCTSConvertIdentify(Priority priority) {
		super(priority);
	}

	protected AbstractCTSConvertIdentify(ApplicationContext context) {
		this();
		this.context = context;
		this.conversionService = (ConversionService) context.getBean("conversionService");
	}

	public void setConversionService(ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	/**
	 * from where do we want to cdk
	 *
	 * @return
	 */
	protected abstract String getFromIdentifier();

	/**
	 * where to do we want to cdk
	 *
	 * @return
	 */
	protected String getToIdentifier() {
		return ConstantHelper.NAMES_INCHI_KEY;
	}

	/**
	 * uses the cts cdk service to identify a compounds or an empty list.
	 *
	 * @param searchTerm
	 * @return
	 */
	final protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException, NullCTSServiceException {

		List<Hit> result = new ArrayList<Hit>();
		List<Map> preresult;
		try {
			if(null == conversionService) {
				throw new NullCTSServiceException("Conversion Service can't be null.");
			}

			if(this.getFromIdentifier().equals(ConstantHelper.NAMES_CHEMICAL_NAME)) {
				preresult = (ArrayList<Map>) conversionService.querySynonymByName(searchTerm, this.getToIdentifier());
			} else {
				preresult = (ArrayList<Map>) conversionService.convert(this.getFromIdentifier(), searchTerm, this.getToIdentifier());
			}

		} catch(NullPointerException e) {
			return Collections.emptyList();
		}

		//parse preresults and convert to list of hits
		for (Map<String, Object> item : preresult) {
			if (item.containsKey(ConstantHelper.NAMES_INCHI_KEY)) {
				List<ScoredValue> inchis = (ArrayList<ScoredValue>) item.get(ConstantHelper.NAMES_INCHI_KEY);
				if (!inchis.isEmpty() && !inchis.get(0).getValue().equals("nothing found") && !inchis.get(0).getValue().contains("is not a valid inchikey")) {
					for (ScoredValue value : inchis) {
						result.add(new HitImpl(value.getValue(), this, searchTerm, original));
					}
				}
			} else {
				throw new ChemifyAlgorithmWrongImplementationException("Error analyzing the identification results: pre-results map doesn't have key 'InChIKey'");
			}
		}

		return result;
	}


	public String getDescription() {
		return "this method utilizes the CTS Webservice to identify a search term";
	}
}
