package edu.ucdavis.fiehnlab.chemify.identify.cts;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import edu.ucdavis.fiehnlab.cts.PatternHelper;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 2:15 PM
 * <p/>
 * convert kegg id's into inchi keys
 */
public class CTSKeggIdentify extends AbstractCTSConvertIdentify implements Patternized {
	@Override
	protected String getFromIdentifier() {
		return "KEGG";
	}

	public String getPattern() {
		return PatternHelper.KEGG_PATTERN;
	}

	public CTSKeggIdentify() {
		super(Priority.PATTERN_BASED_APPROACH);
	}
}
