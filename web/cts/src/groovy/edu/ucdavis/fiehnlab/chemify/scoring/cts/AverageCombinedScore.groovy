package edu.ucdavis.fiehnlab.chemify.scoring.cts

import edu.ucdavis.fiehnlab.chemify.*
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit

/**
 * calculates the combined average score
 * Created by wohlgemuth on 10/16/14.
 */
public final class AverageCombinedScore extends AbstractCombinedScore {

	@Override
	List<Scored> determineBestResult(List<List<Scored>> resultSet) {
		List<Scored> finalResult = []

		//how many items do we have
		int items = resultSet.get(0).size()

		//go over each of our items in each scoring result
		for (int x = 0; x < items; x++) {
			double sumOfScores = 0

			//take item x and calculate the sum of all scores of this item in all it's lists
			for (int i = 0; i < resultSet.size(); i++) {
				//sum score from item x in list i with the sum of scores for this item
				sumOfScores = sumOfScores + resultSet[i][x].score
			}

			//calculate the average score for this item
			double average = sumOfScores / resultSet.size()

			//create a new scoring object
			Scored scored = new ScoredHit(resultSet[0][x], this, average)

			//add this object to the final result
			finalResult.add(scored)
		}

		return finalResult
	}

	public AverageCombinedScore(Scoring... scorers) {
		super(scorers)
	}

	/**
	 * the description of this object
	 * @return
	 */
	@Override
	String getDescription() {
		return "we average the result of every scoring algoritm here"
	}

	/**
	 * am arbitrary number. The higher the value the higher the priority
	 *
	 * @return
	 */
	@Override
	Priority getPriority() {
		return Priority.CALCULATION_BASED
	}
}