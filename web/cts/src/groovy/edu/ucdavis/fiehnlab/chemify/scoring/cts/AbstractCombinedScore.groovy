package edu.ucdavis.fiehnlab.chemify.scoring.cts

import edu.ucdavis.fiehnlab.chemify.*
import edu.ucdavis.fiehnlab.chemify.enhance.chemspiderMetric.ChemSpiderDataSourceCountEnhancement
import edu.ucdavis.fiehnlab.chemify.enhance.chemspiderMetric.ChemSpiderPubmedHitsEnhancement
import edu.ucdavis.fiehnlab.chemify.enhance.chemspiderMetric.ChemSpiderReferencesCountEnhancement
import edu.ucdavis.fiehnlab.cts.Compound
import org.apache.log4j.Logger

/**
 * Created by wohlgemuth on 10/16/14.
 */
public abstract class AbstractCombinedScore implements Scoring {
	private static Logger logger = Logger.getLogger(AbstractCombinedScore.class)
	Scoring[] scorers;

	/**
	 * the score methods converts the result into a scored hit to reduce the amount of data output
	 * @param result
	 * @return
	 */

	@Override
	final List<Scored> score(List<? extends Hit> hits, String searchTerm) {

		//check if input is enhanced with chemspider metrics
		for (Hit hit : hits) {
			if (((Enhanced)hit).enhancements.size() < 1) { // enhance input with chemspider metrics
				enhance((Enhanced) hit);
			}
		}


		List<List<Scored>> results = []

		for (Scoring scorer : scorers) {
			List<Scored> tmpResult = []

			tmpResult = scorer.score(hits, searchTerm)

			//doing the following generates exception since score value must be 0 < n < 1
			results.add(tmpResult)
		}
		return determineBestResult(results)
	}

	/**
	 * takes our incoming list of result lists and computes our final scored list out of it
	 * @param resultSet
	 * @return
	 */
	abstract List<Scored> determineBestResult(List<List<Scored>> resultSet)

	public AbstractCombinedScore(Scoring... scorer) {
		this.scorers = scorer;
	}

	@Override
	int compareTo(Prioritize prioritize) {
		return this.getPriority().compareTo(prioritize.priority)
	}

	protected final void enhance(Enhanced hit) {
		Map<String, Integer> scores = new HashMap<String, Integer>();

		try {
			def cmp = Compound.get(hit.inchiKey)
			if (cmp == null) {
				setBlankScores(scores)
			} else {
				def metrics = cmp.dataSourcesCount + cmp.referencesCount + cmp.referencesCount
//				if (metrics > 0) {
					logger.debug "... getting metrics from database :D "
					scores["DataSourcesCount"] = cmp.dataSourcesCount
					scores["ReferencesCount"] = cmp.referencesCount
					scores["PubmedHits"] = cmp.pubmedHits
//				} else {
//					logger.debug "...getting metrics from chemspider >:( "
//					scores = ChemspiderRester.getScoringMetricsForInchiKey(hit.getInchiKey())
//
//					cmp.dataSourcesCount = scores["DataSourcesCount"]
//					cmp.referencesCount = scores["ReferencesCount"]
//					cmp.pubmedHits = scores["PubmedHits"]
//
//					if (cmp.validate()) {
//						def s = cmp.save(flush: true)
//					} else {
//						logger.error cmp.errors
//					}
//				}
			}

			hit.addEnhancement(new ChemSpiderDataSourceCountEnhancement(scores["DataSourcesCount"]));
			hit.addEnhancement(new ChemSpiderReferencesCountEnhancement(scores["ReferencesCount"]));
			hit.addEnhancement(new ChemSpiderPubmedHitsEnhancement(scores["PubmedHits"]));
		} catch (Exception e) {
			logger.debug("error getting metrics for $hit.inchiKey", e)
			setBlankScores(scores);
		}
	}

	private void setBlankScores(Map<String, Integer> scores) {
		scores.put("PubmedHits", 0);
		scores.put("DataSourcesCount", 0);
		scores.put("ReferencesCount", 0);
	}
}
