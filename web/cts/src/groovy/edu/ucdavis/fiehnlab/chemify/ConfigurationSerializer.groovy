package edu.ucdavis.fiehnlab.chemify;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 11/13/13
 * Time: 5:05 PM
 */
public class ConfigurationSerializer implements JsonSerializer<Configuration> {
    @Override
    public JsonElement serialize(Configuration src, Type typeOfSrc, JsonSerializationContext context) {
        return serialize(src);
    }

    public JsonElement serialize(Configuration src) {
        JsonObject obj = new JsonObject();

        obj.addProperty("class", src.class.name)
	    obj.addProperty("searchTermMinQuality", src.searchTermMinQuality.doubleValue())
	    obj.addProperty("lipidmapsPath", src.lipidmapsPath)

        addFilters(src, obj);
        addIdentifies(src, obj);
        addModifier(src, obj);
        addMultiplexer(src,obj);
        addScoring(src,obj);


        return obj;
    }

    /**
     * adds the scoring algorithms
     * @param src
     * @param obj
     */
    private void addScoring(Configuration src, JsonObject obj) {
        JsonArray filters = new JsonArray();

        for (Scoring filter : src.getScoringAlgorithms()) {
            JsonObject object = new JsonObject();
            object.addProperty("class", filter.getClass().getName());

            addPriority(object, filter);
            addDescription(object, filter);
            filters.add(object);
        }

        obj.add("scoring", filters);
    }

    /**
     * adds modifier
     *
     * @param src
     * @param obj
     */
    private void addModifier(Configuration src, JsonObject obj) {
        JsonArray filters = new JsonArray();

        for (Modify filter : src.getModifications()) {
            JsonObject object = new JsonObject();
            object.addProperty("class", filter.getClass().getName());

            addPriority(object, filter);
            addDescription(object, filter);
            filters.add(object);
        }

        obj.add("modifications", filters);
    }

    private void addMultiplexer(Configuration src, JsonObject obj) {
        JsonArray filters = new JsonArray();

        for (Multiplexer filter : src.getMultiplexer()) {
            JsonObject object = new JsonObject();
            object.addProperty("class", filter.getClass().getName());

            addDescription(object, filter);
            filters.add(object);
        }

        obj.add("multiplexer", filters);
    }


    private void addPriority(JsonObject object, Prioritize filter) {
        object.addProperty("priority", filter.getPriority().toString());
    }

    /**
     * adds identifier objects
     *
     * @param src
     * @param obj
     */
    private void addIdentifies(Configuration src, JsonObject obj) {
        JsonArray algorythms = new JsonArray();

        for (Identify algorythm : src.getAlgorithms()) {
            JsonObject object = new JsonObject();
            object.addProperty("class", algorythm.getClass().getName());

            addPriority(object, algorythm);
            addDescription(object, algorythm);
	        algorythms.add(object);

//            if (algorythm.hasConfiguration()) {
//                object.add("configuration", serialize(algorythm.getConfiguration()));
//            }
        }

        obj.add("algorithms", algorythms);
    }

    /**
     * serializes the filters
     *
     * @param src
     * @param obj
     */
    private void addFilters(Configuration src, JsonObject obj) {
        JsonArray filters = new JsonArray();

        for (Filter filter : src.getFilters()) {
            JsonObject object = new JsonObject();
            object.addProperty("class", filter.getClass().getName());
            addPriority(object, filter);
            addDescription(object, filter);
            filters.add(object);
        }

        obj.add("filters", filters);
    }

    /**
     * serilizes the descriptions
     *
     * @param object
     * @param filter
     */
    private void addDescription(JsonObject object, Describe filter) {
        object.addProperty("description", filter.getDescription());
    }
}
