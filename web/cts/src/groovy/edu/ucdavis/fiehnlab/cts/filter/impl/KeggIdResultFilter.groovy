package edu.ucdavis.fiehnlab.cts.filter.impl

import edu.ucdavis.fiehnlab.cts.filter.AbstractResultFilter

/**
 * Created by diego on 10/8/14.
 */
class KeggIdResultFilter extends AbstractResultFilter {

	public String getAcceptedResultPattern() {
		return "C\\d{5}"
	}
}
