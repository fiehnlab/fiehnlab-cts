package edu.ucdavis.fiehnlab.cts

/**
 * Created by diego on 5/8/14.
 * Holds the system wide used constants
 */
public final class ConstantHelper {
	// scoring algorithms
	public static final String SCORING_BIO = "biological"
	public static final String SCORING_COUNT = "popularity"
	public static final String SCORING_KEGG_PRESENCE = "keggpresence"
	public static final String SCORING_COMBINED_AVERAGE = "combinedagerage"

	//identifiers
	public static final String NAMES_INCHI_KEY = "InChIKey"
	public static final String NAMES_CHEMICAL_NAME = "Chemical Name"

}
