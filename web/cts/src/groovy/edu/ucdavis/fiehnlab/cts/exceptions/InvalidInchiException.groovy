package edu.ucdavis.fiehnlab.cts.exceptions

/**
 * Created by diego on 6/4/2014.
 */
class InvalidInchiException extends IllegalArgumentException {
	public InvalidInchiException(String message) {
		super(message)
	}
}
