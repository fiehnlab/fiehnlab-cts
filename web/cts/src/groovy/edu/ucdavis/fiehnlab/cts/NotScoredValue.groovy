package edu.ucdavis.fiehnlab.cts

/**
 * Created by diego on 5/6/14.
 */
class NotScoredValue extends ScoredValue {

	public NotScoredValue(String value) {
		super(value, 0.0)
	}

	public String toString() {
		return this.value
	}

	public boolean equals(Object other) {
		if (other instanceof NotScoredValue) {
			return this.value.equals(other.value) && this.score.equals(other.score)
		} else {
			return false
		}
	}
}
