package edu.ucdavis.fiehnlab.cts.exceptions

/**
 * Created by diego on 11/3/14.
 */
class NullCTSServiceException extends NullPointerException {
	public NullCTSServiceException(String message) {
		super(message)
	}

	public NullCTSServiceException(String message, Throwable ex) {
		super(message, ex)
	}
}
