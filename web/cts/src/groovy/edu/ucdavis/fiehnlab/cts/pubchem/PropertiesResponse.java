package edu.ucdavis.fiehnlab.cts.pubchem;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by diego on 2/13/2017.
 */
public class PropertiesResponse {
	@SerializedName("PropertyTable")
	public
	PropertyTable propTable;
}

class PropertyTable {
	@SerializedName("Properties")
	public
	List<Property> prop;
}

class Property {
	@SerializedName("CID")
	public
	long cid;
	@SerializedName("MolecularFormula")
	public
	String formula;
	@SerializedName("MolecularWeight")
	public
	Double molWeight;
	@SerializedName("InChI")
	public
	String inchi;
	@SerializedName("IUPACName")
	public
	String iupacName;
	@SerializedName("ExactMass")
	public
	Double exactMass;
}
