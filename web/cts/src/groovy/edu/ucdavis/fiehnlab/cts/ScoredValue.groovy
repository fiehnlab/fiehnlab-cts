package edu.ucdavis.fiehnlab.cts

import groovy.transform.ToString

/**
 * Created by diego on 4/25/14.
 */

@ToString(includeNames = true, includeFields = true, excludes = "metaClass,class")
class ScoredValue implements Comparator<ScoredValue>, Comparable<ScoredValue>, Serializable {
	private Double score = 0.0
	private String value = ""

	public ScoredValue(String value, Double score) {
		super()
		if (value == null) throw new IllegalArgumentException("the value cannot be null")
		if (score == null) throw new IllegalArgumentException("the score cannot be null")
		this.value = value
		this.score = score.round(4)
	}

	public String toString() {
		return "$value (${score >= 0.0 ? score : 'Not Scored'})"
	}

	public String getValue() {
		return value
	}

	public Double getScore() {
		return score
	}

	@Override
	int compareTo(ScoredValue other) {
		return (this.value).compareTo(other.value)
	}

	@Override
	int compare(ScoredValue first, ScoredValue other) {
		return (first).compareTo(other)
	}

	public boolean matches(String pattern) {
		return this.value.matches(pattern)
	}

	public boolean equals(Object other) {
		if (other instanceof ScoredValue) {
			return this.value.equals(other.value) && this.score.equals(other.score)
		} else {
			return false
		}
	}
}
