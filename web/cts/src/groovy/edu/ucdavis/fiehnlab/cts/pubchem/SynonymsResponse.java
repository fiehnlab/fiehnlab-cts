package edu.ucdavis.fiehnlab.cts.pubchem;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by diego on 2/13/2017.
 */
public class SynonymsResponse {
	@SerializedName("InformationList")
	public
	InformationList infoList;
}

class InformationList {
	@SerializedName("Information")
	public
	List<Information> info;
}

class Information {
	@SerializedName("CID")
	long cid;
	@SerializedName("Synonym")
	public
	List<String> synonym;
}
