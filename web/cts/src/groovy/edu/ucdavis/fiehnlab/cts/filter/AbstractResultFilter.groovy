package edu.ucdavis.fiehnlab.cts.filter

/**
 * Created by diego on 10/8/14.
 */
abstract class AbstractResultFilter {

	public boolean filter(String name) {
		return name.matches(getAcceptedResultPattern())
	}

	public boolean find(String name) {
		return name.find(getAcceptedResultPattern())
	}

	protected abstract getAcceptedResultPattern()
}
