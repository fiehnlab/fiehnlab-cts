package edu.ucdavis.fiehnlab.cts.exceptions

/**
 * Created by diego on 6/3/14.
 */
class UnsupportedFormatException extends IOException {
	public UnsupportedFormatException(String message) {
		super(message)
	}
}
