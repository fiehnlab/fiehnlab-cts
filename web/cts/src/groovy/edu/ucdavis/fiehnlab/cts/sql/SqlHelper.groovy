package edu.ucdavis.fiehnlab.cts.sql

import edu.ucdavis.fiehnlab.cts.Queries
import org.apache.log4j.Logger

/**
 * Created by diego on 11/16/2016.
 */
class SqlHelper {
	def logger = Logger.getLogger(this.class)

	public void addQuery(def params, String origin, long speed, String action, Map model) {

		String keywords
		if(params.value?.trim != null && params.value?.trim != "")
			keywords = params.value
		else
			keywords = params.values

		String results = model.results.collect { it as String }.toListString()

		logger.debug(results)

		try {
			def query = new Queries(keywords: keywords, sourceType: params.from, destType: params.to,
					result: results, origin: origin, speed: speed, action: action)
			query.save(flush: true, failOnError: false)

			if (query.hasErrors()) {
				logger.debug(query.errors)
			}
		} catch (Exception ignored){}

	}
}
