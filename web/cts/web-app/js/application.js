if (typeof jQuery != 'undefined') {
	(function ($) {
		$('#spinner').ajaxStart(function () {
			$(this).fadeIn();
		}).ajaxStop(function () {
			$(this).fadeOut();
		}).ajaxError(function () {
			$this.fadeOut();
		});

		$('.wait').attr('display', 'none');

		$('#max').change(function () {
			$(':submit #searchButton').click();
		});

		$(".box > .header:contains('(0)') > span").hide();

		$(".box > .header").hover(function () {
			if (!$(this).hasClass("no-collapse")) {
				$(".box > .header").css('cursor', 'pointer');
			}
		}, function () {
			if (!$(this).hasClass("no-collapse")) {
				$(".box > .header").css('cursor', 'default');
			}
		});

		$(".header").click(function () {
			if (!$(this).hasClass('no-collapse')) {
				$(this).next('span').toggle();
				$(this).toggleClass('ui-corner-top');
				$(this).toggleClass('ui-corner-all');
			}
		});

		$('select[name=from]').change(function () {
			var check = $("input[type=checkbox][value=InChIKey]");
			if ("InChIKey" == this.value) {
				check.attr('oldVal', check.attr('checked'));
				check.attr('checked', false).attr('disabled', true);
			} else {
				check.attr('checked', check.attr('oldVal')).attr('disabled', false).removeAttr('oldVal');
			}
		});

		if ($('option[value=InChIKey]').attr('selected') == "selected") {
			$('input[type=checkbox][value=InChIKey]').attr('checked', false).attr('disabled', true);
		} else {
			$('input[type=checkbox][value=InChIKey]').attr('disabled', false);
		}

		$("[name='convert']").click(function () {
			showWait($('form'));
		});

		$("[name='batchConvert']").click(function () {
			showWait($('form'));
		});

		$('#exportButton').addClass('ui-button ui-state-active ui-corner-all')
				.ajaxStart(function () {
					showWait($('form'));
				})
				.ajaxComplete(function () {
					$('.wait').attr('display', 'none');
				})
				.ajaxError(function () {
					$('.wait').attr('display', 'none');
				});

		$("[name='searchButton']").click(function () {
			showWait($('form'));
		});

		$('#expandIds').click(function () {
			$('#extraIds').slideToggle(500);
			$('#expandIds').toggleClass('ui-corner-bottom');
			return false;
		});

		$('#closePopupLink').click(function () {
			$('#infobox').fadeOut();
		});

		// expands the div with values on batch cdk results
		$("[name='fullIdValues']").hide();

		$("[name='moreValues']").click(function () {
			var key = $(this).attr('id');
			var colname = $(this).attr('colname');
			$("#" + key + "[name=partialValues][colname='" + colname + "']").toggle();
			$("#" + key + "[name=fullIdValues][colname='" + colname + "']").toggle();
			$("#" + key + "[name=partialValues][colname='" + colname + "']score").toggle();
			$("#" + key + "[name=fullIdValues][colname='" + colname + "']score").toggle();
		});

		$("[name='lessValues']").click(function () {
			var key = $(this).attr('id');

			var colname = $(this).attr('colname');
			$("#" + key + "[name=partialValues][colname='" + colname + "']").toggle();
			$("#" + key + "[name=fullIdValues][colname='" + colname + "']").toggle();
			$("#" + key + "[name=partialValues][colname='" + colname + "']score").toggle();
			$("#" + key + "[name=fullIdValues][colname='" + colname + "']score").toggle();
		});

		/* end cts jquery stuff */

		var showWait = function ($form) {
			var w = $form.css('width');
			var h = $form.css('height');
			var top = $form.css('top');
			var left = $form.css('left');
			$('.wait').css('width', w).css('height', h);
			$('.wait').fadeIn();
		};

		$("#extIdTree").jstree({
			"themes": {
				"theme": "classic",
				"dots": false,
				"icons": true
			}
		});

		$("#webserv").accordion();

	})(jQuery);
}

function doPrevAndSubmit(f) {
	var f_max = f.elements["max"];
	var max_index = f_max.selectedIndex;
	var max_val = f_max.options[max_index].value;

	f.elements["offset"].value = Number(f.elements["offset"].value) - Number(max_val);
	if (f.elements["offset"].value < 0) {
		f.elements["offset"].value = Number(0);
	}
	f.submit();
}

function doNextAndSubmit(f) {
	var f_max = f.elements["max"];
	var max_index = f_max.selectedIndex;
	var max_val = Number(f_max.options[max_index].value);

	var offset = Number(f.elements["offset"].value);
	var newoffset = offset + max_val;
	f.elements["offset"].value = newoffset;
//alert("max: " + max_val + " - old offset: " + offset + " - new offset: " + newoffset + "\noffset element: ");
	f.submit();
}

function resetOffsetAndSubmit(f) {
	f.elements["offset"].value = 0;
	f.submit();
}

function imposeMaxLength(Object, MaxLen) {
	if (Object.value.length > MaxLen) {
		alert("you have exceeded the maximum characters allowed for this field\n The maximum value is " + MaxLen);
		Object.value = Object.value.substring(0, MaxLen);
		Object.value = Object.value.substring(0, Object.value.lastIndexOf('\n'))
	}
	return (Object.value.length <= MaxLen);
}
