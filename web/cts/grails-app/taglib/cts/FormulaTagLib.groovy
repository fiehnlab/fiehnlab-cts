package cts

import edu.ucdavis.fiehnlab.cts.ImageService

import java.awt.Color

class FormulaTagLib {

    //our injected image service
    ImageService imageService

    /**
     * renders a simple formula
     */
    def structure = { attrs ->

        try {
            assert attrs.id != null, "please provide a compound id, its needed for the file name"

            assert attrs.inchi != null, "please provide a inchi so we can render it"
            assert attrs.size != null, "please a size"
            assert attrs.color != null, "please provide a background color"

            out << "<img src=\"${imageService.getStructure(attrs.id, attrs.inchi, attrs.size, Color.decode(attrs.color))}\" alt = \"sorry was not able to display structure\"></img>"

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }

    /**
     * renders a simple formula
     */
    def structureThumb = { attrs ->

        try {

            validate(attrs)

            Color color = calculateColor(attrs)
            //log.debug "attrs.color :: "+attrs.color

            out << "<img src=\"${imageService.getThumbmail(attrs.id, attrs.inchi, color)}\" alt = \"sorry was not able to display structure\"></img>"

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }

    /**
     * renders a simple formula
     */
    def structureThumb3D = { attrs ->

        try {

            validate(attrs)

            Color color = calculateColor(attrs)
            //log.debug "attrs.color :: "+attrs.color

            long id = generateRandomId(attrs)

            out << """<div id="${id}" height="${imageService.THUMNAIL_SIZE}" width="${imageService.THUMNAIL_SIZE}"></div>

            <script src="http://cactus.nci.nih.gov/chemical/structure/
                  ${attrs.inchi}/twirl?div_id=${id}"
                    type="text/javascript"></script>"""

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }

    protected double generateRandomId(attrs) {
        return attrs.inchi.hashCode() + new Random().nextFloat() + System.currentTimeMillis()
    }

    /**
     * strips # of the color tag
     * @param attrs
     * @return
     */
    private Color calculateColor(attrs) {
        Color color = Color.WHITE
        if (attrs.color != null) {
            String code = attrs.color.toString()
            if (code.startsWith("#")) {
                code = code.replaceFirst("#", "")
            }
            if (code.startsWith("0x") == false) {
                code = "0x" + code
            }

            color = Color.decode(attrs.color)
        }
        return color
    }

    /**
     * renders a simple formula
     */
    def structureSmall = { attrs ->

        try {
            validate(attrs)
            Color color = calculateColor(attrs)

            out << "<img src=\"${imageService.getSmall(attrs.id, attrs.inchi, color)}\" alt = \"sorry was not able to display structure\"></img>"

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }

    /**
     * renders a simple formula
     */
    def structureSmall3D = { attrs ->

        try {

            validate(attrs)

            Color color = calculateColor(attrs)
            //log.debug "attrs.color :: "+attrs.color


            long id = generateRandomId(attrs)

            out << """<div id="${id}" height="${imageService.SMALL_SIZE}" width="${imageService.SMALL_SIZE}"></div>

              <script src="http://cactus.nci.nih.gov/chemical/structure/
                    ${attrs.inchi}/twirl?div_id=${id}"
                      type="text/javascript"></script>"""

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }

    /**
     * renders a simple formula
     */
	def structureMedium3D = { attrs ->

		try {

			validate(attrs)

			Color color = calculateColor(attrs)
//			log.debug "attrs.color :: ${color}"

			long id = generateRandomId(attrs)

			out << """<div id="${id}" height="${imageService.MEDIUM_SIZE}" width="${imageService.MEDIUM_SIZE}"></div>
				<script src="http://cactus.nci.nih.gov/chemical/structure/${attrs.inchi}/twirl?div_id=${id}" type="text/javascript"></script>"""

        } catch (Exception e) {
			try {
				out << structureMedium
			} catch (Exception e2) {
                out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
	            log.error(e.getMessage(), e)
			}
        }
    }

    /**
     * renders a simple formula
     */
    def structureMedium = { attrs ->

        try {
            validate(attrs)
            Color color = calculateColor(attrs)

	        out << "<img src=\"http://cactus.nci.nih.gov/chemical/structure/${attrs.inchi}/image?height=${imageService.MEDIUM_SIZE}&width=${imageService.MEDIUM_SIZE}&linewidth=2&symbolfontsize=12\"" +
			        " alt = \"sorry was not able to display structure\"/>"

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }
    /**
     * renders a simple formula
     */
    def structureLarge = { attrs ->

        try {
            //log.info "got: ${attrs}"
            validate(attrs)
            Color color = calculateColor(attrs)

            out << "<img src=\"${imageService.getLarge(attrs.id, attrs.inchi, color)}\" alt = \"sorry was not able to display structure\"></img>"

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }

    /**
     * renders a simple formula
     */
    def structureLarge3D = { attrs ->

        try {

            validate(attrs)

            Color color = calculateColor(attrs)
            //log.debug "attrs.color :: "+attrs.color


            long id = generateRandomId(attrs)

            out << """<div id="${id}" height="${imageService.LARGE_SIZE}" width="${imageService.LARGE_SIZE}"></div>

              <script src="http://cactus.nci.nih.gov/chemical/structure/
                    ${attrs.inchi}/twirl?div_id=${id}"
                      type="text/javascript"></script>"""

        } catch (Exception e) {
            out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
            log.error(e.getMessage(), e)

        }
    }


    protected def validate(attrs) {
        assert attrs.id != null, "please provide a compound id, its needed for the file name"

        assert attrs.inchi != null, "please provide a inchi so we can render it"
    }
}
