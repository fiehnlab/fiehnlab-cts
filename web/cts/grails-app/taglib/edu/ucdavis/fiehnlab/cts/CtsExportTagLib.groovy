package edu.ucdavis.fiehnlab.cts

class CtsExportTagLib {

	static namespace = "ctsexport"

	def ctsformats = { attrs ->
		StringWriter writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		def id = "exportButton"

		if (!attrs?.class) {
			attrs.class = "export"
		}

		if (attrs?.id) {
			id = attrs.id
			attrs.remove('id')
		}

		String action = actionName
		String controller = controllerName

		if (attrs?.action) {
			action = new String(attrs.action)
			attrs.remove("action")
		}

		if (attrs?.controller) {
			controller = new String(attrs.controller)
			attrs.remove("controller")
		}

		List formats = ['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']
		if (attrs?.formats) {
			formats = new ArrayList(attrs.formats)
			attrs.remove("formats")
		}

		Map parameters = [:]
		if (attrs?.params) {
			parameters = new HashMap(attrs.params)
			attrs.remove("params")
		}

		Map extensions = [excel: "xls", "EXCEL": "xls"]

		builder."div"(attrs) {
			formats.each { format ->
				span("class": "menuButton") {
					String message = ""

					try {
						message = g.message(code: 'default.' + format)
						if (message == 'default.' + format) {
							message = format.toUpperCase()
						}
					}
					catch (Exception e) {
						message = format.toUpperCase()
					}

					// Extension defaults to format
					String extension = format
					if (extensions.containsKey(format)) {
						extension = extensions[format]
					}

					Map params = [format: format, extension: extension] + parameters

					input(type: 'submit', name: 'format', class: format, value: message, id: id, '')
				}
			}
			String to = ""
			if (parameters.to instanceof String) {
				to = parameters.to
			} else {
				to = parameters.to.join("@@")
			}

			input(type: 'hidden', name: 'total', value: parameters.total)
			input(type: 'hidden', name: 'max', value: parameters.max)
			input(type: 'hidden', name: 'offset', value: parameters.offset)
			input(type: 'hidden', name: 'to', value: to)
			input(type: 'hidden', name: 'from', value: parameters.from)
			input(type: 'hidden', name: 'values', value: parameters.values)
			input(type: 'hidden', name: 'scoring', value: parameters.scoring)
			input(type: 'hidden', name: 'hits', value: parameters.hits)
		}

		writer.flush()
		out << writer.toString()
	}
}
