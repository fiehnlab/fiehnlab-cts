package edu.ucdavis.fiehnlab.cts

class CtsTagsTagLib {
	static namespace = 'cts'
	
	/**
	 * Shows a compound's basic data (Inchi Key, Inchi Code, preferred IUPAC Name and molecular weight)
	 * 
	 * @attr compound REQUIRED The <Compound> object to be rendered
	 */
	def compound = { attrs, body ->
		out << render("Attributes: ${attrs}")
//		Compound comp = (Compound)attrs?.compound
//		comp?.synonyms = Synonym.findAllByInchiKey(comp?.inchiKey) ?: []
//		comp?.extIds = ExternalId.findAllByInchiKey(comp?.inchiKey) ?: []
//		out << render(template:"/compound/compoundTemplate", model: [compound: comp])
	}

	def extIdTree = { attrs, body ->
		def data = attrs.data;

		Map<String, List<String>> tree = new HashMap<>()

		data.each {
			if(tree.containsKey(it.name)) {
				tree[it.name].add(it.value)
			} else {
				tree.put(it.name, new Vector<String>())
				if(tree[it.name].contains(it.value) == false) {
						tree[it.name].add(it.value)
				}
			}
		}

		out << "<li id='extIdTree'><a href='#'></a>"
		out << "<ul>"
		tree.each { key, val ->
			out << "<li id='${key}'><a href='#'>$key</a>"
			out << "<ul>"
			tree[key].each {
				out << "<li id='${key}${it}'><a href='#'>$it</a></li>"
			}
			out << "</ul>"
			out << "</li>"
		}
		out << "</ul>"
		out << "</li>"
	}
}
