import grails.converters.JSON
import org.apache.log4j.Logger

class LoggingFilters {
	private static Logger logger = Logger.getLogger(LoggingFilters.class)

	def filters = {
		long start
		long controllerLap

		filterBadServiceParams(controller: 'service', action:'*') {
			before = {}
			after = {}
			afterView = { Exception e ->
				if (e != null) {
					logger.error "[CTS2 Exception] Invalid 'value' parameter: ${params.value}.\nParameters: ${params.toMapString()}\nRequest: ${request}\nMessage: ${e.message}"
					render ([error: "Invalid value to convert: ${params.value}."]) as JSON
				}
			}
		}

		logTimings(controller: '*', action: '*') {
			before = {
				start = System.nanoTime()
			}
			after = { Map model ->
				controllerLap = System.nanoTime()
			}
			afterView = { Exception e ->
				if (logger.debugEnabled && (e == null || e.message.empty)) {
					logger.info "[CTS2 Timing] Time to show view [$controllerName/$actionName]: controler = ${(controllerLap - start) / 1000000}ms -- view = ${(System.nanoTime() - start) / 1000000}ms"
				}
				if (e != null) {
					logger.error "[CTS2 Exception] Call to ${controllerName}.${actionName} resulted in an exception of type: ${e.getClass()}.\nParameters: ${params.toMapString()}\nRequest: ${request}\nMessage: ${e.message == null ?: e.message}"
				}
			}
		}
	}
}



