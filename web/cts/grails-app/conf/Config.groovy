import org.apache.log4j.Level
// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

//grails.config.locations = ["file:updater.properties",
//                           "file:etc/updater.properties",
//                           "file:grails-app/conf/updater.properties",
//                           "classpath:updater.properties",
//                           "classpath:etc/updater.properties",
//                           "classpath:grails-app/conf/updater.properties"]

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
		all          : '*/*',
		atom         : 'application/atom+xml',
		css          : 'text/css',
		csv          : 'text/csv',
		form         : 'application/x-www-form-urlencoded',
		html         : ['text/html', 'application/xhtml+xml'],
		js           : 'text/javascript',
		json         : ['application/json', 'text/json'],

		// export document mime types
		excel        : 'application/vnd.ms-excel',
		ods          : 'application/vnd.oasis.opendocument.spreadsheet',
		pdf          : 'application/pdf',
		rtf          : 'application/rtf',

		multipartForm: 'multipart/form-data',
		rss          : 'application/rss+xml',
		text         : 'text/plain',
		xml          : ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*', '/downloads/*', 'download/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// Enable pretty rendering of json strings
grails.converters.json.pretty.print = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart = false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = true

disable.auto.recompile = false
grails.gsp.enable.reload = true

def ctsLogDir = './logs'
def updaterConfig = './updater.properties'


// log4j configuration
log4j = {
	// Example of changing the log pattern for the default console appender:
	//
	appenders {
		console name: 'stdout', layout: pattern(conversionPattern: '%-5p (%c)-[%t] %d %m%n'), threshold: Level.TRACE

//		(%-5c{2})
//		file name: 'fullLog', file: "${ctsLogDir}/cts-full.log", threshold: Level.DEBUG, append: false, layout: pattern(conversionPattern: '%-5p (%C{1}:%L) - %m%n')
//		file name: 'nameLog', file: "${ctsLogDir}/cts-names.log", threshold: Level.DEBUG, append: false, layout: pattern(conversionPattern: '%d{ISO8601} -- %m%n')
//		file name: 'restLog', file: "${ctsLogDir}/cts-rest.log", threshold: Level.DEBUG, append: false, layout: pattern(conversionPattern: '%d{ISO8601} -- %m%n')
//		file name: 'quartzLog', file: "${ctsLogDir}/cts-quartz.log", threshold: Level.DEBUG, append: false, layout: pattern(conversionPattern: '%d{ISO8601} -- %m%n')
	}

	root {
		info 'stdout'
//		debug 'fullLog'
	}

//	info quartzLog: ['org.quartz.job', 'grails.app.jobs'], additivity: false
//	info restLog: ['RestCallsFilters'], additivity: false
//	info nameLog: ['NameSaverFilters'], additivity: false
//	info fullLog: ['grails.app', 'edu.ucdavis.fiehnlab', 'grails.app.filters.LoggingFilters'], additivity: false

//	debug fullLog: ['grails.app.filters', 'org.quartz', 'edu.ucdavis.fiehnlab.update', 'edu.ucdavis.fiehnlab.cts', 'grails.app.controllers', 'grails.app.services', 'grails.app.jobs'], additivity: false
//	info fullLog: ['grails.app','grails.app.filters.LoggingFilters']
//	error fullLog: ['grails.app', 'edu.ucdavis.fiehnlab', 'org.quartz.core', 'org.quartz.plugins']

	error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
			'org.codehaus.groovy.grails.web.pages', //  GSP
			'org.codehaus.groovy.grails.web.sitemesh', //  layouts
			'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
			'org.codehaus.groovy.grails.web.mapping', // URL mapping
			'org.codehaus.groovy.grails.commons', // core / classloading
			'org.codehaus.groovy.grails.plugins', // plugins
			'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
			'org.springframework',
			'org.hibernate',
			'org.apache',
			'grails.util.GrailsUtil',
			'grails.app.service.NavigationService',
			'grails.app.service.org.grails.plugin.resource',
			'grails.app.taglib.org.grails.plugin.resource',
			'grails.app.resourceMappers.org.grails.plugin.resource',
			'net.sf.ehcache',
			'org.mortbay.log',
			'grails.plugin.cache',
			'grails.app.service.org.grails.plugin.resource', // too noisy with INFO statements
			'groovyx.net.ws',                   // Web services too noisy with INFO statements
			'org.apache.cxf.endpoint.dynamic'   // Web services too noisy with INFO statements

	error 'org.quartz.core',
			'grails.app.service.org.quartz.plugins',
			'org.grails.plugin.resource',
			'org.grails.datastore',
			'grails.spring',
			'groovyx.net.http.ParserRegistry'

	fatal 'net.sf.ehcache'

	info 'edu.ucdavis.fiehnlab.chemify', 'edu.ucdavis.fiehnlab.cts.DataExtractor'

//	debug stdout: ['edu.ucdavis.fiehnlab', 'grails.app.controllers', 'grails.app.services'], additivity: false
//
//	debug fullLog: ['edu.ucdavis.fiehnlab.cts'], additivity: false
//	debug quartzLog: ['org.quartz.job', 'grails.app.jobs'], additivity: false
//	debug restLog: ['grails.app.filters.RestCallsFilters'], additivity: false
//	debug nameLog: ['grails.app.filters.NameSaverFilters'], additivity: false
//	error 'grails.app.taglib.org.grails.plugin.resource', 'grails.app.services.org.grails.plugin.resource', 'grails.app.resourceMappers.org.grails.plugin.resource'

	environments {
		test {
			debug 'grails.app', 'edu.ucdavis.fiehnlab.cts', 'edu.ucdavis.fiehnlab.chemify.scoring.cts'
			debug 'edu.ucdavis.fiehnlab.chemify'

			// to debug query speed
//			debug 'org.hibernate.SQL'
//			trace 'org.hibernate.type.descriptor.sql.BasicBinder'
		}

		development {
			debug 'grails.app', 'edu.ucdavis.fiehnlab.cts'

			// to debug query speed
			debug 'org.hibernate.SQL'
			trace 'org.hibernate.type.descriptor.sql.BasicBinder'
		}

		production {
			info 'grails.app', 'edu.ucdavis.fiehnlab.cts'
			error 'grails.app.taglib.org.grails.plugin.resource', 'grails.app.services.org.grails.plugin.resource', 'grails.app.resourceMappers.org.grails.plugin.resource'

			// to debug query speed
//			debug 'org.hibernate.SQL'
//			trace 'org.hibernate.type.descriptor.sql.BasicBinder'
		}
	}
}

// plugins
grails.views.javascript.library = "jquery"

/**
 * tied in services to other applications
 */
services {
}

/**
 * CTS environment-dependant properties
 */
environments {
    grails.converters.json.pretty.print = true

	ctsLogDir = '/var/log/jetty'
	updaterConfig = 'updater.properties'

	updaterPath = './lib'
	searchTermMinQuality = 0.25

	def os = System.getProperty("os.name").toLowerCase()
	if (os.contains("win")) {
		lipidmapsPath = 'D:/Downloads/lipidmapstools/bin'
		perlPath = 'D:/perl64/bin'
	} else if (os.contains("nix") || os.contains("nux")) {
		lipidmapsPath = '/home/diego/Downloads/lipidmapstools/bin/'
		perlPath = '/usr/bin'
	} else if (os.contains("mac")) {
		lipidmapsPath = '/Users/diego/Downloads/lipidmapstools/bin/'
		perlPath = '/usr/bin'
	}

	cts {
		rest {
			proxy {
				prefix=""
				suffix=""
			}
		}
	}

	test {
		grails.converters.json.pretty.print = false
		ctsLogDir='./log'

		updater.enabled = true

		cts {
			ctsScoringUrl = 'http://localhost:8080/cts/service/score'
			ctsBaseUrl = 'http://localhost:8080'

			rest {
				url {
					pubchem = "pubchem.ncbi.nlm.nih.gov"
				}
			}
		}
	}

	development {
		ctsLogDir = './log'

		updaterEnabled = false

		cts {
			ctsScoringUrl = 'http://localhost:8888/service/score'
			ctsBaseUrl = 'http://localhost:8888'

			rest {
				url {
					pubchem = "pubchem.ncbi.nlm.nih.gov"
				}
			}
		}
	}

	production {
//		grails.serverURL = "http://cts.fiehnlab.ucdavis.edu"

		updaterEnabled = true

		cts {
			ctsScoringUrl = 'http://cts.fiehnlab.ucdavis.edu/service/score'
			ctsBaseUrl = 'http://cts.fiehnlab.ucdavis.edu'

			rest {
				url {
					pubchem = "pubchem.ncbi.nlm.nih.gov"
				}
			}
		}
	}


    cors.enabled=true
    cors.url.pattern = '/*'
    cors.headers=[
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Headers': 'origin, authorization, accept, content-type, x-requested-with, X-Auth-Token',
            'Access-Control-Allow-Methods': 'GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS',
            'Access-Control-Max-Age': 2600000
    ]
}
