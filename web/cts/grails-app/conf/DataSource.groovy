dataSource {
	pooled = true
	driverClassName = "org.h2.Driver"
	username = "sa"
	password = ""
}
hibernate {
	cache.use_second_level_cache = false
	cache.use_query_cache = false
	cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
	logSql = true
	format_sql = true
	use_sql_comment = true
}
// environment specific settings
environments {

	development {
		dataSource {
			dbCreate = "update"
			url = "jdbc:postgresql://venus.fiehnlab.ucdavis.edu:5432/cts-devel"
//			url = "jdbc:postgresql://localhost:5432/cts-devel"
			driverClassName = "org.postgresql.Driver"
			username = "compound"
			password = "asdf"
			pooled = true
			properties {
				maxActive = 1000
				maxIdle = 25
				minIdle = 5
				initialSize = 5
				minEvictableIdleTimeMillis = 240000
				timeBetweenEvictionRunsMillis = 10000
				maxWait = 10000
				removeAbandoned = true
				removeAbandonedTimeout = 60
				numTestsPerEvictionRun = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = true
				validationQuery = "SELECT 1"
			}
			cache.use_second_level_cache = false
			cache.use_query_cache = false
		}
	}

	test {
		dataSource {
			dbCreate = "update"
			url = "jdbc:postgresql://venus.fiehnlab.ucdavis.edu:5432/cts-test"
			driverClassName = "org.postgresql.Driver"
			username = "compound"
			password = "asdf"
			pooled = true
			properties {
				maxActive = 1000
				maxIdle = 25
				minIdle = 5
				initialSize = 5
				minEvictableIdleTimeMillis = 240000
				timeBetweenEvictionRunsMillis = 10000
				maxWait = 10000
				removeAbandoned = true
				removeAbandonedTimeout = 60
				numTestsPerEvictionRun = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = true
				validationQuery = "SELECT 1"
			}
			cache.use_second_level_cache = false
			cache.use_query_cache = false
		}
	}

	production {
		dataSource {
			dbCreate = "update"
			url = "jdbc:postgresql://venus.fiehnlab.ucdavis.edu:5432/cts"
			driverClassName = "org.postgresql.Driver"
			username = "compound"
			password = "asdf"
			pooled = true
			properties {
				maxActive = 1000
				maxIdle = 25
				minIdle = 5
				initialSize = 5
				minEvictableIdleTimeMillis = 240000
				timeBetweenEvictionRunsMillis = 10000
				maxWait = 10000
				removeAbandoned = true
				removeAbandonedTimeout = 60
				numTestsPerEvictionRun = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = true
				validationQuery = "SELECT 1"
			}
		}
	}
}
