grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.7
grails.project.source.level = 1.7
//grails.project.war.file = "target/${appName}-${appVersion}.war"

//grails.war.resources = { stagingDir, args ->
//	copy(file: "$userHome/.grails/updater.properties", tofile: "${stagingDir}/updater.properties")
//}

grails.project.dependency.resolution.resolver = "maven"
grails.project.dependency.resolution = {
	legacyResolve false

	// inherit Grails' default dependencies
	inherits("global") {
		// specify dependency exclusions here; for example, uncomment this to disable ehcache:
		// excludes 'ehcache'
	}
	log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
	checksums true // Whether to verify checksums on resolve

	pom true

	repositories {
		inherits true // Whether to inherit repository definitions from plugins

		grailsHome()
		grailsCentral()
		grailsPlugins()
		mavenRepo "http://gose.fiehnlab.ucdavis.edu:55000/content/groups/public"
		mavenCentral()
	}

}
