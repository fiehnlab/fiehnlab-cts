class NameSaverFilters {
	def sqlHelper

	def filters = {

		long start

		catchNamesForOptimization(controller: '(conversion|service)', action: '*?[Cc]onvert') {
			before = {
				start = System.currentTimeMillis()

				def names = ""
				if(actionName.contains("batch")) {
					names += params.values
				} else {
					names += params.value
				}

				log.info "[CTS2 NAME DUMP] controller:${controllerName}\taction:${actionName}\tfrom:${params.from}\tto:${params.to}\tname:${names}"
			}
			after = { Map model ->
				if(model != null)
					sqlHelper.addQuery(params, controllerName, System.currentTimeMillis() - start, actionName, model)
			}
			afterView = { Exception e ->
				if (e != null) {
					sqlHelper.addQuery(params, controllerName, System.currentTimeMillis() - start, actionName, [message: e.message, stacktrace: e.stackTrace])
					log.debug("[CTS2 Exception] Something bad happened: ${e.message}")
				}
			}
		}
	}
}
