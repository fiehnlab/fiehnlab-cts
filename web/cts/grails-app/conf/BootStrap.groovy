import edu.ucdavis.fiehnlab.cts.Compound
import edu.ucdavis.fiehnlab.cts.ExternalId
import edu.ucdavis.fiehnlab.cts.IdName
import edu.ucdavis.fiehnlab.cts.Synonym

class BootStrap {
	javax.sql.DataSource dataSource

    def grailsApplication

    def init = { servletContext ->
		// insert data for all environments

//        System.setProperty("CHEMSPIDER_HOST",grailsApplication.config.cts.rest.url.chemspider)
//        System.setProperty("CHEMSPIDER_PARTS_HOST",grailsApplication.config.cts.rest.url.chemspiderParts)
        System.setProperty("PUBCHEM_HOST",grailsApplication.config.cts.rest.url.pubchem)

        environments {
			test {
				//loadData()
			}

			development {
			}

			production {
			}
		}
	}

	private void loadData() {
		def comp

		if(Compound.findAllByInchiKey('RYXHPKXJBBAPCB-UHFFFAOYSA-N').size() <= 0) {
			comp = new Compound(inchiKey: 'RYXHPKXJBBAPCB-UHFFFAOYSA-N',
					inchiCode: 'InChI=1S/C16H15Cl2N5O2/c1-2-25-16(24)23-13-6-11-14(15(19)22-13)21-12(7-20-11)8-3-4-9(17)10(18)5-8/h3-6,20H,2,7H2,1H3,(H3,19,22,23,24)',
					molWeight: 380.2286, exactMass: 380.2286, formula: 'C16H15Cl2N5O2', synonyms:new HashSet<Synonym>(), extIds:new HashSet<ExternalId>()).save(flush:true, failOnError: true)

			if(comp.synonyms.size() <= 0) {
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (CAS-like Style)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (Traditional)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-azanyl-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Systematic)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Preferred)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(3,4-dichlorophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Allowed)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'carbamico', type: 'Synonym', score:0)).save(flush:true, failOnError:true)
			}

			if(comp.extIds.size() <= 0) {
				comp.addToExtIds(new ExternalId(name: 'PubChem CID', url: 'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', value: '150001')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ABI Chem', url: 'http://www.abichem.com', value: 'AC1L43G0')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ChEMBL', url: 'https://www.ebi.ac.uk/chembldb', value: 'CHEMBL9517')).save(flush:true, failOnError:true)
			}
		}


		if(Compound.findAllByInchiKey('QACVMOHIHXGYLO-UHFFFAOYSA-N').size() <= 0) {
			comp = new Compound(inchiKey: 'QACVMOHIHXGYLO-UHFFFAOYSA-N',
					inchiCode: 'InChI=1S/C17H19N5O3/c1-3-25-17(23)22-14-8-12-15(16(18)21-14)20-13(9-19-12)10-5-4-6-11(7-10)24-2/h4-8,19H,3,9H2,1-2H3,(H3,18,21,22,23)',
					molWeight: 341.36446, exactMass: 341.36446, formula: 'C17H19N5O3', synonyms:new HashSet<Synonym>(), extIds:new HashSet<ExternalId>()).save(flush:true, failOnError:true)

			if(comp.synonyms.size() <= 0) {
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (CAS-like Style)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (Traditional)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-azanyl-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Systematic)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Preferred)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(3-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Allowed)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'carbamato', type: 'Synonym', score:0)).save(flush:true, failOnError:true)
			}

			if(comp.extIds.size() <= 0) {
				comp.addToExtIds(new ExternalId(name: 'PubChem CID', url: 'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', value: '150002')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ABI Chem', url: 'http://www.abichem.com', value: 'AC1L43G3')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ChEMBL', url: 'https://www.ebi.ac.uk/chembldb', value: 'CHEMBL274591')).save(flush:true, failOnError:true)
			}
		}


		if(Compound.findAllByInchiKey('DNVPMVJYQWFBIK-UHFFFAOYSA-N').size() <= 0) {
			comp = new Compound(inchiKey: 'DNVPMVJYQWFBIK-UHFFFAOYSA-N',
					inchiCode: 'InChI=1S/C17H19N5O3/c1-3-25-17(23)22-14-8-12-15(16(18)21-14)20-13(9-19-12)10-4-6-11(24-2)7-5-10/h4-8,19H,3,9H2,1-2H3,(H3,18,21,22,23)',
					molWeight: 341.36446, exactMass: 341.36446, formula: 'C17H19N5O3', synonyms:new HashSet<Synonym>(), extIds:new HashSet<ExternalId>()).save(flush:true, failOnError:true)

			if(comp.synonyms.size() <= 0) {
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (CAS-like Style)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (Traditional)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-azanyl-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Systematic)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Preferred)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(4-methoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Allowed)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'carbameso', type: 'Synonym', score:0)).save(flush:true, failOnError:true)
			}

			if(comp.extIds.size() <= 0) {
				comp.addToExtIds(new ExternalId(name: 'PubChem CID', url: 'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', value: '150003')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ABI Chem', url: 'http://www.abichem.com', value: 'AC1L43G6')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ChEMBL', url: 'https://www.ebi.ac.uk/chembldb', value: 'CHEMBL9740')).save(flush:true, failOnError:true)
			}
		}


		if(Compound.findAllByInchiKey('BWJOJAFDVLMQIH-UHFFFAOYSA-N').size() <= 0) {
			comp = new Compound(inchiKey: 'BWJOJAFDVLMQIH-UHFFFAOYSA-N',
					inchiCode: 'InChI=1S/C19H23N5O5/c1-5-29-19(25)24-15-8-11-16(18(20)23-15)22-12(9-21-11)10-6-13(26-2)17(28-4)14(7-10)27-3/h6-8,21H,5,9H2,1-4H3,(H3,20,23,24,25)',
					molWeight: 401.41642, exactMass: 401.41642, formula: 'C19H23N5O5', synonyms:new HashSet<Synonym>(), extIds:new HashSet<ExternalId>()).save(flush:true, failOnError:true)

			if(comp.synonyms.size() <= 0) {
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (CAS-like Style)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (Traditional)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-azanyl-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Systematic)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Preferred)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(3,4,5-trimethoxyphenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Allowed)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'carbamoto', type: 'Synonym', score:0)).save(flush:true, failOnError:true)
			}

			if(comp.extIds.size() <= 0) {
				comp.addToExtIds(new ExternalId(name: 'PubChem CID', url: 'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', value: '150004')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ABI Chem', url: 'http://www.abichem.com', value: 'AC1L43G9')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ChEMBL', url: 'https://www.ebi.ac.uk/chembldb', value: 'CHEMBL110407')).save(flush:true, failOnError:true)
			}
		}


		if(Compound.findAllByInchiKey('OYHTZOLNNSQJLH-UHFFFAOYSA-N').size() <= 0) {
			comp = new Compound(inchiKey: 'OYHTZOLNNSQJLH-UHFFFAOYSA-N',
					inchiCode: 'InChI=1S/C16H18N6O2/c1-2-24-16(23)22-13-7-11-14(15(18)21-13)20-12(8-19-11)9-3-5-10(17)6-4-9/h3-7,19H,2,8,17H2,1H3,(H3,18,21,22,23)',
					molWeight: 326.35312, exactMass: 326.35312, formula: 'C16H18N6O2', synonyms:new HashSet<Synonym>(), extIds:new HashSet<ExternalId>()).save(flush:true, failOnError:true)

			if(comp.synonyms.size() <= 0) {
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(4-aminophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (CAS-like Style)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'N-[5-amino-3-(4-aminophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamic acid ethyl ester', type: 'IUPAC Name (Traditional)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[3-(4-aminophenyl)-5-azanyl-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Systematic)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(4-aminophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Preferred)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'ethyl N-[5-amino-3-(4-aminophenyl)-1,2-dihydropyrido[3,4-b]pyrazin-7-yl]carbamate', type: 'IUPAC Name (Allowed)', score:0)).save(flush:true, failOnError:true)
				comp.addToSynonyms(new Synonym(name: 'carbamistico', type: 'Synonym', score:0)).save(flush:true, failOnError:true)
			}

			if(comp.extIds.size() <= 0) {
				comp.addToExtIds(new ExternalId(name: 'PubChem CID', url: 'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', value: '150005')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ABI Chem', url: 'http://www.abichem.com', value: 'AC1L43GC')).save(flush:true, failOnError:true)
				comp.addToExtIds(new ExternalId(name: 'ChEMBL', url: 'https://www.ebi.ac.uk/chembldb', value: 'CHEMBL275790')).save(flush:true, failOnError:true)
			}
		}


		// test compound without synonyms
		if(Compound.findAllByInchiKey('AAAAAAAAAAAAAA-AAAAAAAAAA-A').size() <= 0) {
			comp = new Compound(inchiKey: 'AAAAAAAAAAAAAA-AAAAAAAAAA-A',
					inchiCode: 'inchi de a',
					molWeight: 1.0, exactMass: 1.0, formula: '', synonyms:new HashSet<Synonym>(), extIds:new HashSet<ExternalId>()).save(flush:true, failOnError:true)

			if(comp.extIds.size() <= 0) {
				comp.addToExtIds(new ExternalId(name: 'PubChem CID', url: 'http://no.url.com', value: '10')).save(flush:true, failOnError:true)
			}
		}

		// test compound without synonyms
		if(Compound.findAllByInchiKey('BBBBBBBBBBBBBB-BBBBBBBBBB-B').size() <= 0) {
			comp = new Compound(inchiKey: 'BBBBBBBBBBBBBB-BBBBBBBBBB-B',
					inchiCode: 'inchi de b',
					molWeight: 1.0, exactMass: 1.0, formula: '', synonyms:new HashSet<Synonym>(), extIds:new HashSet<ExternalId>()).save(flush:true, failOnError:true)

			if(comp.synonyms.size() <= 0) {
				comp.addToSynonyms(new Synonym(name: 'test comp with one synonym and no ids', type: 'Synonym', score:0)).save(flush:true, failOnError:true)
			}
		}
	}

	private updateIdNames() {
		def names = ExternalId.where {}.projections { distinct 'name' }

		names.each {
			if(!IdName.findByIdName(it)) {
				new IdName(idName:it).save(flush:true, failOnError:true)
			}
		}
	}
}