class UrlMappings {


	static mappings = {
		"/$controller/$action?/$id?" {
			constraints {
				// apply constraints here
			}
		}

		/**
		 * service for popularity filtering
		 */
		"/service/count/$inchikey?"(controller: "service", action: "idcount")

		/**
		 * service for biological sorting
		 */
		"/service/countBiological/$inchikey?"(controller: "service", action: "bioCount")

		/**
		 * service to get synonyms for specific inchi key
		 */
		"/service/synonyms/$inchikey?"(controller: "service", action: "synonymsForInchikey")

		/**
		 * service for getting single compound information
		 */
		"/service/compound/$inchiKey?"(controller: "service", action: "compound")

		/**
		 * mapping to get from/to values for conversions
		 */
		"/service/conversion/fromValues"(controller: "service", action: "fromValues")
		"/service/conversion/toValues"(controller: "service", action: "toValues")

		/**
		 * part of the restful api to convert a value
		 */
		"/service/convert/$from?/$to?/$value?/$scoring?"(controller: "service", action: "convert")

		/**
		 * mapping to get a data source name from its id
		 */
		"/service/sourceName/$pcid?"(controller: 'service', action: 'sourceName')

		/**
		 * mapping to get a data source name from its id
		 */
		"/service/pubchemDepositor/$type?/$sid?/$dsid?"(controller: 'service', action: 'pubchemDepositor')

		/**
		 * used to score a list of inchi keys based on the specified algorithm
		 */
		"/service/score/$from?/$value?/$algorithm?"(controller: "scoring", action: "score")

		/**
		 * service that converts a mdl molecule definition into an inchikey and inchi code
		 */
        "/service/moltoinchi"(controller: "service", parseRequest: true) {
            action = [POST: "molToInchi"]
        }

        /**
         * Service that converts smiles into inchi codes
         */
        "/service/smilestoinchi" (controller: "service", parseRequest: true) {
	        action = [POST: "smilesToInchi"]
        }

        /**
		 * service that converts an inchi code into an mdl molecule definition
		 */
		"/service/inchitomol"(controller: "service", parseRequest: true) {
			action = [POST: "inchiToMol"]
		}

		/**
		 * service that converts an inchi code into an mdl molecule definition
		 */
		"/service/inchicodetoinchikey"(controller: "service", parseRequest: true) {
			action = [POST: "inchiCodeToInchiKey"]
		}

		/**
		 * service that converts an inchikey into an mdl molecule definition
		 */
		"/service/inchikeytomol/$inchikey"(controller: "service", action: "inchiKeyToMol")

		/**
		 * service that expands molecular formulas into a comparable string representation
		 */
		"/service/expandFormula/$formula" (controller: "service", action: "expandFormula")

		/**
		 * service to return the score for an external id
		 */
		"/service/extidScore/$name"(controller: "info", action: "getExternalIdScore")

		/**
		 * service to return the scores for all external ids
		 */
		"/service/allExtidScores"(controller: "info", action: "getAllExtIdScores")

		/**
		 * service that maps to chemify services - configuration view
		 */
		"/chemify/rest/identify"(controller: "chemify", action: "index")

		/**
		 * query of chemify
		 */
		"/chemify/rest/identify/$value"(controller: "chemify", action: "identify")

		/**
		 * query of chemify
		 */
		"/chemify/rest/identify/query"(controller: "chemify", action: "identify")

		/**
		 * named mapping for contact page
		 */
		name contact: "/contact"(view: "/contact/contact")

		/**
		 * internal rest endpoint to register a new compound
		 */
		"/service/compound/register"(controller: "update", parseRequest: true) {
			action = [POST: "registerInchi"]
		}

		"/"(controller: "index")
		"500"(controller: "error", action: "serverError")
		"404"(view: '/404')
	}
}
