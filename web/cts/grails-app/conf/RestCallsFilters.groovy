import org.apache.log4j.Logger

class RestCallsFilters {
	private static Logger logger = Logger.getLogger(RestCallsFilters.class)

	def filters = {

		logRestCall(controller: 'service', action: '*') {
			Long start
			before = {
				start = System.nanoTime()
			}
			after = { Map model ->
				logger.info("[CTS2 REST] Action: ${actionName}\tMethod: ${request.method}\tParams: ${params.toMapString()}\tTiming: ${(System.nanoTime() - start) / 1000000}ms")
			}
			afterView = { Exception e ->
			}
		}
	}
}
