package edu.ucdavis.fiehnlab.cts

/**
 * what does this represent?
 * @author dpedrosa
 *
 */
class ExternalId {
//	long id
//	String inchiKey
	String name
	String url
	String value
	
	static belongsTo = [inchiKey: Compound]
	
	static optionals = ['url']
		
    static constraints = {
		inchiKey nullable: false, unique:['name','value']
		name nullable: false, blank: false	// who created the id: PubChem, HMDB, CAS... etc
		url url: true
		value nullable: false, blank: false // actual value for the id
    }

	static mapping = {
		inchiKey column: 'inchi_key'
		name type: 'text'
		value type: 'text'
		version false
	}
	
	String toString() { "${name}:${value} (${url})" }
	
	int hashCode() { return toString().hashCode() }
	
	boolean equals(Object obj) {
		if (!obj) { return false }
		
		if (obj instanceof ExternalId) {
			
			ExternalId extid = (ExternalId) obj
			if (!this.name.equals(extid.name)) { return false }
			if (!this.url.equals(extid.url)) { return false }
			if (!this.value.equals(extid.value)) { return false }
			
			return true
		} else {
			return false
		}
	}
}
