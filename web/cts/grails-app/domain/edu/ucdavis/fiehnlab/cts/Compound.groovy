package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON

class Compound {
	String inchiKey
	String inchiCode
	Double molWeight = 0d
	Double exactMass = 0d
	String formula
	String sourceFile
	Integer dataSourcesCount = 0
	Integer referencesCount = 0
	Integer pubmedHits = 0

	static hasMany = [synonyms: Synonym, extIds: ExternalId]

	static mapping = {
		version false
		id generator: 'assigned', name: 'inchiKey'
		inchiKey unique:true, column: 'id'
		inchiCode type: 'text'
		formula type: 'text'
		exactMass precision: 5, scale: 5
		molWeight precision: 5, scale: 5
        synonyms lazy: false
        extIds lazy: false
		dataSourcesCount defaultValue: "0"
		referencesCount defaultValue: "0"
		pubmedHits defaultValue: "0"
	}

	static constraints = {
		inchiKey nullable: false, blank: false, size: 27..27
		inchiCode nullable: true, type: 'text'
		formula nullable: true, type: 'text'
		exactMass min: 0d, scale: 5, default: 0d
		molWeight min: 0d, scale: 5, default: 0d
	}

	def toJson() {
		def result = [:]
		result.inchikey = this.inchiKey
		result.inchicode = this.inchiCode
		result.molweight = this.molWeight <= 0.0 ? null : this.molWeight
		result.exactmass = this.exactMass <= 0.0 ? null : this.exactMass
		result.formula = this.formula

		result.synonyms = this.synonyms.collect{syn -> [type:syn.type,name:syn.name,score:0]}
		result.externalIds = this.extIds.collect{id -> [name:id.name,value:id.value,url:id.url]}

		result.dataSourcesCount = this.dataSourcesCount
		result.referencesCount = this.referencesCount
		result.pubmedHits = this.pubmedHits

		return result as JSON
	}


	String getId() {
		inchiKey
	}

	void setId(String inchiKey) {
		id = inchiKey
	}

	String toString() {
		inchiKey
	}

	int hashCode() {
		toString().hashCode()
	}

	boolean equals(Object obj) {
		if (!obj) { return false }

		if(obj instanceof Compound){

			Compound comp = (Compound) obj
			if (!this.inchiKey.equals(comp.inchiKey)) { return false }
			if (!this.inchiCode.equals(comp.inchiCode)) { return false }

			return true
		} else {
			return false
		}
	}
}
