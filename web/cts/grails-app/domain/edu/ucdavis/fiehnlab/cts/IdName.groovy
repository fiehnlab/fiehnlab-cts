package edu.ucdavis.fiehnlab.cts

class IdName implements Comparable<IdName> {
	String idName
	Integer hits
	String pcId
	Integer score

	static constraints = {
		idName nullable: false, blank: false, index: 'name_idx'
	    hits nullable: false
	    pcId nullable: true
		score nullable: false
    }

	static mapping = {
		version false
	}

	@Override
	String toString() {
		idName
	}

	@Override
	int compareTo(IdName other) {
		return idName <=> other.idName
	}

	@Override
	boolean equals(Object obj) {
		if (!obj) { return false }

		if (obj instanceof IdName) {

			IdName idn = (IdName) obj
			if (!this.idName.equals(idn.idName)) { return false }

			return true
		} else {
			return false
		}
	}

	@Override
	int hashCode() {
		return idName.hashCode()
	}
}
