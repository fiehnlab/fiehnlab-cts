package edu.ucdavis.fiehnlab.cts

/**
 * what does this represent
 * @author dpedrosa
 *
 */
class Synonym {
//	long id
//	String inchiKey
	String type
	String name
	Double score

	static belongsTo = [inchiKey: Compound]
	
	static constraints = {
		inchiKey nullable: false, unique:['type','name']
		type inList: ['IUPAC Name (Traditional)',
			'IUPAC Name (Systematic)',
			'IUPAC Name (CAS-like Style)',
			'IUPAC Name (Allowed)',
			'IUPAC Name (Preferred)',
			'Synonym']
		name nullable: false, blank: false
		score nullable: false, default: 0d
	}

	static mapping = {
		inchiKey column: 'inchi_key'
		name type: 'text'
		version false
	}

	@Override
	String toString() { "${type}: ${name}" }

	@Override
	int hashCode() { return toString().hashCode() }

	@Override
	boolean equals(Object obj) {
		if (!obj) { return false }

		if (obj instanceof Synonym) {

			Synonym syn = (Synonym) obj
			if (!this.name.equals(syn.name)) { return false }
			if (!this.type.equals(syn.type)) { return false }
			if (!this.score.equals(syn.score)) { return false }
	
			return true
		} else {
			return false
		}
	}
}
