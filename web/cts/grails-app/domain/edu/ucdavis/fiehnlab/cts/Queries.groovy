package edu.ucdavis.fiehnlab.cts

class Queries {
	String keywords
	String sourceType
	String destType
	String result
	String origin
	Long speed
	String action

	Date dateCreated

	static mapping = {
		version true
		autoTimestamp true
		keywords type: 'text'
		destType type: 'text'
		result type: 'text'
	}

    static constraints = {
	    keywords nullable: false
	    sourceType nullable: false, maxSize: 200
	    destType nullable: false
	    origin nullable: false, maxSize: 100
	    action maxSize: 100
	    speed default: 0
    }
}
