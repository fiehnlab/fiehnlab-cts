package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.updater.controller.UpdaterController
import grails.util.Environment
import org.apache.log4j.Logger

//import edu.ucdavis.fiehnlab.update.controller.UpdaterController

/**
 * Created by diego on 1/29/15.
 */
class UpdateJob {
	private static Logger logger = Logger.getLogger(UpdateJob.class)
	static running = false

	def concurrent = false
	def group = "Updater"
	def grailsApplication
	UpdaterController updater

	static triggers = {
		// run updates on sundays
		switch (Environment.current) {
			case Environment.PRODUCTION:
				cron name: 'update-cts', cronExpression: '0 9 0 * * ?'
				break
			case Environment.DEVELOPMENT:
//				cron name: 'minute runner', cronExpression: '0/5 * * * * ?'
				break
			default:
				simple(name: 'singleRun', repeatCount: 0, startDelay: 1000)
		}
	}

	def execute(context) {

		//check for config file
		String configFile = grailsApplication.config
		logger.info("updaterConfig property = ${grailsApplication.config.updater.enabled}")

		boolean enabled = grailsApplication.config.updater.enabled
logger.debug "----------------- UPDATE? $enabled -----------------"
logger.debug "config type: ${grailsApplication.config.class.simpleName}"

		if(enabled) {
			try {
				//use configFile if it's not null or empty constructor otherwise
				updater = new UpdaterController(grailsApplication.config)

				// get updated files
				def files = updater.update()

				def updFolder = ""
				if(files instanceof List) {
					updFolder = files[0].substring(0, files[0].lastIndexOf("/"));
				} else if(files instanceof String) {
					updFolder = files.substring(0, files.lastIndexOf("/"));
				}
				logger.debug "FOLDER: ${updFolder}"

				//remove Killed IDs from DB
				def killed = processKillFiles(updFolder)
				logger.debug "------------ killed files ----------\n${killed}"

				//insert sql files in DB


			} catch (Exception e) {
				logger.error("UpdateJob execution error.\nException: ${e.getMessage()}", e)
			}
		}
	}

	private boolean processKillFiles(String folder) {
		def dir = new File(folder)
		if(!dir.exists() || !dir.isDirectory()) {
			throw new IOException("Update folder not found.")
		}

		def files = dir.listFiles(new FilenameFilter() {
			@Override
			boolean accept(File file, String name) {
				return file.path.toLowerCase().contains('killed')
			}
		})
	}
}
