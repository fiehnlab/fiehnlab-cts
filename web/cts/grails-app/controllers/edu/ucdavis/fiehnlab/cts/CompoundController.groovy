package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class CompoundController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def show(String id) {
        def compoundInstance = Compound.get(id)
        if (!compoundInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'compound.label', default: 'Compound'), id])
            redirect controller: "index"
            return
        }

        [compoundInstance: compoundInstance]
    }

    /**
     * returns the IUPac Names for a compound
     * @param id
     */
    def ajaxGetIUPacNameForCompound(String id){
        render template: "renderIUPACNames", model: [synonyms:Synonym.findAllByInchiKeyAndTypeLike(Compound.get(id),"IUPAC Name%").sort {a,b -> a.type.compareToIgnoreCase b.type}]
    }

    /**
     * returns
     * @param id
     */
    def ajaxGetSynonymsForCompound(String id){
        render template: "renderSynonyms", model: [synonyms:Synonym.findAllByInchiKeyAndTypeNotLike(Compound.get(id),"IUPAC Name%").sort {a,b -> a.type.compareToIgnoreCase b.type}]
    }

}
