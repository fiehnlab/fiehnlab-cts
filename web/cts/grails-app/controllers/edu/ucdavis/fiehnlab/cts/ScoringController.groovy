package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON

class ScoringController {
	ConversionService conversionService
	ScoringService scoringService

	/**
	 * searches for a 'value' of type 'from' and sorts the results based on the 'algorithm' selected
	 * @param from search term type
	 * @param value search term
	 * @param algorithm either 'biological' or 'popularity'
	 * @return sorted
	 */
	def score() {
		def result = ""
		def error = []
		def algorithm = "biological"

		if (!params.from) {
			error.add("Parameter 'from' can't be empty.")
		} else {
			if (!conversionService.fromFields.collect { it.toLowerCase() }.contains(params.from.toLowerCase())) {
				error.add("The name of the identifier to convert 'from' is invalid")
			}
		}

		if (!params.value || !params.algorithm) {
			error.add("You need to provide a 'value' to score and an 'algorythm'.${System.getProperty("line.separator")}Please call http://cts.fiehnlab.ucdavis.edu/service/score/<from>/<value>/<algorithm>${System.getProperty("line.separator")}" +
					"where <value> is a chemical name or an identifier${System.getProperty("line.separator")}" +
					"and <algorithm> is one of: 'biological', 'popularity'")
		}

		if (params.algorithm) {
			if (!params.algorithm.equals("biological") && !params.algorithm.equals("popularity")) {
				error.add("Invalid algorithm selected. The algorithms can be one of: 'biological', 'popularity'")
			}
			algorithm = params.algorithm
		}

		if (error) {
			response.status = 400
			render error as JSON
			return
		}

		def res = conversionService.convert(params.from, params.value, ConstantHelper.NAMES_INCHI_KEY, [scoring: algorithm, hits: 0])
		log.debug("after conv: $res")

		def inchis = res[ConstantHelper.NAMES_INCHI_KEY][0].value
		log.debug("after to score: $inchis")

		switch (algorithm) {
			case ConstantHelper.SCORING_BIO:
			case ConstantHelper.SCORING_COUNT:
				result = scoringService.score(algorithm, inchis)
				break
			default:
				result = [new ScoredValue("no scoring done", 0.0)]
		}

		def finalMap = ['searchTerm': params.value]
		finalMap.put('from', params.from)

		if (result.isEmpty()) {
			finalMap.put("result", [new ScoredValue("no scoring done", 0.0)])
		} else {
			result.each { ScoredValue it ->
//				if(log.debugEnabled) {
//					log.debug("resulting item: $it || class: ${it.class}")
//				}

				if (finalMap["result"] == null) {
					finalMap["result"] = []
				}
				def item = [:]
				item[ConstantHelper.NAMES_INCHI_KEY] = it.value
				item["score"] = it.score
				finalMap["result"].add(item)
			}
		}

//		log.debug("FINAL MAP: $finalMap")
		render finalMap as JSON
	}

}
