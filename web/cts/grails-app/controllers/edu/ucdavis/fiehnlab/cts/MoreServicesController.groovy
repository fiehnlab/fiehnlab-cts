package edu.ucdavis.fiehnlab.cts

class MoreServicesController {

	def index() {
		render view: "index"
	}

	def codeExamples() {
		render view: "codeExamples"
	}
}
