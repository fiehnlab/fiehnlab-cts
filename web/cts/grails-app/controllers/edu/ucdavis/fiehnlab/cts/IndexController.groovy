package edu.ucdavis.fiehnlab.cts

import grails.util.Environment

class IndexController {
	ConversionService conversionService

    def index() {

	    ArrayList<String> fromNames = conversionService.getFromFields()
	    ArrayList<String> toNames = conversionService.getToFields()

	    render view: "/index", model: [fromNames: fromNames, toNames: toNames]
	}
}
