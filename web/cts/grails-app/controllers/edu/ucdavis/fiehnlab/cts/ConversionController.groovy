package edu.ucdavis.fiehnlab.cts

import grails.util.Environment

class ConversionController {
	def conversionService
	def scoringService

	//for export plugin
	def grailsApplication
	def exportService

	public static final int SINGLE_QUERY_MAX_LENGTH = 300
	public static final int BATCH_QUERY_MAX_LENGTH = 10000
	public static final int BATCH_QUERY_MAX_TOKENS = 100
	public static final int QUERY_MAX_HITS_LIMIT = 1000
	public static final int QUERY_DEFAULT_HITS_LIMIT = 1000
	static final String INCHI_KEY = "InChIKey"

	/**
	 *  renders simple conversion form
	 * @return
	 */
	def index() {
		def fromNames = session.fromNames ?: conversionService.getFromFields()
		def toNames = session.toNames ?: conversionService.getToFields()

		session.fromNames = fromNames
		session.toNames = toNames

		render view: "simpleConversion", model: [fromNames: fromNames, toNames: toNames, env: Environment.currentEnvironment]
	}

	/**
	 *  returns list of results matching 'value' converted from 'from' to 'to'
	 * @param value
	 * @param from
	 * @param to
	 * @return
	 */
	def convert() {
//		if (log.debugEnabled) {
//			log.debug "PARAMS: $params"
//		}

		def fromNames = session.fromNames ?: conversionService.getFromFields()
		def toNames = session.toNames ?: conversionService.getToFields()


		if (params.from == null || params.to == null) {
			flash.message = message(code: "conversion.direct.call.to.convert", default: "You didn't fill the form correctly")
			redirect action: "index"
			return
		} else if (params.values == null || params.values == "") {
			flash.message = message(code: "conversion.value.not.empty", default: "You need to enter a value to convert")
			redirect action: "index"
			return
		} else if (params.values?.size() > SINGLE_QUERY_MAX_LENGTH) {
			if (log.debugEnabled) {
				log.debug "query too long... using ${SINGLE_QUERY_MAX_LENGTH} first characters"
			}
			params.values = params.values?.substring(0, SINGLE_QUERY_MAX_LENGTH)
			flash.message = message(code: "conversion.value.too.long", default: "Query too long, using the first ${SINGLE_QUERY_MAX_LENGTH} characters.")
		}

		if (params.max && !(params.max instanceof Integer)) {
			params.max = Integer.parseInt(params.max)
		}

		if (params.offset && !(params.offset instanceof Integer)) {
			params.offset = Integer.parseInt(params.offset)
		}

		if (params.total && !(params.total instanceof Integer)) {
			params.total = Integer.parseInt(params.total)
		}

		if (params.hits == null) {
			params.hits = 1
		}

		if (params.hits && !(params.hits instanceof Integer)) {
			params.hits = Integer.parseInt(params.hits)
		}

		if (params.scoring) {
			params.scoring = "biological"
		}

		params.max = params.max ?: QUERY_DEFAULT_HITS_LIMIT
		params.offset = params.offset ?: 0
		params.total = params.total ?: 0

		if (log.debugEnabled) {
			log.debug("simple conversion of '${params.values}' fromn '${params.from}' to '${params.to}'...")
		}

		def start = System.currentTimeMillis()
		def results = conversionService.convert(params?.from, params?.values, params?.to, params)
		log.info "[Conversion Time] ${(System.currentTimeMillis() - start) / 1000}s)"   //total time for conversion

		if (log.debugEnabled) {
			log.debug("... resulted in ${results.size()} items")
		}

		render view: "simpleConversion",
				model: [fromNames: fromNames, toNames: toNames, results: results, hits: params.hits,
				        from: params.from, to: params.to, values: params.values, scoring: params.scoring,
				        max      : params.max, offset: params.offset, total: params.total, env: Environment.currentEnvironment]
	}

	/**
	 * puts the data in a file and sends it for download
	 * @return
	 */
	def exportTo() {
		if (log.debugEnabled) {
			log.debug "PARAMS: $params"
		}

		if (params.from == null || params.to == null) {
			flash.message = message(code: "conversion.direct.call.to.export", default: "You didn't fill the form correctly")
			redirect action: "index"
			return
		} else if (params.values == null || params.values == "") {
			flash.message = message(code: "conversion.value.not.empty", default: "You need to enter a value to convert")
			redirect action: "index"
			return
		} else if (params.values?.size() > SINGLE_QUERY_MAX_LENGTH) {
			if (log.debugEnabled) {
				log.debug "query too long... using ${SINGLE_QUERY_MAX_LENGTH} first characters"
			}
			params.values = params.values?.substring(0, SINGLE_QUERY_MAX_LENGTH)
			flash.message = message(code: "conversion.value.too.long", default: "Query too long, using the first ${SINGLE_QUERY_MAX_LENGTH} characters.")
		}

		switch (params.format) {
			case "csv":
			case "CSV":
				params.format = 'csv'
				params.extension = 'csv'
				break;
			case "excel":
			case "EXCEL":
				params.format = 'excel'
				params.extension = 'xls'
				break;
			case "ods":
			case "ODS":
				params.format = 'ods'
				params.extension = 'ods'
				break;
			default:
				flash.message = message(code: "conversion.export.parameter.mismatch", default: "The export format specified is not supported...")
				redirect action: "index"
				return
		}

		//we limit the max result to 1000 hits
		params.max = params.max ?: QUERY_MAX_HITS_LIMIT
		params.offset = params.offset ?: 0
		params.total = params.total ?: 0

		if (params.format && params.format != "html") {
			def headers = []
			headers.add(params.from)
			headers.add(params.to)

			boolean applyScoring = params.scoring

			if (applyScoring) {
				headers.add(headers.indexOf(ConstantHelper.NAMES_INCHI_KEY) + 1, "Score")
			}

			def results = conversionService.convert(params?.from, params?.values, params?.to, params).collect {entry ->
				def e = new Expando();
				entry.eachWithIndex {item, i ->

					if (item.value instanceof String && !item.value.empty) {
						e.setProperty(headers[i], item.value)
					} else {
						StringBuilder data = new StringBuilder("")
						StringBuilder scores = new StringBuilder("")
						if (params.hits) {                          // return only top hit
							ScoredValue s = item.value.empty ? new ScoredValue("Not Found", 0.0) : item.value.first()
							data.append(s.value)
							if (applyScoring) {
								scores.append(s.score)
							}
						} else {                                    // return all hits
							for (ScoredValue s : item.value) {
								data.append(s.value).append("\n")
								if (applyScoring) {
									scores.append(s.score).append("\n")
								}
							}
						}
						e.setProperty(headers[i], data.toString().trim().size() == 0 ? "Not Found" : data.toString())
						if (applyScoring) {
							e.setProperty(headers[i + 1], scores.toString().trim().size() == 0 ? "" : scores.toString())
						}
					}
				}
				return e
			}

			Map labels = [:]
			labels.(headers.collectAll {it}) = headers.collectAll {it}

			Map parameters = [title: 'Conversion Data', 'column.widths': [0.4, 0.4, 0.4, 0.4]]
			Map formatters = [:]

			response.contentType = grailsApplication.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=CTS_Conversion_result.${params.extension}")
			exportService.export(params.format.toString(), response.outputStream, results, headers, labels, formatters, parameters)
		}
		return
	}

	/**
	 * renders batch conversion form
	 * @return
	 */
	def batch() {
		def fromNames = session.fromNames ?: conversionService.getFromFields()
		def toNames = session.toNames ?: conversionService.getToFields()

		session.fromNames = fromNames
		session.toNames = toNames

		render view: "batchConversion", model: [fromNames: fromNames, toNames: toNames, env: Environment.currentEnvironment]
	}

	/**
	 *  returns list of results matching 'values' converted from 'from' to 'to'
	 * @param values
	 * @param from
	 * @param to
	 * @return
	 */
	def batchConvert() {

		if (params.from == null || params.from.equals("")) {
			flash.message = message(code: "conversion.direct.call.to.batchConvert", default: "You didn't fill the form correctly, 'from' not specified")
			redirect action: "batch"/*, params: [hits: params.hits, from: params.from, to: params.to, value: params.values, scoring: params.scoring,
			                                   max: params.max, offset: params.offset, total: params.total, end: params.end, env: Environment.currentEnvironment]*/
			return
		}

		if(params.value != null && params.values == null) {
			params.values = params.value
		}

		if (params.values == null || params.values.equals("")) {
			flash.message = message(code: "conversion.direct.call.to.batchConvert", default: "You didn't fill the form correctly, invalid text to search for")
			redirect action: "batch"/*, params: [hits: params.hits, from: params.from, to: params.to, value: params.values, scoring: params.scoring,
			                                   max: params.max, offset: params.offset, total: params.total, end: params.end, env: Environment.currentEnvironment]*/
			return
		}

		if (params.to == null || params.to.equals("")) {
			flash.message = message(code: "conversion.direct.call.to.batchConvert", default: "You didn't fill the form correctly, 'To IDs' not selected")
			redirect action: "batch"/*, params: [hits: params.hits, from: params.from, to: params.to, value: params.value, scoring: params.scoring,
			                                   max: params.max, offset: params.offset, total: params.total, end: params.end, env: Environment.currentEnvironment]*/
			return
		}

		def fromNames = session.fromNames ?: conversionService.getFromFields()
		def toNames = session.toNames ?: conversionService.getToFields()

		int max = 10
		int offset = 0
		int total = 0
		def results
		int end = 0

		String from = params?.from ?: ""

		// clean list of values to search for
		List<String> values
		values = sanitizeSearchValues(params.values)
		params.values = values.join("\n")

		total = values.size()

		// to is a string if only one checkbox is selected -> need change to adjust to array now.
		List<String> to = []

		if (to instanceof String && !to.isEmpty()) {
			log.info("'to' is string!!!")
			String tmpTo = to.trim()
			to = [tmpTo]
		} else {
			log.info("'to' is array")
			to.addAll(params.to)
		}

		if (params.max && !(params.max instanceof Integer)) {
			max = Integer.parseInt(params.max)
		}

		if (params.offset && !(params.offset instanceof Integer)) {
			offset = Integer.parseInt(params.offset)
		}

		if (params.total && !(params.total instanceof Integer)) {
			total = Integer.parseInt(params.total)
		}

		if (params.hits == null) {
			params.hits = 1
		}

		if (params.hits && !(params.hits instanceof Integer)) {
			params.hits = Integer.parseInt(params.hits)
		}

		params.max = max ?: 10
		params.offset = offset ?: 0

		if (offset + max - 1 < values.size()) {
			end = offset + max - 1
		} else if (offset + max - 1 == values.size()) {
			end = values.size() - 1
		} else {
			end = BATCH_QUERY_MAX_TOKENS
		}
		params.end = end

//		log.debug("values: (${values.class.simpleName}) - ${values}")
//		log.debug("offset:${offset}, max: ${max}, values: ${values.size()}, end: ${end}\n")
//		log.debug("\tvalues to search for: values[${offset}..${end}]= ${values[offset..end]}")

		if(end > values.size()) {
			end = values.size()
		}

		results = conversionService.batchConvert(from, values[offset..<end], to, params)

		// since batchConvert is read-only... update names now
//		conversionService.updateIdNames(to)

		render(view: "batchConversion",
				model: [fromNames: fromNames, toNames: toNames, results: results, hits: params.hits,
				        from     : from.toString(), to: to, values: values, scoring: params.scoring,
				        max      : max, offset: offset, total: total, end: end, env: Environment.currentEnvironment])
	}

	def batchExport() {
		def to = []

		if (params.to instanceof String && !params.to.isEmpty()) {
			to.addAll(params.to.split('@@'))
		} else {
			to = params.to ?: []
		}

		switch (params.format) {
			case "CSV":
				params.format = 'csv'
				params.extension = 'csv'
				break;
			case "EXCEL":
				params.format = 'excel'
				params.extension = 'xls'
				break;
			case "ODS":
				params.format = 'ods'
				params.extension = 'ods'
				break;
			default:
				flash.message = message(code: "conversion.export.parameter.mismatch", default: "The export format specified is not supported...")
				redirect action: "batch"
				return
		}

		params.max = Integer.parseInt(params.max) ?: QUERY_MAX_HITS_LIMIT
		params.offset = Integer.parseInt(params.offset) ?: 0
		params.total = Integer.parseInt(params.total) ?: 0

		// clean list of values to search for
		List<String> values
		values = sanitizeSearchValues(params.values)

		def keyNumber = values.size()

		if (keyNumber > params.max) {
			params.end = BATCH_QUERY_MAX_TOKENS
		} else {
			params.end = -1
		}

		if (params?.format && params.format != "html") {

			def headers = []
			def headerIdxMap = [:]
			headers.add("Search Term")

			to.each {
				headers.add(it)
				if (params.scoring) {
					headers.add(it + "_Score")
				}
			}


			def results = conversionService.batchConvert(params?.from, values, to, params).collect {l ->
				def e = new Expando();
				l.eachWithIndex {item, i ->
					if (item.value instanceof String) {
						e.setProperty("Search Term", item.value)
					} else {
						StringBuilder data = new StringBuilder("")
						StringBuilder scores = new StringBuilder("")
						if (Integer.parseInt(params.hits)) {
							ScoredValue s = item.value.empty ? new ScoredValue("Not Found", 0.0) : item.value.first()
							data.append(s.value)
							scores.append(s.score)
						} else {
							for (ScoredValue s : item.value) {
								data.append(s.value).append("\n")
								scores.append(s.score.round(2)).append("\n")
							}
						}
						e.setProperty(item.key, data.size() == 0 ? "Not Found" : data.toString())
						if (params.scoring) {
							e.setProperty(item.key + "_Score", scores.size() == 0 ? "" : scores.toString())
						}
					}
				}
				return e
			}

			Map labels = [:]
			labels.(headers.collectAll {it}) = headers.collectAll {it}
			Map parameters = [title: 'Conversion Data', 'column.widths': [0.4] * headers.size()]
			Map formatters = [:]

			response.contentType = grailsApplication.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=CTS_Conversion_result.${params.extension}")
			exportService.export(params.format, response.outputStream, results, headers, labels, formatters, parameters)
		}
		return
	}

	/**
	 * Replaces empty lines by specific token, and trims all others.
	 * @return a list with empty lines replaced and content lines trimmed
	 */
	private List<String> sanitizeSearchValues(def input) {
		List<String> values = []
		// if keywords come in a string
		if (input instanceof String) {
			// input SHOULDN'T be null here
			if (input != null) {
				//split them by 'new lines'
				List<String> tmp = input.split("\\r?\\n").toList()
				//and collect up to MAX_TOKENS items
				def limit = tmp.size() < BATCH_QUERY_MAX_TOKENS ? tmp.size() : BATCH_QUERY_MAX_TOKENS
				values = tmp.subList(0, limit)
			}
		}
		// if keywords come as array, collect up to MAX_TOKENS itmes
		else {
			def limit = input.size() < BATCH_QUERY_MAX_TOKENS ?: BATCH_QUERY_MAX_TOKENS
			values = input.subList(0, limit-1)
		}

		//replace empty lines (including tabs and spaces) with "Empty"
		values = values.collect {(it == null || it.matches("\\s*")) ? "Missing input" : it.trim()}

		return values
	}
}
