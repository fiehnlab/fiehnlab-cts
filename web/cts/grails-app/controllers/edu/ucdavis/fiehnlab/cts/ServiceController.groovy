package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON
import groovy.json.JsonSlurper
import org.openscience.cdk.exception.CDKException

/**
 * provides us with restful methods to interact with the system. The mapping is
 *
 */
class ServiceController {
	static allowedMethods = [molToInchi         : 'POST', inchiToMol: 'POST',
	                         inchiCodeToInchiKey: 'POST', inchiKeyToMol: 'GET',
	                         smilesToInchi      : 'POST', expandFormula: 'GET']

	/**
	 * our conversion service to do the actual work
	 */
	ConversionService conversionService
	SimilarityService similarityService
	CompoundService compoundService
	MoleculeConversionService moleculeConversionService
	NameCleanupService nameCleanupService

	/**
	 * rest api to count dataSource hits for given inchiKey
	 */

	def idcount() {
		def result
		String message = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/count/<inchikey>"

		if (params.inchikey && params.inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			result = similarityService.externalIdCount(params.inchikey)
		} else {
			response.status = 400
			result = [message]
		}
		render result as JSON
	}

	/**
	 * rest api to count hits in specific dbs for given inchiKey
	 */

	def bioCount() {
		def result
		def biodbs = ['KEGG', 'Human Metabolome Database', 'BioCyc']
		def message = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/countBiological/<inchikey>"

		if (params.inchikey && params.inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			result = similarityService.bioIdCount(params.inchikey, biodbs)
		} else {

			response.status = 400
			result = [message]
		}
		render result as JSON
	}

	/**
	 * a rest based service to cdk values and should be called like this
	 *
	 * http://url/service/convert/from/to/value
	 */

	def convert() {
		def result = []
		def scoring = (params.scoring == null || params.scoring.trim().empty) ? "" : params.scoring

		try {
			if (params.value && params.from && params.to) {
				if (params.value.size() > ConversionController.SINGLE_QUERY_MAX_LENGTH) {
					throw new IllegalArgumentException("Query max length exceeded. Please use a shorter query value")
				}

				//check from and to are valid parameters
				if (!conversionService.fromFields.collect {it.toLowerCase()}.contains(params.from.toLowerCase())) {
					// invalid 'from' type
					throw new IllegalArgumentException("Parameter 'from' is invalid. Get a list of valid id types from http://cts.fiehnlab.ucdavis.edu/service/convert/fromValues")
//					response.status = 400
//					result.add(fromIdentifier: params.from, searchTerm: params.value, toIdentifier: params.to,
//							error: "Parameter 'from' is invalid. Get a list of valid id types from http://cts.fiehnlab.ucdavis.edu/service/convert/fromValues")
				} else if (!conversionService.toFields.collect {it.toLowerCase()}.contains(params.to.toLowerCase())) {
					// invalid 'to' type
					throw new IllegalArgumentException("Parameter 'to' is invalid. Get a list of valid id types from http://cts.fiehnlab.ucdavis.edu/service/convert/toValues")
//					response.status = 400
//					result.add(fromIdentifier: params.from, searchTerm: params.value, toIdentifier: params.to,
//							error: "Parameter 'to' is invalid. Get a list of valid id types from http://cts.fiehnlab.ucdavis.edu/service/convert/toValues")
				} else {
					params.value = nameCleanupService.cleanName(params.value)

					// all params are good
					def conversion
					if (params.scoring && (params.scoring.equals("biological") || params.scoring.equals("popularity"))) {
						conversion = conversionService.convert(params.from, params.value, params.to, [scoring: params.scoring, hits: 0])
					} else {
						conversion = conversionService.convert(params.from, params.value, params.to, [hits: 0])
						conversion.each {
							it[conversionService.identifiers[params.to.toLowerCase()]] = it[conversionService.identifiers[params.to.toLowerCase()]].value
						}
					}

					conversion.each {Map map ->
						List val = map.values().asList().get(1)

						if (!val.equals(null) && !val.isEmpty()) {
							result.add(fromIdentifier: params.from, searchTerm: params.value, toIdentifier: params.to, result: val)
						} else {
							result.add(fromIdentifier: params.from, searchTerm: params.value, toIdentifier: params.to, result: [])
						}
					}
				}

			} else {
				throw new IllegalArgumentException("Missing parameters. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/convert/<fromidtype>/<toidtype>/<searchterm>")
//				response.status = 400
//				result.add(fromIdentifier: params.from, searchTerm: params.value, toIdentifier: params.to,
//						error: "Missing parameters. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/convert/<fromidtype>/<toidtype>/<searchterm>")
			}
		} catch(Exception e) {
			response.status = 400
			result.add(fromIdentifier: params.from, searchTerm: params.value, toIdentifier: params.to,
					error: "${e.getMessage()?:''}")
		}

		render result as JSON
//		return
	}

	/**
	 * renders all available from conversions available
	 */
	def fromValues() {
		render conversionService.fromFields as JSON
	}

	/**
	 * render all to values
	 */
	def toValues() {
		render conversionService.toFields as JSON
	}

	/**
	 * Finds all the synonyms in the database for a specific inchiKey
	 * url: http://cts.fiehnlab.ucdavis.edu/service/synonymsForInchikey/<inchiKey>
	 * @return a list of synonym objects associated with the input inchikey
	 */
	def synonymsForInchikey() {
		def result = []
		def message = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>"

		if (params.inchikey && params.inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			result = similarityService.synonymsForInchikey(params.inchikey)
		} else {
			response.status = 400
			result = [message]
		}

		render result as JSON
	}

	/**
	 * Finds all the Compound data associated with an inchiKey
	 * The URL to call is: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchiKey>
	 * @return a compound object belonging to the requested inchikey
	 */
	def compound() {
		def comp
		def message = "You entered an invalid InChIKey. Please call the service in the following form: http://cts.fiehnlab.ucdavis.edu/service/compound/<inchikey>"

		if (params.inchiKey && params.inchiKey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			if (params.inchiKey.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")) {
				comp = Compound.get(params.inchiKey)
				if (comp.is(null)) {
					response.status = 400
					comp = ["Sorry, we couldn't find any matching results"] as JSON
				} else {
					response.status = 200
					comp = comp.toJson()
				}
			} else {
				response.status = 400
				comp = [message] as JSON
			}
		} else {
			response.status = 400
			comp = [message] as JSON
		}

		render comp
	}

	public void setConversionService(ConversionService cs) {
		this.conversionService = cs
	}

	public ConversionService getConversionService() {
		return this.conversionService
	}

	/**
	 * Returns the name of a Pubchem Depositor based on its id, first looking into the cts db and if the item is not found it asks pubchem for a name.
	 * The URL to call is: http://url[:port]/service/sourceName/pcid
	 * @param pcid the (pubchem) depositor id
	 * @returns a string with the depositor name or an empty string in case of the id not being found
	 */
	def sourceName() {
		String idname

		try {
			if (params.pcid && Integer.parseInt(params.pcid)) {      // if number is integer, try to get the name associated
				idname = IdName.findByPcId(params.pcid)?.idName     // in the cts db
			} else {
				idname = ""
				response.status = 400
			}
		} catch (NumberFormatException e) {
			idname = ""
			response.status = 400
		}

		render idname == null ? "" : idname
	}

	def pubchemDepositor() {
		String idname = null
		String pug = ""

		if (params.type == null || params.type.trim().empty) {
			render ""
			response.status = 400
			return
		}

		try {
			if (params.type instanceof String && !params.type.empty) {
				if (Integer.parseInt(params.sid) && !params.sid.empty) {
					pug = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/${params.type}/sid/${params.sid}/xrefs/SourceName/JSON"
				}
			}

			def dsid = (params.dsid == null || params.dsid.empty) ? "" : params.dsid

			/* response format (JSON):
			 *		{"InformationList": {
			 *			"Information": [{
			 *              "SID": 126523020,
			 *				"SourceName": ["Human Metabolome Database"]
			 *			}]
			 *		}}
			 */

			def json = new JsonSlurper().parseText(pug.toURL().text)
//			log.debug("depositor name: ${json.InformationList.Information[0].SourceName[0]}")

			idname = json.InformationList.Information[0].SourceName[0]

			if (!idname.empty) {
				if (IdName.find { idName == 'MOLI' } != null) {
					def newName = new IdName(idName: idname, pcId: dsid, hits: 0)
					newName.save(flush: true, failOnError: true)
				}
			}

		} catch (NumberFormatException e) {
			log.error("sid is not a number. ${e.message}")
		} catch (Exception e) {     // in case of a connection failure
			log.error("error trying to connect ${e.message}")
		}

		render idname == null ? "" : idname
	}

	def smilesToInchi() {

		def res
		log.info("smiles: ${params.smiles}\nparams: $params")
		if (params.smiles != null && !params.smiles.trim().empty) {
			def msg = ''
			try {
				response.status = 200
				res = moleculeConversionService.smilesToInchicode(params.smiles.trim())
			} catch (CDKException ex) {
				// error generating inchiKey
				msg = "Error generating InChI Key: ${ex.getMessage()}"
				log.error(msg, ex)
				response.status = 400
				res = [error: msg]
			} catch (IllegalArgumentException ex) {
				msg = "Found an invalid molecule block in the file. ${ex.getMessage()}"
				log.error(msg, ex)
				response.status = 400
				res = [error: msg]
			} catch (Exception ex) {
				msg = "Something bad happened.\n${ex.getMessage()}"
				log.error(msg, ex)
//                e.printStackTrace()
				response.status = 400
				res = [error: msg]
			}
		} else {
			response.status = 400
			res = [error: "Invalid smiles definition. The only format supported is smiles or you did not specify the smiles code in the parameter 'smiles'. Smiles: '$params.smiles'"]
		}

		render res as JSON
	}

	def molToInchi() {
		def res

		if (params.mol != null && !params.mol.trim().empty) {
			try {
				response.status = 200
				res = moleculeConversionService.molToInchi(params.mol.trim())
			} catch (Exception e) {
				response.status = 400
				res = [error: "Invalid molecule definition. The only format supported is MDL."]
			}
		} else {
			response.status = 400
			res = [error: "Invalid molecule definition. The only format supported is MDL or you did not specify the mol file in the parameter 'mol'. Mol: '$params.mol'"]
		}

		render res as JSON
	}

	def inchiToMol() {
		def res

		if (params.inchicode != null && !params.inchicode.trim().isEmpty()) {
			try {
				res = moleculeConversionService.inchiToMol(params.inchicode.trim())
			} catch (Exception e) {
				response.status = 400
				log.error("got exception: $e.message")
				res = [error: e.message]
			}
		} else {
			response.status = 400
			log.error("Recived empty or null inchi code")
			res = [error: "Invalid InChI code. The parameter 'inchicode' can't be empty or null."]
		}

		render res as JSON
	}

	def inchiCodeToInchiKey() {
		def res

//		if (params.inchicode != null && !params.inchicode.trim().isEmpty()) {
		try {
			res = moleculeConversionService.inchiToInchiKey(params.inchicode?.trim())
		} catch (Exception e) {
			response.status = 400
			log.error("got exception: $e.message")
			res = [error: e.message]
		}
//		} else {
//			response.status = 400
//			log.error("Recived empty or null inchi code")
//			res = [error: "Invalid InChI code. The parameter 'inchicode' can't be empty or null."]
//		}

		render res as JSON
	}

	def inchiKeyToMol() {
		def res

		if (params.inchikey != null && !params.inchikey.trim().isEmpty()) {
			try {
				response.status = 200
				res = moleculeConversionService.inchiKeyToMol(params.inchikey.trim())
			} catch (Exception e) {
				response.status = 400
				log.error("got exception: ${e.message}")
				res = [error: e.message]
			}
		} else {
			response.status = 400
			log.error("Recived empty or null inchikey")
			res = [error: "Invalid InChIKey. The parameter 'inchikey' can't be empty or null."]
		}

		render res as JSON
	}

	def expandFormula() {
		def res = [:]

		if (params.formula == null || params.formula.isEmpty()) {
			response.status = 400
			def msg = "It would be nice to create something out of nothing. Please provide a valid molecular formula."
			log.error(msg)
			res = [formula: params.formula, result: "", error: msg]
		} else {

			def formula = params.formula

			if (formula.matches("(?i)[a-z]+(?:\\d*?[a-z]*?)*")) {
				try {
					res = [formula: formula, result: compoundService.getExpandedFormula(formula)]
					response.status = 200
				} catch( Exception e) {
					response.status = 400
					res = [formula: formula, result: "", error: e.getLocalizedMessage()]
				}
			} else {
				response.status = 400
				res = [formula: formula, error: "Invalid molecular formula '$formula'", result: ""]
			}
		}

		render res as JSON
	}
}
