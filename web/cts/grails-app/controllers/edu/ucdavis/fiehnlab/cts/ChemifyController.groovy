package edu.ucdavis.fiehnlab.cts

import com.google.gson.GsonBuilder
import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.io.HitSerializerJSONImpl
import grails.converters.JSON

class ChemifyController {

	ChemifyService chemifyService

	def index() {

		String config = chemifyService.configuration
		render config

	}

	def identify() {

		String value = params.value?.trim()

		if (value == null || value.isEmpty()) {
			def message = ['error': "Can't search for a null or empty value. Please call the service like: " +
					"'http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/<value>' or " +
					"'http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/query?value=<search term>'."]

			response.status = 400

			render message as JSON
		} else {
			value = value.replaceAll("[\"]", "")
			try {
				List<? extends Hit> result = chemifyService.identify(value)
//				log.debug("chemify results: $result")
				def data = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().registerTypeHierarchyAdapter(Hit.class, new HitSerializerJSONImpl()).create().toJson(result);
//				log.debug("chemify results: ${data.class.name}")
				render data

			} catch (Exception e) {
				response.status = 400
				render([error: e.message]) as JSON
			}
		}
	}
}
