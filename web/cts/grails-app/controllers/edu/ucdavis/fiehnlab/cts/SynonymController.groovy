package edu.ucdavis.fiehnlab.cts

class SynonymController {

	/*
	 * default asction to redirect to index page, since there's nothing to see here
	 */
	def index() {
		redirect controller: "index"
		return
	}
}
