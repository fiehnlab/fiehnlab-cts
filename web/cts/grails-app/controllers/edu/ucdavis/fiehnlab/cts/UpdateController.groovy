package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.cts.exceptions.InvalidInchiException

/**
 * This Controller recieves a request to register an inchi key on the CTS db checking the parameters and calls the service that inserts the new compound
 */
class UpdateController {
	static allowedMethods = [registerInchi: 'POST']
	static responseFormat = ['JSON']

	UpdateService updateService
	MoleculeConversionService moleculeConversionService;

	def registerInchi() {
//		response.contentType = "application/json"
		def msg = [error:"""Please provide a valid inchikey or inchi code or mol definition and a name in a json string like\n{"inchicode":"InChI=xxxxxx", "searchTerm":"compound x", "inchikey":"ZZZZZZZZZZZZZZ-ZZZZZZZZZZ-Z", "mol":"<valid molecule definition in MDL>"}"""]

		if (!params) {
//			log.error(msg)
			response.status = 400
			params.remove 'action'
			params.remove 'controller'
			render msg.params=params
			return
		}

		def inchikey = (params.inchikey && params.inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) ? params.inchikey : ""
		def inchicode = (params.inchicode && params.inchicode.matches(PatternHelper.STD_INCHI_PATTERN)) ? params.inchicode : ""

		if (!inchikey) {
//			log.debug("have to generate inchikey from somewhere")

			if (!inchicode) {
//				log.debug("checking mol definition")
				// try to convert mol to inchi
				if (!params.mol) {
					log.error(msg)
					response.status = 400
					params.remove 'action'
					params.remove 'controller'
					render msg.params=params
					return
				} else {
					//convert from mol to inchikey and register
					try {
						inchikey = moleculeConversionService.molToInchi(params.mol)
					} catch(Exception e) {
						response.status = 400
						params.remove 'action'
						params.remove 'controller'
						msg = [error:e.message, params:params]
					}
				}
			} else {
				try {
					inchikey = moleculeConversionService.inchiToInchiKey(inchicode)
				} catch (InvalidInchiException e) {
					response.status = 400
					params.remove 'action'
					params.remove 'controller'
					msg = [error: e.message, params: params]
				}
			}
		}

		def res
		try {
			res = updateService.registerCompound(inchikey.toString(), params.searchTerm).result
		} catch (Exception e) {
			response.status = 400
			params.remove 'action'
			params.remove 'controller'
			msg = [error: e.message, params:params]
		}

		if (res != null) {
//	log.debug("RESPONSE: $res")
			if (res.message.contains("registered compound")) {
				response.status = 204
				msg = "Compound registered: ${res.compound.inchiKey}"
//				log.info("$msg\n$res")
			} else {
				response.status = 204
				msg = "Compound not registered: ${inchikey}"
//				log.warn("$msg\n$res")
			}
		}

		render msg
		return
	}
}
