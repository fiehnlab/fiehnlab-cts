package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/9/14
 * Time: 4:38 PM
 */
class SpringConfigController {

    def grailsApplication

    def list(){

        def res = []

        grailsApplication.mainContext
                .beanDefinitionNames.sort().each {
            res.add(it)
        }

        render res as JSON
    }
}
