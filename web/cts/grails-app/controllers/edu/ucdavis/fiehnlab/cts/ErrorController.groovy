package edu.ucdavis.fiehnlab.cts

class ErrorController {

	def serverError() {
		this.properties.each {
			log.info("${it.key} = ${it.value}")
		}
		def exception = request.exception
		render(view: "/error", model: [exception: exception])
	}
}
