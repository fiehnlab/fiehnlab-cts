package edu.ucdavis.fiehnlab.cts

import grails.converters.JSON
import grails.util.Environment
import org.apache.log4j.Logger

import static groovyx.gpars.GParsExecutorsPool.withPool

/**
 * simple ajax driven controller to help us with system properties
 */
class InfoController {
	private static Logger log = Logger.getLogger(InfoController.class.name)
	static allowedMethods = [getExternalIdScore: 'GET', getAllExtIdScores: 'GET']
	def infoService

    /**
     * current system stats, about the available count of compounds and so
     */
    def ajaxGetSystemStats() {

        //get the data and cache it for the time being
        if(params.force != null | session["system_stats"] == null){
            session["system_stats"] = infoService.counts
        }

        render session["system_stats"] as JSON
    }

    /**
     * are we in the development environment
     */
    def ajaxIsDevelopmentEnvironment() {
          render (Environment.current == Environment.DEVELOPMENT || Environment.current == Environment.TEST)
    }

	def getExternalIdScore() {
		log.info "PARAMS: ${params}"

		def name = params.name
		def data = []

		if (name == null || name.equals("")) {
			response.status = 400
			data = [external_id: "invalid", count: 0, error: "parameter 'name' cannot be null or empty."]
			render data as JSON
			return
		}

		data = infoService.getExternalIdScore(params.name)
		if (data.external_id == 'inexistent') {
			response.status = 400
			data = [external_id: name, count: 0, error: "External id name doesn't exist"]
		}
		log.trace "DATA: $data"

		render data as JSON
	}

	def getAllExtIdScores() {
		List<String> extIds = IdName.list().idName

		def res = Collections.synchronizedMap(new HashMap<String, Integer>())

		Closure getSingleScore = {String idName ->
			def score = infoService.getExternalIdScore(idName)
			log.trace("result from service: $score")
			def result = [(score.external_id): score.count]
			log.trace("result to controller: $result")
			return result
		}

		withPool(5) {
			extIds.unique().eachParallel {idName ->
				log.debug("single result: ${idName}")
				idName.withNewSession {session ->
					Map<String, Integer> item = getSingleScore(idName)
					log.trace("single result: ${item}")
					res.put(idName, item[(idName)])
				}
			}

			log.debug("final result: $res")
		}


		render(res.sort {-it.value} as JSON)
	}

}
