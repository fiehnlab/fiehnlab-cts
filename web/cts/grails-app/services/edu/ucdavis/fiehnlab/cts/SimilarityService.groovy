package edu.ucdavis.fiehnlab.cts
import groovy.sql.Sql
import org.springframework.transaction.annotation.Transactional

class SimilarityService {

	def dataSource

	/**
	 * Gets the external_id count for an inchiKey
	 *
	 * @param params.inchiKey String representing the inchiKey to search for
	 * @return Map representing the number of external ids that the inchiKey has in the form: [datasource_count: Integer]
	 */
	@Transactional(readOnly = true)
	def externalIdCount(inchikey) {
		def result = [:]
		if (inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			def query = "select count(*) as count from external_id where inchi_key = ?"
			try {
				new Sql(dataSource: dataSource).eachRow(query, [inchikey]) {
					result.put("datasource_count", it.count)
				}
			}
			catch (Exception e) {
				log.error("Exception : " + e)
			}
		} else {
			result.put("datasource_count", "Please provide a valid InchiKey")
		}

		return result
	}

	@Transactional(readOnly = true)
	def bioIdCount(inchikey, List<String> biodbs) {
		def result = [:]
		def qdb = "?," * biodbs.size()

		if (inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			def query = "select name, count(*) as count from external_id where inchi_key = ? and name in (${qdb[0..-2]}) group by name order by count desc, name"
			def total = 0;

			try {
				def sql = new Sql(dataSource: dataSource)

				sql.eachRow(query, [inchikey] + biodbs) {
					result.put(it.name, it.count)
					total += it.count
				}
				result.put("total", total)
			} catch (Throwable t) {
				log.error("Throwable: ${t.message}")
			} catch (Exception e) {
				log.error("Exception : ${e.message}")
			}
		} else {
			result = [error: "Please provide a valid InchiKey"]
		}

		return result
	}

	@Transactional(readOnly = true)
	Set<String> synonymsForInchikey(String inchikey) {
		Set<String> result = new HashSet<String>()

		if (inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			def query = "select name from synonym where inchi_key = ? order by name"

			try {
				new Sql(dataSource: dataSource).eachRow(query, [inchikey]) {
					result.add(it.name)
				}

			} catch (Throwable t) {
				log.error("Throwable: ${t.message}")
			} catch (Exception e) {
				log.error("Exception: ${e.message}")
			}
		} else {
			result = ["Please provide a valid InchiKey"]
		}

		return result
	}
}

