package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Scored
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit
import edu.ucdavis.fiehnlab.chemify.identify.cts.CTSConvertIdentify
import edu.ucdavis.fiehnlab.chemify.identify.cts.CTSInChIKeyIdentify
import edu.ucdavis.fiehnlab.chemify.scoring.cts.AverageCombinedScore
import org.springframework.transaction.annotation.Transactional

class ConversionService {
	static final String CHEMICAL_NAME = "Chemical Name"
	static final String INCHI_KEY = "InChIKey"
	static final String INCHI_CODE = "InChI Code"
	Map<String, String> identifiers = new HashMap<String, String>()

	NameCleanupService nameCleanupService
	SearchService searchService
	ChemifyService chemifyService
	AverageCombinedScore averageCombinedScore

	/**
	 * Function that searches the compounds that the user wants to convert and finds out if they can be converted
	 * @param pvalue value to search for
	 * @param pfrom type of search
	 * @return an ArrayList of items converted to the desired type.
	 */
	@Transactional
	def convert(def from, def value, def to, def params = [:]) {
		if (identifiers.size() < 1) {
			initializeIds()
		}

		//check that we have all params we need
		assert from
		assert to
		assert value

		def pfrom = identifiers[from.trim().toLowerCase()] ?: ""
		def pto = identifiers[to.trim().toLowerCase()] ?: ""
		def pvalue = nameCleanupService.cleanName(value) ?: ""

		def totmp = []
		def tmp = []    //temp list of inchikeys to score
		List<Scored> scored = []     //scored values to search for destination values

		if (log.debugEnabled) {
			log.debug "About to convert '${value}' from '$from' to '$to'..."
		}

		switch (pfrom) {
			case CHEMICAL_NAME:
				// identify using Chemify
				def idStartTime = System.currentTimeMillis()
				List<? extends Hit> identified = chemifyService.identify(pvalue)
				log.debug("[CTS-Timing] ConversionService identification time: ${System.currentTimeMillis() - idStartTime}ms")

				identified = identified.findAll { !it.inchiKey.toLowerCase().equals("nothing found")}

				//log.debug("FIEHNLAB-169: $identified\n ${identified.collect {it.toString().concat(" == ").concat(it.class.simpleName)}}")
				for (Hit sh : identified) {
					if (sh instanceof ScoredHit) {
						log.debug("Enhancements: ${sh.enhancements.toListString()}")
						sh.enhancements.clear()
					}
				}

				//should get metrics, enhance hits and possibly save in db to corresponding inchikey

				//score by averageCombinedScorer...
				def scoreStartTime = System.currentTimeMillis()
				scored.addAll(averageCombinedScore.score(identified, pvalue))
				log.debug("[CTS-Timing] ConversionService scoring time: ${System.currentTimeMillis() - scoreStartTime}ms")

				break
			case INCHI_KEY:
				//no need to look for inchikeys already have them
				//turn into List<Hit>
				def idStartTime = System.currentTimeMillis()
				tmp.add(new HitImpl(pvalue, new CTSInChIKeyIdentify(), pfrom, pfrom))
				log.debug("[CTS-Timing] ConversionService identification time: ${System.currentTimeMillis() - idStartTime}ms")

				//score by whatever algorithm requested...
				def scoreStartTime = System.currentTimeMillis()
				scored.addAll(averageCombinedScore.score(tmp, pvalue))
				log.debug("[CTS-Timing] ConversionService scoring time: ${System.currentTimeMillis() - scoreStartTime}ms")

				break
			default:
				// gather inchikey
				ExternalId.findAllByNameAndValue(pfrom, pvalue).each {inchi ->
					log.debug("found (${inchi.class.simpleName}): $inchi")
					tmp.add(new HitImpl(inchi.inchiKey.inchiKey, new CTSConvertIdentify(), pfrom, pfrom))
				}

				//make list<scored> with inchikeys in tmp
				//score by whatever algorithm requested...
				scored.addAll(averageCombinedScore.score(tmp, pvalue))

				break
		}

		//gather destination values, or empty list if not found
		def id2StartTime = System.currentTimeMillis()
		totmp = findTo(pto, scored) ?: []
		log.debug("[CTS-Timing] ConversionService identification time: ${System.currentTimeMillis() - id2StartTime}ms")

		//arrange in map and add to result list
		def fixStartTime = System.currentTimeMillis()
		def comps = [[("f${pfrom}".toString()): pvalue, ("${pto}".toString()): totmp]]
		log.debug("[CTS-Timing] ConversionService sorting results time: ${System.currentTimeMillis() - fixStartTime}ms")


		if (log.debugEnabled) {
			log.debug "...found ${comps.size()} items"
		}

		return comps
	}

	/**
	 * finds a value for each of the given inchikeys
	 * @param to the destination type
	 * @param inchiList list of identified inchikeys
	 * @return list of converted values for the list of inchikeys
	 */
	List<ScoredValue> findTo(String to, List<Scored> inchiList) {
		List<ScoredValue> res = []
		if (to.equals(INCHI_KEY)) {
			res.addAll(inchiList.collect {
				if (it.inchiKey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
					new ScoredValue(it.inchiKey, it.score)
				}
			})
		} else {
			switch (to) {
				case CHEMICAL_NAME:
					inchiList.each {
						// get good names from Chemspider
						def name = Synonym.findAllByInchiKey(Compound.get(it.inchiKey)).name

//						def name = ChemspiderRester.getFilteredNamesForInchikey(it.inchiKey)
//
//						def xxnames = ctsNames.intersect(name)
//						log.debug("\nintersect NAMES: $xxnames\n (no commons: ${ctsNames.disjoint(name)})")

						if (name) {
							for (String n : name) {
								log.debug("name: $n")
								def score = new ScoredValue(n.replaceAll("\\r?\\n", ""), it.score)
								if (res.value.contains(n)) {
									//log.debug("idx: ${res.get(res.value.indexOf(n))}")
									if (res.get(res.value.indexOf(n)).score < score.score) {
										//log.debug("1 removing lower scored name: $n")
										res.remove(res.value.indexOf(n))
										//log.debug("1 adding name: $n")
										res.add(score)
									} else {
										//log.debug("skipped: $score")
									}
								} else if (!res.contains(score)) {
									//log.debug("2 adding name: $n")
									res.add(score)
								}
							}
						} else {
							def names = Synonym.findAllByInchiKey(Compound.get(it.inchiKey))

							if (name.size() > 0) {
								name = names.find {n -> n.type.equals("IUPAC Name (Preferred)")}.name
								def score = new ScoredValue(name.replaceAll("\\r?\\n", ""), it.score)
								res.add(score)
							}
						}
					}

					break
				case INCHI_CODE:
					inchiList.each {
						// get inchi code for given inchikey
						Compound cmp = Compound.findByInchiKey(it.inchiKey)
						if (cmp) {
							// add scored value based in found inchikey score
							def score = new ScoredValue(Compound.findByInchiKey(it.inchiKey)?.inchiCode, inchiList[0].score)
							res.add(score)
						}
					}
					break
				default:
					inchiList.each {ik ->
						// get external ids for inchikey
						def ids = Compound.findByInchiKey(ik.inchiKey)?.extIds?.sort {it.value}
						if (ids) {
							ids.each {id ->
								if (id.name.equals(to)) {
									def score = new ScoredValue(id.value, ik.score)
									if (!res.contains(score)) {
										res.add(score)
									}
								}
							}
						}
					}
					break
			}
		}

		return res
	}

	/**
	 * Function that searches the compounds that the user wants to convert and finds out if they can be converted
	 * @param from type of value to search for
	 * @param values values to search for
	 * @params toNames array of types of values to convert to
	 * @return list of items that can be converted to the desired type
	 */
	@Transactional
	def batchConvert(String from, List<String> values, List<String> toNames, def params = [:]) {
		if (identifiers.size() < 1) {
			initializeIds()
		}

		//pfrom should always be a string
		def pFrom = identifiers[from.trim().toLowerCase()] ?: ""
		def pToNames = []
		toNames.each {tname ->
			pToNames.add(identifiers[tname.trim().toLowerCase()])
		}

		def start = params.offset
		def end = params.end


		Map<String, List> keyMapper = [:]

		values.each {word ->
			def lWord = word.trim().toLowerCase()
			if (keyMapper.containsKey(lWord)) {
				keyMapper[lWord].add(word)
			} else {
				keyMapper[lWord] = [word]
			}
		}

		def res = []

		// cycle through submitterd searchTerms list
		for (String searchTerm : values) {
			// flags the outer loop to continue on next name since we already searched for current name
			def nextName = false

			//create key
			String fKey = "f${pFrom}".toString()

			//check if map for searchTerm is in resultList, and re-add as needed (in case there are duplicated search terms)
			for (Map item : res.clone()) {
				if (item.find {k, v -> k.equals(fKey) && v.equals(searchTerm)}) {
					res.add(item)
					nextName = true
					break
				}
			}
			if (nextName) {
				continue
			}

			//map for each searchTerm result
			def searchTermResult = [(fKey): searchTerm]

			//cycle through ToNames
			for (String destination : pToNames) {
				def toMap = []

				// call simpleConversion -- should return a list of 1 map ('from type': 'search term', 'destination': [values])
				def simpleRes = convert(pFrom, searchTerm, destination)[0]

				for (ScoredValue value : simpleRes[destination]) {
					if (!toMap.contains(value)) {
						toMap.add(value)
					}
				}
				// add results for single 'toName' to searchTerm map
				searchTermResult.put(destination, toMap)
			}

			// add map to results list?
			res.add(searchTermResult)
		}

		return res
	}

	/**
	 * from where can we cdk
	 * @return list of names
	 */
	ArrayList<String> getFromFields() {
		def idNames = []
		log.debug("getting fromNames")

		String query = "select idName from IdName where hits < 0 order by hits desc, idName asc"
		def r = IdName.executeQuery(query)

		idNames.addAll(r)
		idNames.add(CHEMICAL_NAME)
		idNames.add(INCHI_KEY)
		idNames.sort()

		query = "select idName from IdName where hits >= 0 order by hits desc, idName asc"
		r = IdName.executeQuery(query)

		idNames.addAll(r)

		return idNames
	}

	/**
	 * to what can we cdk
	 */
	ArrayList<String> getToFields() {
		def idNames = []
		log.debug("getting toNames")

		String query = "select idName from IdName where hits < 0 order by hits desc, idName asc"
		def r = IdName.executeQuery(query)

		idNames.addAll(r)
		idNames.add(CHEMICAL_NAME)
		idNames.add("InChIKey")
		idNames.add("InChI Code")
		idNames.sort()

		query = "select idName from IdName where hits >= 0 order by hits desc, idName asc"
		r = IdName.executeQuery(query)

		idNames.addAll(r)

		return idNames
	}

	/**
	 * Updates the hit count of the names passed as params.
	 * (A hit is when a conversion was made to that name)
	 *
	 * @param names A Set of names to be updated
	 */
	def updateIdNames(List names) {
		if (!names.isEmpty()) {
			names.each {name ->
				IdName.executeUpdate("UPDATE IdName SET hits = hits+1 WHERE idName = '${name}' and hits >= 0")
			}
		}
	}

	protected void initializeIds() {
		for (String id : getToFields()) {
			identifiers.put(id.toLowerCase(), id.toString())
		}
	}

	ArrayList<Map> querySynonymByName(String name, String dest) {
		List<? extends Hit> inchis
		def hits = []
		def scored = []

		inchis = Synonym.findAllWhere(name: name).collect {synonym ->
			new HitImpl(synonym.inchiKey.inchiKey, new CTSConvertIdentify(), name, name)
		}

		scored.addAll(averageCombinedScore.score(inchis, name))

		//gather destination values, or empty list if not found
		def totmp = findTo(dest, scored) ?: []

		//inchis.inchiKey is a Compound object... i need to get the inchiKey from that object
		def resMap = [['fChemical Name': name, (dest): totmp]]

		return resMap
	}
}
