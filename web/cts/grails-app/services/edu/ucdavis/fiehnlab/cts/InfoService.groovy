package edu.ucdavis.fiehnlab.cts

import grails.util.Environment
import org.apache.log4j.Logger

class InfoService {
	private static Logger logger = Logger.getLogger(InfoService.class)
	def sessionFactory
	def sql

	def getCounts() {
		def session = sessionFactory.getCurrentSession()
		def comps, noComp
		def synons, noSyns
		def extids, noIds

		if (Environment.getCurrent().equals(Environment.PRODUCTION)) {
			comps = session.createSQLQuery("select reltuples from pg_class where relname='compound'")
			synons = session.createSQLQuery("select reltuples from pg_class where relname='synonym'")
			extids = session.createSQLQuery("select reltuples from pg_class where relname='external_id'")

			noComp = (Integer)comps.list().get(0)
			noSyns = (Integer)synons.list().get(0)
			noIds = (Integer)extids.list().get(0)
        } else {
			noComp = Compound.count()
			noSyns = Synonym.count()
			noIds = ExternalId.count()
		}

		[compounds: noComp, synonyms: noSyns, ids: noIds]
    }

	def getExternalIdScore(String name) {

		def updateQry = "update IdName set score = :newValue where lower(IdName) = :lowerName"
		int score = 0

		if (name == null || name.trim() == "") {
			return [external_id: 'inexistent', count: 0]
		}

		def idName = IdName.find("from IdName as i where lower(i.idName) = ?", [name.toLowerCase()])

		if (idName == null) {
			return [external_id: 'inexistent', count: 0]
		} else {
			score = idName?.score ?: 0

			if (score <= 0) {
				def start = System.currentTimeMillis()
				score = ExternalId.countByName(name)
				logger.info "query time: ${(System.currentTimeMillis() - start) / 1000}s"

				IdName.executeUpdate(updateQry, [newValue: score, lowerName: name.toLowerCase()])
			}
		}

		return [external_id: name, count: score]
	}
}
