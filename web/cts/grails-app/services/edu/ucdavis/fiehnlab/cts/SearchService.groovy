package edu.ucdavis.fiehnlab.cts

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.springframework.transaction.annotation.Transactional

import groovy.sql.Sql

/**
 * provides us with access to execute searches against the database
 */
class SearchService {

	def dataSource

	boolean enableMatchQueries = false

	/**
	 * This function carries out a simple search of 'pvalue' on ALL possible 'types'
	 * @param pvalue Value to search for
	 * @return List of compounds found
	 */
	@Transactional(readOnly = true)
	def search(params) {
		//converting wildcards     - not longer needed we do full text searches now
		//params.query = params.query.toString().replaceAll("\\*","%")

		//return Compound.executeQuery("select distinct a from Compound a, IN(a.extIds) i, IN(a.synonyms) s where a.id = ? or i.value = ? or lower(s.name) like ? ",[pvalue,pvalue,pvalue.replaceAll("\\*","\\%").toString().toLowerCase()], [max: params.max, offset: ])

		if (params.max instanceof String) {
			try {
				params.max = Integer.parseInt(params.max)
			} catch (NumberFormatException e) {
				params.max = 10
			}
		} else if (params.max == null) {
			params.max = 10
		}
		if (params.offset instanceof String) {
			try {
				params.offset = Integer.parseInt(params.offset)
			} catch (NumberFormatException e) {
				params.offset = 0
			}
		} else if (params.offset == null) {
			params.offset = 0
		}

		def result = []

		String queryString = params.query.toString().trim()

		//if it matches an inchi key we can simplify the query
		if (queryString.matches(/^([0-9A-Z\-]+)$/) && queryString.size() == 27) {
			log.info("using simplistic query...")
			result = [queryString]
		}
		//this is always some form of external id
		else if (queryString.matches(/^([0-9A]+)$/) || queryString.contains("_")) {
			def query = """
                    SELECT external_id.inchi_key AS inchikey FROM external_id where lower(value) = ? OFFSET ${params.offset} LIMIT ${params.max}
		        """

			log.info("execute query: ${query}")
			new Sql(dataSource: dataSource).eachRow(query, [queryString]) {
				result.add((it.inchikey))
			}

		} else {
			//matching using tsv vectors is enabled
			if (enableMatchQueries) {
				if (queryString.trim().contains(" ")) {
					def query = """
		                select  inchikey from (
		                    select inchikey from (
		                        SELECT distinct synonym.inchi_key AS inchikey FROM synonym where lower(name) = ? OFFSET ${params.offset} LIMIT ${params.max}
		                    ) a
		                    UNION
		                    select distinct inchikey from (
		                        SELECT  synonym.inchi_key AS inchikey FROM synonym where (tsv_syns @@ plainto_tsquery('english',?))  OFFSET ${params.offset} LIMIT ${params.max}
		                    ) b
		                ) c
				        """

					log.info("execute query: ${query}")
					new Sql(dataSource: dataSource).eachRow(query, [queryString, queryString]) {
						result.add((it.inchikey))
					}
				}

				//else we have to use a text search approach
				else {
					log.info("using text search query...")

					def query = """
                select  inchikey from (
                    select inchikey from (
                        SELECT distinct synonym.inchi_key AS inchikey FROM synonym where lower(name) = ? OFFSET ${params.offset} LIMIT ${params.max}
                    ) a
                    UNION
                    select  inchikey from (
                        SELECT distinct inchi_key AS inchikey FROM synonym where (tsv_syns @@ plainto_tsquery('english',?)) OFFSET ${params.offset} LIMIT ${params.max}
                    ) b
                    UNION
                    select inchikey from (
                        SELECT distinct external_id.inchi_key AS inchikey FROM external_id where lower(value) = ? OFFSET ${params.offset} LIMIT ${params.max}
                    ) c
                ) d
		        """

					log.info("execute query: ${query}")
					new Sql(dataSource: dataSource).eachRow(query, [queryString, queryString, queryString.toLowerCase()]) {
						result.add((it.inchikey))
					}

				}
			}
			//matching using tsv vectors is disabeled so let's only use exact matches
			else {
				def query = """
                select  inchikey from (
                    select inchikey from (
                        SELECT distinct synonym.inchi_key AS inchikey FROM synonym where lower(name) = ? OFFSET ${params.offset} LIMIT ${params.max}
                    ) a
                    UNION
                    select inchikey from (
                        SELECT distinct external_id.inchi_key AS inchikey FROM external_id where lower(value) = ? OFFSET ${params.offset} LIMIT ${params.max}
                    ) c
                ) d
		        """

				log.info("execute query: ${query}")
				new Sql(dataSource: dataSource).eachRow(query, [queryString.toLowerCase(), queryString.toLowerCase()]) {
					result.add((it.inchikey))
				}

			}
		}
		//a space is a dead giveaway that we are not searching for an external id

        return Compound.findAllByIdInList(result)
	}
}
