package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.chemify.utils.chemspider.ChemspiderRester
import edu.ucdavis.fiehnlab.cts.exceptions.InvalidInchiException
import edu.ucdavis.fiehnlab.cts.exceptions.UnsupportedFormatException
import net.sf.jniinchi.INCHI_RET
import org.openscience.cdk.*
import org.openscience.cdk.exception.CDKException
import org.openscience.cdk.graph.ConnectivityChecker
import org.openscience.cdk.inchi.*
import org.openscience.cdk.interfaces.IAtomContainer
import org.openscience.cdk.io.*
import org.openscience.cdk.io.formats.IChemFormat
import org.openscience.cdk.layout.StructureDiagramGenerator
import org.openscience.cdk.tools.CDKHydrogenAdder
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator
import org.openscience.cdk.tools.manipulator.ChemFileManipulator

class MoleculeConversionService {

	/**
	 * converts a smile to an inchi code (using chemspider service)
	 *
	 * @param smile
	 */
	def smilesToInchicode(String smiles) throws CDKException, IllegalArgumentException, Exception {
		return ['inchicode': ChemspiderRester.getSmilesToInchi(smiles)]
	}

	/**
	 * Returns the InChI Code and InChYKey pair that belongs to the given molecule definition (MDL)
	 * @param inchicode
	 * @return
	 * @throws InvalidInchiException
	 */
	def molToInchi(String mol) throws UnsupportedFormatException {
		String inchikey = ""
		String inchicode = ""
		InChIGenerator inchigen
		def reader

		if (mol == null || mol.trim().isEmpty()) {
			throw new UnsupportedFormatException("Empty molecule definition not supported.")
		}

		Reader stringReader = new StringReader(mol)

		FormatFactory factory = new FormatFactory()
		IChemFormat format = factory.guessFormat(stringReader)

		if (format != null && format.readerClassName.contains("MDL")) {
			reader = new MDLReader(stringReader, IChemObjectReader.Mode.RELAXED)
		} else {
			throw new UnsupportedFormatException("Format not supported.\nTried to read format: $format\nReader contained:\n$stringReader")
		}

		def chemfile = reader.read(new ChemFile())
		reader.close()

		IAtomContainer molecule = ChemFileManipulator.getAllAtomContainers(chemfile).get(0)

		try {
			//adding explicit hydrogens
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance())
			AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(molecule)
			adder.addImplicitHydrogens(molecule)

			// get basic compound info.
			inchigen = InChIGeneratorFactory.getInstance().getInChIGenerator(molecule)

			inchikey = inchigen.getInchiKey()  // generate the inchiKey based on mol structure
			inchicode = inchigen.getInchi()    // get inchi code

		} catch (CDKException ex) {
			// error generating inchiKey
			log.error("Error generating InChI Key: ${ex.getMessage()}\n${inchigen.log}", ex)
		} catch (IllegalArgumentException ex) {
			log.error("Found an invalid molecule block in the file. ${ex.getMessage()}")
		} catch (Exception ex) {
			log.error("Something bad happened.\n${ex.getMessage()}", ex)
		}

		return ['inchikey': inchikey, 'inchicode': inchicode]
	}

	/**
	 * Returns the molecule definition that belongs to the given InChIKey
	 * @param inchicode
	 * @return
	 * @throws InvalidInchiException
	 */
	def inchiToMol(String inchicode) throws InvalidInchiException, CDKException {
		String message

		if (inchicode == null) {
			message = "Invalid InChI code. InChI code can't be null."
			log.error(message)
			throw new InvalidInchiException(message)
		}
		if (inchicode.trim().isEmpty()) {
			message = "Invalid InChI code. InChI code can't be empty"
			log.error(message)
			throw new InvalidInchiException(message)
		}
		if (!inchicode.matches(PatternHelper.STD_INCHI_PATTERN)) {
			message = "Invalid InChI code. I recieved a malformed InChI code: '$inchicode'."
			log.error(message)
			throw new InvalidInchiException(message)
		}

		InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance()
		InChIToStructure structureGen = factory.getInChIToStructure(inchicode, DefaultChemObjectBuilder.instance)

		IAtomContainer mol = structureGen.atomContainer

		AtomContainerManipulator.convertImplicitToExplicitHydrogens(mol);
		AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);
		CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance())
		adder.addImplicitHydrogens(mol)


		def INCHI_RET ret = structureGen.getReturnStatus();
		if (ret == INCHI_RET.WARNING) {
			// Structure generated, but with warning message
			message = "InChI warning: ${structureGen.getMessage()}"
			log.warn(message);
		} else if (ret != INCHI_RET.OKAY) {
			// Structure generation failed
			message = "Structure generation failed failed: ${ret.toString()}\n[ ${structureGen.getMessage()}\t${structureGen.warningFlags} ]"
			log.error(message)
			throw new CDKException(message);
		}

		String molString
		try {
			molString = getMolDefinition(mol)
		} catch (CDKException cdke) {
			message = cdke.getMessage()
			throw new CDKException(cdke.message, cdke)
		} catch (Exception e) {
			message = e.getMessage()
		}

		return [molecule: molString, message: message]
	}

	/**
	 * this helper method returns a 2D molecule definition for a given IAtomContainer object
	 * @param molecule
	 * @return
	 */
	private String getMolDefinition(IAtomContainer molecule) throws CDKException {
		String molString
		StringWriter stringWriter = new StringWriter()
		MDLV2000Writer writer = new MDLV2000Writer(stringWriter)

		AtomContainerSet molset = ConnectivityChecker.partitionIntoMolecules(molecule) as AtomContainerSet

		if (molset.atomContainerCount == 1) {


			//2d structure
			StructureDiagramGenerator gen = new StructureDiagramGenerator(molecule)
			gen.generateCoordinates()

			writer.writeMolecule(gen.molecule)
			writer.close()

			molString = stringWriter.buffer.toString()
			stringWriter.close()

		} else {
			throw new CDKException("Disconected molecule, can't generate mol definition")
		}

		return molString
	}

	/**
	 * Returns the InChIKey that belongs to the given InChI Code
	 * @param inchicode
	 * @return
	 * @throws InvalidInchiException
	 */
	def inchiToInchiKey(String inchicode) throws InvalidInchiException {
		String message

		if (inchicode == null) {
			message = "Invalid InChI code. InChI code can't be null."
			log.error(message)
			throw new InvalidInchiException(message)
		}
		if (inchicode?.trim()?.isEmpty()) {
			message = "Invalid InChI code. InChI code can't be empty"
			log.error(message)
			throw new InvalidInchiException(message)
		}

		InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance()
		InChIToStructure structureGen = factory.getInChIToStructure(inchicode, DefaultChemObjectBuilder.getInstance())
		String inchikey = factory.getInChIGenerator(structureGen.atomContainer).inchiKey

		return [inchicode: inchicode, inchikey: inchikey]
	}

	/**
	 * Returns the molecule definition that belongs to the given InChIKey
	 * @param inchicode
	 * @return
	 * @throws InvalidInchiException
	 */
	def inchiKeyToMol(String inchikey) throws InvalidInchiException {
		String message

		if (inchikey == null) {
			message = "Invalid InChIKey. InChIKey can't be null."
			log.error(message)
			throw new InvalidInchiException(message)
		}
		if (inchikey.trim().isEmpty()) {
			message = "Invalid InChIKey. InChIKey can't be empty"
			log.error(message)
			throw new InvalidInchiException(message)
		}
		if (!inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			message = "Invalid InChIKey. I recieved a malformed InChIKey '$inchikey'."
			log.error(message)
			throw new InvalidInchiException(message)
		}

		//get inchicode from inchikey using cts Compound
		def inchicode = Compound.findById(inchikey)?.inchiCode

		if (inchicode == null) {
			return [molecule: "", message: "Couldn't find a compound for that InChIKey."]
		}

		def result

		try {
			if (log.debugEnabled) {
				log.debug("getting molecule from inchicode: $inchicode")
			}
			result = inchiToMol(inchicode)
		} catch (InvalidInchiException e) {
			message = "There was an error calculating the molecule definition for InChIKey '$inchikey'.\n${e.message}"
			log.error(message)
			throw new InvalidInchiException(message)
		} catch (Exception e) {
			log.error("Found an error converting inchi code to molecule from inchikey '$inchikey'", e)
			throw new InvalidInchiException(message)
		}

		if (result == null) {
			result = [molecule: "", message: "Coudn't find a compound for that InChIKey."]
		}

		return result
	}
}
