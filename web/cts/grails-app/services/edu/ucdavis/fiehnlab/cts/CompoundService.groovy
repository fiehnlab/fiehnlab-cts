package edu.ucdavis.fiehnlab.cts

import com.google.gson.Gson
import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll
import edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify
import edu.ucdavis.fiehnlab.cts.pubchem.PropertiesResponse
import edu.ucdavis.fiehnlab.cts.pubchem.PubChemHelper
import edu.ucdavis.fiehnlab.cts.pubchem.SynonymsResponse
import edu.ucdavis.fiehnlab.cts.utils.FormulaExpander
import groovyx.gpars.GParsPool
import org.apache.log4j.Logger
import org.springframework.transaction.annotation.Transactional

/**
 * Created by diego on 7/29/15.
 */
class CompoundService {
	private static Logger logger = Logger.getLogger(this.class)
	FormulaExpander formulaExpander

	String getExpandedFormula(String formula) {
		formulaExpander = new FormulaExpander()
		return formulaExpander.expand(formula)
	}

	def addCompounds(List<? extends Hit> hits) {
		def added = []
		GParsPool.withPool(5) {
			hits.findAll{!(it instanceof NothingIdentifiedAtAll) }.eachParallel {Hit hit ->
				added.add(addCompoundFromHit(hit))
			}
		}

		added
	}

	def addCompoundFromHit(Hit hit) {
		logger.debug("adding compound: ${hit.inchiKey}")

		if (Compound.findByInchiKey(hit.inchiKey) != null) {
			log.debug("${hit.inchiKey} already exists..."); return
		}

		//check identifier and query full object
		def jsonComp
		def jsonSyns
		if (hit.usedAlgorithm instanceof PCNameIdentify) {
			jsonComp = PubChemHelper.getPropertiesByInChIKey(hit.inchiKey)
			jsonSyns = PubChemHelper.getSynonymsByInchiKey(hit.inchiKey)

			//build compound and update db
			Gson gson = new Gson()
			PropertiesResponse cmp = gson.fromJson(jsonComp, PropertiesResponse)
			SynonymsResponse syn = gson.fromJson(jsonSyns, SynonymsResponse)

			List<Synonym> synonyms = []
			synonyms.add(new Synonym(name: cmp.propTable.prop[0].iupacName, type: "IUPAC Name (Preferred)", score: 0))
			syn.infoList.info[0].synonym.each {s -> synonyms.add(new Synonym(name: s, type: "Synonym", score: 0, sourceFile: "PubChem search"))}

			logger.debug("\tcreating compound object...")

			Compound.withNewSession { session ->
				def compound = new Compound(inchiKey: hit.inchiKey,
						inchiCode: cmp.propTable.prop[0].inchi,
						formula: cmp.propTable.prop[0].formula,
						molWeight: cmp.propTable.prop[0].molWeight,
						exactMass: cmp.propTable.prop[0].exactMass,
						sourceFile: "PubChem search")
				synonyms.each {compound.addToSynonyms(it)}
				compound.addToExtIds(new ExternalId(name: "PubChem CID", value: cmp.propTable.prop[0].cid, url: ""))

				logger.debug("about to save compound ${compound.inchiKey}")
				if (!compound.validate(deepValidate: true)) {
					log.error("------ ERRORS SAVING COMPOUND -------")
					compound.errors.each {log.error(it)}
				} else {
					logger.debug("before save compound ${compound.inchiKey}")
					compound.save(flush: true, failOnError: true)
					logger.debug("compound ${compound.inchiKey} saved")
					return compound
				}
				logger.debug("done with ${compound.inchiKey}")
			}
		}
	}

}
