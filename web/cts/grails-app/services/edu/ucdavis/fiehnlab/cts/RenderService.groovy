package edu.ucdavis.fiehnlab.cts
import org.openscience.cdk.AtomContainer
import org.openscience.cdk.DefaultChemObjectBuilder
import org.openscience.cdk.aromaticity.Aromaticity
import org.openscience.cdk.graph.ConnectivityChecker
import org.openscience.cdk.inchi.InChIToStructure
import org.openscience.cdk.interfaces.IAtomContainer
import org.openscience.cdk.io.SDFWriter
import org.openscience.cdk.layout.StructureDiagramGenerator
import org.openscience.cdk.renderer.AtomContainerRenderer
import org.openscience.cdk.renderer.font.AWTFontManager
import org.openscience.cdk.renderer.generators.*
import org.openscience.cdk.renderer.visitor.AWTDrawVisitor
import org.openscience.cdk.tools.CDKHydrogenAdder
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator

import java.awt.*
import java.awt.image.BufferedImage
import java.util.List
/**
 * this service is used to render a structure into a mol file or a graph
 */
class RenderService {

	boolean transactional = true

	/**
	 * renders a mol file from a compound
	 */
	String renderToMol(Compound compound) {
		assert compound != null, "sorry you need to provide a compound!"

		InChIToStructure convert = new InChIToStructure(compound.inchiCode, DefaultChemObjectBuilder.getInstance())
		AtomContainer container = (AtomContainer) convert.getAtomContainer()

		log.info container.class.getName()
		container.setProperty "InChI", compound.inchi
		container.setProperty "InChI Key", compound.inchiHashKey.completeKey
		container.setProperty "Exact Molare Mass", compound.exactMolareMass
		container.setProperty "Sum Formula", compound.formula



		StringWriter swriter = new StringWriter()
		SDFWriter writer = new SDFWriter(swriter)
		writer.write(container)
		writer.close()
		swriter.flush()
		swriter.close()

		String result = "${compound.id}\n${swriter.getBuffer().toString().trim()}".trim()

		return result

	}

	/**
	 * generates an atom container from the inchi code
	 */
	def renderToAtomContainer(String inchi) {

		assert inchi != null, "sorry you need to provide an inchi!"
		try {
			InChIToStructure convert = new InChIToStructure(inchi, DefaultChemObjectBuilder.getInstance())
			IAtomContainer container = convert.getAtomContainer()
			return container
		}
		catch (Exception e) {
			log.warn "${e.getMessage()} - ${inchi}"
			return new AtomContainer()
		}
	}

	/**
	 * renders an inchi code to mol
	 */
	String renderToMol(String inchi) {

		IAtomContainer container = renderToAtomContainer(inchi)
		StringWriter swriter = new StringWriter()
		SDFWriter writer = new SDFWriter(swriter)
		writer.write(container)
		writer.close()
		swriter.flush()
		swriter.close()

		String result = "1\n ${swriter.getBuffer().toString()}"
		log.debug "generated sdf: ${result}"
		return result

	}
	/**
	 * renders an image from a mol file
	 */
	def renderToImage(Compound compound, int width = 120, int height = 120, Color color = Color.WHITE) {
		return renderToImage(compound.inchi, width, height, color)
	}

	/**
	 * renders an image from a mol file
	 */
	Image renderToImage(String inchi, int width = 120, int height = 120, Color color = Color.WHITE) {


		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)


		try {

			StructureDiagramGenerator sdg = new StructureDiagramGenerator()
			IAtomContainer mol = renderToAtomContainer(inchi)

			AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);

			Aromaticity.cdkLegacy().apply(mol);
			CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance()).addImplicitHydrogens mol

			// generators make the image elements
			List<IGenerator> generators = new ArrayList<IGenerator>()
			generators.add(new RingGenerator())

			generators.add(new BasicAtomGenerator())
			generators.add(new BasicSceneGenerator());
			generators.add(new BasicBondGenerator());

			// the renderer needs to have a toolkit-specific font manager
			AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager())

			// the call to 'setup' only needs to be done on the first paint
			Rectangle drawArea = new Rectangle(width, height)

			if (ConnectivityChecker.isConnected(mol)) {
				sdg.setMolecule(new AtomContainer(mol))
				sdg.generateCoordinates()
				mol = sdg.getMolecule()



				renderer.setup(mol, drawArea);

				// paint the background
				Graphics2D g2 = (Graphics2D) image.getGraphics();
				g2.setColor(color);
				g2.fillRect(0, 0, width, height);

				// the paint method also needs a toolkit-specific renderer
				renderer.paint(mol, new AWTDrawVisitor(g2), drawArea, true);
			} else {

				noStructure(image)
			}
		} catch (Exception e) {
			log.warn "${e.getMessage()} - ${inchi}", e

			noStructure(image)

		}

		return image
	}

	private def noStructure(BufferedImage image) {
		println("not possible to generate a structure...")
		Graphics2D graphics = (Graphics2D) image.getGraphics()
		graphics.setBackground Color.white
		graphics.setColor Color.black

		graphics.drawString "no structure available", 0, 0
	}

}