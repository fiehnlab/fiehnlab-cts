package edu.ucdavis.fiehnlab.cts

import com.google.gson.GsonBuilder
import edu.ucdavis.fiehnlab.chemify.Chemify
import edu.ucdavis.fiehnlab.chemify.Configuration
import edu.ucdavis.fiehnlab.chemify.ConfigurationSerializer
import edu.ucdavis.fiehnlab.chemify.Hit
import org.apache.log4j.Logger

class ChemifyService {
	Logger logger = Logger.getLogger(this.class)
	Chemify chemifyBean
    CompoundService compoundService = new CompoundService()

	String getConfiguration() {
		return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().registerTypeHierarchyAdapter(Configuration.class, new ConfigurationSerializer()).create().toJson(chemifyBean.getDefaultConfiguration())
	}

	List<? extends Hit> identify(String value) {

		List<? extends Hit> res = chemifyBean.identify(value)
		if (log.debugEnabled) {
			log.debug("Chemify identified ${res.size()} items for input '$value'")
		}

		log.info("\n\tFound (${res.size()}): ${res.get(0).inchiKey} ${res.get(0).searchTerm} ${res.get(0).usedAlgorithm}")

        compoundService.addCompounds(res)

		return res
	}
}
