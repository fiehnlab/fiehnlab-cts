package edu.ucdavis.fiehnlab.cts

import groovyx.net.http.HTTPBuilder
import org.openscience.cdk.DefaultChemObjectBuilder
import org.openscience.cdk.exception.CDKException
import org.openscience.cdk.inchi.InChIGeneratorFactory
import org.openscience.cdk.inchi.InChIToStructure
import org.openscience.cdk.interfaces.IAtomContainer
import org.openscience.cdk.tools.CDKHydrogenAdder
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.GET
/**
 * This Service does the actual job of registering a new compound on the CTS db,
 * looking for missing information on PubChem.
 *
 */
class UpdateService {
	private static String BASE_PC_ITEM_URL = "http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?cid="
	private static String BASE_PC_PUG_URL = "http://pubchem.ncbi.nlm.nih.gov/rest/pug/"
	private static String PUBCHEM_COMPOUND_NAME = "PubChem CID"

	def registerCompound(String inchikey, String name) {
//		log.debug("About to register: $inchikey, $name")
		Compound c = Compound.findByInchiKey(inchikey)
//		log.debug(c == null ? "compound missing": "compound exists\n ${c.inchiKey}\t${c.inchiCode}\t${c.synonyms.toArray().first()}")

		//inchikey not found in cts
		if(c == null) {
			//calculate props and add to db
			def props = inchikeyToProperties(inchikey)
//			log.debug("Found Properties: $props")
			if (props instanceof String && props.contains("404")) {
				return [result: [message: "nothing found for inchikey: $inchikey", compound: []]]
			}

			def cmp = new Compound(inchiKey:inchikey,
									inchiCode:props.inchiCode[0],
									formula:props.formula[0],
									molWeight:props.molWeight[0],
					exactMass: props.exactMass[0],
					sourceFile: "PubChem search")

			cmp.addToSynonyms(inchikey:inchikey, name:name, type: "IUPAC Name (Preferred)", score:0)
			cmp.addToExtIds(inchiKey: inchikey, name: PUBCHEM_COMPOUND_NAME, value:props.extId[0], url:BASE_PC_ITEM_URL + props.extId[0])
			props.name.each { synon ->
				if(!cmp.synonyms.contains(synon)) {
					cmp.addToSynonyms(name:synon, inchikey:inchikey, type:"Synonym", score:0)
				}
			}

			String message
			if(cmp.validate()) {
				cmp.save(flush:true)
				message = "registered compound with inchikey: $inchikey"
//				log.info(message)
			} else {
				message = "There were errors trying to save the compound.\nErrors: ${cmp.errors}"
				log.error(message)
			}

			return [result: [message: message, compound: cmp]]
		}

		return [result: [message: "inchikey '$inchikey' already exists", compound: Compound.findByInchiKey(inchikey)]]
	}

	def inchikeyToProperties(String inchikey) {

		def inchiCode
        List<Double> molWeight
        List<Double> exactMass
		def formula
		def name
		def extId

		def error

		//get molecule definition somehow...
		def http = new HTTPBuilder(BASE_PC_PUG_URL)
		http.request(GET, JSON) {
			uri.path = "compound/inchikey/$inchikey/property/Inchi,IUPACName,MolecularFormula,MolecularWeight,ExactMass/json"

			response.success = { resp, json ->


				def props = json.PropertyTable.Properties

				inchiCode = props.InChI
				name = props.IUPACName
				formula = props.MolecularFormula
				molWeight = props.MolecularWeight
				exactMass = props.ExactMass
				extId = props.CID
			}

			response.failure = { resp, json ->
				error = "${json.Fault.Message}; (${resp.statusLine.statusCode})"
				log.error("inchiekeyToProperties ($inchikey): " + error)
			}
		}

		http.shutdown()

		return (error == null) ? [inchiCode: inchiCode, molWeight: molWeight, exactMass: exactMass, formula: formula, name: name, extId: extId] : [error: error]
	}

	def registerInchiCode(String inchicode, String name) {
		String inchiKey
		Double molWeight
		Double exactMass
		def formula
		def extId

		if(inchicode == null || inchicode.isEmpty()) {
			return [error: "Inchi Code cannot be null or empty."]
		}

		InChIToStructure parser = new InChIToStructure(inchicode, DefaultChemObjectBuilder.instance)
		IAtomContainer container = parser.getAtomContainer()

//		IMolecule molecule = new Molecule(container)

		def inchigen
		String formulaStr
		try {
			//adding explicit hydrogens
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.instance)
			AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(container)
			adder.addImplicitHydrogens(container)

			// get basic compound info.
			inchigen = InChIGeneratorFactory.getInstance().getInChIGenerator(container)

			inchiKey = inchigen.getInchiKey()  // generate the inchiKey based on mol structure

			formula = MolecularFormulaManipulator.getMolecularFormula(container)
			formulaStr = MolecularFormulaManipulator.getString(formula)
			molWeight = MolecularFormulaManipulator.getNaturalExactMass(formula)
			exactMass = MolecularFormulaManipulator.getTotalExactMass(formula)

		} catch (CDKException ex) {
			// error generating inchiKey
			log.error("Error generating InChI Key: ${ex.getMessage()}\n${inchigen.log}", ex);
		} catch (Exception ex) {
			log.error("Something bad happened.\n${ex.getMessage()}", ex);
		}

		// possibly look for names

		// possibly look for external ids

		// create and save compound in database
		def cmp = new Compound(inchiKey:inchiKey,
				inchiCode:inchicode,
				formula:formulaStr,
				molWeight:molWeight,
				exactMass:exactMass)

		if(Compound.findByInchiKey(inchiKey)) {
//			log.info("Compound already exists... skipping.")
			return [result: [message: "inchikey '$inchiKey' already exists", compound: cmp]]
		}

		if(cmp.validate()) {
			cmp.save(flush: true)
		} else {
			def msg = "There were errors trying to save the compound.\n Errors: $cmp.errors"
			log.error(msg)
			return [error:msg]
		}

		return [result: [message: "registered compound with InChIKey: $inchiKey", compound: cmp]]
	}
}
