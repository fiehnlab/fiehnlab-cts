package edu.ucdavis.fiehnlab.cts

import edu.ucdavis.fiehnlab.chemify.exception.InvalidSearchTermException

import javax.validation.constraints.NotNull

/**
 * created by: diego  2015-03-12
 *
 * removes bad tokens from the name
 */
class NameCleanupService {
	long start = 0
	def badTokens = ["&amp": "", "&gt": "", "&lt": "", ";": "", "&": "", "%": "", "<": "", ">": "", "=": "", "~": "", "\\\\": "", "/": "", "\\|": "", "\\^": "", "@": "", "\\?": "", "!": "", "`": "", "\\n": "", "\\t": "", "'": "\\'"]
	def greek = ["&alpha": "alpha", "&beta": "beta", "&gamma": "gamma", "&delta": "delta"]

	String cleanName(@NotNull String name) throws InvalidSearchTermException {

		String cleanName = name
		if (name == null || name.trim() == "") {
			throw new InvalidSearchTermException("The name to clean can't be null or empty")
		}

		if (log.debugEnabled) {
			start = System.nanoTime()
		}

		//removing HTML tags
		cleanName = cleanName.replaceAll("(?i)(?:<|&lt;)([A-Z][A-Z0-9]*)\\b[^>]*?(?:>|&gt;)(.*?)(?:<|&lt;)/\\1(?:>|&gt;)", "\$2");

		//removing greek chars
		greek.each { g -> cleanName = cleanName.replaceAll(g.key, g.value) }

		//removing other bad chars
		badTokens.each { t -> cleanName = cleanName.replaceAll(t.key, t.value) }

		if (cleanName == null || cleanName.trim() == "") {
			throw new InvalidSearchTermException("Cleaned name is empty. Original name is invalid")
		}

		if (log.debugEnabled) {
			log.info "Cleaned $name to $cleanName in ${(System.nanoTime() - start) / 1000000} ms"
		}

		return cleanName
	}
}
