package edu.ucdavis.fiehnlab.cts
import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Scored
import org.apache.log4j.Logger

import java.text.DecimalFormat
/**
 * This service implements the different scoring algorithms
 */
class ScoringService {
	SimilarityService similarityService

	public static final List<String> biodbs = ['KEGG', 'Human Metabolome Database', 'BioCyc']
	public static final KEGG_WEIGHT = 10

	Logger logger = Logger.getLogger(this.getClass())

	/**
	 * this method calls the apropriate scoring algorithm based on user selection
	 * @param algorithm can be 'biological
	 * @param input
	 */
	List<ScoredValue> score(String algorithm, List<String> inchis) {

		switch (algorithm) {
			case ConstantHelper.SCORING_COUNT:
				return this.scoreByDatabaseCount(inchis)
				break
			case ConstantHelper.SCORING_KEGG_PRESENCE:
				return this.scoreByKeggPresence(inchis)
				break
			case ConstantHelper.SCORING_BIO:
			default:
				return this.scoreByBiologicalCount(inchis)
		}
	}

	List<ScoredValue> scoreByBiologicalCount(List<String> inchis) {
		Integer max = 0
		def result = [:]

		/* calculate counts in biological databases for each hit in input
		 * each input comes from a unique searchTerm, and can contain several hits (0 to n)
		 * so we score ALL hits for the searchTerm
		 */
		for (String hit : inchis) {

			//get bioCount for inchikey
			def test = similarityService.bioIdCount(hit, biodbs)
			if (test.error == null) {
				result.put(hit, test["total"])
			} else {
//				hit = ""
				result.put(hit, 0.0)
//				result.put("error", "'$hit' is not a valid inchikey")
			}

			Integer count = 0

			/* check for total count and if it has KEGG reference
			 * and update count and max accordingly
			 */
			for (Map.Entry a : test) {
				if (a.key.equals("total") && a.value > 0) {
					count += a.value
				}

				// increase value if it has KEGG
				if (a.key.equals("KEGG")) {
					count += KEGG_WEIGHT
				}

				// find the max
				if (count >= max) {
					max = count
				}
				result[hit] = count
			}
		}

		//calculate the relative score (scale score based on max count from all hits)
		for (String hit : result.keySet()) {
			if(hit.equals("error")) {
				continue
			}
			Double score = result[hit] / max.doubleValue()

			//in case of Nan or 0/0 we always set it to 0
			if (Double.isNaN(score)) {
				score = 0.0;
			}
			DecimalFormat f = new DecimalFormat("#0.##")
			result[hit] = f.format(score).toDouble()
		}

		//sort it by highest hit (using map-sorting by value is tricky)
		def finalResult = sortByValues(result)

		return finalResult
	}

	/**
	 * Calculates the score of a list of inchi keys based on the external reference count
	 * @param input list of inchi keys (Strings)
	 * @return List <ScoredValue>  of inchi keys and their respective score
	 */
	List<ScoredValue> scoreByDatabaseCount(List<String> inchis) {
		Integer max = 0
		def result = [:]

		/* calculate counts in biological databases for each inchikey in input
		 * each input comes from a unique searchTerm, and can contain several hits (0 to n)
		 * so we score ALL hits for the searchTerm
		 */
		for (String hit : inchis) {
			if (hit.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
				//just add it in case its an empty hit
				result.put(hit, 0)

				//get bioCount for inchikey
				def test = similarityService.externalIdCount(hit)

				result[hit] = test["datasource_count"]

				Integer count = 0

				/* check for total count and if it has KEGG reference
				 * and update count and max accordingly
				 */
				for (Map.Entry a : test) {
					if (a.key.equals("datasource_count") && a.value > 0) {
						count += a.value
					}

					// find the max
					if (count >= max) {
						max = count
					}
					result[hit] = count
				}
			} else {
//				hit = "'$hit' is not a valid inchikey"
				result.put(hit, 0.0D)
//				result.put("error":"$hit is not a valid inchikey")
			}
		}

		//calculate the relative score (scale score based on max count from all hits)
		for (String hit : result.keySet()) {
			Double score = result[hit] / max.doubleValue()

			//in case of Nan or 0/0 we always set it to 0
			if (Double.isNaN(score)) {
				score = 0.0;
			}
			DecimalFormat f = new DecimalFormat("#0.##")
			result[hit] = f.format(score).toDouble()
		}

		//sort it by highest hit (using map-sorting by value is tricky)
		def finalResult = sortByValues(result)

		return finalResult
	}

	/*
     * Java method to sort Map in Java by value e.g. HashMap or Hashtable
     * throw NullPointerException if Map contains null values
     * It also sort values even if they are duplicates
     */

	private List<ScoredValue> sortByValues(Map map) {
		List<Map.Entry> entries = new LinkedList<Map.Entry>(map.entrySet())

		Collections.sort(entries, new Comparator<Map.Entry>() {
			@Override
			int compare(Map.Entry o1, Map.Entry o2) {
				return o2.getValue().compareTo(o1.getValue())
			}
		});

		//LinkedHashMap will keep the keys in the order they are inserted
		//which is currently sorted on natural ordering
		List sortedMap = new ArrayList();

		for (Map.Entry entry : entries) {
			sortedMap.add(new ScoredValue(entry.getKey(), entry.getValue()))
		}

		return sortedMap
	}


	/**
	 * this method scores the inchis based on whether they have a kegg id or not in the cts db
	 *
	 * @param input
	 * @param searchTerm
	 * @return
	 */
	public List<Scored> scoreByKeggPresence(List<? extends Hit> input) {

		List<Scored> results = new ArrayList<Scored>();

		for (String inchi : input) {
			if (ExternalId.findByInchiKeyAndName(Compound.findByInchiKey(inchi), "KEGG")) {
				results.add(new ScoredValue(inchi, 1.0D));
			} else {
				results.add(new ScoredValue(inchi, 0.0D));
			}
		}

		return results;
	}

}
