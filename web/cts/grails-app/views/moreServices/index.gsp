<%--
  Created by IntelliJ IDEA.
  User: dpedrosa
  Date: 3/18/13
  Time: 11:52 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta name="layout" content="main"/>
	<title>Chemical Translation Service</title>
	<script type="application/javascript">
		//<![CDATA][
		$(function () {
			// Stick the #nav to the top of the window
			var nav = $('#floatingMenu');
			var navHomeY = nav.offset().top;
			var isFixed = false;
			var $w = $(window);
			$w.scroll(function () {
				var scrollTop = $w.scrollTop();
				var shouldBeFixed = scrollTop > navHomeY;
				if (shouldBeFixed && !isFixed) {
					nav.css({
						position: 'fixed',
						top: '1em',
						left: nav.offset().left,
						width: nav.width()
					});
					isFixed = true;
				}
				else if (!shouldBeFixed && isFixed) {
					nav.css({
						position: 'static',
						top: navHomeY
					});
					isFixed = false;
				}
			});
		});
		//]]>
	</script>
</head>

<body>
<div>
	<script type="text/javascript">
		jQuery(function () {
			jQuery.ajax({
				dataType: "text",
				url: "/cts/info/ajaxIsDevelopmentEnvironment"
			}).success(function (data) {
				if (data == 'true') {
					jQuery("#inDevelopment").html('development database');
				}
			});
		});
	</script>
</div>


<div id="main">
	<div class="page_header">
		<h2>More Services</h2>

		<p class="siteinfo">here you can find an overview of additional services and resources the CTS offers you.</p>

		<div class="hr"></div>
	</div>

	<div class="box">
		<div class="clear left" style="width: 70%; margin: 0 auto;">
			<ul>
				<li>
					<h3>Web Services</h3>

					<p>you can directly access the system by the use of our simple REST-based webservices. Which can be invoked from most programming languages or workflow tools. They will all return the result in form of a JSON string.</p>

					<div class="navigation">
						<h3 id="convert">convert from one id to another</h3>

						<div>
							<p>this service allows you convert from one given id to another id. Please ensure that you encode the url properly -- <a
									href="http://www.w3schools.com/tags/ref_urlencode.asp">URL Encoding reference</a>
							</p>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/convert/InChIKey/Chemical%20Name/QNAYBMKLOCPYGJ-REOHCLBHSA-N</code>
							</div>

							<p>expected result</p>

							<div class="codeblock">
								<code><pre>[
  {
    "fromIdentifier": "InChIKey",
    "searchTerm": "QNAYBMKLOCPYGJ-REOHCLBHSA-N",
    "toIdentifier": "Chemical Name",
    "result":
    [
      "L-Alanine"
    ]
  }
]</pre>
								</code>
								<br/>
							</div>

							<p>also adding a parameter as <em>query string</em>: '?scoring=biological' like this:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/convert/Chemical%20Name/InChIKey/alanine<strong>?scoring=biological</strong>
								</code>
							</div>

							<p>will return a scored list of values. This scoring only works when converting to InChIKeys</p>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/convert/Chemical%20Name/InChIKey/alanine<strong>?scoring=biological</strong>
								</code>
							</div>

							<p>expected result</p>

							<div class="codeblock">
								<code><pre>[
  {
    "fromIdentifier": "chemical name",
    "searchTerm": "alanine",
    "toIdentifier": "inchikey",
    "result":
    [
      {
        "class": "edu.ucdavis.fiehnlab.cts.ScoredValue",
        "score": 1,
        "value": "QNAYBMKLOCPYGJ-REOHCLBHSA-N"
      },
      {
        "class": "edu.ucdavis.fiehnlab.cts.ScoredValue",
        "score": 0.75,
        "value": "QNAYBMKLOCPYGJ-UWTATZPHSA-N"
      },
      {
        "class": "edu.ucdavis.fiehnlab.cts.ScoredValue",
        "score": 0.48,
        "value": "QNAYBMKLOCPYGJ-UHFFFAOYSA-N"
      }
    ]
  }
]</pre>
								</code>
							</div>

							<p>the default scoring is based on 'biological importance'; a weighted count of references in the following databases: KEGG, Human Metabolome Database and BioCyc.</p>
						</div>

						<div class="hr"></div>

						<h3 id="score">scoring service</h3>

						<div>
							<p>this service allows the scoring of a search term (only when converted to InChIKey).<br/> It returns the InChIKeys associated to the search term scored by the selected algorithm.<br/>
								Please note that the only scoring that makes sense is when scoring a chemical name, since it may have many InChIKeys associated to it, in contrast to all other identifiers that will only
								map to a single InChIKey.</p>

							<p>to use this service call:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/score/&lt;from&gt;/&lt;value&gt;/&lt;algorithm&gt;</code>
							</div>

							<p>where <em>from</em> is a valid id coming from #get a list of available 'from' ids#<br/>
								<em>value</em> is the chemical name or chemical id to score<br/>
								and <em>algorithm</em> can be one of: 'biological', 'popularity' (without the quotes)
							</p>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/score/Chemical%20Name/threonine/biological</code>
							</div>

							<p>expected result</p>

							<div class="codeblock">
								<code><pre>{
  "searchTerm": "threonine",
  "from": "chemical name",
  "result":
  [
    {
      "InChIKey": "AYFVYJQAPQTCCC-GBXIJSLDSA-N",
      "score": 1
    },
    {
      "InChIKey": "AYFVYJQAPQTCCC-STHAYSLISA-N",
      "score": 0.86
    },
    {
      "InChIKey": "AYFVYJQAPQTCCC-UHFFFAOYSA-N",
      "score": 0
    }
  ]
}</pre>
								</code>
							</div>

							<p>the score is a 'double' in the range 0.0 to 1.0<br/>
								the 'biological' algorithm is a weighted count of the references the search term has to the following ids (in CTS database): BioCyc, KEGG and Human Metabolome Database<br/>
								the 'popularity' algorithm is a weighted count of the references the search term has to all the ids (in CTS database)
							</p>
						</div>

						<div class="hr"></div>

						<h3 id="moltoinchi">get an InChI Code and an InChIKey from a molecule definition (SDF)</h3>

						<div>
							<p>this service lets you an InChI Code and InChIKey calculated from a molecule definition (in MOL or SDF format). This service uses the CDK library internally to do the conversion.<br/>
								The service needs to be provided with a 'mol' parameter as part of a POST request's body with the actual molecule definition.
							</p>

							<p>molecule definition sample: (be aware of the comment lines, these can have any content or be empty, except the first one)</p>

							<div class="codeblock">
								<code><pre>977                             --comment line--
  -OEChem-07071413282D          --comment line--
                                --comment line--
  2  1  0     0  0  0  0  0  0999 V2000
    2.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0  0  0  0
M  END</pre>
								</code>
							</div>

							<p>to call the service use the following url:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/moltoinchi</code>
							</div>

							<p>example curl command with settings:<br/>
								Header: Content-type: application/json (required)<br/>
								Charset: UTF-8 (optional)<br/>
								Request Body:</p>

							<div class="codeblock">
								<code><pre>{"mol":"977\n  -OEChem-07071413282D\n\n  2 1 0   0 0 0 0 0 0999 V2000\n    2.0000    0.0000    0.0000  O 0  0  0  0  0  0  0  0  0  0  0  0\n    3.0000    0.0000    0.0000  O    0  0  0  0  0  0  0  0  0  0  0  0\n  1  2  2  0  0  0  0\nM END"}</pre>
								</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code><pre>curl -X POST -H "Content-Type: application/json" -d '{"mol":"977\n  -OEChem-07071413282D\n\n  2 1 0   0 0 0 0 0 0999 V2000\n    2.0000    0.0000    0.0000  O 0  0  0  0  0  0  0  0  0  0  0  0\n    3.0000    0.0000    0.0000  O 0  0  0  0  0  0  0  0  0  0  0  0\n  1  2  2  0  0  0  0\nM END"}' http://cts.fiehnlab.ucdavis.edu/service/moltoinchi</pre>
								</code>
							</div>

							<p>the response is the one shown below:</p>

							<div class="codeblock">
								<code><pre>{
  "inchikey": "MYMOFIZGZYHOMD-UHFFFAOYSA-N",
  "inchicode": "InChI=1S/O2/c1-2"
}</pre>
								</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="inchitomol">get a molecule definition (SDF) from an InChI Code</h3>

						<div>
							<p>this service lets you get the molecule definition having an InChI Code. It need to be provided with an 'inchicode' parameter as part of a POST request's body with the encoded InChI code string</p>

							<p>please call the service using the following url:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/inchitomol</code>
							</div>

							<p>and these settings:<br/>
								Header: Content-type: application/json (required)<br/>
								Charset: UTF-8 (optional)<br/>
								Request Body:</p>

							<div class="codeblock">
								<code>{"inchicode":"InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3"}</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl -X POST -H "Content-type: application/json" -d '{"inchicode":"InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3"}' http://cts.fiehnlab.ucdavis.edu/service/inchitomol</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code><pre>{
 "molecule": "\n
 CDK     0223171650\n
 \n
   9  8  0  0  0  0  0  0  0  0999 V2000\n
     1.2990   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n
     2.5981   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n
     3.8971   -0.7500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n
     0.0000    0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n
     0.3349   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n
     2.2632   -1.8991    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n
     3.5623    1.1491    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n
     1.6339    1.1491    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n
     5.1962   -0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n
   2  1  1  0  0  0  0 \n
   3  2  1  0  0  0  0 \n
   1  4  1  0  0  0  0 \n
   1  5  1  0  0  0  0 \n
   1  6  1  0  0  0  0 \n
   2  7  1  0  0  0  0 \n
   2  8  1  0  0  0  0 \n
   3  9  1  0  0  0  0 \n
 M  END\n",
 "message": null
}</pre>
								</code>
							</div>

							<p>Where <strong>message</strong> will contain any possible conversion errors</p>
						</div>

						<div class="hr"></div>

						<h3 id="inchikeytomol">get a molecule definition (MDL/SDF) from an InChIKey</h3>

						<div>
							<p>having an InChIKey, you can get the molecule definition (MDL/SDF) calling this service url:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/inchikeytomol/LFQSCWFLJHTTHZ-UHFFFAOYSA-N</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>
									<pre>{
 "molecule": "\n
 CDK     0707141125\n
 \n
   3 2 0 0 0 0 0 0 0 0999 V2000\n
     0.0000    0.0000    0.0000 C  0  0  0  0  0  0  0  0  0  0  0  0\n
     1.5230    0.0000    0.0000 C  0  0  0  0  0  0  0  0  0  0  0  0\n
     1.9446    1.3371    0.0000 O  0  0  0  0  0  0  0  0  0  0  0  0\n
   2  1  1  0  0  0  0\n
   3  2  1  0  0  0  0\n
 M  END\n",
 "message": null
}</pre>
								</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="inchitoinchikey">get an InChIKey from an InChI Code</h3>

						<div>
							<p>this service returns an InChIKey when given an 'inchicode' as a parameter in POST request's body.</p>

							<p>please call this service url:</p>

							<div class="codeblock">
								<code>http://cts.fiehlab.ucdavis.edu/service/inchicodetoinchikey</code>
							</div>

							<p>with these settings:<br/>
								Header: Content-type: application/json (required)<br/>
								Charset: UTF-8 (optional)<br/>
								Request Body:</p>

							<div class="codeblock">
								<code>{"inchicode":"InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3"}</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl -X POST -H "Content-type: application/json" -d '{"inchicode":"InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3"}' http://cts.fiehnlab.ucdavis.edu/service/inchicodetoinchikey</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>
									<pre>{
  "inchicode": "InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3",
  "inchikey": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N"
}</pre>
								</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="smilestoinchi">get an InChI Code from SMILES code</h3>

						<div>
							<p>this service returns the InChI Code converted form a SMILES code given as the 'smiles' parameter in a POST request's body.</p>

							<p>please call this service url:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/smilestoinchi</code>
							</div>

							<p>with these settings:<br/>
								Header: Content-type: application/json (required)<br/>
								Charset: UTF-8 (optional)<br/>
								Request Body:</p>

							<div class="codeblock">
								<code>{"smiles":"[Cu+2].[O-]S(=O)(=O)[O-]"}</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl -X POST -H "Content-type: application/json" -d '{"smiles":"[Cu+2].[O-]S(=O)(=O)[O-]"}' http://cts.fiehnlab.ucdavis.edu/service/smilestoinchi</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>{"inchicode":"InChI=1S/Cu.H2O4S/c;1-5(2,3)4/h;(H2,1,2,3,4)/q+2;/p-2"}</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="expandformula">get an expanded Molecular Formula string</h3>

						<div>
							<p>this service returns an expanded form of the molecular formula.</p>

							<p>please call this service url:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/expandFormula/&lt;formula&gt;</code>
							</div>

							<p>for water this would look like:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/expandFormula/H2O</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/expandFormula/H2O</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>
									<pre>{
  "formula": "H2O",
  "result": "HHO"
}</pre>
								</code>
							</div>

							<p>in case of an invalid formula is provided, the resulting JSON will contain an extra 'error' element:</p>

							<div class="codeblock">
								<code>
									<pre>{
  "formula": "bad formula()",
  "error": "Invalid molecular formula 'bad formula()'",
  "result": ""
}</pre>
								</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="compdata">get all information for a single compound</h3>

						<div>
							<p>this service returns all the information we currently have on our database about a single compound.</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/compound/&lt;inchikey&gt;</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/compound/LFQSCWFLJHTTHZ-UHFFFAOYSA-N</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>
									<pre>{
  "inchikey": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N",
  "inchicode": "InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3",
  "molweight": 46.06844,
  "exactmass": 46.041865,
  "formula": "C2H6O",
  "synonyms":
  [
    {
    "type": "Synonym",
    "name": "Alcohol, anhydrous",
    "score": 0
    },
    {
    "type": "IUPAC Name (CAS-like Style)",
    "name": "ethanol",
    "score": 0
    },
    ...
  ],
  "externalIds":
  [
    {
    "name": "Pubchem SID",
    "value": "127256982",
    "url": "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=127256982"
    },
    {
    "name": "KEGG",
    "value": "C00469",
    "url": "http://www.genome.jp/dbget-bin/www_bget?cpd:C00469"
    },
    {
    "name": "PubChem CID",
    "value": "702",
    "url": "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi"
    },
    ...
  ]
}</pre>
								</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="synonyms">get synonyms for an InChIKey</h3>

						<div>
							<p>this service will give you all the synonyms contained in Pubchem for the compound whose InChIKey you pass as the parameter.</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/synonyms/&lt;inchikey&gt;</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/synonyms/LFQSCWFLJHTTHZ-UHFFFAOYSA-N</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>
									<pre>[
  "Ethyl Alcohol & Water, 80%",
  "silent spirit",
  "Alcohol, dehydrated",
  "Ethanol Fixative 80% v/v",
  "Thanol",
  "Alcohol, Rubbing",
  "Alcohol Anhydrous",
  "eth",
  "Sekundasprit",
  "Ethyl Hydrate",
  "Alcohol 5% in dextrose 5%",
  "EINECS 200-578-6",
  "Ethyl alcohol & water, 96%",
  "etanol",
  "Distilled spirits",
  "Reagent Alcohol",
  "Ethyl alcohol & water, 10%",
  "Ethyl alcohol and water",
  "ALCOHOL, ANHYDROUS REAGENT",
  "Alkohol [German]",
  "Anhydrol PM 4085",
  "15P",
  "ethanol",
  "ALCOHOL",
  ...
]</pre>
								</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="extidscores">get the score for an external id name</h3>

						<div>
							<p>the service returns the score (number of compounds linked to) the provided external id name.<br/>For the list of external id names check <a
									href="#fromnames">From Ids</a>.<br/>Take into account that "InChIKey" and "Inchi Code" are not external ids.
							</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/extidScore/&lt;name&gt;</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/extidScore/KEGG</code>
							</div>

							<p>expected result</p>

							<div class="codeblock">
								<code>{"external_id":"KEGG","count":4}</code>
							</div>

							<p>In case of an error, the result will have an extra key "error" with the error message</p>

							<div class="codeblock">
								<code>{"external_id":null,"count":"invalid","error":"parameter 'name' cannot be null or empty."}</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="fromnames">get a list of available 'from' ids</h3>

						<div>
							<p>the service returns a list of all possible id's to be using in the 'from' field of the conversion service</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/conversion/fromValues</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/conversion/fromValues</code>
							</div>

							<p>expected result</p>

							<div class="codeblock">
								<code>["ABI Chem","ChEMBL","Chemical Name","InChIKey","MICAD","MLSMR","MMDB","PubChem CID","PubChem SID", ...]</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="tonames">get a list of available 'to' ids</h3>

						<div>
							<p>the service returns a list of all possible id's to be using in the 'to' field of the conversion service.</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/conversion/toValues</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/conversion/toValues</code>
							</div>

							<p>expected result</p>

							<div class="codeblock">
								<code>["ABI Chem","ChEMBL","Chemical Name","InChI Code","InChIKey","MICAD","MLSMR","MMDB","PubChem CID","PubChem SID", ...]</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="chemquery">service to query chemify</h3>

						<div>
							<p>this service allows you to call the Chemify application. Chemify is a tool that tries to find the most appropriate InChIKey from a given name.</p>

							<p>there are two ways to call this service:</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/&lt;name&gt;</code>
								or
								<code>http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify/query?value=&lt;name&gt;</code>
							</div>

							<p>expected result: (when name='ethanol')</p>

							<div class="codeblock">
								<code>
									<pre>[
  {
    "result": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N",
    "query": "ethanol",
    "algorithm": "edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify",
    "score": 1.0,
    "scoring_algorithm": "edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone",
    "enhancements": [
      {
      "unmodified search term": "ethanol"
      },
      {
      "time(ms) for identification": "461"
      }
    ]
  }
]</pre>
								</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="chemconfig">get chemify configuration</h3>

						<div>
							<p>this service returns the current configuration for the Chemify application</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/chemify/rest/identify</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="idcount">external id count</h3>

						<div>
							<p>this service is part of the popularity scoring algorithm, returning the number of external references an InChIKey has.</p>

							<div class="codeblock">
								<code>http://cts.fiehnlab.ucdavis.edu/service/count/QNAYBMKLOCPYGJ-UHFFFAOYSA-N</code>
							</div>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/count/QNAYBMKLOCPYGJ-UHFFFAOYSA-N</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>{"datasource_count": 324}</code>
							</div>
						</div>

						<div class="hr"></div>

						<h3 id="biocount">biological id count</h3>

						<div>
							<p>in a similar way to the external id count, this service returns the number of external references an InChIKey has only on databases with <br/>
								biological content. Our defined set of bio-databases is: BioCyc, KEGG and HMDB</p>
							<code>http://cts.fiehnlab.ucdavis.edu/service/countBiological/LFQSCWFLJHTTHZ-UHFFFAOYSA-N</code>

							<p>curl command:</p>

							<div class="codeblock">
								<code>curl http://cts.fiehnlab.ucdavis.edu/service/countBiological/LFQSCWFLJHTTHZ-UHFFFAOYSA-N</code>
							</div>

							<p>expected result:</p>

							<div class="codeblock">
								<code>
									<pre>{
  "KEGG": 2,
  "BioCyc": 1,
  "Human Metabolome Database": 1,
  "total": 4
}</pre>
								</code>
							</div>
						</div>
					</div>

					<div class="hr"></div>
				</li>

				<li>
					<h3>Related Publications and Posters</h3>

					<p>All publications and posters related to this application.</p>

					<div class="navigation">
						<ol>
							<li><a class="external" id="ms2015"
							       href="${r.resource(dir: 'download', file: 'CTS2-MS2015.pdf')}"
							       target="_blank">2015 Metabolomics Society Poster (CTS 2.0)</a>
							</li>
							<li><a class="external" id="ncmtp2011"
							       href="${r.resource(dir: 'download/publications', file: 'NCMTP2011.pdf')}"
							       target="_blank">2011 National Conference On Metabolomics, Transcriptomics and Proteomics (NCMTP) Poster (CTS 1.0)</a>
							</li>
							<li><a class="external" id="asms2010"
							       href="${r.resource(dir: 'download/publications', file: 'ASMS2010.pdf')}"
							       target="_blank">2010 ASMS Poster (CTS 1.0)</a></li>
							<li><a class="external" id="ms2010"
							       href="${r.resource(dir: 'download/publications', file: 'MS2010.pdf')}"
							       target="_blank">2010 Metabolomic Society Poster (CTS 1.0)</a></li>
						</ol>
					</div>
				</li>
			</ul>
		</div>

		<div id="floatingMenu" class="clear left" style="width: 28%; margin: 0 auto;">
			<h3>Services list</h3>

			<div class="hr"></div>
			<a href="#convert">Convert one id to another</a><br/>
			<a href="#score">Scoring service</a><br/>
			<a href="#moltoinchi">Get InChI Code from molecule</a><br/>
			<a href="#inchitomol">Get molecule from InChI Code</a><br/>
			<a href="#inchikeytomol">Get molecule from InChIKey</a><br/>
			<a href="#inchitoinchikey">Get InChIKey from InChI Code</a><br/>
			<a href="#smilestoinchi">Get InChI Code from SMILES Code</a><br/>
			<a href="#expandformula">Get expanded molecular formula string</a><br/>
			<a href="#compdata">Get a compund's data</a><br/>
			<a href="#synonyms">Get a compund's synonyms</a><br/>
			<a href="#fromnames">List all 'From' identifiers</a><br/>
			<a href="#tonames">List all 'To' identifiers</a><br/>
			<a href="#extidscores">Get ExternalId Scores</a><br/>
			<a href="#chemquery">Query Chemify</a><br/>
			<a href="#chemconfig">Get Chemify configuration</a><br/>
			<a href="#idcount">Get count of external ids</a><br/>
			<a href="#biocount">Get count of biological ids</a><br/>
			<br/>

			<h3>Publications & Posters</h3>

			<div class="hr"></div>
			<a href="#ms2015">2015 Metabolomics Society Poster</a><br/>
			<a href="#ncmtp2011">2011 National Conference On Metabolomics Poster</a><br/>
			<a href="#asms2010">2010 ASMS Poster</a><br/>
			<a href="#ms2010">2015 Metabolomics Society Poster</a><br/>
		</div>
	</div>
</div>

</body>
</html>

