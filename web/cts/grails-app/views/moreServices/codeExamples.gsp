<%--
  Created by IntelliJ IDEA.
  User: dpedrosa
  Date: 3/18/13
  Time: 1:57 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta name="layout" content="main"/>
	<title>Chemical Translation Service</title>
	<script type="text/javascript" src="/js/syntaxhighlighter/shCore.js"></script>
	<script type="text/javascript" src="/css/syntaxhighlighter/shBrushGroovy.js"></script>
	<script type="text/javascript" src="/css/syntaxhighlighter/shBrushXml.js"></script>

	<!-- Include *at least* the core style and default theme -->
	<link href="/css/syntaxhighlighter/shCore.css" rel="stylesheet" type="text/css" />
	<link href="/css/syntaxhighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
</head>

<body class="BodyBackground">
<div id="main">
	<div class="page_header">
		<h2>Web Services</h2>

		<div class="hr"></div>
	</div>

	<div class="box">
		<h3>Using WebServices</h3>

		<div class="box">
			<h4 class="details">Using HTTPBuilder</h4>

			<p>HTTPBuilder provides a convenient builder API for complex HTTP requests.  It is built on top of Apache HttpClient. The <a
					href="http://groovy.codehaus.org/modules/http-builder/">project home page</a> includes full documentation and downloads.
			</p>

			<p>Note that the example below works for HTTPBuilder version 0.5.0-RC2.  In prior versions, the <code>uri</code> property was called <code>url</code>.  Also note that <code>@Grab</code> dependency management requires Groovy 1.6 or later.  More information may be <a
					href="http://groovy.codehaus.org/modules/http-builder/download.html"
					class="external-link">found here</a>.</p>

			<p>Example:  HTTP GET, automatically parsed as a JSON response.</p>
			<br/>
			<pre class="brush: groovy">
			@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.5.0-RC2' )
			import groovyx.net.http.*
			import static groovyx.net.http.ContentType.*
			import static groovyx.net.http.Method.*

			def http = new HTTPBuilder( 'http://ajax.googleapis.com' )

			// perform a GET request, expecting JSON response data
			http.request( GET, JSON ) {
			  uri.path = '/ajax/services/search/web'
			  uri.query = [ v:'1.0', q: 'Calvin and Hobbes' ]

			  headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'

			  // response handler for a success response code:
			  response.success = { resp, json -&gt;
				println resp.statusLine

				// parse the JSON response object:
				json.responseData.results.each {
				  println "  &#36;{it.titleNoFormatting} : &#36;{it.visibleUrl}"
				}
			  }

			  // handler for any failure status code:
			  response.failure = { resp -&gt;
				println "Unexpected error: &#36;{resp.statusLine.statusCode} : &#36;{resp.statusLine.reasonPhrase}"
			  }
			}
			</pre>
		</div>
		<div class="hr"></div>

		<div class="box">
			<a name="conversionDocs"></a>
			<h4>Conversion Service</h4>

			<p>This web service allows the user to convert a big amount of items (compounds) between all the data types that we currently support in the database.</p>

			<p>Call the service in the following form:<br/>
				<pre class="brush: xml">        http://url:port/service/convert/&lt;from&gt;/&lt;to&gt;/&lt;value&gt;</pre>
			</p>

			<p>This can be done from any application (desktop or web) that retrieves data from an internet data stream.</p>

		</div>
		<div class="hr"></div>

		<div class="box">
			<a name="tanimotoDocs"></a>
			<h4>Tanimoto Scores</h4>

			<p>With this service the user can obtain the 'distance' or 'similitud' between two compounds passed as part of the URL</p>
			<br/>
			<p>The general format of the URL looks like:<br/>
				<pre class="brush: xml">        http://<em>&lt;host&gt;[:port]</em>/cts/service/calculate/tanimoto/<em>&lt;Compound1 InChI Key&gt;</em>/<em>&lt;Compound1 InChI Key&gt;</em></pre>
			</p>
			<br/>
			<p>Example URL:<br/>
				<pre class="brush: xml">        http://127.0.0.1:8080/cts/service/calculate/tanimoto/ZUXNZUWOTSUBMN-UHFFFAOYSA-N/ZUXNZUWOTSUBMN-UHFFFAOYSA-N</pre>
			</p>
			<div class="hr"></div>
		</div>
	</div>
	<script type="text/javascript">
		SyntaxHighlighter.all()
	</script>

</div>
</body>
</html>