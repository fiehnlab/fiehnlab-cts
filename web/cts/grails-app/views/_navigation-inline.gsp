<ul class="navigation vertical-list" id="topNav">

	<li class="home">
		<g:link uri="/">
			<g:message code="home"/>
		</g:link>
	</li>

	<li class="transform">
		<g:link controller="conversion">
			<g:message code="convert.simple"/>
		</g:link>
	</li>

	<li class="mass_transform">
		<g:link controller="conversion" action="batch">
			<g:message code="convert.batch"/>
		</g:link>
	</li>

	<li class="mass_transform">
		<g:link controller="moreServices" action="index">
			<g:message code="moreserv"/>
		</g:link>
	</li>

	<li class="discovery">
		<g:link mapping="contact"><g:message code="cts.menu.contact"/></g:link>
	</li>

	<li class="right">
		<div id="inDevelopment"></div>
	</li>
</ul>

%{--<jq:jquery>--}%

	%{--jQuery.ajax({--}%
		%{--dataType: "text",--}%
		%{--url: "${g.createLink(controller: 'info', action: 'ajaxIsDevelopmentEnvironment')}"--}%

             %{--}).success(function(data){--}%

                    %{--if(data == 'true'){--}%
                        %{--jQuery("#inDevelopment").html('development database'--}%
                    %{--);--}%

                    %{--}--}%
           %{--});--}%
%{--</jq:jquery>--}%
