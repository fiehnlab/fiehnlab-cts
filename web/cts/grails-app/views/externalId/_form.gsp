<%@ page import="edu.ucdavis.fiehnlab.cts.ExternalId" %>



<div class="fieldcontain ${hasErrors(bean: externalIdInstance, field: 'origin', 'error')} ">
	<label for="origin">
		<g:message code="externalId.origin.label" default="Origin" />
		
	</label>
	<g:textField name="origin" value="${externalIdInstance?.origin}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: externalIdInstance, field: 'value', 'error')} ">
	<label for="value">
		<g:message code="externalId.value.label" default="Value" />
		
	</label>
	<g:textField name="value" value="${externalIdInstance?.value}"/>
</div>

