<div class="box">
	<h3 class="header ui-corner-top no-collapse">Services</h3>
	<span class="content  ui-corner-bottom">
		<ul class="services">
			<li class="transform">
				<g:link controller="conversion">
					<h4><g:message code="convert.simple"/></h4>
					<h5><g:message code="convert.simple.description"/></h5>
				</g:link>
			</li>
			<li class="mass_transform">
				<g:link controller="conversion" action="batch">
					<h4><g:message code="convert.batch"/></h4>
					<h5><g:message code="convert.batch.description"/></h5>
				</g:link>
			</li>
			<li class="search">
				<g:link controller="search">
					<h4><g:message code="search" default="Search"/></h4>
					<h5><g:message code="search.description" default="Search for a specific Id"/></h5>
				</g:link>
			</li>

			<!--
			<li class="transform">
				<h4><g:message code="discover"/></h4>
				<h5><g:message code="discover.description"/></h5>
			</li>

			-->
			<li class="transform">
				<g:link controller="moreServices">
					<h4><g:message code="moreserv"/></h4>
					<h5><g:message code="moreserv.description"/></h5>
				</g:link>
			</li>
		</ul>
	</span>
</div>