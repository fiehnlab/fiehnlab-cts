<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 3/13/13
  Time: 2:52 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta name="layout" content="main"/>
	<export:resource/>
	<title>Chemical Translation Service</title>
</head>

<body>

<div id="main">
	<div class="page_header">
		<h2>Compound Search</h2>

		<p class="siteinfo">
			Welcome to the simple search dialog, which will allow you to query against the database
		</p>

		<div class="hr"></div>
	</div>

	<div class="box">
		<div class="space top small">
			<g:render template="searchForm"/>
		</div>
	</div>
</div>

</body>
</html>