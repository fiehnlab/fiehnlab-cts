<g:render template="../infoBox"/>

<g:form name="searchForm" method="post" class="searchForm" controller="search" action="search">
	<div id="wait" class="wait">
		<g:img id="wait-img" file="spinner.gif"  />
	</div>

	<div class="center">
		<g:textField class="query" name="query" value="${params.query ?: ''}" tabindex="1"/>
		<label for="max">Hits:</label> <g:select name="max" from="${[10, 25, 50]}" value="${max}" tabindex="2"/>
		<g:submitButton value="CTS Search..." class="ui-corner-all query" name="searchButton" id="search" tabindex="3"/>
	</div>

	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
		${flash.message = ""}
	</g:if>
</g:form>