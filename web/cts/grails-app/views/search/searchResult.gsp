<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="grails.util.Environment" %>
<html>
<head>
	<meta name="layout" content="main"/>
	<title>Search Results</title>
</head>

<body>
<div id="main">
	<div class="page_header">
		<h2>Compound Search</h2>

		<p class="siteinfo">
			Welcome to the simple search dialog, which will allow you to query against the database
		</p>

		<div class="hr"></div>
	</div>

	<g:hasErrors>
		<g:eachError><p>${it.defaultMessage}</p></g:eachError>
	</g:hasErrors>
	<div class="box">
		<g:form name="searchForm" method="post" class="searchForm" controller="search" action="search">
			<div id="wait" class="wait">
				<g:img id="wait-img" file="spinner.gif"  />
			</div>

			<div class="form-background">
				<g:textField class="query" name="query" value="${params.query ?: ''}" tabindex="1"/>
				<label for="max">Hits:</label> <g:select class="query" name="max" from="${[10, 25, 50]}" value="${max}" tabindex="2"/>
				<g:submitButton value="CTS Search..." class="query right" name="searchButton" id="search" tabindex="3" onclick="ajaxStart()"/>
			</div>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
				${flash.message = ""}
			</g:if>

		</g:form>

		<g:if test="${results?.size() <= 0}">
			<p style="font-size: 0.95em; text-align: center;">Sorry, we couldn't find any matching results</p>
		</g:if>
		<g:else>
			<h3 class="space top tiny">Search Results</h3>

			<div class="hr"></div>
			<div class="left">

							<div id="results">

								<ul>
			<g:each in="${results}" var="compound" status="i">
				<li>
					<div>
						<g:link class="detail" action="show" controller="compound"
						        id="${compound?.inchiKey}">${compound?.inchiKey}</g:link>
						<ul>
							<g:if test="${compound?.synonyms.size() < 0}">
								<li>name: ${compound?.synonyms?.name?.get(0)?:''}</li>
							</g:if>
							<g:else>
								<li>name: ${compound?.inchiKey}</li>
							</g:else>
							<li>synonyms: ${compound?.synonyms?.size()}</li>
							<li>known id's: ${compound?.extIds?.size()}</li>
							<li>InChI code: ${compound?.inchiCode}</li>
							<li>InChI key: ${compound?.inchiKey}</li>
						</ul>
					</div>
				</li>
			</g:each>

			</ul>
			<g:if test="${offset > 0}"><p class="prevLink">
				<g:link controller="search" action="search"
				        params="${[query: query, max: max, total: total, offset: offset - max]}">Previous</g:link>
			</p>
			</g:if>
			<g:if test="${results?.size() && !(results?.size() < max)}"><p class="nextLink">
				<g:link controller="search" action="search"
				        params="${[query: query, max: max, total: total, offset: offset + max]}">Next</g:link>
			</p>
			</g:if>

			</div>
		</g:else>
	</div>
	</div>

	<div class="right">
	</div>
</div>
</body>
</html>
<script type="text/javascript">
	$('.wait').fadeOut();
</script>