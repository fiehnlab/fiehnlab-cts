<%@ page import="edu.ucdavis.fiehnlab.cts.Compound" %>
<%@ page import="edu.ucdavis.fiehnlab.cts.CtsTagsTagLib" %>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta name="keywords"
	      content="translator, chemical name to inchikey, conversion, pubchem, chemspider, cas, cross reference, chemichal name, database id, smiles to inchikey, inchi, smiles to inchi conversion, inchi to inchikey, inchi to inchikey conversion, inchikey to structure, chemical name to inchi"/>
	<meta name="description"
	      content="Publicly available chemical information including structures, chemical names, chemical synonyms, database identifiers, molecular masses, XlogP and proton-donor/acceptor data were downloaded from different databases and combined into a single internal repository for compound-specific, structure-based cross references"/>
	<meta name="author" content="Diego Pedrosa"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:layoutTitle default="Chemical Translation Service"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">

	<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'favicon.ico')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'favicon.ico')}">
	<!-- 		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">-->

	<link rel="stylesheet" type="text/css"
	      href="${resource(dir: 'js/jstree-v.pre1.0/themes/classic', file: 'style.css')}"/>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'cts2.css')}?noCache=${System.currentTimeMillis()}"
	      type="text/css">

	<g:javascript library="jquery" plugin="jquery"/>
	<g:javascript library="jquery" plugin="jquery-ui"/>

	<g:layoutHead/>
	<r:layoutResources/>
	<jqui:resources
			themeCss="${resource(dir: 'css/jquery-ui/themes/smoothness', file: 'jquery-ui-1.10.0.custom.css')}"/>

	<script type="text/javascript" src="${resource(dir: 'js/jstree-v.pre1.0', file: 'jquery.jstree.js')}"></script>

</head>

<body>
	<!-- analytics code -->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-39617172-1']);
		_gaq.push(['_trackPageview']);

		(function () {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>

<div class="ctsHeader">
		<div class="ctsTitle">Chemical Translation Service</div>
		<g:render template="/navigation-inline"/>
	</div>

	<script type="text/javascript">
		if (navigator.appName == 'Microsoft Internet Explorer') {
			document.write('<div class=\"warning\">we recommend that you use Firefox, Safari or Google Chrome for the best experience! '
					+ 'Right now you are using the Microsoft Internet Eplorer and we do not actively support or recommend this browser!</div>');
		}
	</script>


	<div id="spinner" class="spinner warning" style="display:none;">
		<g:message code="spinner.alt" default="Loading&hellip;"/>
	</div>

	<g:layoutBody/>
	<r:layoutResources disposition="defer"/>

	<div class="footer">
	</div>

	<g:javascript library="application"/>
	<r:layoutResources/>
<script type="text/javascript">$('.wait').fadeOut();</script>
</body>
</html>
