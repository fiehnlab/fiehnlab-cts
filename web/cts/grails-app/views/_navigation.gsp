<!--
general navigation template
-->
<g:setProvider library="jquery"/>

<div class="box">

    <h3>Services</h3>

	<div>
        <ul class="navigation">
            <li class="transform">
                <g:link controller="conversion">
                    <h4><g:message code="convert.simple" /></h4>
                    <h5><g:message code="convert.simple.description" /></h5>
                </g:link>
            </li>
            <li class="mass_transform">
                <g:link controller="conversion" action="batch">
                    <h4><g:message code="convert.batch" /></h4>
                    <h5><g:message code="convert.batch.description" /></h5>
                </g:link>
            </li>
	        <li class="mass_transform">
		        <g:link controller="moreServices">
			        <h4><g:message code="moreserv"/></h4>
			        <h5><g:message code="moreserv.description"/></h5>
		        </g:link>
	        </li>
	        <li class="discovery">
		        <g:link controller="contact">
			        <h4><g:message code="cts.menu.contact"/></h4>
		        </g:link>
	        </li>
        </ul>
	</div>
</div>
