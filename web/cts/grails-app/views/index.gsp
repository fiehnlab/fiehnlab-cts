<%--
  Created by IntelliJ IDEA.
  User: dpedrosa
  Date: 3/18/13
  Time: 11:52 AM
  To change this template use File | Settings | File Templates.
--%>

<!doctype html>
<%@ page import="grails.util.Environment" %>
<html>
<head>
	<meta name="layout" content="main"/>
	<title>Chemical Translation Service</title>
</head>

<body class="BodyBackground">
<div id="main">
	<div class="page_header">
		<h2>Home</h2>

		<div class="hr"></div>
	</div>

	<div class="left">
		<div class="box">
			<h3>Welcome</h3>

			<p>Publicly available chemical information
			including structures, chemical names, chemical synonyms, database
			identifiers, molecular masses, XlogP and proton-donor/acceptor data
			were downloaded from different databases and combined into a single
			internal repository for compound-specific, structure-based cross
			references. Molfile (SDF) to InChI code converters (vs. 1.0.2) and
			InChI code to InChI key converters were integrated into the tool
			set to allow any type of query access, e.g. by chemical names,
			structures, database identifiers. For data storage and retrieval we
			are using a PostgresSQL database.<br/>
			This database allows executing queries at far
			higher speed compared to web-based applications that query external
			repositories in real time. For accessibility we use a web GUI and a
			soap-based application-programming interface was implemented for
			automated access.</p>

			<div class="hr"></div>
		</div>

		<div class="box">
			<g:render template="/error"/>
			<g:render template="/conversion/simpleConversionForm" model="${[env: Environment]}"/>
		</div>
	</div>

	<div class="right">
		<g:render template="/navigation"/>
	</div>
</div>
</body>
</html>