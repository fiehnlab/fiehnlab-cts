<%@ page import="grails.util.Environment" %>
<%--
  Created by IntelliJ IDEA.
  User: Diego
  Date: 12/8/2015
  Time: 2:49 PM
--%>

<!doctype html>
<html>
<head>
	<title>Chemical Translation Service</title>
	<meta name="layout" charset="main">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">

	<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
	<!-- 		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">-->


	<link rel="stylesheet" type="text/css"
	      href="${resource(dir: 'js/jstree-v.pre1.0/themes/classic', file: 'style.css')}"/>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'cts2.css')}?noCache=${System.currentTimeMillis()}"
	      type="text/css">

</head>

<body>
<div class="center" style="border: 1px solid #aaa; margin-top: 20px; width: 60%; display: table">
	<div style="display: table-row;">
		<h1 style="background-color: #464646; color: #c3c3c3; text-align: center; font-size: 2em; padding: 0.1em;">Chemical Translation Service</h1>
	</div>

	<g:if test="${env.equals(Environment.DEVELOPMENT)}">
		<div>
			<p>this (${this?.count ?: 0})</p>
			<g:each in="${this?.properties}" var="p">
				<p>${p.key} = ${p.value}</p>
			</g:each>

			<p>model (${model?.count ?: 0})</p>
			<g:each in="${model?.properties}" var="p">
				<p>${p.key} = ${p.value}</p>
			</g:each>
		</div>
	</g:if>

	<div style="display: table-row; vertical-align: middle;">
		<div style="display: table-cell; text-align: center; width: 60%; float: left; margin-top: 50px;">
			<h2>The server encountered problems.</h2>

			<h2>An unexpected error occurred while processing your request.</h2>
			<h2>Please try again, or
				<a href="mailto:dpedrosa@ucdavis.edu? subject=[CTS ${Environment.current}] Application Error occured : '${exception?.message?.encodeAsHTML()}'">
					contact</a> the IT team
			</h2>

			<p class="code">Message:<br/>${exception?.message?.encodeAsHTML()}</p>
		</div>

		<div style="display: table-cell; text-align: right; float: right">
			<img width="250px" alt="TNT ballandstick" src="../images/tnt.png"/>
		</div>

	</div>
</div>
</body>
</html>
