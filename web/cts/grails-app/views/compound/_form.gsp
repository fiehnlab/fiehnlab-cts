<%@ page import="edu.ucdavis.fiehnlab.cts.Compound" %>



<div class="fieldcontain ${hasErrors(bean: compoundInstance, field: 'inchiCode', 'error')} ">
	<label for="inchiCode">
		<g:message code="compound.inchiCode.label" default="Inchi Code" />
		
	</label>
	<g:textField name="inchiCode" value="${compoundInstance?.inchiCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: compoundInstance, field: 'molWeight', 'error')} required">
	<label for="molWeight">
		<g:message code="compound.molWeight.label" default="Mol Weight" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="molWeight" value="${fieldValue(bean: compoundInstance, field: 'molWeight')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: compoundInstance, field: 'extIds', 'error')} ">
	<label for="extIds">
		<g:message code="compound.extIds.label" default="Ext Ids" />
		
	</label>
	<g:select name="extIds" from="${edu.ucdavis.fiehnlab.cts.ExternalId.list()}" multiple="multiple" optionKey="id" size="5" value="${compoundInstance?.extIds*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: compoundInstance, field: 'inchiKey', 'error')} ">
	<label for="inchiKey">
		<g:message code="compound.inchiKey.label" default="Inchi Key" />
		
	</label>
	<g:textField name="inchiKey" value="${compoundInstance?.inchiKey}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: compoundInstance, field: 'synonyms', 'error')} ">
	<label for="synonyms">
		<g:message code="compound.synonyms.label" default="Synonyms" />
		
	</label>
	<g:select name="synonyms" from="${edu.ucdavis.fiehnlab.cts.Synonym.list()}" multiple="multiple" optionKey="id" size="5" value="${compoundInstance?.synonyms*.id}" class="many-to-many"/>
</div>

