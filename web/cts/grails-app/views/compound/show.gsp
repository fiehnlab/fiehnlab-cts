<!doctype html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'compound.label', default: 'Compound')}"/>
	<title>Compound Details</title>
</head>

<body>

<div class="page_header">
	<h2>Compound Details</h2>

	<p class="siteinfo">
		Welcome, here you can see all information we have about the currently selected compound.
	</p>

	<div class="hr"></div>

	<div class="box">

		<div>
			<ul class="results">
				<li>
					<div class="left">
						<h3>General Information</h3>

						<ul>
							<li>Associated names: ${compoundInstance.synonyms.size()}</li>
							<li>Molecular weight: ${compoundInstance.molWeight}</li>
							<li>Exact mass: ${compoundInstance.exactMass}</li>
							<li>Formula: ${compoundInstance.formula}</li>
							<li>Known Id's: ${compoundInstance.extIds.size()}</li>
							<li>InChI code: ${compoundInstance.inchiCode}</li>
							<li>InChI key: ${compoundInstance.inchiKey}</li>
						</ul>

					</div>

					<div class="right">
						<h3>Structure Information</h3>

						%{--<g:structureMedium3D id="${compoundInstance.id}" inchi="${compoundInstance.inchiCode}"/>--}%
						<g:structureMedium id="${compoundInstance.id}" inchi="${compoundInstance.inchiCode}"/>
					</div>

					<div class="clear"></div>
				</li>
			</ul>
			<ul class="treeList">
				<h3>External Id Information</h3>

				<cts:extIdTree data="${compoundInstance.extIds}"/>
			</ul>
			<ul class="results">
				<li>
					<h3>IUPac Names</h3>

					<div id="iupac"><div class="spinner"></div></div>
					<jq:jquery>
						<g:remoteFunction controller="compound" action="ajaxGetIUPacNameForCompound"
						                  id="${compoundInstance.inchiKey}" update="iupac"/>
					</jq:jquery>
				</li>
				<li>
					<h3>Synonyms</h3>

					<div id="synonyms"><div class="spinner"></div></div>

					<jq:jquery>
						<g:remoteFunction controller="compound" action="ajaxGetSynonymsForCompound"
						                  id="${compoundInstance.inchiKey}" update="synonyms"/>
					</jq:jquery>
				</li>
			</ul>
		</div>
	</div>
</div>

</body>
</html>
