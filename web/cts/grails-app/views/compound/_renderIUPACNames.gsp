<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Name</th>

        </tr>
    </thead>
    <tbody>
    <g:each var="syn" in="${synonyms}">

        <tr>
            <td>${syn.type}</td>
            <td>${syn.name}</td>

        </tr>
    </g:each>

    </tbody>
</table>