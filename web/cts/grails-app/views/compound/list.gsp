
<%@ page import="edu.ucdavis.fiehnlab.cts.Compound" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'compound.label', default: 'Compound')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-compound" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-compound" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="inchiKey" title="${message(code: 'compound.inchiKey.label', default: 'Inchi Key')}" />
					
						<g:sortableColumn property="inchiCode" title="${message(code: 'compound.inchiCode.label', default: 'Inchi Code')}" />
					
						<g:sortableColumn property="molWeight" title="${message(code: 'compound.molWeight.label', default: 'Molecular Weight')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${compoundInstanceList}" status="i" var="compoundInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${compoundInstance.inchiKey}">${fieldValue(bean: compoundInstance, field: "inchiKey")}</g:link></td>
					
						<td>${fieldValue(bean: compoundInstance, field: "inchiCode")}</td>
					
						<td>${fieldValue(bean: compoundInstance, field: "molWeight")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${compoundInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
