
<!-- display our list in several columns to make it look nice and not take up so much space -->
<ul class="multi-column">
    <g:each in="${synonyms}" var="synonym" status="i">
        <li>${synonym.name}</li>
    </g:each>
</ul>
<br class="multi-column clear"/>