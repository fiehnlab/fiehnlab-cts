<div class='compound'>
<g:each in='${ compound?.synonyms }' var='syn'>
<g:if test='${"IUPAC Name (Preferred)".equals(syn.type)}'>
	<p><span class='compound.label'>Compound:</span> <span class='compound.value'>${syn.name}</span></p>
</g:if>
</g:each>
	<p class='compound.details'>
		<span class='compound.label'>InChI Key:</span> <span class='compound.value'>${compound?.inchiKey}<br/></span>
		<span class='compound.label'>InChI Code:</span> <span class='compound.value'>${compound?.inchiCode ?: "Not Found"}<br/></span>
		<span class='compound.label'>Molecular weight:</span> <span class='compound.value'>${compound?.molWeight}</span>
	</p>
	<div class="hr"></div>
	<g:each in="${compound?.extIds}" var="extid">
		<p>${extid?.name}: ${extid?.value}</p>
	</g:each>
	<g:each in="${compound?.synonyms}" var="syn">
		<p>${syn?.type}: ${syn?.name}</p>
	</g:each>
	<div class="hr"></div>

</div>
