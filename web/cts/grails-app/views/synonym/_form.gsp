<%@ page import="edu.ucdavis.fiehnlab.cts.Synonym" %>



<div class="fieldcontain ${hasErrors(bean: synonymInstance, field: 'type', 'error')} ">
	<label for="type">
		<g:message code="synonym.type.label" default="Type" />
		
	</label>
	<g:textField name="type" value="${synonymInstance?.type}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: synonymInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="synonym.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${synonymInstance?.name}"/>
</div>

