<%--
  Created by IntelliJ IDEA.
  User: diego
  Date: 4/22/14
  Time: 9:46 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<title>Contact</title>
	<meta name="layout" content="main">
</head>

<body class="BodyBackground">
<div id="main">
	<div class="page_header">
		<h2>Contact Information</h2>

		<div class="hr"></div>
	</div>

	<div class="left">
		<div class="box" style="padding-left: 10%;">
			<h3 class="bodybackground">Fiehn Lab</h3>
			<div class="hr"></div>

			<h4 class="box">Programmers:</h4>
			<a class="center" href="mailto:dpedrosa@ucdavis.edu">Diego Pedrosa</a> (Lead, Maintainer)<br/>
			<a class="center" href="mailto:wohlgemuth@ucdavis.edu">Gert Wohlgemuth</a><br/>
			<br/>
			<h4 class="box">Principal Investigator:</h4>
			<a class="center" href="mailto:ofiehn@ucdavis.edu">Dr. Oliver Fiehn</a>
		</div>
	</div>
</div>

<div class="right" style="width: 30%;">
	<g:render template="/navigation"/>
</div>
</div>
</body>
</html>
