<!doctype html>
<html>
<head>
	<title>Chemical Translation Service</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">

	<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
	<!-- 		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">-->


	<link rel="stylesheet" type="text/css"
	      href="${resource(dir: 'js/jstree-v.pre1.0/themes/classic', file: 'style.css')}"/>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'cts2.css')}?noCache=${System.currentTimeMillis()}"
	      type="text/css">
</head>

<body>
<div class="center" style="border: 1px solid #aaa; margin-top: 20px; width: 50%; display: table">
	<div style="display: table-row;">
		<h1 style="background-color: #464646; color: #c3c3c3; text-align: center; font-size: 2em; padding: 0.1em;">Chemical Translation Service</h1>
	</div>

	<div class="errors box" style="display: table-row; margin-top: 0px">
		<div class="errorMessage" style="text-align: center;">
			<p>404 - Not Found.</p>
		</div>
	</div>

	<div style="display: table-row; vertical-align: middle">
		<div style="display: table-cell; text-align: center; width: 50%; float: left; margin-top: 50px;">
			<h2>Sorry, the page you requested is not available.</h2>
			<br/>

			<p><a href="javascript:history.back();">Previous page.</a></p>
		</div>

		<div style="display: table-cell; text-align: right; width: 50%; float: right">
			<a title="Caffeine.
		By The original uploader was ALoopingIcon at English Wikipedia (Transferred from en.wikipedia to Commons.)
		[CC BY-SA 2.5 (http://creativecommons.org/licenses/by-sa/2.5)], via Wikimedia Commons"
			   href="https://commons.wikimedia.org/wiki/File%3ACaffeine_ballandstick.png">
				<img width="250"
				     alt="Caffeine ballandstick"
				     src="https://upload.wikimedia.org/wikipedia/commons/d/df/Caffeine_ballandstick.png"/>
			</a>

			<br/>
		</div>
	</div>
</div>
</body>
</html>
