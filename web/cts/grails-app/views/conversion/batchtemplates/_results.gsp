<%@ page import="grails.util.Environment" %>
%{-- setting up pagination params --}%
<g:if test="${values instanceof java.lang.String}">
	<g:set var="total" value="${values.split("\\r?\\n+").size()}"/>
</g:if>
<g:else>
	<g:set var="total" value="${values.size()}"/>
</g:else>

<g:set var="MAX_VALUES" value="${hits ?: 10}"/>

<g:if test="${env.equals(Environment.DEVELOPMENT) || env.equals(Environment.TEST)}">
	<p class="message" style="margin: 1em">values:
		<g:if test="${values?.size() > 100}">
			${values[0..100]} ... <br/>
		</g:if>
		<g:else>
			${values}<br/>
		</g:else>
		from: ${from}, to: ${to.toString()},<br/>
		max:${max}, offset:${offset}, total:${total}, end: ${end}, scoring: ${scoring}, hits: ${hits}
		results: ${results?.size()},<br/>
	</p>
</g:if>
<hr size="3"/>
<g:render template="./paginationLinks" model="${[values: values, scoring: scoring, max: max, offset: offset, hits: hits, end: end]}"/>
<hr size="3"/>

%{-- results table --}%
<g:set var="firstKey" value="${"f$from".toString()}"/>

<table>
	<thead>
	<tr><!-- table headers -->
		<th style="width: ${styleClassFrom}">${from}</th>
		<g:each in="${to}" var="toVal">
			<g:if test="${toVal.equals('Chemical Name')}">
				<g:set var="styleClass" value="30%"/>
			</g:if>
			<g:if test="${scoring && toVal.equals('InChIKey')}">
				<th style="width: ${styleClass}">${toVal}</th>
				<th style="width: ${styleClassScored}">Score</th>
			</g:if>
			<g:else>
				<th style="width: ${styleClass}">${toVal}</th>
			</g:else>
		</g:each>
	</tr>
	</thead>
	<tbody>
	<g:set var="i" value="${0}"/>
		<g:each in="${results}" var="row">
			<tr>
				%{-- search term column --}%
				<td>${offset + i++} - ${row[firstKey]}</td>

				%{-- found values columns --}%
				<g:each in="${to}" var="toVal">
					<td>${row[toVal]}</td>
				</g:each>
			</tr>
		</g:each>

	</tbody>
</table>
<script type="text/javascript">
	$(".wait").fadeOut();
</script>
