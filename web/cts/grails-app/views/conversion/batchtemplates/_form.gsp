<g:render template="../infoBox"/>
<g:set var="MAX_TEXT_BYTES" value="150000"/>

<g:form name="batchConversion" method="POST" controller="conversion" action="batchConvert">
	<div id="wait" class="wait">
		<g:img id="wait-img" file="spinner.gif"/>
	</div>

	<div class="center">
		<label for="values"><g:message code="conversion.value" default="Convert"/></label> -
	<g:message class="message" code="conversion.alert" default="(wilcards won't work)"/>
		<br/>
		<g:textArea class="query" name="values" value="${values?.join("\n")}" rows="5" tabindex="1" onkeyup="return imposeMaxLength(this, ${MAX_TEXT_BYTES})"/>

		<br/> <label for="from"><g:message code="conversion.from" default="From "/></label>
		<g:select class="query" name="from" from="${fromNames}" value="${from ?: "Chemical Name"}" tabindex="2"/>

		<label for="toTypes"><g:message code="conversion.to" default=" To "/></label>

		<div id="defaultIds">
			<table id="toTypes">
				<thead></thead>
				<tbody>
				<tr>
				<g:set var="max_cols" value="${5}" />
				<g:set var="col_ctr" value="${max_cols}" />
				<g:each in="${toNames[0..9]}" var="item">
					<g:if test="${col_ctr <= 0}">
						</tr>
						<tr>
						<g:set var="col_ctr" value="${max_cols}"/>
					</g:if>
					<td class="centered width-20"><g:checkBox name="to" value="${item}" checked="${to?.contains(item)}"
					                                          tabindex="${toNames.indexOf(item) + 3}"/> ${item}</td>
					<g:set var="col_ctr" value="${col_ctr - 1}"/>
				</g:each>
				<g:if test="${col_ctr > 0}">
					<td colspan="${col_ctr}"></td>
				</g:if>
				</tbody>
			</table>
		</div>  <!-- end default ids -->
		<div><a id="expandIds" href="#">More Ids...</a></div>

		<div id="extraIds" style="display: none;">
			<table id="toTypesExt">
				<thead></thead>
				<tbody>
				<tr>
				<g:set var="col_ctr" value="${max_cols}" />
				<g:each in="${toNames[10..-1]}" var="item">
					<g:if test="${col_ctr <= 0}">
						</tr>
						<tr>
						<g:set var="col_ctr" value="${max_cols}"/>
					</g:if>
					<td class="centered width-20"><g:checkBox name="to" value="${item}" checked="${to?.contains(item)}"
					                                          tabindex="${toNames.indexOf(item) + 3}"/> ${item}</td>
					<g:set var="col_ctr" value="${col_ctr - 1}"/>
				</g:each>
				<g:if test="${col_ctr > 0}">
					<td colspan="${col_ctr}"></td>
				</g:if>
				</tr>
				</tbody>
			</table>
		</div> <!-- end extraIds -->

		<div class="query">
			<g:checkBox name="scoring" value="${scoring}" tabindex="${toNames.size() + 3}"/>
			%{--<g:checkBox name="scoring" value="${params.scoring}" tabindex="${toNames.size() + 3}"/>--}%
			<label for="scoring">Sort results (biological importance)</label>
		</div>

		<label for="scoring"><g:message code="conversion.hits" default="Number of hits per result:"/></label>
		<select class="query" id="hits" name="hits" tabindex="${toNames.size() + 4}">
			<option value="${1}" ${(hits == null || hits == 1) ? "selected" : ""}>Top hit</option>
			<option value="${0}" ${hits == 0 ? "selected" : ""}>All hits</option>
		</select>
		<br/>

		<g:hiddenField name="offset" value="0"/>
		<label for="max"><g:message code="conversion.max" default="Results per page:"/></label>

		<g:select id="max" name="max" from="${[10, 25, 50]}" value="${max}" tabindex="${toNames.size() + 5}"/>

		<g:submitToRemote controller="conversion" action="batchConvert" update="results" name="convert" id="convertButton" value="Convert"
			class="query ui-button ui-state-active ui-corner-all" tabindex="${toNames.size() + 6}" />

		<g:if test="${flash.message}">
			<div class="errors">
				${flash.message}
			</div>
		</g:if>
	</div>
</g:form>
