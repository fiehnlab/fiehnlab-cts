<g:if test="${offset > 0}">
	<a class="prevLink" onclick="doPrevAndSubmit(document.forms[0]);">Previous</a>

	<a class="nextLink" onclick="<g:remoteFunction controller="conversion" action="batchConvert" params="${[from: from.toString(), to: to, values: values, max: max, offset: offset-max >= 0 ?: 0, total: total, end: end, scoring: scoring, hits: hits]}" />">Previous</a>
</g:if>

<g:if test="${end < total - 1 && end > 0}">
	<a class="nextLink" onclick="doNextAndSubmit(document.forms[0]);">Next</a>

	<a class="nextLink" onclick="<g:remoteFunction controller="conversion" action="batchConvert" params="${[from: from.toString(), to: to, values: values, max: max, offset: offset+max, total: total, end: end, scoring: scoring, hits: hits]}" />">Next</a>
</g:if>
