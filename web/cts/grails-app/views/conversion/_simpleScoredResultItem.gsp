<%@ page import="edu.ucdavis.fiehnlab.cts.ScoredValue" %>
<td>
	<div name="smallValues">
		<g:each in="${item ?: [new ScoredValue("Not Found", 0.0)]}" var="idValue">
			<g:if test="${idValue?.getValue().matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
				<g:link controller="compound" action="show" id="${idValue.value}">${idValue.value}<br/></g:link>
			</g:if>
			<g:else>
				${idValue.value}<br/>
			</g:else>
		</g:each>
	</div>
</td>
<td>
	<div name="smallValues">
		<g:each in="${item ?: [new ScoredValue("Not Found", 0.0)]}" var="idScore">
			${idScore.score}<br/>
		</g:each>
	</div>
</td>
