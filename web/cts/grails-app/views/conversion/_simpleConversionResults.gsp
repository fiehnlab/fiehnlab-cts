<%@ page import="grails.util.Environment; edu.ucdavis.fiehnlab.cts.ScoredValue" %>

<g:render template="simpleConversionForm"/>
<g:set var="MAX_VALUES" value="${hits ?: 10}"/>   %{-- defines the max amount of items to show in each column group (for a specific 'converted-to' type --}%

<g:if test="${results?.size() <= 0}">
	<p id="noresults">Sorry, we couldn't find any matching results</p>
</g:if>
<g:else>
	<h3 class="space top tiny">Conversion Results</h3>

	<div class="hr"></div>

	<div class="results">
		<form method="POST" name="simpleConversionExport" action="exportTo">
			<ctsexport:ctsformats controller="conversion" action="exportTo" formats="['excel']"
			                      params="${[values: values, from: from, to: to, max: 1000, offset: 0, total: 0, scoring: scoring, hits: hits]}"/>
		</form>

		<g:if test="${params.scoring}">
			<g:set var="styleClass" value="45%"/>
			<g:set var="styleClassScored" value="10%"/>
		</g:if>
		<g:else>
			<g:set var="styleClass" value="50%"/>
		</g:else>

		<g:set var="firstKey" value="${"f$from".toString()}"/>
		<table>
			<thead>
			%{-- add titles --}%
			<tr>
				<th style="width: ${styleClass}">${from}</th>
				<th style="width: ${styleClass}">${to}</th>
				<g:if test="${params.scoring}">
					<th style="width: ${styleClassScored}">Score</th>
				</g:if>
			</tr>
			</thead>
			<tbody>
			<g:each in="${results}" var="res">
				<tr>
				<g:if test="${res[firstKey]?.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
					<td><g:link controller="compound" action="show" id="${res[firstKey]}">${res[firstKey]}</g:link></td>
				</g:if>
				<g:else>
					<td>${res[firstKey]}</td>
				</g:else>

			%{-- displaying data for each group of 'converted to' values --}%
				<g:if test="${res[to] != [] && res[to].size() > MAX_VALUES.toInteger()}">
					<g:set var="idAttr" value="${from.replace(" ", "_")}_${res[firstKey].toString().replace(" ", "_").replaceAll(",", "-").replaceAll("'", "-")}"/>
					<td>
						<div name="partialValues" id="${idAttr}" colname="${to.replace(" ", "_")}">
							<span name="moreValues" id="${idAttr}" colname="${to.replace(" ", "_")}"></span>
							<g:each in="${res[to][0..(MAX_VALUES.toInteger() - 1)]}" var="idValue">
								<g:if test="${idValue?.value.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
									<g:link controller="compound" action="show" id="${idValue.value}">${idValue.value}<br/></g:link>
								</g:if>
								<g:else>${idValue.value}<br/></g:else>
							</g:each>
						</div>

						<div name="fullIdValues" id="${idAttr}" colname="${to.replace(" ", "_")}">
							<span name="lessValues" id="${idAttr}" colname="${to.replace(" ", "_")}"></span>
							<g:each in="${res[to][0..-1]}">
								<g:if test="${it?.value.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
									<g:link controller="compound" action="show" id="${it.value}">${it.value}<br/></g:link>
								</g:if>
								<g:else>
									${it.value}<br/>
								</g:else>
							</g:each>
						</div>
					</td>
					<td>%{-- displaying scores --}%
						<g:if test="${params.scoring}">
							<div name="partialValues" id="${idAttr}" colname="${to.replace(" ", "_")}">
								<span name="moreValues" id="${idAttr}" colname="${to.replace(" ", "_")}"></span>
								<g:each in="${res[to][0..(MAX_VALUES.toInteger() - 1)]?}" var="idScore">
									${idScore.score}<br/>
								</g:each>
							</div>

							<div name="fullIdValues" id="${idAttr}" colname="${to.replace(" ", "_")}">
								<span name="lessValues" id="${idAttr}" colname="${to.replace(" ", "_")}"></span>
								<g:each in="${res[to][0..-1] ?}" var="idScore">${idScore.score}<br/></g:each>
							</div>
						</g:if>
					</td>
				</g:if>
			%{-- displaying data when results are < 10 items --}%
				<g:else>
					<g:if test="${scoring}">
						<g:render template="simpleScoredResultItem" model="${[item: res[to], hits: hits]}"/>
					</g:if>
					<g:else>
						<g:render template="simplePlainResultItem" model="${[item: res[to], hits: hits]}"/>
					</g:else>
				</g:else>
			</g:each>
			</tr>
			</tbody>
		</table>

		<g:if test="${offset > 0}">
			<p class="prevLink">
				<g:link controller="conversion" action="convert"
				        params="${[values: values, from: from, to: to, max: max, total: total, offset: offset - max, hits: params.hits]}">Previous</g:link>
			</p>
		</g:if>
		<g:if test="${!(results.size() < max)}">
			<p class="nextLink">
				<g:link controller="conversion" action="convert"
				        params="${[values: values, from: from, to: to, max: max, total: total, offset: offset + max]}">Next</g:link>
			</p>
		</g:if>
		<div>&nbsp;</div>
	</div>
</g:else>
<script type="text/javascript">$('.wait').fadeOut();</script>
