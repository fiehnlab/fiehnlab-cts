<div class="box">
	<h3 class="header ui-corner-top" id="compound-header">Results (${data?.compounds?.size()==null?'0':data?.compounds?.size()})</h3>
	<span class="content ui-corner-bottom" id="compound-content">
		<g:each in="${data.compounds}" var="compound">
			<cts:compound compound="${compound}" />
			<hr />
			<table width="90%" id="results-table">
				<thead>
					<tr>
						<th style="text-align: center;">From</th>
						<th style="text-align: center;">To</th>
					</tr>
				</thead>
				<g:each in="${compound?.extIds}" var="item">
					<g:if test="${req?.to.equals(item.name)}">
						<tr>
							<td style="text-align: center;">
								${req?.from}: ${req?.value}
							</td>
							<td style="text-align: center;">
								${req?.to}: ${item.value}
							</td>
						</tr>
					</g:if>
				</g:each>
				<g:each in="${compound?.synonyms}" var="syn">
					<g:if test="${ 'Chemical Name'.equals(req?.to) }">
						<tr>
							<td style="text-align: center;">
								${req?.from}: ${req?.value}
							</td>
							<td style="text-align: center;">
								${syn?.type}:<br /> ${syn.name}
							</td>
						</tr>
					</g:if>
				</g:each>
			</table>
			<hr />
		</g:each>
	</span>
</div>
<span class="hr" style="display: block;"></span>
<div class="box">
	<h3 class="header ui-corner-top" id="relevant-header">Relevant results (${data?.relevant?.size()==null?'0':data?.relevant?.size()})</h3>
	<span class="content ui-corner-bottom" id="relevant-content">
		<g:each in="${data.relevant}" var="compound">
			<cts:compound compound="${compound}" />
		</g:each>
	</span>
</div>