<%@ page import="edu.ucdavis.fiehnlab.cts.ScoredValue" %>
<td>
	<div name="smallValues">
		<g:each in="${item ?: ["Not Found"]}" var="idValue">
			<g:if test="${idValue?.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
				<g:link controller="compound" action="show" id="${idValue.value}">${idValue.value}<br/></g:link>
			</g:if>
			<g:else>
				${(idValue instanceof ScoredValue)?idValue.value:idValue}<br/>
			</g:else>
		</g:each>
	</div>
</td>
