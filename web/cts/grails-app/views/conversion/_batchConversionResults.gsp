<%@ page import="edu.ucdavis.fiehnlab.cts.ScoredValue; grails.util.Environment" %>
<g:if test="${values instanceof java.lang.String}">
	<g:set var="total" value="${values.split("\\r?\\n+").size()}"/>
</g:if>
<g:else>
	<g:set var="total" value="${values.size()}"/>
</g:else>

<g:render template="batchConversionForm"/>
<g:set var="MAX_VALUES"
       value="${hits ?: 10}"/>   %{-- defines the max amount of items to show in each column group (for a specific 'converted-to' type --}%

<g:if test="${results?.size() <= 0}">
	<p style="font-size: 0.95em; text-align: center;">Sorry, we couldn't find any matching results</p>
</g:if>
<g:else>
	<div class="results">
		<form method="POST" name="batchConversionExport" action="batchExport">
			<ctsexport:ctsformats controller="conversion" action="batchExport" formats="['excel']"
			                      params="${[values: values, from: from, to: to, max: 100, offset: offset, total: total, scoring: scoring, hits: hits]}"/>
		</form>
		<g:if test="${to instanceof String}">
			<g:set var="to" value="${[to]}"/>
		</g:if>

		%{-- setting up paging links --}%
		<g:render template="paginationLinks" model="${[offset: offset, end: end, total: total]}"/>

		%{-- setting up dynamic column sizes --}%
		<g:set var="tableWidth" value="${100}"/>
		<g:set var="styleClassFrom" value="${15}"/>
		<g:set var="styleClassScored" value="${0}"/>

		<g:if test="${from.equals('Chemical Name')}">
			<g:set var="styleClassFrom" value="${30}"/>
		</g:if>
		<g:set var="tableWidth" value="${tableWidth - styleClassFrom}"/>

		<g:if test="${scoring}">
			<g:set var="styleClassScored" value="${5}"/>
		</g:if>

		<g:set var="tableWidth" value="${tableWidth - (styleClassScored * to?.size())}"/>
		<g:if test="${to.contains('Chemical Name')}">
			<g:set var="tableWidth" value="${tableWidth - 30}"/>
			<g:set var="styleClass" value="${(tableWidth / Math.max(to?.size() - 1, 1))}"/>
		</g:if>
		<g:else>
			<g:set var="styleClass" value="${(tableWidth / to?.size())}"/>
		</g:else>

		<g:set var="firstKey" value="${"f$from".toString()}"/>
		<table>
			<thead>
			<tr><!-- table headers -->
				<th style="width: ${styleClassFrom}%">${from}</th>
				<g:each in="${to}" var="toVal">
					<g:if test="${toVal.equals('Chemical Name')}">
						<g:set var="styleClassFinal" value="30"/>
					</g:if>
					<g:else>
						<g:set var="styleClassFinal" value="${styleClass}"/>
					</g:else>
					<g:if test="${scoring}">
						<th style="width: ${styleClassFinal}%">${toVal}</th>
						<th style="width: ${styleClassScored}%">Score</th>
					</g:if>
					<g:else>
						<th style="width: ${styleClassFinal}%">${toVal}</th>
					</g:else>
				</g:each>
			</tr>
			</thead>

			<!-- RESULTS -->
			<tbody>
			<g:each in="${results}" var="row">
				<tr>
				%{--show From values--}%
					<g:if test="${row[firstKey]?.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
						<td>
							<g:link controller="compound" action="show" id="${row[firstKey]}">${row[firstKey]}</g:link>
						</td>
					</g:if>
					<g:else>
						<td>${row[firstKey]}</td>
					</g:else>

				%{--show to values--}%
					<g:each in="${to}" var="toVal">
						<g:if test="${row[toVal]?.size() > MAX_VALUES.toInteger()}">
							<g:set var="idAttr"
							       value="${from.replace(" ", "_")}_${row[firstKey].toString().replaceAll(" ", "_").replaceAll(",", "-").replaceAll("'", "-").replaceAll("\\(|\\)", "-")}"/>
							<g:if test="${scoring}">%{-- if results are scored and we are in the inchikey column...--}%
								<td>%{-- display inchis for expanded list of values --}%
									<div name="partialValues" id="${idAttr}" colname="${toVal.replace(" ", "_")}">
										<span style="float: left;">
											<g:each in="${row[toVal][0..MAX_VALUES.toInteger() - 1]}" var="idValue">
												<g:if test="${idValue?.value.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
													<g:link controller="compound" action="show"
													        id="${idValue.value}">${idValue.value.trim()}<br/></g:link>
												</g:if>
												<g:else>${idValue.value.trim()}<br/></g:else>
											</g:each>
										</span>
										<span name="moreValues" id="${idAttr}"
										      colname="${toVal.replace(" ", "_")}"></span>
									</div>

									<div name="fullIdValues" id="${idAttr}" colname="${toVal.replace(" ", "_")}">
										%{--<r:img dir="images/skin/flag.png"/>--}%
										%{--</span>--}%
										<span style="float: left;">
											<g:each in="${row[toVal][0..-1]}">
												<g:if test="${it?.value.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
													<g:link controller="compound" action="show"
													        id="${it.value}">${it.value.trim()}<br/></g:link>
												</g:if>
												<g:else>${it.value.trim()}<br/></g:else>
											</g:each>
										</span>
										<span name="lessValues" id="${idAttr}"
										      colname="${toVal.replace(" ", "_")}"></span>
									</div>
								</td>
								<td>%{-- displaying scores for expanded list of values --}%
									<div name="partialValues" id="${idAttr}" colname="${toVal.replace(" ", "_")}">
										<span style="float: left;">
											<g:each in="${row[toVal][0..MAX_VALUES.toInteger() - 1]}" var="idValue">
												${idValue.score}<br/>
											</g:each>
										</span>
										<span name="moreValues" id="${idAttr}"
										      colname="${toVal.replace(" ", "_")}"></span>
									</div>

									<div name="fullIdValues" id="${idAttr}" colname="${toVal.replace(" ", "_")}">
										<span style="float: left;">
											<g:each in="${row[toVal][0..-1]}">${it.score}<br/></g:each>
										</span>
										<span name="lessValues" id="${idAttr}"
										      colname="${toVal.replace(" ", "_")}"></span>
									</div>
								</td>
							</g:if>
							<g:else>%{-- no scoring in results --}%
								<td>
									<div name="partialValues" id="${idAttr}" colname="${toVal.replace(" ", "_")}">
										<span style="float: left;">
											<g:each in="${row[toVal][0..MAX_VALUES.toInteger() - 1]}" var="idValue">
												<g:if test="${idValue?.value.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
													<g:link controller="compound" action="show"
													        id="${idValue.value}">${idValue.value.trim()}<br/></g:link>
												</g:if>
												<g:else>${idValue.value.trim()}<br/></g:else>
											</g:each>
										</span>
										<span name="moreValues" id="${idAttr}"
										      colname="${toVal.replace(" ", "_")}"></span>
									</div>

									<div name="fullIdValues" id="${idAttr}" colname="${toVal.replace(" ", "_")}">
										<span style="float: left;">
											<g:each in="${row[toVal][0..-1]}">
												<g:if test="${it?.value.matches("[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]")}">
													<g:link controller="compound" action="show"
													        id="${it.value}">${it.value.trim()}<br/></g:link>
												</g:if>
												<g:else>${it.value.trim()}<br/></g:else>
											</g:each>
										</span>
										<span name="lessValues" id="${idAttr}"
										      colname="${toVal.replace(" ", "_")}"></span>
									</div>
								</td>
							</g:else>
						</g:if>
						<g:else>%{-- display all values in one cell --}%
						%{--check for scored or plain result--}%
							<g:if test="${scoring}">
								<g:render template="batchScoredResultItem" model="${[item: row[toVal]]}"/>
							</g:if>
							<g:else>
								<g:render template="batchPlainResultItem" model="${[item: row[toVal]]}"/>
							</g:else>
						</g:else>
					</g:each>
				</tr>
			</g:each>
			</tbody>
		</table>

		<g:render template="paginationLinks" model="${[offset: offset, end: end, total: total]}"/>
		<div>&nbsp;</div>
	</div>
</g:else>
<script type="text/javascript">$('.wait').fadeOut();</script>
