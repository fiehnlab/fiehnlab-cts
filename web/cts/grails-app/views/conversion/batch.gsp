<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="grails.util.Environment" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.util.regex.Pattern" %>

<html>
<head>
	<meta name="layout" content="main"/>
	<export:resource/>
	<title>Chemical Translation Service</title>
</head>

<body class="BodyBackground">

<div id="main">
	<div class="page_header">
		<h2>Batch Conversion</h2>

		<p class="siteinfo">Welcome to the batch conversion form, this will allow you to cross-reference many items of
		the specified type to many other types of compound data.</p>

		<div class="hr"></div>
	</div>

	<div class="box">
		<div class="space top small">
			<g:render template="batchConversionForm" model="[from: from, to: to, env: Environment.currentEnvironment]"/>
		</div>
		<div id="results">
			hello
		</div>
	</div>
</div>
</body>
</html>
