<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="grails.util.Environment" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.util.regex.Pattern" %>

<html>
<head>
	<meta name="layout" content="main"/>
	<export:resource/>
	<title>Chemical Translation Service</title>
</head>

<body class="BodyBackground">
<g:if test="${env.equals(Environment.DEVELOPMENT) || env.equals(Environment.TEST)}">
	<p class="message" style="padding: 10px">
		values:<br/>
		<g:if test="${values?.size() > 100}">
			${values[0..100]} ... <br/>
		</g:if>
		<g:else>
		to search: ${values}<br/>
		</g:else>
		from: ${from},<br/>
		to: ${to.toString()},<br/>
		max:${max},<br/>
		offset:${offset},<br/>
		total:${total},<br/>
		results: ${results?.size()},<br/>
		end: ${end} <br/>
		scoring: ${scoring},<br/>
		hits: ${hits}
	</p>
</g:if>

<div id="main">
	<div class="page_header">
		<h2>Batch Conversion</h2>

		<p class="siteinfo">Welcome to the batch conversion form, this allows you to cross-reference many items of the specified identifier to many other identifier types.</p>

		<div class="hr"></div>
	</div>

	<div class="box">
		<g:if test="${params.action != "batchConvert"}">
			<div class="space top small">
				<g:render template="batchConversionForm" model="${[env: Environment.currentEnvironment]}"/>
			</div>
		</g:if>
		<g:else>
			<g:render template="batchConversionResults"
			          model="${[fromNames: fromNames, toNames: toNames, results: results, end: end,
			                    from: from.toString(), to: to, values: params.values, env: Environment.currentEnvironment,
			                    max: max, offset: offset, total: total, scoring: params.scoring]}"/>
		</g:else>
	</div>
</div>
</body>
</html>
