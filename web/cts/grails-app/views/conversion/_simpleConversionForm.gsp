<g:render template="/conversion/notice"/>
<h3>Simple Conversion</h3>
<g:form name="simpleConversion" method="POST" controller="conversion" action="convert">
	<div id="wait" class="wait">
		<g:img id="wait-img" file="spinner.gif"/>
	</div>

	<g:set var="tabIndex" value="1"/>
	<div class="center">
		<g:textField class="query" name="values" value="${values}" tabindex="${tabIndex++}" maxlength="300"/>
	</div>

	<div class="center">
		<label for="from"><g:message code="conversion.from" default="from"/></label>
		<g:select class="query" name="from" from="${fromNames}" value="${from ?: "Chemical Name"}" tabindex="${tabIndex++}"/>

		<label for="to"><g:message code="conversion.to" default="to"/></label>
		<g:select class="query" name="to" from="${toNames}" value="${to ?: "InChIKey"}" tabindex="${tabIndex++}"/>

		<div class="query">
			<g:checkBox name="scoring" value="${scoring}" tabindex="${tabIndex++}"/>
			<label for="scoring">Show scores</label>
		</div>

		<label for="hits"><g:message code="conversion.hits" default="Number of hits per result:"/></label>
		<select class="query" id="hits" name="hits" tabindex="${tabIndex++}">
			<option value="${1}" ${(hits == null || hits == 1) ? "selected" : ""}>Top hit</option>
			<option value="${0}" ${hits == 0 ? "selected" : ""}>All hits</option>
		</select>
		<br/>

		<g:submitButton name="convert" id="convertButton" value="Convert" class="query ui-button ui-state-active ui-corner-all" tabindex="${tabIndex++}"/>


		<g:if test="${flash.message}">
			<div class="errors">
				${flash.message}
			</div>
		</g:if>
	</div>
</g:form>
