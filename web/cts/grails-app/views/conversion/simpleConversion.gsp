<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="grails.util.Environment" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.util.regex.Pattern" %>

<html>
<head>
	<meta name="layout" content="main"/>
	<export:resource/>
	<title>Chemical Translation Service</title>
</head>

<body class="BodyBackground">
<g:if test="${env.equals(Environment.DEVELOPMENT) || env.equals(Environment.TEST)}">
	<p class="message" style="padding: 10px">
		query: ${values}, <br>
		from: ${from}, <br>
		to: ${to.toString()}, <br/>
		max:${max}, <br/>
		offset:${offset}, <br/>
		total:${total}, <br/>
		res: ${results}, <br/>
		scoring: ${scoring}, <br/>
		hits: ${hits}... ${hits?.class}
	</p>
</g:if>

<div id="main">
	<div class="page_header">
		<h2>Simple Conversion</h2>

		<p class="siteinfo">Welcome to the simple conversion form, which allows one-to-one identifier conversions</p>

		<div class="hr"></div>
	</div>

	<div class="box">
		<g:if test="${params.action != "convert"}">
			<div class="space top small">
				<g:render template="simpleConversionForm"/>
			</div>
		</g:if>
		<g:else>
			<g:render template="simpleConversionResults"
			          model="${[fromNames: fromNames, toNames: toNames, results: results, hits: hits,
			                    from: from.toString(), to: to, values: values,
			                    max      : max, offset: offset, total: total, scoring: scoring]}"/>
		</g:else>
	</div>
</div>
</body>
</html>
