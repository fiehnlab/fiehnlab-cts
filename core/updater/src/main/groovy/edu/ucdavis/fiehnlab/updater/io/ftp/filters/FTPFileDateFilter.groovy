package edu.ucdavis.fiehnlab.updater.io.ftp.filters

import org.apache.commons.net.ftp.FTPFile
import org.apache.log4j.Logger

/**
 * Created by diego on 3/16/15.
 */
class FTPFileDateFilter extends AbstractFTPFileFilter {
	Date date = new Date().clearTime()
	private static Logger log = Logger.getLogger(FTPFileDateFilter.class)

	public FTPFileDateFilter(Date date) {
		this.date = date.clearTime()
	}

	@Override
	public boolean accept(FTPFile ftpFile) {
		if (log.traceEnabled) {
			log.debug("diff (${ftpFile.timestamp.time} - ${date}):${ftpFile.timestamp.time - date}")
			log.debug("compareTo: ${ftpFile.timestamp.time.compareTo(date)}")
		}
		return (ftpFile.isFile() && ftpFile.getName().endsWith(".sdf.gz") && ftpFile.timestamp.time.compareTo(date))
	}
}
