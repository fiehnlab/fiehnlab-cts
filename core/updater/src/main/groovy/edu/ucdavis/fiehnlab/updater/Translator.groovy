package edu.ucdavis.fiehnlab.updater
/**
 * Created by diego on 9/22/15.
 */
interface Translator {
//	CollectionResource<? extends Resource> translate(String filename)
	List<String> translate(List<String> files) throws IOException
}
