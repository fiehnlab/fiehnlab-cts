package edu.ucdavis.fiehnlab.updater

import edu.ucdavis.fiehnlab.updater.controller.UpdaterController
import org.apache.log4j.Logger
/*
 * Created by diego 05/03/2015
 *
 */

public class Updater {
	private static Logger log = Logger.getLogger(Updater.class.name)
	private final static String CONFIG_FILE = "config.properties"
	private ConfigObject config
	private File cf //config file

	public static void main(String[] args) {
		Updater updater = new Updater(CONFIG_FILE)

		int code = updater.run();

		code ? log.info("Update Successful") : log.info("There were errors in update")
		System.exit(code);
	}

	public ConfigObject getConfig() {
		return this.config
	}

	public Updater(ConfigObject config) {
		this.config = config
	}

	public Updater(String file) {

		if(log.debugEnabled) {
			log.debug "config file: ${file}"
		}

		try {
			def props = new Properties()
			cf = new File(file)
			if (!cf.exists()) {
				cf = new File(getClass().getResource("/${file}").file)
			}
			cf.withInputStream {
				stream -> props.load(stream)
			}
			config = new ConfigSlurper().parse(props)

		} catch (FileNotFoundException e) {
			log.error("Error loading configuration file '${file}'", e)
		} catch (Exception e) {
			log.error("Something bad happened: ${e.cause}: ${e.message}")
		}
	}

	public int run() {

		UpdaterController controller = new UpdaterController(config)

		try {
			controller.update()
		} catch(Exception e) {
			log.error "ERROR: ${e.message}", e
			return 1
		}

		return 0
	}

//	public Updater(IntConfig c) {
//		init(c);
//
//		log.debug("using database: ${config.sql.address}")
//	}
//
//	private void finishUpdate() {
//		config.save()
//		sql.close()
//		ftp.disconnect()
//	}
//
//	private void glogStart() {
//		def date = new Date()
//		log.info("Starting update (${date.format(dateFormat)}")
//		log.info("beginning update ${date.toString()}")
//		log.info("beginning update ${date.toString()}")
//	}
//
//	private void init(IntConfig c) {
//		try {
//			config = c
//			if (!(boolean)config.getProperty("test")) {
//				//connecting sql
//				String SAddress = config.getProperty("sql.address")
//				String SName = config.getProperty("sql.name")
//				String SPass = config.getProperty("sql.password")
//				String SDriver = config.getProperty("sql.driver")
//
//				sql = Sql.newInstance(SAddress, SName, SPass, SDriver)
//
//				//connecting ftp
//				ftp = new FTPClient()
//				ftp.setConnectTimeout(5000)
//				ftp.connect(config.getProperty("ftp.address"))
//				ftp.setControlKeepAliveTimeout(300)
//				ftp.setDataTimeout(5000)
//				ftp.setSoTimeout(5000)
//				ftp.enterLocalPassiveMode()
//				ftp.login(config.getProperty("ftp.name"), config.getProperty("ftp.password"))
//				ftp.setBufferSize(33554432)
//
//				config.setFTP(ftp)
//			}
//		} catch (Exception e) {
//			e.printStackTrace()
//			log.error("Error initializing the updater service.", e)
//		}
//	}

//	public void intervalUpdate(int interval) {
//		try {
//
//			//gathering list of paths to update files
//			List updatePaths = findUpdatePaths(new Date(), interval)
//
//			if (updatePaths.size() < 1) {
//				log.info("no updates found")
//				return
//			}
//
//			updatePaths.each { path ->
//				//updating from each path
//				updateFolder(path)
//			}
//		}
//		catch (Exception e) {
//			e.printStackTrace()
//			log.error("intervalUpdate", e)
//		}
//	}
//
//	public void continueUpdate() {
//		String status //status of last updated directory (complete/incomplete)
//		String date //date of last update
//		String type //type of last directory (comp/sub)
//		String interval
//		Date parsedDate //date as a Date object
//		int dateDiff //time in days since last update
//		interval = config.getProperty("interval")
//		def last = config.getProperty("last.update")
//
//		try {
//
//			try {
//				status = last.substring(last.length() - 10, last.length())
//			} catch (Exception e) {
//				errLog.error("finding last update", e)
//				mainLog.info("could not identify previous update, applying all $interval updates.")
//				try {
//					def dirs = ftp.listDirectories("/pubchem/Compound/$interval/")
//					def dirlist = []
//					dirs.each { dir ->
//						dirlist.add("pubchem/Compound/$interval/" + dir.getName() + "/SDF/")
//					}
//					dirs = ftp.listDirectories("/pubchem/Substance/$interval/")
//					dirs.each { dir ->
//						dirlist.add("pubchem/Substance/$interval/" + dir.getName() + "/SDF/")
//					}
//					dirlist.each { dir ->
//						updateFolder(dir)
//					}
//				} catch (Exception a) {
//					errLog.error("Error reprocessing last update directory: $last")
//				}
//				return
//			}
//			if (status[0] == ':') {
//				status = status.substring(2, status.length())
//			}
//
//			date = last.substring(0, 10)
//
//
//			if (last[12] == 'C') type = "Compound"
//			if (last[12] == 'S') type = "Substance"
//			log.info("$status $date")
//
//			if (status == "incomplete") {
//				/*if the most recently used update directory was not finished
//				finish updating from that directory
//				 */
//
//				def PID = Integer.parseInt(last.substring(last.length() - 38, last.length() - 29))
//				def dir = "/pubchem/$type/$interval/$date/SDF/"
//				log.info("")
//				log.info("continuing update from from folder $dir")
//
//				List<FTPFile> files = ftp.listFiles(dir)
//				files.each {
//					String file = it.getName()
//					if (file[0] == 'C' || file[0] == 'S') {
//						if (Integer.parseInt(file.substring(file.length() - 16, file.length() - 7)) >= PID) {
//
//							log.info("")
//							log.info(it.getName())
//							updateFile(dir, file)
//						}
//					}
//				}
//			}
//			if (type == "Compound") {
//				/*if the most resently updated directory was a compound directory
//				update the corrosponding substance directory
//				(compounds are always done first)
//				 */
//				updateFolder("/pubchem/Substance/$interval/$date")
//
//			}
//
//			//update everything that came after the most recently applied update
//			parsedDate = new SimpleDateFormat(dateFormat).parse(date)
//			parsedDate++ //making sure the same update is not reapplied
//			dateDiff = TimeUnit.MILLISECONDS.toDays(new Date().getTime() - parsedDate.getTime())
//			intervalUpdate(dateDiff)
//		} catch (Exception e) {
//			log.error("Error parsing folder from: ${parsedDate}", e)
//		}
//	}
//
//	private List findUpdatePaths(Date date, int period) {
//
//		List updates = []
//		List folderNames = []
//		String interval = config.getProperty("Interval")
//
//		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat)
//
//		//gathering a list of the update folders
//		String compPath = "/pubchem/Compound/$interval/"
//		String subPath = "/pubchem/Substance/$interval/"
//
//		compDirs = config.getProperty(compPath)
//		subDirs = config.getProperty(subPath)
//
//		if (period > 0) {
//
//			//making a list of the possible update folder names
//			for (int i = 0; i < period; i++) {
//				date--
//				folderNames.add(sdf.format(date))
//			}
//
//			folderNames.each { folder ->
//
//				compDirs.each { dir ->
//					if (dir == compPath + folder) {
//						updates.add(dir)
//					}
//				}
//
//				subDirs.each { dir ->
//					if (dir == subPath + folder) {
//						updates.add(dir)
//					}
//				}
//			}
//		} else {
//			compDirs.each { dir ->
//				updates.add(dir)
//			}
//			subDirs.each { dir ->
//				updates.add(dir)
//			}
//		}
//
//		return updates
//	}
//
//	private void updateFolder(String path) {
//
//		def type
//
//		if (path[8] == 'C') {
//			type = "Compound"
//		}
//		if (path[8] == 'S') {
//			type = "Substance"
//		}
//		log.info("")
//		log.info("updating from folder $path")
//		totalUpdates = 0
//		successfulUpdates = 0
//		try {
//			removeKilledIDs(path)
//		} catch (FTPConnectionClosedException e) {
//			log.error("", e)
//			ftp.connect("ftp.ncbi.nih.gov")
//			ftp.enterLocalPassiveMode()
//			ftp.login("anonymous", "")
//		}
//		catch (Exception e) {
//			log.error("", e)
//		}
//
//		ftp.listFiles("$path/SDF/").each {
//			def T = it.getName().substring(0, 1)
//			if (T == 'C' || T == 'S') {
//				if (T == 'C') {
//					type = "Compound"
//				}
//				if (T == 'S') {
//					type = "Substance"
//				}
//
//				totalUpdates++
//				log.info("")
//				log.info(it.getName())
//
//				try {
//					updateFile("$path/SDF/", it.getName())
//				}
//
//				catch (NullPointerException a) {/*updateFile() produces null pointers whenever it encounters another error*/
//				}
//
//				catch (FTPConnectionClosedException e) {
//					//when disconnected from ftp the connection is reestablished and an update is reattepted.
//					log.error("connection closed, retrying: " + it.getName(), e)
//					ftp.connect(config.getProperty("ftp.address"))
//					ftp.enterLocalPassiveMode()
//					ftp.login(config.getProperty("ftp.name"), config.getProperty("ftp.password"))
//
//					try {
//						updateFile("$path/SDF/", it.getName())
//					}
//
//					catch (NullPointerException b) {/*updateFile() produces null pointers whenever it encounters another error*/
//					}
//
//					catch (FTPConnectionClosedException f) {
//						//if the second attempt fails the connection is reestablished and the file is skipped
//						log.error("unable to retrieve file: ${it.getName()}", f)
//						ftp.connect(config.getProperty("ftp.address"))
//						ftp.enterLocalPassiveMode()
//						ftp.login(config.getProperty("ftp.name"), config.getProperty("ftp.password"))
//					}
//				}
//				catch (Exception e) {
//					log.error("${it.getName()} in main", e)
//				}
//			}
//		}
//
//		config.setProperty("last.update", (String) "${path.substring(path.length() - 15, path.length() - 5)}: $type: complete")
//		config.save()
//		log.info("${successfulUpdates} update files successfully applied out of ${totalUpdates} files")
//	}
//
//	private void removeKilledIDs(String path) {
//		String fileName = ftp.listFiles(path).find { it.getName().substring(0, 1) == "k" }.getName()
//		def file = new File(fileName)
//		def fos = new FileOutputStream(fileName)
//
//		try {
//
//			ftp.retrieveFile("$path/$fileName", fos)
//			if (config.getProperty("test") != "true") {
//				if (path.substring(9, 10) == 'C') {
//					file.eachLine { killCID(it) }
//				}
//				if (path.substring(9, 10) == 'S') {
//					file.eachLine { killSID(it) }
//				}
//			}
//
//		} catch (Exception e) {
//			log.error("removeKilledIDs", e)
//		}
//
//		finally {
//			fos.close()
//			file.delete()
//		}
//	}
//
//	private void killCID(String id) {
//		try {
//			def idEntry = sql.firstRow("select inchi_key from external_id where name = 'PubChem CID' and value = ?", [id])
//
//			if (idEntry == null) {
//				log.info("unable to kill CID $id; entry not found")
//				return
//			}
//
//			def inchi = idEntry.inchi_key
//
//			log.info("CID $id killed")
//
//			sql.execute("delete from synonym where inchi_key = ?", [inchi])
//			sql.execute("delete from partial_inchi where inchi_key = ?", [inchi])
//			sql.execute("delete from external_id where inchi_key = ?", [inchi])
//			sql.execute("delete from compound where id = ?", [inchi])
//
//		}
//		catch (Exception e) {
//			log.error("killCID", e)
//		}
//	}
//
//	private void killSID(String id) {
//		/*
//		conflicts occur when two SIDs share an inchi key and a source file
//		*/
//
//		try {
//			def idEntry = sql.firstRow("select * from external_id where name = 'Pubchem SID' and value = ?", [id])
//
//			if (idEntry == null) {
//				log.info("unable to kill SID $id; entry not found")
//				return
//			}
//
//			def inchi = idEntry.inchi_key
//			def file = idEntry.source_file
//
//			List conflicts = []
//
//			sql.eachRow("select * from external_id where inchi_key = ? and name = 'Pubchem SID' and source_file = ?"[inchi, file])
//					{
//						if (it.value != id) {
//							conflicts.add(it.value)
//						}
//					}
//
//			if (conflicts.size() > 0) {
//				handleConflict(id, conflicts)
//				return
//			}
//
//			log.info("SID $id killed")
//
//			sql.execute("delete from external_id where inchi_key = ? and source_file = ?", [inchi, file])
//			sql.execute("delete from synonym where inchi_key = ? and source_file = ?", [inchi, file])
//
//		}
//
//		catch (Exception e) {
//			log.error("killSID", e)
//		}
//	}
//
//	private void killSIDWithoutCheck(String id) {
//		try {
//			def idEntry = sql.firstRow("select inchi_key from external_id where name = 'Pubchem SID' and value = ?", [id])
//
//			if (idEntry == null) {
//				log.info("unable to kill SID $id; entry not found")
//				return
//			}
//
//			def inchi = idEntry.inchi_key
//
//			log.info("SID $id killed")
//
//			sql.execute("delete from partial_inchi where inchi_key = ?", [inchi])
//			sql.execute("delete from compound where id = ?", [inchi])
//		}
//		catch (Exception e) {
//			log.error("killSIDWithoutCheck", e)
//		}
//	}
//
//	private void handleConflict(String sid, List conflicts) {
//		//handling the removal of SIDs from the same source file
//		File confFile = new File(config.log.conflict)
//		//File confFile = new File(config.getProperty("conflict.log"))
//
//		//if the file is empty the conflict is formatted and logged
//		if (confFile.size() == 0) {
//			String outp = sid + ":"
//			conflicts.each { outp += it + "," }
//			outp = outp.substring(0, outp.size() - 1)
//			confFile.write(outp)
//			return
//		}
//
//		//if the file is not empty each logged conflict is checked against
//		//the current conflict
//		List entries = confFile.readLines()
//		List marked = []
//		List unmarked = []
//		String outp = null
//		String removeStr = null
//		String addStr = null
//		boolean entryFound = false
//		//processing each line
//		//each character is added to a string untill , or : is encountered
//		//then the string is added to the correct list
//		//, indicates seperation between sids
//		//:indicates seperation between marked and unmarked sids
//		//ex: 123,321:555,444
//		entries.each { entry ->
//			marked.clear()
//			unmarked.clear()
//			String temp = ""
//			int group = 0
//			entry.each { chr ->
//				if (chr != ':') {
//					if (chr != ',') {
//						temp += chr
//					} else {
//						if (group == 0) {
//							marked.add(temp)
//						} else {
//							unmarked.add(temp)
//						}
//						temp = ""
//					}
//				} else {
//					marked.add(temp)
//					temp = ""
//					group = 1
//				}
//			}
//			unmarked.add(temp)
//
//			if (unmarked.any() { it == sid }) {
//				entryFound = true
//				removeStr = entry
//				marked.add(sid)
//				unmarked -= sid
//
//				//killing SIDs if the conflict is resolved
//				if (unmarked.size() == 0) {
//					if (config.test != "true") {
//						marked.each { killSIDWithoutCheck(it) }
//					}
//				} else {
//					//if the conflict is not resolved
//					//building new conflict string
//					addStr = ""
//					marked.each { addStr += it + ',' }
//					addStr = addStr.substring(0, addStr.size() - 1)
//					addStr += ":"
//					unmarked.each { addStr += it + ',' }
//					addStr = addStr.substring(0, addStr.size() - 1)
//				}
//				return //terminating .each
//			}
//		}
//
//		if (!entryFound) {
//			//building conflict entry if no matching conflicts were found
//			addStr = sid + ":"
//			conflicts.each { addStr += it + "," }
//			addStr = addStr.substring(0, addStr.size() - 1)
//		}
//		//rewriting conflict file
//		confFile.write("")
//		entries.each {
//			if (it != removeStr) {
//				confFile << it + "\n"
//			}
//		}
//		if (addStr != null) {
//			confFile << addStr + "\n"
//		}
//	}

//	private void updateFile(String path, String name) throws FTPConnectionClosedException {
//		def fos
//		def file
//		def list = []
//		try {
//			file = new File(name)
//			fos = new FileOutputStream(name)
//			def size = 0
//			String sType
//
//			try {
//				if (!ftp.retrieveFile(path + name, fos)) {
//					log.info("could not download $name")
//					log.error("could not download $name")
//					return
//				}
//
//				if (file == null) {
//					log.info("file null for $name")
//					log.error("null file")
//					return
//				}
//				if (file.size() > 1048576) {
//					size = Math.round(file.size() * 10 / 1048576) / 10
//					sType = "Mb"
//				} else {
//					size = Math.round(file.size() * 10 / 1024) / 10
//					sType = "kb"
//				}
//				log.info("file retrieved size: $size$sType")
//
//				try {
//					list.addAll(FileTranslator.translate(file))
//					log.info("translate complete ${list.size()} entries")
//				} catch (Throwable e) {
//					log.info("translate exception: ${e.getClass()}")
//					log.error("$name translation exception", e)
//				}
//				if (list.isEmpty()) {
//					log.info("update failed")
//					return
//				}
//
//			}
//			catch (Exception e) {
//				log.info("file: $name: unable to update")
//				throw e
//			}
//
//			list.each {
//				boolean skip = false
//				String id
//				String idType
//				it.getIdList().each {
//					if (it.name == "PubChem CID") {
//						if (!(it.value ==~ '\\d{1,9}')) {
//							skip = true
//						}
//						id = it.value
//						idType = "CID"
//					} else if (it.name == "Pubchem SID") {
//						if (!(it.value ==~ '\\d{1,9}')) {
//							skip = true
//						}
//						id = it.value
//						idType = "SID"
//					}
//				}
//
//				if (!skip) {
//					if (it.inchiKey.length() == 27) {
//						if (!checkDatabase(it)) {
//							sql.execute(it.getQuery())
//						} else {
//							updateEntry(it)
//						}
//					} else {
//						log.error("invalid inchi key for $idType $id: ${it.inchiKey}")
//					}
//				} else {
//					log.error("invalid $idType:  $id")
//				}
//			}
//
//			successfulUpdates++
//		}
//		catch (Exception e) {
//			log.error("$name in updatefile()", e)
//			if (e.class == FTPConnectionClosedException) {
//				throw e
//			}
//
//		}
//
//		finally {
//			config.setProperty("last.update", (String) "${path.substring(path.length() - 15, path.length() - 5)}: ${name}: directory incomplete")
//			config.save()
//			fos.close()
//			file.delete()
//			list.clear()
//		}
//	}
//
//	private boolean checkDatabase(AbstractResourceImpl entry) {
//		//returns true if the entry exists in the database
//		try {
//			def r = false
//			sql.eachRow("select * from compound where id = ?", [entry.getInchiKey()]) {
//				r = true
//			}
//			return r
//		} catch (Exception e) {
//			log.error("checkDatabase", e)
//			return false
//		}
//	}
//
//	private void updateEntry(AbstractResourceImpl entry) {
//		def params = []
//
//		try {
//			//updating compound table
//			params = [entry.inchiKey, entry.inchiCode, entry.molWeight, entry.exactMass, entry.getFormula(), entry.getSourceFile()]
//			sql.execute("select update_compound(?,?,?,?,?,?)", params)
//
//			//updating synonyms table
//			entry.getNames().each {
//
//				def args = [entry.inchiKey, it.getValue().value[0], it.key, entry.getSourceFile()]
//
//				def name
//
//				try {
//					name = sql.firstRow("select name from synonym where type = ? and inchi_key = ?;", [it.getValue().type[0], entry.inchiKey]).name
//
//				} catch (NullPointerException) {
//					name = null
//				}
//
//				if (name == null) {
//					sql.execute((String) "select insert_synonym(?, ?, ?, ?);", args)
//				} else if (name != it.getValue().value[0]) {
//					sql.execute((String) "select update_synonym(?, ?, ?, ?);", args)
//				}
//			}
//
//			//updating external_id table
//			entry.getIdList().each {
//
//
//				boolean found = false
//
//				sql.eachRow("select value from external_id where inchi_key = ? and name = ? and value = ?;", [entry.inchiKey, it.name, it.value])
//						{ found = true }
//
//
//				if (!found) {
//					def args = [entry.inchiKey, it.name, it.url, it.value, entry.getSourceFile()]
//					sql.execute((String) "select insert_external_id(?, ?, ?, ?, ?);", args)
//				}
//			}
//		}
//		catch (Exception e) {
//			log.error("${entry.getSourceFile()} in updateEntry", e)
//		}
//	}

}
