package edu.ucdavis.fiehnlab.updater.io.ftp

import edu.ucdavis.fiehnlab.updater.IFileHandler
import edu.ucdavis.fiehnlab.updater.utils.Interval
import org.apache.commons.lang.NotImplementedException
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPReply
import org.apache.log4j.Logger

/**
 * Created by diego on 3/3/2015.
 */
abstract class FTPFileHandler implements IFileHandler {
	private static Logger log = Logger.getLogger(FTPFileHandler.class.name)
	protected String username
	protected String password
	protected String hostname
	protected String downloadDest
	protected FTPClient ftpclient
	protected Date since


	FTPFileHandler() {
		this.hostname = "ftp.ncbi.nih.gov"
		this.username = "anonymous"
		this.password = "anon@mail.com"
		this.downloadDest = "downloads"
		this.ftpclient = new FTPClient()
		this.ftpclient.setConnectTimeout(5000)
		this.ftpclient.setControlKeepAliveTimeout(300)
		this.ftpclient.setDataTimeout(5000)
		this.ftpclient.setBufferSize(33554432)
		this.since = new Date().clearTime()
	}

	@Override
	List<String> getFiles(String baseDir) {
		throw new NotImplementedException()
	}

	@Override
	List<String> getFilesSince(String BaseDir, Date since) {
		throw new NotImplementedException()
	}

	protected final boolean openConnection() {
		try {
			if(log.debugEnabled) {
				log.debug("Connecting to $hostname")
			}
			ftpclient.connect(hostname)
			ftpclient.login(username, password)
			int reply = ftpclient.getReplyCode();
			//FTPReply stores a set of constants for FTP reply codes.
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftpclient.disconnect();
				return false;
			}
			ftpclient.enterLocalPassiveMode()
		} catch (SocketException e) {
			log.error("Unable to create connection to $hostname", e)
			return false
		} catch (IOException e) {
			log.error("Error communicating with $hostname\n${ftpclient.getReplyStrings().collect {"\t" + it + "\n"}}", e)
			return false
		}
		return true
	}

	protected final boolean closeConnection() {
		if(ftpclient.isConnected()) {
			ftpclient.logout()
			ftpclient.disconnect()
		}
		if(log.debugEnabled) {
			log.debug "Connection to $hostname closed."
		}
		return true
	}

	public Map<String, List<String>> download(List<String> files) {
		return this.doDownload(files)
	}

	protected abstract Map<String, List<String>> doDownload(List<String> files)

	public abstract List<String> getAvailableFolders(Interval interval, Date lastUpdateDate)

	public abstract List<String> listFilesThreaded(List<String> dirs)

	public static final String fixDirectory(String s) {
		if (s.startsWith("/")) {
			s = s[1..-1]
		}
		if (s.endsWith("/")) {
			s = s[0..-2]
		}

		if (!s.startsWith("/")) {
			if (s.startsWith("pubchem")) {
				s = "/".concat(s)
			} else {
				s = "/pubchem/".concat(s)
			}
		}

		return s
	}
}
