package edu.ucdavis.fiehnlab.updater

/**
 * Created by diego on 3/3/2015.
 */
public interface IFileHandler {
	List<String> getFiles(String BaseDir)
	List<String> getFilesSince(String BaseDir, Date from)
}
