package edu.ucdavis.fiehnlab.updater.io.ftp.filters

import org.apache.commons.net.ftp.FTPFile

/**
 * Created by diego on 3/16/15.
 */
class FTPFileExtensionFilter extends AbstractFTPFileFilter {

	@Override
	boolean accept(FTPFile ftpFile) {
		return (ftpFile.isFile() && ftpFile.getName().endsWith(".sdf.gz"))
	}
}
