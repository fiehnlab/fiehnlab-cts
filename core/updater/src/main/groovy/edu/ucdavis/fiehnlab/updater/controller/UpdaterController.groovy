package edu.ucdavis.fiehnlab.updater.controller

import edu.ucdavis.fiehnlab.updater.impl.FileTranslator
import edu.ucdavis.fiehnlab.updater.io.ftp.FTPFileHandler
import edu.ucdavis.fiehnlab.updater.io.ftp.PubChemFTPFileHandler
import edu.ucdavis.fiehnlab.updater.utils.Interval
import org.apache.log4j.Logger

/**
 * Created by diego on 3/3/2015.
 */
class UpdaterController {
	private static Logger log = Logger.getLogger(UpdaterController.class.name)
	private ConfigObject config
	private FTPFileHandler pcFileHdlr
	private FileTranslator translator

	public UpdaterController(ConfigObject config) {
		this.config = config
		translator = new FileTranslator(config)
	}

	public List<String> update() throws IOException {
		// get list of (downloaded) files to update
		def files = getFileListToUpdate()

		// convert SDFs to SQL
		List<String> toUpdate = []
		def start = ~/.*(?:Compound|Substance).*/
		toUpdate = files.good.findAll { it =~ start}

		def converted = translator.translate(toUpdate)

		// deleting source of converted files
		converted.each {
			def f = new File(it.replace("sql", "sdf.gz"))
			if(f.exists()) {
				f.delete()
			}
		}

		return converted
	}

	public Map<String, List<String>> getFileListToUpdate() {

		Date lastUpdateDate = new Date().minus(30)

		// check last update date is in format yyyy-MM-dd
		try {
			lastUpdateDate = new Date(config.lastUpdate.date.toString().replaceAll("-", "/"))//checkLastUpdateDate()
			log.debug("last update: $lastUpdateDate")
		} catch (Exception e) {
			log.error("Invalid date '${config.lastUpdate.date}'. The correct format should be yyyy-MM-dd.")
			return [good: [], failed: []]
		}

		// get list of files to process
		List<String> files = []
		pcFileHdlr = new PubChemFTPFileHandler(config)

		files = getFilesToUpdate(pcFileHdlr, lastUpdateDate)

		log.info "\tFiles to update: ${files.size()}\n"

		// starting download
		Map<String, List<String>> downloaded = pcFileHdlr.download(files)

		return downloaded

	}


	private List<String> getFilesToUpdate(FTPFileHandler pcFileHdlr, Date lastUpdateDate) {
		List<String> files
		List<String> dirs = []

		// build directory in which to search files
		if(log.debugEnabled) {
			log.debug "getting ${config.interval} folders since ${lastUpdateDate.format("yyyy/MM/dd")}"
		}

		List<String> dateStr = pcFileHdlr.getAvailableFolders((Interval) Interval[config.interval.toUpperCase()] ?: Interval.WEEKLY, lastUpdateDate)

		dateStr.each {
			dirs.add("$it")
		}

		// should get files in folders newer than lastUpdateDate
		files = pcFileHdlr.listFilesThreaded(dirs)

		return files
	}

	protected final Date checkLastUpdateDate() {
		Date lastUpdateDate = new Date().clearTime()

		try {
			if(log.debugEnabled) {
				log.debug "Last update: '${config.lastUpdate.date}'"
			}

			if (config.lastUpdate.date.matches("\\d{4}[/-]\\d{2}[/-]\\d{2}")) {
				String ud = config.lastUpdate.date.replaceAll("-", "/")
				lastUpdateDate = new Date(ud)
			} else {
				throw new IllegalArgumentException("bad date format")
			}
		} catch (IllegalArgumentException e) {
			log.warn("Invalid date of last update in config file: '${config.lastUpdate.date}'.\nSet last update date to ${lastUpdateDate.format("yyyy/MM/dd")}")
			lastUpdateDate = new Date().minus(365)
		}

		return lastUpdateDate
	}

	private void persistConfig(ConfigObject conf, File file) {
		// re-writing config (refactor into other object)
		file.withWriter { writer ->
			conf.toProperties().store(writer, "Updater properties")
		}
		log.info("Updated configuratin file (${file})")
	}
}
