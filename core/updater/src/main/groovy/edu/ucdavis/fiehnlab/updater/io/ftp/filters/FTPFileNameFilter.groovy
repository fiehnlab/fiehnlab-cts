package edu.ucdavis.fiehnlab.updater.io.ftp.filters

import org.apache.commons.net.ftp.FTPFile

/**
 * Created by diego on 4/9/2015.
 */
class FTPFileNameFilter extends AbstractFTPFileFilter {

	@Override
	boolean accept(FTPFile ftpFile) {
		return (ftpFile.isFile() && (ftpFile.getName().matches("(?:killed|updated)-[CS]IDs") || ftpFile.getName().matches("(?:Compound|Substance)(?:.*)")))
	}
}
