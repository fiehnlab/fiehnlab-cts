package edu.ucdavis.fiehnlab.updater.io.ftp

import edu.ucdavis.fiehnlab.updater.io.ftp.filters.FTPFileDateFilter
import edu.ucdavis.fiehnlab.updater.io.ftp.filters.FTPFileNameFilter
import edu.ucdavis.fiehnlab.updater.utils.Interval
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPFile
import org.apache.commons.net.ftp.FTPFileFilter
import org.apache.commons.net.ftp.FTPReply
import org.apache.log4j.Logger
import sun.net.ftp.FtpLoginException

import static groovyx.gpars.GParsPool.withPool

/**
 * Created by diego on 3/3/2015.
 *
 * pubchem ftp folders
 * host/pubchem/<item>/<interval>/<date>/<type>/<File>
 *     item: Compound || Substance
 *     interval: CURRENT-Full || Daily || Weekly || Monthly || Extras
 *     date: directories named like 'yyyy-MM-dd'
 *     type: ASN || SDF || XML || killed-[CS]IDs || updated-[CS]IDs
 *     file: (Compound || Substance)_########1_########0.(asn || sdf || xml).gz
 *
 */
class PubChemFTPFileHandler extends FTPFileHandler {
	private static Logger log = Logger.getLogger(PubChemFTPFileHandler.class.name)
	private Interval interval
	private static int count = 0
	private List<String> basePaths = ["/pubchem/Compound", "/pubchem/Substance"]
	private ConfigObject config = new ConfigObject()

	public PubChemFTPFileHandler(ConfigObject conf) {
		super()
		this.config = conf
		this.username = conf.ftp.name
		this.password = conf.ftp.password
		this.interval = conf.interval.toUpperCase()
		this.downloadDest = conf.ftp.downloadDest

		def d = conf.lastUpdate.date.trim()
		if(d) {
			this.since = new Date(d.replaceAll("-","/").toString()).clearTime()
		} else {
			log.warn("The last update 'date' was unset. Setting deafult to 30 days back.")
			this.since = new Date().clearTime().minus(30)
		}

		this.interval = ((conf.interval != ""?Interval.valueOf(conf.interval.toUpperCase()):Interval.WEEKLY))
		if (log.debugEnabled) {
			log.debug "Downloads: ${new File(downloadDest).absoluteFile}"
			log.debug "Date: $since"
		}
	}

	/**
	 * generates a list of files found in the 'dirs' remote folders, with an optional date for filtering.
	 * @param dirs folders to search for files
	 * @param since date to filter files to update (older files are ignored)
	 * @return
	 */
//	@Override
	public List<String> listFiles(List<String> dirs) {
		List<String> files = []

		if (ftpIsOpen) {
			dirs.each {dir ->
				//get killed/updated-ids
				files.addAll(fetchFiles(dir, new FTPFileNameFilter()))

				// get SDF files
				dir = dir.concat("/SDF")
				files.addAll(fetchFiles(dir, new FTPFileDateFilter(since)))
			}
		} else {
			log.error("Can't get files from $hostname")
		}

		return files
	}

	/**
	 * generates a list of files found in the 'dirs' remote folders, with an optional date for filtering. Threaded version
	 * @param dirs folders to search for files
	 * @param since date to filter files to update (older files are ignored)
	 * @return
	 */
	@Override
	public List<String> listFilesThreaded(List<String> dirs) {
		List<String> files = Collections.synchronizedList(new ArrayList<String>())

		withPool 5, {
			dirs.each/*Parallel*/ {dir ->
				try {
					//get killed/updated-ids
					files.addAll(fetchFiles(dir, new FTPFileNameFilter()))

					//get SDF files
					files.addAll(fetchFiles(dir.concat("/SDF"), new FTPFileDateFilter(since)))
				} catch (Exception e) {
					log.error(e.message)
				}
			}
		}

		log.debug("returning ${files.size()} files")
		return files
	}

	/**
	 * returns a list of files in the 'dir' folder
	 * @param dir folder to get file info
	 * @param filter limits what files are returned
	 * @return List of file names (with folder information)
	 */
	private List<String> fetchFiles(String dir, FTPFileFilter filter = null) {
		FTPClient ftp = new FTPClient()
		def tmp = []
		int count = 0

		try {
			ftp.connect(hostname)
			ftp.login(this.username, "anonymous${count++}@gmail.com")
			int reply = ftp.getReplyCode();

			//FTPReply stores a set of constants for FTP reply codes.
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return [];
			}
			ftp.enterLocalPassiveMode()

			if (ftp.changeWorkingDirectory(dir)) {
				def files = ftp.listFiles(dir, filter)
				tmp.addAll(files.collect {"$dir/$it.name"})
				log.debug "${tmp.size()} files in $dir"
			} else {
				log.error("Invalid directory: $dir")
			}
		} catch (Exception e) {
			log.warn("Can't get files from $dir.", e)
		} finally {
			try {
				if (ftp.connected) {
					ftp.logout()
					ftp.disconnect()
					log.trace("disconnected $dir")
				}
			} catch (Exception e) {
				log.error(e.message)
			}
			ftp = null
		}
		return tmp
	}

	/**
	 * get available pubchem folders for the specified interval to be updated
	 * @param since get only folders newer than this date
	 * @return a List<String> of folders
	 */
	public List<String> getAvailableFolders(Interval interval, Date psince = null) {
		List<String> subs = []

		if (psince != null) {
			// if a different since date was passed, adjust the config object
			since = psince
		}

		basePaths = basePaths.collect {"$it/$interval"}

		boolean ftpIsOpen = openConnection()
		if (ftpIsOpen) {
			basePaths.each {dir ->
				if (ftpclient.changeWorkingDirectory(dir)) {
					List<FTPFile> dirs = ftpclient.listDirectories()
					dirs.each {FTPFile subdir ->
						if (subdir.timestamp.time >= since) {
							subs.add("$dir/${subdir.name}")
							if (log.debugEnabled) {
								log.debug "need to update folder: ${dir}/${subdir.name}"
							}
						}
					}
				} else {
					log.error("Invalid directory: $dir")
				}
			}
		} else {
			log.error("Can't get files from $hostname")
		}

		closeConnection()

		return subs
	}

	/**
	 * copies the files from the pubchem server to the destination folder in parallel mode
	 * @param files list of files to be downloaded
	 * @return a Map<List<String>, List<String>> with the lists of good (downloaded), and fails (not downloaded) files
	 */
	protected final Map<String, List<String>> doDownload(List<String> files) {
		List<String> bad = Collections.synchronizedList([])
		List<String> good = Collections.synchronizedList([])

		//make sure downloadDest exists
		def dir = new File("${this.downloadDest}")  //root of running module
		if (!dir.exists()) {
			dir.mkdirs()
		}

		log.info("\nDownloding ${files.size()} files to: ${dir.absolutePath}\n")

		withPool 5, {
			files.eachParallel {String file ->
				def dest = ""
				// skip nulls or empties (just in case)
				if (file != null && !file.isEmpty()) {
					try {
						int idx = file.lastIndexOf("/") - 1
						String fileName = file[(idx + 2)..-1]
						String date = file[(file.lastIndexOf("/", idx) + 1)..idx]
						dest = "$downloadDest/$fileName"

						// adjust name for general files "killed or updated -[CS]IDs
						if (fileName.matches("(?:killed|updated)-[CS]IDs")) {
							dest = "$downloadDest/${date.concat(fileName)}"
						}

						// try to download file
						if (new File(dest).exists()) {
							print("/")
							good.add(new File(dest).absolutePath)
						} else {
							getFile(file, dest, good, bad)
						}
					} catch (Exception e) {
						log.error("Can't download the file $file to $dest.\n${e.message}", e)
					}
				} else {
					bad.add("Invalid file name. ${file == null ? "null" : "'' (empty)"}")
				}
				dest = ""
			}
		}
		println("")

//		log.debug "Downloaded: $good"
//		log.debug "Failed: $bad"

		return [good: good, failed: bad]
	}

	/**
	 * Does the actual download job
	 */
	Closure getFile = {String file, String destFile, List<String> good, List<String> bad ->
		log.debug "Downloading file: $file"
		int count = 0;
		new FTPClient().with {
			setAutodetectUTF8(true)
			connect(hostname)
			enterLocalPassiveMode()
			login(this.username, "anonymous${count++}@mail.com")

			int reply = getReplyCode();

			//FTPReply stores a set of constants for FTP reply codes.
			if (!FTPReply.isPositiveCompletion(reply)) {
				disconnect();
				throw new FtpLoginException("Can't login")
			}
			setFileType(BINARY_FILE_TYPE);

			File dd = new File(downloadDest)
			if (!dd.exists()) {
				dd.mkdir()
			}

			def inFile = new File(destFile)
			inFile.withOutputStream {fos ->
				// downloaded
				if (retrieveFile(file, fos)) {
					log.debug("downloaded: ${inFile.name}")
					good.add(inFile.absolutePath)
				}
				// not downloaded
				else {
					log.debug("couldn't download: ${inFile.name}")
					bad.add(inFile.name)
					inFile.deleteOnExit()
				}
			}
			if (connected) {
				logout()
				disconnect()
			}
			print "*"
		}
	}
}
