package edu.ucdavis.fiehnlab.updater

/**
 * Created by diego on 9/23/15.
 */
public interface Config {
	String getFile()

	Object get(String key)
	void put(Object key, value)
	void persist() throws IOException
}
