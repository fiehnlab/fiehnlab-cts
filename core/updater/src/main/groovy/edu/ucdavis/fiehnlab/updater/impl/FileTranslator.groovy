package edu.ucdavis.fiehnlab.updater.impl

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.PubchemCompoundFile
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.PubchemSubstanceFile
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource
import edu.ucdavis.fiehnlab.updater.Translator
import org.apache.commons.configuration.Configuration
import org.apache.commons.configuration.MapConfiguration
import org.apache.log4j.Logger

import java.util.zip.GZIPInputStream

import static groovyx.gpars.GParsPool.withPool

/**
 * Created by diego on 8/07/15.
 *
 * Converts SDF files into SQL scripts
 */


class FileTranslator implements Translator {
	Logger logger = Logger.getLogger(getClass());
	private ConfigObject config
	private static Logger log = Logger.getLogger(FileTranslator.class.name)
	private static final int TRANSACTION_BLOCK_SIZE = 100

	public FileTranslator(ConfigObject config) {
		this.config = config
	}


	public List<String> translate(List<String> files) throws FileNotFoundException, IOException {
		def results = Collections.synchronizedList(new ArrayList<String>())

		withPool 5, {
			files.eachParallel { String file ->
				File f = new File(file)
				try {
					doTranslation(f, results)
				} catch (FileNotFoundException e) {
					log.error "Can't find file ${f.absoluteFile}"
				}
			}
		}

		return results
	}

	/**
	 * This closure calls the DataExtractor parser classes and saves the results
	 */
	Closure doTranslation = { File file, def results ->
		synchronized (results) {
			results.add(translate(file))
		}
	}


	public String translate(File file) throws IllegalArgumentException, FileNotFoundException, IOException {
		CompressedResource pcxml = null
		GZIPInputStream gzfile = null
		Resource pcProcessor = null

		// changing from groovy's ConfigObject (it's a Map) to apache commons' Configuration object
		Configuration cfg = new MapConfiguration(config)

		CollectionResource<? extends Resource> res = null;

		def filename = file.path[file.path.lastIndexOf("\\")+1..-1]

		if (file.path.contains("Substance")) {
			pcProcessor = new PubchemSubstanceFile(filename, cfg)
		} else if (file.path.contains("Compound")) {
			pcProcessor = new PubchemCompoundFile(filename)
		} else {
			throw new IllegalArgumentException("File ${file.path} is not a Compound or Substance file.")
		}

		pcxml = new CompressedResource(new GZIPInputStream(file.newInputStream()))
		res = (CollectionResource<? extends Resource>) pcProcessor.process(pcxml)

		def sqlFileName = file.absolutePath[0..file.absolutePath.indexOf(".sdf.gz")].concat("sql")
		def sqlFile = new File(sqlFileName)

		sqlFile.withWriter { sout ->
			sout.write("BEGIN;\n")
			res.eachWithIndex { Resource compound, int i ->
				sout.write(compound.query)
				if((i + 1)%TRANSACTION_BLOCK_SIZE == 0) {
					sout.write("COMMIT;\n\nBEGIN;\n")
				}
			}
			sout.write("COMMIT;\n")
			sout.flush()
		}

		return sqlFile.absolutePath
	}
}
