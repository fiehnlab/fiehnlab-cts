package edu.ucdavis.fiehnlab.updater.utils

/**
 * Created by diego on 3/3/2015.
 */
public enum Interval {
	DAILY(1), WEEKLY(7), MONTHLY(30)

	private final int value

	Interval(int days) { this.value = days }

	public int value() { return value }

	public String toString() {
		def tmp = name()[0].toUpperCase() + name()[1..-1].toLowerCase()
		return tmp
	}
}
