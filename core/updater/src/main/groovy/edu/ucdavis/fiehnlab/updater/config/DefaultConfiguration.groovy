package edu.ucdavis.fiehnlab.updater.config

import edu.ucdavis.fiehnlab.updater.Config
import org.apache.log4j.Logger

/**
 * Created by diego on 9/23/15.
 */
public class DefaultConfiguration implements Config {
	private static Logger logger = Logger.getLogger(DefaultConfiguration.class)
	private ConfigObject cfg
	private File file

	public DefaultConfiguration(String fileName) {
		try {
			file = new File(getClass().getResource("/$fileName").file)
			if (file.exists()) {
				logger.debug "Reading config... ${file.getName()}"

				Properties p = new Properties()
				file.withInputStream { stream ->
					p.load(stream)
				}
				cfg = new ConfigSlurper().parse(p)
			} else {
				logger.debug "Creating new config..."
				cfg = new ConfigObject()
			}
		} catch (Exception e) {
			logger.error("Error reading config file. A new one will be created.\nMessage: ${e.message}")
			file = new File(getClass().getResource("/.").file + fileName)
			cfg = new ConfigObject()
		}
	}

	@Override
	def String getFile() {
		return this.file.getAbsolutePath()
	}

	protected ConfigObject getConfig() {
		return cfg
	}

	@Override
	def get(String key) {
		def value = cfg[key]

		if(value instanceof String) {
			value = value.replaceAll("'", "")
		}
		return value
	}

	@Override
	void put(Object key, value) {
		cfg[key] = value
	}

	@Override
	public void persist() throws IOException {
		file.withWriter { writer ->
			cfg.writeTo(writer)
		}
	}
}
