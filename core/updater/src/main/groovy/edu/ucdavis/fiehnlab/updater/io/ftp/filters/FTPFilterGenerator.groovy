package edu.ucdavis.fiehnlab.updater.io.ftp.filters

import org.apache.log4j.Logger

/**
 * Created by diego on 3/18/15.
 */
public class FTPFilterGenerator {
	private static Logger log = Logger.getLogger("FTPFilterGenerator")

	public static AbstractFTPFileFilter getFilter(Date date) {
		try {
			def today = new Date().clearTime()
			if(date.clearTime() < today) {
				return new FTPFileDateFilter(date)
			} else {
				return new FTPFileExtensionFilter()
			}
		} catch (IllegalArgumentException e) {
			return new FTPFileExtensionFilter()
		}
	}

}
