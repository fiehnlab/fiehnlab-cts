package edu.ucdavis.fiehnlab.updater.controller

import org.apache.log4j.Logger
import org.junit.After
import org.junit.Before
import org.junit.Ignore

/**
 * Created by diego on 3/16/15.
 */
@Ignore
class UpdaterControllerTest extends GroovyTestCase {
	private static Logger log = Logger.getLogger(UpdaterControllerTest.class.name)
	UpdaterController controller

	@Before
	void setUp() {
		Properties props = new Properties()
		props.load(new FileReader(getClass().getResource("/updaterTest.properties").file))

		ConfigObject conf = new ConfigSlurper().parse(props)
		log.debug "Config: ${conf}"
		controller = new UpdaterController(conf)

	}

	@After
	void tearDown() {
		controller = null
	}

	void testGetFileListToUpdate() {
		Map<String, List<String>> res = controller.getFileListToUpdate()

		if (res.good.size() > 2) {
			res.good = res.good[0..1]
		}

		assert res.good.size() > 0
		assert res.good.size() <= 2
	}

	//@Ignore doesn't seem to work...
//	@Ignore
//	void testUpdate() {
//		def res = controller.update()
//
//		log.debug "testUpdate RESULT: $res"
//
//		assert res.size() > 0;
//	}
}
