package edu.ucdavis.fiehnlab.updater

import edu.ucdavis.fiehnlab.updater.impl.FileTranslator
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
/**
 * Created by nilmargis on 5/30/14.
 */
@Ignore
class FileTranslatorTest extends GroovyTestCase {
	private static Logger log = Logger.getLogger(FileTranslatorTest.class.name)
	private static final String TEST_COMP = "src/test/resources/CompoundTest.sdf.gz"
	private static final String TEST_SUBST = "src/test/resources/SubstanceTest.sdf.gz"
	static final String CONFIG_FILE = "updaterTest.properties"

	FileTranslator fileTranslator
	ConfigObject config

	@Before
	void setUp() {
		Properties props = new Properties()
		props.load(FileTranslatorTest.getResource("/$CONFIG_FILE").openStream())
		config = new ConfigSlurper().parse(props)

		fileTranslator = new FileTranslator(config)
	}

	@Test
	void testTranslateCompoundFile() {
		log.debug "Path: ${new File("").absolutePath}"
		File comp = new File(TEST_COMP)
		String resC = fileTranslator.translate(comp)

		assertEquals "CompoundTest.sql", resC[resC.lastIndexOf("\\")+1..-1]
	}

	@Test
	void testTranslateSubstanceFile() {
		File subs = new File("src/test/resources/SubstanceTest2.sdf.gz")
		String resS = fileTranslator.translate(subs)

		assertEquals "SubstanceTest2.sql", resS[resS.lastIndexOf("\\")+1..-1]
	}

	@Test
	void testTranslateList() {
		List<String> files = [TEST_COMP,TEST_SUBST]
		List<String> res = fileTranslator.translate(files)
		def exp = ["CompoundTest.sql", "SubstanceTest.sql"]

		assert res != null
		assertEquals exp.size(), res.size()
		assertEquals exp.sort(), res.sort().collect { it[it.lastIndexOf("\\")+1..-1]}
	}

	@Test
	void testSubstanceWithNullDepositor() {

	}
}
