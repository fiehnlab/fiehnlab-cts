package edu.ucdavis.fiehnlab.updater

import org.apache.log4j.Logger
import org.junit.Ignore
import org.junit.Test

/**
 * Created by diego on 3/3/15.
 */
@Ignore
class UpdaterTest extends GroovyTestCase {
	private static Logger log = Logger.getLogger(UpdaterTest.class.name);
	static final String CONFIG_FILE = "updaterTest.properties"

	@Test
	void testGoodLaunch() {
		def app = new Updater(CONFIG_FILE)

		assert app.config != null
		assert app.config.pubchemIds.containsKey("skip")

		assertEquals "141382403,3,53242", app.config.pubchemIds.skip

		log.info "Updater is configured"
	}

	@Test
	@Ignore("not ready yet")
	void testRunNoArgs() {
		def u = new Updater(CONFIG_FILE)

		int exit = -1
		try {
			exit = u.run()
		} catch (Exception e) {
			log.error e.message, e
		}

		assertEquals 0, exit
	}
}

