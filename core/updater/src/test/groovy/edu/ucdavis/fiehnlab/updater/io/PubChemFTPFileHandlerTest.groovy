package edu.ucdavis.fiehnlab.updater.io

import edu.ucdavis.fiehnlab.updater.io.ftp.FTPFileHandler
import edu.ucdavis.fiehnlab.updater.io.ftp.PubChemFTPFileHandler
import edu.ucdavis.fiehnlab.updater.utils.Interval
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Ignore
import org.junit.Test

/**
 * Created by diego on 3/16/15.
 */
@Ignore
class PubChemFTPFileHandlerTest extends GroovyTestCase {
	private static Logger log = Logger.getLogger(PubChemFTPFileHandlerTest.class.name)
	PubChemFTPFileHandler handler
	ConfigObject config

	@Before
	void setUp() {
		Properties props = new Properties()
		props.load(getClass().getResourceAsStream("/updaterTest.properties"))
		log.info "props: $props"
		config = new ConfigSlurper().parse(props)
		handler = new PubChemFTPFileHandler(config)
	}

//	@Test
//	void testListFilesNonThreaded() {
//		List<String> folders = []
//		folders.addAll(handler.getAvailableFolders())
//		log.debug("\tinterval: ${config.interval}")
//		log.debug("\tfound folders: ${folders.collect { "\n\t\t$it" }}\n")
//
//		def start = System.nanoTime()
//		List<String> files = handler.listFiles(folders)
//		log.info("Time: ${(System.nanoTime() - start)/1000000}ms")
//
//		log.debug("${files.size()} files")
//		log.trace("${files.collect {"$it\n"}}")
//
//		assert files != null
//		assert !files.empty
//	}

//	@Test
//	void testListFilesNonThreadedFromDate() {
//		Date since = new Date() - 7 //since last week
//
//		List<String> folders = []
//		folders.addAll(handler.getAvailableFolders(since))
//		log.debug("\tinterval: ${config.interval}")
//		log.debug("\tsince: ${since.format("yyyy-MM-dd")}")
//		log.debug("\tfound folders: ${folders.collect { "\n\t\t$it" }}\n")
//
//		def start = System.nanoTime()
//		List<String> files = handler.listFiles(folders)
//		log.info("Time: ${(System.nanoTime() - start)/1000000}ms")
//
//		log.debug("${files.size()} files")
//		log.trace("${files.collect {"$it\n"}}")
//
//		assert files != null
//		assert !files.empty
//
//		assert files.collectAll { it.contains("killed-CIDs") || it.contains("updated-SIDs") }
//	}

	@Test
	void testListFilesThreaded() {
		List<String> folders = []
		log.debug("\tinterval: ${config.interval}")
		folders.addAll(handler.getAvailableFolders(Interval.DAILY))
		log.debug("\tfound folders: ${folders.collect { "\n\t\t$it" }}\n")

		def start = System.nanoTime()
		List<String> files = handler.listFilesThreaded(folders)
		log.info("Time: ${(System.nanoTime() - start)/1000000}ms")

		log.info("${files.size()} files")
		log.debug("${files.collect {"$it\n"}}")

		assert files != null
		assert !files.empty
	}

	@Test
	void testListFilesThreadedFromDate() {
		config.lastUpdate.date = (new Date() - 1).format( 'yyyy-MM-dd' )
		handler = new PubChemFTPFileHandler(config)

		List<String> folders = []
		folders.addAll(handler.getAvailableFolders(Interval.DAILY))
		log.debug("\tinterval: ${config.interval}")
		log.debug("\tsince: ${config.lastUpdate.date}")
		log.debug("\tfound folders: ${folders.collect { "\n\t\t$it" }}\n")

		def start = System.nanoTime()
		List<String> files = handler.listFilesThreaded(folders)
		log.info("Time: ${(System.nanoTime() - start)/1000000}ms")

		log.info("${files.size()} files")
		log.debug("${files.collect { "$it\n" }}")

		assert files != null
		assert !files.empty
	}

	@Test
	void testGetAvailableFolders() {
		def subs = []

		subs = handler.getAvailableFolders(Interval.DAILY)
		log.debug("Daily folders: ${subs.collect {"$it\n"}}")

		assert subs.size() >= 1
	}

	@Test
	void testDownload() {
		List<String> files = ["/pubchem/Compound/CURRENT-Full/SDF/Compound_044675001_044700000.sdf.gz",
		                      "/pubchem/Substance/CURRENT-Full/SDF/Substance_000700001_000725000.sdf.gz",
		                      "bad file", "", null]

		def res = handler.download(files)

		log.debug("Good: ${res.good}")
		log.debug("Fails: ${res.failed}")

		assert !res.good.empty
		assert !res.failed.empty

		assert res.failed.sort() == ["Invalid file name. null", "Invalid file name. '' (empty)"].sort()
	}

	@Test
	void testFixDierectory() {
		assertEquals("/pubchem/Compound", FTPFileHandler.fixDirectory("Compound"))
		assertEquals("/pubchem/Compound", FTPFileHandler.fixDirectory("/Compound"))
		assertEquals("/pubchem/Compound", FTPFileHandler.fixDirectory("Compound/"))
		assertEquals("/pubchem/Compound", FTPFileHandler.fixDirectory("pubchem/Compound"))
		assertEquals("/pubchem/Compound", FTPFileHandler.fixDirectory("/pubchem/Compound"))
	}
}
