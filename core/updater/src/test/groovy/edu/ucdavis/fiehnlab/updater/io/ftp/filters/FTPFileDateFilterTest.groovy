package edu.ucdavis.fiehnlab.updater.io.ftp.filters

import org.apache.commons.net.ftp.FTPFile
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Ignore
import org.junit.Test

/**
 * Created by diego on 1/21/2016.
 */
@Ignore
class FTPFileDateFilterTest extends GroovyTestCase {
	private static Logger log = Logger.getLogger(FTPFileDateFilterTest.class)
	FTPFile ftpfile

	@Before
	void setUp() {
		ftpfile = new FTPFile()
		ftpfile.name = "testFile.sdf.gz"
		ftpfile.timestamp = new Date().toCalendar()
		ftpfile.type = FTPFile.FILE_TYPE
		ftpfile.user = "homer"
		ftpfile.group = "Simpson"
	}

	@Test
	void testAcceptTodayFile() {
		Date now = new Date()
		FTPFileDateFilter filter = new FTPFileDateFilter(now)


		log.debug "\nnow: $now\ndate: ${ftpfile.timestamp.time.toString()}"

		assertTrue("should accept a file from today", filter.accept(ftpfile))
	}

	@Test
	void testDontAcceptNonSdfGz() {
		FTPFileDateFilter filter = new FTPFileDateFilter(new Date())

		ftpfile.name = "testFile"

		assertFalse("reject non '.sdf.gz' files", filter.accept(ftpfile))
	}

	@Test
	void testNotAcceptOlderFile() {
		FTPFileDateFilter filter = new FTPFileDateFilter(new Date())

		FTPFile goodftpfile = new FTPFile()
		goodftpfile.name = "testFile.sdf.gz"
		goodftpfile.timestamp = new Date().minus(1).toCalendar()

		assertFalse("should not accept a file from yesterday", filter.accept(goodftpfile))
	}
}
