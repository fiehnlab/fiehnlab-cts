@GrabResolver(name = 'jboss', root = 'http://repository.jboss.org/maven2/')
@Grapes([
		@Grab(group = 'commons-logging', module = 'commons-logging', version = '1.1.1'),
		@Grab(group = 'org.codehaus.groovy.modules.http-builder', module = 'http-builder', version = '0.7.2'),
		@Grab(group = 'org.codehaus.gpars', module = 'gpars', version = '1.2.1'),
])

import groovyx.gpars.GParsPool
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder

class Converter {
	private String host = "http://cts.fiehnlab.ucdavis.edu/"

	public static void main(String... args) {

		Converter app = new Converter()

		if (args.length < 1) {
			println """I need a text file with the list of keywords to convert and the IDs to convert from/to.
\tThe first line should be the ID that are you converting from.
\tThe second line should be a comma separated list of IDs to convert to.
\tThe rest should be the list of keywords (one per line) to convert.
\n\tThe results are saved to a "result.csv" file.\n
For a list of supported IDs, please check http://cts.fiehnlab.ucdavis.edu/service/conversion/fromValues and http://cts.fiehnlab.ucdavis.edu/service/conversion/toValues
"""
			System.exit(-1)
		} else {
			File input = new File(args[0])

			if (input.exists()) {
				def start = System.nanoTime()
				List<String> keyList = []
				String from = ""
				List to = []

				input.withReader { reader ->
					from = reader.readLine()
					to = reader.readLine().split(",").collect { it.trim() }

					reader.eachLine { line ->
						if (!line.trim().empty) {
							keyList.add(line.trim())
						}
					}
				}

				def data = app.process(from, to, keyList, input)
				println "Conversion took: ${(System.nanoTime() - start)/1000000000} seconds."
			} else {
				println "File ${input.absoluteFile} doesn't exists."
				System.exit(-1)
			}
		}
	}


	def process(String from, List<String> to, List<String> input, File inFile) {
		def fails = checkTos(to)
		if (fails.size() > 0) {
			println "The following destination ID name/s is/are invalid:\n\t${fails.join(", ")}"
			System.exit(-1)
		}

		int keywordCount = 0
		int repeated = 0
		def result = new HashMap<String, Map>()

		input.each() { String line ->

			def resLine = new HashMap<String, String>()
			if (line != null && !line.empty) {
				println "Converting '$line'"

				keywordCount++
				GParsPool.withPool(5) {
					try {
						to.eachParallel {

							List<String> conversion = ctsConvert(from, it, line)

							resLine.put(it, conversion.join(";"))
							println ""
						}
					} catch (Exception e) {
						if (e != null) {
							e.message
						}
					}
				}

				if (result.containsKey(line)) {
					repeated++
					result[line].putAll(resLine)
				} else {
					result.putAll([(line): resLine])
				}
				println ""
			}
		}

		// save data
		String name = inFile.name[0..inFile.name.lastIndexOf(".")] + "csv"

		println "output name: result.csv"
		def outFile = new File("result.csv")

		if (outFile.exists()) {
			outFile.delete()
		}

		int saveCount = 0
		outFile.withWriter { writer ->
			writer.append("Keyword\t${to.join("\t")}\n")
			input.each { token ->

				print "Saving ($token): ${result[token]}\n"
				writer.append("$token\t")
				to.each {
					writer.append("${result[token][it]}\t")
				}
				writer.append("\n")
				saveCount++
			}
		}

		println "\nKeywords requested: $keywordCount"
		println "Keywords repeated: $repeated"
		println "Results count: ${result.size()}"

	}


	def ctsConvert(String from, String to, String keyword) {
		HTTPBuilder http = new HTTPBuilder(host)
		def path = "service/convert/$from/$to/$keyword"

		def servResp = http.get(path: path, contentType: ContentType.JSON) { resp, reader ->
			return reader.result[0] ?: ["Not Found"]
		}

		print "$to: $servResp"
		return servResp
	}

	private List checkTos(List<String> to) {
		HTTPBuilder http = new HTTPBuilder(host)
		def path = "service/conversion/toValues"

		def fails = []

		List<String> toVals = http.get(path: path, contentType: ContentType.JSON) { resp, reader ->
			return reader.collect { String val -> val.toLowerCase() }
		}

		to.each { val ->
			if (!toVals.contains(val.toLowerCase())) {
				fails.add(val)
			}
		}

		return fails
	}
}

//==================== script start ====================
Converter app = new Converter()

if (args.length < 1) {
	println """I need a text file with the list of keywords to convert and the IDs to convert from/to.
\tThe first line should be the ID that are you converting from.
\tThe second line should be a comma separated list of IDs to convert to.
\tThe rest should be the list of keywords (one per line) to convert.
\n\tThe results are saved to a "result.csv" file.\n
For a list of supported IDs, please check http://cts.fiehnlab.ucdavis.edu/service/conversion/fromValues and http://cts.fiehnlab.ucdavis.edu/service/conversion/toValues
"""
	System.exit(-1)
} else {
	File input = new File(args[0])

	if (input.exists()) {
		def start = System.nanoTime()
		List<String> keyList = []
		String from = ""
		List to = []

		input.withReader { reader ->
			from = reader.readLine()
			to = reader.readLine().split(",").collect { it.trim() }

			reader.eachLine { line ->
				if (!line.trim().empty) {
					keyList.add(line.trim())
				}
			}
		}

		def data = app.process(from, to, keyList, input)
		println "Conversion took: ${(System.nanoTime() - start)/1000000000} seconds."
	} else {
		println "File ${input.absoluteFile} doesn't exists."
		System.exit(-1)
	}
}
