<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			http://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			http://www.springframework.org/schema/context/spring-context.xsd">

	<import resource="scoringConf.xml"/>
	<import resource="filtersConf.xml"/>
	<import resource="identifyConf.xml"/>
	<import resource="multiplexersConf.xml"/>

	<context:property-placeholder location="classpath:defaultTest.properties" />

	<!-- default configuration -->
	<bean id="defaultConfiguration" class="edu.ucdavis.fiehnlab.chemify.configuration.SpringConfiguration">
		<property name="searchTermMinQuality" value="${searchTermMinQuality}"/>
		<property name="filters">
			<ref bean="top5Filter"/>
		</property>
		<property name="scoringAlgorithms">
			<ref bean="scoreByBiologicalCount"/>
		</property>
		<property name="multiplexer">
			<ref bean="multiplexers"/>
		</property>
		<property name="algorithms">
			<ref bean="algorithms"/>
		</property>
		<property name="modifications">
			<ref bean="modifiers"/>
		</property>
	</bean>

	<!-- customized configuration -->
	<bean id="combinedConfiguration" class="edu.ucdavis.fiehnlab.chemify.configuration.SpringConfiguration">
		<property name="searchTermMinQuality" value="${searchTermMinQuality}"/>
		<property name="filters">
			<set>
				<ref bean="top5Filter"/>
			</set>
		</property>
		<property name="scoringAlgorithms">
			<ref bean="scoreByBiologicalCount"/>
		</property>
		<property name="multiplexer">
			<ref bean="multiplexers"/>
		</property>

		<!-- customized identifiers -->
		<property name="algorithms">
			<list>
				<ref bean="combinedIdentify"/>
				<ref bean="nothingIdentified"/>
			</list>
		</property>

		<property name="modifications">
			<ref bean="modifiers"/>
		</property>
	</bean>

	<bean id="topHitConfiguration" class="edu.ucdavis.fiehnlab.chemify.configuration.SpringConfiguration">
		<property name="searchTermMinQuality" value="${searchTermMinQuality}"/>
		<property name="filters">
			<set>
				<ref bean="topHitFilter"/>
			</set>
		</property>
		<property name="scoringAlgorithms">
			<ref bean="scoreByBiologicalCount"/>
		</property>
		<property name="multiplexer">
			<ref bean="multiplexers"/>
		</property>
		<property name="algorithms">
			<ref bean="algorithms"/>
		</property>
		<property name="modifications">
			<ref bean="modifiers"/>
		</property>
	</bean>
</beans>
