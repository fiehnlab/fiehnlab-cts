package edu.ucdavis.fiehnlab.chemify.modify;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/10/13
 * Time: 4:52 PM
 */
abstract class AbstractLipidModifierTest {

    protected abstract AbstractLipidModifier getModifier();

}
