package edu.ucdavis.fiehnlab.chemify.identify.lucene;

import edu.ucdavis.fiehnlab.chemify.helper.lucene.GenerateIndex;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;
import org.junit.Ignore;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/15/2013
 * Time: 7:08 PM
 */
@Ignore
public class LuceneIdentifyTest extends AbstractIdentifyTest {
    public void setUp() throws Exception {
        new GenerateIndex().generateIndex(new File("./src/test/resources/lucene"));
    }

    @Override
    protected String[] getTestTerms() {
        //should be aline, but people do stupid spelling errors...
        return new String[]{"methylpentanal","ethanamine"};
    }



    @Override
    public AbstractIdentify getIdentify() {
        return new LuceneFuzzyIdentify();
    }
}
