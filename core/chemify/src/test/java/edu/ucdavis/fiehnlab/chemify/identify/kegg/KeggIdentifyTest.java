package edu.ucdavis.fiehnlab.chemify.identify.kegg;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;
import org.junit.Ignore;

/**
 * Created by diego on 9/28/2014.
 */
@Ignore("half implementation")
public class KeggIdentifyTest extends AbstractIdentifyTest {
	@Override
	public AbstractIdentify getIdentify() {
		return new KeggIdentify();
	}

	public String getTestTerm() {
		return "alanine";
	}
}
