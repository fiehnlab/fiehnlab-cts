package edu.ucdavis.fiehnlab.chemify.identify;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.filter.TopNFilter;
import edu.ucdavis.fiehnlab.chemify.modify.*;
import edu.ucdavis.fiehnlab.chemify.multiplex.DashMultiPlexer;
import edu.ucdavis.fiehnlab.chemify.multiplex.SpaceMultiPlexer;
import edu.ucdavis.fiehnlab.chemify.statistics.TestDataCollector;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by diego on 9/29/14.
 */

@Ignore
public abstract class AbstractValidationTest {

	/**
	 * access to our identify confifugartion
	 *
	 * @return
	 */
	public abstract Chemify getChemify();

	private TestDataCollector statisticsCollector = TestDataCollector.getInstance();

	private long time = 0;

	Logger logger = Logger.getLogger(this.getClass().getSimpleName());

	protected Configuration configureConfiguration(Configuration configuration) {
		configuration.setSearchTermMinQuality(0.25);

		List<Modify> modifiers = new ArrayList<Modify>();
		modifiers.add(new DeuteratedCompoundModifer());
		modifiers.add(new ISTDModifier());
		modifiers.add(new NISTModifier());
		modifiers.add(new TrimRetentionTimeModifier());
		modifiers.add(new TrimModifier());
		modifiers.add(new ArtifactModifier());
		modifiers.add(new TMSModifier());
		modifiers.add(new MinorModifier());
		modifiers.add(new ZModifier());
		modifiers.add(new GeneralUnderscoreModifier());
		modifiers.add(new NegativeAductModifier());
		modifiers.add(new PositiveAductModifier());
		modifiers.add(new LipidModifier());
		modifiers.add(new PhospholipidModifier());
		modifiers.add(new SterolModifier());
		modifiers.add(new SphingolipidModifier());
		modifiers.add(new ParensStarModifier());
		modifiers.add(new DehydratedModifier());
		modifiers.add(new MeoxModifer());
		configuration.setModifications(modifiers);

		Set<Multiplexer> multiplexers = new HashSet<Multiplexer>();
		multiplexers.add(new SpaceMultiPlexer());
		multiplexers.add(new DashMultiPlexer());
//		multiplexers.add(new AcidSuffixMultiPlexer());
		configuration.setMultiplexer(multiplexers);

		configuration.addFilter(new TopNFilter(5));

		return configuration;
	}

	@AfterClass
	public static void printStats() {
//		TestDataCollector.getInstance().printData();
		TestDataCollector.getInstance().saveData();
		System.out.println("Total tests: " + TestDataCollector.getInstance().getTotal() + "\n" +
				"Passing: " + TestDataCollector.getInstance().getPass() + " (" + TestDataCollector.getInstance().getPct() + "%)\n" +
				"Passing: " + TestDataCollector.getInstance().getPartial() + " (" + TestDataCollector.getInstance().getPartial() * 100 / TestDataCollector.getInstance().getTotal() + "%)");
	}

	private void runTestToken(String term, String expected) {
		logger.debug("testing '" + term + "'...");
		Chemify chemify = getChemify();
		long start = System.nanoTime();
		List<? extends Hit> result = chemify.identify(term);
		time = System.nanoTime() - start;


		statisticsCollector.collect(term, expected, result, time);

		assertTrue(result.size() > 0);
		Hit hit = result.get(0);
		assertEquals(expected, hit.getInchiKey());
	}

	@Test
	public void testGlucose() {
		runTestToken("glucose", "WQZGKKKJIJFFOK-DVKNGEFBSA-N");
	}

	@Test
	public void testStearicAcid() {
		runTestToken("stearic acid", "QIQXTHQIDYTFRH-VJNPICPZSA-N");
	}

	@Test
	public void testAlanine() {
		runTestToken("alanine", "QNAYBMKLOCPYGJ-REOHCLBHSA-O");
	}

	@Test
	public void testCholesterol() {
		runTestToken("cholesterol", "HVYWMOMLDIMFJA-UHFFFAOYSA-N");
	}

	@Test
	public void testValine() {
		runTestToken("valine", "KZSNJWFQEVHDMF-BYPYZUCNSA-N");
	}

	@Test
	public void testGlycine() {
		runTestToken("glycine", "DHMQDGOQFOQNFH-UHFFFAOYSA-N");
	}

	@Test
	public void testGlutamine() {
		runTestToken("glutamine", "ZDXPYRJPNDTMRX-GSVOUGTGSA-N");
	}

	@Test
	public void testLacticAcid() {
		runTestToken("lactic acid", "VMBUTBBKKXOLOE-UHFFFAOYSA-H");
	}

	@Test
	public void testUrea() {
		runTestToken("urea", "XGCPRKZQVACHIU-UHFFFAOYSA-N");
	}

	@Test
	public void testOxoproline() {
		runTestToken("oxoproline", "ODHCTXKNWHHXJC-VKHMYHEASA-N");
	}

	@Test
	public void testPhosphoricAcid() {
		runTestToken("phosphoric acid", "QVLTXCYWHPZMCA-UHFFFAOYSA-N");
	}

	@Test
	public void testLeucine() {
		runTestToken("leucine", "ROHFNLRQFUQHCH-YFKPBYRVSA-N");
	}

	@Test
	public void testTyrosine() {
		runTestToken("tyrosine", "OUYCCCASQSFEME-UHFFFAOYSA-N");
	}

	@Test
	public void testGlycerol() {
		runTestToken("glycerol", "RBNPOMFGQQGHHO-UWTATZPHSA-N");
	}

	@Test
	public void test15Anhydroglucitol() {
		runTestToken("1,5-anhydroglucitol", "MPCAJMNYNOGXPB-SLPGGIOYSA-N");
	}

	@Test
	public void testTryptophan() {
		runTestToken("tryptophan", "QIVBCDIJIAJPQS-UHFFFAOYSA-N");
	}

	@Test
	public void testCitricAcid() {
		runTestToken("citric acid", "BJEPYKJPYRNKOW-UWTATZPHSA-N");
	}

	@Test
	public void testIsoleucine() {
		runTestToken("isoleucine", "AGPKZVBTJJNPAG-RFZPGFLSSA-N");
	}

	@Test
	public void testUricAcid() {
		runTestToken("uric acid", "LEHOTFFKMJEONL-NUTRPMROSA-N");
	}

	@Test
	public void testThreonine() {
		runTestToken("threonine", "AYFVYJQAPQTCCC-UHFFFAOYSA-N");
	}

	@Test
	public void testLysine() {
		runTestToken("lysine", "KDXKERNSBIXSRK-YFKPBYRVSA-O");
	}

	@Test
	public void testSerine() {
		runTestToken("serine", "QNAYBMKLOCPYGJ-UHFFFAOYSA-N");
	}

	@Test
	public void testPalmiticAcid() {
		runTestToken("palmitic acid", "IPCSVZSSVZVIGE-ZXWYYLDKSA-N");
	}

	@Test
	public void testProline() {
		runTestToken("proline", "ONIBWKKTOPOVIA-OUBTZVSYSA-N");
	}

	@Test
	public void testOrnithine() {
		runTestToken("ornithine", "AHLPHDHHMVZTML-BYPYZUCNSA-P");
	}

	@Test
	public void test2HydroxybutanoicAcid() {
		runTestToken("2-hydroxybutanoic acid", "AFENDNXGAFYKQO-UHFFFAOYSA-N");
	}

	@Test
	public void testPhenylalanine() {
		runTestToken("phenylalanine", "COLNVLDHVKWLRT-MRVPVSSYSA-N");
	}

	@Test
	public void testPelargonicAcid() {
		runTestToken("pelargonic acid", "FBUKVWPVBMHYJY-UHFFFAOYSA-N");
	}

	@Test
	public void testHistidine() {
		runTestToken("histidine", "HNDVDQJCIGZPNO-RXMQYKEDSA-N");
	}

	@Test
	public void testInositolMyo() {
		runTestToken("inositol myo-", "Not Found");
	}

	@Test
	public void testGlutamicAcid() {
		runTestToken("glutamic acid", "WHUUTDBJXJRKMK-VKHMYHEASA-N");
	}

	@Test
	public void testNMethylalanine() {
		runTestToken("N-methylalanine", "GDFAOVXKHJXLEI-VKHMYHEASA-N");
	}

	@Test
	public void testBenzoicAcid() {
		runTestToken("benzoic acid", "WPYMKLBDIGXBTP-UHFFFAOYSA-N");
	}

	@Test
	public void testLauricAcid() {
		runTestToken("lauric acid", "POULHZVOKOAJMA-HNHCFKFXSA-N");
	}

	@Test
	public void testPentadecanoicAcid() {
		runTestToken("pentadecanoic acid", "WQEPLUUGTLDZJY-UHFFFAOYSA-N");
	}

	@Test
	public void testBetaMannosylglycerate() {
		runTestToken("beta-mannosylglycerate", "Not Found");
	}

	@Test
	public void testTalose() {
		runTestToken("talose", "GZCGUPFRVQAUEE-KAZBKCHUSA-N");
	}

	@Test
	public void testIminodiaceticAcid() {
		runTestToken("iminodiacetic acid", "NBZBKCUXIYYUSX-UHFFFAOYSA-N");
	}

	@Test
	public void test3HydroxybutanoicAcid() {
		runTestToken("3-hydroxybutanoic acid", "OZFFNUOMWYGCPW-UHFFFAOYSA-N");
	}

	@Test
	public void testOleicAcid() {
		runTestToken("oleic acid", "QSBYPNXLFMSGKH-HJWRWDBZSA-N");
	}

	@Test
	public void testGlycericAcid() {
		runTestToken("glyceric acid", "RBNPOMFGQQGHHO-UHFFFAOYSA-N");
	}

	@Test
	public void testCreatinine() {
		runTestToken("creatinine", "BTXYOFGSUFEOLA-UHFFFAOYSA-N");
	}

	@Test
	public void test2KetoisocaproicAcid() {
		runTestToken("2-ketoisocaproic acid", "BKAJNAXTPSGJCU-UHFFFAOYSA-N");
	}

	@Test
	public void testAsparagine() {
		runTestToken("asparagine", "DCXYFEDJOCDNAF-UWTATZPHSA-N");
	}

	@Test
	public void test1Monoolein() {
		runTestToken("1-monoolein", "RZRNAYUHWVFMIP-QJRAZLAKSA-N");
	}

	@Test
	public void testCystine() {
		runTestToken("cystine", "LEVWYRKDKASIDU-IMJSIDKUSA-N");
	}

	@Test
	public void testAcetophenoneNist() {
		runTestToken("acetophenone NIST", "Not Found");
	}

	@Test
	public void testFructose() {
		runTestToken("fructose", "LKDRXBCSQODPBY-AZGQCCRYSA-N");
	}

	@Test
	public void testMethylhexadecanoicAcid() {
		runTestToken("methylhexadecanoic acid", "Not Found");
	}

	@Test
	public void testMethionine() {
		runTestToken("methionine", "FFEARJCKVFRZRR-BYPYZUCNSA-N");
	}

	@Test
	public void test2KetoadipicAcid() {
		runTestToken("2-ketoadipic acid", "FGSBNBBHOZHUBO-UHFFFAOYSA-N");
	}

	@Test
	public void testTocopherolAlpha() {
		runTestToken("tocopherol alpha", "Not Found");
	}

	@Test
	public void testCapricAcid() {
		runTestToken("capric acid", "GHVNFZFCNZKVNT-OUBTZVSYSA-N");
	}

	@Test
	public void testArachidicAcid() {
		runTestToken("arachidic acid", "VKOBVWXKNCXXDE-UHFFFAOYSA-N");
	}

	@Test
	public void test6DeoxyglucitolNist() {
		runTestToken("6-deoxyglucitol NIST", "Not Found");
	}

	@Test
	public void testDiacetoneAlcoholNist() {
		runTestToken("diacetone alcohol NIST", "Not Found");
	}

	@Test
	public void testPalmitoleicAcid() {
		runTestToken("palmitoleic acid", "SECPZKHBENQXJG-FPLPWBNLSA-M");
	}

	@Test
	public void test2HydroxyvalericAcid() {
		runTestToken("2-hydroxyvaleric acid", "JRHWHSJDIILJAT-UHFFFAOYSA-N");
	}

	@Test
	public void testHydroxylamine() {
		runTestToken("hydroxylamine", "VRXOQUOGDYKXFA-UHFFFAOYSA-N");
	}

	@Test
	public void testMyristicAcid() {
		runTestToken("myristic acid", "VLKZOEOYAKHREP-UHFFFAOYSA-N");
	}

	@Test
	public void testMethionineSulfoxide() {
		runTestToken("methionine sulfoxide", "QEFRNWWLZKMPFJ-KNODYTOMSA-N");
	}

	@Test
	public void testNicotinicAcid() {
		runTestToken("nicotinic acid", "PVNIIMVLHYAWGP-IDEBNGHGSA-N");
	}

	@Test
	public void testAminomalonicAcid() {
		runTestToken("aminomalonic acid", "JINBYESILADKFW-UHFFFAOYSA-N");
	}

	@Test
	public void test3HydroxypropionicAcid() {
		runTestToken("3-hydroxypropionic acid", "ALRHLSYJTWAHJZ-UHFFFAOYSA-N");
	}

	@Test
	public void testLinoleicAcid() {
		runTestToken("linoleic acid", "QIQXTHQIDYTFRH-UHFFFAOYSA-N");
	}

	@Test
	public void testPhenol() {
		runTestToken("phenol", "SNQQPOLDUKLAAF-UHFFFAOYSA-N");
	}

	@Test
	public void testPseudoUridine() {
		runTestToken("pseudo uridine", "Not Found");
	}

	@Test
	public void testDeoxytetronicAcidNist() {
		runTestToken("deoxytetronic acid NIST", "Not Found");
	}

	@Test
	public void testGlycolicAcid() {
		runTestToken("glycolic acid", "AEMRFAOFKBGASW-UHFFFAOYSA-N");
	}

	@Test
	public void testCysteine() {
		runTestToken("cysteine", "XUJNEKJLAYXESH-UHFFFAOYSA-N");
	}

	@Test
	public void testThreonicAcid() {
		runTestToken("threonic acid", "JPIJQSOTBSSVTP-STHAYSLISA-N");
	}

	@Test
	public void testCaprylicAcid() {
		runTestToken("caprylic acid", "WWZKQHOCKIZLMA-OUBTZVSYSA-N");
	}

	@Test
	public void testAsparticAcid() {
		runTestToken("aspartic acid", "CKLJMWTZIZZHCS-UHFFFAOYSA-N");
	}

	@Test
	public void test4Hydroxyproline() {
		runTestToken("4-hydroxyproline", "PMMYEEVYMWASQN-IUYQGCFVSA-N");
	}

	@Test
	public void testFucoseRhamnose() {
		runTestToken("fucose + rhamnose", "Not Found");
	}

	@Test
	public void testEthanolamine() {
		runTestToken("ethanolamine", "HZAXFHJVJLSVMW-UHFFFAOYSA-N");
	}

	@Test
	public void test2DeoxyerythritolNist() {
		runTestToken("2-deoxyerythritol NIST", "Not Found");
	}

	@Test
	public void testParabanicAcidNist() {
		runTestToken("parabanic acid NIST", "Not Found");
	}

	@Test
	public void testCitrulline() {
		runTestToken("citrulline", "RHGKLRLOHDJJDR-UHFFFAOYSA-N");
	}

	@Test
	public void testXylose() {
		runTestToken("xylose", "SRBFZHDQGSBBOR-LECHCGJUSA-N");
	}

	@Test
	public void testIsothreonicAcid() {
		runTestToken("isothreonic acid", "Not Found");
	}

	@Test
	public void testPyruvicAcid() {
		runTestToken("pyruvic acid", "LCTONWCANYUPML-UHFFFAOYSA-N");
	}

	@Test
	public void testHeptadecanoicAcidNist() {
		runTestToken("heptadecanoic acid NIST", "Not Found");
	}

	@Test
	public void testIndole3Acetate() {
		runTestToken("indole-3-acetate", "SEOVTRFCIGRIMH-UHFFFAOYSA-M");
	}

	@Test
	public void testElaidicAcid() {
		runTestToken("elaidic acid", "ZQPPMHVWECSIRJ-UHFFFAOYSA-N");
	}

	@Test
	public void testArachidonicAcid() {
		runTestToken("arachidonic acid", "LBHYAARBIPDUPG-UHFFFAOYSA-N");
	}

	@Test
	public void testGammaTocopherol() {
		runTestToken("gamma-tocopherol", "QUEDXNHFTDJVIY-DQCZWYHMSA-N");
	}

	@Test
	public void testErythritol() {
		runTestToken("erythritol", "UNXHWFMMPAWVPI-UHFFFAOYSA-N");
	}

	@Test
	public void testSuccinicAcid() {
		runTestToken("succinic acid", "KDYFGRWQOYBRFD-LBPDFUHNSA-N");
	}

	@Test
	public void testGlycerolAlphaPhosphate() {
		runTestToken("glycerol-alpha-phosphate", "Not Found");
	}

	@Test
	public void testIsocitricAcid() {
		runTestToken("isocitric acid", "ODBLHEXUDAPZAU-ZAFYKAAXSA-N");
	}

	@Test
	public void testBehenicAcid() {
		runTestToken("behenic acid", "UKMSUNONTOPOIO-ZVDSADNZSA-N");
	}

	@Test
	public void testBenzylalcohol() {
		runTestToken("benzylalcohol", "WVDDGKGOMKODPV-UHFFFAOYSA-N");
	}

	@Test
	public void testIndole3Lactate() {
		runTestToken("indole-3-lactate", "XGILAAMKEQUXLS-UHFFFAOYSA-M");
	}

	@Test
	public void testUracil() {
		runTestToken("uracil", "ISAKRJDGNUQOIC-DOMIDYPGSA-N");
	}

	@Test
	public void testMethanolphosphate() {
		runTestToken("methanolphosphate", "Not Found");
	}

	@Test
	public void testGlyoxalureaNist() {
		runTestToken("glyoxalurea NIST", "Not Found");
	}

	@Test
	public void testAdipicAcid() {
		runTestToken("adipic acid", "YVSCCMNRWFOKDU-UHFFFAOYSA-N");
	}

	@Test
	public void testSorbitol() {
		runTestToken("sorbitol", "FBPFZTCFMRRESA-NQAPHZHOSA-N");
	}

	@Test
	public void testPhosphoethanolamine() {
		runTestToken("phosphoethanolamine", "SUHOOTKUPISOBE-UHFFFAOYSA-N");
	}

	@Test
	public void testTrans4Hydroxyproline() {
		runTestToken("trans-4-hydroxyproline", "PMMYEEVYMWASQN-UHFFFAOYSA-N");
	}

	@Test
	public void testGlucuronicAcid() {
		runTestToken("glucuronic acid", "IAJILQKETJEXLJ-UHFFFAOYSA-N");
	}

	@Test
	public void testMaleimide() {
		runTestToken("maleimide", "XHXZBCFAQZKILF-UHFFFAOYSA-N");
	}

	@Test
	public void testLignocericAcid() {
		runTestToken("lignoceric acid", "QZZGJDVWLFXDLK-UHFFFAOYSA-N");
	}

	@Test
	public void testPropane13DiolNist() {
		runTestToken("propane-1,3-diol NIST", "Not Found");
	}

	@Test
	public void testPropane123TricarboxylateNist() {
		runTestToken("propane-1,2,3-tricarboxylate NIST", "Not Found");
	}

	@Test
	public void test2HydroxyglutaricAcid() {
		runTestToken("2-hydroxyglutaric acid", "HWXBTNAVRSUOJR-UHFFFAOYSA-N");
	}

	@Test
	public void testHypoxanthine() {
		runTestToken("hypoxanthine", "JPOVYURXCDZDIC-UHFFFAOYSA-M");
	}

	@Test
	public void testGluconicAcid() {
		runTestToken("gluconic acid", "RGHNJXZEOKUKBD-SQOUGZDYSA-N");
	}

	@Test
	public void testShikimicAcid() {
		runTestToken("shikimic acid", "JXOHGGNKMLTUBP-HSUXUTPPSA-N");
	}

	@Test
	public void testRibitol() {
		runTestToken("ribitol", "HEBKCHPVOIAQTA-SCDXWVJYSA-N");
	}

	@Test
	public void testGalactonicAcid() {
		runTestToken("galactonic acid", "RGHNJXZEOKUKBD-UHFFFAOYSA-N");
	}

	@Test
	public void testGlucose1Phosphate() {
		runTestToken("glucose-1-phosphate", "HXXFSFRBOHSIMQ-GASJEMHNSA-N");
	}

	@Test
	public void testArabinose() {
		runTestToken("arabinose", "PYMYPHUHKUWMLA-VAYJURFESA-N");
	}

	@Test
	public void testMalicAcid() {
		runTestToken("malic acid", "BJEPYKJPYRNKOW-UHFFFAOYSA-N");
	}

	@Test
	public void testMaltose() {
		runTestToken("maltose", "GUBGYTABKSRVRQ-PICCSMPSSA-N");
	}

	@Test
	public void testAlloxanoicAcid() {
		runTestToken("alloxanoic acid", "Not Found");
	}

	@Test
	public void testDihydroabieticAcid() {
		runTestToken("dihydroabietic acid", "BTAURFWABMSODR-UHFFFAOYSA-N");
	}

	@Test
	public void testTrehalose() {
		runTestToken("trehalose", "HDTRYLNUVZCQOY-LIZSDCNHSA-N");
	}

	@Test
	public void testThreitol() {
		runTestToken("threitol", "UNXHWFMMPAWVPI-UHFFFAOYSA-N");
	}

	@Test
	public void testDodecane() {
		runTestToken("dodecane", "SNRUBQQJIBEYMU-UHFFFAOYSA-N");
	}

	@Test
	public void testLevoglucosan() {
		runTestToken("levoglucosan", "TWNIBLMWSKIRAT-VFUOTHLCSA-N");
	}

	@Test
	public void test24DiaminobutyricAcid() {
		runTestToken("2,4-diaminobutyric acid", "OGNSCSPNOLGXSM-VKHMYHEASA-N");
	}

	@Test
	public void testFumaricAcid() {
		runTestToken("fumaric acid", "VZCYOOQTPOCHFL-OWOJBTEDSA-N");
	}

	@Test
	public void testHydroxycarbamateNist() {
		runTestToken("hydroxycarbamate NIST", "Not Found");
	}

	@Test
	public void testTartaricAcid() {
		runTestToken("tartaric acid", "SXFBQAMLJMDXOD-UHFFFAOYSA-N");
	}

	@Test
	public void testOctadecanol() {
		runTestToken("octadecanol", "GLDOVTGHNKAZLK-UHFFFAOYSA-N");
	}

	@Test
	public void testGlyceroGuloheptoseNist() {
		runTestToken("glycero-guloheptose NIST", "Not Found");
	}

	@Test
	public void testConduritolBetaEpoxide() {
		runTestToken("conduritol beta epoxide", "Not Found");
	}

	@Test
	public void testPutrescine() {
		runTestToken("putrescine", "KIDHWZJUCRJVML-UHFFFAOYSA-N");
	}

	@Test
	public void test1Hexadecanol() {
		runTestToken("1-hexadecanol", "BXWNKGSJHAJOGX-UHFFFAOYSA-N");
	}

	@Test
	public void testSalicylicAcid() {
		runTestToken("salicylic acid", "YGSDEFSMJLZEOE-UHFFFAOYSA-N");
	}

	@Test
	public void testPhytol() {
		runTestToken("phytol", "BOTWFXYSPFMFNR-UHFFFAOYSA-N");
	}

	@Test
	public void testXylitol() {
		runTestToken("xylitol", "HEBKCHPVOIAQTA-SCDXWVJYSA-N");
	}

	@Test
	public void testGlycerol3Galactoside() {
		runTestToken("glycerol-3-galactoside", "Not Found");
	}

	@Test
	public void testPyrrole2CarboxylicAcid() {
		runTestToken("pyrrole-2-carboxylic acid", "WRHZVMBBRYBTKZ-UHFFFAOYSA-N");
	}

	@Test
	public void testAlphaKetoglutaricAcid() {
		runTestToken("alpha ketoglutaric acid", "Not Found");
	}

	@Test
	public void test2DeoxytetronicAcid() {
		runTestToken("2-deoxytetronic acid", "DZAIOXUZHHTJKN-UHFFFAOYSA-N");
	}

	@Test
	public void test1Monostearin() {
		runTestToken("1-monostearin", "VBICKXHEKHSIBG-UHFFFAOYSA-N");
	}

	@Test
	public void testDodecanol() {
		runTestToken("dodecanol", "LQZZUXJYWNFBMV-UHFFFAOYSA-N");
	}

	@Test
	public void testGaba() {
		runTestToken("GABA", "HAMNKKUPIHEESI-UHFFFAOYSA-N");
	}

	@Test
	public void testTrihydroxypyrazineNist() {
		runTestToken("trihydroxypyrazine NIST", "Not Found");
	}

	@Test
	public void testTerephtalicAcid() {
		runTestToken("terephtalic acid", "Not Found");
	}

	@Test
	public void testRibonicAcid() {
		runTestToken("ribonic acid", "QXKAIJAYHKCRRA-UHFFFAOYSA-N");
	}

	@Test
	public void test5Methoxytryptamine() {
		runTestToken("5-methoxytryptamine", "TXVAYRSEKRMEIF-UHFFFAOYSA-N");
	}

	@Test
	public void testBiuret() {
		runTestToken("biuret", "OHJMTUPIZMNBFR-UHFFFAOYSA-N");
	}

	@Test
	public void testTriethanolamine() {
		runTestToken("triethanolamine", "GSEJCLTVZPLZKY-UHFFFAOYSA-N");
	}

	@Test
	public void testInulobiose() {
		runTestToken("inulobiose", "QGQYPBMXRYBEPG-YPUGLKDQSA-N");
	}

	@Test
	public void testCellobiotol() {
		runTestToken("cellobiotol", "VQHSOMBJVWLPSR-WELRSGGNSA-N");
	}

	@Test
	public void testKynurenine() {
		runTestToken("kynurenine", "YGPSJZOEDVAXAB-UHFFFAOYSA-N");
	}

	@Test
	public void test1Monopalmitin() {
		runTestToken("1-monopalmitin", "QHZLMUACJMDIAE-UHFFFAOYSA-N");
	}

	@Test
	public void testUdpGlucuronicAcid() {
		runTestToken("UDP-glucuronic acid", "HDYANYHVCAPMJV-TVUPIXCLSA-N");
	}

	@Test
	public void testInositolAllo() {
		runTestToken("inositol allo-", "Not Found");
	}

	@Test
	public void testUridine() {
		runTestToken("uridine", "DRTQHJPVMGBUCF-UHFFFAOYSA-N");
	}

	@Test
	public void test3AminoisobutyricAcid() {
		runTestToken("3-aminoisobutyric acid", "QCHPKSFMDHPSNR-UHFFFAOYSA-N");
	}

	@Test
	public void testPyrophosphate() {
		runTestToken("pyrophosphate", "XPPKVPWEQAFLFU-UHFFFAOYSA-K");
	}

	@Test
	public void testTocopherolBetaNist() {
		runTestToken("tocopherol beta NIST", "Not Found");
	}

	@Test
	public void test23DihydroxybutanoicAcidNist() {
		runTestToken("2,3-dihydroxybutanoic acid NIST", "Not Found");
	}

	@Test
	public void test5HydroxynorvalineNist() {
		runTestToken("5-hydroxynorvaline NIST", "Not Found");
	}

	@Test
	public void testGlycylProline() {
		runTestToken("glycyl proline", "KZNQNBZMBZJQJO-YFKPBYRVSA-N");
	}

	@Test
	public void testEnolpyruvateNist() {
		runTestToken("enolpyruvate NIST", "Not Found");
	}

	@Test
	public void testPyrazine25DihydroxyNist() {
		runTestToken("pyrazine 2,5-dihydroxy  NIST", "Not Found");
	}

	@Test
	public void testSucrose() {
		runTestToken("sucrose", "VLWPFYSNMQPWKT-JZINUAPNSA-N");
	}

	@Test
	public void testBetaGlycerolphosphate() {
		runTestToken("beta-glycerolphosphate", "Not Found");
	}

	@Test
	public void test4HydroxybutyricAcid() {
		runTestToken("4-hydroxybutyric acid", "SJZRECIVHVDYJC-UHFFFAOYSA-N");
	}

	@Test
	public void test3Phosphoglycerate() {
		runTestToken("3-phosphoglycerate", "OSJPPGNTCRNQQC-UHFFFAOYSA-K");
	}

	@Test
	public void testNAcetylasparticAcid() {
		runTestToken("N-acetylaspartic acid", "OTCCIMWXFLJLIA-BYPYZUCNSA-N");
	}

	@Test
	public void testIsobuteneGlycolNist() {
		runTestToken("isobutene glycol NIST", "Not Found");
	}

	@Test
	public void testPhenylethylamine() {
		runTestToken("phenylethylamine", "BHHGXPLMPWCGHP-UHFFFAOYSA-N");
	}

	@Test
	public void testCysteineGlycine() {
		runTestToken("cysteine-glycine", "Not Found");
	}

	@Test
	public void testQuinicAcid() {
		runTestToken("quinic acid", "AAWZDTNXLSGCEK-RKGSPJAZSA-N");
	}

	@Test
	public void testArabitol() {
		runTestToken("arabitol", "HEBKCHPVOIAQTA-UHFFFAOYSA-N");
	}

	@Test
	public void testPipecolicAcid() {
		runTestToken("pipecolic acid", "HXEACLLIILLPRG-UHFFFAOYSA-N");
	}

	@Test
	public void test124Benzenetriol() {
		runTestToken("1,2,4-benzenetriol", "GGNQRNBDZQJCCN-UHFFFAOYSA-N");
	}

	@Test
	public void testSaccharicAcid() {
		runTestToken("saccharic acid", "DSLZVSRJTYRBFB-UHFFFAOYSA-N");
	}

	@Test
	public void testIdonicAcidNist() {
		runTestToken("idonic acid NIST", "Not Found");
	}

	@Test
	public void test3MethoxytyrosineNist() {
		runTestToken("3-methoxytyrosine NIST", "Not Found");
	}

	@Test
	public void testLactobionicAcid() {
		runTestToken("lactobionic acid", "JYTUSYBCFIZPBE-AMTLMPIISA-N");
	}

	@Test
	public void testCellobiose() {
		runTestToken("cellobiose", "GUBGYTABKSRVRQ-QUYVBRFLSA-N");
	}

	@Test
	public void testTaurine() {
		runTestToken("taurine", "XOAAWQZATWQOTB-UHFFFAOYSA-N");
	}

	@Test
	public void testNaproxen() {
		runTestToken("naproxen", "CMWTZPSULFXXJA-SECBINFHSA-N");
	}

	@Test
	public void testGlutaricAcid() {
		runTestToken("glutaric acid", "JFCQEDHGNNZCLN-UHFFFAOYSA-N");
	}

	@Test
	public void testGlycerolBetaPhosphate() {
		runTestToken("glycerol-beta-phosphate", "Not Found");
	}

	@Test
	public void testNAcetylDMannosamine() {
		runTestToken("N-acetyl-D-mannosamine", "OVRNDRQMDRJTHS-UHFFFAOYSA-N");
	}

	@Test
	public void testAconiticAcid() {
		runTestToken("aconitic acid", "GTZCVFVGUGFEME-HNQUOIGGSA-N");
	}

	@Test
	public void testOAcetylserine() {
		runTestToken("O-acetylserine", "VZXPDPZARILFQX-BYPYZUCNSA-N");
	}

	@Test
	public void testMaltotriose() {
		runTestToken("maltotriose", "RXVWSYJTUUKTEA-UHFFFAOYSA-N");
	}

	@Test
	public void testStigmasterol() {
		runTestToken("stigmasterol", "HCXVJBMSMIARIN-HJWRWDBZSA-N");
	}

	@Test
	public void testPyrogallol() {
		runTestToken("pyrogallol", "WQGWDDDVZFFDIG-UHFFFAOYSA-N");
	}

	@Test
	public void test2OxogluconicAcidNist() {
		runTestToken("2-oxogluconic acid NIST", "Not Found");
	}

	@Test
	public void test235TrihydroxypyrazineNist() {
		runTestToken("2,3,5-trihydroxypyrazine NIST", "Not Found");
	}

	@Test
	public void test2AminoadipicAcid() {
		runTestToken("2-aminoadipic acid", "OYIFNHCXNCRBQI-UHFFFAOYSA-N");
	}

	@Test
	public void test2Monopalmitin() {
		runTestToken("2-monopalmitin", "BBNYCLAREVXOSG-UHFFFAOYSA-N");
	}

	@Test
	public void testEpsilonCaprolactam() {
		runTestToken("epsilon-caprolactam", "JBKVHLHDHHXQEQ-UHFFFAOYSA-N");
	}

	@Test
	public void testCitramalicAcid() {
		runTestToken("citramalic acid", "XFTRTWQBIOMVPK-UHFFFAOYSA-N");
	}

	@Test
	public void testBetaAlanine() {
		runTestToken("beta-alanine", "UCMIRNVEIXFBKS-UHFFFAOYSA-N");
	}

	@Test
	public void testAzelaicAcid() {
		runTestToken("azelaic acid", "BDJRBEYXGGNYIS-UHFFFAOYSA-N");
	}

	@Test
	public void testNAcetylDTryptophan() {
		runTestToken("N-acetyl-D-tryptophan", "DZTHIGRZJZPRDV-UHFFFAOYSA-N");
	}

	@Test
	public void test36Anhydrogalactose() {
		runTestToken("3,6-anhydrogalactose", "WZYRMLAWNVOIEX-BGPJRJDNSA-N");
	}

	@Test
	public void testParacetamol() {
		runTestToken("paracetamol", "RZVAJINKPMORJF-UHFFFAOYSA-N");
	}

	@Test
	public void testAdenosine5Phosphate() {
		runTestToken("adenosine-5-phosphate", "UDMBCSSLTHHNCD-KQYNXXCUSA-N");
	}

	@Test
	public void testLathosterolNist() {
		runTestToken("lathosterol NIST", "Not Found");
	}

	@Test
	public void testGlucoheptulose() {
		runTestToken("glucoheptulose", "Not Found");
	}

	@Test
	public void testMevalonicAcidNist() {
		runTestToken("mevalonic acid NIST", "Not Found");
	}

	@Test
	public void testRibose() {
		runTestToken("ribose", "SRBFZHDQGSBBOR-UHFFFAOYSA-N");
	}

	@Test
	public void testChlorogenicAcid() {
		runTestToken("chlorogenic acid", "CWVRJTMFETXNAD-JUHZACGLSA-N");
	}

	@Test
	public void testAllantoicAcid() {
		runTestToken("allantoic acid", "NUCLJNSWZCHRKL-UHFFFAOYSA-M");
	}

	@Test
	public void testSophorose() {
		runTestToken("sophorose", "HIWPGCMGAMJNRG-UHFFFAOYSA-N");
	}

	@Test
	public void testGalactose6Phosphate() {
		runTestToken("galactose-6-phosphate", "VFRROHXSMXFLSN-UHFFFAOYSA-N");
	}

	@Test
	public void testFormononetin() {
		runTestToken("formononetin", "AEDDIBAIWPIIBD-ZJKJAXBQSA-N");
	}

	@Test
	public void testAdenosine() {
		runTestToken("adenosine", "OIRDTQYFTABQOQ-CRKDRTNXSA-N");
	}

	@Test
	public void test2HydroxyhippuricAcid() {
		runTestToken("2-hydroxyhippuric acid", "ONJSZLXSECQROL-UHFFFAOYSA-N");
	}

}

