package edu.ucdavis.fiehnlab.chemify;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 8:47 PM
 * <p/>
 * Tests the chemify test in general
 */
public class OddBallsTest {

	protected Chemify chemify;
	protected Logger logger = Logger.getLogger(this.getClass());

	@Before
	public void setUp() {
		this.chemify = this.getChemify();

//		logger.error(this.chemify.getDefaultConfiguration().toString());
	}

	@After
	public void tearDown() {
		this.chemify = null;
	}

	@Test
	public void test3hydroxybutanoicAcid() {
		String term = "3-hydroxybutanoic acid";
		String exp = "WHBMMWSBFZVSSR-GSVOUGTGSA-N";

		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
//		assertEquals(exp, res.get(0).getInchiKey());
	}

	@Test
	public void test2HydroxyhippuricAcid() {
		String term = "2-hydroxyhippuric acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testmethoxytyrosine() {
		String term = "3-methoxytyrosine NIST";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testarabinose() {
		String term = "arabinose";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testanhydrogalactose() {
		String term = "3,6-anhydrogalactose";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testacetylmannosamine() {
		String term = "N-acetyl-D-mannosamine";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testglyoxalurea() {
		String term = "glyoxalurea NIST";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testglucose() {
		String term = "glucose";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testdiaminobutyric() {
		String term = "2,4-diaminobutyric acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testelaidic() {
		String term = "elaidic acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testnaproxen() {
		String term = "naproxen";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testmaltotriose() {
		String term = "maltotriose";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testindolelactate() {
		String term = "indole-3-lactate";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testNacetylDtryptophan() {
		String term = "N-acetyl-D-tryptophan";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testgalactose() {
		String term = "galactose-6-phosphate";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testcysteine() {
		String term = "cysteine";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testglyceric() {
		String term = "glyceric acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testsophorose() {
		String term = "sophorose";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testhydroxybutanoic() {
		String term = "2-hydroxybutanoic acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testpipecolic() {
		String term = "pipecolic acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testlactobionic() {
		String term = "lactobionic acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testoxogluconic() {
		String term = "2-oxogluconic acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testpropanetricarboxylate() {
		String term = "propane-1,2,3-tricarboxylate";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testarachidonic() {
		String term = "arachidonic acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testkynurenine() {
		String term = "kynurenine";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testinulobiose() {
		String term = "inulobiose";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	@Test
	public void testdihydroxybutanoic() {
		String term = "2,3-dihydroxybutanoic acid";
		List<? extends Hit> res = chemify.identify(term);

		for (Hit hit : res) {
			logger.debug(hit.getOriginalTerm() + "\t" + hit.getSearchTerm() + "\t" + hit.getInchiKey());
		}

		assert !res.isEmpty();
	}

	/**
	 * returns our chemify instance to be used
	 *
	 * @return
	 */
	protected Chemify getChemify() {
		ApplicationContext ctx = new FileSystemXmlApplicationContext("src/test/resources/defaultTestConf.xml");
		return new Chemify((Configuration) ctx.getBean("defaultConfiguration"));
	}
}

