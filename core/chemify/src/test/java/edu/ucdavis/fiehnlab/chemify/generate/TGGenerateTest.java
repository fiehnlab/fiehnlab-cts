package edu.ucdavis.fiehnlab.chemify.generate;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 3:45 PM
 */
public class TGGenerateTest {
	Logger logger = Logger.getLogger(this.getClass());
	TGGenerate generate;

	@Before
	public void setUp() {
		generate = new TGGenerate();
	}

	@After
	public void tearDown() {
		generate = null;
	}

	@Test
    public void testGenerateIncludeOdds() throws Exception {

        Set<String> result = generate.generate("TG(36:0)",true);

	    logger.debug("RESULT: " + result);

        assertTrue(result.contains("TG(18:0/14:0/4:0)"));
        assertTrue(result.contains("TG(18:0/13:0/5:0)"));
        assertFalse(result.contains("TG(18:0/14:1/4:0)"));

    }

    @Test
    public void testGenerateIncludeOddsWithDoubleBound() throws Exception {

        Set<String> result = generate.generate("TG(36:1)",true);

        assertTrue(result.contains("TG(18:0/14:0/4:1)"));
        assertTrue(result.contains("TG(18:0/13:0/5:1)"));
        assertTrue(result.contains("TG(18:0/14:1/4:0)"));

    }

    @Test
    public void testGenerateExcludeOdds() throws Exception {

        Set<String> result = generate.generate("TG(36:0)",false);

        assertTrue(result.contains("TG(18:0/14:0/4:0)"));
        assertFalse(result.contains("TG(18:0/13:0/5:0)"));
        assertFalse(result.contains("TG(18:0/14:1/4:0)"));

    }

    @Test
    public void testGenerateExcludeOddsWithDoubleBounds() throws Exception {

        Set<String> result = generate.generate("TG(36:2)",false);

        assertTrue(result.contains("TG(18:0/14:2/4:0)"));
        assertFalse(result.contains("TG(18:1/13:0/5:1)"));
        assertTrue(result.contains("TG(18:1/14:1/4:0)"));

    }
}
