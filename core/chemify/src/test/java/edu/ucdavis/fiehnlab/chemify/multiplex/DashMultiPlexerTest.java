package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/30/14
 * Time: 3:49 PM
 */
public class DashMultiPlexerTest {
	Logger logger = Logger.getLogger(getClass());


	@Test
	public void testMultiplex() throws Exception {

		Multiplexer multiplexer = new DashMultiPlexer();

		String testString = "cytidine-5'-diphosphate";

		Set<String> result = multiplexer.multiplex(testString);

		if(logger.isDebugEnabled()) {
			logger.debug("resutls:" + result);
		}

		assertNotNull(result);

		assertTrue(result.contains("cytidine-5'-diphosphate"));
		assertTrue(result.contains("cytidine 5'-diphosphate"));
		assertTrue(result.contains("cytidine 5' diphosphate"));
	}

	@Test
	public void testDashes1() {
		Multiplexer multiplexer = new DashMultiPlexer();

		String testString = "N-acetyl-D-mannosamine";

		Set<String> result = multiplexer.multiplex(testString);

		if(logger.isDebugEnabled()) {
			logger.debug("resutls:" + result);
		}

		assertNotNull(result);

		assertEquals(3, result.size());
		assertTrue(result.contains("N-acetyl-D-mannosamine"));
		assertTrue(result.contains("N-acetyl D-mannosamine"));
		assertTrue(result.contains("N-acetyl D mannosamine"));
	}

	@Test
	public void testDashes2() {
		Multiplexer multiplexer = new DashMultiPlexer();

		String testString = "N-methylalanine";

		Set<String> result = multiplexer.multiplex(testString);

		if(logger.isDebugEnabled()) {
			logger.debug("resutls:" + result);
		}

		assertNotNull(result);

		assertEquals(1, result.size());
		assertTrue(result.contains("N-methylalanine"));
	}

	@Test
	public void testTooManyDashesSpaces() {
		Multiplexer multiplexer = new DashMultiPlexer();

		String testCombo = "4H 1-Benzopyran 4-one,-2 (3,4 dihydroxyphenyl)-3 (.beta.-D-glucopyranosyloxy)-2,3 dihydro-5,7 dihydroxy , (2R,3S)-";

		Set<String> exp = new HashSet();
		exp.add("4H 1-Benzopyran 4-one,-2 (3,4 dihydroxyphenyl)-3 (.beta.-D-glucopyranosyloxy)-2,3 dihydro-5,7 dihydroxy , (2R,3S)");
		exp.add("4H 1 Benzopyran 4 one, 2 (3,4 dihydroxyphenyl) 3 (.beta. D glucopyranosyloxy) 2,3 dihydro 5,7 dihydroxy , (2R,3S)");

		Set<String> res = multiplexer.multiplex(testCombo);

//		if(logger.isDebugEnabled()) {
			logger.info("Results: ");
			for(String name : res) {
				logger.info("\t" + name);
			}
//		}

		assertEquals(exp, res);
	}
}
