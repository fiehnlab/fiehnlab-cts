package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Before;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/24/13
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractAductDataModifierTest {
	Modify modify;

	@Before
	public void setUp() {
		modify = getModifier();

	}

	protected abstract AbstractAductDataModifier getModifier();
}
