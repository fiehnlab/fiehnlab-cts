package edu.ucdavis.fiehnlab.chemify.modify;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/24/13
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class NegativeAductModifierTest extends AbstractAductDataModifierTest {
	@Test
	public void testModifyNoChange() throws Exception {
		String result = modify.modify("Ceramide(16:0)");

		assertEquals("Ceramide(16:0)", result);
	}

	@Test
	public void testModifyFix1() throws Exception {
		String result = modify.modify("alanine [M+H]-");

		assertEquals("alanine", result);
	}

	@Test
	public void testModifyFix2() throws Exception {
		String result = modify.modify("Ceramide(16:0) [M+Na]+");

		assertEquals("Ceramide(16:0) [M+Na]+", result);
	}

	@Test
	public void testModifyFix3() throws Exception {
		String result = modify.modify("TG(16:0) [M+NH4]- ISTD");

		assertEquals("TG(16:0)", result);
	}

	@Test
	public void testGetPattern() throws Exception {
		assertEquals("(.*?)\\s?\\[M.*?\\]\\-.*", modify.getPattern());
	}

	protected AbstractAductDataModifier getModifier() {
		return new NegativeAductModifier();
	}
}
