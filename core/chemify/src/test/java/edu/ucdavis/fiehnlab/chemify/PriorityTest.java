package edu.ucdavis.fiehnlab.chemify;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 12:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class PriorityTest {

    @Test
    public void testCompare() throws Exception {

        List<Priority> priorityList = new Vector<Priority>();

        priorityList.add(Priority.CALCULATION_BASED);
        priorityList.add(Priority.EXACT_SEARCH_BASED);
        priorityList.add(Priority.HIGH);
        priorityList.add(Priority.HIGHEST);
        priorityList.add(Priority.LOW);
        priorityList.add(Priority.LOWEST);
        priorityList.add(Priority.MEDIUM);
        priorityList.add(Priority.NO_RESULT_POSSIBLE);
        priorityList.add(Priority.VERY_HIGH);
        priorityList.add(Priority.VERY_LOW);
        priorityList.add(Priority.WEB_SERVICE_BASED_EXTERNAL);
        priorityList.add(Priority.WEB_SERVICE_BASED_INTERNAL);

        Collections.sort(priorityList);

        System.out.println("list of priorities: " + priorityList);

        Assert.assertTrue(priorityList.get(0) == Priority.HIGHEST);
        Assert.assertTrue(priorityList.get(priorityList.size() - 1) == Priority.NO_RESULT_POSSIBLE);
        Assert.assertTrue(priorityList.get(priorityList.size() - 2) == Priority.LOWEST);
    }
}
