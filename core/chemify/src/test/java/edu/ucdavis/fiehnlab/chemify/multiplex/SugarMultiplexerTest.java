package edu.ucdavis.fiehnlab.chemify.multiplex;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;

public class SugarMultiplexerTest {

	ArrayList<String> expected;
	SugarMultiplexer sugaplex = new SugarMultiplexer();

	@Before
	public void setUp() {
		expected = new ArrayList<String>();
		expected.clear();
	}

	@Test
	public void testMultiplexGood1() throws Exception {
		expected.add("mannose");
		expected.add("mannopyranose");

		assertArrayEquals(expected.toArray(), sugaplex.multiplex("mannose").toArray());
	}

	@Test
	public void testMultiplexGood2() throws Exception {
		expected.add("mannose");
		expected.add("mannopyranose");

		assertArrayEquals(expected.toArray(), sugaplex.multiplex("mannopyranose").toArray());
	}

	@Test
	public void testMultiplexNonSugar() {
		expected.add("salt");

		assertArrayEquals(expected.toArray(), sugaplex.multiplex("salt").toArray());
	}
}