package edu.ucdavis.fiehnlab.chemify.modify;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/24/13
 * Time: 2:21 PM
 */
@Ignore("modifier disabled")
public class TrimNumberUnderscoreModifierTest {
    @Test
    @Ignore("modifier disabled")
    public void testModifyMatchesPattern() throws Exception {

        TrimNumberUnderscoreModifier modifier = new TrimNumberUnderscoreModifier();

        assertTrue("1_cholesterol".matches(modifier.getPattern()));
    }

    @Test
    @Ignore("modifier disabled")
    public void testModifyMatchesPattern2() throws Exception {

        TrimNumberUnderscoreModifier modifier = new TrimNumberUnderscoreModifier();

        assertTrue( "3_cholesterol".matches(modifier.getPattern()));
    }

    @Test
    @Ignore("modifier disabled")
    public void testModify() throws Exception {

        TrimNumberUnderscoreModifier modifier = new TrimNumberUnderscoreModifier();

        assertEquals( "cholesterol",modifier.modify("3_cholesterol"));
    }


}
