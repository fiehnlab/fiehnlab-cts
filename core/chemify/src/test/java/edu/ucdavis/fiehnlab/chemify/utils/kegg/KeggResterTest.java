package edu.ucdavis.fiehnlab.chemify.utils.kegg;

import junit.framework.TestCase;
import org.junit.Test;

public class KeggResterTest extends TestCase {

	@Test
	public void testGetNamesForKeggId() throws Exception {
		assert false == KeggRester.getNamesForKeggId("C00041").isEmpty();
	}

	@Test
	public void testGetKeggIdForName() throws Exception {
		assert false == KeggRester.getKeggIdForName("alanine").isEmpty();
	}
}