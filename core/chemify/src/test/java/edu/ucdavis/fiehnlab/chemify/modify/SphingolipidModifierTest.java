package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/18/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class SphingolipidModifierTest extends AbstractLipidModifierTest {
	Modify modify;

	@Before
	public void setUp() {
		modify = new SphingolipidModifier();

	}

	@Test
	public void testModifyCeramide1() throws Exception {

		String result = modify.modify("CeramideC16");

        assertEquals("Cer(18:1/16:0)", result);
	}

	@Test
	public void testModifyCeramide2() throws Exception {

		String result = modify.modify("Ceramide(16:0)");

		assertEquals("Cer(18:1/16:0)", result);
	}

	@Test
	public void testModifyCeramide3() throws Exception {

		String result = modify.modify("Ceramide 16:0");

		assertEquals("Cer(18:1/16:0)", result);
	}

	@Test
	public void testModifyCeramide4() throws Exception {

		String result = modify.modify("Cer(d18:1/16:0)");

		assertEquals("Cer(d18:1/16:0)", result);
	}

	@Test
	public void testModifySphing1() {
		String result = modify.modify("1_Sphingosine d17:1 [M+H]+ ISTD");
		assertEquals("Sphingosine(d17:1)", result);
	}

	@Test
	public void testNoCeramide() throws Exception {
		String result = modify.modify("TG(36:2)");
		assertEquals("TG(36:2)", result);
	}

	@Test
	public void testModifySM1() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("1_SM 17:0 [M+H]+ ISTD");

		assertEquals("SM(17:0)",result);
	}

	@Test
	public void testModifyMultiNestedParens() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("CE((22:6))");

		assertEquals("CE(22:6)",result);
	}

	@Override
	protected AbstractLipidModifier getModifier() {
		return new SphingolipidModifier();
	}

}
