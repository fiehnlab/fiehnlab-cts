package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:30 AM
 */
public class SPStrGenLipifyTest extends AbstractIdentifyTest
{
    @Override
    protected String getTestTerm() {
        return "SM(d18:0/16:0)";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new SPStrGenLipify();
    }
}
