package edu.ucdavis.fiehnlab.chemify.scoring;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diego on 8/22/14.
 */
public class TestScoringMapping implements Scoring {
	/**
	 * the score methods converts the result into a scored hit to reduce the amount of data output
	 *
	 * @param result
	 * @param searchTerm
	 * @return
	 */
	@Override
	public List<Scored> score(List<? extends Hit> result, String searchTerm) {
		List<Scored> list;
		list = new ArrayList<Scored>();

		Double score = result.size() + 0.0;

		for (Hit hit : result) {
			list.add(new ScoredHit(hit.getInchiKey(), hit.getUsedAlgorithm(), hit.getSearchTerm(), this, score--, hit.getOriginalTerm()));
		}
		return list;
	}

	/**
	 * am arbitrary number. The higher the value the higher the priority
	 *
	 * @return
	 */
	@Override
	public Priority getPriority() {
		return Priority.NO_RESULT_POSSIBLE;
	}

	@Override
	public int compareTo(Prioritize prioritize) {
		return 0;
	}

	/**
	 * the description of this object
	 *
	 * @return
	 */
	@Override
	public String getDescription() {
		return "This is used only for testing";
	}
}
