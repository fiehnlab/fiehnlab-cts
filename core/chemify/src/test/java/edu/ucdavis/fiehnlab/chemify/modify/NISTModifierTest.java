package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 4:02 PM
 */
public class NISTModifierTest {
    @Test
    public void testModify() throws Exception {

        Modify modify = new NISTModifier();

        assertEquals("alanine", modify.modify("alanine NIST"));
    }

    @Test
    public void testModify2() throws Exception {

        Modify modify = new NISTModifier();

        assertEquals("alanine", modify.modify("NIST alanine"));
    }

	@Test
	public void testModify3() throws Exception {

		Modify modify = new NISTModifier();

		assertEquals("alanine", modify.modify("-nist alanine"));
	}

	@Test
	public void testModify4() throws Exception {

		Modify modify = new NISTModifier();

		assertEquals("alanine", modify.modify("_nist alanine"));
	}

	@Test
	public void testModify5() throws Exception {

		Modify modify = new NISTModifier();

		assertEquals("alanine", modify.modify("alanine -nist"));
	}

	@Test
	public void testModify6() throws Exception {

		Modify modify = new NISTModifier();

		assertEquals("alanine", modify.modify("alanine _nist"));
	}

	@Test
	public void testModify7() throws Exception {

		Modify modify = new NISTModifier();

		assertEquals("alanine more", modify.modify("alanine NIST more"));
	}

	@Test
	public void testModify8() throws Exception {

		Modify modify = new NISTModifier();

		assertEquals("alanine more", modify.modify("alanine more NIST"));
	}

	@Test
	public void testModify9() throws Exception {

		Modify modify = new NISTModifier();

		assertEquals("alanine more more", modify.modify("alanine more nist more"));
	}
}
