package edu.ucdavis.fiehnlab.chemify.io;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.StringWriter;
import java.util.List;
import java.util.Vector;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/25/13
 * Time: 1:35 PM
 */
public class HitSerializerJSONImplTest {

	Logger logger = Logger.getLogger(getClass());

	@Test
	public void testSerialize() throws Exception {

		HitSerializerJSONImpl serializerJSON = new HitSerializerJSONImpl();
		StringWriter writer = new StringWriter();

		serializerJSON.serialize(writer, new HitImpl("a test", new NothingIdentifiedAtAll(), "no query", "no query"));

		JsonParser parser = new JsonParser();
		JsonElement object = parser.parse(writer.toString());

		assertTrue(object.isJsonObject());
		assertTrue(object.getAsJsonObject().has("result"));
		assertTrue(object.getAsJsonObject().has("query"));
		assertTrue(object.getAsJsonObject().has("algorithm"));
	}

	@Test
	public void testSerializeList() throws Exception {

		HitSerializerJSONImpl serializerJSON = new HitSerializerJSONImpl();
		StringWriter writer = new StringWriter();

		List<Hit> list = new Vector<Hit>();
		list.add(new HitImpl("a test", new NothingIdentifiedAtAll(), "no query", "no query"));

		serializerJSON.serialize(writer, list);

		JsonParser parser = new JsonParser();
		JsonElement object = parser.parse(writer.toString());

		assertTrue(object.isJsonObject());
		assertTrue(object.getAsJsonObject().get("result").getAsJsonArray().get(0).getAsJsonObject().has("result"));
		assertTrue(object.getAsJsonObject().get("result").getAsJsonArray().get(0).getAsJsonObject().has("query"));
		assertTrue(object.getAsJsonObject().get("result").getAsJsonArray().get(0).getAsJsonObject().has("algorithm"));
	}
}
