package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 3:56 PM
 */
public class SummarizedTGLipidIdentifyTest extends AbstractIdentifyTest {
    @Override
    protected String getTestTerm() {
        return "TG(6:0)";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new SummarizedTGLipidIdentify();
    }
}
