package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by diego on 7/27/15.
 */
public class LogicalComparatorModifierTest {

	@Test
	public void testModify() {
		Modify modify = new LogicalComparatorModifier();

		assertEquals("beer whiskey", modify.modify("beer and whiskey"));
	}
}
