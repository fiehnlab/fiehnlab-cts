package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by diego on 1/7/14.
 */
public class ArtifactModifierTest {

	@Test
	public void testModify() throws Exception {
		Modify modify = new ArtifactModifier();

		assertEquals("z mstfa", modify.modify("z mstfa artifact"));
	}
}
