package edu.ucdavis.fiehnlab.chemify.identify.pubchem;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/10/13
 * Time: 3:02 PM
 * To change this template use File | Settings | File Templates.
 */

public class PCNameIdentifyTest extends AbstractIdentifyTest {

	/**
	 * returns the actual term to be tested
	 *
	 * @return
	 */
	@Override
	protected String getTestTerm() {
		return "alanine";
	}

	/**
	 * implmementation needs to provide us with a new identify object
	 *
	 * @return
	 */
	@Override
	public AbstractIdentify getIdentify() {
		return new PCNameIdentify();
	}

}
