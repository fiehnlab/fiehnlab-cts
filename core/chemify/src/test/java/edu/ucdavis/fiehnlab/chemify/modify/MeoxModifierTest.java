package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 3:59 PM
 */
public class MeoxModifierTest {
	@Test
	public void testModifyMeoxAfter() throws Exception {

		Modify modify = new MeoxModifer();

		assertEquals("asparagine", modify.modify("asparagine_meox"));
	}

	@Test
	public void testModifyMeoxNumber() throws Exception {

		Modify modify = new MeoxModifer();

		assertEquals("pyrophosphate", modify.modify("pyrophosphate meox1"));
	}

	@Test
	public void testModifyUnderscoreMeox() throws Exception {

		Modify modify = new MeoxModifer();

		assertEquals("asparagine", modify.modify("asparagine_meox"));
	}

	@Test
	public void testModifyNonMeox() {

		Modify modify = new MeoxModifer();

		assertEquals("glucose", modify.modify("glucose non-meox"));
	}
}
