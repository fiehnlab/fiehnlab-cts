package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 4:05 PM
 */
public class SummarizedTGLipidIdentifyDoubleBoundTest extends SummarizedTGLipidIdentifyTest {
    @Override
    protected String getTestTerm() {
        return "TG(6:0)";
    }
}
