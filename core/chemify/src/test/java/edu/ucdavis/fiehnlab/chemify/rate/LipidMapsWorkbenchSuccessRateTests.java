package edu.ucdavis.fiehnlab.chemify.rate;

import org.junit.Ignore;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/31/14
 * Time: 2:34 PM
 */
@Ignore
public class LipidMapsWorkbenchSuccessRateTests extends WorkbenchSuccessRateTests {
	protected File getBaseDir() {
		return new File("src/test/resources/filesToRate/workbench/lipidmaps/");
	}

	/**
	 * lots of lipids which are causing issues
	 *
	 * @return
	 */
	protected double getMinimumScore() {
		return 0.58;
	}
}
