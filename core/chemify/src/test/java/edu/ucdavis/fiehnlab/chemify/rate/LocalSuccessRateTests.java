package edu.ucdavis.fiehnlab.chemify.rate;

import org.junit.Ignore;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/30/14
 * Time: 3:21 PM
 */
@Ignore
public class LocalSuccessRateTests extends SuccessRateTests {
    protected File getBaseDir() {
        return new File("src/test/resources/filesToRate/local/");
    }

    protected double getMinimumScore() {
        return 0.96;
    }
}
