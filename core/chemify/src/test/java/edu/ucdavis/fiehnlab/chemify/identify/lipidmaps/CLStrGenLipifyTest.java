package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;
import org.junit.Ignore;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:48 AM
 */
@Ignore
public class CLStrGenLipifyTest extends AbstractIdentifyTest {
    @Override
    protected String getTestTerm() {
        return "CL(1'-[18:2(9Z,12Z)/0:0],3'-[18:2(9Z,12Z)/0:0])";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new CLStrGenLipify();
    }
}
