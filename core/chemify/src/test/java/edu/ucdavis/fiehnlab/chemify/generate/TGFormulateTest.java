package edu.ucdavis.fiehnlab.chemify.generate;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/14/13
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class TGFormulateTest {
	TGFormulate formulate;

	/**
	 * Sets up the fixture, for example, open a network connection.
	 * This method is called before a test is executed.
	 */
	@Before
	public void setUp() throws Exception {
		formulate = new TGFormulate();
	}

	@Test
	public void testGetFormula() throws Exception {

		String res = formulate.getFormula("TG(54:4)");

		assertEquals("C57H102O6", res);

	}
}
