package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/14/13
 * Time: 4:02 PM
 */
public class MinorModifierTest {
	@Test
	public void testModify() throws Exception {
		Modify modify = new MinorModifier();

		assertEquals("isocitric acid", modify.modify("isocitric acid minor"));
	}
}
