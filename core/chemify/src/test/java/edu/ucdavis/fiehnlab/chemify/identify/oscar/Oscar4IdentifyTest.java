package edu.ucdavis.fiehnlab.chemify.identify.oscar;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 2:22 PM
 */
public class Oscar4IdentifyTest extends AbstractIdentifyTest {
    @Override
    protected String getTestTerm() {
        return "4-hydroxycinnamic acid";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new Oscar4Identify();
    }
}
