package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.junit.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CombinedMultiplexerTest {

	@Test
	public void testMultiplex() throws Exception {
		Multiplexer multiplexer = new CombinedMultiplexer();
		String testString = "l-glutamic acid";

		Set<String> exp = new HashSet<String>();
		exp.add("l-glutamic acid");
		exp.add("L-Glutamic-Acid");
		exp.add("L-Glutamic Acid");
		exp.add("l-glutamic-acid");

		Set<String> result = multiplexer.multiplex(testString);

		assertNotNull(result);

		Iterator i = result.iterator();

		assertTrue(result.containsAll(exp));
	}
}
