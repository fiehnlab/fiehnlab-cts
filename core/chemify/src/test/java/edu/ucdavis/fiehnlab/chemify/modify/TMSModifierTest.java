package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/14/13
 * Time: 4:02 PM
 */
public class TMSModifierTest {
	@Test
	public void testModify() throws Exception {

		Modify modify = new TMSModifier();

		assertEquals("cyclohexylamine", modify.modify("cyclohexylamine 1TMS"));
	}

	@Test
	public void testModify2() throws Exception {

		Modify modify = new TMSModifier();

		assertEquals("N-acetylaspartate", modify.modify("N-acetylaspartate TMS3"));
	}

	@Test
	public void testModify3() throws Exception {

		Modify modify = new TMSModifier();

		assertEquals("gylcine", modify.modify("gylcine TMS1x"));
	}

	@Test
	public void testModify4() throws Exception {

		Modify modify = new TMSModifier();

		assertEquals("gylcine", modify.modify("gylcine 1tms"));
	}
}
