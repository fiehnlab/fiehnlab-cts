package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:32 AM
 */
public class LMStrGenLipifyTest extends AbstractIdentifyTest {
    @Override
    protected String getTestTerm() {
        return "DIMA22(16:0/18:0)";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new LMStrGenLipify();
    }
}
