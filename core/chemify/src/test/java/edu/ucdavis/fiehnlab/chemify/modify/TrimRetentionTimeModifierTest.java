package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 3:59 PM
 */
public class TrimRetentionTimeModifierTest {
	Modify modify;

	@Before
	public void setup() {
		modify = new TrimRetentionTimeModifier();
	}

	@Test
	public void testModifyNull() throws Exception {

		assertEquals("", modify.modify(null));
	}

	@Test
	public void testModifyEmpty() throws Exception {

		assertEquals("", modify.modify(""));
	}

	@Test
	public void testModifyName1() throws Exception {

		assertEquals("Santene", modify.modify("4.66 Santene"));
	}

	@Test
	public void testModifyName2() throws Exception {

		assertEquals("Fenchyl acetate <endo->", modify.modify("16.57 Fenchyl acetate <endo->"));
	}

	@Test
	public void testModifyName3() throws Exception {

		assertEquals("Aconitic acid", modify.modify("Aconitic acid 685 s"));
	}

	@Test
	public void testModifyName4() throws Exception {

		assertEquals("Glucoheptonic acid major", modify.modify("Glucoheptonic acid major 888.75"));
	}

	@Test
	public void testModifyName5() throws Exception {

		assertEquals("glucoheptonic acid, minor peak", modify.modify("glucoheptonic acid, minor peak 811.35 s"));
	}

	@Test
	public void testModifyName6() throws Exception {

		assertEquals("4-Hydroxybenzoic acid", modify.modify("4-Hydroxybenzoic acid 604 s"));
	}

	@Test
	public void testModifyName7() throws Exception {

		assertEquals("2(?)-hydroxybutanoic acid", modify.modify("2(?)-hydroxybutanoic acid 380.36 s"));
	}

	@Test
	public void testModifyName8() throws Exception {

		assertEquals("succinate", modify.modify("succinate 469.68 s"));
	}

	@Test
	public void testModifyName9() throws Exception {

		assertEquals("Pyruvic acid meox", modify.modify("Pyruvic acid meox 318.70 s"));
	}

	@Test
	public void testModifyName10() throws Exception {

		assertEquals("2-Oxoisocaproic acid meox1", modify.modify("2-Oxoisocaproic acid meox1 392.47s"));
	}

	@Test
	public void testModifyName11() throws Exception {

		assertEquals("2-Oxoisovaleric acid meox2", modify.modify("2-Oxoisovaleric acid meox2 367.05 s"));
	}

	@Test
	public void testModifyName12() throws Exception {

		assertEquals("ascorbic acid minor peak", modify.modify("ascorbic acid minor peak 731.12s"));
	}

	@Test
	public void testModifyName13() throws Exception {

		assertEquals("Tryptophane 1x TMS", modify.modify("Tryptophane 1x TMS 865.93 s"));
	}
}
