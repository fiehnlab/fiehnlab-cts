package edu.ucdavis.fiehnlab.chemify.helper;

/**
 * Created by diego on 8/15/14.
 */
public class TestConstantsRegistry {
	public static final String CTS_BASE_URL = "http://localhost:8888";
	public static final String CTS_SCORING_URL = CTS_BASE_URL + "/service/score";
}
