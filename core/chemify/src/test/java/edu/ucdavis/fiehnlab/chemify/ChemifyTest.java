package edu.ucdavis.fiehnlab.chemify;

import edu.ucdavis.fiehnlab.chemify.configuration.SimpleConfiguration;
import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll;
import edu.ucdavis.fiehnlab.chemify.scoring.TestScoringMapping;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 8:47 PM
 * <p/>
 * Tests the chemify test in general
 */
public class ChemifyTest {

	protected Chemify chemify;
	protected Logger logger = Logger.getLogger(this.getClass());

	@Before
	public void setUp() {
		this.chemify = this.getChemify();

		if (logger.isDebugEnabled())
			logger.debug(this.chemify.getDefaultConfiguration().toString());
	}

	@After
	public void tearDown() {
		this.chemify = null;
	}

	@Test
	public void testIdentifyForAlanine() throws Exception {
		List<? extends Hit> results = chemify.identify("alanine");
		assertTrue(results.contains(new HitImpl("QNAYBMKLOCPYGJ-REOHCLBHSA-N", new TestIdentifyMapper(), "alanine", "alanine")));
	}

	@Test
	public void testIdentifyForPalmeticAcid() throws Exception {

		List<? extends Hit> results = chemify.identify("palmitic acid");

		//this is the wrong result due to wrong data in the cts
		assertFalse(results.contains(new HitImpl("XBDQKXXYIPTUBI-UHFFFAOYSA-N", new TestIdentifyMapper(), "palmitic acid", "palmitic acid")));
	}

	@Test
	public void testIdentifyForOctanoicAcidMethylEster() throws Exception {
//		String keyword ="methyl octanoate";
		String keyword = "octanoic acid methyl ester";
		List<? extends Hit> results = chemify.identify(keyword);
		assertTrue(results.contains(new HitImpl("JGHZJRVDZXSNKQ-UHFFFAOYSA-N", new TestIdentifyMapper(), keyword, keyword)));

	}

    @Test
    @Ignore
    public void testIdentifyForDG1() throws Exception {
        String term = "DG(16:0/16:0/0:0)";
        List<? extends Hit> results = chemify.identify(term);
        Scored item = new ScoredHit(new HitImpl("JEJLGIQLPYYGEE-XIFFEERXSA-N", new TestIdentifyMapper(), "DG(16:0/16:0/0:0)", term), new TestScoringMapping(), 1.0);
        assertTrue(results.contains(item));

    }

    @Test
    @Ignore
    public void testIdentifyForDG2() throws Exception {
        String term = "DG (16:0/0:0/16:0)";
        List<? extends Hit> results = chemify.identify(term);

        Scored item = new ScoredHit(new HitImpl("GFAZGHREJPXDMH-UHFFFAOYSA-N", new TestIdentifyMapper(), "DG(16:0/0:0/16:0)", term), new TestScoringMapping(), 0.0);
        assertEquals(1, results.size());
        assertTrue(results.contains(item));
    }


    @Test
    @Ignore
    public void testIdentifyForDG3() throws Exception {
        String term = "DG 16:0/0:0/16:0";
        List<? extends Hit> results = chemify.identify(term);

        Scored item = new ScoredHit(new HitImpl("GFAZGHREJPXDMH-UHFFFAOYSA-N", new TestIdentifyMapper(), "DG(16:0/0:0/16:0)", "DG (16:0/0:0/16:0)"), new TestScoringMapping(), 0.0);

        assertEquals(1, results.size());
        assertTrue(results.contains(item));
    }

	@Test
	public void testIdentifyFor1_CholesterolD7AdductISTD() throws Exception {

		List<? extends Hit> results = chemify.identify("Cholesterol d7 [M-H2O+H]+ ISTD");  // because the numberUnderscore modifier was disabled
		assertTrue(results.contains(new HitImpl("HVYWMOMLDIMFJA-IFAPJKRJSA-N", new TestIdentifyMapper(), "Cholesterol-d7", "Cholesterol d7 [M-H2O+H]+ ISTD")));

	}

	@Test
	public void testIdentifyForNoneExistingEntryWithConfigurationPermittingEmptyResults() throws Exception {

		//ensure that our configuration allows this
		assertTrue(chemify.getDefaultConfiguration().isNonIdentifiedResultsPermitted());

		List<? extends Hit> results = chemify.identify("beer and whiskey");

		assertEquals(1, results.size());

		assertTrue(results.get(0).getUsedAlgorithm() instanceof NothingIdentifiedAtAll);
	}

	@Test
	public void testIdentifyForNoneExistingEntryWithConfigurationNotPermittingEmptyResults() throws Exception {

		SimpleConfiguration configuration = (SimpleConfiguration) chemify.getDefaultConfiguration();
		configuration.setNonIdentifiedResultsPermitted(false);

		assertFalse(chemify.getDefaultConfiguration().isNonIdentifiedResultsPermitted());

		List<? extends Hit> results = chemify.identify("beer and whiskey");

		assertTrue(results.size() == 0);
	}

	@Test
	public void testIdentifyForNull() throws Exception {
		try {
			List<? extends Hit> result = chemify.identify(null);

			fail("we should have caught an exception!");
		} catch (IllegalArgumentException e) {
			//expected
		}
	}

	@Test
	public void testIdentifyForEmptyString() throws Exception {
		try {
			List<? extends Hit> result = chemify.identify("");

			fail("we should have caught an exception!");
		} catch (IllegalArgumentException e) {
			//expected
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIdentifyForEmpty() throws Exception {
		List<? extends Hit> result = chemify.identify(null);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testIdentifyForValidInChiCode() throws Exception {
		List<? extends Hit> result = chemify.identify(null);
	}

	@Test
	public void testIdentifyForValidSumarizedTG() {
		String term = "TG(56:4)";
		List<Hit> exp = new ArrayList<Hit>();
		exp.add(new ScoredHit(new HitImpl("YONCDTJKIZDSKQ-IYASBODOSA-N", new TestIdentifyMapper(), term, term), new TestScoringMapping(), 1.0));

		List<? extends Hit> results = chemify.identify("TG(56:4)");

		assertEquals(exp, results);
	}

	@Test
	public void testMultipleTokens1() throws Exception {
		List<? extends Hit> exp = chemify.identify("alanine");

		List<? extends Hit> results = chemify.identify("alanine 1TMS minor");
		assertEquals(exp, results);
	}

	@Test
	public void testMultipleTokens2() throws Exception {
		List<? extends Hit> exp = chemify.identify("alanine");

		List<? extends Hit> results = chemify.identify("alanine minor 1TMS");
		assertEquals(exp, results);
	}

	@Ignore("doesn't make sense anymore")
	@Test
	public void testEmptyResult() throws Exception {
		List<Hit> exp = new ArrayList<Hit>();
		exp.add(new ScoredHit(
				new HitImpl("DZMNWPSOYTUVJQ-UHFFFAOYSA-N",
						new TestIdentifyMapper(),
						"trihydroxipirazine",
						"trihydroxipyrazine").addEnhancement(
						new Enhancement("ERROR", "Error matching scored results to identified results.\n Identified InChIKey: YMRCYAMOHVNOSM-UHFFFAOYSA-N\n Scored InChIKey: DZMNWPSOYTUVJQ-UHFFFAOYSA-N")),
				new TestScoringMapping(), 0.0));
//		exp.add(new ScoredHit(new HitImpl("Error matching scored results to identified results.\n" +
//				"Identified InChIKey: YMRCYAMOHVNOSM-UHFFFAOYSA-N\n" +
//				"Scored InChIKey: DZMNWPSOYTUVJQ-UHFFFAOYSA-N", new TestIdentifyMapper(), "trihydroxipirazine", "trihydroxipyrazine"), new ScoreByBiologicalCount(TestConstantsRegistry.CTS_SCORING_URL), 0.0));

		List<? extends Hit> results = chemify.identify("trihydroxypyrazine");
		logger.debug("IDENTIFIED: " + results);
		assertEquals(exp.get(0).getInchiKey(), results.get(0).getInchiKey());
	}

	@Test
	public void testBromopropanolSimple() {
		String term = "bromopropanol";

		List<? extends Hit> res = chemify.identify(term);

		assert res.get(0).getInchiKey().matches("(?:JCERKCRUSDOWLT|RQFUZUMFPRMVDX)-UHFFFAOYSA-N");
	}

	@Test
	public void testBromopropanolBug() {
		String term = "z bromopropanol artifact";

		List<? extends Hit> res = chemify.identify(term);

		assert res.get(0).getInchiKey().matches("(?:JCERKCRUSDOWLT|RQFUZUMFPRMVDX)-UHFFFAOYSA-N");
	}

	@Test
	public void testComplexName1() {
		String term = "N-acetyl L glutamic acid";
		List<Hit> exp = new ArrayList<Hit>();
		exp.add(new ScoredHit(new HitImpl("RFMMMVDNIPUKGG-UHFFFAOYSA-N", new TestIdentifyMapper(), term, term), new TestScoringMapping(), 0.0));

		List<? extends Hit> res = chemify.identify(term);

		assertEquals(exp.get(0).getInchiKey().split("-")[0], res.get(0).getInchiKey().split("-")[0]);
	}

	@Test
	public void testComplexName2() {
		String term = "N-acetyl-L-glutamic acid TMS3";
		List<Hit> exp = new ArrayList<Hit>();
		exp.add(new ScoredHit(new HitImpl("RFMMMVDNIPUKGG-UHFFFAOYSA-N", new TestIdentifyMapper(), term, term), new TestScoringMapping(), 0.0));

		List<? extends Hit> res = chemify.identify(term);

		assertEquals(exp.get(0).getInchiKey().split("-")[0], res.get(0).getInchiKey().split("-")[0]);
	}

	/**
	 * returns our chemify instance to be used
	 *
	 * @return
	 */
	protected Chemify getChemify() {
		ApplicationContext ctx = new FileSystemXmlApplicationContext("src/test/resources/defaultTestConf.xml");
		return new Chemify((Configuration) ctx.getBean("defaultConfiguration"));
	}
}

