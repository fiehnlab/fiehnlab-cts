package edu.ucdavis.fiehnlab.chemify.identify.pubchem;

import edu.ucdavis.fiehnlab.chemify.Chemify;
import edu.ucdavis.fiehnlab.chemify.Scoring;
import edu.ucdavis.fiehnlab.chemify.configuration.SimpleConfiguration;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractValidationTest;
import edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone;
import edu.ucdavis.fiehnlab.chemify.statistics.TestDataCollector;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;

import java.util.Vector;

/**
 * Created by diego on 9/29/14.
 */

@Ignore
public class PubchemValidationTest extends AbstractValidationTest {

	@BeforeClass
	public static void setUp() {
		TestDataCollector.getInstance().setTester(PubchemValidationTest.class.getSimpleName());
		Logger.getLogger(PubchemValidationTest.class).debug(PubchemValidationTest.class.getSimpleName());
		Logger.getLogger(PubchemValidationTest.class).debug(TestDataCollector.getInstance().getTester());
	}

	public Chemify getChemify() {
		SimpleConfiguration configuration = new SimpleConfiguration();
		Vector scoring = new Vector<Scoring>();
		scoring.add(new NoScoringDone());
		configuration.setScoringAlgorithms(scoring);

		this.configureConfiguration(configuration);

//		configuration.setAlgorithms(new ArrayList<Identify>());
		configuration.addAlgorithm(new PCNameIdentify());

		return new Chemify(configuration);
	}
}
