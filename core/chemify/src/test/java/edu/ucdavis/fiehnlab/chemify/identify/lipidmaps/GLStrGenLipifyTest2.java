package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 1:38 PM
 */
public class GLStrGenLipifyTest2 extends GLStrGenLipifyTest {

    @Override
    protected String getTestTerm() {
        return "TG(34:0/10:0/12:0)";
    }
}
