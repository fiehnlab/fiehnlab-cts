package edu.ucdavis.fiehnlab.chemify.identify;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.configuration.SpringConfiguration;
import edu.ucdavis.fiehnlab.chemify.identify.fiehnlab.LipidIdentifyFile;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class LipidsIdentifyTest {
	Logger logger = Logger.getLogger(getClass().getName());
	AbstractIdentify identify;

	@Before
	public void setUp() {
		identify = getIdentify();
		identify.setConfiguration(new SpringConfiguration("defaultTestConf.xml"));
	}

	@Test
	public void testIdentify() {
		String[] dgs = {"DG 16:0/0:0/16:0", "DG (16:0/0:0/16:0)", "DG(16:0/16:0/0:0)"};
		for (String s : dgs) {
			List<Hit> result = identify.identify(s, s);

			logger.info("result: " + result);

			assert result.isEmpty();
		}
	}

	public AbstractIdentify getIdentify() {
		return new LipidIdentifyFile();
	}
}
