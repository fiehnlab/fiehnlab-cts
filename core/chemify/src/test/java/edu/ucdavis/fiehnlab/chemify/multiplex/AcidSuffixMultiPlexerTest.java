package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/30/14
 * Time: 3:49 PM
 */
public class AcidSuffixMultiPlexerTest {


	@Test
	public void testMultiplex() throws Exception {

		Multiplexer multiplexer = new AcidSuffixMultiPlexer();

		String testString = "threonic acid";

		Set<String> result = multiplexer.multiplex(testString);

		assertNotNull(result);

		assertTrue(result.contains("threonic acid"));
		assertTrue(result.contains("threonate"));
	}
}
