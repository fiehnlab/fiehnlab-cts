package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:22 AM
 */
public class STStrGenLipifyTest extends AbstractIdentifyTest {
    @Override
    protected String getTestTerm() {
        return "CHOLESTANE(3,b,OH/5,b,Ep)";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new STStrGenLipify();
    }
}
