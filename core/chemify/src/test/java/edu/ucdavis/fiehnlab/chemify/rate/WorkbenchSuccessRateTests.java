package edu.ucdavis.fiehnlab.chemify.rate;

import org.junit.Ignore;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/30/14
 * Time: 2:35 PM
 */
@Ignore
public class WorkbenchSuccessRateTests extends SuccessRateTests {
	protected File getBaseDir() {
		return new File("src/test/resources/filesToRate/workbench/");
	}

	protected double getMinimumScore() {
		return 0.88;
	}
}
