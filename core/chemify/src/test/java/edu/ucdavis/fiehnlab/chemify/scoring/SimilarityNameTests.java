package edu.ucdavis.fiehnlab.chemify.scoring;


import edu.ucdavis.fiehnlab.chemify.Configuration;
import edu.ucdavis.fiehnlab.chemify.Modify;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.*;

import static org.junit.Assert.assertFalse;


/**
 * Created by diego on 5/28/14.
 */
public class SimilarityNameTests {
	Logger logger = Logger.getLogger(this.getClass());
	ScoreBySimilarity scorer;
	Configuration conf;

	private static final String nameList = "src/test/resources/similarityFiles/similNames.txt";
	private static final String resultsList = "src/test/resources/similarityFiles/similScores.txt";

	@Before
	public void setup() {
		scorer = new ScoreBySimilarity();
		ApplicationContext ctx = new FileSystemXmlApplicationContext("src/test/resources/defaultTestConf.xml");
		conf = (Configuration) ctx.getBean("defaultConfiguration");
	}

	@Test
	public void scoreNames() {

		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(nameList)));

			File of = new File(resultsList);

			FileWriter out = new FileWriter(of);
			out.write("original name" + "\t" + "modified name" + "\t" + "% change" + "\n");

			String name = "";
			while ((name = reader.readLine()) != null) {
				name = name.trim();

				String fixed = modifySearchTerm(name, conf);
				Double dif = scorer.score(name, fixed);

				out.write(name + "\t" + fixed + "\t" + dif + "\n");
			}

			out.flush();
			out.close();
			reader.close();

			reader = new BufferedReader(new FileReader(new File(resultsList)));
			StringBuffer text = new StringBuffer("");
			while(reader.ready()) {
				text.append(reader.readLine());
			}

			assertFalse(text.toString().contains("1-hexadecanol\\t1-hexadecanol\\t1.0"));
			assertFalse(text.toString().contains("z artifact\\tz\\t0.099"));

		} catch (FileNotFoundException e) {
			System.out.println("Can't find names file " + nameList + "\n" + e.getMessage());
		} catch (IOException e) {
			System.out.println("Can't crerate results file.\n" + e.getMessage());
		}

	}

	private String modifySearchTerm(String name, Configuration configuration) {
		//modify our search term
		for (Modify modify : configuration.getModifications()) {

			//only name's which match the given pattern should be modified
			if (name.matches(modify.getPattern()))
				name = modify.modify(name);
		}
		return name;
	}
}
