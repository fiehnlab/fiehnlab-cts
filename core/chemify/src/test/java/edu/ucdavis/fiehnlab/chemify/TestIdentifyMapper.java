package edu.ucdavis.fiehnlab.chemify;

import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/9/13
 * Time: 2:13 PM
 * <p/>
 * just for testing and mocking nothing else
 */
public class TestIdentifyMapper extends AbstractIdentify {
	/**
	 * sets the internal priority of this identity object
	 */
	public TestIdentifyMapper() {
		super(Priority.NO_RESULT_POSSIBLE);
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {
        return new ArrayList<>();
	}

	public String getDescription() {
		return "this method is used for testing only";
	}
}
