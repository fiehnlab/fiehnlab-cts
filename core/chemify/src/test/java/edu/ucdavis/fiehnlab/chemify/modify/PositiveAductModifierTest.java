package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/24/13
 * Time: 2:27 PM
 */
public class PositiveAductModifierTest extends AbstractAductDataModifierTest {
	Modify modify;

	@Before
	public void setUp() {
		modify = getModifier();
	}

	@Override
	protected AbstractAductDataModifier getModifier() {
		return new PositiveAductModifier();
	}

	@Test
	public void testModifyNoChange() throws Exception {
		String result = modify.modify("Ceramide(16:0)");

		assertEquals("Ceramide(16:0)", result);
	}

	@Test
	public void testModifyFix1() throws Exception {
		String result = modify.modify("alanine [M+H]+");

		assertEquals("alanine", result);
	}

	@Test
	public void testModifyFix2() throws Exception {
		String result = modify.modify("Ceramide(16:0) [M+Na]+");

		assertEquals("Ceramide(16:0)", result);
	}

	@Test
	public void testModifyFix3() throws Exception {
		String result = modify.modify("TG(16:0) [M+NH4]+ ISTD");

		assertEquals("TG(16:0)", result);
	}

}
