package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by diego on 1/28/14.
 */
public class ParensStarModifierTest {

	@Test
	public void testModify() throws Exception {
		Modify modify = new ParensStarModifier();

		assertEquals("15(S)-HETE", modify.modify("15(S)-HETE (*)"));
		assertEquals("15(S)(*)-HETE", modify.modify("15(S)(*)-HETE"));
		assertEquals("(*)15(S)-HETE", modify.modify("(*)15(S)-HETE"));
	}
}
