package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:43 AM
 */
public class FAStrGenLipifyTest extends AbstractIdentifyTest {
    @Override
    protected String getTestTerm() {
	    return "12:0(2NH2)";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new FAStrGenLipify();
    }
}
