package edu.ucdavis.fiehnlab.chemify.identify;

import edu.ucdavis.fiehnlab.chemify.Chemify;
import edu.ucdavis.fiehnlab.chemify.ChemifyTest;
import edu.ucdavis.fiehnlab.chemify.Configuration;
import org.junit.Ignore;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/15/13
 * Time: 12:44 PM
 */

@Ignore
public class CombinedIdentifyTest extends ChemifyTest {

    @Override
    protected Chemify getChemify() {
	    ApplicationContext ctx = new FileSystemXmlApplicationContext("src/test/resources/defaultTestConf.xml");
	    return new Chemify((Configuration) ctx.getBean("defaultConfiguration"));
    }
}
