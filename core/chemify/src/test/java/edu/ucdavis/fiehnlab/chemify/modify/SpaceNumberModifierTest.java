package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by diego on 1/7/14.
 */
public class SpaceNumberModifierTest {
	@Test
	public void testModify() throws Exception {
		Modify modify = new SpaceNumberModifier();

		assertEquals("fucose", modify.modify("fucose 2"));
	}

	@Test
	public void testModify2() throws Exception {
		Modify modify = new SpaceNumberModifier();

		assertEquals("fructose", modify.modify("fructose-2"));
	}
}
