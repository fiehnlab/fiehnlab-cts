package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/18/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class PhospholipidModifierTest extends AbstractLipidModifierTest {


	/**
	 * TODO no idea how to do this yet
	 * @throws Exception
	 */
	@Test
	public void testModifyPC1() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("PC (16:0/20:4) (OH[S])");

		assertEquals("PC(16:0/20:4) (OH[S])",result);
	}


	/**
	 * TODO no idea how to do this yet
	 * @throws Exception
	 */
	@Test
	public void testModifyPC2() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("PC (16:0/9:0(CHO))");

		assertEquals("PC(16:0/9:0(CHO))",result);
	}

	@Test
	public void testModifyPC3() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("1_PC 12:0/13:0 [M+H]+ ISTD");

		assertEquals("1_PC(12:0/13:0)",result);
	}

	@Test
	public void testModifyLPC1() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("LPC (16:0) [M+H]+");

		assertEquals("LPC(16:0)",result);
	}

	@Test
	public void testModifyLysoPC1() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("LysoPC (16:0) [M+H]+");

		assertEquals("LPC(16:0)",result);
	}

	@Test
	public void testPlasmenylPC1() throws Exception {
		Modify modify = getModifier();

		String result = modify.modify("plasmenyl-PC 36:4 [M+H]+");

		assertEquals("Plasmenyl-PC(36:4)",result);
	}

	@Test
	public void testPlasmenylPE1() throws Exception {
		Modify modify = getModifier();

		String result = modify.modify("Plasmenyl-PE (36:4) [M+H]+");

		assertEquals("Plasmenyl-PE(36:4)",result);
	}

	@Test
	public void testPlasmenylPE2() throws Exception {
		Modify modify = getModifier();

		String result = modify.modify("pPE (36:4) [M+H]+");

		assertEquals("Plasmenyl-PE(36:4)",result);
	}

	@Test
	public void testPlasmenylPE3() throws Exception {
		Modify modify = getModifier();

		String result = modify.modify("oPE (36:4) [M+H]+");

		assertEquals("Plasmenyl-PE(36:4)",result);
	}

	@Test
	public void testPEe1() throws Exception {
		Modify modify = getModifier();

		String res = modify.modify("PE(40:7e)");

		assertEquals("PE(40:7e)", res);
	}

	@Test
	public void testPEe2() throws Exception {
		Modify modify = getModifier();

		String res = modify.modify("PE(14:3/16:4e)");

		assertEquals("PE(14:3/16:4e)", res);
	}

	@Test
	public void testPEe3() throws Exception {
		Modify modify = getModifier();

		String res = modify.modify("PE(14:3e/16:4)");

		assertEquals("PE(14:3e/16:4)", res);
	}

	@Test
	public void testPEe4() throws Exception {
		Modify modify = getModifier();

		String res = modify.modify("PE(42:6p)");

		assertEquals("PE(42:6p)", res);
	}

	@Test
	public void testPCe1() {
		Modify modify = getModifier();

		String res = modify.modify("PC(36:4e)/PC(36:3p)");

		assertEquals("PC(36:4e)", res);
	}

	@Test
	public void testLongPCName1() {
		Modify modify = getModifier();

		String res = modify.modify("Phosphatidylcholine 18:0-20:0");

		assertEquals("PC(18:0/20:0)", res);
	}

	@Test
	public void testLongPCName2() {
		Modify modify = getModifier();

		String res = modify.modify("Phosphatidylcholine 18:0/20:0");

		assertEquals("PC(18:0/20:0)", res);
	}

	@Test
	public void testLongPCName3() {
		Modify modify = getModifier();

		String res = modify.modify("PC 18:0-20:0");

		assertEquals("PC(18:0/20:0)", res);
	}

	@Override
	protected AbstractLipidModifier getModifier() {
		return new PhospholipidModifier();
	}
}
