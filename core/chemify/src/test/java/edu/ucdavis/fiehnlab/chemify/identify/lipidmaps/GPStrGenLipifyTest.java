package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:41 AM
 */
public class GPStrGenLipifyTest extends AbstractIdentifyTest
{
    @Override
    protected String getTestTerm() {
        return "PC(16:0/0:0)";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new GPStrGenLipify();
    }
}
