package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 3:59 PM
 */
public class DehydratedModifierTest {
	@Test
	public void testModifyDehydAfter() throws Exception {

		Modify modify = new DehydratedModifier();

		assertEquals("alanine", modify.modify("alanine dehydrated"));
	}

	@Test
	public void testModifyDehydBefore() throws Exception {

		Modify modify = new DehydratedModifier();

		assertEquals("alanine", modify.modify("dehydrated alanine"));
	}

	@Test
	public void testModifyDehydParensAfter() throws Exception {

		Modify modify = new DehydratedModifier();

		assertEquals("alanine", modify.modify("alanine (dehydrated)"));
	}

	@Test
	public void testModifyDehydParensBefore() throws Exception {

		Modify modify = new DehydratedModifier();

		assertEquals("alanine", modify.modify("(dehydrated) alanine"));
	}

	@Test
	public void testModifyDehydBetweenStrings() throws Exception {

		Modify modify = new DehydratedModifier();

		Logger.getLogger(this.getClass()).info(modify.modify("alanine dehydrated"));

		assertEquals("alanine  methil ester", modify.modify("alanine dehydrated methil ester"));
	}
}
