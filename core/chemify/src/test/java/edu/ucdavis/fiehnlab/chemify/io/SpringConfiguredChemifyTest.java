package edu.ucdavis.fiehnlab.chemify.io;

import edu.ucdavis.fiehnlab.chemify.Chemify;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.TestIdentifyMapper;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll;
import edu.ucdavis.fiehnlab.chemify.scoring.ScoreBySimilarity;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/30/13
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class SpringConfiguredChemifyTest {

	Logger logger = Logger.getLogger(this.getClass());
	Chemify chemify;

	@Before
	public void setUp() throws Exception {
		chemify = new SpringConfiguredChemify();
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testIt() {
		List<? extends Hit> res = chemify.identify("alanine");

		assert res.size() > 0;

		for (Hit h : res) {
			logger.debug(h.toString());
		}
	}

	@Test
	public void testEmptyModifiedTerm() {
		String term = "610256";
		HitImpl nothing = new HitImpl("invalid modified search term", new NothingIdentifiedAtAll(), term, term);
		List<Hit> exp = new ArrayList<Hit>();
		exp.add(nothing);

		List<? extends Hit> results = chemify.identify(term);

		assert results.size() == 1;
		assertEquals(exp, results);
	}

	@Test
	public void testIdentifyDisctinct() {
		String term = "alanine";
		List<Hit> exp = new ArrayList<Hit>();
		exp.add(new ScoredHit(new HitImpl("QNAYBMKLOCPYGJ-REOHCLBHSA-N", new TestIdentifyMapper(), term, term), new ScoreBySimilarity(), 1.0));
		exp.add(new ScoredHit(new HitImpl("QNAYBMKLOCPYGJ-UWTATZPHSA-N", new TestIdentifyMapper(), term, term), new ScoreBySimilarity(), 1.0));
		exp.add(new ScoredHit(new HitImpl("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", new TestIdentifyMapper(), term, term), new ScoreBySimilarity(), 1.0));

		List<? extends Hit> results = chemify.identify(term);

		assert (exp.size() <= results.size());
		assert (results.contains(exp.get(0)));
		assert (results.contains(exp.get(1)));
		assert (results.contains(exp.get(2)));
	}

	@Test
	public void testQualityValue() {
		assertTrue(0.25 == chemify.getDefaultConfiguration().getSearchTermMinQuality());
	}
}
