package edu.ucdavis.fiehnlab.chemify.configuration;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 12:19 PM
 */
public abstract class AbstractConfigurationTest extends TestCase {
	@Test
	public void testGetAlgorithms() throws Exception {
		Configuration configuration = getConfiguration();

		assert (configuration.getAlgorithms() != null);
	}

	@Test
	public void testAddAlgorithm() throws Exception {

		Configuration configuration = getConfiguration();

		int size = getConfiguration().getAlgorithms().size();
		assert (configuration.addAlgorithm(new PCNameIdentify()).getAlgorithms().size() == size + 1);

	}

	protected abstract Configuration getConfiguration();

	@Test
	public void testAddFilter() throws Exception {

		//ain't a filter but needed for testing
		Filter filter = new Filter() {
			public List<Scored> filter(List<? extends Scored> input) {
				return (List<Scored>) input;
			}

			public Priority getPriority() {
				return Priority.MEDIUM;
			}

			public int compareTo(Prioritize prioritize) {
				return 0;
			}

			public String getDescription() {
				return "this filter is used for testing only";
			}
		};

		Configuration configuration = getConfiguration();

		assert (configuration.getFilters().isEmpty());

		configuration.addFilter(filter);

		assert (configuration.getFilters().isEmpty() == false);
	}

	@Test
	public void testHasScoring() {
		assertTrue(getConfiguration().getScoringAlgorithms() != null);
	}

	@Test
	public void testAccessEnhancements() {
		assertTrue(getConfiguration().getEnhances() != null);
	}

	@Test
	public void testAccessFilter() {
		assertTrue(getConfiguration().getFilters() != null);
	}

	@Test
	@Ignore
	public void testGetQuality() {
		assertEquals(0.3, getConfiguration().getSearchTermMinQuality());
	}
}
