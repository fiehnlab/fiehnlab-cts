package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 3:59 PM
 */
public class ISTDModifierTest {
    @Test
    public void testModify() throws Exception {

        Modify modify = new ISTDModifier();

        assertEquals(modify.modify("alanine ISTD"), "alanine");
    }

    @Test
    public void testModify2() throws Exception {

        Modify modify = new ISTDModifier();

        assertEquals(modify.modify("ISTD alanine"), "ISTD alanine");
    }
}
