package edu.ucdavis.fiehnlab.chemify.filter;

import edu.ucdavis.fiehnlab.chemify.Filter;
import edu.ucdavis.fiehnlab.chemify.Scored;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify;
import edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 9:58 PM
 * <p/>
 * Tests our TopNFilter
 */
public class TopNFilterTest extends TestCase {
	@Test
	public void testFilterSuccess() throws Exception {

		List<Scored> data = new Vector<Scored>();

		for (int i = 0; i < 10; i++) {
			data.add(new ScoredHit("none", new PCNameIdentify(), "none", new NoScoringDone(), new Double(i / 10.0), "none"));
		}

		Filter filter = new TopNFilter(4);

		List<Scored> result = filter.filter(data);

		assertTrue(result.size() == 4);
		assertTrue(result.get(0).getScore() == 0.0);
		assertTrue(result.get(1).getScore() == 0.1);
		assertTrue(result.get(2).getScore() == 0.2);
		assertTrue(result.get(3).getScore() == 0.3);

	}
}
