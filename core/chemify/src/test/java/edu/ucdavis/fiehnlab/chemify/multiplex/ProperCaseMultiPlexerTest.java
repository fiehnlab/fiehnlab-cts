package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.junit.Test;

import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/30/14
 * Time: 3:49 PM
 */
public class ProperCaseMultiPlexerTest {


	@Test
	public void testMultiplex() throws Exception {

		Multiplexer multiplexer = new ProperCaseMultiPlexer();

		String testString = "n acetyl l glutamic acid";

		Set<String> result = multiplexer.multiplex(testString);

		assertNotNull(result);

		Iterator i = result.iterator();
		while (i.hasNext()) {
			System.out.println("Multiplexed: '" + i.next() + "'");
		}
		assertTrue(result.contains("N Acetyl L Glutamic Acid"));
		assertTrue(result.contains("n acetyl l glutamic acid"));
	}
}
