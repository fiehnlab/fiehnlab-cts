package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/18/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class LipidModifierTest extends AbstractLipidModifierTest {

	@Test
	public void testModifyTG1() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("TG(30:0)");

		assertEquals("TG(30:0)",result);
	}

	@Test
	public void testModifyTG2() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("TG (30:0)");

		assertEquals("TG(30:0)",result);
	}


	@Test
	public void testModifyTG3() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("TG 30:0");

		assertEquals("TG(30:0)",result);
	}


//	@Test
//	@Ignore("NumberUnderscore modifier disabled...")
//	public void testModifyDG1() throws Exception {
//
//		Modify modify = getModifier();
//
//		String result = modify.modify("1_DG (12:0/12:0/0:0) [M+Na]+ ISTD");
//
//		assertEquals("1_DG(12:0/12:0/0:0)",result);
//	}

	@Test
	public void testModifyDG2() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("DG (12:0/12:0/0:0)");

		assertEquals("DG(12:0/12:0/0:0)",result);
	}


	@Test
	public void testModifyDG3() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("DG(12:0/12:0/0:0)[M+Na]");

		assertEquals("DG(12:0/12:0/0:0)",result);
	}

	@Test
	public void testModifyDG4() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("DG (36:2) [M+Na]+");

		assertEquals("DG(36:2)",result);
	}

	@Test
	public void testModifyParensSpace() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("DG( 36:2)");

		assertEquals("DG(36:2)",result);
	}

	@Test
	public void testModifyParensSpace2() throws Exception {

		Modify modify = getModifier();

		String result = modify.modify("1,2-DG( 36:2)");

		assertEquals("1,2-DG(36:2)",result);
	}

	protected AbstractLipidModifier getModifier() {
		return new LipidModifier();
	}
}
