package edu.ucdavis.fiehnlab.chemify.configuration;

import org.junit.Ignore;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 8:44 PM
 * <p/>
 * Tests simple configurations
 */
@Ignore
public class SimpleConfigurationTest extends AbstractConfigurationTest {

	@Ignore
	protected SimpleConfiguration getConfiguration() {
		return new SimpleConfiguration();
	}

}
