package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/10/2013
 * Time: 11:38 PM
 */
public class GLStrGenLipifyTest extends AbstractIdentifyTest {
    @Override
    protected String getTestTerm() {
        return "DG(2:0/2:0/12:0)";
    }

    @Override
    public AbstractIdentify getIdentify() {
        return new GLStrGenLipify();
    }
}
