package edu.ucdavis.fiehnlab.chemify.helper.tables;

import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MonosaccharideNamesTest {
	Logger logger = Logger.getLogger(this.getClass());


	@Test
	public void testIsMonosaccharide() throws Exception {
		assert MonosaccharideNames.isMonosaccharide("glucose");
	}

	@Test
	public void testIsMonosaccharideClosed() throws Exception {
		assert MonosaccharideNames.isMonosaccharide("glucopyranose");
	}

	@Test
	public void testGetAlternateNameFromOpen() throws Exception {
		assertEquals("glucopyranose", MonosaccharideNames.getAlternateName("glucose"));
	}

	@Test
	public void testGetAlternateNameFromClosed() throws Exception {
		assertEquals("glucose", MonosaccharideNames.getAlternateName("glucopyranose"));
	}

	@Test
	public void testIsNotMonosaccharide() {
		assert false == MonosaccharideNames.isMonosaccharide("sodium chloryde");    // ;-P
	}

	@Test
	public void testGetAlternateNameFromNonSugar() {
		assertEquals("salt", MonosaccharideNames.getAlternateName("salt"));
	}
}