package edu.ucdavis.fiehnlab.chemify.modify;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/24/13
 * Time: 3:14 PM
 */
public class DeuteratedCompoundModiferTest {
    @Test
    public void testModify() throws Exception {

        DeuteratedCompoundModifer modifer = new DeuteratedCompoundModifer();
        assertEquals("Cholesterol-d7", modifer.modify("Cholesterol d7"));

    }

    @Test
    public void testModify2() throws Exception {

        DeuteratedCompoundModifer modifer = new DeuteratedCompoundModifer();
        assertEquals("Cholesterol-d7", modifer.modify("Cholesterold7"));

    }

    @Test
    public void testGetPattern() throws Exception {

        DeuteratedCompoundModifer modifer = new DeuteratedCompoundModifer();
        assertTrue("Cholesterol d7".matches(modifer.getPattern()));
    }

    @Test
    public void testGetPattern2() throws Exception {

        DeuteratedCompoundModifer modifer = new DeuteratedCompoundModifer();
        assertTrue("Cholesterold7".matches(modifer.getPattern()));
    }

    @Test
    public void testNotMatch() throws Exception {

        DeuteratedCompoundModifer modifer = new DeuteratedCompoundModifer();
        assertFalse("CUDA".matches(modifer.getPattern()));
    }

}
