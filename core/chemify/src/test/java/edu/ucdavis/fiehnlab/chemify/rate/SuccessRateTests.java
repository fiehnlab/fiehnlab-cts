package edu.ucdavis.fiehnlab.chemify.rate;

import edu.ucdavis.fiehnlab.chemify.Chemify;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll;
import edu.ucdavis.fiehnlab.chemify.io.HitSerializer;
import edu.ucdavis.fiehnlab.chemify.io.HitSerializerTSVImpl;
import edu.ucdavis.fiehnlab.chemify.io.SpringConfiguredChemify;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertTrue;

/**
 * A rather complex test to try to calculate the overall hit rate over several files and ensure
 * that they are always on average better than X
 * <p/>
 * Created by diego on 1/29/14.
 */
@Ignore
public class SuccessRateTests {
	@Rule
	public TestName name = new TestName();

	ArrayList<String> sourceFiles = new ArrayList<String>();
	Logger logger = Logger.getLogger(this.getClass());

	@Before
	public void setUp() {
		getFilesToRate(getBaseDir());
	}

	/**
	 * our base dear
	 *
	 * @return
	 */
	protected File getBaseDir() {
		return new File("src/test/resources/filesToRate");
	}

	protected void getFilesToRate(File directory) {

		assertTrue(directory != null);
		assertTrue(directory.isDirectory());

		for (File f : directory.listFiles()) {
			if (f.isFile())
				sourceFiles.add(f.getAbsolutePath());
			else
				getFilesToRate(f);
		}
	}

	@After
	public void tearDown() {
		sourceFiles.clear();
	}

	@Test
    @Ignore
	public void testSuccessRate() throws Exception {

		class FutureResult {
			String file;
			double percentage;
		}


		ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		ExecutorCompletionService<FutureResult> refutures = new ExecutorCompletionService<FutureResult>(service);

		for (final String fileName : sourceFiles) {

			refutures.submit(new Callable<FutureResult>() {
				@Override
				public FutureResult call() throws Exception {
					FutureResult res = new FutureResult();
					res.file = fileName;
					res.percentage = calculateSuccessRate(fileName);
					return res;
				}
			});

		}


		FileWriter resultWriter = new FileWriter(new File(name.getMethodName() + "_result.txt"));
		double total = 0;

		for (String s : sourceFiles) {

			FutureResult result = refutures.take().get();
			total = total + result.percentage;
			logger.info(result.file + " - " + result.percentage);

			resultWriter.write(result.file + "\t-\t" + result.percentage + "\n");
		}

		double finalScore = total / (double) sourceFiles.size();

		logger.info("Total Files: " + sourceFiles.size());
		logger.info("Overall Score: " + finalScore);

		resultWriter.write("total score\t-\t" + finalScore + "\n");

		resultWriter.flush();
		resultWriter.close();
		service.shutdown();

		assert (finalScore > getMinimumScore());

	}

	protected double getMinimumScore() {
		return 0.89;
	}

	/**
	 * calculates the success rate for this file
	 *
	 * @param fileName
	 * @return
	 */
	private Double calculateSuccessRate(String fileName) {

		logger.info("calculating file: " + fileName);
		int total = 0;
		int missing = 0;
		try {
			File file = new File(fileName);

			BufferedReader reader = new BufferedReader(new FileReader(file));
			Chemify springConfiguredChemify = new SpringConfiguredChemify("defaultTestConf.xml");

			String line = reader.readLine();
			HitSerializer serializer = new HitSerializerTSVImpl();

			Writer writer = new BufferedWriter(new FileWriter("target/" + getClass().getName() + "_missing_" + file.getName()));

			while (line != null && line.length() > 0) {

				List<? extends Hit> result = springConfiguredChemify.identify(line);

				if (result.size() > 0) {
					if ((result.iterator().next().getUsedAlgorithm() instanceof NothingIdentifiedAtAll)) {
						missing = missing + 1;
						serializer.serialize(writer, result);
					}
				}

				line = reader.readLine();
				total = total + 1;
			}

			writer.flush();
			writer.close();

			reader.close();
		} catch (FileNotFoundException ex) {
			logger.error("Can't find file '" + fileName + "'");
		} catch (IOException ex) {
			logger.error("Error reading file: '" + fileName + "'");
		}

		//success rate is returned
		return (total - missing) / (double) total;
	}
}
