package edu.ucdavis.fiehnlab.chemify.scoring;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.TestIdentifyMapper;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ScoreBySimilarityTest extends TestCase {
	Logger logger = Logger.getLogger(this.getClass());
	ScoreBySimilarity similScorer;

	@Before
	public void setUp() {
		similScorer = new ScoreBySimilarity();
	}

	@Test
	public void testScore() throws Exception {

		assert(similScorer.score("", "a") == 0);
		assert(similScorer.score("a", "") == 0);
		assertEquals(1.0, similScorer.score("a", "a"), 0.001);
		assertEquals(0.0, similScorer.score("a", "b"), 0.001);
		assertEquals(0.099, similScorer.score("z artifact", "z"), 0.001);

		assertEquals(0.571, similScorer.score("alanine", "alone"), 0.001);
	}

	@Test
	public void testAlanines() {
		List<Hit> hits = new ArrayList<Hit>();
		hits.add(new HitImpl("QNAYBMKLOCPYGJ-REOHCLBHSA-N", new TestIdentifyMapper(), "L-alanine", "alanine"));
		hits.add(new HitImpl("QNAYBMKLOCPYGJ-UHFFFAOYSA-N", new TestIdentifyMapper(), "alanine", "alanine"));
		hits.add(new HitImpl("QNAYBMKLOCPYGJ-UWTATZPHSA-N", new TestIdentifyMapper(), "D-alanine", "alanine"));

		List<? extends Hit> res = similScorer.score(hits, "alanine");

		if(logger.isDebugEnabled()) {
			logger.debug("score: " + res);
		}

		assert res.size() == 3;
		assert res.get(0) instanceof ScoredHit;
	}
}