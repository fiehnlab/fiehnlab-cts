package edu.ucdavis.fiehnlab.chemify.identify.cdk;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;
import org.junit.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/26/13
 * Time: 11:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class InchiCodeConverterTest extends AbstractIdentifyTest {
	/**
	 * implmementation needs to provide us with a new identify object
	 *
	 * @return
	 */
	@Override
	public AbstractIdentify getIdentify() {
		return new InchiCodeConverter();
	}

	@Override
	protected String getTestTerm() {
		return ("InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3");
	}

	@Test
	public void testIdentifyBadSearchString() {

		List<Hit> res = getIdentify().identify("not an inchi code", "not an inchi code");
		assert res.isEmpty();
	}

	@Test
	public void testIdentifyNullSearchString() {

		List<Hit> res = getIdentify().identify(null, null);
		assert res.isEmpty();
	}
}
