package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Modify;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by diego on 1/7/14.
 */
public class MajorModifierTest {
	@Test
	public void testModify() throws Exception {
		Modify modify = new MajorModifier();

		assertEquals("glucose-6-phosphate", modify.modify("glucose-6-phosphate major"));
	}

	@Test
	public void testModify2() throws Exception {
		Modify modify = new MajorModifier();

		assertEquals("glucose-6-phosphate", modify.modify("glucose-6-phosphate major TMS"));
	}
}
