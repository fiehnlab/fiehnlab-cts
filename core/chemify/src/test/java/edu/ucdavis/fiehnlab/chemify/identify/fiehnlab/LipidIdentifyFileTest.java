package edu.ucdavis.fiehnlab.chemify.identify.fiehnlab;

import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentifyTest;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/23/13
 * Time: 3:34 PM
 */
public class LipidIdentifyFileTest extends AbstractIdentifyTest {
    @Override
    public LipidIdentifyFile getIdentify() {
        return new LipidIdentifyFile();
    }

    protected String[] getTestTerms() {
        return new String[]{"PC(28:0)","LPC(20:1)"};
    }
}
