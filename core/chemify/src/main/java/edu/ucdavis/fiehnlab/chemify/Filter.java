package edu.ucdavis.fiehnlab.chemify;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/25/13
 * Time: 9:42 PM
 *
 * A simple interface to allow use to filter the result and so reduce data.
 */
public interface Filter extends Prioritize, Describe{

    /**
     * filters the input list
     * @param input
     * @return
     */
    public List<Scored> filter(List<? extends Scored> input);
}
