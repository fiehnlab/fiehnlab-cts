package edu.ucdavis.fiehnlab.chemify;

import java.io.Serializable;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 10:28 PM
 * defines a simple hit
 */
public interface Hit extends Comparable<Hit>, Serializable {

	/**
	 * the discovered inchi key
	 *
	 * @return
	 */
	public String getInchiKey();

	/**
	 * which algorithm was used to discover this hit
	 *
	 * @return
	 */
	public Identify getUsedAlgorithm();

	/**
	 * what kind of search term was used
	 *
	 * @return
	 */
	public String getSearchTerm();

	/**
	 * returns the original search term in case of multiplexed terms
	 *
	 * @return
	 */
	public String getOriginalTerm();

	/**
	 * sets the original search term in case of multiplexed terms
	 *
	 * @return
	 */
	public void setOriginalTerm(String term);

	public Map<String,Object> getTrackedProperties();
}
