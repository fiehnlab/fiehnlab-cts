package edu.ucdavis.fiehnlab.chemify.utils.kegg;

import edu.ucdavis.fiehnlab.chemify.helper.constants.ConstantsRegistry;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by diego on 10/17/14.
 */
public final class KeggRester {
	private static Logger logger = Logger.getLogger(KeggRester.class);

	public static String KEGG_RESTAPI_URL = "rest.kegg.jp";

	public static String getKeggIdForName(String name) {
		String keggId = "";

		logger.debug(new StringBuilder().append("Getting KeggId for name: ").append(name));

		String data = KeggRester.callKeggApi(KeggOperation.KFIND, name);
		keggId = data.isEmpty() ? "" : data.substring(0, data.indexOf("\t")).split(":")[1];

		return keggId;
	}

	public static List<String> getNamesForKeggId(String kid) {
		List<String> names = new ArrayList<String>();

		logger.debug(new StringBuilder().append("Getting names for KeggId: ").append(kid));

		String data = KeggRester.callKeggApi(KeggOperation.KGET, "cpd:" + kid);
		String nameTag = data.substring(data.indexOf("NAME") + 4, data.indexOf("FORMULA")).trim();
		Collections.addAll(names, nameTag.split(";"));

		return names;
	}

	private static String callKeggApi(KeggOperation operation, String searchTerm) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.setConnectTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.build();

		String result = "";
		URI url;
		URIBuilder builder = new URIBuilder();

		String kdb = operation.equals(KeggOperation.KFIND) ? "/compound/" : "/";

		try {
			url = builder.setScheme("http").setHost(KEGG_RESTAPI_URL).setPath(operation.operation + kdb + searchTerm).build();

			logger.debug(new StringBuilder().append("Calling KEGG API's opperation:").append(operation).append(", url: ").append(url));

			HttpGet request = new HttpGet(url);

			request.addHeader("Content-Type", "application/json");
			request.setConfig(requestConfig);
			CloseableHttpResponse response = null;

			String message = "";
			try {
				response = httpclient.execute(request);

				int status = response.getStatusLine().getStatusCode();
				message = response.getStatusLine().getReasonPhrase();

				BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				StringBuilder respStr = new StringBuilder();
				while ((line = reader.readLine()) != null) {
					respStr.append(line + "\n");
				}
				result = respStr.toString();

				Logger.getLogger(KeggRester.class).debug("response: " + respStr);

				response.close();
				httpclient.close();

			} catch (IOException e) {
				Logger.getLogger(KeggRester.class).error("Error getting data from KEGG", e);
			}
		} catch (Exception e) {
			Logger.getLogger(KeggRester.class).error("can't build url to kegg rest api", e);
		}

		return result;
	}

	public static enum KeggOperation {
		KGET("/get"),
		KFIND("/find"),
		KLIST("/list");

		private String operation;

		private KeggOperation(String op) {
			operation = op;
		}

		public String getOperation() {
			return operation;
		}
	}
}
