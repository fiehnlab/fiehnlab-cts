package edu.ucdavis.fiehnlab.chemify.utils.chemspider;

import edu.ucdavis.fiehnlab.chemify.Caching;
import edu.ucdavis.fiehnlab.chemify.cache.EHBasedCache;
import edu.ucdavis.fiehnlab.chemify.exception.ChemSpiderException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by diego on 10/8/14.
 * <p/>
 * generate requests agains the chemspider rest api
 */
public class ChemspiderRester {
    static Logger logger = Logger.getLogger("edu.ucdavis.fiehnlab.chemify.utils.chemspider.ChemspiderRester");

    @Autowired
    private Caching<String> cache = new EHBasedCache<>("chemspider");

//	private final String CHEMSPIDER_SCHEME = "http";
//
//	public final String CHEMSPIDER_NAME_SEARCH_PATH = "/Search.asmx/SimpleSearch";
//	public final String CHEMSPIDER_CSID_RECORD_PATH = "/JSON.ashx";
//	public final String CHEMSPIDER_INCHI_PATH = "/inchi.asmx/SMILESToInChI";
//
//	public final String chemspiderhost = (System.getProperty("CHEMSPIDER_HOST") != null ? System.getProperty("CHEMSPIDER_HOST") : "www.chemspider.com");
//	public final String chemspiderpartshost = (System.getProperty("CHEMSPIDER_PARTS_HOST") != null ? System.getProperty("CHEMSPIDER_PARTS_HOST") : "parts.chemspider.com");
//
//	//authorization for the Chemspider webseite -- Will expire
//	public final String TOKEN = "15bdfcf3-d5f1-459d-b97a-4e3b43a9a445";

    private ChemspiderRester() {
    }

    /**
     * returns a map with chemspider metrics data for a specific inchikey. If inchikey is not found the metric values are 0.
     *
     * @param inchikey
     * @return
     * @throws IOException
     */
    public Map<String, Integer> getScoringMetricsForInchiKey(String inchikey) throws ChemSpiderException {
        Map<String, Integer> metrics = new HashMap<>();
        clearMetrics(metrics);

		/*
		if (inchikey.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
			List<String> csids = getCsidsForToken(inchikey, 1);
			clearMetrics(metrics);

			String jsonStr = "";
			for (String id : csids) {
				try {
					URI encodedUrl = new URIBuilder()
							.setScheme(CHEMSPIDER_SCHEME)
							.setHost(chemspiderpartshost)
							.setPath(CHEMSPIDER_CSID_RECORD_PATH)
							.setParameter("op", "GetRecordsAsCompounds")
							.setParameter("csids[0]", id)
							.setParameter("serfilter", "Compound[DataSourcesCount|ReferencesCount|PubmedHits]")
							.build();

					logger.debug(new StringBuilder().append("Getting metrics for '").append(inchikey).append("' ").append(" from URL: ").append(encodedUrl));

					jsonStr = queryChemspider(encodedUrl);

				} catch (URISyntaxException e) {
					logger.error("Error creating chsmpider rest api url");
				} catch (IOException e) {
					throw new ChemSpiderException(new StringBuilder().append("Error getting ChemSpider metrics for inchikey: ").append(inchikey).append(" and CSID: ").append(id).toString(), e);
				}


				// make Map<String, Integer> based on the json string [{"DataSourcesCount":0,"ReferencesCount":0,"PubmedHits":0}]
				jsonStr = jsonStr.substring(1, jsonStr.length() - 1);

				Type type = new TypeToken<ChemSpiderMetric>() {
				}.getType();
				ChemSpiderMetric data = new Gson().fromJson(jsonStr, type);

				try {
					metrics.put("DataSourcesCount", data.getDataSourcesCount());
					metrics.put("ReferencesCount", data.getReferencesCount());
					metrics.put("PubmedHits", data.getPubmedHits());
				} catch (ArrayIndexOutOfBoundsException e) {
					logger.error(new StringBuilder().append("Error getting metrics data for csid: ").append(csids)
							.append(" (").append(inchikey).append("). Setting metrics to 0\n")
							.append(e.getMessage()).toString());
					clearMetrics(metrics);
				}
			}
		} else {
			logger.error(new StringBuilder().append("error getting metrics for inchikey (").append(inchikey).append(").\n"));
			throw new ChemSpiderException("Invalid InChIKey: " + inchikey);
		}

		logger.info(new StringBuilder().append("Found metrics for inchikey (").append(inchikey).append(") : ").append(metrics));
		*/

        return metrics;
    }

    public void clearMetrics(Map<String, Integer> metrics) {
        metrics.put("DataSourcesCount", 0);
        metrics.put("ReferencesCount", 0);
        metrics.put("PubmedHits", 0);
    }

    /**
     * returns a json string with the list of names from chemspider for a single inchikey
     *
     * @param inchikey
     * @return
     * @throws IOException
     */
    public String getNamesForInchikey(String inchikey) throws IOException {
        String jsonres = "{}";
		/*
		List<String> csids = getCsidsForToken(inchikey, 1);

		String jsonres = "";
		for (String id : csids) {
			try {
				URI encodedUrl = new URIBuilder()
						.setScheme(CHEMSPIDER_SCHEME)
						.setHost(chemspiderpartshost)
						.setPath(CHEMSPIDER_CSID_RECORD_PATH)
						.setParameter("op", "GetRecordsAsCompounds")
						.setParameter("csids[0]", id)
						.setParameter("serfilter", "Compound[Synonyms]")
						.build();

				logger.debug(new StringBuilder().append("Getting names for '").append(inchikey).append("' ").append(" from URL: ").append(encodedUrl));
				logger.debug("url: " + encodedUrl);
				jsonres = queryChemspider(encodedUrl);
			} catch (URISyntaxException e) {
				logger.error("Error creating chsmpider rest api url");
			}
		}
*/
        return jsonres;
    }

    /**
     * searches in chemspider for the best names belonging to a specific inchikey
     *
     * @param inchikey
     * @return a list of names, or null if the inchikey is invalid or no names are found
     * @throws IOException if there is any problem connecting to chemspider
     */
    public List<String> getFilteredNamesForInchikey(String inchikey) throws ChemSpiderException {
        List<String> names = new ArrayList<>();

		/*
		try {
			String jsonStr = new ChemspiderRester().getNamesForInchikey(inchikey);

			if (!jsonStr.trim().isEmpty()) {
				String data = jsonStr.substring(1, jsonStr.length() - 1);

				Type type = new TypeToken<ChemSpiderSynonymList>() {
				}.getType();
				ChemSpiderSynonymList syns = new Gson().fromJson(data, type);

				for (Synonym s : syns.getSynonyms()) {
					int sType = s.getSynonymType();
					if (!s.getLangID().equals("en") || (sType >= 2 && sType <= 4)) continue;
					if (s.getReliability() < 3) continue;

					if (s.getRank() == null || s.getRank() > 1) {
						continue;
					}

					if (names.size() < 10 && s.isValid()) {
						names.add(s.getName());
					}
				}
				logger.info(new StringBuilder().append("Good Chemspider Names for inchikey: '").append(inchikey).append("' = ").append(names).toString());
			} else {
				logger.info(new StringBuilder().append("No Chemspider Names for inchikey: '").append(inchikey).append("'").toString());
			}
		} catch (IOException e) {
			logger.error("Error calling ChemSpider: " + e.getMessage());
			throw new ChemSpiderException("Error getting ChemSpider names for inchikey: " + inchikey, e);
		}
		*/
        return names;
    }

    /**
     * returns a list of up to 'max' CSIDs for the input token
     *
     * @param token search term
     * @param max   total number of items to be returned (if chemspider has more results)
     * @return
     * @throws URISyntaxException
     * @throws IOException
     */
    public List<String> getCsidsForToken(String token, Integer max) throws ChemSpiderException {
        List<String> csids = new ArrayList<String>();
		/*
		try {
			URI encodedUrl = new URIBuilder()
					.setScheme(CHEMSPIDER_SCHEME)
					.setHost(chemspiderhost)
					.setPath(CHEMSPIDER_NAME_SEARCH_PATH)
					.setParameter("query", token)
					.setParameter("token", TOKEN)
					.build();

			SAXBuilder builder = new SAXBuilder();
			Document document;

			logger.debug(new StringBuilder().append("Getting CSIDs for token: ").append(token).append(" from URL: ").append(encodedUrl));

			document = builder.build(new StringReader(queryChemspider(encodedUrl)));
			Element rootNode = document.getRootElement();

			if (rootNode.getChildren() != null) {
				List list = rootNode.getChildren();

				//loop through the top 10 Chemspider cids
				for (int i = 0; i < (list.size() < max ? list.size() : max); i++) {

					Element node = (Element) list.get(i);

					// get chemspider id
					csids.add(node.getText());
				}
			}
		} catch (JDOMException e) {
			logger.error("Error parsing response from Chemspider");
		} catch (URISyntaxException e) {
			logger.error("Error creating chemspider rest api url");
//		} catch (SocketTimeoutException e) {
//			logger.error("Chemspider is taking too long to respond, probably nothing found");
		} catch (IOException e) {
			throw new ChemSpiderException("Error getting Chemspider ids for search term: " + token, e);
		}

		logger.info(new StringBuilder().append("found csids for '").append(token).append("' => ").append(csids));
		*/
        return csids;
    }

    /**
     * returns a json string with the compound data for the given CSIDs [{cmpd-1 data}]
     *
     * @param csid
     * @return
     * @throws URISyntaxException
     */
    public String getDataForCsid(String csid) {
        String res = "";
        URI encodedUrl = null;
		/*
		try {
			encodedUrl = new URIBuilder()
					.setScheme(CHEMSPIDER_SCHEME)
					.setHost(chemspiderpartshost)
					.setPath(CHEMSPIDER_CSID_RECORD_PATH)
					.setParameter("op", "GetRecordsAsCompounds")
					.setParameter("csids[0]", csid)
					.setParameter("serfilter", "Compound[CSID|Name|Mol|Identifiers|DataSourcesCount|ReferencesCount|PubmedHits]")
					.build();

			logger.debug(new StringBuilder().append("Get compound data from URL: ").append(encodedUrl));
			res = queryChemspider(encodedUrl);
		} catch (URISyntaxException e) {
			logger.error("Error creating chsmpider rest api url");
		} catch (SocketTimeoutException e) {
			logger.error("Chsmpider timedout running: " + encodedUrl.toString());
		} catch (IOException e) {
			logger.error("Can't get data for CSID: " + csid, e);
		}
		*/
        return res;
    }

    /**
     * returns a json string with the compound data for the given CSIDs (max 10 compounds) [{cmpd-1 data}, ..., {cmpd-9 data}]
     *
     * @param csids
     * @return
     * @throws URISyntaxException
     */
    public String getDataForCsids(List<String> csids) throws URISyntaxException {

		/*
		if (csids.size() > 10) {
			logger.warn("CSID List has more items that allowed; results will only contain data for the 10 first elements.");
		}

		URIBuilder ub = new URIBuilder();
		ub.setScheme(CHEMSPIDER_SCHEME)
				.setHost(chemspiderpartshost)
				.setPath(CHEMSPIDER_CSID_RECORD_PATH)
				.setParameter("op", "GetRecordsAsCompounds")
				.setParameter("serfilter", "Compound[CSID|Name|Mol|Identifiers|DataSourcesCount|ReferencesCount|PubmedHits]");

		for (int i = 0; i < (csids.size() <= 10 ? csids.size() : 10); i++) {
			ub.setParameter("csids[" + i + "]", csids.get(i));
		}

		URI encodedUrl = ub.build();*/

        String data = "";
		/*
		try {
			data = queryChemspider(encodedUrl);

			logger.debug(new StringBuilder().append("Calling url: ").append(encodedUrl));
		} catch (SocketTimeoutException e) {
			logger.error("Chsmpider timedout running: " + encodedUrl.toString());
			data = "";
		} catch (IOException e) {
			logger.error("Can't get data.", e);
		}
		*/
        return data;
    }

    public String getSmilesToInchi(String smiles) throws Exception {
        StringBuffer input = new StringBuffer();
		/*
		URI url = null;
		try {
			url = new URIBuilder().setScheme(CHEMSPIDER_SCHEME)
					.setHost(chemspiderhost)
					.setPath(CHEMSPIDER_INCHI_PATH)
					.build();

			logger.debug(new StringBuilder().append("Getting InChIKey for SMILES:").append(smiles).append(" from url: ").append(url));
			CloseableHttpClient client = HttpClients.createDefault();
			RequestConfig requestConfig = RequestConfig.custom()
					.setSocketTimeout(ConstantsRegistry.TWOSECONDS)
					.setConnectTimeout(ConstantsRegistry.TWOSECONDS)
					.build();

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("smiles", smiles));

			HttpPost request = new HttpPost(url);
			request.addHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
			request.addHeader(HttpHeaders.ACCEPT, "application/json");
			request.setEntity(new UrlEncodedFormEntity(params));
			request.setConfig(requestConfig);

			CloseableHttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			Pattern p = Pattern.compile("<string .*?>(InChI=.*?)</string>");
			while ((line = rd.readLine()) != null) {
				Matcher m = p.matcher(line);
				if (m.matches()) {
					input.append(m.group(1));
				}
			}

			rd.close();
			response.close();
			client.close();

		} catch (SocketTimeoutException e) {
			logger.error("Chsmpider timedout running: " + url.toString());
			return "";
		} catch (Exception e) {
			throw new Exception("Couldn't convert the provided SMILES to an InChI Code.\n" + e.getMessage());
		}
		*/

        return input.toString();
    }

    /**
     * this executes the call to the chemspider rest api
     *
     * @param url
     * @return
     * @throws IOException
     */
    private String queryChemspider(URI url) throws IOException {
        String res = "";
		/*
		CloseableHttpClient client = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(ConstantsRegistry.TWOSECONDS)
				.setConnectTimeout(ConstantsRegistry.TWOSECONDS)
				.build();

		HttpGet request = new HttpGet(url);
		request.addHeader(HttpHeaders.ACCEPT, "application/json");
		request.setConfig(requestConfig);

		final String cachedStuff = cache.queryCache(request.toString());
		if (cachedStuff != null && !cachedStuff.isEmpty()) {
			logger.debug("returning cached data...");
			return cachedStuff;
		} else {
			CloseableHttpResponse target = null;
			StringBuffer input = new StringBuffer();
			try {
				target = client.execute(request);
			} catch (IOException e) {
				try {
					logger.warn("Exception calling ChemSpider: " + e.getMessage() + "\n\tCalling chemspider again...");
					target = client.execute(request);
				} catch (IOException ex) {
					logger.error("second call exception was: " + ex.getMessage());
					target.close();
					client.close();
					return "";
				}
			}

			if (target.getStatusLine().getStatusCode() == 404) {
				logger.error(new StringBuilder().append("Got a 404 calling: ").append(request).toString());
				return "Error getting Chemspider data.";
			}

			//read input into a string
			BufferedReader rd = new BufferedReader(new InputStreamReader(target.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				input.append(line);
			}

			rd.close();
			target.close();
			client.close();

			String res = input.toString();
			logger.debug("caching data...");
			cache.cache(request.getURI().toString(), res);*/
        return res;
    }
}

