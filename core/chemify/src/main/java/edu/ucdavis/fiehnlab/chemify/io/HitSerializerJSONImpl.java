package edu.ucdavis.fiehnlab.chemify.io;

import com.google.gson.*;
import edu.ucdavis.fiehnlab.chemify.Enhanced;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/25/13
 * Time: 1:25 PM
 * <p/>
 * serializes a hit as json element
 */
public class HitSerializerJSONImpl implements JsonSerializer<Hit>, HitSerializer {

    /**
     * can be used in webservices directly
     *
     * @param src
     * @param typeOfSrc
     * @param context
     * @return
     */
    public JsonElement serialize(Hit src, Type typeOfSrc, JsonSerializationContext context) {
        return getJsonElement(src);
    }

    /**
     * converts a hit to a json object
     *
     * @param src
     * @return
     */
    private JsonObject getJsonElement(Hit src) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", src.getInchiKey());
        obj.addProperty("query", src.getSearchTerm());
        obj.addProperty("algorithm", src.getUsedAlgorithm().getClass().getName());

        if (src instanceof ScoredHit) {
            obj.addProperty("score", ((ScoredHit) src).getScore());
            obj.addProperty("scoring_algorithm", ((ScoredHit) src).getScoringAlgorithm().getClass().getName());
        }
        if (src instanceof Enhanced) {
            JsonArray array = new JsonArray();
            for (Enhancement e : ((Enhanced) src).getEnhancements()) {
                JsonObject o = new JsonObject();
                o.addProperty(e.getIdentifier(), e.getValue().toString());
                array.add(o);
            }

            obj.add("enhancements", array);
        }
        return obj;
    }

    /**
     * serializes a single hit
     *
     * @param writer
     * @param hit
     * @throws IOException
     */
    public void serialize(Writer writer, Hit hit) throws IOException {

        writeJSON(writer, getJsonElement(hit));

    }

    /**
     * serializes some hits
     *
     * @param writer
     * @param result
     * @throws IOException
     */
    public void serialize(Writer writer, List<? extends Hit> result) throws IOException {
        JsonObject obj = new JsonObject();
        JsonArray array = new JsonArray();

        for (Hit hit : result) {
            array.add(getJsonElement(hit));
        }

        obj.add("result", array);

        writeJSON(writer, obj);
    }

    /**
     * writes the object to the string
     *
     * @param writer
     * @param obj
     * @throws IOException
     */
    private void writeJSON(Writer writer, JsonObject obj) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        writer.write(gson.toJson(obj));
        writer.flush();
    }
}
