package edu.ucdavis.fiehnlab.chemify.helper.tables;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by diego on 10/20/14.
 */
public class MonosaccharideNames {

	/**
	 * contains the list of tetroses, pentoses and hexoses names in their open and closed forms.
	 * The key represents the name of the open structure
	 * The value represents the name of the closed (furanose or pyranose) structure
	 */
	public static Map<String, String> monosaccharideNames;

	static {
		monosaccharideNames = new HashMap<String, String>();
		monosaccharideNames.put("erythrose", "erythrofuranose");
		monosaccharideNames.put("threose", "threofuranose");
		monosaccharideNames.put("ribose", "ribofuranose");
		monosaccharideNames.put("arabinose", "arabinofuranose");
		monosaccharideNames.put("xylose", "xylofuranose");
		monosaccharideNames.put("lyxose", "lyxofuranose");
		monosaccharideNames.put("allose", "allopyranose");
		monosaccharideNames.put("altose", "altopyranose");
		monosaccharideNames.put("glucose", "glucopyranose");
		monosaccharideNames.put("mannose", "mannopyranose");
		monosaccharideNames.put("gulose", "gulopyranose");
		monosaccharideNames.put("idose", "idopyranose");
		monosaccharideNames.put("galactose", "galactopyranose");
		monosaccharideNames.put("talose", "talopyranose");
	}

	public static Boolean isMonosaccharide(String name) {
		if (monosaccharideNames.containsKey(name)) {
			return true;
		}

		if (monosaccharideNames.containsValue(name)) {
			return true;
		}

		return false;
	}

	public static String getAlternateName(String name) {
		if (isMonosaccharide(name)) {
			if (monosaccharideNames.containsKey(name)) {
				return monosaccharideNames.get(name);
			} else {
				String key = "";
				for (Map.Entry<String, String> item : monosaccharideNames.entrySet()) {
					if (name.equals(item.getValue())) {
						return item.getKey();
					}
				}
			}
		}
		return name;
	}
}
