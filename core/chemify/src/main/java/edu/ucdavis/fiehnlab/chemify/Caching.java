package edu.ucdavis.fiehnlab.chemify;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/12/2013
 * Time: 8:36 PM
 *
 * Allows us to cache elements, before we actually execute queries
 */
public interface Caching <T>{

    /**
     * adds an element to the cache
     * @param searchTerm
     * @param result
     */
    public void cache(String searchTerm, T result);

    /**
     * queries the cache and receives the object or returns null
     * @param searchTerm
     * @return
     */
    public T queryCache(String searchTerm);

    public void clear();
}
