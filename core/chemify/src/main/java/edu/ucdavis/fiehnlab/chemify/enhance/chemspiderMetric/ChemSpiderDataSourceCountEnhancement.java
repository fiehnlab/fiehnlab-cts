package edu.ucdavis.fiehnlab.chemify.enhance.chemspiderMetric;

import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;

/**
 * Created by diego on 10/9/14.
 */
public class ChemSpiderDataSourceCountEnhancement extends Enhancement {
	private ChemSpiderDataSourceCountEnhancement(String identifier, Object value) {
		super(identifier, value);
	}

	public ChemSpiderDataSourceCountEnhancement(Integer count) {
		this("DataSourcesCount", count);
	}
}
