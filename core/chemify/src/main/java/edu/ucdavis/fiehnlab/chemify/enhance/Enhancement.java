package edu.ucdavis.fiehnlab.chemify.enhance;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 10:08 PM
 *
 * defines any enhanced object. Basically a metadata object
 */
public class Enhancement implements Serializable {

    public Enhancement(String identifier, Object value){
        this.setIdentifier(identifier);
        this.setValue(value);
    }

    private String identifier;

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    private Object value;

    public String toString(){
        return this.getClass().getSimpleName() + " [" + identifier + ":" + value + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Enhancement)) return false;

        Enhancement that = (Enhancement) o;

        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = identifier != null ? identifier.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
