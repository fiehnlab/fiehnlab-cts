package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wohlgemuth on 3/31/14.
 */
public class SpaceMultiPlexer implements Multiplexer {

    Logger logger = Logger.getLogger(getClass());

    @Override
    public Set<String> multiplex(String searchTerm) {

        Set<String> result = new HashSet<String>();
        int count = 0;

        searchTerm = searchTerm.trim();

        result.add(searchTerm);

        //only do full multiplexing in case dashes and spaces are less than 5
        for(char c : searchTerm.toCharArray()) {
            if(c == ' ' || c == '-') {
                count++;
            }
        }

        if(5 >= count) {
            String content = searchTerm;

            while (content.contains(" ")) {
                content = content.replaceFirst(" ", "-");
                result.add(content.trim());
            }


            if (logger.isDebugEnabled())
                logger.debug("multiplexed " + searchTerm + " to " + result.size() + " terms");
        } else {
            result.add(searchTerm.replaceAll(" ", "-").trim());
        }

        return result;
    }

    @Override
    public String getDescription() {
        return "generates different combinations of spaces replaced with dashes characters";
    }
}
