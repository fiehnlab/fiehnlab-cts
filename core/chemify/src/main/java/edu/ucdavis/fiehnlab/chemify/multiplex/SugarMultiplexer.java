package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import edu.ucdavis.fiehnlab.chemify.helper.tables.MonosaccharideNames;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wohlgemuth on 3/31/14.
 */
public class SugarMultiplexer implements Multiplexer {

	Logger logger = Logger.getLogger(getClass());

	@Override
	public Set<String> multiplex(String searchTerm) {

		Set<String> result = new HashSet<String>();

		result.add(searchTerm);

		if (MonosaccharideNames.isMonosaccharide(searchTerm.toLowerCase())) {
			result.add(MonosaccharideNames.getAlternateName(searchTerm));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("\tmultiplexed " + searchTerm + " to " + result.size() + " term/s");
			logger.debug("\t" + result);
		}

		return result;
	}

	@Override
	public String getDescription() {
		return "adds the name for the alternate form of the sugar's name we are multiplexing. [closed structure]'s name -> [open structure]'s name and viceversa";
	}
}
