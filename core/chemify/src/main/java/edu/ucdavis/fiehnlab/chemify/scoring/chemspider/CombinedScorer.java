package edu.ucdavis.fiehnlab.chemify.scoring.chemspider;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import edu.ucdavis.fiehnlab.chemify.scoring.AbstractCombinedScoring;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by diego on 10/9/14.
 */
public class CombinedScorer implements Scoring {
    private Set<AbstractCombinedScoring> scorers = new HashSet<AbstractCombinedScoring>();
    Logger logger = Logger.getLogger(this.getClass());

    /**
     * the score methods converts the result into a scored hit to reduce the amount of data output
     *
     * @param input
     * @param searchTerm
     * @return
     */
    @Override
    public List<Scored> score(List<? extends Hit> input, String searchTerm) {
        List<? extends Hit> enhanced = new LinkedList<>();
        List<Scored> scoredRes = new ArrayList<>();

        Map<String, Double> tmpScores = new HashMap<>();

        Double max = 1.0;

        //score input with all 3 CS scorers
        for (AbstractCombinedScoring scorer : scorers) {
            List<Scored> tmp = scorer.score(input, searchTerm);

            //calculate sum for average of scorers
            for (Scored hit : tmp) {

                double weightedScore = hit.getScore() * scorer.getWeight();

                if (tmpScores.containsKey(hit.getInchiKey()))
                    tmpScores.put(hit.getInchiKey(), tmpScores.get(hit.getInchiKey()) + weightedScore);
                else {
                    tmpScores.put(hit.getInchiKey(), weightedScore);
                }
            }
        }
        //get average of scorers and max value for scaling
        for (Map.Entry<String, Double> sum : tmpScores.entrySet()) {
            sum.setValue(sum.getValue() / scorers.size());
            max = Math.max(max, sum.getValue());
        }
        //create new scoredHit list and scale score to range 0 - 1
        for (Hit hit : input) {
            scoredRes.add(new ScoredHit(hit, this, tmpScores.get(hit.getInchiKey()) / max));
        }

        //reverse sort  1.0 -> 0.0
        Collections.sort(scoredRes, new Comparator<Scored>() {
            @Override
            public int compare(Scored scored, Scored scored2) {
                return scored.compareTo(scored2);
            }
        });

        return scoredRes;
    }

    public void addScorer(AbstractCombinedScoring scorer) {
        this.scorers.add(scorer);
    }

    public void setScorers(Set<AbstractCombinedScoring> scorers) {
        this.scorers = scorers;
    }

    public Set<AbstractCombinedScoring> getScorers() {
        return scorers;
    }

    /**
     * am arbitrary number. The higher the value the higher the priority
     *
     * @return
     */
    @Override
    public Priority getPriority() {
        return Priority.MEDIUM;
    }

    @Override
    public int compareTo(Prioritize prioritize) {
        return getPriority().compareTo(prioritize.getPriority());
    }

    /**
     * the description of this object
     *
     * @return
     */
    @Override
    public String getDescription() {
        return "This scorer combines the scores of the configured ChemSpider scorers";
    }
}
