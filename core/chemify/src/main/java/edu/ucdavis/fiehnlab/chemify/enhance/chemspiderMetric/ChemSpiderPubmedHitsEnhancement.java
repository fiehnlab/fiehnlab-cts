package edu.ucdavis.fiehnlab.chemify.enhance.chemspiderMetric;

import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;

/**
 * Created by diego on 10/9/14.
 */
public class ChemSpiderPubmedHitsEnhancement extends Enhancement {

	private ChemSpiderPubmedHitsEnhancement(String identifier, Object value) {
		super(identifier, value);
	}

	public ChemSpiderPubmedHitsEnhancement(Integer hits) {
		this("PubmedHits", hits);
	}
}
