package edu.ucdavis.fiehnlab.chemify.utils.pubchem;

import edu.ucdavis.fiehnlab.chemify.helper.constants.ConstantsRegistry;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

/**
 * Created by diego on 10/17/14.
 */
public class PubchemRester {
	private static Logger logger = Logger.getLogger(PubchemRester.class);

	public static String PUBCHEM_PUG_URL = "pubchem.ncbi.nlm.nih.gov";

	public static String hasKeggForInchikey(String inchikey) {
		String hasKegg = "";

		String data = PubchemRester.callPubchemPug("/xrefs/SourceName", "inchikey/" + inchikey);
		logger.debug(new StringBuilder().append("has kegg? ").append(data.contains("KEGG")));

		return hasKegg;
	}

	private static String callPubchemPug(String operation, String searchTerm) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.setConnectTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.build();

		String result = "";
		URI url;
		URIBuilder builder = new URIBuilder();
		try {
			url = builder.setScheme("https").setHost(PUBCHEM_PUG_URL).setPath("/rest/pug/compound/" + searchTerm + operation + "/json").build();

			logger.debug(new StringBuilder().append("Calling Pubchem PUG ").append(url));

			HttpGet request = new HttpGet(url);

			request.addHeader("Content-Type", "application/json");
			request.setConfig(requestConfig);
			CloseableHttpResponse response = null;

			String message = "";
			try {
				response = httpclient.execute(request);

				int status = response.getStatusLine().getStatusCode();
				message = response.getStatusLine().getReasonPhrase();

				BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				StringBuilder respStr = new StringBuilder();
				while ((line = reader.readLine()) != null) {
					respStr.append(line + "\n");
				}
				result = respStr.toString();

				logger.debug(new StringBuilder().append("response: ").append(respStr));

				response.close();
				httpclient.close();

			} catch (IOException e) {
				logger.error(new StringBuilder().append("Error getting data from pubchem"), e);
			}
		} catch (Exception e) {
			logger.error(new StringBuilder().append("can't build url to pubchem rest api"), e);
		}

		return result;

	}
}
