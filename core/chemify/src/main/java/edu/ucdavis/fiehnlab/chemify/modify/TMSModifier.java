package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/14/13
 * Time: 3:52 PM
 *
 * if a search term ends with ISTD we assume it's an internal standard
 */
public class TMSModifier extends AbstractModifier {
	Logger logger = Logger.getLogger(this.getClass());

	public TMSModifier() {
		super(Priority.HIGH);
	}

	public String modify(String searchTerm) {
		String result = searchTerm.replaceFirst(CommonPatterns.TMS_PATTERN, "$1").trim();
		StringBuffer msg = new StringBuffer();

		msg.append("modified: ").append(searchTerm).append(" -> ").append(result);
		logger.debug(msg);
		return result;
	}

	public String getDescription() {
		return "removes unneeded TMS String from the search term, since this marks a sample with added TMS";
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.TMS_PATTERN;
	}
}
