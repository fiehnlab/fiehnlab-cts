package edu.ucdavis.fiehnlab.chemify.io;

import edu.ucdavis.fiehnlab.chemify.Hit;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/25/13
 * Time: 1:27 PM
 *
 * a basic serializer for hits
 */
public interface HitSerializer {
    void serialize(Writer writer, Hit hit) throws IOException;

    void serialize(Writer writer, List<? extends Hit> result) throws IOException;
}
