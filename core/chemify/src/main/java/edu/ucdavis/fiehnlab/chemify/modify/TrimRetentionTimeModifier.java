package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by diego on 7/3/14.
 */
public class TrimRetentionTimeModifier extends AbstractModifier {
	Logger logger = Logger.getLogger(this.getClass().getName());

	public TrimRetentionTimeModifier() {
		super(Priority.MEDIUM);
	}

	/**
	 * modifies the search term based on incoming data, to ensure that it's compatible with this algorithm
	 *
	 * @param searchTerm
	 * @return
	 */
	@Override
	public String modify(String searchTerm) {
		if (searchTerm == null || searchTerm.isEmpty()) {
			return "";
		}

		Pattern p = Pattern.compile(CommonPatterns.RETENTION_TIME_PATTERN);
		Matcher m = p.matcher(searchTerm);

		String term = m.replaceAll("$1");
		logger.debug("modifying: " + searchTerm + " => " + term);

		return term;
	}

	/**
	 * the description of this object
	 *
	 * @return
	 */
	@Override
	public String getDescription() {
		return "Removes any retention time information from the search term.\nExamples: 1.00000 s; 0123.3s; 3212.123 s";
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	@Override
	public String getPattern() {
		return CommonPatterns.RETENTION_TIME_PATTERN;
	}
}
