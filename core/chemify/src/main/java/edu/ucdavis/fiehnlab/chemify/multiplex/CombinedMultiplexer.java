package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by diego on 7/16/14.
 */
public class CombinedMultiplexer implements Multiplexer {
	Logger logger = Logger.getLogger(getClass());

	@Override
	public Set<String> multiplex(String searchTerm) {
		Set<String> result = new HashSet<String>();

		result.add(searchTerm);

		result.addAll(new ProperCaseMultiPlexer().multiplex(searchTerm));

		Set<String> tmpRes = new HashSet<String>();

		for (String term : result) {
			tmpRes.addAll(new DashMultiPlexer().multiplex(term));
		}

		result.addAll(tmpRes);
		tmpRes.clear();
		for (String term : result) {
			tmpRes.addAll(new SpaceMultiPlexer().multiplex(term));
		}

		result.addAll(tmpRes);
		return result;
	}

	/**
	 * the description of this object
	 *
	 * @return
	 */
	@Override
	public String getDescription() {
		return "This multiplexer combines the behavior of the SpaceMultiplexer, ProperCaseMultiplexer and DashMultiplexer";
	}
}

