package edu.ucdavis.fiehnlab.chemify.configuration;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.scoring.ScoreBySimilarity;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 8:41 PM
 * <p/>
 * Simple configurations, which can also used as builder
 */
public class SimpleConfiguration implements Configuration {

	private Double searchTermMinQuality = 0.25;
	private String lipidmapsPath = "";
	private String perlPath = "";

	private List<Identify> algorithms = new Vector<Identify>();
	private Set<Filter> filters = new TreeSet<Filter>();
	private Set<Multiplexer> multiplexer = new TreeSet<Multiplexer>();
	private Set<Enhance> enhances = new HashSet<Enhance>();
	private List<Modify> modifications = new Vector<Modify>();
	private List<Scoring> scoringAlgorithms = new Vector<Scoring>();

	/**
	 * we allow non identified results in the result set by default
	 */
	private boolean nonIdentifiedResultsPermitted = true;

	public SimpleConfiguration() {
		this.addScoringAlgorithm(new ScoreBySimilarity());
	}

	/**
	 * adds an enhance object
	 *
	 * @param enhance
	 * @return
	 */
	public Configuration addEnhance(Enhance enhance) {
		this.enhances.add(enhance);
		return this;
	}

	public Configuration addFilter(Filter filter) {
		this.filters.add(filter);
		return this;
	}

	public SimpleConfiguration addScoringAlgorithm(Scoring scoring) {
		this.scoringAlgorithms.add(scoring);
		return this;
	}

	/**
	 * adds another algorithm and allows chaining
	 *
	 * @param identify
	 * @return
	 */
	public SimpleConfiguration addAlgorithm(Identify identify) {
		identify.setConfiguration(this);
		algorithms.add(identify);

		return this;
	}

	/**
	 * easier way to instance them for testing
	 *
	 * @return
	 */
	public static SimpleConfiguration newConfiguration() {
		return new SimpleConfiguration();
	}


	/**
	 * one line representation of our configuration
	 *
	 * @return
	 */
	public String toString() {
		Collections.sort(this.getAlgorithms(), new Comparator<Identify>() {
			public int compare(Identify identify, Identify identify2) {
				return identify2.getPriority().compareTo(identify.getPriority());
			}
		});

		StringBuffer buffer = new StringBuffer();

		buffer.append(this.getClass().getSimpleName());

		buffer.append("\nsearch term quality threshold: " + this.searchTermMinQuality);

		buffer.append("\nlipidmaps tools path: " + this.lipidmapsPath);

		buffer.append("\nperl path: " + this.perlPath);

		buffer.append("\nmodifier: [");
		for (Modify modifyy : this.getModifications()) {

			buffer.append(modifyy.getClass().getSimpleName());
			buffer.append(" -> ");

		}


		if (buffer.indexOf(" -> ") > -1) {
			buffer.replace(buffer.lastIndexOf(" -> "), buffer.length(), "");
		}
		buffer.append("]\nmultiplexer: [");

		for (Multiplexer multiplexer : this.getMultiplexer()) {

			buffer.append(multiplexer.getClass().getSimpleName());
			buffer.append(" -> ");

		}

		if (buffer.indexOf(" -> ") > -1) {
			buffer.replace(buffer.lastIndexOf(" -> "), buffer.length(), "");
		}

		buffer.append("]\nalgorithm: [");
		for (Identify identify : this.getAlgorithms()) {

			buffer.append(identify.getClass().getSimpleName());
			buffer.append(" -> ");

		}

		if (buffer.indexOf(" -> ") > -1) {
			buffer.replace(buffer.lastIndexOf(" -> "), buffer.length(), "");
		}

		buffer.append("]\nfilter: [");

		for (Filter filter1 : this.getFilters()) {
			buffer.append(filter1.getClass().getSimpleName());
			buffer.append(",");
		}

		if (buffer.lastIndexOf(",") > -1) {
			buffer.deleteCharAt(buffer.lastIndexOf(","));
		}

		buffer.append("]\nscoring: [");
		for (Scoring scoring : this.getScoringAlgorithms()) {
			buffer.append(scoring.getClass().getSimpleName());
			buffer.append(" -> ");
		}

		if (buffer.indexOf(" -> ") > -1) {
			buffer.replace(buffer.lastIndexOf(" -> "), buffer.length(), "");
		}

		buffer.append("]");
		return buffer.toString();
	}

	/**
	 * we will continue the algorithms in case of a chemify exception. Other exceptions should kill it
	 *
	 * @return
	 */
	public boolean isContinueOnException(Exception e) {
		return e instanceof ChemifyException;
	}

	/**
	 * by default we do not allow none identified results in the result set
	 *
	 * @return
	 */
	public boolean isNonIdentifiedResultsPermitted() {
		return nonIdentifiedResultsPermitted;
	}

	public SimpleConfiguration setNonIdentifiedResultsPermitted(boolean value) {
		this.nonIdentifiedResultsPermitted = value;
		return this;
	}

	@Override
	public Set<Multiplexer> getMultiplexer() {
		return multiplexer;
	}

	/**
	 * returns the list of identifier objects sorted by the comparator of the identify object
	 *
	 * @return
	 */
	public List<Identify> getAlgorithms() {
		Collections.sort(algorithms);
		return algorithms;

	}

	/**
	 * returns the filters used by the identify objects
	 *
	 * @return
	 */
	public Set<Filter> getFilters() {
		return filters;
	}

	/**
	 * returns the enhancements applied to the results
	 *
	 * @return
	 */
	public Set<Enhance> getEnhances() {
		return enhances;
	}

	/**
	 * avaialbe modifications for search terms
	 */
	public List<Modify> getModifications() {
		Collections.sort(modifications);
		return modifications;
	}

	/**
	 * Sets the list of filter objects
	 *
	 * @param filter
	 */
	public SimpleConfiguration setFilters(Set<Filter> filter) {
		this.filters = filter;
		return this;
	}

	/**
	 * Sets the list of identifiers to use
	 *
	 * @param list
	 */
	public SimpleConfiguration setAlgorithms(List<Identify> list) {
		this.algorithms = list;
		return this;
	}

	/**
	 * Sets the list of available enhancements to the results
	 *
	 * @param enhances
	 */
	public SimpleConfiguration setEnhances(Set<Enhance> enhances) {
		this.enhances = enhances;
		return this;
	}

	/**
	 * Sets the list of available modifications to the search terms
	 *
	 * @param modifications
	 */
	public SimpleConfiguration setModifications(List<Modify> modifications) {
		this.modifications = modifications;
		return this;
	}

	/**
	 * by default we score by data source count
	 */
	public List<Scoring> getScoringAlgorithms() {
		return scoringAlgorithms;
	}

	public SimpleConfiguration setScoringAlgorithms(List<Scoring> scoringAlgorithm) {
		this.scoringAlgorithms = scoringAlgorithm;
		return this;
	}

	public void setMultiplexer(Set<Multiplexer> multiplexer) {
		this.multiplexer = multiplexer;
	}

	public Double getSearchTermMinQuality() {
		return this.searchTermMinQuality;
	}

	public SimpleConfiguration setSearchTermMinQuality(Double searchTermMinQuality) {
		this.searchTermMinQuality = searchTermMinQuality;
		return this;
	}

	public String getLipidmapsPath() {
		return lipidmapsPath;
	}

	public SimpleConfiguration setLipidmapsPath(String lipidmapsPath) {
		this.lipidmapsPath = lipidmapsPath;
		return this;
	}

	public String getPerlPath() {
		return this.perlPath;
	}

	public Configuration setPerlPath(String path) {
		this.perlPath = path;
		return this;
	}
}
