package edu.ucdavis.fiehnlab.chemify.io.serialize.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import edu.ucdavis.fiehnlab.chemify.Configuration;

import java.lang.reflect.Type;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 11/13/13
 * Time: 5:05 PM
 */
public class ConfigurationSerializer implements JsonSerializer<Configuration> {
    @Override
    public JsonElement serialize(Configuration src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.addProperty("class",src.getClass().getName());

        return obj;
    }
}
