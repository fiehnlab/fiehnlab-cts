package edu.ucdavis.fiehnlab.chemify.filter;

import edu.ucdavis.fiehnlab.chemify.AbstractPrioritize;
import edu.ucdavis.fiehnlab.chemify.Filter;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.Scored;

import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 9:53 PM
 * <p/>
 * Limits the result to the top n hits
 */
public class TopNFilter extends AbstractPrioritize implements Filter {

	/**
	 * our defined limit
	 */
	private Integer limit;

	/**
	 * the default limit
	 */
	public static final Integer DEFAULT_LIMIT = 10;

	public TopNFilter(Integer limit) {
		super(Priority.MEDIUM);
		this.limit = limit;
	}

	public TopNFilter() {
		this(DEFAULT_LIMIT);
	}

	/**
	 * filters the top n items from the given input
	 *
	 * @param input
	 * @return
	 */
	public List<Scored> filter(List<? extends Scored> input) {

//        Collections.sort(input, new Comparator<Scored>() {
//            public int compare(Scored o, Scored o2) {
//                return o2.getScore().compareTo(o.getScore());
//            }
//        });

		List<Scored> result = new Vector<Scored>();

		int counter = limit;
		if (input.size() <= limit) {
			counter = input.size();
		}

		for (int i = 0; i < counter; i++) {
			result.add(input.get(i));
		}

		return result;
	}

	public Integer getLimit() {
		return limit;
	}

	public String getDescription() {
		return "this filter returns the Top " + limit + " hits, ordered by score";
	}
}


