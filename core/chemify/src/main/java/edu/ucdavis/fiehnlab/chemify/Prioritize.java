package edu.ucdavis.fiehnlab.chemify;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 4:34 PM
 */
public interface Prioritize extends Comparable<Prioritize>,Serializable{
    /**
     * am arbitrary number. The higher the value the higher the priority
     *
     * @return
     */
    Priority getPriority();
}
