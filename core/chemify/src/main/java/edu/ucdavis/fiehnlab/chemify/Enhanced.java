package edu.ucdavis.fiehnlab.chemify;

import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 10:26 PM
 * <p/>
 * basic enhanced object
 */
public interface Enhanced extends Hit {

	/**
	 * adds an enhancement
	 *
	 * @param enhancement
	 * @return
	 */
	public Enhanced addEnhancement(Enhancement enhancement);

	/**
	 * returns all registered enhancements
	 *
	 * @return
	 */
	public Set<Enhancement> getEnhancements();

	/**
	 * returns the enhancement with identifier: 'identifier'
	 *
	 * @param identifier
	 * @return
	 */
	public Enhancement getEnhancement(String identifier);
}
