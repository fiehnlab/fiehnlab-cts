package edu.ucdavis.fiehnlab.chemify.statistics;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Identify;

import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 10:21 PM
 * <p/>
 * simple statistics collector to get some stats about found or none found hits
 */
public interface StatisticsCollector {

    /**
     * will collect statistics how often certain algorithms are used
     *
     * @param result
     * @param identify
     */
    public void collect(Collection<Hit> result, Identify identify);

    /**
     * keeps track on how long it takes for this identify object
     * @param result
     * @param identify
     * @param duration
     */
    public void collect(Collection<Hit> result, Identify identify, long duration);

    /**
     * keeps track on how long it takes for this test to identify object
     * @param expected
     * @param result
     * @param duration
     */
    public void collect(String term, String expected, List<? extends Hit> result, long duration);


    /**
     * fluses all the statistics
     */
    public void clear();
}
