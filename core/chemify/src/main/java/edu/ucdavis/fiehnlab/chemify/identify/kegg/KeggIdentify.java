package edu.ucdavis.fiehnlab.chemify.identify.kegg;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.helper.constants.ConstantsRegistry;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by diego on 9/28/2014.
 * <p/>
 * KEGG COMPOUND
 * Web page      	Flat file
 * Entry	ENTRY
 * Name	NAME
 * Formula	FORMULA
 * Exact mass	EXACT_MASS
 * Mol weight	MOL_WEIGHT
 * Structure	(gif image)
 * Sequence	SEQUENCE
 * Remark	REMARK
 * Comment	COMMENT
 * Reaction	REACTION
 * Pathway	PATHWAY
 * Enzyme	ENZYME
 * Brite	BRITE
 * Reference	REFERENCE
 * Other DBs	DBLINKS
 * KCF data	ATOM / BOND / BRACKET
 */
public class KeggIdentify extends AbstractIdentify {

	/**
	 * custom constructor
	 *
	 * @param priority
	 */
	public KeggIdentify(Priority priority) {
		super(priority);

		// check compound file's date and update as needed (daily)
	}

	/**
	 * default contructor
	 */
	public KeggIdentify() {
		this(Priority.HIGH);
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {
		List<Hit> results =new ArrayList<Hit>();

		// search name with http://rest.kegg.jp/find/cpd/<name>
		// possibly use levenshtein similarity for keyword and result to filter off names
		CloseableHttpClient httpclient = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.setConnectTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.build();
		HttpGet request = new HttpGet("http://rest.kegg.jp/find/cpd/" + searchTerm);

		request.addHeader("Content-Type", "application/json");
		request.setConfig(requestConfig);
		CloseableHttpResponse response = null;

		String message="";
		try {
			response = httpclient.execute(request);

			int status = response.getStatusLine().getStatusCode();
			message = response.getStatusLine().getReasonPhrase();

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			StringBuilder respStr = new StringBuilder();
			while((line = reader.readLine())!= null) {
				respStr.append(line + "\n");
			}

			logger.debug("response: " + respStr);

			response.close();
			httpclient.close();
		} catch (IOException e) {
			logger.error("Error getting data from KEGG\n" + e.getMessage());
			return Collections.emptyList();
		}

		// get more data on compounds using kegg id and rest: http://rest.kegg.jp/get/<token from prev step>

		//create hit, add to list

		//rinse recycle

		return results;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
