package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/24/13
 * Time: 3:04 PM
 */
public class MeoxModifer extends AbstractModifier {

	protected MeoxModifer(Priority priority) {
		super(priority);
	}

	public MeoxModifer() {
		this(Priority.VERY_LOW);
	}

	/**
	 * basically replaces the whitespace with a dash
	 *
	 * @param searchTerm
	 * @return
	 */
	public String modify(String searchTerm) {
		String res = searchTerm.replaceAll(this.getPattern(), "").trim();
		return res;
	}

	public String getDescription() {
		return "removes the string 'meox', '_meox' or 'meoxN' (where N is a number) from the searchg term. This is a case insensitive modifier";
	}

	public String getPattern() {
		return CommonPatterns.MEOX_PATTERN;
	}
}
