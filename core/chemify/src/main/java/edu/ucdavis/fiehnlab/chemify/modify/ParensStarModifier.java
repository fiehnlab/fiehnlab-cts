package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created by diego on 1/28/14.
 */
public class ParensStarModifier extends AbstractModifier {
	public ParensStarModifier() {
		super(Priority.HIGH);
	}

	@Override
	public String modify(String searchTerm) {
		return searchTerm.replaceFirst(CommonPatterns.PARENS_STAR_PATTERN, "$1").trim();
	}

	@Override
	public String getDescription() {
		return "removes not needed '(*)' token from the end of the search string.\n" +
				"15(S)-HETE (*)\t->\t15(S)-HETE\n" +
				"15(S)(*)-HETE\t->\tno change\n" +
				"(*)15(S)-HETE\t->\tno change";
	}

	@Override
	public String getPattern() {
		return CommonPatterns.PARENS_STAR_PATTERN;
	}
}
