package edu.ucdavis.fiehnlab.chemify.identify;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Identify;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.annotations.RegisterOnStartup;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.helper.ReflectionHelper;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 3:41 PM
 * <p/>
 * alternative approach, it executes all registered Identity algorithms and combines there final result
 * this allows us to correct for skipped output or wrong output
 */
@RegisterOnStartup(disabled = true)
public class CombinedIdentify extends AbstractIdentify {

	private List<Identify> identifies = new Vector<Identify>();
	ApplicationContext context;

	/**
	 * sets the internal priority of this identity object
	 *
	 * @param priority
	 */
	protected CombinedIdentify(Priority priority) {
		super(priority);

	}

	public CombinedIdentify(Priority priority, List<Identify> identifies) {
		this(priority);
		this.identifies = identifies;
	}

	public CombinedIdentify() {
		this(Priority.LOW, new ReflectionHelper().registerIdentifyObjects(new Vector<Identify>()));

	}

	/**
	 * build up a combined idendity set
	 *
	 * @param identify
	 * @return
	 */
	public CombinedIdentify addIdentify(Identify identify) {
		this.identifies.add(identify);
		return this;
	}

	/**
	 * aggregates a list of all identifications and leave the scoring/filtering to be decided by a later method.
	 * <p/>
	 * Careful, can use a lot of memory!
	 *
	 * @param searchTerm
	 * @return
	 * @throws ChemifyException
	 */
	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {
		List<Hit> result = new Vector<Hit>();

		for (Identify identify : identifies) {

			List<Hit> temp = identify.identify(searchTerm, original);

			if (temp != null) {
				for (Hit hit : temp) {
					if (result.contains(hit) == false) {
						result.add(hit);
					}
				}
			}
		}

		return result;
	}

	public void setIdentifies(List<Identify> identifies) {
		this.identifies = identifies;
	}

	public String getDescription() {
		return "this combines several identity hits to a single result set";
	}
}
