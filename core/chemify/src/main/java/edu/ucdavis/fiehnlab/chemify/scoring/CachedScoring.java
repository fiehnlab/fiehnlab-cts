package edu.ucdavis.fiehnlab.chemify.scoring;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.cache.EHBasedCache;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 1:23 PM
 */
public class CachedScoring implements Scoring {

    private Scoring scoring;

    private Caching<List<Scored>> caching;

    public CachedScoring(Scoring scoring, Caching<List<Scored>> caching) {
        this.scoring = scoring;
        this.caching = caching;
    }

    /**
     * uses our default caching solution
     * @param scoring
     */
    public CachedScoring(Scoring scoring){
        this(scoring,new EHBasedCache<List<Scored>>(scoring.getClass().getName()));
    }

    /**
     * caches the scored result, using the internal scoring algorithm
     *
     * @param result
     * @param searchTerm
     * @return
     */
    public List<Scored> score(List<? extends Hit> result, String searchTerm) {


        List<Scored> scoredList = caching.queryCache(searchTerm);

        if(scoredList == null || scoredList.isEmpty()){
            scoredList = scoring.score(result, searchTerm);
            caching.cache(searchTerm,scoredList);
        }
        return scoredList;
    }


    public String getDescription() {
        return "this scoring adds a cache in front of the used scoring algorithm, to speed up the scoring";
    }

    public Priority getPriority() {
        return scoring.getPriority();
    }

    public int compareTo(Prioritize prioritize) {
        return scoring.compareTo(prioritize);
    }
}
