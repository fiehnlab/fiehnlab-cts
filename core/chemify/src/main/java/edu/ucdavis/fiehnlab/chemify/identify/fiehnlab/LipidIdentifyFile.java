package edu.ucdavis.fiehnlab.chemify.identify.fiehnlab;

import edu.ucdavis.fiehnlab.chemify.Configuration;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Modify;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.helper.ReflectionHelper;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;

import java.io.InputStream;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/23/13
 * Time: 3:24 PM
 */
public class LipidIdentifyFile extends AbstractIdentify {

	/**
	 * internal cache of our library file
	 */
	private Map<String, String> cache = new HashMap<String, String>();

	/**
	 * initialize based on a list of modifiers. which need to be the same as used by the chemify algorithm
	 *
	 * @param modifyList
	 */
	public LipidIdentifyFile(List<Modify> modifyList) {
		super(Priority.CALCULATION_BASED);
		fillCache(modifyList);
	}

	/**
	 * loads the internal cache with the content from the list
	 *
	 * @param modifyList
	 */
	protected void fillCache(List<Modify> modifyList) {
		Scanner scanner = new Scanner(getLibrary());
		cache.clear();

		if (logger.isDebugEnabled())
			logger.debug("filling cache and have to use: " + modifyList.size() + " modifiers");

		while (scanner.hasNextLine()) {
			String[] content = scanner.nextLine().split("\t");
			String key = content[0];
			String value = content[1];

			if (logger.isDebugEnabled())
				logger.debug("received key: " + key);
			for (Modify modify : modifyList) {
				key = modify.modify(key);
			}

			if (logger.isDebugEnabled())
				logger.debug("=> modified it to: " + key);

			cache.put(key, value);
		}

		scanner.close();

		if (logger.isDebugEnabled())
			logger.debug("=> size of cache: " + cache.size());
	}

	/**
	 * sets the internal priority of this identity object
	 */
	public LipidIdentifyFile() {
		super(Priority.CALCULATION_BASED);
		List<Modify> list = new Vector<Modify>();
		new ReflectionHelper().registerModifierObjects(list);
		fillCache(list);
	}

	@Override
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		if (configuration != null)
			this.fillCache(configuration.getModifications());
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {
		String result = cache.get(searchTerm);

		List<Hit> hits = new Vector<Hit>();

		if (result != null) {
			if (logger.isDebugEnabled())
				logger.debug("=> found " + result + " for search term " + searchTerm);

			Hit hit = new HitImpl(result, this, searchTerm, original);

			hits.add(hit);
		} else {
			if (logger.isDebugEnabled())
				logger.debug("=> found nothing for: " + searchTerm);
		}
		return hits;
	}

	/**
	 * returns the actual library
	 *
	 * @return
	 */
	protected InputStream getLibrary() {
		return getClass().getResourceAsStream("/fiehnlab/LipidPos.txt");
	}

	public String getDescription() {
		return "identifies the search term by using the Fiehnlab lipid/lcms database based on sodium citrate. Please be aware that it only has a subset of InChI Keys";
	}
}
