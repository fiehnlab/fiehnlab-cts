package edu.ucdavis.fiehnlab.chemify;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 11:40 PM
 *
 * Used to score our results some how to reduce the amount of data, reported to the server
 */
public interface Scoring extends Serializable, Describe, Prioritize{

    /**
     * the score methods converts the result into a scored hit to reduce the amount of data output
     * @param result
     * @return
     */
    public List<Scored> score(List<? extends Hit> result, String searchTerm);
}
