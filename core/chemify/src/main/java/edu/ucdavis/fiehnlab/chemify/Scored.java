package edu.ucdavis.fiehnlab.chemify;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 10:33 PM

 * a scored object
 */
public interface Scored extends Hit, Enhanced {

    /**
     * the used scoring algorithm
     * @return
     */
    Scoring getScoringAlgorithm();

    /**
     * the used score
     * @return
     */
    Double getScore();
}
