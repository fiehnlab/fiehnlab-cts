package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/24/13
 * Time: 2:18 PM
 */
public class SpaceNumberModifier extends AbstractModifier {
	Logger logger = Logger.getLogger(this.getClass());

    public SpaceNumberModifier(){
        super(Priority.CALCULATION_BASED);
    }

    public String modify(String searchTerm) {
	    String result = searchTerm.replaceFirst(CommonPatterns.SPACE_NUMBER_PATTERN, "$1").trim();
	    StringBuffer msg = new StringBuffer();

	    msg.append("modified: ").append(searchTerm).append(" -> ").append(result);
	    logger.debug(msg);
	    return result;
    }

    public String getDescription() {
        return "this modifier removes the ' x' (x being a number) from the search string";
    }

    public String getPattern() {
        return CommonPatterns.SPACE_NUMBER_PATTERN;
    }
}
