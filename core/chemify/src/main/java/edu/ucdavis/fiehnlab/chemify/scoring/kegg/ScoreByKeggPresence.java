package edu.ucdavis.fiehnlab.chemify.scoring.kegg;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Prioritize;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.Scored;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import edu.ucdavis.fiehnlab.chemify.scoring.AbstractCombinedScoring;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by diego on 10/17/14.
 */
public class ScoreByKeggPresence extends AbstractCombinedScoring {
	Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	private static final String KEGGFILE = "src/main/resources/inchis_kegs.txt";
	private static Map<String, String> contents = new HashMap<String, String>();

	static {
		scanFile();
	}

	/**
	 * the score methods converts the result into a scored hit to reduce the amount of data output
	 *
	 * @param input
	 * @param searchTerm
	 * @return
	 */
	@Override
	public List<Scored> score(List<? extends Hit> input, String searchTerm) {
		List<Scored> results = new ArrayList<Scored>();

		for (Hit hit : input) {
			if (contents.containsKey(hit.getInchiKey())) {

				String kid = contents.get(hit.getInchiKey()).trim();
//				PubchemRester.hasKeggForInchikey(hit.getInchiKey());

				if (kid != "") {
					logger.debug("has kegg id: " + kid);
					results.add(new ScoredHit(hit, this, 1.0));
				} else {
					logger.debug("has kegg id: " + kid);
					results.add(new ScoredHit(hit, this, 0.0));
				}
			}
		}

		return results;
	}

	private static void scanFile() {
		try {
			final Scanner scanner = new Scanner(new File(KEGGFILE));

			while (scanner.hasNextLine()) {
				final String lineFromFile = scanner.nextLine();
				String[] item = lineFromFile.split("\t");
				contents.put(item[0].trim(), item[1].trim());
			}
		} catch (IOException e) {
			Logger.getLogger(ScoreByKeggPresence.class).error("cant get kegg information", e);
		}
	}

	/**
	 * get temporary score metrics to create the scoredHits
	 *
	 * @return a map with inchikey: value pairs
	 */
	@Override
	protected String getMetric() {
		return "kegg";
	}

	/**
	 * am arbitrary number. The higher the value the higher the priority
	 *
	 * @return
	 */
	@Override
	public Priority getPriority() {
		return Priority.MEDIUM;
	}

	@Override
	public int compareTo(Prioritize prioritize) {
		return this.getPriority().compareTo(prioritize.getPriority());
	}

	/**
	 * the description of this object
	 *
	 * @return
	 */
	@Override
	public String getDescription() {
		return "Scores the hit by looking for a keg id, if its found the score is 1 if not the score is 0";
	}

	@Override
	public Integer getWeight() {
		return this.weight;
	}
}
