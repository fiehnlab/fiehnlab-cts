package edu.ucdavis.fiehnlab.chemify;

import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/10/13
 * Time: 4:21 PM
 *
 * a simple interface to modify search terms in the algorithm to ensure that
 */
public interface Modify extends Prioritize, Describe, Patternized {

    /**
     * modifies the search term based on incoming data, to ensure that it's compatible with this algorithm
     * @param searchTeam
     * @return
     */
    public String modify(String searchTeam);
}
