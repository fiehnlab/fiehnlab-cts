package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 3:52 PM
 * <p/>
 * if a search term ends with ISTD we assume it's an internal standard
 */
public class ISTDModifier extends AbstractModifier {

	public ISTDModifier() {
        super(Priority.HIGH);
    }

    public String modify(String searchTeam) {
        return searchTeam.replaceFirst(CommonPatterns.ISTD_PATTERN, "$1").trim();
    }

    public String getDescription() {
        return "removes not needed ISTD String from the search term, since this marks an internal standard";
    }

    /**
     * returns the pre defined pattern for this implementation
     *
     * @return
     */
    public String getPattern() {
        return CommonPatterns.ISTD_PATTERN;
    }
}
