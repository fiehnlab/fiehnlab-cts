package edu.ucdavis.fiehnlab.chemify.exception;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/15/2013
 * Time: 7:14 PM
 */
public class ChemifyConfigurationException extends ChemifyException{
    public ChemifyConfigurationException(String s) {
        super(s);
    }

    public ChemifyConfigurationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
