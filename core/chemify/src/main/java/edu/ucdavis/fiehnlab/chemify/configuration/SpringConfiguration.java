package edu.ucdavis.fiehnlab.chemify.configuration;

import edu.ucdavis.fiehnlab.chemify.Configuration;
import edu.ucdavis.fiehnlab.chemify.Filter;
import edu.ucdavis.fiehnlab.chemify.Scoring;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by diego on 4/24/14.
 */
public class SpringConfiguration extends SimpleConfiguration {
	private ApplicationContext context;

	public SpringConfiguration() {
		super();
	}

	public SpringConfiguration(String confFile) {
		super();
		context = new ClassPathXmlApplicationContext(confFile);

		this.setNonIdentifiedResultsPermitted(true);

		Configuration defConf = (SimpleConfiguration) context.getBean("defaultConfiguration");

		this.setSearchTermMinQuality(defConf.getSearchTermMinQuality());
		this.setLipidmapsPath(defConf.getLipidmapsPath());

		this.addFilter((Filter) context.getBean("top5Filter"));
		this.setAlgorithms((ArrayList) context.getBean("algorithms"));
		this.addScoringAlgorithm((Scoring) context.getBean("similarityScoring"));
//		this.addScoringAlgorithm((Scoring) context.getBean("scoreByBiologicalCount"));
		this.setMultiplexer((HashSet) context.getBean("multiplexers"));
		this.setModifications((ArrayList) context.getBean("modifiers"));
	}
}
