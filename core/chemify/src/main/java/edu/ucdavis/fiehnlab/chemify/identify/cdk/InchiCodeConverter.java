package edu.ucdavis.fiehnlab.chemify.identify.cdk;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.InvalidSearchTermException;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.inchi.InChIToStructure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/26/13
 * Time: 10:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class InchiCodeConverter extends AbstractIdentify implements Patternized {

	/**
	 * defines a pattern for a standard inchi
	 */
	public final static String STD_INCHI_PATTERN = CommonPatterns.STD_INCHI_PATTERN;

	public InchiCodeConverter(Priority priority) {
		super(priority);
	}

	/**
	 * uses the default cdk builder
	 */
	public InchiCodeConverter() {
		this(Priority.CALCULATION_BASED);
	}

	@Override
	protected List<Hit> doIdentify(String searchString, String original) throws InvalidSearchTermException {
		List<Hit> results = new ArrayList<Hit>();

		if (searchString == null || searchString.isEmpty()) {
			throw new InvalidSearchTermException("InChI Code can't be null or empty");
		}

		try {
			InChIToStructure parser = InChIGeneratorFactory.getInstance().getInChIToStructure(searchString, DefaultChemObjectBuilder.getInstance());
			InChIGenerator gen = InChIGeneratorFactory.getInstance().getInChIGenerator(parser.getAtomContainer());

			String key = gen.getInchiKey();

			results.add(new HitImpl(key, this, searchString, original));

		} catch (CDKException e) {
			System.out.println("Error converting '" + searchString + "' to InChIKey.");
			System.out.println(e.getMessage());
		} catch (Exception e) {
			throw new InvalidSearchTermException("Invalid InChI Code: " + searchString);
		}

		return results;
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return STD_INCHI_PATTERN;
	}


	public String getDescription() {
		return "this method utilizes the CDK tools to convert a search term to a valid InChI Code";
	}
}
