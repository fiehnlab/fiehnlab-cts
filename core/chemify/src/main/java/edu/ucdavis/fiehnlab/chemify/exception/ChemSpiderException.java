package edu.ucdavis.fiehnlab.chemify.exception;

/**
 * Created by diego on 10/29/14.
 */
public class ChemSpiderException extends RuntimeException {
	public ChemSpiderException(String msg) {
		super(msg);
	}

	public ChemSpiderException (String msg, Throwable exception) {
		super(msg, exception);
	}
}
