package edu.ucdavis.fiehnlab.chemify;

import java.util.Set;

/**
 *
 * takes one string and generates a bunch of different strings out of it to provide us with more possible search terms
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/30/14
 * Time: 3:05 PM
 */
public interface Multiplexer extends Describe {

    /**
     * returns a set of unique strings based on this search term
     * @param searchTerm
     * @return
     */
    Set<String> multiplex(String searchTerm);
}
