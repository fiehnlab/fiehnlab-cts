package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

import java.util.regex.Matcher;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/18/13
 * Time: 3:56 PM
 */
public class LipidModifier extends AbstractLipidModifier {

	public String getDescription() {
		return "ensures that the search term is a valid glycerolipid or fatty acid string and re-formats it to a valid glycerolipid abbreviation";
	}

	protected String refine(Matcher matcher) {
		if (logger.isDebugEnabled())
			logger.debug("=> is a glicerolipid");

		String result = matcher.group(1).trim();

		String prefix = "";
		if (matcher.group(2) != null) {
			prefix = matcher.group(2) + matcher.group(3);
		} else {
			prefix = matcher.group(3);
		}

		if (logger.isTraceEnabled()) {
			logger.trace("=> extracted value is: " + result);

			logger.trace("groups: " + matcher.groupCount());
			for (int i = 1; i <= matcher.groupCount(); i++) {
				logger.debug("group" + i + ": " + matcher.group(i));
			}
		}

		//if there are no brackets, we need to add brackets
		if (matcher.group(4).indexOf("(") != 0 && matcher.group(4).endsWith(")") == false) {
			if (logger.isDebugEnabled())
				logger.debug("=> contains no parentheses, which will be added!");
			result = prefix + "(" + matcher.group(4) + ")";
		}

		//if there is a space after the Lipid-Class name (first 2-3 characters remove it
		if (result.indexOf(" ") == prefix.trim().length()) {

			if (logger.isDebugEnabled())
				logger.debug("=> contains a space, which will be removed!");

			result = result.replaceFirst(" ", "");
		}

		// removes any space after the first parenthesis following the class name
		if (result.contains(matcher.group(3) + "( ")) {
			result = result.replaceFirst("\\(\\s", "(");
		}

		//TODO find out what to do with substitutions outside the chain definition
		// add post chain info
		if (matcher.group(6) != null) {
			//result = result + matcher.group(5);      ignore for now
		}

		logger.debug("=> modified result is: " + result);

		//all modifications are done, let's return the result
		return result;
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.GLICEROLIPIDS_PATTERN;
	}

}
