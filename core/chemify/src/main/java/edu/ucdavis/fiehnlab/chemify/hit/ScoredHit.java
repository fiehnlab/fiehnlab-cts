
package edu.ucdavis.fiehnlab.chemify.hit;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Identify;
import edu.ucdavis.fiehnlab.chemify.Scored;
import edu.ucdavis.fiehnlab.chemify.Scoring;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 11:42 PM
 * <p/>
 * A scored, identified hit,
 */
public class ScoredHit extends HitImpl implements Scored {

	public Scoring getScoringAlgorithm() {
		return scoringAlgorithm;
	}

	public Double getScore() {
		return score;
	}

	/**
	 * used scoring algorithm
	 */
	private final Scoring scoringAlgorithm;

	/**
	 * the score
	 */
	private final Double score;

	/**
	 * constructor
	 *
	 * @param inchiKey
	 * @param identify
	 * @param searchTerm
	 * @param originalTerm searchTerm before multiplexing
	 */
	public ScoredHit(String inchiKey, Identify identify, String searchTerm, Scoring scoringAlgorithm, Double score, String originalTerm) {
		super(inchiKey, identify, searchTerm, originalTerm);

		if (scoringAlgorithm == null) throw new IllegalArgumentException("the scoring algorithm cannot be null, please ensure you set one!");
		if ((score >= 0 && score <= 1) == false)
			throw new IllegalArgumentException("Score " + score + " not valid. The score has to be between 0 and 1 to be acceptable");

		this.scoringAlgorithm = scoringAlgorithm;
		this.score = score;
	}

	/**
	 * generates a new hit based on the original hit
	 *
	 * @param hit
	 */
	public ScoredHit(Hit hit, Scoring scoringAlgorithm, Double score) {
		this(hit.getInchiKey(), hit.getUsedAlgorithm(), hit.getSearchTerm(), scoringAlgorithm, score, hit.getOriginalTerm());
	}

	public String toString() {
		return getInchiKey() + " [" + getScoringAlgorithm() + "/" + getScore() + "]";
	}
}
