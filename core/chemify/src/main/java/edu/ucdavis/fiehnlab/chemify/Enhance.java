package edu.ucdavis.fiehnlab.chemify;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/26/13
 * Time: 10:06 PM
 *
 * Enhances a result and tries to add metadata to it. This could be further attributes or convertions to a hit or other properties
 */
public interface Enhance {

    /**
     * iterates over the list of scored hits and enhances them with a couple of meta data of the type @see edu.ucdavis.fiehnlab.chemify.enhance.Enhance
     * @param hit
     * @return a list of enhanced hits
     */
    public List<? extends Enhanced> enhance(List<? extends Hit> hit);
}
