package edu.ucdavis.fiehnlab.chemify.exception;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/26/13
 * Time: 2:29 PM
 * <p/>
 * someting goes wrong in chemify and exceptions are cool
 */
public abstract class ChemifyException extends RuntimeException {
	public ChemifyException(String s) {
		super(s);
	}

	public ChemifyException(String s, Throwable throwable) {
		super(s, throwable);
	}
}
