package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 12:44 PM
 *
 * removes unnecessary whitespaces from query terms
 */
public class TrimModifier extends AbstractModifier{

    public TrimModifier(){
        super(Priority.LOWEST);
    }

    public String modify(String searchTeam) {
        return searchTeam.trim();
    }

    public String getDescription() {
        return "removes not needed white spaces at the begin and the end of the search term";
    }

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.TRIM_PATTERN;
	}
}
