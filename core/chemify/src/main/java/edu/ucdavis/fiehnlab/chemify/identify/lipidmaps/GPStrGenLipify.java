package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:19 AM
 */
public class GPStrGenLipify extends AbstractLipify {
	@Override
	protected String getPerlScriptName() {
		return "GPStrGen.pl";
	}

	public String getPattern() {
		return CommonPatterns.PHOSPHOLIPIDS_PATTERN;
	}
}
