package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/14/13
 * Time: 3:52 PM
 *
 * if a search term ends with 'major' we remove that substring
 */
public class MajorModifier extends AbstractModifier {

	public MajorModifier() {
		super(Priority.HIGH);
	}

	public String modify(String searchTerm) {
		return searchTerm.replaceFirst(CommonPatterns.MAJOR_PATTERN, "$1").trim();
	}

	public String getDescription() {
		return "removes not needed 'major *' String from the search term";
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.MAJOR_PATTERN;
	}
}
