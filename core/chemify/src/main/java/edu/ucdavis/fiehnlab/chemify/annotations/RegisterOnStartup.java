package edu.ucdavis.fiehnlab.chemify.annotations;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 1:31 PM
 *
 * should an indentify implementation automatically loaded during started
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface RegisterOnStartup {
    /**
     * is this identify object enabled to register it self during startup
     * @return
     */
    boolean disabled() default false;
}
