package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created by diego on 6/4/2014.
 */
public class DehydratedModifier extends AbstractModifier {

	public DehydratedModifier() {
		super(Priority.HIGH);
	}

	@Override
	public String modify(String searchTerm) {
		String res = searchTerm.replaceAll(this.getPattern(), "").trim();
		return res;
	}

	@Override
	public String getDescription() {
		return "removes all occurrences of 'dehydrated' or '(dehydrated)' from the search term. this is a case insensitive modifier";
	}

	@Override
	public String getPattern() {
		return CommonPatterns.DEHYDRATED_PATTERN;
	}
}
