package edu.ucdavis.fiehnlab.chemify.scoring;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;

import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 11:54 PM
 * <p/>
 * just copies the list without any scoring done
 */
public class NoScoringDone implements Scoring {

	/**
	 * just converts the list
	 *
	 * @param result
	 * @return
	 */
	public List<Scored> score(List<? extends Hit> result, String searchTerm) {

		List<Scored> list;
		list = new Vector<Scored>();

		for (Hit hit : result) {
			list.add(new ScoredHit(hit.getInchiKey(), hit.getUsedAlgorithm(), hit.getSearchTerm(), this, 0.0, hit.getOriginalTerm()));
		}
		return list;
	}


	public String getDescription() {
		return "this method does not do any scoring fo the result term";
	}

	public Priority getPriority() {
		return Priority.NO_RESULT_POSSIBLE;
	}

	public int compareTo(Prioritize prioritize) {
		return getPriority().compareTo(prioritize.getPriority());
	}
}
