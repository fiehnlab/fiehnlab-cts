package edu.ucdavis.fiehnlab.chemify.statistics;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Identify;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by diego on 9/29/14.
 */
public class TestDataCollector implements StatisticsCollector {

	List<String> data = new ArrayList<String>();
	int total = 0;
	int pass = 0;
	int partial = 0;
	String tester = null;

	private static TestDataCollector instance = null;

	private TestDataCollector() {
	}

	public static TestDataCollector getInstance() {
		if (null == instance) {
			instance = new TestDataCollector();
			instance.clear();
		}

		return instance;
	}

	/**
	 * will collect statistics how often certain algorithms are used
	 *
	 * @param result
	 * @param identify
	 */
	@Override
	public void collect(Collection<Hit> result, Identify identify) {
	}

	/**
	 * keeps track on how long it takes for this identify object
	 *
	 * @param result
	 * @param identify
	 * @param duration
	 */
	@Override
	public void collect(Collection<Hit> result, Identify identify, long duration) {
	}

	/**
	 * keeps track on how long it takes for this test to identify object
	 *
	 * @param expected
	 * @param result
	 * @param duration
	 */
	@Override
	public void collect(String term, String expected, List<? extends Hit> result, long duration) {
		String testStatus = "FAIL; expected not in results";

		String message = "";

		if (result.size() > 0) {
			String algorythm = result.get(0).getUsedAlgorithm().getClass().getSimpleName();

			total++;
			int size = (result.size() > 10) ? 10 : result.size();
			String gotInchi = result.get(0).getInchiKey();
			if (expected.equals(gotInchi)) {
				testStatus = "PASS; first hit";
				pass++;
			} else {
				List<? extends Hit> hits = result.subList(1, size);
				List<String> inchis = new ArrayList<String>();
				int count=0;
				for (Hit hit : hits) {
					if (hit.getInchiKey().equals(expected)) {
						testStatus = "fail; appeared in the top " + size + " ("+count+")";
						partial++;
					}
					count++;
				}

			}
			//Search term; we wanted; got; identifier; time (s); status; notes
			message = term + "; " + expected + "; " + gotInchi + "; " + algorythm + "; " + duration / 1000000 + "; " + testStatus;
		} else {
			message = term + "; " + expected + "; no results; nothing identified;" + duration / 1000000 + "; FAIL; no results";
		}

		data.add(message);
	}

	/**
	 * fluses all the statistics
	 */
	@Override
	public void clear() {
		this.data.clear();
		this.total = 0;
		this.pass = 0;
		this.tester=null;
	}

	public void printData() {
		Logger.getAnonymousLogger().info("Total tests: " + total + "\n" +
				"Passing: " + pass + " (" + pass * 100 / total + "%)\n" +
				"Unsorted but in results: " + partial + " (" + partial * 100 / total + "%)");

		for (String entry : data) {
			System.out.println(entry);
		}
	}

	public void saveData() {
		File fout = new File("src/test/resources/ValidataionData-"+ tester +".txt");

		try {
			FileWriter fw = new FileWriter(fout);

			fw.write("Total tests: " + total + "\n" +
					"Passing: " + pass + " (" + pass * 100 / total + "%)\n" +
					"Unsorted but in results: " + partial + " (" + partial * 100 / total + "%)\n");
			//Search term; we wanted; got; identifier; time (s); status; notes
			fw.write("Search term; expected; actual; identifier; time (s); status; notes\n");
			for (String line : data) {
				fw.write(line + "\n");
			}

			fw.close();
			System.out.println("Data saved.");
		} catch (IOException e) {
			System.out.println("Can't save data...");
		}
	}

	public List<String> getData() {
		return data;
	}

	public int getTotal() {
		return total;
	}

	public int getPass() {
		return pass;
	}

	public int getPartial() {
		return partial;
	}

	public String getTester() {
		return tester;
	}

	public void setTester(String tester) {
		this.tester = tester;
	}

	public int getPct() {
		return (pass * 100 / total);
	}
}
