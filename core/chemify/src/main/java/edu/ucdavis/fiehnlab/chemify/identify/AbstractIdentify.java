package edu.ucdavis.fiehnlab.chemify.identify;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.cache.EHBasedCache;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyAlgorithmWrongImplementationException;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.statistics.SimpleStatisticsCollectorImpl;
import edu.ucdavis.fiehnlab.chemify.statistics.StatisticsCollector;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 7:25 PM
 * <p/>
 * Helper class to simplify identify objects
 */
public abstract class AbstractIdentify implements Identify {

	/**
	 * used for logging of stuff. Please ensure that you check for DEBUG and TRACE enabled to avoid not neccesaery operations
	 */
	protected transient Logger logger = Logger.getLogger(this.getClass().getName());

	private transient Caching<List<Hit>> cache;

	public Identify setCache(Caching<List<Hit>> caching) {
		this.cache = caching;

		return this;
	}

	/**
	 * internal configuration object. Not required
	 */
	protected transient Configuration configuration;

	/**
	 * associated statistics element
	 */
	private transient StatisticsCollector statisticsCollector;

	/**
	 * provides access to the statistics collector
	 *
	 * @return
	 */
	public StatisticsCollector getStatisticsCollector() {
		return this.statisticsCollector;
	}

	/**
	 * internal priority
	 */
	private Priority priority;

	/**
	 * sets the internal priority of this identity object
	 *
	 * @param priority
	 */
	protected AbstractIdentify(Priority priority) {
		this.setPriority(priority);
		this.statisticsCollector = SimpleStatisticsCollectorImpl.getInstance();
		this.cache = new EHBasedCache<List<Hit>>(this.getClass().getName());
	}

	/**
	 * compare by priority
	 *
	 * @param identify
	 * @return
	 */
	public int compareTo(Prioritize identify) {
		return getPriority().compareTo(identify.getPriority());
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public boolean hasConfiguration() {
		return this.getConfiguration() != null;
	}

	/**
	 * set's the priority of this identify object
	 *
	 * @param value
	 */
	protected void setPriority(Priority value) {
		this.priority = value;
	}

	/**
	 * return the priority
	 *
	 * @return
	 */
	public Priority getPriority() {
		return this.priority;
	}

	public String toString() {
		return this.getClass().getSimpleName() + "[" + this.getPriority() + "/" + this.hasConfiguration() + "]";
	}

	/**
	 * in case we need to add later fucntionality we delegate to an abstract method
	 *
	 * @param searchTerm
	 * @return
	 */
	public final List<Hit> identify(String searchTerm, String original) throws ChemifyException {
		if (logger.isDebugEnabled())
			logger.debug("search term: " + searchTerm);

		long begin = System.nanoTime();

		List<Hit> result = cache.queryCache(searchTerm);

		if (result == null || result.isEmpty()) {
//			if (logger.isDebugEnabled())
//				logger.debug("=> not in cache need to search against filter");

			try {
				logger.info("identifying " + searchTerm + " with " + this.getClass().getSimpleName());
				result = doIdentify(searchTerm, original);
			} catch(Exception e) {
				logger.error(e.getMessage());
				result = Collections.EMPTY_LIST;
			}

			if (result == null) {
				throw new ChemifyAlgorithmWrongImplementationException("result can never be null!");
			}
			//we only cache in case it's not empty
			if (result != null && result.isEmpty() == false) {
//				if (logger.isDebugEnabled())
//					logger.debug("=> adding to cache");

				cache.cache(searchTerm, result);
			}

//		} else {
//			if (logger.isDebugEnabled())
//				logger.debug("=> found in cache");
		}

		long end = System.nanoTime();
		long used = (end - begin) / (1000^2);

		if (logger.isDebugEnabled()) {
			logger.debug("=> result: " + result);
			logger.debug("=> took: " + used + " ms");
		}

		this.getStatisticsCollector().collect(result, this, used);

		return result;
	}

	/**
	 * does the actual work
	 *
	 * @param searchTerm
	 * @return
	 */
	protected abstract List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException;

}

