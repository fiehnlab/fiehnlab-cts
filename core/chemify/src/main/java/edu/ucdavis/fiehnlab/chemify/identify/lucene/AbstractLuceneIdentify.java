package edu.ucdavis.fiehnlab.chemify.identify.lucene;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyConfigurationException;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexNotFoundException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/15/2013
 * Time: 7:06 PM
 * <p/>
 * A simialrity based implementation, which uses a lucene index
 */
public abstract class AbstractLuceneIdentify extends AbstractIdentify {

	private static final int MAX_SEARCH_RESULT = 10;

	public static File getIndexFile() throws IOException {
		File file = new File(File.createTempFile("chemify", "index").getParentFile(), "chemify");
		file.mkdirs();

		return file;
	}

	/**
	 * sets the internal priority of this identity object
	 *
	 * @param priority
	 */
	protected AbstractLuceneIdentify(Priority priority) {
		super(priority);
	}

	public AbstractLuceneIdentify() {
		this(Priority.LOW);
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {

		List<Hit> result = new Vector<Hit>();

		try {


			IndexSearcher searcher = new IndexSearcher(IndexReader.open(FSDirectory.open(getIndexFile())));

			TopDocs docs = searcher.search(getQuery(searchTerm), MAX_SEARCH_RESULT);
			logger.debug("=> total results: " + docs.totalHits);

			ScoreDoc[] hits = docs.scoreDocs;

			for (int i = 0; i < hits.length; ++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				logger.debug("hit: " + d.get("name") + " - inchi - " + d.get("inchiKey") + " score: " + hits[i].score);

				Hit hit = new HitImpl(d.get("inchiKey"), this, searchTerm, original);

				if (result.contains(hit) == false)
					result.add(hit);
			}

		} catch (IndexNotFoundException e) {
			logger.warn("looks like no index was defined, return empty result!");
			return result;
		} catch (IOException e) {
			throw new ChemifyConfigurationException(e.getMessage(), e);
		}

		return result;
	}

	/**
	 * builds a query
	 *
	 * @return
	 */
	public abstract Query getQuery(String query);
}
