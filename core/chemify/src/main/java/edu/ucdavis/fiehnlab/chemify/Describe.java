package edu.ucdavis.fiehnlab.chemify;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/21/13
 * Time: 4:38 PM
 *
 * returns the description what a certain class does
 */
public interface Describe {

    /**
     * the description of this object
     * @return
     */
    public String getDescription();
}
