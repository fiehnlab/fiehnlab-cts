package edu.ucdavis.fiehnlab.chemify;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/8/13
 * Time: 11:47 AM
 * Defines standard priories for chemify algorithms and should allow you to assign correctly based priorities, by usage of responding approaches.
 */
public enum Priority implements Comparable<Priority>{

    /**
     * highest possible priority
     */
    HIGHEST(Double.MAX_VALUE),

    /**
     * based on a calculation
     */
    CALCULATION_BASED(HIGHEST.priority - 100),

    /**
     * a very high priority
     */
    VERY_HIGH(10000.0),

    /**
     * do we use a pattern to pre filter results and so have a rather small data amount
     */
    PATTERN_BASED_APPROACH(VERY_HIGH.priority - 50),
    /**
     * a high priority
     */
    HIGH(1000.0),

    /**
     * based on an exact search quickly returning algorithm
     */
    EXACT_SEARCH_BASED(HIGH.priority - 100),
    /**
     * medium priority for simple alogirthm
     */
    MEDIUM(0.0),
    /**
     * based on an internal webservice
     */
    WEB_SERVICE_BASED_INTERNAL(MEDIUM.priority - 50),
    /**
     * based on an external webservice
     */
    WEB_SERVICE_BASED_EXTERNAL(MEDIUM.priority - 100),

    /**
     * a rather low priority for resource intensive searches
     */
    LOW(-1000.0),

    /**
     * similarity based searches
     */
    SIMILARITY_BASED_SEARCH(LOW.priority - 100.0),

    /**
     * very low
     */
    VERY_LOW(-10000.0),
    /**
     * lowest possible priority
     */
    LOWEST(Double.MIN_VALUE + 1),
    /**
     * should not be used in anything except for @link NothingIdentifiedAtAll
     */
    NO_RESULT_POSSIBLE(Double.MIN_VALUE);





    //the default priority for database searches
    private final Double priority;

    //default constructor
    Priority(Double value){
        this.priority = value;
    }

}