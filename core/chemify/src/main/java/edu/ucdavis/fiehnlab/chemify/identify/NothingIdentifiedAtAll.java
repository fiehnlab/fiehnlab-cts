package edu.ucdavis.fiehnlab.chemify.identify;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;

import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 9:43 PM
 * stupid class which just tells us nothing was identified for this search term
 */
public class NothingIdentifiedAtAll extends AbstractIdentify {

	/**
	 * priority is set to @see Double.MIN_VALUE since it should always be run last
	 */
	public NothingIdentifiedAtAll() {
		super(Priority.NO_RESULT_POSSIBLE);
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) {
		List<Hit> result = new Vector<Hit>();
		result.add(new NoHit(searchTerm, original));
		return result;
	}

	/**
	 * nothing found
	 */
	protected class NoHit extends HitImpl {

		/**
		 * constructor
		 *
		 * @param searchTerm
		 */
		public NoHit(String searchTerm, String original) {
			super("nothing found", NothingIdentifiedAtAll.this, searchTerm, original);
		}
	}


	public String getDescription() {
		return "this declares that there was no identification possible";
	}
}
