package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.AbstractPrioritize;
import edu.ucdavis.fiehnlab.chemify.Modify;
import edu.ucdavis.fiehnlab.chemify.Priority;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/24/13
 * Time: 3:05 PM
 */
public abstract class AbstractModifier extends AbstractPrioritize implements Modify{

    protected AbstractModifier(Priority priority){
        super(priority);
    }
}
