package edu.ucdavis.fiehnlab.chemify.cache;

import edu.ucdavis.fiehnlab.chemify.Caching;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/12/2013
 * Time: 8:44 PM
 * <p/>
 * Uses EHCache internally to provide us with some form of caching.
 */
public class EHBasedCache<T> implements Caching<T> {

    /**
     * internal EH Cache
     */
    private final Cache cache;

    /**
     * most simple constructor to generate a simple cahce
     */
    public EHBasedCache(String identifier) {

        CacheManager manager = CacheManager.create();

        manager.addCacheIfAbsent(identifier);
        cache = manager.getCache(identifier);

        CacheConfiguration config = cache.getCacheConfiguration();
        config.setTimeToIdleSeconds(600);
        config.setTimeToLiveSeconds(1200);
        config.setMaxEntriesLocalHeap(1000);
        config.setMaxEntriesLocalDisk(1000000);

    }

    /**
     * adds an element to the cache
     *
     * @param searchTerm
     * @param result
     */
    public void cache(String searchTerm, T result) {
        cache.put(new Element(searchTerm, result));
    }

    /**
     * queries the cache
     *
     * @param searchTerm
     * @return
     */
    public T queryCache(String searchTerm) {
        Element e = cache.get(searchTerm);

        if (e != null) {
            return (T) e.getObjectValue();
        }
        return null;
    }

    public void clear() {
        cache.removeAll();
    }
}
