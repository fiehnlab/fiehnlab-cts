package edu.ucdavis.fiehnlab.chemify.io;

import edu.ucdavis.fiehnlab.chemify.Chemify;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.configuration.SimpleConfiguration;
import edu.ucdavis.fiehnlab.chemify.statistics.SimpleStatisticsCollectorImpl;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.*;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/30/13
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopHitChemify extends Chemify {

	private Logger logger = Logger.getLogger(getClass());
	private HitSerializerTSVImpl serializer = new HitSerializerTSVImpl();
	ApplicationContext context;

	public TopHitChemify() {
		this("defaultConf.xml");
	}

	public TopHitChemify(String confFile) {
		logger.debug("Creating SpringConfiguredChemify " + confFile);
		context = new ClassPathXmlApplicationContext(confFile);
		this.setDefaultConfiguration((SimpleConfiguration)context.getBean("topHitConfiguration"));

	}


	/**
	 * reads one compound name from each file and writes it out to the writer. Trying to be as memory efficient as possible
	 *
	 * @param reader
	 * @param writer
	 */
	public void identify(Reader reader, Writer writer) throws IOException {
		Scanner scanner = new Scanner(reader);

		while (scanner.hasNextLine()) {
			String entry = scanner.nextLine();

			if (entry.isEmpty()) {
				continue;
			}

			logger.info("reading: " + entry);
			List<? extends Hit> result = identify(entry);

			logger.info("=> received hits: " + result.size());
			serializer.serialize(writer, result);

		}

		writer.flush();
		writer.close();

		logger.info("identification is done");
	}

	/**
	 * main method to run this
	 *
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
			System.out.println("Usage: ");
			System.out.println("\tjava -jar SpringIdentifier-(version).jar <input file> <output file>");
			System.out.println("");
			System.out.println("Please provide an input and output file as arguments separated by space.");
			System.out.println("");

			System.exit(-1);
		}

		TopHitChemify identifier = new TopHitChemify();
		identifier.identify(new FileReader(args[0]), new FileWriter(args[1]));

		System.out.println(SimpleStatisticsCollectorImpl.getInstance().toString());

		System.exit(0);
	}
}
