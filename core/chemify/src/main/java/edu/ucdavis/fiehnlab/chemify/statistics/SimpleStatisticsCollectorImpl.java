package edu.ucdavis.fiehnlab.chemify.statistics;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Identify;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 10:25 PM
 * most simples statistics collector possible, just counts how often which identifier is invoked
 */
public class SimpleStatisticsCollectorImpl implements StatisticsCollector {

    /**
     * little internal collector
     */
    private final Map<String, Integer> counter = new ConcurrentHashMap<String, Integer>();
    private final Map<String, Long> times = new ConcurrentHashMap<String, Long>();


    private static SimpleStatisticsCollectorImpl instance = null;

    private SimpleStatisticsCollectorImpl() {
    }

    /**
     * just a single ton
     *
     * @return
     */
    public static SimpleStatisticsCollectorImpl getInstance() {
        if (instance == null) {
            instance = new SimpleStatisticsCollectorImpl();
        }
        return instance;
    }

    public void collect(Collection<Hit> result, Identify identify) {
        String clazz = identify.getClass().getName();

        if (counter.containsKey(clazz) == false) {
            counter.put(clazz, 0);
        }

        if (result != null) {
            if (result.size() > 0) {
                counter.put(clazz, counter.get(clazz) + 1);
            }
        }
    }

    public void collect(Collection<Hit> result, Identify identify, long duration) {
        collect(result, identify);

        String clazz = identify.getClass().getName();

        if (times.containsKey(clazz) == false) {
            times.put(clazz, duration);
        } else {
            times.put(clazz, (duration + times.get(clazz)) / times.size());
        }

    }

	/**
	 * keeps track on how long it takes for this test to identify object
	 *
	 * @param expected
	 * @param result
	 * @param duration
	 */
	@Override
	public void collect(String term, String expected, List<? extends Hit> result, long duration) {
		//not implemented
	}

	/**
     * resets the counter
     */
    public void clear() {
        counter.clear();
    }

    public String toString() {
        return this.getClass().getSimpleName() + " " + counter.toString() + " times: " + times.toString();
    }
}
