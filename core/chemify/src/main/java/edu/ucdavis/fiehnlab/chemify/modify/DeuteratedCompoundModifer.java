package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/24/13
 * Time: 3:04 PM
 */
public class DeuteratedCompoundModifer extends AbstractModifier {

	private Pattern pattern = Pattern.compile(getPattern());

    protected DeuteratedCompoundModifer(Priority priority) {
        super(priority);
    }

    public DeuteratedCompoundModifer(){
        this(Priority.VERY_LOW);
    }

    /**
     * basically replaces the whitespace with a dash
     * @param searchTeam
     * @return
     */
    public String modify(String searchTeam) {
        Matcher matcher = pattern.matcher(searchTeam);

        if(matcher.matches()){
            return matcher.group(1).trim() + "-" + matcher.group(2).trim();
        }

        return searchTeam;
    }

    public String getDescription() {
        return "format deuterated compounds correctly, so that they can be found in databases. This only applies for compounds which look like 'name d7' or so and will not work on lipid abbreviations." +
		        "The resulting string will look like name-d7";
    }

    public String getPattern() {
        return CommonPatterns.DEUTERATED_PATTERN;
    }
}
