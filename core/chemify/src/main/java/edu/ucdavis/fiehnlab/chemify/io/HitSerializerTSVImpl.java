package edu.ucdavis.fiehnlab.chemify.io;

import edu.ucdavis.fiehnlab.chemify.Enhanced;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Scored;
import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/24/13
 * Time: 12:58 PM
 * <p/>
 * a simple class serialzie hits
 */

public class HitSerializerTSVImpl implements HitSerializer {

    protected Logger logger = Logger.getLogger(getClass());

    public HitSerializerTSVImpl() {
    }

    /**
     * serializes a single hit in the result
     *
     * @param writer
     * @param hit
     * @throws IOException
     */
    public void serialize(Writer writer, Hit hit) throws IOException {
        if(logger.isDebugEnabled())
            logger.debug("writing hit: " + hit);
        writer.write(hit.getInchiKey());
        writer.write("\t");
        writer.write(hit.getSearchTerm());
        writer.write("\t");
        writer.write(hit.getUsedAlgorithm().getClass().getSimpleName());

        if (hit instanceof Scored) {
            if(logger.isDebugEnabled())
                logger.debug("=> hit is scored");

            writer.write("\t");
            writer.write(((Scored) hit).getScoringAlgorithm().getClass().getSimpleName());
            writer.write(" ");
            writer.write(((Scored) hit).getScore().toString());
        }

        if (hit instanceof Enhanced) {
            if(logger.isDebugEnabled())
                logger.debug("=> hit is enhanced");

            for (Enhancement enhancement : ((Enhanced) hit).getEnhancements()) {
                writer.write("\t");
                writer.write(enhancement.getIdentifier());
                writer.write(" ");
                writer.write(enhancement.getValue().toString());
            }
        }

        writer.write("\n");
    }

    /**
     * serializes all the hits in the result
     *
     * @param writer
     * @param result
     * @throws IOException
     */
    public void serialize(Writer writer, List<? extends Hit> result) throws IOException {
        for (Hit hit : result) {
            serialize(writer, hit);
        }

        writer.flush();
    }
}