package edu.ucdavis.fiehnlab.chemify.exception;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/26/13
 * Time: 2:27 PM
 *
 * Simple notification object, if configuration explicitly disallow continuations in case of an exceptions
 */
public class CantContinueException extends ChemifyException {
    public CantContinueException(String s, ChemifyException e) {
        super(s,e);
    }
    public CantContinueException(String s) {
        super(s);
    }

}
