package edu.ucdavis.fiehnlab.chemify.pattern;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.PatternHelper;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/25/13
 * Time: 6:24 PM
 * <p/>
 * A compilation of common patterns used for chemical identifiers, etcs exposed as static variables
 */
public class CommonPatterns extends PatternHelper {

	/**
	 * basic pattern to describe a lipid
	 */
	public final static String LIPID_PATTERN = "((.*?)((?:(?:.*?-)?CE|(?:[Cc]er(?:amide)?)|(?:[Cc](?:HOLEST|holest)|[Ee](?:RGOST|ergost)|[Ss](?:TIGMAST|tigmast))(?:erol)|[Ss](?:phing(?:osine|anine))|FA|MG|DG|TG|(?:(?:[Pp]lasmenyl-)?(?:(?:L|Lyso)?PA|(?:L|Lyso)?PC|(?:L|Lyso)?PE|(?:L|Lyso)?PI|(?:L|Lyso)?PS|(?:L|Lyso)?PG))|SP|SM|CE))\\s?(\\(?\\s?(?:C|d|[Pp]-)?\\d+(?::\\d{0,2}[eop]?)?(?:/\\d+:\\d{0,2}){0,2}[eop]?\\)?(\\([\\w\\d]*\\))?\\)?))(\\s?(?:\\(.*\\)))?(.*)?";

	/**
	 * a sumarized TG string like
	 * <p/>
	 * TG(36:4)
	 * TG(4:1)
	 * TG (4:1)
	 */
	public final static String TG_SUMMARIZED_LIPID_PATTERN = "^((.*?)?(TG)?\\s?(\\(?\\s?\\/?(\\d+):(\\d+)\\))+)";

	/**
	 * pattern to match ONLY glicerolipids
	 */
	public static final String GLICEROLIPIDS_PATTERN = "((.*?)?(FA|MG|DG|TG)\\s?(\\(?\\s?(?:C|d)?\\d+(?::\\d{0,2})?(?:/\\d+:\\d{0,2}){0,2}\\)?(\\([\\w\\d]*\\))?\\)?))(\\s?(?:\\(.*\\)))?(.*)?";

	/**
	 * pattern to match ONLY glicerolipids
	 */
	public static final String PHOSPHOLIPIDS_PATTERN = "((.*?)?((?:(?:[Pp]lasmenyl|[Ppo])?-?(?:(?:L|Lyso)?PA|(?:L|Lyso)?PC|(?:L|Lyso)?PE|(?:L|Lyso)?PI|(?:L|Lyso)?PS|(?:L|Lyso)?PG))|SP|SM|(?:Phosphatidyl|Glycerophospho)(?:choline|ethanolamine|glycerol|inositol|serine))\\s?(\\(?\\s?(?:C|d|[Ppo]-?)?\\d+(?::\\d{0,2}[eop]?)?(?:[/-]\\d+:\\d{0,2}){0,2}[eop]?\\)?(\\([\\w\\d]*\\))?\\)?))(\\s?(?:\\(.*\\)))?(.*)?";

	/**
	 * pattern to match ONLY ceramides
	 */
	public static final String SPHINGOLIPIDS_PATTERN = "((.*?)?([Cc][Ee]|(?:(?:.*?-)?(?:[Cc]er(?:amide)?))|SM|SP|[Ss](?:phing(?:osine|anine)))\\s?(\\(?\\s?(?:C|d|[Pp]-)?\\d+(?::\\d{0,2})?(?:/\\d+:\\d{0,2}){0,2}\\)?(\\([\\w\\d]*\\))?\\)?))(\\s?(?:\\(.*\\)))?(.*)?";
//	public static final String SPHINGOLIPIDS_PATTERN = "((.*?)?((?:(?:.*?-)?(?:[Cc]er(?:amide)?))|SM|SP|[Ss](?:phing(?:osine|anine)))\\s?(\\(?\\s?(?:C|d|[Pp]-)?\\d+(?::\\d{0,2})?(?:/\\d+:\\d{0,2}){0,2}\\)?(\\([\\w\\d]*\\))?\\)?))(\\s?(?:\\(.*\\)))?(.*)?";

	/**
	 * pattern to match ONLY sterols
	 */
	public static final String STEROLS_PATTERN = "((.*?)?(CE|(?:[Cc](?:HOLEST|holest)|[Ee](?:RGOST|ergost)|[Ss](?:TIGMAST|tigmast)(?:erol|ane)?))\\s?(\\(?\\s?(?:C|d|[Pp]-)?\\d+(?::\\d{0,2})?(?:/\\d+:\\d{0,2}){0,2}\\)?(\\([\\w\\d]*\\))?\\)?))(\\s?(?:\\(.*\\)))?(.*)?";

	public static final String POSITIVE_ADUCT_PATTERN = "(.*?)\\s?\\[M.*?\\]\\+.*";

	public static final String NEGATIVE_ADUCT_PATTERN = "(.*?)\\s?\\[M.*?\\]\\-.*";

	public static final String TRIM_PATTERN = ".*";

	public static final String NUMBER_UNDERSCORE_PATTERN = "^\\d_+(.+)";

	public static final String NIST_PATTERN = "(?im)^(.*?)?(?:[-\\s_]?NIST|nist[-\\s_]?)(.+)?$";
//	public static final String NIST_PATTERN = "(?i)(.+)[-\\s_]+[Nn][Ii][Ss][Tt]$";

	public static final String ISTD_PATTERN = "(?i)(.+)[Ii][Ss][Tt][Dd]$";

	public static final String TMS_PATTERN = "(?i)(.+?)\\d?[Tt][Mm][Ss]\\d?(.+)?";

	//	public static final String DEUTERATED_PATTERN = "(^.*)\\s([a-z]\\d)";
	public static final String DEUTERATED_PATTERN = "([a-zA-Z]*?)\\s?(d\\d.*)";

	public static final String HMDB_PATTERN = "^HMDB\\d{5}$";

	public static final String ARTIFACT_PATTERN = "(?i)(.+)artifact(.+)?";

	public static final String LOGICAL_COMPARATOR_PATTERN = "(.*?)\\s(?:and|or|not|xor)\\s(.*?)";

	public static final String Z_PATTERN = "^z (.+)";

	public static final String MINOR_PATTERN = "(?i)(.+)minor(.+)?";

	public static final String MAJOR_PATTERN = "(?i)(.+)major(.+)?";

	public static final String SPACE_NUMBER_PATTERN = "(.+)[-_\\s]\\d$";

	public static final String PARENS_STAR_PATTERN = "(.+?)\\s?\\(\\*\\)$";

	public static final String DEHYDRATED_PATTERN = "(?i)\\(?dehydrated\\)?";

	public static final String MEOX_PATTERN = "(?i)(?:_|non-)?meox\\d?";

	public static final String RETENTION_TIME_PATTERN = "^(?:\\d+[.]\\d+\\s)?(.+?)(?:\\s\\d+(?:[.]\\d+)?\\s?s?)?$";
}
