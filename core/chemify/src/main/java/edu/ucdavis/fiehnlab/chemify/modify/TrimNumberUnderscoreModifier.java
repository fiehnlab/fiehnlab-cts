package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.annotations.RegisterOnStartup;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/24/13
 * Time: 2:18 PM
 */
@RegisterOnStartup(disabled = true)
public class TrimNumberUnderscoreModifier extends AbstractModifier {

    public TrimNumberUnderscoreModifier(){
        super(Priority.CALCULATION_BASED);
    }

	private Pattern pattern = Pattern.compile(getPattern());

    public String modify(String searchTeam) {
        Matcher matcher = pattern.matcher(searchTeam);

        if(matcher.matches()){
            return matcher.group(1);
        }

        return searchTeam;
    }

    public String getDescription() {
        return "this modifier removes the 1_ from the given name, using the pattern: " + this.getPattern();
    }

    public String getPattern() {
        return CommonPatterns.NUMBER_UNDERSCORE_PATTERN;
    }
}
