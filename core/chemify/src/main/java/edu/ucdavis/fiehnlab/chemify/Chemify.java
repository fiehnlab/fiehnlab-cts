package edu.ucdavis.fiehnlab.chemify;

import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;
import edu.ucdavis.fiehnlab.chemify.exception.CantContinueException;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.NothingIdentifiedAtAll;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import edu.ucdavis.fiehnlab.chemify.scoring.ScoreBySimilarity;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 6:44 PM
 * Basic chemify interface
 */
public class Chemify {

	@Override
	protected void finalize() throws Throwable {
		service.shutdown();
		super.finalize();
	}

	/**
	 * used for multi threaded parts of the application
	 */
	private ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * internal configuration
	 */
	private Configuration configuration;

	/**
	 * default constructor to assign a configuration
	 *
	 * @param configuration
	 */
	public Chemify(Configuration configuration) {
		this.configuration = configuration;
	}

	public Chemify() {
	}

	/**
	 * returns the default configuration, used by the algorithm
	 *
	 * @return
	 */
	public Configuration getDefaultConfiguration() {
		return this.configuration;
	}

	/**
	 * sets our used configuration
	 *
	 * @param configuration
	 * @return
	 */
	public Chemify setDefaultConfiguration(Configuration configuration) {
		this.configuration = configuration;
		return this;
	}

	/**
	 * identifies the given molecule and returns a list of matching hits
	 *
	 * @param searchTerm
	 * @param configuration
	 */
	public List<? extends Hit> identify(final String searchTerm, final Configuration configuration) throws ChemifyException {
		if (logger.isDebugEnabled()) {
			logger.debug("=> search term: " + searchTerm);
		}

//		logger.info("conf: " + configuration);

		String name = searchTerm;

		//design by contract pattern
		if (name == null) throw new IllegalArgumentException("you need to provide a name!");
		if (name.length() == 0) throw new IllegalArgumentException("you need to provide a name!");
		if (configuration.getAlgorithms() == null)
			throw new IllegalArgumentException("you need to have an algorithm defined");
		if (configuration.getAlgorithms().isEmpty())
			throw new IllegalArgumentException("you need to have an algorithm defined");
		if (configuration.getScoringAlgorithms() == null)
			throw new IllegalArgumentException("you need to define a scoring algorythm");

		//bring the name into the best possible format
		name = modifySearchTerm(name, configuration);
		if (logger.isDebugEnabled()) {
			logger.debug("  => modified search term: " + (name.isEmpty() ? "null" : name));
		}

		// pre-filter search term for quality
		if (ScoreBySimilarity.score(searchTerm, name) < configuration.getSearchTermMinQuality()) {
			logger.debug("  invalid name after modification... returning invalid hit");
			HitImpl emptyHit = new HitImpl("invalid modified search term", new NothingIdentifiedAtAll(), name, searchTerm);
			List<Hit> result = new ArrayList<Hit>();
			result.add(emptyHit);
			return result;
		}

		//access our algorithm
		List<Identify> options = configuration.getAlgorithms();

		//usefull to log how long it takes
		long begin = System.currentTimeMillis();

		//generate our search terms
		Set<String> multiplexed = multiplex(name, configuration.getMultiplexer());

		if (logger.isDebugEnabled()) {
			logger.debug("=> multiplexed: " + multiplexed);
		}
		//loop over the options and try to identify the name
		for (Identify identify : options) {
			if (logger.isDebugEnabled()) {
				logger.debug("=> identifying with: " + identify.getClass().getSimpleName());
			}
			//check if we are allowed to add empty results, using this specific identification class
			if (identify instanceof NothingIdentifiedAtAll) {
				//if we do not allow null results => just skip them
				if (configuration.isNonIdentifiedResultsPermitted() == false) {
					continue;
				}
			}

			//can we use a patterned approach
			if (identify instanceof Patternized) {
				Patternized p = (Patternized) identify;

				if (name.matches(p.getPattern()) == false) {
					//skipped since the pattern doesn't matches so next identify object comes
					continue;
				}
				//else just proceed with the processing
			}

			try {
				List<Hit> result = identifyMultiplex(identify, multiplexed, searchTerm);

				if (result.isEmpty() == false) {

					//score the results
					List<Scored> scoredHits = scoreResult(configuration, result, name);

					//remove duplicates
					scoredHits = unify(scoredHits);

					//filter the result using all available filter
					if (configuration.getFilters() != null && configuration.getFilters().size() > 0) {
						for (Filter filter : configuration.getFilters()) {
							scoredHits = filter.filter(scoredHits);
						}
					}
					addDefaultEnhancement(searchTerm, scoredHits, System.currentTimeMillis() - begin);

					Collections.sort(scoredHits, new Comparator<Scored>() {
						@Override
						public int compare(Scored scored, Scored scored2) {
							return scored2.getScore().compareTo(scored.getScore());
						}
					});

					if (name == null) {
						return Collections.EMPTY_LIST;
					}

					//no enhances so let's just return the hit
					if (configuration.getEnhances().isEmpty() == false) {
						return enhanceResults(configuration, scoredHits);
					}

					return scoredHits;
				}
			} catch (ChemifyException e) {
				if (configuration.isContinueOnException(e) == false) {
					throw new CantContinueException("configuration disallowed continuation in case of an exception", e);

				} else {
					logger.warn("an exception occured and was ignored: " + e.getMessage(), e);
					//log that an exception happened in warn mode
				}
			}
			//else go to the next algorithm and try again
		}
		//by default we return an empty list. Since this just means there were no matching algorithms
		return Collections.emptyList();
	}

	private List<Scored> unify(List<Scored> scoredHits) {
		// remove duplicates and recreate the results list
		List<Scored> uniqueHits = new ArrayList<Scored>();
		for (Scored hit : scoredHits) {
			if (!uniqueHits.contains(hit))
				uniqueHits.add(hit);
		}
		scoredHits.clear();

		for (Scored hit : uniqueHits) {
			scoredHits.add(hit);
		}
		uniqueHits.clear();
		uniqueHits = null;

		return scoredHits;
	}

	/**
	 * generates a list of hits for he multipliex set with the given result set
	 *
	 * @param identify
	 * @param multiplexed
	 * @return
	 */
	private List<Hit> identifyMultiplex(final Identify identify, Set<String> multiplexed, final String originalTerm) {

		//no need to get all fancy, just return the results in one run
		if (multiplexed.size() == 1) {
			return identify.identify(multiplexed.iterator().next(), originalTerm);
		}

		//ok more than one hit, let's generate some threads to take care of it.
		List<Hit> result = new ArrayList<Hit>();

		ExecutorCompletionService<List<Hit>> refutures = new ExecutorCompletionService<List<Hit>>(service);

		//submit our tasks for calulation
		for (final String s : multiplexed) {

			refutures.submit(new Callable<List<Hit>>() {
				@Override
				public List<Hit> call() throws Exception {
					return identify.identify(s, originalTerm);
				}
			});

		}

		//fetch our results
		for (String s : multiplexed) {
			try {
				for (Hit hit : refutures.take().get()) {
					if (result.contains(hit) == false) {
						hit.setOriginalTerm(originalTerm);
//						logger.debug("adding hit to result: " + hit);
						result.add(hit);
					}
				}
			} catch (Exception e) {
				logger.warn(e.getMessage(), e);
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("multipelxed term results: " + result);
		}
		return result;
	}

	/**
	 * multiplexes our search term
	 *
	 * @param searchTerm
	 * @param multiplexer
	 * @return
	 */
	private Set<String> multiplex(String searchTerm, Set<Multiplexer> multiplexer) {

		if (logger.isDebugEnabled()) {
			logger.debug("multiplexing search term: " + searchTerm);
		}
		Set<String> result = new HashSet<String>();

		if (multiplexer.isEmpty()) {
			result.add(searchTerm);
		}

		for (Multiplexer multi : multiplexer) {
			result.addAll(multi.multiplex(searchTerm));
		}
		return result;
	}


	/**
	 * enhance all the given results
	 *
	 * @param configuration
	 * @param scoredHits
	 * @return
	 */
	private List<? extends Hit> enhanceResults(Configuration configuration, List<? extends Scored> scoredHits) {
		Iterator<Enhance> enhanceIterator = configuration.getEnhances().iterator();
		List<? extends Enhanced> enhanced = enhanceIterator.next().enhance(scoredHits);

		while (enhanceIterator.hasNext()) {
			enhanced = enhanceIterator.next().enhance(enhanced);
		}

		return enhanced;
	}

	/**
	 * add some default enhancements to the result set
	 *
	 * @param searchTerm
	 * @param scoredHits
	 */
	private void addDefaultEnhancement(String searchTerm, List<? extends Scored> scoredHits, long timeNeeded) {
		//add the default enhancements to the hits
		for (Hit hit : scoredHits) {
			if (hit instanceof Enhanced) {
				((Enhanced) hit).getEnhancements().clear();
				((Enhanced) hit).addEnhancement(new Enhancement("unmodified search term", searchTerm));
				((Enhanced) hit).addEnhancement(new Enhancement("time(ms) for identification", timeNeeded));
			}
		}
	}

	/**
	 * modifies the search term
	 *
	 * @param name
	 * @param configuration
	 * @return
	 */
	private String modifySearchTerm(String name, Configuration configuration) {
		//modify our search term
		for (Modify modify : configuration.getModifications()) {

			//only name's which match the given pattern should be modified
			if (name.matches(modify.getPattern()))
				name = modify.modify(name);
		}
		return name;
	}

	/*
	 * computes the scoring of all scored algorithms
	 */
	private List<Scored> scoreResult(final Configuration configuration, final List<Hit> result, String searchTerm) {
		List<Scored> scoredResult = new ArrayList<Scored>();

		//temporary list
		List<Hit> temp = new ArrayList<Hit>();
		temp.addAll(result);

		//assemble the scoring
		for (Scoring scoring : configuration.getScoringAlgorithms()) {

			scoredResult.addAll(scoring.score(temp, searchTerm));
			temp.clear();

			//add all the newly scored results to the temp list and score them over and over
			temp.addAll(scoredResult);
		}
		return scoredResult;
	}

	/**
	 * simplistic method, which utilizes an internally set configuration instead of a custom one
	 *
	 * @param name
	 * @return
	 */
	public final List<? extends Hit> identify(String name) throws ChemifyException {
		return identify(name, this.getDefaultConfiguration());
	}

}
