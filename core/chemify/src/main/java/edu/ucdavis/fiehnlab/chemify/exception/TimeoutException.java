package edu.ucdavis.fiehnlab.chemify.exception;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 2/3/14
 * Time: 1:17 PM
 */
public class TimeoutException extends ChemifyException{
    public TimeoutException(String s) {
        super(s);
    }
}
