package edu.ucdavis.fiehnlab.chemify.scoring;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diego on 4/2/14.
 *
 * This class implements Levenshtein algorithm (non recursive) to sort results based on similarity to the search term.
 */
public class ScoreBySimilarity implements Scoring {
	private static Logger logger = Logger.getLogger(ScoreBySimilarity.class);

	/**
	 * the score methods converts the result into a scored hit to reduce the amount of data output
	 *
	 * @param input
	 * @param searchTerm
	 * @return
	 */
	@Override
	public List<Scored> score(List<? extends Hit> input, String searchTerm) {
		logger.debug("-> about to score: " + input);

		List<Scored> result= new ArrayList<Scored>();

		//for each hit, calculate levenshtein distance from searchTerm -> hitTerm
		for (Hit hit : input) {
			logger.debug("comparing search: '" + searchTerm + "' with found '" + hit.getSearchTerm() + "'");
			int score = leven(searchTerm, hit.getSearchTerm());
			Double similarity = 1.0 - (Double.valueOf(score) / Math.max(hit.getSearchTerm().length(), searchTerm.length()));
			result.add(new ScoredHit(hit, this, similarity));
			logger.debug("result so far " + result);
		}

		// build scored result
		return result;
	}

	public static Double score(String from, String to) {
		int score = leven(from, to);
		Logger.getLogger("ScoreBySimilarity").debug("scoring: " + from + " vs " + to + " -- " + score);

		Double similarity = 1.0 - (Double.valueOf(score) / Math.max(from.length(), to.length()));
		Logger.getLogger("ScoreBySimilarity").debug("similarity: " + similarity);

		return similarity;
	}

	/**
	 * am arbitrary number. The higher the value the higher the priority
	 *
	 * @return
	 */
	@Override
	public Priority getPriority() { return Priority.MEDIUM; }

	@Override
	public int compareTo(Prioritize prioritize) { return getPriority().compareTo(prioritize.getPriority()); }

	/**
	 * the description of this object
	 *
	 * @return
	 */
	@Override
	public String getDescription() {
		return "Sorts the results based on similarity to the search term";
	}


	private static int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	public static int leven(String str1,String str2) {
		int[][] distance = new int[str1.length() + 1][str2.length() + 1];

		for (int i = 0; i <= str1.length(); i++)
			distance[i][0] = i;
		for (int j = 1; j <= str2.length(); j++)
			distance[0][j] = j;

		for (int i = 1; i <= str1.length(); i++)
			for (int j = 1; j <= str2.length(); j++)
				distance[i][j] = minimum(
						distance[i - 1][j] + 1,
						distance[i][j - 1] + 1,
						distance[i - 1][j - 1]+ ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0 : 1));

		return distance[str1.length()][str2.length()];
	}
}
