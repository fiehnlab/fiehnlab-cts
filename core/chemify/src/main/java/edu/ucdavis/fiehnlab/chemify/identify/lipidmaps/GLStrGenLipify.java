package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/10/2013
 * Time: 11:37 PM
 * <p/>
 * using the GLStrGen perl script to generate a sdf file
 */
public class GLStrGenLipify extends AbstractLipify {
	@Override
	protected String getPerlScriptName() {
		return "GLStrGen.pl";
	}

	public String getPattern() {
		return CommonPatterns.GLICEROLIPIDS_PATTERN;
	}
}
