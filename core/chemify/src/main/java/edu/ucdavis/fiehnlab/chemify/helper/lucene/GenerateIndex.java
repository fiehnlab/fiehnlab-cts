package edu.ucdavis.fiehnlab.chemify.helper.lucene;

import edu.ucdavis.fiehnlab.chemify.identify.lucene.LuceneFuzzyIdentify;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/15/2013
 * Time: 7:20 PM
 * <p/>
 * generates a lucene index for us.
 */
public class GenerateIndex {

    public static final String INCHI_KEY = "inchiKey";
    public static final String NAME = "name";
    private Logger logger = Logger.getLogger(getClass());

    /**
     * generates an index at the default location from a specific file
     */
    public void generateIndex(File from) throws IOException {
        generateIndex(from, LuceneFuzzyIdentify.getIndexFile());
    }

    /**
     * generates the index from the file to the given index
     *
     * @param from
     * @param index
     * @throws IOException
     */
    public void generateIndex(File from, File index) throws IOException {

        if (from.exists() == false) {
            throw new FileNotFoundException("sorry the given file doesn't exist: " + from.getAbsolutePath());
        }

        if (index.exists() == false) {
            index.mkdirs();
        }

        File[] files = from.listFiles();

        if (files.length == 0)
            throw new FileNotFoundException("your specific directory does not have any registered files: " + from);

        Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_45);
        SimpleFSDirectory d = new SimpleFSDirectory(index);

        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_45, analyzer).setWriteLockTimeout(5000).setRAMBufferSizeMB(5000);

        IndexWriter indexWriter = new IndexWriter(d, config);

        for (File file : files) {

            if (logger.isDebugEnabled())
                logger.debug("indexing: " + file.getName());


            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();
                if (logger.isTraceEnabled())
                    logger.trace("read line: " + line);

                String[] items = line.split("\t");


                Document document = new Document();

                document.add(new StringField("UNIQUE_KEY", line, Field.Store.YES));
                document.add(new StringField(INCHI_KEY, items[0], Field.Store.YES));
                document.add(new Field(NAME, items[1].replaceAll("\"",""), TextField.TYPE_STORED));

                indexWriter.updateDocument(new Term("UNIQUE_KEY", line), document, analyzer);

            }
            scanner.close();
            indexWriter.commit();
        }

        indexWriter.close();

    }
}
