package edu.ucdavis.fiehnlab.chemify;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 4:38 PM
 */
public abstract class AbstractPrioritize implements Prioritize{

    private Priority priority;

    /**
     * set's our priority
     * @param priority
     */
    protected AbstractPrioritize(Priority priority){
        this.priority = priority;
    }

    public final Priority getPriority() {
        return priority;
    }

	public final void setPriority(Priority priority) {
		this.priority = priority;
	}

    /**
     * simple im0plementation - will fail in case of NULL
     * @param prioritize
     * @return
     */
    public final int compareTo(Prioritize prioritize) {
        return this.getPriority().compareTo(prioritize.getPriority());
    }
}
