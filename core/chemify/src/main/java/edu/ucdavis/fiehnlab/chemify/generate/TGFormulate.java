package edu.ucdavis.fiehnlab.chemify.generate;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/14/13
 * Time: 9:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class TGFormulate {

	private Pattern pattern = Pattern.compile(CommonPatterns.TG_SUMMARIZED_LIPID_PATTERN);

	private Logger logger = Logger.getLogger(getClass());

	private Map<String, Integer> glycerol_formula = new HashMap<String, Integer>();

	/**
	 * default constructor
	 */
	public TGFormulate() {
		glycerol_formula.put("C",3);
		glycerol_formula.put("H",5);
		glycerol_formula.put("O",0);
	}

	/**
	 * Calculates the molecular formula for a TG (straight FAs only right now...)
	 *
	 * @param input
	 * @return
	 */
	public String getFormula(String input) {

		//adding glycerol part to current atom counter "C3H5"
		Integer curCarbons = glycerol_formula.get("C");
		Integer curHydrogens = glycerol_formula.get("H");
		Integer curOxygens = glycerol_formula.get("O");

		if (logger.isDebugEnabled())
			logger.debug("input: " + input);

		StringBuilder result = new StringBuilder();

		Matcher matcher = pattern.matcher(input);

		if(matcher.matches()) {
			logger.debug(matcher.group(5));
			logger.debug(matcher.group(6));
			Integer carbons = new Integer(matcher.group(5));
			Integer db_bonds = new Integer(matcher.group(6));

			//start formula creation

			// carboxilic group -COO- add to current and subtract from counters (times 3 since it's a TG)
			curCarbons = curCarbons + 3;
			curOxygens = curOxygens + 6;
			carbons = carbons - 3;

			for(int i=0; i < carbons; i++) {
				logger.debug("processing carbon: " + i);
				curCarbons = curCarbons + 1;
				if (i >= carbons - 3) {   // terminal CH3- insteadd of -CH2-
					curHydrogens = curHydrogens + 3;
				} else {            // still -CH2-
					curHydrogens = curHydrogens + 2;
				}

				logger.debug(">>> carbons: " + curCarbons);
				logger.debug(">>> hydrogens: " + curHydrogens);
				logger.debug(">>> oxygens: " + curOxygens);
			}

			// double bonds eliminate 2 Hydrogens
			for(int db=0; db < db_bonds; db++) {
				curHydrogens = curHydrogens - 2;
			}

			result.append("C").append(curCarbons);
			result.append("H").append(curHydrogens);
			result.append("O").append(curOxygens);
		}

		return result.toString();
	}
}
