package edu.ucdavis.fiehnlab.chemify.hit;

import edu.ucdavis.fiehnlab.chemify.Enhanced;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Identify;
import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 6:55 PM
 * <p/>
 * A basic HitImpl for an identification search
 */
public class HitImpl implements Hit, Enhanced {

	private Identify identify;

	/**
	 * what we were looking for
	 */
	private String searchTerm;

	/**
	 * searchTerm before multiplexing
	 */
	private String originalTerm;

	private String inchiKey;

//    public HitImpl(String inchiKey, Identify identify, String searchTerm) {
//	    this(inchiKey, identify, searchTerm, searchTerm);
//    }

	/**
	 * constructor
	 *
	 * @param inchiKey
	 * @param identify
	 * @param searchTerm
	 */
	public HitImpl(String inchiKey, Identify identify, String searchTerm, String originalTerm) {
		if (inchiKey == null) throw new IllegalArgumentException("please provide an inchi key!");
		if (identify == null) throw new IllegalArgumentException("please provide an identify value!");
		if (searchTerm == null) throw new IllegalArgumentException("please provide a search term!");

		this.identify = identify;
		this.inchiKey = inchiKey;
		this.searchTerm = searchTerm;
		this.originalTerm = originalTerm;
	}

	/**
	 * discovered inchi key
	 *
	 * @return
	 */
	public String getInchiKey() {
		return inchiKey;
	}

	/**
	 * which algorithm was used to discover this hit
	 *
	 * @return
	 */
	public Identify getUsedAlgorithm() {
		return identify;
	}

	public int compareTo(Hit hit) {
		return this.getInchiKey().compareTo(hit.getInchiKey());
	}

	/**
	 * simple to string method
	 *
	 * @return
	 */
	public String toString() {
		return inchiKey + " [" + identify + "]";
	}

	public String getSearchTerm() {
		return this.searchTerm;
	}

	public String getOriginalTerm() {
		return originalTerm;
	}

	public void setOriginalTerm(String term) {
		this.originalTerm = term;
	}

	@Override
	public Map<String, Object> getTrackedProperties() {
		return null;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof HitImpl)) return false;

		HitImpl hit = (HitImpl) o;

		if (!inchiKey.equals(hit.inchiKey)) return false;
		if (!originalTerm.equals(hit.originalTerm)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = originalTerm.hashCode();
		result = 31 * result + inchiKey.hashCode();
		return result;
	}

	/**
	 * adds an enhancement to this hit
	 *
	 * @param enhancement
	 * @return
	 */
	public HitImpl addEnhancement(Enhancement enhancement) {
		this.enhancements.add(enhancement);
		return this;
	}

	public Set<Enhancement> getEnhancements() {
		return this.enhancements;
	}

	public Enhancement getEnhancement(String identifier) {
		for (Enhancement e : enhancements) {
			if (e.getIdentifier().equals(identifier)) {
				return e;
			}
		}

		return null;
	}

	/**
	 * associated enhancements
	 */
	private Set<Enhancement> enhancements = new HashSet<Enhancement>();

}
