package edu.ucdavis.fiehnlab.chemify;

import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 6:53 PM
 * <p/>
 * Interface, which needs to be implemented to allow basic identification of results.
 */
public interface Identify extends Prioritize, Describe {

	/**
	 * implementation of this method will, provide you with a sorted list of hit's, which could be the related compound
	 *
	 * @param searchTerm
	 * @return
	 */
	public List<Hit> identify(String searchTerm, String originalTerm) throws ChemifyException;

	/**
	 * if this algorithm depends on any configuration, it should be set here
	 */
	public void setConfiguration(Configuration configuration);

	/**
	 * returns the used configuration
	 *
	 * @return
	 */
	public Configuration getConfiguration();

	/**
	 * do we have a configuration
	 *
	 * @return
	 */
	public boolean hasConfiguration();

	/**
	 * set's the caching to be used with this identify object
	 *
	 * @param caching
	 */
	public Identify setCache(Caching<List<Hit>> caching);


}
