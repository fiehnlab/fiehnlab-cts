package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/30/14
 * Time: 3:41 PM
 */
public class DashMultiPlexer implements Multiplexer {

	Logger logger = Logger.getLogger(getClass());

	@Override
	public Set<String> multiplex(String searchTerm) {

		Set<String> result = new HashSet<String>();

		//clean "-" at end of search term
		if(searchTerm.matches(".*?-")) {
			searchTerm = searchTerm.substring(0, searchTerm.length()-1);
		}

		if (searchTerm.contains("-")) {
			result = test(searchTerm);
		} else {
			result.add(searchTerm);
		}

		if (logger.isDebugEnabled())
			logger.debug("multiplexed " + searchTerm + " to " + result.size() + " terms");

		return result;
	}

	public Set<String> test(String searchTerm) {
		Set<String> result = new HashSet<String>();

		int count = 0;

		//only do full multiplexing in case dashes and spaces are less than 5
		for(char c : searchTerm.toCharArray()) {
			if(c == ' ' || c == '-') {
				count++;
			}
		}

		if(5 >= count) {

			int index;
			searchTerm = searchTerm.replaceAll(" ", "-");

			if (searchTerm.matches("^[a-zA-Z][\\s-].*$")) {
				index = searchTerm.indexOf("-", searchTerm.indexOf("-") + 1);
			} else {
				index = searchTerm.indexOf("-");
			}


			List<Integer> dashIndices = new ArrayList();

			while (index > 1) {
				dashIndices.add(index);
				index = searchTerm.indexOf("-", index + 1);
			}

			char[] s = searchTerm.toCharArray();

			for (int i = 0; i < Math.pow(2, dashIndices.size()); i++) {
				String b = Integer.toBinaryString(i);

				try {
					for (int j = 0; j < b.length(); j++) {
						s[dashIndices.get(j)] = (b.charAt(j) == '1') ? ' ' : '-';
					}
				} catch (IndexOutOfBoundsException e) {  // no replaceable dashes
					logger.warn("No replaceable dashes in '" + searchTerm + "'");
				}

				result.add(new String(s));
			}
		} else {
			result.add(searchTerm);
			result.add(searchTerm.replaceAll("-", " ").trim());
		}

		return result;
	}

	@Override
	public String getDescription() {
		return "generates different combinations of dashes replaced with space characters";
	}
}
