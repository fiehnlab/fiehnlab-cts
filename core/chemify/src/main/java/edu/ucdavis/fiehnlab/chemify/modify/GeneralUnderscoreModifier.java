package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import org.apache.log4j.Logger;

/**
 * replaces the _ in a word with a dash since in a lot of cases we do not want to deal with them
 * User: wohlgemuth
 * Date: 1/23/14
 * Time: 1:33 PM
 */
public class GeneralUnderscoreModifier extends AbstractModifier {

    private Logger logger = Logger.getLogger(getClass());

    public GeneralUnderscoreModifier() {
        super(Priority.VERY_LOW);
    }

    /**
     * modifies the search term and trys to build a IUPAC acceptable name instead
     *
     * @param searchTeam
     * @return
     */
    @Override
    public String modify(String searchTeam) {

        //split everything by an underscore
        String[] content = searchTeam.trim().split("_");

        StringBuffer result = new StringBuffer();

        boolean wasWord = false;

        //go over the array of term
        for (int i = 0; i < content.length; i++) {

            //if it's a number
            try {
                Integer.parseInt(content[i]);

                //if the current term is a number and the last term, we don't want it
                if (i + 1 != content.length) {

                    //if it's not a space
                    if (!content[i].isEmpty()) {
                        //add our current content
                        result.append(content[i]);
                    }

                }

                if (i + 1 < content.length) {

                    if (wasWord) {
                        result.append("-");
                    } else {
                        try {
                            Integer.parseInt(content[i + 1]);

                            result.append(",");
                        } catch (NumberFormatException e) {
                            result.append("-");
                        }

                    }
                }
                wasWord = false;

            }
            //if it was not a number
            catch (NumberFormatException e) {

                if (!content[i].isEmpty()) {
                    //add our current content
                    result.append(content[i]);
                }

                //if we have more content to come
                if (i + 1 < content.length) {
                    try {
                        Integer.parseInt(content[i + 1]);

                        //the next one is a number so preface it with a - since the current term is a word
                        if (i + 1 != content.length) {
                            result.append("-");
                        }
                    }
                    //the next one is a character
                    catch (NumberFormatException ex) {

                        //the term only has a single digit
                        if (content[i].length() == 1) {

                            //think about n-methyl amine
                            result.append("-");
                        } else {
                            result.append(" ");
                        }
                    }
                }


                wasWord = true;
            }

        }

        return result.toString().trim();
    }

    @Override
    public String getDescription() {
        return "tries to build a valid IUPAC name from a term, if it looks like a word has been separated by underscores like 'citric_acid' or containing underscores and numbers";
    }

    @Override
    public String getPattern() {
        return ".*";
    }
}