package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/18/13
 * Time: 3:55 PM
 */
public class SphingolipidModifier extends AbstractLipidModifier {

	public String getDescription() {
		return "ensures that the search term is a valid sphingolipid string and re-formats it to a valid sphingolipid abbreviation";
	}

	/**
	 * sanitizes the ceramides name
	 *
	 * @param matcher
	 * @return
	 */
	public String refine(Matcher matcher) {

		if (logger.isDebugEnabled())
			logger.debug("=> is a sphingolipid");

		if (logger.isTraceEnabled()) {
			logger.trace("groups: " + matcher.groupCount());
			for (int i = 1; i <= matcher.groupCount(); i++) {
				logger.trace("group" + i + ": " + matcher.group(i));
			}
		}

		String result = matcher.group(1);
		String lipidClass = matcher.group(3);

		lipidClass = lipidClass.replaceFirst("Ceramide", "Cer");


		if (matcher.group(4).indexOf("(") != 0 && matcher.group(4).endsWith(")")) {
			if (logger.isDebugEnabled())
				logger.debug("has parens");
			result = lipidClass + matcher.group(4);
		}

		if (matcher.group(4).indexOf("/") < 0) {   // condensed chains

			String chain = matcher.group(4);

			Integer carbons = 0;
			Integer dbonds = 0;

			Matcher chainMchr = Pattern.compile("\\(?[Cd]?(\\d{1,2}):?(\\d{0,2})\\)?").matcher(chain);
			if (chainMchr.matches()) {
				carbons = Integer.parseInt(chainMchr.group(1));
				dbonds = Integer.parseInt(chainMchr.group(2).equals("") ? "0" : chainMchr.group(2));
			}

			// TODO find out what to do whith the rest of the posibilities: 18:0/, 16:0/,16:1/, 16:2/...
			// try adding 18:1 chain as most probable only for lipidClass == Cer
			if (lipidClass.equals("Cer")) {
				if (carbons > 24) {
					if (logger.isDebugEnabled())
						logger.debug("expanding to most probable form, (18:1/*)");
					result = lipidClass + "(18:1/" + (carbons - 18) + ":" + (dbonds - 1) + ")";
				} else {
					result = lipidClass + "(18:1/" + carbons + ":" + dbonds + ")";
				}
			} else {
				if (matcher.group(4).matches("\\(.*?\\)")) {
					result = lipidClass + matcher.group(4);
				} else {
					result = lipidClass + "(" + matcher.group(4) + ")";
				}
			}
		}

		if (logger.isDebugEnabled())
			logger.debug("=> result is: " + result);

		return result;
	}

	/* returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.SPHINGOLIPIDS_PATTERN;
	}
}
