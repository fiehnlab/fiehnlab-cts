package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/14/13
 * Time: 3:52 PM
 * <p/>
 * if a search term ends with 'artifact' we remove that substring
 */
public class LogicalComparatorModifier extends AbstractModifier {

	public LogicalComparatorModifier() {
		super(Priority.HIGH);
	}

	public String modify(String searchTerm) {
		return searchTerm.replaceAll(CommonPatterns.LOGICAL_COMPARATOR_PATTERN, "$1 $2").trim();
	}

	public String getDescription() {
		return "removes problematic logical comparators 'and', 'or', 'not' and 'xor' from the search term";
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.LOGICAL_COMPARATOR_PATTERN;
	}
}
