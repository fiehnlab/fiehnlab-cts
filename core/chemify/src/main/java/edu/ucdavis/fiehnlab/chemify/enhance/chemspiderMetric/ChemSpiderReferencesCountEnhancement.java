package edu.ucdavis.fiehnlab.chemify.enhance.chemspiderMetric;

import edu.ucdavis.fiehnlab.chemify.enhance.Enhancement;

/**
 * Created by diego on 10/9/14.
 */
public class ChemSpiderReferencesCountEnhancement extends Enhancement {
	private ChemSpiderReferencesCountEnhancement(String identifier, Object value) {
		super(identifier, value);
	}

	public ChemSpiderReferencesCountEnhancement(Integer count) {
		this("ReferencesCount", count);
	}
}
