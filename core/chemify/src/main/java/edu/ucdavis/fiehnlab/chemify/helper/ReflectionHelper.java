package edu.ucdavis.fiehnlab.chemify.helper;

import edu.ucdavis.fiehnlab.chemify.Filter;
import edu.ucdavis.fiehnlab.chemify.Identify;
import edu.ucdavis.fiehnlab.chemify.Modify;
import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import edu.ucdavis.fiehnlab.chemify.annotations.RegisterOnStartup;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/15/13
 * Time: 12:58 PM
 * <p/>
 * simple helper class to use reflections to discover classes
 */
public class ReflectionHelper {

    private Set<String> initialized = new HashSet<String>();

    private static final String PREFIX = "edu.ucdavis.fiehnlab.chemify";

    private Logger logger = Logger.getLogger(getClass());

    /**
     * internal method to find all identify objects
     */
    public List<Identify> registerIdentifyObjects(List<Identify> list) {

        Reflections reflections = new Reflections(PREFIX);

        Set<Class<? extends Identify>> subTypes =
                reflections.getSubTypesOf(Identify.class);

        for (Class<? extends Identify> clazz : subTypes) {
            try {

                if (clazz.isInterface()) {
                } else if (Modifier.isAbstract(clazz.getModifiers())) {

                } else {
                    if (checkIfClassIsDisabled(clazz)) continue;

                    if (logger.isDebugEnabled())
                        logger.debug("=> loading:" + clazz.getSimpleName());
                    //no reason to skip it found, just add an instance

                    if (initialized.contains(clazz.getName()) == false) {
                        initialized.add(clazz.getName());
                        list.add(clazz.newInstance());
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return list;
    }

    private boolean checkIfClassIsDisabled(Class<?> clazz) {
        //we can explicitly declare if we'd like to register our classes on startup
        if (clazz.isAnnotationPresent(RegisterOnStartup.class)) {
            RegisterOnStartup startup = clazz.getAnnotation(RegisterOnStartup.class);
            if (startup.disabled()) {

                if (logger.isDebugEnabled())
                    logger.debug("=> skipping class, register is disabled: " + clazz.getName());
                //automatic startup is d
                return true;
            }
        }
        return false;
    }

    /**
     * registers all modifier
     *
     * @param list
     */
    public List<Modify> registerModifierObjects(List<Modify> list) {

        Reflections reflections = new Reflections(PREFIX);

        Set<Class<? extends Modify>> subTypes =
                reflections.getSubTypesOf(Modify.class);

        for (Class<? extends Modify> clazz : subTypes) {
            try {

                if (clazz.isInterface()) {
                    //we can't initialized an interface
                } else if (Modifier.isAbstract(clazz.getModifiers())) {
                    //we can't initialize an abstract class
                } else {
                    //is this class disabled
                    if (checkIfClassIsDisabled(clazz)) continue;

                    if (logger.isDebugEnabled())
                        logger.debug("=> loading:" + clazz.getSimpleName());

                    //no reason to skip it found, just add an instance


                    if (initialized.contains(clazz.getName()) == false) {
                        initialized.add(clazz.getName());
                        list.add(clazz.newInstance());
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return list;
    }

    /**
     * finds all implementation of the filter interface and registers them, if we permit this add startup
     *
     * @param list
     * @return
     */
    public Set<Filter> registerFilterObjects(Set<Filter> list) {

        Reflections reflections = new Reflections(PREFIX);

        Set<Class<? extends Filter>> subTypes =
                reflections.getSubTypesOf(Filter.class);

        for (Class<? extends Filter> clazz : subTypes) {
            try {

                if (clazz.isInterface()) {
                } else if (Modifier.isAbstract(clazz.getModifiers())) {

                } else {
                    if (checkIfClassIsDisabled(clazz)) continue;

                    if (logger.isDebugEnabled())
                        logger.debug("=> loading:" + clazz.getSimpleName());
                    //no reason to skip it found, just add an instance

                    if (initialized.contains(clazz.getName()) == false) {
                        initialized.add(clazz.getName());
                        list.add(clazz.newInstance());
                    }                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return list;
    }

    public Set<Multiplexer> registerMultiplexerObjects(Set<Multiplexer> list) {

        Reflections reflections = new Reflections(PREFIX);

        Set<Class<? extends Multiplexer>> subTypes =
                reflections.getSubTypesOf(Multiplexer.class);

        for (Class<? extends Multiplexer> clazz : subTypes) {
            try {

                if (clazz.isInterface()) {
                } else if (Modifier.isAbstract(clazz.getModifiers())) {

                } else {
                    if (checkIfClassIsDisabled(clazz)) continue;

                    if (logger.isDebugEnabled())
                        logger.debug("=> loading:" + clazz.getSimpleName());
                    //no reason to skip it found, just add an instance

                    if (initialized.contains(clazz.getName()) == false) {
                        initialized.add(clazz.getName());
                        list.add(clazz.newInstance());
                    }                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return list;
    }

}
