package edu.ucdavis.fiehnlab.chemify.scoring;

import edu.ucdavis.fiehnlab.chemify.*;
import edu.ucdavis.fiehnlab.chemify.exception.ChemSpiderException;
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by diego on 10/8/14.
 */
public abstract class AbstractCombinedScoring implements Scoring {
	private static Logger logger = Logger.getLogger(AbstractCombinedScoring.class);

	Double max = 0.0;
	public int weight;

	protected AbstractCombinedScoring() {
		super();
	}

	protected AbstractCombinedScoring(int weight) {
		this.weight = weight;
	}

	/**
	 * the score methods converts the result into a scored hit to reduce the amount of data output
	 *
	 * @param input
	 * @param searchTerm
	 * @return
	 */
	@Override
	public List<Scored> score(List<? extends Hit> input, String searchTerm) {
        List<Scored> scoredRes = new ArrayList<>();
		//add pubmed values  -- keep track of max to reduce to 0.0 - 1.0 range
        Map<String, Double> tmpScores = new HashMap<>();

		//check if input is enhanced with chemspider metrics
		for (Hit hit : input) {
			if (((Enhanced)hit).getEnhancements().size() < 1) { // enhance input with chemspider metrics
				enhance((Enhanced) hit);
			}
		}

		for (Hit hit : input) {

			Enhanced tmp = (Enhanced) hit;
			Integer value = 0;
			if (tmp.getEnhancements().size() > 0) {
				value = (Integer) tmp.getEnhancement(this.getMetric()).getValue();
			} else {    // not enhanced, fetch metrics from chemspider
				try {
                    Map<String, Integer> metrics = new HashMap<>();
                    setBlankScores(metrics);
                    logger.debug("Metrics: " + metrics);
					value = metrics.get(this.getMetric());

				} catch (ChemSpiderException e) {
					logger.error("Error getting metrics data. setting score = 0.0 for " + hit.getInchiKey());
					tmpScores.put(hit.getInchiKey(), 0.0);
				}
			}

			tmpScores.put(hit.getInchiKey(), value + 0.0);
			if (max < value) {
				max = value.doubleValue();
			}
		}

		if (max == 0) {
			max = 1.0;
		}

		// create scored hit
		for (Map.Entry<String, Double> item : tmpScores.entrySet()) {
			for (Hit hit : input) {
				if (hit.getInchiKey().equals(item.getKey())) {
					scoredRes.add(new ScoredHit(hit, this, item.getValue() / max));
					break;
				}
			}
		}

		Collections.sort(scoredRes, new Comparator<Scored>() {
			@Override
			public int compare(Scored scored, Scored scored2) {
				return scored2.getScore().compareTo(scored.getScore());
			}
		});

		max = 0.0;
		return scoredRes;
	}


	protected final void enhance(Enhanced hit) {
        Map<String, Integer> scores = new HashMap<>();

//		try {
//			scores = setBlankScores(scores);
//			hit.addEnhancement(new ChemSpiderDataSourceCountEnhancement(scores.get("DataSourcesCount")));
//			hit.addEnhancement(new ChemSpiderReferencesCountEnhancement(scores.get("ReferencesCount")));
//			hit.addEnhancement(new ChemSpiderPubmedHitsEnhancement(scores.get("PubmedHits")));
//		} catch (ChemSpiderException e) {
			setBlankScores(scores);
//		}
	}

	private void setBlankScores(Map<String, Integer> scores) {
		scores.put("PubmedHits", 0);
		scores.put("DataSourcesCount", 0);
		scores.put("ReferencesCount", 0);
	}

	/**
	 * get temporary score metrics to create the scoredHits
	 *
	 * @return a map with inchikey: value pairs
	 */
	protected abstract String getMetric();

	/**
	 * am arbitrary number. The higher the value the higher the priority
	 *
	 * @return
	 */
	@Override
	public abstract Priority getPriority();

	@Override
	public abstract int compareTo(Prioritize prioritize);

	/**
	 * the description of this object
	 *
	 * @return
	 */
	@Override
	public abstract String getDescription();

	public abstract Integer getWeight();
}
