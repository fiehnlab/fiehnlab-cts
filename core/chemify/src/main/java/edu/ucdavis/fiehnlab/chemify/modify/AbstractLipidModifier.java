package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/10/13
 * Time: 4:48 PM
 * <p/>
 * check if the search term is a lipid and attemps to change it to the correct format
 */
public abstract class AbstractLipidModifier extends AbstractModifier {

	protected Logger logger = Logger.getLogger(getClass());

	protected Pattern pattern;
	private String generator;

	protected AbstractLipidModifier() {
		this(Priority.WEB_SERVICE_BASED_INTERNAL);
	}

	protected AbstractLipidModifier(Priority priority) {
		super(priority);
		this.pattern = Pattern.compile(getPattern());
	}


	/**
	 * brings any string, which matches the lipid pattern into the correct format
	 *
	 * @param searchTerm
	 * @return
	 */
	public String modify(String searchTerm) {

		if (logger.isDebugEnabled())
			logger.debug("searchTerm is: " + searchTerm);

		if (searchTerm.indexOf("((") > 0 && searchTerm.indexOf("))") > 0) {
			searchTerm = searchTerm.replace("((", "(");
			searchTerm = searchTerm.replace("))", ")");
		}

		Matcher matcher = pattern.matcher(searchTerm);

		//check if our matcher matches
		if (matcher.find()) {
			return refine(matcher);
		}

		if (logger.isDebugEnabled())
			logger.debug("=> does not match the " + this.getClass().getSimpleName() + " pattern " + this.getPattern());


		return searchTerm;
	}

	protected abstract String refine(Matcher matcher);

	public void setGenerator(String generator) {
		this.generator = generator;
	}

	public String getGenerator() {
		return generator;
	}
}
