package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wohlgemuth on 3/31/14.
 */
public class ProperCaseMultiPlexer implements Multiplexer {

	Logger logger = Logger.getLogger(getClass());

	@Override
	public Set<String> multiplex(String searchTerm) {

		Set<String> result = new HashSet<String>();

		result.add(searchTerm);

		if (!searchTerm.contains(" ") && !searchTerm.contains("-")) {
			return result;
		}

		String tmpresult = "", tmpresult1 = "";
		for (int i = 0; i < searchTerm.length(); i++) {
			String next = searchTerm.substring(i, i + 1);
			if (i == 0) {
				tmpresult += next.toUpperCase();
			} else {
				tmpresult += next.toLowerCase();
			}
		}
		String[] splited = searchTerm.split("[\\s+-]");
		String[] splited1 = new String[splited.length];

		for (int i = 0; i < splited.length; i++) {
			int l = splited[i].length();
			tmpresult1 = "";
			for (int j = 0; j < splited[i].length(); j++) {
				String next = splited[i].substring(j, j + 1);

				if (j == 0) {
					tmpresult1 += next.toUpperCase();
				} else {
					tmpresult1 += next.toLowerCase();
				}
			}
			splited1[i] = tmpresult1;
		}
		tmpresult = splited1[0];
		for (int i = 1; i < splited1.length; i++) {
			tmpresult += searchTerm.charAt(tmpresult.length()) + splited1[i];
		}

		if (tmpresult.matches("^[a-zA-Z]\\s.*$")) {
			logger.debug("BINGO");
			tmpresult.replaceFirst(" ", "-");
		}

		result.add(tmpresult.trim());

		logger.debug("multiplexed " + searchTerm + " to " + result.size() + " terms");

		return result;
	}

//	public Set<String> test(String searchTerm) {
//		Set<String> result = new HashSet<String>();
//
//		searchTerm = searchTerm.replace(' ', '-');
//
//		int index = searchTerm.indexOf("-");
//
//		List<Integer> spaceIndices = new ArrayList();
//
//		while (index > 0) {
//			spaceIndices.add(index);
//			index = searchTerm.indexOf("-", index + 1);
//		}
//
//		char[] s = searchTerm.toCharArray();
//
//		for(int i = 0; i < Math.pow(2, spaceIndices.size()); i++) {
//			String b = Integer.toBinaryString(i);
////			StringBuilder sb = new StringBuilder(b);
////			b = sb.reverse().toString();
//
//			for(int j = 0; j < b.length(); j++) {
//				s[spaceIndices.get(j)] = (b.charAt(j) == '1')? ' ' : '-';
//			}
//
//			result.add(new String(s));
//		}
//		return result;
//	}

	@Override
	public String getDescription() {
		return "generates different combinations of spaces replaced with dashes characters";
	}
}
