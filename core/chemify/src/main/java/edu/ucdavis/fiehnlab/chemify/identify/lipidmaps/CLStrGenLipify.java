package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:17 AM
 */
public class CLStrGenLipify extends AbstractLipify {
	/**
	 * this class only works for lipds
	 *
	 * @return
	 */
	@Override
	public String getPattern() {
		return CommonPatterns.LIPID_PATTERN;
	}

	@Override
	protected String getPerlScriptName() {
		return "CLStrGen.pl";
	}
}
