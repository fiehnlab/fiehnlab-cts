package edu.ucdavis.fiehnlab.chemify.identify.pubchem;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.helper.constants.ConstantsRegistry;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.CommonChemDBs;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/10/13
 * Time: 2:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class PCNameIdentify extends AbstractIdentify {

	// logger instance
	private transient static Logger logger = Logger.getLogger(PCNameIdentify.class);

	/**
	 * defines the pubchem rest api url to call
	 */
	public static final String PUG_PATH_PREFIX = "/rest/pug/compound/name/";
	public static final String PUG_PATH_SUFIX = "/property/Inchi,InchiKey,IUPACName,MolecularFormula,MolecularWeight,ExactMass/";
	private String pubchemhost;

	/**
	 * defines the format returned by the call to PUG
	 */
	public static final String PUG_FORMAT = "json";

	/**
	 * custom constructor
	 *
	 * @param priority
	 */
	public PCNameIdentify(Priority priority) {
		super(priority);
	}

	/**
	 * default contructor
	 */
	public PCNameIdentify() {
		this(Priority.EXACT_SEARCH_BASED);

		try {
			if (System.getProperty("PUBCHEM_HOST") != null && !System.getProperty("PUBCHEM_HOST").isEmpty())
				this.pubchemhost = System.getProperty("PUBCHEM_HOST");
			else {
				this.pubchemhost = "pubchem.ncbi.nlm.nih.gov";
			}
		} catch (Exception e) {
			logger.error(e.getClass().getSimpleName());
			logger.error(e.getCause() + ": " + e.getMessage());
		}
	}

	/**
	 * does the actual work
	 *
	 * @param searchTerm
	 * @return
	 */
	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {
		List<Hit> results = new Vector<Hit>();

		String path = new StringBuilder().append(PUG_PATH_PREFIX).append(searchTerm).append(PUG_PATH_SUFIX).append(PUG_FORMAT).toString();

		try (CloseableHttpClient client = HttpClientBuilder.create().build()) {

			RequestConfig requestConfig = RequestConfig.custom()
					.setSocketTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
					.setConnectTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
					.build();

			URI encodedUrl = new URI("https", this.pubchemhost, path, null);

			HttpGet request = new HttpGet(encodedUrl);
			request.setConfig(requestConfig);
			request.addHeader("Accept-Encoding", "gzip, deflate");
			request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0");

			CloseableHttpResponse response = client.execute(request);

			if (response.getStatusLine().getStatusCode() >= 400) {
//				logger.error(new StringBuilder().append("response code: ").append(response.getStatusLine().getStatusCode())
//						.append(" ").append(response.getStatusLine().getReasonPhrase())
//						.append(" [search term: ").append(searchTerm).append("]"));
				return results;
			}

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuilder entity = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				entity.append(line);
			}

			response.close();
			client.close();

//			if (logger.isDebugEnabled()) {
//				logger.debug("response: " + response);
//				logger.debug("entity: " + entity);
//			}

			JsonParser parser = new JsonParser();

			JsonObject jobj = parser.parse(entity.toString()).getAsJsonObject();

			String cid = "";
			String inchiCode = "";
			String inchiKey = "";

			for (JsonElement tmp : jobj.get("PropertyTable").getAsJsonObject().get("Properties").getAsJsonArray()) {
				JsonObject subobj = tmp.getAsJsonObject();
				cid = subobj.get("CID").getAsString();
				inchiCode = subobj.get("InChI").getAsString();
				inchiKey = subobj.get("InChIKey").getAsString();

				results.add(new HitImpl(inchiKey, this, searchTerm, original));

//				Map<String, String> extId = new HashMap<String, String>();
//				extId.put("PubChem CID", cid);

//				try {
//					registerInchi(inchiCode, original, extId);
//				} catch (ClientProtocolException e) {
//					logger.error("Got ClientProtocolException registering inchi\nMessage: " + e.getMessage() + "\nCause: " + e.getCause());
//				} catch (UnsupportedEncodingException e) {
//					logger.error("Got UnsupportedEncodingException registering inchi\nMessage: " + e.getMessage() + "\nCause: " + e.getCause());
//				} catch (IOException e) {
//					logger.error("Got IOException registering inchi\nMessage: " + e.getMessage() + "\nCause: " + e.getCause());
//				}
			}
		} catch (IOException e) {
			logger.error("Unknown error trying to identify '" + searchTerm + "'.\n\tMessage: " + e.getMessage() + "\n" + this.pubchemhost + path);
		} catch (URISyntaxException e) {
			logger.error("Malformed PUG URL", e);
		}

		return results;
	}

	public String getDescription() {
		return "this method utilizes the PubChem webservice to identify a search term";
	}

	protected final int registerInchi(String code, String original, Map<String, String> entity) throws ClientProtocolException, UnsupportedEncodingException, IOException {
		logger.debug("registering ExtID:" + entity.get("PubChem CID") + " at " + ConstantsRegistry.CTS_BASE_URL);

		String message = "";
		String json_body = "{\"inchi\":\"" + code + "\", \"name\":\"" + original + "\", \"extId\":\"" + entity.get("PubChem CID") + "\",\"idType\":\"" + CommonChemDBs.PUBCHEM_CID_NAME + "\"}";

		CloseableHttpClient httpclient = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.setConnectTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
				.build();
		HttpPost request = new HttpPost(ConstantsRegistry.CTS_UPDATER_URL);
		request.addHeader("content-type", "application/x-www-form-urlencoded");
		request.setConfig(requestConfig);
		CloseableHttpResponse response = null;

		StringEntity body = new StringEntity(json_body);
		request.setEntity(body);
		response = httpclient.execute(request);

		response.close();
		httpclient.close();

		int status = response.getStatusLine().getStatusCode();

		if (logger.isDebugEnabled()) {
			logger.debug("(" + status + ") CTS Update: " + message);
		}

		return status;
	}
}
