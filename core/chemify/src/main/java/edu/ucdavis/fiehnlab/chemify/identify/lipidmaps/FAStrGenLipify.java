package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:18 AM
 */
public class FAStrGenLipify extends AbstractLipify {
	@Override
	protected String getPerlScriptName() {
		return "FAStrGen.pl";
	}

	public String getPattern() {
		return CommonPatterns.LIPID_PATTERN;
	}
}
