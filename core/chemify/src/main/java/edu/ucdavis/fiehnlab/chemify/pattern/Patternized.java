package edu.ucdavis.fiehnlab.chemify.pattern;

import edu.ucdavis.fiehnlab.chemify.Identify;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/25/13
 * Time: 4:23 PM
 *
 * This interface is useful in case your identify objects supported regular expressions as search term and so can avoid unesseary hits
 * against online resources or database
 */
public interface Patternized {

    /**
     * returns the pre defined pattern for this implementation
     * @return
     */
    public String getPattern();
}
