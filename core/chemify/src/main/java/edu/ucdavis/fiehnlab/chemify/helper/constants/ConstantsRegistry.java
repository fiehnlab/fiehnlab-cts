package edu.ucdavis.fiehnlab.chemify.helper.constants;

/**
 * Created by diego on 8/15/14.
 */
public class ConstantsRegistry {
	public static final String CTS_BASE_URL = "http://cts.fiehnlab.ucdavis.edu";
	public static final String CTS_SCORING_URL = CTS_BASE_URL + "/service/score";

	/**
	 * default httpclient request timeout
	 */
	public static final Integer DEFAULT_TIMEOUT = 5000;
	public static final Integer TWOSECONDS = 2 * 1000;
	public static final Integer THREESECONDS = 3 * 1000;

	/**
	 * defines the default production cts updater rest url to send new data to
	 */
	public static final String CTS_UPDATER_URL = "http://cts.fiehnlab.ucdavis.edu/service/compound/register";

}
