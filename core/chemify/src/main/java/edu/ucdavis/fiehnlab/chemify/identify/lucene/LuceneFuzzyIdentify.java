package edu.ucdavis.fiehnlab.chemify.identify.lucene;

import edu.ucdavis.fiehnlab.chemify.helper.lucene.GenerateIndex;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.Query;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/15/2013
 * Time: 8:18 PM
 *
 * uses a fuzzy search algorithm
 */
public class LuceneFuzzyIdentify extends AbstractLuceneIdentify {
    public LuceneFuzzyIdentify() {
    }

    @Override
    public Query getQuery(String query) {
        return new FuzzyQuery(new Term(GenerateIndex.NAME,query + "~"),2);
    }


    public String getDescription() {
        return "this method utilizes the Lucene Text search algorithm to identify a search term by similarity";
    }
}
