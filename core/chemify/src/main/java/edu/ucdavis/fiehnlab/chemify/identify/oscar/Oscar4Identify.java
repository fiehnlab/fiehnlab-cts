package edu.ucdavis.fiehnlab.chemify.identify.oscar;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.exception.InvalidSearchTermException;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.cdk.InchiCodeConverter;
import uk.ac.cam.ch.wwmm.opsin.NameToInchi;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 2:20 PM
 * <p/>
 * uses the oscar 4 system to identify compounds
 */
public class Oscar4Identify extends AbstractIdentify {
	/**
	 * sets the internal priority of this identity object
	 *
	 * @param priority
	 */
	protected Oscar4Identify(Priority priority) {
		super(priority);
	}

	public Oscar4Identify() {
		super(Priority.VERY_LOW);
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {

		try {
			NameToInchi nti = new NameToInchi();

			InchiCodeConverter codeConverter = new InchiCodeConverter();

			String std = nti.parseToStdInchi(searchTerm);

			if (std != null) {
				List<Hit> result = codeConverter.identify(std, original);

				List<Hit> finalResult = new Vector<Hit>();
				for (Hit hit : result) {
					finalResult.add(new HitImpl(hit.getInchiKey(), this, searchTerm, original));
				}

				return finalResult;
			}

		} catch (Exception e) {
			throw new InvalidSearchTermException(e.getMessage(), e);
		}
		return Collections.EMPTY_LIST;
	}

	public String getDescription() {
		return "this method utilizes the Oscar 4 algorithm to identify a search term";
	}
}
