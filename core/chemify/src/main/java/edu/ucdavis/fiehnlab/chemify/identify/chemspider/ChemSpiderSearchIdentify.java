package edu.ucdavis.fiehnlab.chemify.identify.chemspider;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.helper.constants.ConstantsRegistry;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.CommonChemDBs;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.PatternHelper;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: jennifervannier
 * Modified: diego
 * Date: 9/25/13
 * Time: 4:32 PM
 */
public class ChemSpiderSearchIdentify extends AbstractIdentify {

    private String CTS_UPDATER_URL = ConstantsRegistry.CTS_UPDATER_URL;

    /**
     * sets the internal priority of this identity object
     *
     * @param priority
     */
    protected ChemSpiderSearchIdentify(Priority priority) {
        super(priority);
    }

    public ChemSpiderSearchIdentify() {
        this(Priority.WEB_SERVICE_BASED_EXTERNAL);
    }

    protected ChemSpiderSearchIdentify(String ctsBaseUrl) {
        this(Priority.WEB_SERVICE_BASED_EXTERNAL);
        this.CTS_UPDATER_URL = ctsBaseUrl + "/service/compound/register";
    }

    /**
     * uses ChemSpider to identify compounds or empty lists
     *
     * @param searchTerm
     * @return
     */
    @Override
    protected List<Hit> doIdentify(String searchTerm, String original) {

        List<Hit> results = new Vector<>();

//		try {
//			List<String> input = ChemspiderRester.getCsidsForToken(searchTerm, 10);
//
//			//loop through the Chemspider cids
//			for (String item : input) {
//
//				// get more info from chemspider
//				String extraData = ChemspiderRester.getDataForCsid(item);
//
//				JsonParser parser = new JsonParser();
//				JsonObject jel = parser.parse(extraData).getAsJsonArray().get(0).getAsJsonObject();
//
//				// getting name
//				String name = jel.get("Name").getAsString();
//
//				// getting inchi code and key
//				String inchicode = "", inchikey = "";
//				int pubmedHits, dataSourceCount, referenceCount = 0;
//
//				pubmedHits = jel.get("PubmedHits").getAsInt();
//				dataSourceCount = jel.get("DataSourcesCount").getAsInt();
//				referenceCount = jel.get("ReferencesCount").getAsInt();
//
//				for (JsonElement ele : jel.get("Identifiers").getAsJsonArray()) {
//					JsonObject tmp = ele.getAsJsonObject();
//
//					if (tmp.get("IdentifierType").getAsInt() == 0 && tmp.get("Version").getAsString().equals("v1.02s")) {
//						inchicode = tmp.get("Value").getAsString();
//					}
//					if (tmp.get("IdentifierType").getAsInt() == 2 && tmp.get("Version").getAsString().equals("v1.02s")) {
//						inchikey = tmp.get("Value").getAsString();
//					}
//				}
//
//				HitImpl newHit = new HitImpl(inchikey, this, searchTerm, original);
//				newHit.addEnhancement(new ChemSpiderPubmedHitsEnhancement(pubmedHits));
//				newHit.addEnhancement(new ChemSpiderDataSourceCountEnhancement(dataSourceCount));
//				newHit.addEnhancement(new ChemSpiderReferencesCountEnhancement(referenceCount));
//
//				//send inchikey/name to CTS.registerCompound
////						registerInchi(inchikey, searchTerm, chemSpiderId);
//
//				if (results.contains(newHit) == false) {
//					results.add(newHit);
//				}
//			}
//
//		} catch (ChemSpiderException e) {
////			logger.error("here is my error",e);
//			throw new InvalidSearchTermException(searchTerm + " -- " + e.getMessage(), e);
//		}

        return results;
    }

    /**
     * we do not allow to accept numbers in this class
     *
     * @return
     */
//	public String getPattern() {
//		return "[^0-9]*";
//	}
    public String getDescription() {
        return "this method utilizes the Chemspider webservice to identify a search term";
    }

    private void registerInchi(String code, String original, String extIdVal) throws ClientProtocolException, UnsupportedEncodingException, IOException {
        if (logger.isDebugEnabled()) {
            logger.info("registering ExtID:" + extIdVal + " with inchi: " + code);
        }

        String message = "";
        String codeType = code.matches(PatternHelper.STD_INCHI_KEY_PATTERN) ? "inchikey" : "inchicode";

        String json_body = "{" + codeType + ":\"" + code + "\", \"searchTerm\":\"" + original + "\", \"extId\":\"" + extIdVal + "\",\"idType\":\"" + CommonChemDBs.CHEMSPIDER_NAME + "\"}";

        CloseableHttpClient httpclient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom()
              .setSocketTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
              .setConnectTimeout(ConstantsRegistry.DEFAULT_TIMEOUT)
              .build();
        HttpPost request = new HttpPost(CTS_UPDATER_URL);

        request.addHeader("Content-Type", "application/json");
        request.setConfig(requestConfig);
        CloseableHttpResponse response = null;

        StringEntity body = new StringEntity(json_body);
        request.setEntity(body);
        response = httpclient.execute(request);

        int status = response.getStatusLine().getStatusCode();
        message = response.getStatusLine().getReasonPhrase();

        response.close();
        httpclient.close();

        if (logger.isDebugEnabled()) {
            logger.debug("(" + status + ") CTS Updater: " + message);
        }

    }
}
