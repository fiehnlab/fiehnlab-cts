package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:20 AM
 */
public class SPStrGenLipify extends AbstractLipify {
	@Override
	protected String getPerlScriptName() {
		return "SPStrGen.pl";
	}

	public String getPattern() {
		return CommonPatterns.SPHINGOLIPIDS_PATTERN;
	}
}
