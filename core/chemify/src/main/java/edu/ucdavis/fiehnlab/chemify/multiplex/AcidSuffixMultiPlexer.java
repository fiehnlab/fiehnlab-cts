package edu.ucdavis.fiehnlab.chemify.multiplex;

import edu.ucdavis.fiehnlab.chemify.Multiplexer;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wohlgemuth on 3/31/14.
 */
public class AcidSuffixMultiPlexer implements Multiplexer {

	Logger logger = Logger.getLogger(getClass());

	@Override
	public Set<String> multiplex(String searchTerm) {

		Set<String> result = new HashSet<String>();

		result.add(searchTerm);

		result.add(searchTerm.replace("ic acid", "ate"));

		if (logger.isDebugEnabled()) {
			logger.debug("multiplexed " + searchTerm + " to " + result.size() + " term/s");
		}

		return result;
	}

	@Override
	public String getDescription() {
		return "generates different combinations of spaces replaced with dashes characters";
	}
}
