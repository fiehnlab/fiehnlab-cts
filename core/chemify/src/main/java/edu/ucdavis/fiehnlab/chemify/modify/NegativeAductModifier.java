package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/24/13
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class NegativeAductModifier extends AbstractAductDataModifier {

    public NegativeAductModifier() {
        super(Priority.MEDIUM);
    }

    /**
     * returns the pre defined pattern for this implementation
     *
     * @return
     */
    public String getPattern() {
        return CommonPatterns.NEGATIVE_ADUCT_PATTERN;
    }

    /**
     * the description of this object
     *
     * @return
     */
    public String getDescription() {
        return "Removes the '[M?]- ?' from the search term, since this is MS aduct information";
    }
}
