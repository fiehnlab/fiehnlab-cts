package edu.ucdavis.fiehnlab.chemify.exception;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/26/13
 * Time: 2:21 PM
 * <p/>
 * A basic class to provide us with information why a part failed
 */
public class InvalidSearchTermException extends ChemifyException {
	public InvalidSearchTermException(String message) {
		super(message);
	}

	public InvalidSearchTermException(String message, Exception e) {
		super(message, e);
	}
}
