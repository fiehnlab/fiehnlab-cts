package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.exception;

import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 12:26 AM
 */
public class LipidException extends ChemifyException{
    public LipidException(String s) {
        super(s);
    }


    public LipidException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
