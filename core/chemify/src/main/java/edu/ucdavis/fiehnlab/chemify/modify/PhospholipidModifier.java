package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/18/13
 * Time: 3:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class PhospholipidModifier extends AbstractLipidModifier {

	public String getDescription() {
		return "ensures that the search term is a valid phospholipid string and re-formats it to a valid phospholipid abbreviation";
	}

	protected String refine(Matcher matcher) {
		if (logger.isDebugEnabled())
			logger.debug("=> is a phospholipid");

		String result = matcher.group(1);

		if (logger.isTraceEnabled()) {
			logger.trace("=> extracted value is: " + result);

			logger.trace("groups: " + matcher.groupCount());
			for (int i = 1; i <= matcher.groupCount(); i++) {
				logger.trace("group" + i + ": " + matcher.group(i));
			}
		}

		String lipidclass = matcher.group(3);
		String chains = matcher.group(4);

		//replace possible - with /
		if (chains.contains("-")) {
			chains = chains.replace("-", "/");
		}

		// if there are no parenthesis, we need to add brackets
		if (chains.indexOf("(") != 0 && chains.endsWith(")") == false) {
			if (logger.isDebugEnabled())
				logger.debug("=> contains no parentheses, which will be added!");

			if (matcher.group(2) != null) {
				result = matcher.group(2) + lipidclass + "(" + chains + ")";
			} else {
				result = lipidclass + "(" + chains + ")";
			}
		}

		// get rid of space between class and chain definition
		if (result.indexOf(" ") == lipidclass.length()) {
			if (logger.isDebugEnabled())
				logger.debug("=> contains a space, which will be removed!");
			result = result.replaceFirst("(.*?) (.*)", "$1$2");
		}

		// normalize the 'plasmenyl' variations -> Plasmenyl-*
		if (lipidclass.matches("(?:\\b[Pp]lasmenyl|\\b[po])-?(.*)")) {
			result = result.replaceFirst("(?:\\b[Pp]lasmenyl|\\b[po])-?(.*)", "Plasmenyl-$1");
		}

		// normalize the 'Lyso' variations -> L*
		if (lipidclass.matches("(.*?)(?:\\b[Ll](?:yso)?)-?(.*)")) {
			result = result.replaceFirst("(.*?)(?:\\b[Ll](?:yso)?)-?(.*)", "$1L$2");
		}

		//long name variations
		if (lipidclass.toLowerCase().startsWith("phosphatidyl") || lipidclass.toLowerCase().startsWith("glycerophospho")) {
			Map<String, String> prefix = new HashMap<String, String>();
			prefix.put("phosphatidyl", "P");
			prefix.put("glycerophosp", "P");

			Map<String, String> suffix = new HashMap<String, String>();
			suffix.put("choline", "C");
			suffix.put("ethanolanime", "E");
			suffix.put("inositol", "I");
			suffix.put("glycerol", "G");
			suffix.put("serine", "S");

			logger.debug("pre: " + prefix.get(lipidclass.substring(0, 12).toLowerCase()));
			logger.debug("pos: " + suffix.get(lipidclass.substring(12, lipidclass.length()).toLowerCase()));
			String pre = prefix.get(lipidclass.substring(0, 12).toLowerCase());
			String pos = suffix.get(lipidclass.substring(12, lipidclass.length()).toLowerCase());

			result = result.replace(lipidclass, pre + pos);
		}

		// add post chain info
		if (matcher.group(6) != null) {
			result = result + matcher.group(6);
		}

		if (logger.isDebugEnabled())
			logger.debug("=> result is: " + result);

		//all modifications are done, let's return the result
		return result;
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.PHOSPHOLIPIDS_PATTERN;
	}
}
