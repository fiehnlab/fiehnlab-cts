package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/11/2013
 * Time: 1:21 AM
 */
public class STStrGenLipify extends AbstractLipify {
	@Override
	protected String getPerlScriptName() {
		return "STStrGen.pl";
	}

	@Override
	protected String getSuccessMessage() {
		return ".*for 1 compound....*";
	}

	public String getPattern() {
		return CommonPatterns.STEROLS_PATTERN;
	}
}
