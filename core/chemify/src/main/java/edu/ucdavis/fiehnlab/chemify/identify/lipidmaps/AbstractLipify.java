package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl;
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify;
import edu.ucdavis.fiehnlab.chemify.identify.lipidmaps.exception.LipidException;
import edu.ucdavis.fiehnlab.chemify.pattern.Patternized;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.iterator.IteratingSDFReader;

import java.io.*;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/10/2013
 * Time: 11:30 PM
 * <p/>
 * using the lipid map tools to identify a lipid
 */
public abstract class AbstractLipify extends AbstractIdentify implements Patternized {

	/**
	 * needs to be somehow set dynamically
	 */
	private String lipidmapsPath = "";

	/**
	 * sets the internal priority of this identity object
	 */
	protected AbstractLipify() {
		super(Priority.CALCULATION_BASED);
	}

	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {

		List<Hit> result = new Vector<Hit>();

		this.lipidmapsPath = configuration.getLipidmapsPath();
		logger.debug("Lipidmaps path: " + lipidmapsPath);
		if (!this.lipidmapsPath.endsWith("/")) {
			lipidmapsPath = lipidmapsPath.concat("/");
		}

		//ensure we got a correct lipid based search string

		if (logger.isDebugEnabled())
			logger.debug("searching for: " + searchTerm);

		try {

			File out = File.createTempFile(this.getClass().getSimpleName(), "", new File(System.getProperty("java.io.tmpdir")));
			File out2 = new File(out.getAbsolutePath() + ".sdf");
			try {
				if (logger.isDebugEnabled())
					logger.debug("storing sdf file at: " + out.getAbsolutePath());

				StringBuilder scriptName = new StringBuilder();
				scriptName.append(lipidmapsPath).append(getPerlScriptName());

				if (!(new File(scriptName.toString()).exists())) {
					throw new LipidException("Can't find the required lipidmaps script to identify this name.\nScript name: " + scriptName.toString());
				}

				StringBuilder command = new StringBuilder();
				String os = System.getProperty("os.name").toLowerCase();

				command.append(getPerlPath())
//					.append("perl ")
						.append(os.contains("windows") ? "perl.exe " : "perl ")
						.append(scriptName)
						.append(" -o -r ")
						.append(out.getAbsolutePath())
						.append(" ")
						.append(searchTerm);

				if (logger.isDebugEnabled())
					logger.debug("=> lipid maps command: " + command.toString());

				Process process = Runtime.getRuntime().exec(command.toString());

				process.waitFor();

				boolean success = checkProcessResult(process);

				if (success) {
					if (logger.isDebugEnabled())
						logger.debug("=> found a valid structure");

					//time to load the SDF and build an inchi key from it

					IteratingSDFReader reader = new IteratingSDFReader(new FileInputStream(out.getAbsolutePath() + ".sdf"), DefaultChemObjectBuilder.getInstance(), true);
					while (reader.hasNext()) {

						IAtomContainer mol = reader.next();
						String key = InChIGeneratorFactory.getInstance().getInChIGenerator(mol).getInchiKey();

						if (logger.isDebugEnabled())
							logger.debug("=> generated key: " + key);

						Hit hit = new HitImpl(key, this, searchTerm, original);

						if (!result.contains(hit)) {
							result.add(hit);
						}
					}

				} else {
					logger.warn("=> not a valid structure!");
				}
			} finally {
				out2.delete();
				out.delete();
			}
		} catch (NullPointerException e) {
			logger.error("Null pointer exception: " + e.getMessage() + "\n" + e.getCause()); //+ "\n" + e.getStackTrace()
		} catch (Exception e) {
			throw new LipidException(e.getMessage(), e);
		}

		return result;
	}

	/**
	 * ensures that the process finds an abbreviation
	 *
	 * @param process
	 * @return
	 * @throws IOException
	 */
	private boolean checkProcessResult(Process process) throws IOException {
		String line = null;

		boolean success = false;

		BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
		while ((line = input.readLine()) != null) {
			if (logger.isDebugEnabled())
				logger.debug(line);
			if (line.matches(getSuccessMessage())) {
				success = true;
				break;
			}
		}

		input.close();

		OutputStream outputStream = process.getOutputStream();
		PrintStream printStream = new PrintStream(outputStream);
		printStream.println();
		printStream.flush();
		printStream.close();
		return success;
	}

	/**
	 * pattern for a successful command generation
	 *
	 * @return
	 */
	protected String getSuccessMessage() {
		return "^Valid abbreviations count:.*";
	}

	/**
	 * this class only works for lipds
	 *
	 * @return
	 */
	public abstract String getPattern();

	/**
	 * which perl script would we like to use to convert our search term into an inchi key
	 *
	 * @return
	 */
	protected abstract String getPerlScriptName();

	/**
	 * where is perl installed (from PERL_HOME env var)
	 *
	 * @return
	 */
	protected String getPerlPath() {
		logger.error(configuration.getClass());
		return configuration.getPerlPath();
	}

	public String getDescription() {
		return "this method utilizes the Lipid Maps tools to identify a search term";
	}
}
