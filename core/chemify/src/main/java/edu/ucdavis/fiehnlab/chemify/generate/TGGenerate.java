package edu.ucdavis.fiehnlab.chemify.generate;

import edu.ucdavis.fiehnlab.chemify.exception.*;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 3:26 PM
 * <p/>
 * little stupid generator to get all lipid possibilities for summarized TG's
 */
public class TGGenerate {

    private Pattern pattern = Pattern.compile(CommonPatterns.TG_SUMMARIZED_LIPID_PATTERN);

    private Logger logger = Logger.getLogger(getClass());

    /**
     * generates all possible combinations, but get's cancelled after a specified timeout
     *
     * @param input
     * @return
     */
    public Set<String> generate(final String input, final boolean includeOdd) throws ChemifyException {

        if (logger.isDebugEnabled())
            logger.debug("input: " + input);


        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<Set<String>> future = service.submit(new Callable<Set<String>>() {
            @Override
            public Set<String> call() throws Exception {
                Matcher matcher = pattern.matcher(input);

                Set<String> result = new HashSet<String>();

                if (matcher.matches()) {
                    if (matcher.groupCount() == 6) {
                        int max = new Integer(matcher.group(5));
                        int maxDouble = new Integer(matcher.group(6));

                        for (int sn1 = 2; sn1 < max; sn1++) {
                            for (int sn2 = 2; sn2 < max; sn2++) {
                                for (int sn3 = 2; sn3 < max; sn3++) {
                                    if (sn1 + sn2 + sn3 == max) {
                                        for (int dbSn1 = 0; dbSn1 <= maxDouble; dbSn1++) {
                                            for (int dbSn2 = 0; dbSn2 <= maxDouble; dbSn2++) {
                                                for (int dbSn3 = 0; dbSn3 <= maxDouble; dbSn3++) {
                                                    if (dbSn2 + dbSn3 + dbSn1 == maxDouble) {
                                                        if (dbSn1 <= sn1) {
                                                            if (dbSn2 <= sn2) {
                                                                if (dbSn3 <= sn3) {
                                                                    if ((sn1) > dbSn1) {
                                                                        if ((sn2) > dbSn2) {
                                                                            if ((sn3) > dbSn3) {
                                                                                if (includeOdd == false) {
                                                                                    if (sn1 % 2 == 0) {
                                                                                        if (sn2 % 2 == 0) {
                                                                                            if (sn3 % 2 == 0) {
                                                                                                result = generateTGString(result, sn1, sn2, sn3, dbSn1, dbSn2, dbSn3);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    result = generateTGString(result, sn1, sn2, sn3, dbSn1, dbSn2, dbSn3);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (logger.isDebugEnabled())
                            logger.debug("=> generated " + result.size() + " TG's");

                    }
                }
                return result;
            }
        });


        Set<String> result = null;
        try {
            result = future.get(calculationTimeout(), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new edu.ucdavis.fiehnlab.chemify.exception.TimeoutException("exception: " + e.getMessage());
        } catch (ExecutionException e) {

            throw new edu.ucdavis.fiehnlab.chemify.exception.TimeoutException("exception: " + e.getMessage());
        } catch (TimeoutException e) {

            throw new edu.ucdavis.fiehnlab.chemify.exception.TimeoutException("exception: " + e.getMessage());


        } finally {
            service.shutdown();
        }

        return result;
    }

    /**
     * how long can this calculations maximal take
     * @return
     */
    protected int calculationTimeout() {
        return 15;
    }

    private Set<String> generateTGString(Set<String> result, int sn1, int sn2, int sn3, int dbSn1, int dbSn2, int dbSn3) {
        StringBuffer lipid = new StringBuffer();

        lipid.append("TG(");
        lipid.append(sn1);
        lipid.append(":");
        lipid.append(dbSn1);
        lipid.append("/");
        lipid.append(sn2);
        lipid.append(":");
        lipid.append(dbSn2);
        lipid.append("/");
        lipid.append(sn3);
        lipid.append(":");
        lipid.append(dbSn3);
        lipid.append(")");

        result.add(lipid.toString());

        if (logger.isDebugEnabled())
            logger.debug("=> generated: " + lipid.toString());

        return result;
    }

}
