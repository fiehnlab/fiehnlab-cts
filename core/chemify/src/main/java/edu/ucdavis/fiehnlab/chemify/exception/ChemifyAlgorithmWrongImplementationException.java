package edu.ucdavis.fiehnlab.chemify.exception;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Oct/15/2013
 * Time: 8:08 PM
 *
 * somebody implemented a sub algorithm wrong...
 */
public class ChemifyAlgorithmWrongImplementationException extends CantContinueException
{
    public ChemifyAlgorithmWrongImplementationException(String s) {
        super(s);
    }

    public ChemifyAlgorithmWrongImplementationException(String s, ChemifyException e) {
        super(s, e);
    }
}
