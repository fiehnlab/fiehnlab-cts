package edu.ucdavis.fiehnlab.chemify.identify.lipidmaps;

import edu.ucdavis.fiehnlab.chemify.Hit;
import edu.ucdavis.fiehnlab.chemify.annotations.RegisterOnStartup;
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException;
import edu.ucdavis.fiehnlab.chemify.generate.TGGenerate;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

import java.util.List;
import java.util.Set;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/11/13
 * Time: 3:53 PM
 */
@RegisterOnStartup(disabled = true)
public class SummarizedTGLipidIdentify extends AbstractLipify {
	@Override
	protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {
		List<Hit> result = new Vector<Hit>();

		if (searchTerm.matches(CommonPatterns.TG_SUMMARIZED_LIPID_PATTERN)) {
			Set<String> tgs = new TGGenerate().generate(searchTerm, true);

			for (String tg : tgs) {

				for (Hit hit : super.doIdentify(tg, original)) {
					result.add(hit);
				}
			}
		}

		return result;
	}

	/**
	 * this class only works for lipds
	 *
	 * @return
	 */
	@Override
	public String getPattern() {
		return CommonPatterns.TG_SUMMARIZED_LIPID_PATTERN;
	}

	@Override
	protected String getPerlScriptName() {
		return "GLStrGen.pl";
	}
}
