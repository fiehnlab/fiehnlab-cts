package edu.ucdavis.fiehnlab.chemify;

import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/24/13
 * Time: 6:52 PM
 * <p/>
 * Basic configuration object for chemify algorithms
 */
public interface Configuration {

	/**
	 * returns all configured algorithms to be used by the chemify algorithm
	 *
	 * @return
	 */
	public List<Identify> getAlgorithms();

	public Configuration setAlgorithms(List<Identify> algorithms);

	/**
	 * a list of registered filters to reduce the data output
	 *
	 * @return
	 */
	public Set<Filter> getFilters();

	public Configuration setFilters(Set<Filter> filters);

	/**
	 * a set of enhancement objects to modify our result to our liking and add additional data to it
	 *
	 * @return
	 */
	public Set<Enhance> getEnhances();

	public Configuration setEnhances(Set<Enhance> enhances);

	/**
	 * returns all known search term modifications
	 *
	 * @return
	 */
	public List<Modify> getModifications();

	public Configuration setModifications(List<Modify> modifications);

	/**
	 * returns an implementation of the scoring algorithm
	 *
	 * @return
	 */
	public List<Scoring> getScoringAlgorithms();

	public Configuration setScoringAlgorithms(List<Scoring> scoringAlgorithms);

	/**
	 * adds a method to enhance our result
	 *
	 * @param enhance
	 * @return
	 */
	public Configuration addEnhance(Enhance enhance);

	/**
	 * adds a filter
	 *
	 * @param filter
	 * @return
	 */
	public Configuration addFilter(Filter filter);

	/**
	 * adds a new algorithm to this configuration
	 *
	 * @param identify
	 * @return
	 */
	public Configuration addAlgorithm(Identify identify);

	/**
	 * sets the scoring algortihm
	 *
	 * @param scoringAlgorithm
	 * @return
	 */
	public Configuration addScoringAlgorithm(Scoring scoringAlgorithm);

	/**
	 * do we want to continue the algorithm, if we encounter an exception
	 *
	 * @return
	 */
	public boolean isContinueOnException(Exception e);

	/**
	 * do we accept none identified results?
	 *
	 * @return
	 */
	public boolean isNonIdentifiedResultsPermitted();

	public Configuration setNonIdentifiedResultsPermitted(boolean value);

    /**
     * returns all available multiplexer
     * @return
     */
    public Set<Multiplexer> getMultiplexer();

    /**
     * sets the multiplexers
     * @return
     */
    public void setMultiplexer(Set<Multiplexer> multiplexers);

	/**
	 * returns an implementation of the search term quality threshold
	 *
	 * @return
	 */
	public Double getSearchTermMinQuality();

	public Configuration setSearchTermMinQuality(Double searchTermMinQuality);


	public String getLipidmapsPath();

	public Configuration setLipidmapsPath(String lipidmapsPath);

	public String getPerlPath();

	public Configuration setPerlPath(String path);
}
