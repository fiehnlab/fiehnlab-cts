package edu.ucdavis.fiehnlab.chemify.modify;

import edu.ucdavis.fiehnlab.chemify.Priority;
import edu.ucdavis.fiehnlab.chemify.pattern.CommonPatterns;

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 10/14/13
 * Time: 3:58 PM
 */
public class NISTModifier extends AbstractModifier {

    public NISTModifier(){
        super(Priority.HIGH);
    }

	public String modify(String searchTeam) {
		return searchTeam.replaceFirst(getPattern(), "$1$2").trim();

	}

	public String getDescription() {
		return "removes not needed NIST String at the end of the search term since this marks a NIST standard";
	}

	/**
	 * returns the pre defined pattern for this implementation
	 *
	 * @return
	 */
	public String getPattern() {
		return CommonPatterns.NIST_PATTERN;
	}
}
