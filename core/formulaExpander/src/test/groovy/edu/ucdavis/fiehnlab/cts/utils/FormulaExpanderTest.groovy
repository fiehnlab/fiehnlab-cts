package edu.ucdavis.fiehnlab.cts.utils

import org.junit.Test
/**
 * Created by diego on 7/30/15.
 */
class FormulaExpanderTest extends GroovyTestCase {
	FormulaExpander fe

	@Test
	void testExpand() {
		def formula = [:]
		fe = new FormulaExpander()

		formula["H"] = "H"
		formula["N2"] = "NN"
		formula["CO2"] = "COO"
		formula["NaCl"] = "ClNa"
		formula["H2O"] = "HHO"
		formula["OH2"] = "HHO"
		formula["C6H12"] = "CCCCCCHHHHHHHHHHHH"
		formula["C8H10N4O2"] = "CCCCCCCCHHHHHHHHHHNNNNOO"
		formula["C8H10O2N4"] = "CCCCCCCCHHHHHHHHHHNNNNOO"
		formula["CH3CH2CH2CH3"] = "CCCCHHHHHHHHHH"

		formula.entrySet().each { Map.Entry<String, String> entry ->
			long start = System.nanoTime()
			def res = fe.expand(entry.key)
			def diff = (System.nanoTime() - start) / 1000000000
			log.info("${entry.key} expanded to ${res} in ${diff}ms")
			assertEquals(entry.value, res)
		}
	}

}
