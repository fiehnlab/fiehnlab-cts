package edu.ucdavis.fiehnlab.cts.utils

/**
 * Created by diego on 7/30/15.
 *
 * Tool that converts a molecular formula in a string of atoms
 */
public class FormulaExpander {

	public String expand(String formula) throws Exception {
		def r = []

		formula.split("(?=\\p{Upper})").each {
			if (!it.isEmpty()) {
				def multy = it.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

				if(1 == multy.length) {
					r.add(multy[0])
				} else {
					int times = Integer.parseInt(multy[1])
					r.add(multy[0] * times)
				}
			}
		}

		return r.sort().join("")
	}
}
