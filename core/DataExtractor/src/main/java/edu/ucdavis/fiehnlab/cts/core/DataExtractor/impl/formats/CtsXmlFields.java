package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

public class CtsXmlFields {
	public static final String CTSXML_COMPOUND_ROOT = "Compounds";
	public static final String CTSXML_SUBSTANCE_ROOT = "Substances";
	public static final String CTSXML_COMPOUND_ELE = "Compound";
	public static final String CTSXML_SUBSTANCE_ELE = "Substance";
	public static final String CTSXML_NAMES_ELE = "Names";
	public static final String CTSXML_NAME_ELE = "Name";
	public static final String CTSXML_IDS_ELE = "Ids";
	public static final String CTSXML_ID_ELE = "Id";
	public static final String CTSXML_INCHI_ELE = "Inchi";
	public static final String CTSXML_INCHICODE_ELE = "Code";
	public static final String CTSXML_INCHIKEY_ELE = "Key";
	public static final String CTSXML_PUBCHEM_CID = "cid";
	public static final String CTSXML_PUBCHEM_SID = "sid";
	public static final String CTSXML_SYNONYMS_ELE = "Synonyms";
	public static final String CTSXML_SYNONYM_ELE = "Synonym";
}
