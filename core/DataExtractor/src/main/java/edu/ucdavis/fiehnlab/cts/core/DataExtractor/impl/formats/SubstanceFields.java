package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

/**
 *
 * @author Diego
 */
public class SubstanceFields {
	public static final String ID = "ID";
	
	public static final String ROOT_ELE = "PC-Substances";
	public static final String MAIN_ELE = "PC-Substance";
	public static final String SID_ELE = "PC-ID_id";
	public static final String CID_LONG_ELE = "PC-Compound/PC-Compound_id/PC-CompoundTypePC-CompoundType_type/PC-CompoundType_id/PC-CompoundType_id_cid";
	public static final String CID_ELE = "PC-CompoundType_id_cid";
	public static final String SYNONYMS_ELE = "PC-Substance_synonyms";
	public static final String SYNONYMS_VALUE_ELE = "PC-Substance_synonyms_E";
	public static final String NAME_ELE = "";
	public static final String INCHIKEY_ELE = "InChIKey";
	
	public static final String SID_PROPERTY = "PUBCHEM_SUBSTANCE_ID";
	public static final String NAME_PROPERTY ="PUBCHEM_MMDB_MOLECULE_NAME";
	public static final String SYNONYM_PROPERTY="PUBCHEM_SUBSTANCE_SYNONYM";
	public static final String XREF_DATASOURCE_NAME_PROPERTY="PUBCHEM_EXT_DATASOURCE_NAME";
	public static final String XREF_DATASOURCE_ID_PROPERTY="PUBCHEM_EXT_DATASOURCE_REGID";
	public static final String XREF_SUBSTANCE_URL_PROPERTY="PUBCHEM_EXT_SUBSTANCE_URL";
	public static final String XREF_DATASOURCE_URL_PROPERTY="PUBCHEM_EXT_DATASOURCE_URL";
	public static final String CAS_NUMBER = "PUBCHEM_GENERIC_REGISTRY_NAME";

	public static final String PUBCHEM_SID = "Pubchem SID";
	public static final String PUBCHEM_BASE_URL = "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=";



}
