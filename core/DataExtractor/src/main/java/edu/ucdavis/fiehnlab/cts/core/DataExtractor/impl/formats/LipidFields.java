package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/27/13
 * Time: 4:12 PM
 *
 * Declaration of constants for use in Lipid file parsers
 */
public class LipidFields {
	public static final String ABBREV_PROPERTY = "Abbrev";
	public static final String SYSTEMATIC_NAME_PROPERTY = "Systematic Name";

}
