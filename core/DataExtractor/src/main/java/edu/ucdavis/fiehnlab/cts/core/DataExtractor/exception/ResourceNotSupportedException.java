package edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception;

public class ResourceNotSupportedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResourceNotSupportedException() {
	}

	public ResourceNotSupportedException(String arg0) {
		super(arg0);
	}

	public ResourceNotSupportedException(Throwable arg0) {
		super(arg0);
	}

	public ResourceNotSupportedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
