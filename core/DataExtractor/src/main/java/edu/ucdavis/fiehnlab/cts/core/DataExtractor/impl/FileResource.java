package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl;

import java.io.File;

/**
 * represents a file on the file system
 * 
 * @author wohlgemuth
 * 
 */
public class FileResource extends AbstractResourceImpl {

	private File file;

	public FileResource(File file) {
		this.file = file;
	}

	/**
	 * return the internal file as representation
	 */
	public File getRepresentation() {
		return file;
	}

    /**
     * returns the name of the file
     */
    public String getName() {
        return file.getName();
    }

	/**
	 * test if the given file is actually valid and exist
	 */
	public boolean isValid() {
		if (file == null) {
			return false;
		}
		if (file.exists()) {
			return true;
		}
		if (file.canRead()) {
			return true;
		}

		return false;
	}

}
