package edu.ucdavis.fiehnlab.cts.core.DataExtractor;

import java.io.File;
import java.io.InputStream;

import org.jdom.Element;

/**
 * you can work on resources and chain their processing methods
 * 
 * @author wohlgemuth
 * 
 */
public interface Resource {

	/**
	 * processes the given resource with the algorithm of the current resource
	 * 
	 * @param resource
	 * @return
	 */
	public Resource process(Resource resource);

	/**
	 * simple internal processing, doesn't take an argument
	 * 
	 * @return
	 */
	public Resource process();

	/**
	 * processes an input stream
	 * 
	 * @param input
	 * @return
	 */
	public Resource process(InputStream input);

	/**
	 * processes an jdom element
	 * 
	 * @param input
	 * @return
	 */
	public Resource process(Element element);

	/**
	 * processes a file
	 * 
	 * @param input
	 * @return
	 */
	public Resource process(File file);

	/**
	 * representation of this resource
	 * 
	 * @return
	 */
	Object getRepresentation();

	/**
	 * previous processing step
	 * 
	 * @return
	 */
	public Resource getProcessedWith();

	/**
	 * sets the previous step
	 * 
	 * @param previousStep
	 */
	void setProcessedWith(Resource previousStep);

	/**
	 * has this resource been processed with something else
	 * 
	 * @return
	 */
	public boolean isProcessed();

	/**
	 * is this resource valid
	 * 
	 * @return
	 */
	public boolean isValid();

	/**
	 * does this resource accepts this resource type? If this is not the case
	 * the processing method will throw an error
	 * 
	 * @param resource
	 * @return
	 */
	public boolean isResourceTypeAccepted(Resource resource);
	
	public String getQuery();

    public String getName();
    public void setName(String name);
}
