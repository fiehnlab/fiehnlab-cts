/**
 *
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor.model;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.CtsXmlFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author dpedrosa
 */
public class Substance extends AbstractResourceImpl {

	/**
	 * Substance Pubchem ID
	 */
	private String sid = "";

	/**
	 * InChIKey to wich this substance is related
	 */
	private String inchiKey = "";
	private String inchiCode = "";
	private Double molWeight = 0.0;
	private Double exactMass = 0.0;
	private String sourceFile = "";
	private String formula = "";

	/**
	 * List of ids for the substance
	 * k = name of id
	 * v = value of id
	 * u = url
	 */
	private Map<String, IdObject> ids = new HashMap<String, IdObject>();

	/**
	 * List of names for the substance
	 * k = key is the type (IUPAC, Synonym)
	 * v = list of names for this type
	 */
	private Map<String, Collection<NameObject>> names = new HashMap<String, Collection<NameObject>>();

	/**
	 * Creates a new empty Substance.
	 */
	public Substance() {}

	/**
	 * Creates a new Substance with a pubchem substance id.
	 *
	 * @param sid substance id in pubchem db
	 */
	public Substance(String sid) {
		this.sid = sid;
	}

	public String getSid() {
		return this.sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	/**
	 * @return the inchiKey
	 */
	public String getInchiKey() {
		return inchiKey;
	}

	/**
	 * @param inchiKey the inchiKey to set
	 */
	public void setInchiKey(String inchiKey) {
		this.inchiKey = inchiKey;
	}

	public String getInchiCode() {
		return inchiCode;
	}

	public void setInchiCode(String inchiCode) {
		this.inchiCode = inchiCode;
	}

	public Double getMolWeight() {
		return molWeight;
	}

	public void setMolWeight(Double molWeight) {
		this.molWeight = molWeight;
	}

	public void setNames(Map<String, Collection<NameObject>> names) {
		this.names = names;
	}

	public void addName(NameObject name) {
		if (!names.containsKey(CtsXmlFields.CTSXML_SYNONYMS_ELE)) {
			names.put(CtsXmlFields.CTSXML_SYNONYMS_ELE, new HashSet<NameObject>());
		}
		names.get(CtsXmlFields.CTSXML_SYNONYMS_ELE).add(name);
	}

	public void addId(String key, IdObject id) {
		this.ids.put(key, id);
	}

	public void setIds(Map<String, IdObject> ids) {
		this.ids = ids;
	}

	public Map<String, IdObject> getIds() {
		return this.ids;
	}

	public ArrayList<IdObject> getIdList() {
		return new ArrayList<IdObject>(ids.values());
	}

	public Double getExactMass() {
		return exactMass;
	}

	public void setExactMass(Double exactMass) {
		this.exactMass = exactMass;
	}

	public String getFormula() {
		return this.formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	/**
	 * Returns a map containing the names for this substance
	 * @return A map of Strings (as keys) with a Collection<NameObject> as values
	 */
	public Map<String, Collection<NameObject>> getNames() {
		if(!names.isEmpty() && !names.values().isEmpty()) {
			return names;
		} else {
			return new HashMap<String, Collection<NameObject>>();
		}
	}

	public Set<String> getSynonymNames() {
		return getNames(CtsXmlFields.CTSXML_SYNONYMS_ELE);
	}

	private Set<String> getNames(String key) {
		Map<String, Collection<String>> nameList = new HashMap<String, Collection<String>>();

		nameList.put(CtsXmlFields.CTSXML_SYNONYMS_ELE, new HashSet<String>());
		// turning NameObjects to Strings
		if(names.size() > 0) {
			for (NameObject no : names.get(CtsXmlFields.CTSXML_SYNONYMS_ELE)) {
				nameList.get(CtsXmlFields.CTSXML_SYNONYMS_ELE).add(no.value);
			}

			return (Set<String>) nameList.get(CtsXmlFields.CTSXML_SYNONYMS_ELE);
		} else {
			return new HashSet<String>();
		}
	}

	@Override
	public String toString() {
		List iupaclist = new ArrayList<>();
		for (String key : getNames().keySet()) {
			List<NameObject> vals = new ArrayList<>(names.get(key));
			Collections.sort(vals);
			iupaclist.add(key + "=" + vals);
			Collections.sort(iupaclist);
		}

		return this.getSid() + Encoder.newline +
				this.getInchiKey() + Encoder.newline +
				this.getInchiCode() + Encoder.newline +
				this.getFormula() + Encoder.newline +
				this.getMolWeight() + Encoder.newline +
				this.getExactMass() + Encoder.newline +
				iupaclist + Encoder.newline +
				this.getIds();
	}

	@Override
	public boolean equals(Object obj) {
		try {
			Substance i = (Substance) obj;

			Boolean value = i.sid.equals(this.sid) && i.inchiKey.equals(this.inchiKey);

			for (Entry<?, ?> e : ids.entrySet()) {
				value = value && this.ids.containsKey(e.getKey());
				value = value && this.ids.containsValue((IdObject) e.getValue());
			}

			for (Entry<?, ?> name : names.entrySet()) {
				value = value && this.names.containsKey(name.getKey()) && this.names.containsValue(name.getValue());
			}
			return value;
		} catch (ClassCastException e) {
			// not required class just continues to false
		} catch (NullPointerException e) {
			// not required class just continues to false
		}
		return false;
	}

	@Override
	public String getQuery() {
		/*
		 * old sholdn't be needed delete from synonym where inchi_key = E'123456789012345678901234567' and name = E'xxxx';
		 *
		 * select insert_synonym(E'123456789012345678901234567', E'xxxx', E'Synonym');
		 */
		StringBuilder query = new StringBuilder();
		if (!this.inchiKey.isEmpty()) {
			// insert compound info
			query.append("select insert_compound(E'").append(Encoder.encodePostgre(this.inchiKey))
				.append("', E'").append(Encoder.encodePostgre(this.inchiCode).trim())
				.append("', ").append(this.molWeight)
				.append(", ").append(this.exactMass)
				.append(", E'").append(Encoder.encodePostgre(this.formula).trim())
				.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);

			// refresh info about synonyms for compound
			for (Entry<String, Collection<NameObject>> syn : this.names.entrySet()) {
				for (NameObject n : syn.getValue()) {
					// reinsert synonym
					query.append("select insert_synonym(E'").append(Encoder.encodePostgre(this.inchiKey))
						.append("', E'").append(Encoder.encodePostgre(n.value).trim())
						.append("', E'").append(Encoder.encodePostgre(CtsXmlFields.CTSXML_SYNONYM_ELE).trim())
						.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);
				}
			}

			// refresh info for external ids for compound
			for (Entry<String, IdObject> id : ids.entrySet()) {
				// insert external_id
				query.append("select insert_external_id(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().name).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().url).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().value).trim())
					.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);
			}
		}

		return query.toString();
	}

	public String getUpdateQuery() {
		StringBuilder query = new StringBuilder();

		if (!this.inchiKey.isEmpty()) {
			// updates compound info using function insert_compound
			// Store procedure parameters:
			// (in inchikey varchar, in inchicode text, in molweight float8, in exactmass float8, in formulac text, in source text)
			query.append("select update_compound(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
				.append("', E'").append(Encoder.encodePostgre(this.inchiCode).trim())
				.append("', ").append(this.molWeight)
				.append(", ").append(this.exactMass)
				.append(", E'").append(Encoder.encodePostgre(this.getFormula()).trim())
				.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);

			// updates each synonym for compound
			// Store procedure parameters:
			// (in inchikey varchar, in namec text, in typec varchar, in source text)
			for (Map.Entry<String, Collection<NameObject>> syn : this.names.entrySet()) {
				for (NameObject n : syn.getValue()) {
					query.append("select update_synonym(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
						.append("', E'").append(Encoder.encodePostgre(n.value).trim())
						.append("', E'").append(Encoder.encodePostgre(n.type).trim())
						.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);
				}
			}

			// updates each external-id for compound
			// Store procedure parameters:
			// (in inchikey varchar, in namec text, in urlc varchar, in valuec varchar, in source text)
			for (Map.Entry<String, IdObject> id : ids.entrySet()) {
				// insert id
				query.append("select update_external_id(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().name).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().url).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().value).trim())
					.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);
			}
		}

		return query.toString();
	}

	public String getDeleteQuery() {
		StringBuilder query = new StringBuilder();

		if(!this.inchiKey.isEmpty()) {

			//delete synonyms
			query.append("DELETE FROM synonym WHERE inchi_key = '").append(this.inchiKey).append("'").append(Encoder.newline);
			query.append("DELETE FROM partial_inchi WHERE inchi_key = '").append(this.inchiKey).append("'").append(Encoder.newline);
			query.append("DELETE FROM external_id WHERE inchi_key = '").append(this.inchiKey).append("'").append(Encoder.newline);
			query.append("DELETE FROM compound WHERE id = '").append(this.inchiKey).append("'").append(Encoder.newline);
		}

		return query.toString();
	}

}
