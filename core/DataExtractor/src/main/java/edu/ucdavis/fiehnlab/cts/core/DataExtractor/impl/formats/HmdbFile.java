package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceNotSupportedException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.FileResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Compound;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.IdObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.iterator.IteratingSDFReader;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;

public class HmdbFile extends AbstractResourceImpl {
	CollectionResource<Compound> data = null;
	File outFile = null;
//	private final Logger logger = Logger.getLogger(getClass());

	public HmdbFile(String name, File outFile) {
		super();
		this.setName(name);
		this.outFile = outFile;
	}

	public Resource internalProcessing(Resource resource) throws ResourceNotSupportedException {
		int count = 0;
		int saved = 0;

		// parse file to get data
		if (resource instanceof InputStreamResource) {
			data = new CollectionResource<Compound>(new ArrayList<Compound>());

			try {
				if(outFile.exists()) {
					outFile.delete();
				}
				FileWriter fw = new FileWriter(outFile);

				IteratingSDFReader reader = new IteratingSDFReader((InputStream) resource.getRepresentation(), DefaultChemObjectBuilder.getInstance(), true);
				boolean go = reader.hasNext();

				while (go) {
					count++;
					IAtomContainer mol = (IAtomContainer) reader.next();
					Compound comp = new Compound();

					if(logger.isDebugEnabled()) {
						logger.debug("mol: " + mol.getID());
					}

					String hmdbId;
					try {
						hmdbId = mol.getProperty(CompoundFields.SDF_HMDB_ID).toString().trim();
					} catch (NullPointerException e) {
						logger.error("No HMDB id found for molecule block: " + count + "\n" + mol.toString());
						continue;
					}

					try {
						comp.setInchiCode(mol.getProperty("INCHI_IDENTIFIER").toString().trim());

						comp.setInchiKey(mol.getProperty("INCHI_KEY").toString().trim().replace("InChIKey=", ""));
					} catch (NullPointerException e) {
						logger.error("Inchi Code or inchikey undefined for compound " + hmdbId);
						continue;
					}

					try {
						comp.setMolWeight(Double.parseDouble(mol.getProperty("MOLECULAR_WEIGHT").toString().trim()));
					} catch (NumberFormatException e) {
						logger.error("Can't calculate molecular weight for compound " + hmdbId + ". (setting to -1.0)");
						comp.setMolWeight(-1.0);
					}

					try {
						comp.setExactMass(Double.parseDouble(mol.getProperty("EXACT_MASS").toString().trim()));
					} catch (NumberFormatException e) {
						logger.error("Can't calculate exact mass for compound " + hmdbId + ". (setting to -1.0)");
						comp.setExactMass(-1.0);
					}

					comp.setFormula(mol.getProperty("CHEMICAL_FORMULA").toString().trim());

					comp.setSourceFile(this.getName());

					IdObject idObj = new IdObject(CompoundFields.CTS_HMDB_ID, hmdbId, CompoundFields.HMDB_METABOLITE_URL + "/" + hmdbId);

					comp.addId(idObj.name, idObj);

					try {
						comp.addName(new NameObject(CtsSDFFields.CTSSDF_IUPAC_NAME, mol.getProperty("IUPAC_NAME").toString().trim()));
						comp.addName(new NameObject(CtsSDFFields.CTSSDF_IUPAC_TRADITIONAL_NAME, mol.getProperty("TRADITIONAL_IUPAC_NAME").toString().trim()));
					} catch (NullPointerException ex) {
						try {
							comp.addName(new NameObject(CtsSDFFields.CTSSDF_IUPAC_NAME, mol.getProperty("GENERIC_NAME").toString().trim()));
						} catch (NullPointerException ex2) {
							logger.error("compound (" + hmdbId + ") null name");
						}
					}

					try {
						String synstr = mol.getProperty("SYNONYMS").toString();
						String[] synonyms;
						if (synstr.contains(";")) {
							synonyms = synstr.split(";");
						} else {
							synonyms = new String[]{synstr};
						}
						for (String synon : synonyms) {
							NameObject name = new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, synon.trim());
							comp.addName(name);
						}
					} catch (NullPointerException ex) {
						logger.error("compound " + hmdbId + " (" + comp.getInchiKey() + ") null synonyms");
					}

					fw.write(comp.getQuery() + "\n");
//					data.add(comp);
					saved++;
					mol = null;
					comp = null;
					go = reader.hasNext();
					logger.info("checked: " + count + " -- saved: " + saved + " (" + hmdbId + ") -- next? " + go);
				}

				fw.close();
			} catch (Exception e) {
				logger.error("Found a bad error: ", e);
			}
		} else {
			logger.error("resource not supported");
			throw new ResourceNotSupportedException();
		}
		logger.info("Processed " + count + " compounds");
		logger.info("Saved " + saved + " compounds");
		return data;
	}

	@Override
	public boolean isResourceTypeAccepted(Resource resource) {
		if (resource instanceof FileResource) {
			return true;
		}
		if (resource instanceof InputStreamResource) {
			return true;
		}

		return false;
	}

}
