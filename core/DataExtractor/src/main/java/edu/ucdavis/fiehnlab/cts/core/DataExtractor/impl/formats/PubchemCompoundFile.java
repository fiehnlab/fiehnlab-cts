package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceNotSupportedException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.FileResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Compound;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.IdObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import org.apache.log4j.Logger;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.formula.IsotopeContainer;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.iterator.IteratingSDFReader;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

public class PubchemCompoundFile extends AbstractResourceImpl {
	CollectionResource<Compound> data = null;
	private final Logger logger = Logger.getLogger(getClass());

	public PubchemCompoundFile(String name) {
		super();
		this.setName(name);
	}

	public Resource internalProcessing(Resource resource) throws ResourceNotSupportedException {
		logger.debug("Resource: " + this.getName());

		// parse file to get data
		if (resource instanceof InputStreamResource) {
			data = new CollectionResource<>(new ArrayList<Compound>());

			try {
				IteratingSDFReader reader = new IteratingSDFReader((InputStream) resource.getRepresentation(), DefaultChemObjectBuilder.getInstance());
				while (reader.hasNext()) {
					IAtomContainer mol = reader.next();
					AtomContainerManipulator.convertImplicitToExplicitHydrogens(mol);
					AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);

					Compound comp = new Compound();

					comp.setInchiCode(mol.getProperty(CompoundFields.SDF_INCHI_CODE).toString().trim());

					comp.setInchiKey(mol.getProperty(CompoundFields.SDF_INCHI_KEY).toString().trim());

					comp.setMolWeight(AtomContainerManipulator.getNaturalExactMass(mol));
					if(comp.getMolWeight().isNaN()) {
						logger.error("Can't calculate molecular weight for compound " + comp.getInchiKey() + ". (setting to -1.0)");
						comp.setMolWeight(-1.0);
					}

					Double exMass = -1.0;
					try {
						exMass = new IsotopeContainer(MolecularFormulaManipulator.getMolecularFormula(mol), 1).getMass();
//						 exMass = AtomContainerManipulator.getTotalExactMass(mol);  --> NullPointerException
						comp.setExactMass(exMass);
					} catch (Exception e) {
						logger.error("Error: " + e.getMessage());
					}

					comp.setFormula(MolecularFormulaManipulator.getString(MolecularFormulaManipulator.getMolecularFormula(mol)));

					comp.setSourceFile(this.getName());

					comp.setSmiles(mol.getProperty(CompoundFields.SDF_CAN_SMILES).toString().trim());

					IdObject idObj = new IdObject(CompoundFields.PUBCHEM_CID, mol.getProperty(CompoundFields.SDF_CID).toString().trim(), CompoundFields.PUBCHEM_BASE_URL);

					comp.addId(idObj.name, idObj);

					Map<Object, Object> props = mol.getProperties();

					for (Entry<?, ?> e : props.entrySet()) {
						String key_name = e.getKey().toString();

						if (key_name.matches(CompoundFields.SDF_IUPAC_NAME_REGEX)) {

							if ("PUBCHEM_IUPAC_OPENEYE_NAME".equals(key_name)) {
								key_name = CtsSDFFields.CTSSDF_IUPAC_OPENEYE_NAME;
							} else if ("PUBCHEM_IUPAC_CAS_NAME".equals(key_name)) {
								key_name = CtsSDFFields.CTSSDF_IUPAC_CAS_NAME;
							} else if ("PUBCHEM_IUPAC_NAME".equals(key_name)) {
								key_name = CtsSDFFields.CTSSDF_IUPAC_NAME;
							} else if ("PUBCHEM_IUPAC_SYSTEMATIC_NAME".equals(key_name)) {
								key_name = CtsSDFFields.CTSSDF_IUPAC_SYSTEMATIC_NAME;
							} else if ("PUBCHEM_IUPAC_TRADITIONAL_NAME".equals(key_name)) {
								key_name = CtsSDFFields.CTSSDF_IUPAC_TRADITIONAL_NAME;
							} else {
								key_name = "IUPAC Name";
							}

							NameObject name = new NameObject(key_name, e.getValue().toString().trim());

							comp.addName(name);
						}
						if (e.getKey().toString().startsWith(CompoundFields.SYNONYMS)) {
							NameObject name = new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, e.getValue().toString().trim());

							comp.addName(name);
						}
					}
					data.add(comp);
				}

				reader.close();
			} catch (Exception e) {
				logger.error(String.format("Found a bad error: %s", e.getMessage()), e);
			}

		} else {
			throw new ResourceNotSupportedException();
		}

		return data;
	}

	@Override
	public boolean isResourceTypeAccepted(Resource resource) {
		if (resource instanceof FileResource) {
			return true;
		}
		if (resource instanceof InputStreamResource) {
			return true;
		}

		return false;
	}

}
