package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

public class CtsSDFFields {
	public static final String CTSSDF_IUPAC_OPENEYE_NAME = "IUPAC Name (Allowed)";
	public static final String CTSSDF_IUPAC_CAS_NAME = "IUPAC Name (CAS-like Style)";
	public static final String CTSSDF_IUPAC_NAME = "IUPAC Name (Preferred)";
	public static final String CTSSDF_IUPAC_SYSTEMATIC_NAME = "IUPAC Name (Systematic)";
	public static final String CTSSDF_IUPAC_TRADITIONAL_NAME = "IUPAC Name (Traditional)";
}
