package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceNotSupportedException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.MyIteratingMDLReader;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

import java.io.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/25/13
 * Time: 11:01 AM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated
public class TextNameExport extends AbstractResourceImpl {
	static Integer nameCount = 0;
	static Integer inchiCount = 0;

	/**
	 * Processes a Pubchem Substance sdf file and extracts a collection of Substances
	 */
	@Override
	protected Resource internalProcessing(Resource resource) throws ResourceNotSupportedException {
		Set<String> badSIDS = new HashSet<String>();
		badSIDS.add("141382403");
		CollectionResource<String> data = new CollectionResource<String>(new Vector<String>());
		String separator = "\t";

		if (resource instanceof InputStreamResource) {
			MyIteratingMDLReader sdfReader = null;
			IAtomContainer mol;

			try {
				// read the sdf file
				sdfReader = new MyIteratingMDLReader((InputStream) resource.getRepresentation(), DefaultChemObjectBuilder.getInstance());

				// step through each substance section in the file
				while (sdfReader.hasNext()) {
					mol = sdfReader.next(); // get the current molecule
					logger.trace("\nFound a molecule: " + mol);

					String sid = mol.getProperty(SubstanceFields.SID_PROPERTY).toString(); // get current molecule's PubChem SID;

					// highly symetrycal molecules run forever... so skip them
					if (badSIDS.contains(sid)) {
						continue;
					}

					String inchiKey = "";
					Set<String> molnames = new HashSet<String>();

					// start gathering properties
					try {
						logger.debug("\tGetting data for SID " + sid);

						//adding explicit hydrogens
						CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
						AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);
						adder.addImplicitHydrogens(mol);

						// get basic compound info.
						InChIGenerator inchigen = InChIGeneratorFactory.getInstance().getInChIGenerator(mol);

						inchiKey = inchigen.getInchiKey();  // generate the inchiKey based on mol structure

					} catch (CDKException ex) {
						// error generating inchiKey
						logger.error("Error generating InChI Key" + ex.getMessage() + " -- SID:" + mol.getProperty(SubstanceFields.SID_PROPERTY));
						continue;
					} catch (IllegalArgumentException ex) {
						logger.error("Found an invalid molecule block (SID:" + sid + ") in the file. " + ex.getMessage());
						continue;
					} catch (Exception ex) {
						logger.error("Something bad happened. " + ex.getMessage(), ex);
					}

					// get molecule name if any
					for (String value : getMultiData(mol, SubstanceFields.NAME_PROPERTY)) {
						logger.trace("\t\tadding name: " + value);
						molnames.add(value);
					}

					//get molecule synonyms if any and consolidate with names
					for (String value : getMultiData(mol, SubstanceFields.SYNONYM_PROPERTY)) {
						logger.trace("\t\tadding synonym: " + value);
						molnames.add(value);
					}

					for (String name : molnames) {
						logger.debug("inchikey: " + inchiKey + "\t\tname: " + name);
						data.add(inchiKey + separator + name);

						nameCount++;
					}

					inchiCount++;
				}
				sdfReader.close();

			} catch (IOException e) {
				logger.error("Error closing the data stream." + e.getMessage(), e);
			} finally {
				try {
					sdfReader.close();
				} catch (Exception e) {
				}
			}

		} else {
			throw new ResourceNotSupportedException();
		}

		return data;
	}

	/**
	 * Returns a set of values contained in the specified property for the current molecule.
	 * If the property dosn't have any values or is not found in the molecule the resulting set is empty
	 *
	 * @param mol      Molecule being parsed
	 * @param property Property to get values from
	 * @return A set containing the values of the selected property, or an empty set.
	 */
	private Set<String> getMultiData(IAtomContainer mol, String property) {
		Set<String> items = new HashSet<String>();

		logger.trace("\tGetting data for property: " + property);

		if (mol.getProperty(property) != null) {
			for (String n : mol.getProperty(property).toString().split("\n")) {
				if ((property.equals(SubstanceFields.NAME_PROPERTY) || property.equals(SubstanceFields.SYNONYM_PROPERTY)) && n.contains(";")) {
					logger.trace("\t\t[getMultiData] skipping '" + n + "'");
					continue;
				}
				logger.trace("\t\t[getMultiData] adding '" + n + "' to property data");
				items.add(n);
			}
		}

		return items;
	}

	public static void main(String[] args) {
		File output = new File("NameList.txt");
		int fileCount = 0;

		FileWriter writer = null;
		try {
			writer = new FileWriter(output);

			if (args.length != 1) {
				System.out.println("Usage: TextNameExtractor <input_directory>");
				System.exit(1);
			}

			File inputDir = new File(args[0]);

			if (inputDir.exists() && inputDir.isDirectory()) {
				Resource pcProcessor = null;
				CollectionResource<String> res = null;

				File[] fileList = inputDir.listFiles();
				for (File pcFile : fileList) {
					if (pcFile.getName().endsWith("sdf.gz") == false) {
						continue;
					}

					pcProcessor = new TextNameExport();

					GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcFile));
					InputStreamResource pcxml = new CompressedResource(gzfile);

					res = (CollectionResource<String>) pcProcessor.process(pcxml);

					Iterator it = res.iterator();
					while (it.hasNext()) {
						String line = it.next().toString().trim();
						if (line != "") {
							writer.append(line + "\n");
						}
					}
					writer.flush();
					fileCount++;
				}
			} else

			{
				System.out.println("Invalid input directory.");
			}

			writer.close();
		} catch (IOException e) {
			System.out.println("error reading or writing to file...\n" + e.getMessage());
			e.printStackTrace();
		}

		System.out.println("Processed " + TextNameExport.nameCount + " names in " + TextNameExport.inchiCount + " inchiKeys in " + fileCount + " files");
	}
}
