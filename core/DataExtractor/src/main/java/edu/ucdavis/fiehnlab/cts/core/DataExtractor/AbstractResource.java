package edu.ucdavis.fiehnlab.cts.core.DataExtractor;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceNotSupportedException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceNotValidException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.FileResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.xml.JDomResource;
import org.apache.log4j.Logger;
import org.jdom.Element;

import java.io.File;
import java.io.InputStream;

/**
 * an abstract resource to simplify the work with resources a bit
 *
 * @author wohlgemuth
 */
public abstract class AbstractResource implements Resource {
	String name = null;

	/**
	 * used for logging statements
	 */
	protected Logger logger = Logger.getLogger(getClass());

	private Resource processedWith = null;

	public void setProcessedWith(Resource previousStep) {
		this.processedWith = previousStep;
	}

	/**
	 * previous processing step
	 *
	 * @return
	 */
	public final Resource getProcessedWith() {
		return processedWith;
	}

	/**
	 * has this resource been processed with something else
	 *
	 * @return
	 */
	public final boolean isProcessed() {
		return this.processedWith != null;
	}

	@Override
	public final Resource process(InputStream input) {
		return process(new InputStreamResource(input));
	}

	@Override
	public final Resource process(Element element) {
		return process(new JDomResource(element));
	}

	@Override
	public final Resource process(File file) {
		return process(new FileResource(file));
	}

	@Override
	public final Resource process(Resource resource) {
		if (resource == null) {
			throw new ResourceNotValidException(
					"sorry the given resource is null!");
		}
		if (!resource.isValid()) {
			throw new ResourceNotValidException(
					"sorry the given resource " + resource.getClass() + " is not valid");
		}

		boolean isAccepted = isResourceTypeAccepted(resource);

		if (logger.isDebugEnabled()) {
			logger.debug("checking if giving ressource is supported: "
					+ resource.getClass().getName() + " result is: " + isAccepted);
		}

		if (isAccepted) {
			resource.setProcessedWith(this);
			return internalProcessing(resource);
		}
		throw new ResourceNotSupportedException("sorry this resource type, can't be applied to this resource");
	}

	@Override
	public final Resource process() {
		if (!isValid()) {
			throw new ResourceNotValidException("sorry something is not valid with me");
		}
		// it's processed with it self
		this.processedWith = this;
		return internalProcessing(null);
	}

	/**
	 * does some internal processing, the resource can be null
	 *
	 * @return
	 */
	protected abstract Resource internalProcessing(Resource resource);

//    public abstract String getName();
//    public abstract void setName(String name);

	public boolean isValid() {
		return true;
	}

	@Override
	public Object getRepresentation() {
		return this.toString();
	}

	@Override
	public boolean isResourceTypeAccepted(Resource resource) {
		return true;
	}
}
