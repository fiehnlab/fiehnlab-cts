package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl;

/**
 * simple interface to provide us with sql
 * @author wohlgemuth
 *
 */
public interface SQLGenerator {

	/**
	 * generated query
	 * @return
	 */
	String getQuery() throws NullPointerException;
	String getUpdateQuery() throws NullPointerException;
	String getDeleteQuery() throws NullPointerException;
}
