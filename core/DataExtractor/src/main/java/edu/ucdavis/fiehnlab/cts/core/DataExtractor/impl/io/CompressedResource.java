package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;


/**
 * convinience class to get a gzip compressed input stream
 * 
 * @author dpedrosa
 * 
 */
public class CompressedResource extends InputStreamResource {
	private GZIPInputStream stream;

    public CompressedResource(InputStream file) throws FileNotFoundException, IOException {
		if (file instanceof GZIPInputStream) {
			this.stream = (GZIPInputStream) file;
		} else {
			this.stream = new GZIPInputStream(file);
		}
	}

	public CompressedResource(File file) throws FileNotFoundException, IOException {
		this(new FileInputStream(file));
	}

	/**
	 * return the internal stream as representation
	 */
	public GZIPInputStream getRepresentation() {
		return stream;
	}

}
