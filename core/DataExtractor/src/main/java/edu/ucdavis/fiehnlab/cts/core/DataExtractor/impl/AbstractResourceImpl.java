package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.AbstractResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;

/**
 * very basic abstract resource impl
 * 
 * @author wohlgemuth
 * 
 */
public abstract class AbstractResourceImpl extends AbstractResource implements SQLGenerator {
    String name;
//	public Logger logger = Logger.getLogger(getClass());

	@Override
	protected Resource internalProcessing(Resource resource) {
		if (resource == null) {
			return this;
		} else {
			return resource;
		}
	}

	public String getQuery() throws NullPointerException {
		return null;
	}

	public String getUpdateQuery() throws NullPointerException {
		return null;
	}

	public String getDeleteQuery() throws NullPointerException {
		return null;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
	    logger.debug("setting name: " + name);
	    if(name.contains("/")) {
		    name = name.substring(name.lastIndexOf("/")+1, name.length());
	    }
        this.name = name;
    }
}
