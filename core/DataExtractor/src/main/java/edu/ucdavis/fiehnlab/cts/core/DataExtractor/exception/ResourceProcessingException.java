package edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception;

public class ResourceProcessingException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResourceProcessingException() {
	}

	public ResourceProcessingException(String arg0) {
		super(arg0);
	}

	public ResourceProcessingException(Throwable arg0) {
		super(arg0);
	}

	public ResourceProcessingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
