package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Level;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceNotSupportedException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.MyIteratingMDLReader;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Compound;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.IdObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.CommonChemDBs;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.PatternHelper;

public class GenericSDFFile extends AbstractResourceImpl {

	CollectionResource<Substance> data = null;

	/**
	 * empty constructor
	 */
	public GenericSDFFile() {
		super();
	}

	/**
	 * Processes a Pubchem Substance sdf file and extracts a collection of
	 * Substances
	 */
	@Override
	protected Resource internalProcessing(Resource resource) throws ResourceNotSupportedException {
		CollectionResource<Compound> data = null;

		if (resource instanceof InputStreamResource) {
			MyIteratingMDLReader sdfReader = null;
			IAtomContainer mol = null;

			try {
				data = new CollectionResource<Compound>(
						new ArrayList<Compound>());

				// read the sdf file
				sdfReader = new MyIteratingMDLReader(
						(InputStream) resource.getRepresentation(),
						DefaultChemObjectBuilder.getInstance());

				// step through each substance section
				while (sdfReader.hasNext()) {
					mol = sdfReader.next(); // get the current molecule
					logger.trace("Found a molecule: " + mol + "\n");

					String inchiKey = "";
					String inchiCode = "";
					Double molWeight = 0.0;

					// start gathering properties
					try {

						// load the inchiGenerator for the current molecule
						InChIGenerator inchigen = InChIGeneratorFactory
								.getInstance().getInChIGenerator(mol);
						inchiKey = inchigen.getInchiKey(); // generate the
						// inchiKey based on
						// mol structure
						inchiCode = inchigen.getInchi();
						molWeight = AtomContainerManipulator
								.getTotalExactMass(mol);

					} catch (CDKException ex) {
						// error generating inchiKey
						logger.error("Error generating InChI Key"
								+ ex.getMessage() + "\n SID:"
								+ mol.getProperty(SubstanceFields.SID_PROPERTY));
						continue;
					} catch (IllegalArgumentException ex) {
						logger.error("Found an invalid molecule block in the file.");
						continue;
					} catch (Exception ex) {
						logger.error("Something bad happened.", ex);
					}

					// assign data to the substance
					Compound subst = new Compound();
					subst.setInchiKey(inchiKey);
					subst.setInchiCode(inchiCode);
					subst.setMolWeight(molWeight);
					data.add(subst);
				}

			} finally {
				try {
					sdfReader.close();
				} catch (Exception e) {
				}
			}

		} else {
			throw new ResourceNotSupportedException();
		}
		if (logger.isDebugEnabled()) {
			logger.debug(data.size() + " substances found.");
		}
		return data;
	}

}
