package edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding;

/**
 * @author Diego
 *
 */
public final class Encoder {
	/**
	 * Makes easy to use platform independent newline caracter.
	 */
	public final static String newline = "\n";
//	public final static String newline = System.getProperty("line.separator");

	/**
	 * Helper function to encode values to send to Postgre
	 */
	public final static String encodePostgre(String unsafeStr) {
		String safeStr;

		safeStr = unsafeStr.replace("\\", "\\\\");
		safeStr = safeStr.replace("\'", "\\'");
		return safeStr;
	}

}
