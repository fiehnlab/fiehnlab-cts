/**
 *
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor.model;

/**
 * @author dpedrosa
 */
public class NameObject implements Comparable<NameObject> {
	public String type;
	public String value;

	public NameObject() {
		this("", "");
	}

	public NameObject(String type, String value) {
		this.type = type;
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	@Override
	public boolean equals(Object x) {
		if (x.getClass().equals(this.getClass())) {
			try {
				NameObject i = (NameObject) x;

				return (i.type.equals(this.type) && i.value.equals(this.value));
			} catch (ClassCastException e) {
				//not required class just continues to false
			}
		}
		return false;
	}

	@Override
	public int compareTo(NameObject other) {
		return this.value.compareTo(other.value);
	}

	public int hashCode() {
		return this.toString().hashCode();
	}
}

