package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.sql;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.xml.JDomResource;

import java.util.HashSet;

public class SQLResource extends AbstractResourceImpl {

	@SuppressWarnings("unused")
	private CollectionResource<String> queries = null;	
	
	public SQLResource() {
		super();
		this.queries = new CollectionResource<String>(new HashSet<String>());
	}
	
	public Resource internalProcess(Resource resource) {
		return null;
	}
	
	@Override
	public boolean isResourceTypeAccepted(Resource resource) {
		return resource instanceof JDomResource;
	}

}
