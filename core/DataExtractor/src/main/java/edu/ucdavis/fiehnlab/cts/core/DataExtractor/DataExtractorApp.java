package edu.ucdavis.fiehnlab.cts.core.DataExtractor;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.HmdbFile;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.LipidsSDFFile;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.PubchemCompoundFile;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.PubchemSubstanceFile;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.InputMismatchException;
import java.util.zip.GZIPInputStream;

/**
 * DataExtractor processes a resource and outputs a translated version of a resource
 */
public class DataExtractorApp {

	private String inDir = "";
	private String outDir = "";
	private String resource = "";
	private final static String newline = System.getProperty("line.separator");
	final Logger logger = Logger.getLogger(getClass());
	Configuration config;

	/**
	 * Processes one gzipped pubchem file Call using: java -jar DataExtractor -r <compound | substance> <infile>
	 * <outdir>
	 *
	 * @param args should be 4 arguments -r <compound | substance> specifies the typr of files to parse; <infile> is the
	 *            gzipped pubchem file to process; <outdir> is the directory where the single processed xml files are to
	 *            be stored.
	 */
	public static void main(String[] args) {
		DataExtractorApp app = new DataExtractorApp();

		try {
			app.config = new PropertiesConfiguration("config.properties");
			app.logger.debug("Config loaded.");

			if (app.hasValidArgs(args)) {
				app.logger.info("processing " + app.getInFile() + " to " + app.getOutDir() + " ...");

				String result = app.processPubchemFile(new File(args[2]), args[3]);

				app.logger.info(result);
			} else {
				app.printUsage();
			}
		} catch (FileNotFoundException ex) {
			app.logger.error(ex);
			app.printUsage();
		} catch (InputMismatchException ex) {
			app.logger.error(ex);
			app.printUsage();
		} catch(ConfigurationException ce) {
			app.logger.error("Can't load config file");
			System.exit(10);
		}
	}

	/**
	 * checks for arguments and processes the specified file.
	 *
	 * @param args commandline arguments to process.
	 * @return returns true if the arguments are valid, otherwise returns false.
	 * @throws FileNotFoundException
	 */
	protected final Boolean hasValidArgs(String[] args) throws FileNotFoundException, InputMismatchException {

		if (args == null || args.length < 4) { // less arguments than expected
			throw new InputMismatchException("Wrong number of arguments.");
		} else if (!args[0].equals("-r")) { // first argument is not -r
			throw new InputMismatchException("Wrong option: " + args[0]);
		} else if (!args[1].equals("compound") && !args[1].equals("substance") && !args[1].equals("lipid") && !args[1].equals("hmdb")) {
			throw new InputMismatchException("Invalid resource type: [" + args[1] + "]\nCan only be \"compound\", \"substance\", \"hmdb\",  or \"lipid\"");
		} else {
			File fp = new File(args[2]);
			if (!fp.exists()) {
				throw new FileNotFoundException("Could not find file: " + args[2]);
			} else {
				this.resource = args[1];
				this.inDir = args[2];
				this.outDir = args[3];
			}
		}
		return true;
	}

	private void printUsage() {
		String text = "\nUsage: java -jar DataExtractorApp.jar -r <compound | substance | lipid | hmdb> <in file> <out dir>\n" + "<in file> is the file to be processed.\n" + "<out dir> is the directory where the processed files will be saved.\n" + "Options:\n\t -r specifies if the input directory contains compound files or substance files.\n\t" + "Can be either compound, substance, hmdb or lipid.\n";

		System.out.println(text);
	}

	@SuppressWarnings("unchecked")
	protected final String processPubchemFile(File pcFile, String outputDir) {
		Resource pcProcessor = null;
		CollectionResource<? extends Resource> res = null;

		logger.debug("Processing: " + pcFile.getName());

		if (getResource().equals("compound")) {
			pcProcessor = new PubchemCompoundFile(pcFile.getName());
		} else if (getResource().equals("substance")) {
			pcProcessor = new PubchemSubstanceFile(pcFile.getName(), config);
		} else if (getResource().equals("lipid")) {
			pcProcessor = new LipidsSDFFile(pcFile.getName());
		} else if (getResource().equals("hmdb")) {
			pcProcessor = new HmdbFile(pcFile.getName(), new File(outputDir + File.separatorChar + pcFile.getName().substring(0, pcFile.getName().indexOf(".")) + ".sql"));
		} else {
			logger.error("Missing resource type value");
			return null;
		}


		try {

//			GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcFile));
			InputStreamResource pcxml = new CompressedResource(new GZIPInputStream(new FileInputStream(pcFile)));

			res = (CollectionResource<? extends Resource>) pcProcessor.process(pcxml);

			/*if(getResource().equals("compound")) {
				res = (CollectionResource<Compound>)pcProcessor.process(pcxml);
			} else {
				res = (CollectionResource<Substance>)pcProcessor.process(pcxml);
			}*/
			if(!getResource().equals("hmdb")) {
				logger.debug("writing sql file " + outputDir + pcFile.getName());
				generateSqlScript(res, pcFile, outputDir);
			}

		} catch(FileNotFoundException e) {
			logger.error("Can't find the file: " + pcFile.getAbsolutePath() + "\n" + e);
		} catch(IOException e) {
			logger.error("Something bad happened trying to read the file: " + pcFile.getName() + "\n" + e);
		}

		return "processed " + res.size() + " resources.";
	}

	/**
	 * @return the inFile
	 */
	public String getInFile() {
		return inDir;
	}

	/**
	 * @param inFile the inFile to set
	 */
	@SuppressWarnings("unused")
	private void setInFile(String inFile) {
		this.inDir = inFile;
	}

	/**
	 * @return the outDir
	 */
	public String getOutDir() {
		return outDir;
	}

	/**
	 * @param outDir the outDir to set
	 */
	@SuppressWarnings("unused")
	private void setOutDir(String outDir) {
		this.outDir = outDir;
	}

	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}

	private void generateSqlScript(CollectionResource<? extends Resource> res, File pcfile, String dest) throws FileNotFoundException, IOException, NullPointerException {
		final int TRANSACTION_BLOCK = 100;

		File sqlDir = new File(dest);
		if(!sqlDir.isDirectory()) {
			sqlDir.mkdirs();
		}
		File name = new File(sqlDir.getPath() + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
		if (name.exists()) {
			name.delete();
		}

		FileWriter sqlFile = new FileWriter(name);
		BufferedWriter fw = new BufferedWriter(sqlFile);

		fw.write("BEGIN;" + newline);
		fw.flush();

		int count = 0;

		for (Resource c : res) {
			if(TRANSACTION_BLOCK == count++) {
				logger.trace("new transaction block");
				fw.append("COMMIT;" + Encoder.newline + Encoder.newline);
				fw.write("BEGIN;" + Encoder.newline);
				fw.flush();
				count = 0;
			}

			fw.append(c.getQuery());
			fw.flush();
		}

		fw.append("COMMIT;" + newline);
		fw.flush();
		fw.close();
	}
}
