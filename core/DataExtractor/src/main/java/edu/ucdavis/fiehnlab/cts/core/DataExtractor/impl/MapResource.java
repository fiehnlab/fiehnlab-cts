package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * provides us with a map resource, which works like a normal map
 * 
 * @author wohlgemuth
 * 
 * @param <K>
 * @param <V>
 */
public class MapResource<K, V> extends AbstractResourceImpl implements
		Map<K, V> {

	private Map<K, V> map;

	@Override
	public Map<K, V> getRepresentation() {
		return this;
	}

	public void clear() {
		map.clear();
	}

	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	public Set<Entry<K, V>> entrySet() {
		return map.entrySet();
	}

	public boolean equals(Object o) {
		return map.equals(o);
	}

	public V get(Object key) {
		return map.get(key);
	}

	public int hashCode() {
		return map.hashCode();
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	public Set<K> keySet() {
		return map.keySet();
	}

	public V put(K key, V value) {
		return map.put(key, value);
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		map.putAll(m);
	}

	public V remove(Object key) {
		return map.remove(key);
	}

	public int size() {
		return map.size();
	}

	public Collection<V> values() {
		return map.values();
	}
}
