package edu.ucdavis.fiehnlab.cts.core.DataExtractor.model;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.CompoundFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.CtsXmlFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.PatternHelper;
import org.jdom.Document;
import org.jdom.Element;

import java.util.*;
import java.util.Map.Entry;

/**
 * Representation of a Compound in the CTS
 *
 * notes: InChI code can be converted to InChIKey, not the other way around.
 *
 * @author Diego
 */
public class Compound extends AbstractResourceImpl {

	private String inchiCode = "";
	private String inchiKey = "";
	private Double molWeight = 0.0;
	private Double exactMass = 0.0;
	private String sourceFile = "";
	private String formula = "";
	private String smiles = "";

	/**
	 * List of ids for the compound
	 * k = name of id
	 * v = value of id
	 * u = url
	 */
	private Map<String, IdObject> ids = new HashMap<String, IdObject>();

	/**
	 * List of names for the compound
	 * k = key is the type (IUPAC,Synonym)
	 * v = list of names for this type
	 */
	private Map<String, Collection<NameObject>> names = new HashMap<String, Collection<NameObject>>();

	/**
	 * Creates a new empty Compound.
	 */
	public Compound() {
		this("");
	}

	/**
	 * Creates a new Compound with an InChI code set.
	 *
	 * @param inchiCode code with which to generate the inchi key
	 */
	public Compound(String inchiCode) {
		this.inchiCode = inchiCode;
	}

	/**
	 *
	 * @return Returns a String with the InChIKey hash
	 */
	public String getInchiKey() {
		return this.inchiKey;
	}

	/**
	 * Sets the inchi key hash to the compound
	 *
	 * @param inchiKey A String value with the inchi key hash
	 */
	public void setInchiKey(String inchiKey) {
		this.inchiKey = inchiKey;
	}

	/**
	 *
	 * @return the inchi code string
	 */
	public String getInchiCode() {
		return this.inchiCode;
	}

	public void setInchiCode(String inchiCode) {
		this.inchiCode = inchiCode;
	}

	public Double getMolWeight() {
		return this.molWeight;
	}

	public void setMolWeight(Double weight) {
		this.molWeight = weight;
	}

	public void setNames(Map<String, Collection<NameObject>> names) {
		this.names = names;
	}

	public void addName(NameObject name) {
		if (names.containsKey(name.type) == false) {
			names.put(name.type, new HashSet<NameObject>());
		}
		names.get(name.type).add(name);
	}

	public void addId(String key, IdObject id) {
		this.ids.put(key, id);
	}

	public void setIds(Map<String, IdObject> ids) {
		this.ids = ids;
	}

	public Map<String, IdObject> getIds() {
		return this.ids;
	}

	public Collection<IdObject> getIdList() {
		return ids.values();
	}

	public Map<String, Collection<NameObject>> getNames() {
		return names;
	}

	public Double getExactMass() {
		return exactMass;
	}

	public void setExactMass(Double exactMass) {
		this.exactMass = exactMass;
	}

	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	public Set<String> getIupacNames() {
		Set<String> iupacNames = new HashSet<String>();
		Map<String, Collection<String>> iupacn = getNames(CompoundFields.IUPAC_NAME);
		for (Entry<String, Collection<String>> e : iupacn.entrySet()) {
			if (e.getKey().contains(CompoundFields.IUPAC_NAME)) {
				iupacNames.addAll(e.getValue());
			}
		}

		return iupacNames;
	}

	public Set<String> getSynonymNames() {
		return (Set<String>) (getNames(CompoundFields.SYNONYMS).get(CompoundFields.SYNONYMS));
	}

	private Map<String, Collection<String>> getNames(String key) {
		Map<String, Collection<String>> nameList = new HashMap<String, Collection<String>>();

		for (String k : names.keySet()) {
			if (k.startsWith(key)) {
				nameList.put(k, new HashSet<String>());
				// turning NameObjects to Strings
				for (NameObject no : names.get(k)) {
					nameList.get(k).add(no.value);
				}
			}
		}

		return nameList;
	}

	public String getSmiles() {
		return this.smiles;
	}

	public void setSmiles(String newSmiles) {
		newSmiles = newSmiles.trim();
		if(smiles.length() <= 5) {
			this.smiles = newSmiles;
		} else {
			if(newSmiles.trim().matches(PatternHelper.SMILES_PATTERN)) {
				System.out.println("Long SMILES.");
				this.smiles = newSmiles.trim();
			} else {
				System.out.println("BAAAAD SMILES.");
				this.smiles = "";
			}

		}
	}

	/**
	 * xml represenation of this compound
	 *
	 * @return xml Document with the compound data
	 */
	public Document toXML() {
		Element compound = new org.jdom.Element(CtsXmlFields.CTSXML_COMPOUND_ELE);
		// adding <Inchi> <Key></Key> <Code></Code> </Inchi>
		compound.addContent(new Element(CtsXmlFields.CTSXML_INCHI_ELE).addContent(new Element(CtsXmlFields.CTSXML_INCHIKEY_ELE).setText(inchiKey)));
		compound.getChild(CtsXmlFields.CTSXML_INCHI_ELE).addContent(new Element(CtsXmlFields.CTSXML_INCHICODE_ELE).setText(inchiCode));

		// adding <IDS></IDS>
		compound.addContent(new Element(CtsXmlFields.CTSXML_IDS_ELE));
		for (Entry<String, IdObject> entry : ids.entrySet()) {
			String idType = "";
			if (entry.getKey() == CompoundFields.PUBCHEM_CID) {
				idType = CtsXmlFields.CTSXML_PUBCHEM_CID;
			} else {
				idType = "unknown";
			}
			Element ele = new Element(CtsXmlFields.CTSXML_ID_ELE).setText(entry.getValue().value).setAttribute("origin", "PubChem").setAttribute("type", idType).setAttribute("url", CompoundFields.PUBCHEM_BASE_URL);

			compound.getChild(CtsXmlFields.CTSXML_IDS_ELE).addContent(ele);
		}

		// adding <Names></Names>
		compound.addContent(new Element(CtsXmlFields.CTSXML_NAMES_ELE));
		for (Entry<String, Collection<NameObject>> entry : names.entrySet()) {
			String label = entry.getKey();
			for (NameObject name : entry.getValue()) {
				compound.getChild(CtsXmlFields.CTSXML_NAMES_ELE).addContent(new Element(CtsXmlFields.CTSXML_NAME_ELE).setAttribute("type", label).setText(name.type));
			}
		}

		return (new Document()).addContent(compound);
	}

	@Override
	/**
	 * Generates the sql query for this compound
	 * @return the sql query to insert/update this compound with names and ids
	 */
	public String getQuery() throws NullPointerException {
		/*
		 * NEW DOMAIN MODEL
		 * select insert_compound('123456789012345678901234567','tada',33.0,32.98,'compound_xxxxx_xxxxx.sdf.gz');
		 * select insert_synonym(nextval('hibernate_sequence'),'123456789012345678901234567','dasdsasa','dadsa','compound_xxxxx_xxxxx.sdf.gz')
		 */
		StringBuilder query = new StringBuilder();
		if (!this.inchiKey.isEmpty()) {
			// insert compound info using function insert_compound
			query.append("select insert_compound(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
				.append("', E'").append(Encoder.encodePostgre(this.inchiCode).trim())
				.append("', ").append(this.molWeight)
				.append(", ").append(this.exactMass)
				.append(", E'").append(Encoder.encodePostgre(this.getFormula()).trim())
				.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim())
				.append("', E'").append(Encoder.encodePostgre(this.smiles).trim())
				.append("');").append(Encoder.newline);

			// refresh info about synonyms for compound
			for (Entry<String, Collection<NameObject>> syn : this.names.entrySet()) {
				for (NameObject n : syn.getValue()) {
					// reinsert synonym
					query.append("select insert_synonym(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
						.append("', E'").append(Encoder.encodePostgre(n.value).trim())
						.append("', E'").append(Encoder.encodePostgre(n.type).trim())
						.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim())
						.append("');").append(Encoder.newline);
				}
			}

			// refresh info for external ids for compound
			for (Entry<String, IdObject> id : ids.entrySet()) {
				// insert id
				query.append("select insert_external_id(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().name).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().url).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().value).trim())
					.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim())
					.append("');").append(Encoder.newline);
			}
		}

		return query.toString();
	}

	@Override
	public String toString() {
		List iupaclist = new ArrayList<>();
		for (String key : getNames().keySet()) {
			List<NameObject> vals = new ArrayList<>(names.get(key));
			Collections.sort(vals);
			iupaclist.add(key + "=" + vals);
			Collections.sort(iupaclist);
		}

		return this.getInchiKey() + Encoder.newline +
				this.getInchiCode() + Encoder.newline +
				this.getFormula() + Encoder.newline +
				this.getMolWeight() + Encoder.newline +
				this.getExactMass() + Encoder.newline +
				this.ids.toString() + Encoder.newline +
				iupaclist.toString();
	}

	@Override
	public boolean equals(Object x) {
		try {
			Compound i = (Compound) x;

			Boolean value = i.inchiCode.equals(this.inchiCode) && i.inchiKey.equals(this.inchiKey) && i.molWeight.equals(this.molWeight);

			for (Entry<?, ?> e : ids.entrySet()) {
				value = value && this.ids.containsKey(e.getKey());
				value = value && this.ids.containsValue((IdObject) e.getValue());
			}

			for (Entry<?, ?> name : names.entrySet()) {
				value = value && this.names.containsKey(name.getKey()) && this.names.containsValue(name.getValue());
			}
			return value;
		} catch (ClassCastException e) {
			// not required class just continues to false
		} catch (NullPointerException e) {
			// not required class just continues to false
		}
		return false;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getUpdateQuery() {
		StringBuilder query = new StringBuilder();

		if (!this.inchiKey.isEmpty()) {
			/* updates compound info using function insert_compound
			 * Store procedure parameters:
			 * (in inchikey varchar, in inchicode text, in molweight float8, in exactmass float8, in formulac text, in source text)
			 */
			query.append("select update_compound(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
				.append("', E'").append(Encoder.encodePostgre(this.inchiCode).trim())
				.append("', ").append(this.molWeight)
				.append(", ").append(this.exactMass)
				.append(", E'").append(Encoder.encodePostgre(this.getFormula()).trim())
				.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim())
				.append("');").append(Encoder.newline);

			// updates each synonym for compound
			// Store procedure parameters:
			// (in inchikey varchar, in namec text, in typec varchar, in source text)
			for (Map.Entry<String, Collection<NameObject>> syn : this.names.entrySet()) {
				for (NameObject n : syn.getValue()) {
					query.append("select update_synonym(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
						.append("', E'").append(Encoder.encodePostgre(n.value).trim())
						.append("', E'").append(Encoder.encodePostgre(n.type).trim())
						.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);
				}
			}

			// updates each external-id for compound
			// Store procedure parameters:
			// (in inchikey varchar, in namec text, in urlc varchar, in valuec varchar, in source text)
			for (Map.Entry<String, IdObject> id : ids.entrySet()) {
				// insert id
				query.append("select update_external_id(E'").append(Encoder.encodePostgre(this.inchiKey).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().name).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().url).trim())
					.append("', E'").append(Encoder.encodePostgre(id.getValue().value).trim())
					.append("', E'").append(Encoder.encodePostgre(this.sourceFile).trim()).append("');").append(Encoder.newline);
			}
		}

		return query.toString();
	}

	public String getDeleteQuery() {
		StringBuilder query = new StringBuilder();

		if(!this.inchiKey.isEmpty()) {

			//delete synonyms
			query.append("DELETE FROM synonym WHERE inchi_key = '").append(this.inchiKey).append("'").append(Encoder.newline);
			query.append("DELETE FROM partial_inchi WHERE inchi_key = '").append(this.inchiKey).append("'").append(Encoder.newline);
			query.append("DELETE FROM external_id WHERE inchi_key = '").append(this.inchiKey).append("'").append(Encoder.newline);
			query.append("DELETE FROM compound WHERE id = '").append(this.inchiKey).append("'").append(Encoder.newline);
		}

		return query.toString();
	}

}
