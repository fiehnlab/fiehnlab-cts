package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.xml;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.jdom.Attribute;
import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.Parent;
import org.jdom.filter.Filter;
import org.jdom.input.SAXBuilder;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceProcessingException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.FileResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;

/**
 * takes an resource and converts it to jdom project. We also provide all the
 * normal methods, which a jdom parent has to simplify the work with it
 * 
 * @author wohlgemuth
 * 
 */
public class JDomResource extends AbstractResourceImpl implements Parent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// we just delegate over it
	private Element root;

	public JDomResource() {
		root = new Element("root");
	}

	@Override
	protected Resource internalProcessing(Resource resource) {

		// if resource equals null, we just ignore it and let the super class
		// handle it
		if (resource == null) {
			return super.internalProcessing(resource);
		}

		// if it's of the same type as this class just return it
		if (resource instanceof JDomResource) {
			return resource;
		}

		// cdk from the file resource to the input stream resource
		if (resource instanceof FileResource) {
			return this.internalProcessing(new InputStreamResource()
					.process(resource));
		}

		// if it's an input stream resource process it
		if (resource instanceof InputStreamResource) {
			SAXBuilder builder = new SAXBuilder();
			try {
				Document doc = builder.build(((InputStreamResource) resource).getRepresentation());
				Element root = doc.getRootElement();

				// once the conversion is done we return it as a new jdom
				// resource
				return new JDomResource(root);
			} catch (Exception e) {
				throw new ResourceProcessingException(e);
			}
		}
		return super.internalProcessing(resource);
	}

	@Override
	public boolean isResourceTypeAccepted(Resource resource) {
		if (resource instanceof FileResource)
			return true;
		if (resource instanceof JDomResource)
			return true;
		if(resource instanceof InputStreamResource)
			return true;
		
		return false;
	}

	public JDomResource(Element root) {
		super();
		this.root = root;
	}

	@Override
	public Element getRepresentation() {
		return root;
	}

	public Element addContent(Collection<Element> newContent) {
		return root.addContent(newContent);
	}

	public Element addContent(Content child) {
		return root.addContent(child);
	}

	public Element addContent(int index, Collection<Element> newContent) {
		return root.addContent(index, newContent);
	}

	public Element addContent(int index, Content child) {
		return root.addContent(index, child);
	}

	public Element addContent(String str) {
		return root.addContent(str);
	}

	public void addNamespaceDeclaration(Namespace additionalNamespace) {
		root.addNamespaceDeclaration(additionalNamespace);
	}

	public Object clone() {
		return root.clone();
	}

	public List<Element> cloneContent() {
		return root.cloneContent();
	}

	public Content detach() {
		return root.detach();
	}

	public final boolean equals(Object ob) {
		return root.equals(ob);
	}

	public List<Element> getAdditionalNamespaces() {
		return root.getAdditionalNamespaces();
	}

	public Attribute getAttribute(String name, Namespace ns) {
		return root.getAttribute(name, ns);
	}

	public Attribute getAttribute(String name) {
		return root.getAttribute(name);
	}

	public String getAttributeValue(String name, Namespace ns, String def) {
		return root.getAttributeValue(name, ns, def);
	}

	public String getAttributeValue(String name, Namespace ns) {
		return root.getAttributeValue(name, ns);
	}

	public String getAttributeValue(String name, String def) {
		return root.getAttributeValue(name, def);
	}

	public String getAttributeValue(String name) {
		return root.getAttributeValue(name);
	}

	public List<Element> getAttributes() {
		return root.getAttributes();
	}

	public Element getChild(String name, Namespace ns) {
		return root.getChild(name, ns);
	}

	public Element getChild(String name) {
		return root.getChild(name);
	}

	public String getChildText(String name, Namespace ns) {
		return root.getChildText(name, ns);
	}

	public String getChildText(String name) {
		return root.getChildText(name);
	}

	public String getChildTextNormalize(String name, Namespace ns) {
		return root.getChildTextNormalize(name, ns);
	}

	public String getChildTextNormalize(String name) {
		return root.getChildTextNormalize(name);
	}

	public String getChildTextTrim(String name, Namespace ns) {
		return root.getChildTextTrim(name, ns);
	}

	public String getChildTextTrim(String name) {
		return root.getChildTextTrim(name);
	}

	public List<Element> getChildren() {
		return root.getChildren();
	}

	public List<Element> getChildren(String name, Namespace ns) {
		return root.getChildren(name, ns);
	}

	public List<Element> getChildren(String name) {
		return root.getChildren(name);
	}

	public List<Element> getContent() {
		return root.getContent();
	}

	public List<Element> getContent(Filter filter) {
		return root.getContent(filter);
	}

	public Content getContent(int index) {
		return root.getContent(index);
	}

	public int getContentSize() {
		return root.getContentSize();
	}

	public Iterator<Element> getDescendants() {
		return root.getDescendants();
	}

	public Iterator<Element> getDescendants(Filter filter) {
		return root.getDescendants(filter);
	}

	public Document getDocument() {
		return root.getDocument();
	}

	public String getName() {
		return root.getName();
	}

	public Namespace getNamespace() {
		return root.getNamespace();
	}

	public Namespace getNamespace(String arg0) {
		return root.getNamespace(arg0);
	}

	public String getNamespacePrefix() {
		return root.getNamespacePrefix();
	}

	public String getNamespaceURI() {
		return root.getNamespaceURI();
	}

	public Parent getParent() {
		return root.getParent();
	}

	public Element getParentElement() {
		return root.getParentElement();
	}

	public String getQualifiedName() {
		return root.getQualifiedName();
	}

	public String getText() {
		return root.getText();
	}

	public String getTextNormalize() {
		return root.getTextNormalize();
	}

	public String getTextTrim() {
		return root.getTextTrim();
	}

	public String getValue() {
		return root.getValue();
	}

	public final int hashCode() {
		return root.hashCode();
	}

	public int indexOf(Content child) {
		return root.indexOf(child);
	}

	public boolean isAncestor(Element element) {
		return root.isAncestor(element);
	}

	public boolean isRootElement() {
		return root.isRootElement();
	}

	public boolean removeAttribute(Attribute attribute) {
		return root.removeAttribute(attribute);
	}

	public boolean removeAttribute(String name, Namespace ns) {
		return root.removeAttribute(name, ns);
	}

	public boolean removeAttribute(String name) {
		return root.removeAttribute(name);
	}

	public boolean removeChild(String name, Namespace ns) {
		return root.removeChild(name, ns);
	}

	public boolean removeChild(String name) {
		return root.removeChild(name);
	}

	public boolean removeChildren(String name, Namespace ns) {
		return root.removeChildren(name, ns);
	}

	public boolean removeChildren(String name) {
		return root.removeChildren(name);
	}

	public List<Element> removeContent() {
		return root.removeContent();
	}

	public boolean removeContent(Content child) {
		return root.removeContent(child);
	}

	public List<Element> removeContent(Filter arg0) {
		return root.removeContent(arg0);
	}

	public Content removeContent(int index) {
		return root.removeContent(index);
	}

	public void removeNamespaceDeclaration(Namespace additionalNamespace) {
		root.removeNamespaceDeclaration(additionalNamespace);
	}

	public Element setAttribute(Attribute attribute) {
		return root.setAttribute(attribute);
	}

	public Element setAttribute(String arg0, String arg1, Namespace arg2) {
		return root.setAttribute(arg0, arg1, arg2);
	}

	public Element setAttribute(String arg0, String arg1) {
		return root.setAttribute(arg0, arg1);
	}

	public Element setAttributes(List<Attribute> newAttributes) {
		return root.setAttributes(newAttributes);
	}

	public Element setContent(Collection<Element> newContent) {
		return root.setContent(newContent);
	}

	public Element setContent(Content child) {
		return root.setContent(child);
	}

	public Parent setContent(int index, Collection<Element> newContent) {
		return root.setContent(index, newContent);
	}

	public Element setContent(int index, Content child) {
		return root.setContent(index, child);
	}

	public void setName(String name) {
		root.setName(name);
	}

	public Element setNamespace(Namespace namespace) {
		return root.setNamespace(namespace);
	}

	public Element setText(String text) {
		return root.setText(text);
	}

	public String toString() {
		return root.toString();
	}
}
