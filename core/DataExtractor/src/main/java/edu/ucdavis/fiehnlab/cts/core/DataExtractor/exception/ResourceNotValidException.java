package edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception;

public class ResourceNotValidException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResourceNotValidException() {
	}

	public ResourceNotValidException(String arg0) {
		super(arg0);
	}

	public ResourceNotValidException(Throwable arg0) {
		super(arg0);
	}

	public ResourceNotValidException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
