package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.SQLGenerator;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.formula.IsotopeContainer;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.iterator.IteratingSDFReader;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 9/27/13
 * Time: 3:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class LipidsSDFFile extends AbstractResourceImpl implements SQLGenerator {
	/**
	 * constructor
	 *
	 * @param name
	 */
	public LipidsSDFFile(String name) {
		super();
		this.setName(name);
	}

	@Override
	protected Resource internalProcessing(Resource resource) {
		CollectionResource<Substance> data = null;

		if (resource instanceof InputStreamResource) {
			IteratingSDFReader sdfReader = null;
			IAtomContainer mol = null;

			try {
				data = new CollectionResource<Substance>(new ArrayList<Substance>());

				// read the sdf file
				sdfReader = new IteratingSDFReader((InputStream) resource.getRepresentation(), DefaultChemObjectBuilder.getInstance());

				// step through each substance section in the file
				while (sdfReader.hasNext()) {
					mol = sdfReader.next(); // get the current molecule
					logger.trace("\nFound a molecule: " + mol);

					String abbrev = mol.getProperty(LipidFields.ABBREV_PROPERTY).toString(); // get current molecule's abbreviation
					String systName = mol.getProperty(LipidFields.SYSTEMATIC_NAME_PROPERTY).toString(); // get current molecule's systematic name

					if (!abbrev.matches("^(FA|PA|PC|PE|PI|PS|PG|MG|DG|TG).*")) {
						abbrev = "FA".concat(abbrev);
					}

					abbrev = clean_name(abbrev);

					String inchiKey = "";
					String inchiCode = "";
					Double molWeight = 0.0;
					Double exactMass = 0.0;
					String formula = "";
					Set<NameObject> molnames = new HashSet<NameObject>();

					// start gathering properties
					try {
						logger.debug("\tGetting data for " + abbrev);
						logger.trace(mol.getProperties());

						//adding explicit hydrogens
						AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);

						// get basic compound info.
						InChIGenerator inchigen = InChIGeneratorFactory.getInstance().getInChIGenerator(mol);

						inchiKey = inchigen.getInchiKey();  // generate the inchiKey based on mol structure
						inchiCode = inchigen.getInchi();    // get inchi code

						// get molecular weight and check if it is valid (this is based on natural abundance of isotopes, molecules without natural occurring isotopes will return NaN)
						molWeight = AtomContainerManipulator.getNaturalExactMass(mol);
						if (molWeight.isNaN()) {
							logger.error("Can't calculate molecular weight for lipid " + inchiKey + ". (setting to -1.0)");
							molWeight = -1.0;
						}

						// get exact mass based on most abundant isotope, this should always be a value > 0, but checking just in case.
						try {
							exactMass = new IsotopeContainer(MolecularFormulaManipulator.getMolecularFormula(mol), 1).getMass();
						} catch (Exception e) {
							logger.error("Error: " + e.getMessage());
						}

						// get molecular formula, should come with all hydrogens
						formula = MolecularFormulaManipulator.getString(MolecularFormulaManipulator.getMolecularFormula(mol));

					} catch (CDKException ex) {
						// error generating inchiKey
						logger.error("Error generating InChI Key" + ex.getMessage() + " -- abbrev: " + abbrev, ex);
						continue;
					} catch (IllegalArgumentException ex) {
						logger.error("Found an invalid molecule block (abbrev:" + abbrev + ") in the file. " + ex.getMessage());
						continue;
					} catch (Exception ex) {
						logger.error("Something bad happened. mol=" + mol + "\n" + ex.getMessage(), ex);
					}


					Substance subst = new Substance(abbrev);
					subst.setInchiKey(inchiKey);
					subst.setInchiCode(inchiCode);
					subst.setMolWeight(molWeight);
					subst.setExactMass(exactMass);
					subst.setFormula(formula);
					subst.setSourceFile(this.getName());
					subst.addName(new NameObject(SubstanceFields.SYNONYM_PROPERTY, abbrev));
					subst.addName(new NameObject(SubstanceFields.SYNONYM_PROPERTY, systName));

					data.add(subst);
				}
			} catch (Exception e) {

			}
		}

		return data;
	}

	private String clean_name(String name) {
		String type = name.substring(0, 2);

		if (type.equals("MG")) {
			return name.replaceAll("/0:0", "");
		}
		if (type.equals("DG")) {
			return name.replace("/0:0)", ")");
		}

		return name;
	}
}
