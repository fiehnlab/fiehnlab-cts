package edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception;

/**
 * Created by diego on 8/26/2016.
 */
public class NullDataSourceException extends Exception {
	public NullDataSourceException() {
		super("Invalid PubChem depositor ID");
	}
}
