package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import com.google.gson.Gson;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.NullDataSourceException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceNotSupportedException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.IdObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.CommonChemDBs;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.PatternHelper;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.formula.IsotopeContainer;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.iterator.IteratingSDFReader;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * @author dpedrosa
 */
public class PubchemSubstanceFile extends AbstractResourceImpl {
	private static final int DEPOSITOR_REQUEST_TIMEOUT = 1000;
	private static Map<String, String> dataSourceNameMap = new HashMap<>();

	CollectionResource<Substance> data = null;
	Configuration config;

	List<String> badSynonyms = new ArrayList<String>();
	List<String> goodDepositors = new ArrayList<String>();

	public PubchemSubstanceFile(String name, Configuration config) {
		super();
		this.setName(name);
		this.config = config;
	}

	/**
	 * Processes a Pubchem Substance sdf file and extracts a collection of Substances
	 */
	@Override
	protected Resource internalProcessing(Resource resource) throws ResourceNotSupportedException {
		logger.info("processing...");
		CollectionResource<Substance> data = null;
		final String URL_PATTERN = "^(https?://)";


		Set<String> badSIDS = new HashSet<String>();
		Collections.addAll(badSIDS, config.getStringArray("pubchemIds.skip"));

		if (resource instanceof InputStreamResource) {
			IteratingSDFReader sdfReader = null;
			IAtomContainer mol = null;

			try {
				Collections.addAll(badSynonyms, config.getStringArray("synonyms.skip"));
				Collections.addAll(goodDepositors, config.getStringArray("depositors.include"));

				data = new CollectionResource<Substance>(new ArrayList<Substance>());

				// read the sdf file
				sdfReader = new IteratingSDFReader((InputStream) resource.getRepresentation(), DefaultChemObjectBuilder.getInstance());

				// step through each substance section in the file
				int i = 0;
				while (sdfReader.hasNext()) {
					mol = sdfReader.next(); // get the current molecule
					logger.trace("\nFound a molecule: " + mol);

					String sid = mol.getProperty(SubstanceFields.SID_PROPERTY).toString(); // get current molecule's PubChem SID;
//					if(i++ % 100 == 0) {
//						logger.debug("--(" + (i-1) + ") parsing substance " + sid + "--");
//					}

					// highly symmetrical molecules run forever... so skip them
					if (badSIDS.contains(sid)) {
						continue;
					}

					String inchiKey = "";
					String inchiCode = "";
					Double molWeight = -1.0;
					Double exactMass = -1.0;
					String formula = "";
					Set<NameObject> molnames = new HashSet<NameObject>();
					Map<String, IdObject> extids = new HashMap<String, IdObject>();
					String extid_name = "";
					String extid_url = "";
					String extid_val = "";

					// start gathering properties
					try {
						//adding explicit hydrogens
						AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);
//						CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
//						adder.addImplicitHydrogens(mol);
//						AtomContainerManipulator.convertImplicitToExplicitHydrogens(mol);

						// get basic compound info.
						InChIGenerator inchigen = InChIGeneratorFactory.getInstance().getInChIGenerator(mol);

						inchiKey = inchigen.getInchiKey();  // generate the inchiKey based on mol structure
						inchiCode = inchigen.getInchi();    // get inchi code

						// get molecular weight and check if it is valid (this is based on natural abundance of isotopes, molecules without natural occurring isotopes will return NaN)
						molWeight = AtomContainerManipulator.getNaturalExactMass(mol);
						if (molWeight.isNaN()) {
							logger.error("Can't calculate molecular weight for substance " + inchiKey + ". (setting to -1.0)");
						}

						// get exact mass based on most abundant isotope, this should always be a value > 0, but checking just in case.
						try {
							exactMass = new IsotopeContainer(MolecularFormulaManipulator.getMolecularFormula(mol), 1).getMass();
						} catch (Exception e) {
							logger.error("Error: " + e.getMessage());
						}

						// get molecular formula, should come with all hydrogens
						formula = MolecularFormulaManipulator.getString(MolecularFormulaManipulator.getMolecularFormula(mol));

					} catch (CDKException ex) {
						// error generating inchiKey
						logger.error("Error generating InChI Key " + ex.getMessage() + " -- SID:" + sid, ex);
						continue;
					} catch (IllegalArgumentException ex) {
						logger.error("Found an invalid molecule block (SID:" + sid + ") in the file. " + ex.getMessage());
						continue;
					} catch (Exception ex) {
						logger.error("Something bad happened. " + ex.getMessage(), ex);
					}


					// get possible external id value
					Iterator<String> item_iter = getMultiData(mol, SubstanceFields.XREF_DATASOURCE_ID_PROPERTY).iterator();
					if (item_iter.hasNext()) {
						extid_val = item_iter.next();
					} else {
						extid_val = "";
					}

					// getting external id source name, and Synonyms for good depositors
					for (String item : getMultiData(mol, SubstanceFields.XREF_DATASOURCE_NAME_PROPERTY)) {
						if (StringUtils.isNumeric(item)) {
							try {
								extid_name = getDataSourceName(item, sid);
							} catch (NullDataSourceException e) {
								continue;
							}
						} else {
							extid_name = item;
						}
						if (goodDepositors.contains(extid_name)) {
							molnames = resolveSynonyms(mol, extids);
						}
					}

					// getting external id url
					item_iter = getMultiData(mol, SubstanceFields.XREF_SUBSTANCE_URL_PROPERTY).iterator();
					if (item_iter.hasNext()) {
						String item = item_iter.next();
						try {
							URL url = new URL(item);
							extid_url = url.toString();
						} catch (MalformedURLException e) {
							try {
								URL url = new URL(extid_val);
								extid_url = url.toString();
								extid_val = item;
							} catch (MalformedURLException e1) {
								logger.error("Molecule: " + sid + " has invalaid url.");
								extid_url = "";
								extid_val = "";
							}
						}
					}

					// get possible CAS #
					for (String value : getMultiData(mol, SubstanceFields.CAS_NUMBER)) {
						extids.put(CommonChemDBs.CAS_NAME, new IdObject(CommonChemDBs.CAS_NAME, value, CommonChemDBs.CAS_URL));
					}


					// if not empty assign extid values to an IdObject
					if (!(extid_name.isEmpty() && extid_val.isEmpty())) {
						extids.put(extid_name, new IdObject(extid_name, extid_val, extid_url));
						extid_name = "";
						extid_val = "";
						extid_url = "";
					} else {
						logger.warn("Molecule: " + sid + " doesn't have enough information.");
					}

					// add PubChem SID as external ID
					extids.put(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, sid, SubstanceFields.PUBCHEM_BASE_URL.concat(sid)));

					//removing ids from names
					Set<NameObject> toBeRemoved = new HashSet<NameObject>();
					for (IdObject id : extids.values()) {
						for (NameObject name : molnames) {
							if (name.value.trim().equals(id.value.trim())) {
								toBeRemoved.add(name);
								logger.debug("\ttagged for removal");
							}
						}
						molnames.removeAll(toBeRemoved);
						toBeRemoved.clear();
					}

					// assign data to the substance
					Substance subst = new Substance(sid);
					subst.setInchiKey(inchiKey);
					subst.setInchiCode(inchiCode);
					subst.setMolWeight(molWeight);
					subst.setExactMass(exactMass);
					subst.setFormula(formula);
					subst.setSourceFile(this.getName());

					HashMap<String, Collection<NameObject>> names = new HashMap<String, Collection<NameObject>>();

					if (!molnames.isEmpty()) {
						names.put(CtsXmlFields.CTSXML_SYNONYMS_ELE, molnames);
					}
					subst.setNames(names);

					if (!extids.isEmpty()) {
						subst.setIds(extids);
					}

					data.add(subst);
				}
				sdfReader.close();

			} catch (IOException e) {
				logger.error("Error closing the data stream." + e.getMessage(), e);
			}

		} else {
			throw new ResourceNotSupportedException();
		}

		return data;
	}

	private Set<NameObject> resolveSynonyms(IAtomContainer mol, Map<String, IdObject> extids) {
		Set<NameObject> molnames = new HashSet<NameObject>();

		// get molecule name if it's not a bad Synonym ea. 'UNK'
		for (String value : getMultiData(mol, SubstanceFields.NAME_PROPERTY)) {
			if (isBadSynonym(value)) {
				continue;
			}

			molnames.add(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, value));
		}

		//get molecule synonyms if any and consolidate with names
		for (String value : getMultiData(mol, SubstanceFields.SYNONYM_PROPERTY)) {
			NameObject newName = new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, value);
			if (isBadSynonym(value)) {
				continue;
			}

			molnames.add(newName);
		}

		return molnames;
	}


	/**
	 * Returns true or false whether the name is accepted or trash
	 *
	 * @param value the name to be checked
	 * @return boolean
	 */
	private boolean isBadSynonym(String value) {
		return this.badSynonyms.contains(value.trim());
	}

	/**
	 * Returns true or false whether the name is from good or bad depositors
	 *
	 * @param value the depositor to be checked
	 * @return boolean
	 */
	private boolean isFromGoodDepositor(String value) {
		return this.goodDepositors.contains(value);
	}

	/**
	 * Returns a set of values contained in the specified property for the current molecule.
	 * If the property dosn't have any values or is not found in the molecule the resulting set is empty
	 *
	 * @param mol      Molecule being parsed
	 * @param property Property to get values from
	 * @return A set containing the values of the selected property, or an empty set.
	 */
	private Set<String> getMultiData(IAtomContainer mol, String property) {
		Set<String> items = new HashSet<String>();

		if (mol.getProperty(property) != null) {
			for (String n : mol.getProperty(property).toString().split("\r?\n")) {
				if (property.equals(SubstanceFields.NAME_PROPERTY) || property.equals(SubstanceFields.SYNONYM_PROPERTY)) {
					if (n.contains(";")) {
						continue;
					}
					// test against known external id patterns in synonym list to exclude from names...
					if (matchExternalId(n)) {
						continue;
					}
				}
				items.add(n);
			}
		}

		return items;
	}

	/**
	 * Gets the textual name of a PubChem depositor's id from the CTS db (through a RESTful service)
	 *
	 * @param dsid a String containing the data source id (from sdf file)
	 * @return a String with the name of the data source or an empty string if it wasn't found
	 */
	public synchronized String getDataSourceName(String dsid, String sid) throws NullDataSourceException {
		String name = "";

		if (dsid == null) {
			throw new NullDataSourceException();
		}

		if (dataSourceNameMap.containsKey(dsid)) {
			return dataSourceNameMap.get(dsid);
		}

		String cts = new StringBuilder().append(config.getString("depositorNameHost")).append("/service/sourceName/").append(dsid).toString();

		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		try {
			HttpGet request = new HttpGet(cts);

			response = client.execute(request);
			name = EntityUtils.toString(response.getEntity());

			if (name.trim().isEmpty()) {        // didn't find in cts, try pubchem (through cts)
				name = getDepositorNameFromPubchem("substance", sid, client);
			}

			logger.debug(String.format("Name from pubchem [dsid(%s), sid(%s)]: %s", dsid, sid, name));
			dataSourceNameMap.put(dsid, name);

		} catch (Exception e) {     // in case of a connection failure
			logger.error("error getting datasource name " + e.getCause());
			logger.error("missing from " + dsid);
			throw new NullDataSourceException();
		} finally {
			try {
				if (response != null) {
					response.close();
				}
				client.close();
			} catch (Exception e) {
				logger.error("[getDepositorNameFromPubchem] error closing client");
			}
		}

		return name;
	}

	/**
	 * searches pubchem directly for a depositor name
	 *
	 * @param type 'compound' or 'substance', the type of element whose depositor name needs to be resolved
	 * @param sid  source ID
	 * @return
	 */
	protected synchronized String getDepositorNameFromPubchem(String type, String sid, HttpClient client) throws IllegalArgumentException {
		String idname = "";
		String pug = "";

		if (type == null || type.trim().isEmpty()) {
			throw new IllegalArgumentException("type parameter can't be null or empty");
		} else {
			if (sid != null && !sid.trim().isEmpty()) {
				Long isid = Long.parseLong(sid);
				pug = new StringBuilder().append("http://pubchem.ncbi.nlm.nih.gov/rest/pug/").append(type)
						.append("/sid/").append(isid).append("/xrefs/SourceName/JSON").toString();
			}
			logger.debug("url: " + pug);
		}

		CloseableHttpResponse response = null;
		RequestConfig requestConfig = null;
		try {
			requestConfig = RequestConfig.custom()
					.setSocketTimeout(DEPOSITOR_REQUEST_TIMEOUT)
					.setConnectTimeout(DEPOSITOR_REQUEST_TIMEOUT)
					.build();

			HttpGet request = new HttpGet(pug);
			request.setConfig(requestConfig);
			request.addHeader("Accept-Encoding", "gzip, deflate");
			request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0");

			response = (CloseableHttpResponse) client.execute(request);

			/* response format (JSON):
			 *	{"InformationList": {
			 *		"Information": [{
			 *          "SID": 126523020,
			 *			"SourceName": [
			 *		    	"Human Metabolome Database"
			 *		    ]
			 *		}]
			 *	}}
			 */

			String data = EntityUtils.toString(response.getEntity(), "UTF-8");
			response.close();

			long start = System.nanoTime();
			// converting JSON response to Depositor object
			Gson g = new Gson();
			Depositor depositor = g.fromJson(data, Depositor.class);
			long end = System.nanoTime();

			logger.debug(new StringBuilder().append(depositor.InformationList.Information[0].SourceName[0]));

			idname = depositor.InformationList.Information[0].SourceName[0];

		} catch (IOException e) {
			logger.error("Error connecting to pubchem: " + e.getMessage() + "\nDetails: url" + requestConfig.toString());
			if (response != null) {
				logger.error("--\n" + response.getStatusLine());
			}
		} catch (Exception e) {
			logger.error("{getDepositorNameFromPubchem} made a booboo\n" + e.getMessage());
		}

		return idname;
	}

	/*
	 * Matches some known Id patterns and returns the item is an IdObject or null.
	 * @param synonyms
	 * @returns an IdObject if the synonym is a match, null otherwise
	 */
	protected boolean matchExternalId(String value) {

		if (value.matches(PatternHelper.CAS_PATTERN)) {
			// we have a cas number
			logger.trace(value + " is an cas id.\n");
			return true;
		} else if (value.matches(PatternHelper.HMDB_PATTERN)) {
			// we have an hamdb number
			logger.trace(value + " is an hmdb id.\n");
			return true;
		} else if (value.matches(PatternHelper.KEGG_PATTERN)) {
			// we have a kegg number
			logger.trace(value + " is an kegg id.\n");
			return true;
		} else if (value.matches(PatternHelper.LIPID_MAPS_PATTERN)) {
			// we have a LMSD number
			return true;
		} else if (value.matches(PatternHelper.NCGC_PATTERN)) {
			// we have an ncgc number
			logger.trace(value + " is an ncgc id.\n");
			return true;
		} else if (value.matches(PatternHelper.SIGMA_PATTERN)) {
			// we have an SIGMA number
			logger.trace(value + " is an SIGMA id.\n");
			return true;
		} else if (value.matches(PatternHelper.ALDRICH_PATTERN)) {
			// we have an ALDRICH number
			logger.trace(value + " is an ALDRICH id.\n");
			return true;
		} else if (value.matches(PatternHelper.FLUKA_PATTERN)) {
			// we have an FLUKA number
			logger.trace(value + " is an FLUKA id.\n");
			return true;
		} else if (value.matches(PatternHelper.RIEDEL_PATTERN)) {
			// we have an RIEDEL number
			logger.trace(value + " is an RIEDEL id.\n");
			return true;
		} else if (value.matches(PatternHelper.CHEBI_PATTERN)) {
			// we have an CHEBI number
			logger.trace(value + " is an CHEBI id.\n");
			return true;
		} else if (value.matches(PatternHelper.CHEMBL_PATTERN)) {
			// we have an CHEMBL number
			logger.trace(value + " is an CHEMBL id.\n");
			return true;
		} else if (value.matches(PatternHelper.HSDBL_PATTERN)) {
			// we have an HSDBL number
			logger.trace(value + " is an HSDBL id.\n");
			return true;
		}

		return false;
	}

	class Depositor {
		InformationList InformationList;

		public Depositor() {
			this.InformationList = new InformationList();
		}

		public Depositor(InformationList infoList) {
			this.InformationList = infoList;
		}
	}

	class InformationList {
		Information[] Information;

		public InformationList() {
			this.Information = new Information[]{};
		}

		public InformationList(Information[] info) {
			this.Information = info;
		}
	}

	class Information {
		long SID;
		String[] SourceName;

		public Information() {
			this.SID = 0;
			this.SourceName = new String[]{""};
		}

		public Information(long sid, String[] names) {
			this.SID = sid;
			this.SourceName = names;
		}
	}
}
