package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.exception.ResourceProcessingException;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.AbstractResourceImpl;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.FileResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.xml.JDomResource;

/**
 * provides us with a simple inputstream based resource
 * 
 * @author wohlgemuth
 * 
 */
public class InputStreamResource extends AbstractResourceImpl {

	InputStream input;

	public InputStreamResource(InputStream input) {
		super();
		this.input = input;
	}

	public InputStreamResource() {
		super();
	}

	@Override
	protected Resource internalProcessing(Resource resource) {

		// if resource equals null, we just ignore it and let the super class
		// handle it
		if (resource == null) {
			return super.internalProcessing(resource);
		}

		// if it's of the same type as this class just return it
		if (resource instanceof InputStreamResource) {
			return resource;
		}

		// cdk the jdom resource to an input stream, so that we can chain it
		// further
		if (resource instanceof JDomResource) {
			XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());
			String result = out.outputString(((JDomResource) resource)
					.getRepresentation());

			return new InputStreamResource(new ByteArrayInputStream(
					result.getBytes()));
		}

		// if it's a file resource process it
		if (resource instanceof FileResource) {
			try {
				return new InputStreamResource(new FileInputStream(
						((FileResource) resource).getRepresentation()));
			} catch (FileNotFoundException e) {
				throw new ResourceProcessingException(e);
			}
		}
		return super.internalProcessing(resource);
	}

	@Override
	public InputStream getRepresentation() {
		return input;
	}

	@Override
	public boolean isResourceTypeAccepted(Resource resource) {
		if (resource instanceof FileResource)
			return true;
		if (resource instanceof JDomResource)
			return true;
		if (resource instanceof InputStreamResource)
			return true;

		return false;
	}

	public int available() throws IOException {
		return input.available();
	}

	public void close() throws IOException {
		input.close();
	}

	public boolean equals(Object obj) {
		return input.equals(obj);
	}

	public int hashCode() {
		return input.hashCode();
	}

	public void mark(int arg0) {
		input.mark(arg0);
	}

	public boolean markSupported() {
		return input.markSupported();
	}

	public int read() throws IOException {
		return input.read();
	}

	public int read(byte[] arg0, int arg1, int arg2) throws IOException {
		return input.read(arg0, arg1, arg2);
	}

	public int read(byte[] arg0) throws IOException {
		return input.read(arg0);
	}

	public void reset() throws IOException {
		input.reset();
	}

	public long skip(long arg0) throws IOException {
		return input.skip(arg0);
	}

	public String toString() {
		return input.toString();
	}

}
