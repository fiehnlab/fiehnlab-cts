/**
 * 
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor.model;

/**
 * @author dpedrosa
 */
public class IdObject {
	public String name;
	public String value;
	public String url;

	public IdObject() {
		this("", "", "");
	}

	public IdObject(String name, String value, String url) {
		this.name = name;
		this.value = value;
		this.url = url;
	}

	@Override
	public boolean equals(Object x) {
		try{
			IdObject i = (IdObject) x;
			
			return (i.name.equals(this.name) && i.value.equals(this.value) && i.url.equals(this.url));
		}
		catch(ClassCastException e){
			//not required class just continues to false
		}
		catch(NullPointerException e){
			//not required class just continues to false
			
		}
		return false;
	}

	@Override
	public String toString() {
		return name + ": " + value + " (" + url + ")";
	}
	
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}
