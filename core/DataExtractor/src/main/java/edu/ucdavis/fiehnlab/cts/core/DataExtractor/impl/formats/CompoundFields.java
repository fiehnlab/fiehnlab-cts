package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

/**
 *
 * @author Diego
 */
public class CompoundFields {

	public static final String ID = "ID";
	public static final String INCHI = "InChI (Standard)";
	public static final String INCHI_KEY = "InChIKey (Standard)";
	public static final String SMILES = "SMILES";
	public static final String IUPAC_NAME = "IUPAC Name";
	public static final String PUBCHEM_CID = "PubChem CID";
	public static final String PUBCHEM_BASE_URL = "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi";
	public static final String SYNONYMS = "Synonyms";
	public static final String CTS_HMDB_ID = "Human Metabolome Database";
	public static final String HMDB_METABOLITE_URL = "http://www.hmdb.ca/metabolites/";

	public static final String ROOT_ELE = "PC-Compounds";
	public static final String MAIN_ELE = "PC-Compound";
	public static final String ID_ELE = "/PC-Compound_id/PC-CompoundType/PC-CompoundType_id/PC-CompoundType_id_cid";
	
	public static final String SDF_CID = "PUBCHEM_COMPOUND_CID";
	public static final String SDF_IUPAC_NAME_REGEX = "PUBCHEM_IUPAC_[A-Z_]*NAME";
	public static final String SDF_IUPAC_ALLOWED = "PUBCHEM_IUPAC_OPENEYE_NAME";
	public static final String SDF_IUPAC_CAS = "PUBCHEM_IUPAC_CAS_NAME";
	public static final String SDF_IUPAC_PREFERRED = "PUBCHEM_IUPAC_NAME";
	public static final String SDF_IUPAC_SYSTEMATIC = "PUBCHEM_IUPAC_SYSTEMATIC_NAME";
	public static final String SDF_IUPAC_TRADITIONAL = "PUBCHEM_IUPAC_TRADITIONAL_NAME";
	public static final String SDF_INCHI_CODE = "PUBCHEM_IUPAC_INCHI";
	public static final String SDF_INCHI_KEY = "PUBCHEM_IUPAC_INCHIKEY";
	public static final String SDF_FORMULA = "PUBCHEM_MOLECULAR_FORMULA";
	public static final String SDF_WEIGHT = "PUBCHEM_MOLECULAR_WEIGHT";
	public static final String SDF_EXACT_MASS = "PUBCHEM_EXACT_MASS";
	public static final String SDF_CAN_SMILES = "PUBCHEM_OPENEYE_CAN_SMILES";

	public static final String SDF_HMDB_ID = "HMDB_ID";
}