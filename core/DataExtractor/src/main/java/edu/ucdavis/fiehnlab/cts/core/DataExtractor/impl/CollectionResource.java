package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl;

import java.util.Collection;
import java.util.Iterator;

/**
 * provides us with a collection of arguments as resource and implements the
 * collection interface
 * 
 * @author wohlgemuth
 * 
 */
public class CollectionResource<E> extends AbstractResourceImpl implements Collection<E> {

	private Collection<E> collection = null;

	public CollectionResource(Collection<E> collection) {
		super();
		this.collection = collection;
	}

	@Override
	public Collection<E> getRepresentation() {
		return this;
	}

	public synchronized boolean add(E e) {
		return collection.add(e);
	}

	public synchronized boolean addAll(Collection<? extends E> c) {
		return collection.addAll(c);
	}

	public synchronized void clear() {
		collection.clear();
	}

	public synchronized boolean contains(Object o) {
		return collection.contains(o);
	}

	public synchronized boolean containsAll(Collection<?> c) {
		return collection.containsAll(c);
	}

	public boolean equals(Object o) {
		return collection.equals(o);
	}

	public int hashCode() {
		return collection.hashCode();
	}

	public synchronized boolean isEmpty() {
		return collection.isEmpty();
	}

	public Iterator<E> iterator() {
		return collection.iterator();
	}

	public synchronized boolean remove(Object o) {
		return collection.remove(o);
	}

	public synchronized boolean removeAll(Collection<?> c) {
		return collection.removeAll(c);
	}

	public synchronized boolean retainAll(Collection<?> c) {
		return collection.retainAll(c);
	}

	public synchronized int size() {
		return collection.size();
	}

	public Object[] toArray() {
		return collection.toArray();
	}

	public <E> E[] toArray(E[] a) {
		return collection.toArray(a);
	}

}
