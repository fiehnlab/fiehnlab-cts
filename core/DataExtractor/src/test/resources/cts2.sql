CREATE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO postgres;

SET search_path = public, pg_catalog;

CREATE FUNCTION insert_compound(inchikey character varying, inchicode text, molweight double precision) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO compound(id,inchi_code,mol_weight) VALUES (inchikey,inchicode,molweight);
           RETURN;
    EXCEPTION WHEN unique_violation THEN
            RETURN;
END;
$$;


ALTER FUNCTION public.insert_compound(inchikey character varying, inchicode text, molweight double precision) OWNER TO compound;


CREATE TABLE compound (
    id character varying(27) NOT NULL,
    inchi_code text,
    mol_weight double precision NOT NULL
);

ALTER TABLE public.compound OWNER TO compound;


CREATE TABLE external_id (
    id bigint NOT NULL,
    inchi_key character varying(27) NOT NULL,
    name text NOT NULL,
    url character varying(255) NOT NULL,
    value text NOT NULL
);

ALTER TABLE public.external_id OWNER TO compound;


CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE public.hibernate_sequence OWNER TO compound;


CREATE TABLE synonym (
    id bigint NOT NULL,
    inchi_key character varying(27) NOT NULL,
    name text NOT NULL,
    type character varying(27) NOT NULL
);

ALTER TABLE public.synonym OWNER TO compound;

