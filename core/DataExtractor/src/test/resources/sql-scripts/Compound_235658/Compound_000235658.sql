BEGIN;
select insert_compound(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'InChI=1S/C10H11NO5/c1-6(2)16-9-7(10(12)13)4-3-5-8(9)11(14)15/h3-6H,1-2H3,(H,12,13)', 225.19804, 0.0, E'', E'');
select insert_synonym(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'3-nitro-2-propan-2-yloxybenzoic acid', E'IUPAC Name (CAS-like Style)', E'');
select insert_synonym(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'2-isopropoxy-3-nitro-benzoic acid', E'IUPAC Name (Traditional)', E'');
select insert_synonym(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'3-nitro-2-propan-2-yloxy-benzoic acid', E'IUPAC Name (Systematic)', E'');
select insert_synonym(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'3-nitro-2-propan-2-yloxybenzoic acid', E'IUPAC Name (Preferred)', E'');
select insert_synonym(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'2-isopropoxy-3-nitro-benzoic acid', E'IUPAC Name (Allowed)', E'');
select insert_external_id(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'235658', E'');

COMMIT;
