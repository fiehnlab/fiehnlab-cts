select insert_compound(E'PWHULOQIROXLJO-UHFFFAOYSA-N', E'InChI=1S/Mn', 54.9380451, 54.9380451, E'Mn', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'PWHULOQIROXLJO-UHFFFAOYSA-N', E'Manganese (Ii) Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'PWHULOQIROXLJO-UHFFFAOYSA-N', E'MN', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'PWHULOQIROXLJO-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109868', E'11109868', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'PWHULOQIROXLJO-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38459', E'38459.5', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'InChI=1S/Ca/q+2', 40.0780226519885, 39.96149382018146, E'Ca', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Calcium Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'CA', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109869', E'11109869', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38459', E'38459.6', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'InChI=1S/H2O4S/c1-5(2,3)4/h(H2,1,2,3,4)', 98.07958621096718, 97.967379544, E'H2O4S', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'SO4', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Sulfate Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109874', E'11109874', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38469', E'38469.2', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'InChI=1S/C21H27N7O14P2/c22-17-12-19(25-7-24-17)28(8-26-12)21-16(32)14(30)11(41-21)6-39-44(36,37)42-43(34,35)38-5-10-13(29)15(31)20(40-10)27-3-1-2-9(4-27)18(23)33/h1-4,7-8,10-11,13-16,20-21,29-32H,5-6H2,(H5-,22,23,24,25,33,34,35,36,37)/p+1/t10-,11-,13-,14-,15-,16-,20-,21-/m1/s1', 664.4339096348934, 664.1163982560907, E'C21H28N7O14P2', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'NAD', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'Nicotinamide-Adenine-Dinucleotide', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109875', E'11109875', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38469', E'38469.7', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'QTBSBXVTEAMEQO-UHFFFAOYSA-N', E'InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)', 60.052044664017956, 60.021129368, E'C2H4O2', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QTBSBXVTEAMEQO-UHFFFAOYSA-N', E'ACT', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QTBSBXVTEAMEQO-UHFFFAOYSA-N', E'Acetate Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QTBSBXVTEAMEQO-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109881', E'11109881', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QTBSBXVTEAMEQO-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38909', E'38909.3', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'DHPCBFMFERFZLR-UHFFFAOYSA-N', E'InChI=1S/C15H9BrO3/c16-14-11-6-5-10(18)7-12(11)15(19)13(14)8-1-3-9(17)4-2-8/h1-7,17-18H', 317.1342479898101, 315.973506248, E'C15H9BrO3', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'DHPCBFMFERFZLR-UHFFFAOYSA-N', E'789', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'DHPCBFMFERFZLR-UHFFFAOYSA-N', E'3-Bromo-6-Hydroxy-2-(4-Hydroxyphenyl)-1h-Inden-1-One', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'DHPCBFMFERFZLR-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109904', E'11109904', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'DHPCBFMFERFZLR-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38485', E'38485.5', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'InChI=1S/Mg/q+2', 24.305051612423, 23.98394454018146, E'Mg', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'MG', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'Magnesium Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109917', E'11109917', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38494', E'38494.3', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'PEDCQBHIVMGVHV-UHFFFAOYSA-N', E'InChI=1S/C3H8O3/c4-1-3(6)2-5/h3-6H,1-2H2', 92.09394850367855, 92.04734411599999, E'C3H8O3', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'PEDCQBHIVMGVHV-UHFFFAOYSA-N', E'Glycerol', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'PEDCQBHIVMGVHV-UHFFFAOYSA-N', E'GOL', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'PEDCQBHIVMGVHV-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110136', E'11110136', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'PEDCQBHIVMGVHV-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38662', E'38662.5', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'WHUUTDBJXJRKMK-VKHMYHEASA-N', E'InChI=1S/C5H9NO4/c6-3(5(9)10)1-2-4(7)8/h3H,1-2,6H2,(H,7,8)(H,9,10)/t3-/m0/s1', 147.12946918568574, 147.05315776799998, E'C5H9NO4', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'WHUUTDBJXJRKMK-VKHMYHEASA-N', E'Glutamic Acid', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'WHUUTDBJXJRKMK-VKHMYHEASA-N', E'GLU', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'WHUUTDBJXJRKMK-VKHMYHEASA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110137', E'11110137', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'WHUUTDBJXJRKMK-VKHMYHEASA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38662', E'38662.7', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'DSOJUJXBVIHWRS-VKHMYHEASA-N', E'InChI=1S/C5H9NO4/c6-3(5(7)8)1-2-4-9-10-4/h3-4H,1-2,6H2,(H,7,8)/t3-/m0/s1', 147.12946918568574, 147.05315776799998, E'C5H9NO4', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'DSOJUJXBVIHWRS-VKHMYHEASA-N', E'Glutamic Acid', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'DSOJUJXBVIHWRS-VKHMYHEASA-N', E'GLU', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'DSOJUJXBVIHWRS-VKHMYHEASA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110138', E'11110138', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'DSOJUJXBVIHWRS-VKHMYHEASA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38662', E'38662.9', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'InChI=1S/Mg/q+2', 24.305051612423, 23.98394454018146, E'Mg', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'MG', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'Magnesium Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110171', E'11110171', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38685', E'38685.3', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'NBIIXXVUZAFLBC-UHFFFAOYSA-N', E'InChI=1S/H3O4P/c1-5(2,3)4/h(H3,1,2,3,4)', 97.99520360175498, 97.976895206, E'H3O4P', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'NBIIXXVUZAFLBC-UHFFFAOYSA-N', E'Phosphate Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'NBIIXXVUZAFLBC-UHFFFAOYSA-N', E'PO4', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'NBIIXXVUZAFLBC-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110172', E'11110172', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'NBIIXXVUZAFLBC-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38685', E'38685.4', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'FKNQFGJONOIPTF-UHFFFAOYSA-N', E'InChI=1S/Na/q+1', 22.98976928, 22.98922070009073, E'Na', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'FKNQFGJONOIPTF-UHFFFAOYSA-N', E'Sodium Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'FKNQFGJONOIPTF-UHFFFAOYSA-N', E'NA', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'FKNQFGJONOIPTF-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110173', E'11110173', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'FKNQFGJONOIPTF-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38685', E'38685.5', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'VEXZGXHMUGYJMC-UHFFFAOYSA-M', E'InChI=1S/ClH/h1H/p-1', 35.452937578184, 34.96940125990927, E'Cl', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'VEXZGXHMUGYJMC-UHFFFAOYSA-M', E'Chloride Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'VEXZGXHMUGYJMC-UHFFFAOYSA-M', E'CL', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'VEXZGXHMUGYJMC-UHFFFAOYSA-M', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110197', E'11110197', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'VEXZGXHMUGYJMC-UHFFFAOYSA-M', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38692', E'38692.2', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'ZRALSGWEFCBTJO-UHFFFAOYSA-N', E'InChI=1S/CH5N3/c2-1(3)4/h(H5,2,3,4)', 59.07054928702494, 59.04834716, E'CH5N3', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'ZRALSGWEFCBTJO-UHFFFAOYSA-N', E'GAI', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'ZRALSGWEFCBTJO-UHFFFAOYSA-N', E'Guanidine', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'ZRALSGWEFCBTJO-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110198', E'11110198', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'ZRALSGWEFCBTJO-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38692', E'38692.3', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'KYNFOMQIXZUKRK-UHFFFAOYSA-N', E'InChI=1S/C4H10O2S2/c5-1-3-7-8-4-2-6/h5-6H,1-4H2', 154.25333096662473, 154.01222156, E'C4H10O2S2', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'KYNFOMQIXZUKRK-UHFFFAOYSA-N', E'2-Hydroxyethyl Disulfide', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'KYNFOMQIXZUKRK-UHFFFAOYSA-N', E'HED', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'KYNFOMQIXZUKRK-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110199', E'11110199', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'KYNFOMQIXZUKRK-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38692', E'38692.4', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'InChI=1S/H2O4S/c1-5(2,3)4/h(H2,1,2,3,4)', 98.07958621096718, 97.967379544, E'H2O4S', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'SO4', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Sulfate Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110239', E'11110239', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38715', E'38715.7', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'InChI=1S/Ca/q+2', 40.0780226519885, 39.96149382018146, E'Ca', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Calcium Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'CA', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110294', E'11110294', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38742', E'38742.2', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'InChI=1S/H2O4S/c1-5(2,3)4/h(H2,1,2,3,4)', 98.07958621096718, 97.967379544, E'H2O4S', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'SO4', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Sulfate Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110295', E'11110295', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38742', E'38742.3', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'WGTGQGJDNAGBCC-ZCFIWIBFSA-N', E'InChI=1S/C6H12O2/c1-2-3-4-6(8)5-7/h2,6-8H,1,3-5H2/t6-/m1/s1', 116.15851428177632, 116.083729624, E'C6H12O2', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'WGTGQGJDNAGBCC-ZCFIWIBFSA-N', E'(2s)-Hex-5-Ene-1,2-Diol', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'WGTGQGJDNAGBCC-ZCFIWIBFSA-N', E'217', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'WGTGQGJDNAGBCC-ZCFIWIBFSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110296', E'11110296', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'WGTGQGJDNAGBCC-ZCFIWIBFSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38742', E'38742.5', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'InChI=1S/Mg/q+2', 24.305051612423, 23.98394454018146, E'Mg', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'MG', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'Magnesium Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110350', E'11110350', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'JLVVSXFLKOJNIY-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38776', E'38776.3', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'InChI=1S/H2O4S/c1-5(2,3)4/h(H2,1,2,3,4)', 98.07958621096718, 97.967379544, E'H2O4S', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'SO4', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Sulfate Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110427', E'11110427', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38835', E'38835.4', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'InChI=1S/Ca/q+2', 40.0780226519885, 39.96149382018146, E'Ca', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Calcium Ion', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'CA', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110434', E'11110434', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38845', E'38845.5', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'YBJHBAHKTGYVGT-ZKWXMUAHSA-N', E'InChI=1S/C10H16N2O3S/c13-8(14)4-2-1-3-7-9-6(5-16-7)11-10(15)12-9/h6-7,9H,1-5H2,(H,13,14)(H2,11,12,15)/t6-,7-,9-/m0/s1', 244.31211721891077, 244.08816337199997, E'C10H16N2O3S', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'YBJHBAHKTGYVGT-ZKWXMUAHSA-N', E'BTN', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'YBJHBAHKTGYVGT-ZKWXMUAHSA-N', E'Biotin', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'YBJHBAHKTGYVGT-ZKWXMUAHSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110441', E'11110441', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'YBJHBAHKTGYVGT-ZKWXMUAHSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38849', E'38849.3', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'YBJHBAHKTGYVGT-OOZYFLPDSA-N', E'InChI=1S/C10H16N2O3S/c13-8(14)4-2-1-3-7-9-6(5-16-7)11-10(15)12-9/h6-7,9H,1-5H2,(H,13,14)(H2,11,12,15)/t6-,7+,9-/m0/s1', 244.31211721891077, 244.08816337199997, E'C10H16N2O3S', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'YBJHBAHKTGYVGT-OOZYFLPDSA-N', E'Epi-Biotin', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'YBJHBAHKTGYVGT-OOZYFLPDSA-N', E'BTQ', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'YBJHBAHKTGYVGT-OOZYFLPDSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110442', E'11110442', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'YBJHBAHKTGYVGT-OOZYFLPDSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38849', E'38849.4', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'ZHBOBAJCKHPQBT-INIZCTEOSA-N', E'InChI=1S/C24H28ClN5O3/c1-16(24(2,3)32)28-22-26-14-13-21(29-22)30(18-9-11-19(33-4)12-10-18)23(31)27-15-17-7-5-6-8-20(17)25/h5-14,16,32H,15H2,1-4H3,(H,27,31)(H,26,28,29)/t16-/m0/s1', 469.96467102610603, 469.188067436, E'C24H28ClN5O3', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'ZHBOBAJCKHPQBT-INIZCTEOSA-N', E'3-(2-Chlorobenzyl)-1-(2-{[(1s)-2-Hydroxy-1,2-Dimethylpropyl]amino}pyrimidin-4-Yl)-1-(4-Methoxyphenyl)urea', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'ZHBOBAJCKHPQBT-INIZCTEOSA-N', E'LIC', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'ZHBOBAJCKHPQBT-INIZCTEOSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110448', E'11110448', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'ZHBOBAJCKHPQBT-INIZCTEOSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38854', E'38854.2', E'Substance_011100001_011125000.sdf.gz');

select insert_compound(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'InChI=1S/C34H38N4O4.Fe/c1-7-21-17(3)25-13-26-19(5)23(9-11-33(39)40)31(37-26)16-32-24(10-12-34(41)42)20(6)28(38-32)15-30-22(8-2)18(4)27(36-30)14-29(21)35-25;/h13-16H,7-12H2,1-6H3,(H4,35,36,37,38,39,40,41,42);/q;+2/p-2/b25-13-,26-13-,27-14-,28-15-,29-14-,30-15-,31-16-,32-16-;', 620.5204657904685, 620.2085931319999, E'C34H36FeN4O4', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Protoporphyrin Ix Containing Fe', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'HEM', E'Synonym', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110513', E'11110513', E'Substance_011100001_011125000.sdf.gz');
select insert_external_id(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38892', E'38892.3', E'Substance_011100001_011125000.sdf.gz');

COMMIT;
