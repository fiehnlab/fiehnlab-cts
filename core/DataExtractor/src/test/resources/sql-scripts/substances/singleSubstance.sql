select insert_compound(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'InChI=1S/C34H38N4O4.Fe/c1-7-21-17(3)25-13-26-19(5)23(9-11-33(39)40)31(37-26)16-32-24(10-12-34(41)42)20(6)28(38-32)15-30-22(8-2)18(4)27(36-30)14-29(21)35-25;/h13-16H,7-12H2,1-6H3,(H4,35,36,37,38,39,40,41,42);/q;+2/p-2/b25-13-,26-13-,27-14-,28-15-,29-14-,30-15-,31-16-,32-16-;', 620.5204657904685, 620.208593132, E'C34H36FeN4O4', E'singleSubstance.sdf.gz');
select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Protoporphyrin Ix Containing Fe', E'Synonym', E'singleSubstance.sdf.gz');
select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'HEM', E'Synonym', E'singleSubstance.sdf.gz');
select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Fe-40', E'Synonym', E'singleSubstance.sdf.gz');
select insert_external_id(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110513', E'11110513', E'singleSubstance.sdf.gz');
select insert_external_id(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38892', E'38892.3', E'singleSubstance.sdf.gz');

COMMIT;
