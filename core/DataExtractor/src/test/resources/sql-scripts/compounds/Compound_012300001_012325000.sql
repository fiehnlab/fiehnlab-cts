BEGIN;
select insert_compound(E'GNWNPLBSEQDDQV-FRPWFYLFSA-N', E'InChI=1S/C15H20O3/c1-9(2)5-6-13-15(4,17)14-11(16)7-10(3)8-12(14)18-13/h5-7,12,14,17H,8H2,1-4H3/b13-6-/t12-,14+,15-/m1/s1', 248.31806831104373, 248.14124449999997, E'C15H20O3', E'Compound_012300001_012325000.sdf.gz', E'CC1=CC(=O)C2C(C1)OC(=CC=C(C)C)C2(C)O');
select insert_synonym(E'GNWNPLBSEQDDQV-FRPWFYLFSA-N', E'(2Z,3S,3aS,7aR)-3-hydroxy-3,6-dimethyl-2-(3-methylbut-2-enylidene)-7,7a-dihydro-3aH-benzofuran-4-one', E'IUPAC Name (CAS-like Style)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'GNWNPLBSEQDDQV-FRPWFYLFSA-N', E'(2Z,3S,3aS,7aR)-3-hydroxy-3,6-dimethyl-2-(3-methylbut-2-enylidene)-7,7a-dihydro-3aH-benzofuran-4-one', E'IUPAC Name (Traditional)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'GNWNPLBSEQDDQV-FRPWFYLFSA-N', E'(2Z,3S,3aS,7aR)-3,6-dimethyl-2-(3-methylbut-2-enylidene)-3-oxidanyl-7,7a-dihydro-3aH-1-benzofuran-4-one', E'IUPAC Name (Systematic)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'GNWNPLBSEQDDQV-FRPWFYLFSA-N', E'(2Z,3S,3aS,7aR)-3-hydroxy-3,6-dimethyl-2-(3-methylbut-2-enylidene)-7,7a-dihydro-3aH-1-benzofuran-4-one', E'IUPAC Name (Preferred)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'GNWNPLBSEQDDQV-FRPWFYLFSA-N', E'(2Z,3S,3aS,7aR)-3-hydroxy-3,6-dimethyl-2-(3-methylbut-2-enylidene)-7,7a-dihydro-3aH-benzofuran-4-one', E'IUPAC Name (Allowed)', E'Compound_012300001_012325000.sdf.gz');
select insert_external_id(E'GNWNPLBSEQDDQV-FRPWFYLFSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12300142', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'WLCHQSHZHFLMJH-OHVPXIODSA-N', E'InChI=1S/C30H46O5/c1-16(2)17-10-13-30(25(34)35)15-14-27(5)18(21(17)30)8-9-20-28(27,6)12-11-19-26(3,4)23(31)22(24(32)33)29(19,20)7/h17-23,31H,1,8-15H2,2-7H3,(H,32,33)(H,34,35)/t17-,18+,19-,20-,21+,22-,23+,27+,28+,29-,30-/m0/s1', 486.6843762174726, 486.33452457199996, E'C30H46O5', E'Compound_012300001_012325000.sdf.gz', E'CC(=C)C1CCC2(C1C3CCC4C(C3(CC2)C)(CCC5C4(C(C(C5(C)C)O)C(=O)O)C)C)C(=O)O');
select insert_external_id(E'WLCHQSHZHFLMJH-OHVPXIODSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12302582', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'RPAORVSEYNOMBR-UVBJJODRSA-N', E'InChI=1S/C16H17NO3/c18-11-1-2-16-3-4-17(15(16)6-11)8-10-5-13-14(7-12(10)16)20-9-19-13/h1-2,5,7,11,15,18H,3-4,6,8-9H2/t11-,15-,16-/m0/s1', 271.3116851533904, 271.120843404, E'C16H17NO3', E'Compound_012300001_012325000.sdf.gz', E'C1CN2CC3=CC4=C(C=C3C15C2CC(C=C5)O)OCO4');
select insert_external_id(E'RPAORVSEYNOMBR-UVBJJODRSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12304132', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'LAQCZBYXNRANFU-WQOXNDGTSA-N', E'InChI=1S/C19H24O5/c1-5-6-14(20)23-12-8-13-19(9-21-19)18(12,4)17(3)11(22-13)7-10(2)15-16(17)24-15/h5-7,11-13,15-16H,8-9H2,1-4H3/b6-5-/t11-,12-,13-,15+,16+,17-,18-,19+/m1/s1', 332.3915847686376, 332.162373868, E'C19H24O5', E'Compound_012300001_012325000.sdf.gz', E'CC=CC(=O)OC1CC2C3(C1(C4(C(O2)C=C(C5C4O5)C)C)C)CO3');
select insert_external_id(E'LAQCZBYXNRANFU-WQOXNDGTSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12304146', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'HFSACQSILLSUII-VXRRTDEQSA-N', E'InChI=1S/C30H50O/c1-25(2)13-14-27(5)15-17-29(7)22-11-9-20-21(10-12-24(31)26(20,3)4)28(22,6)16-18-30(29,8)23(27)19-25/h9,21-24,31H,10-19H2,1-8H3/t21-,22+,23-,24-,27-,28+,29-,30+/m1/s1', 426.7185195224982, 426.38616621999995, E'C30H50O', E'Compound_012300001_012325000.sdf.gz', E'CC1(CCC2(CCC3(C4CC=C5C(C4(CCC3(C2C1)C)C)CCC(C5(C)C)O)C)C)C');
select insert_synonym(E'HFSACQSILLSUII-VXRRTDEQSA-N', E'(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol', E'IUPAC Name (CAS-like Style)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HFSACQSILLSUII-VXRRTDEQSA-N', E'(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol', E'IUPAC Name (Traditional)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HFSACQSILLSUII-VXRRTDEQSA-N', E'(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol', E'IUPAC Name (Systematic)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HFSACQSILLSUII-VXRRTDEQSA-N', E'(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol', E'IUPAC Name (Preferred)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HFSACQSILLSUII-VXRRTDEQSA-N', E'(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol', E'IUPAC Name (Allowed)', E'Compound_012300001_012325000.sdf.gz');
select insert_external_id(E'HFSACQSILLSUII-VXRRTDEQSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12309496', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'KBHLNNQHHPFDSG-VTGIHGMASA-N', E'InChI=1S/C19H34O3/c1-3-4-5-6-7-8-9-10-11-12-13-14-15-17-18(20)16(2)22-19(17)21/h15-16,18,20H,3-14H2,1-2H3/b17-15+/t16-,18+/m1/s1', 310.4721824517566, 310.25079494799996, E'C19H34O3', E'Compound_012300001_012325000.sdf.gz', E'CCCCCCCCCCCCCC=C1C(C(OC1=O)C)O');
select insert_synonym(E'KBHLNNQHHPFDSG-VTGIHGMASA-N', E'(3E,4R,5R)-4-hydroxy-5-methyl-3-tetradecylidene-2-oxolanone', E'IUPAC Name (CAS-like Style)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'KBHLNNQHHPFDSG-VTGIHGMASA-N', E'(3E,4R,5R)-4-hydroxy-5-methyl-3-tetradecylidene-tetrahydrofuran-2-one', E'IUPAC Name (Traditional)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'KBHLNNQHHPFDSG-VTGIHGMASA-N', E'(3E,4R,5R)-5-methyl-4-oxidanyl-3-tetradecylidene-oxolan-2-one', E'IUPAC Name (Systematic)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'KBHLNNQHHPFDSG-VTGIHGMASA-N', E'(3E,4R,5R)-4-hydroxy-5-methyl-3-tetradecylideneoxolan-2-one', E'IUPAC Name (Preferred)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'KBHLNNQHHPFDSG-VTGIHGMASA-N', E'(3E,4R,5R)-4-hydroxy-5-methyl-3-tetradecylidene-tetrahydrofuran-2-one', E'IUPAC Name (Allowed)', E'Compound_012300001_012325000.sdf.gz');
select insert_external_id(E'KBHLNNQHHPFDSG-VTGIHGMASA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12311312', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'VOXZDWNPVJITMN-LUWVYCGLSA-N', E'InChI=1S/C18H24O2/c1-18-9-8-14-13-5-3-12(19)10-11(13)2-4-15(14)16(18)6-7-17(18)20/h3,5,10,14-17,19-20H,2,4,6-9H2,1H3/t14?,15-,16+,17+,18+/m1/s1', 272.38263408914145, 272.177630008, E'C18H24O2', E'Compound_012300001_012325000.sdf.gz', E'CC12CCC3C(C1CCC2O)CCC4=C3C=CC(=C4)O');
select insert_synonym(E'VOXZDWNPVJITMN-LUWVYCGLSA-N', E'(8R,13S,14S,17S)-13-methyl-6,7,8,9,11,12,14,15,16,17-decahydrocyclopenta[a]phenanthrene-3,17-diol', E'IUPAC Name (CAS-like Style)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'VOXZDWNPVJITMN-LUWVYCGLSA-N', E'(8R,13S,14S,17S)-13-methyl-6,7,8,9,11,12,14,15,16,17-decahydrocyclopenta[a]phenanthrene-3,17-diol', E'IUPAC Name (Traditional)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'VOXZDWNPVJITMN-LUWVYCGLSA-N', E'(8R,13S,14S,17S)-13-methyl-6,7,8,9,11,12,14,15,16,17-decahydrocyclopenta[a]phenanthrene-3,17-diol', E'IUPAC Name (Systematic)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'VOXZDWNPVJITMN-LUWVYCGLSA-N', E'(8R,13S,14S,17S)-13-methyl-6,7,8,9,11,12,14,15,16,17-decahydrocyclopenta[a]phenanthrene-3,17-diol', E'IUPAC Name (Preferred)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'VOXZDWNPVJITMN-LUWVYCGLSA-N', E'(8R,13S,14S,17S)-13-methyl-6,7,8,9,11,12,14,15,16,17-decahydrocyclopenta[a]phenanthrene-3,17-diol', E'IUPAC Name (Allowed)', E'Compound_012300001_012325000.sdf.gz');
select insert_external_id(E'VOXZDWNPVJITMN-LUWVYCGLSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12313791', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'HPQKNJHVWUWAOR-QBSBYYDBSA-N', E'InChI=1S/C20H32O4/c1-13(12-17(21)22)6-8-15-14(2)7-9-16-19(15,3)10-5-11-20(16,4)18(23)24/h13,15-16H,2,5-12H2,1,3-4H3,(H,21,22)(H,23,24)/t13-,15-,16+,19+,20+/m0/s1', 336.4664417684624, 336.230059504, E'C20H32O4', E'Compound_012300001_012325000.sdf.gz', E'CC(CCC1C(=C)CCC2C1(CCCC2(C)C(=O)O)C)CC(=O)O');
select insert_synonym(E'HPQKNJHVWUWAOR-QBSBYYDBSA-N', E'(1R,4aR,5S,8aR)-5-[(3S)-4-carboxy-3-methylbutyl]-1,4a-dimethyl-6-methylene-3,4,5,7,8,8a-hexahydro-2H-naphthalene-1-carboxylic acid', E'IUPAC Name (CAS-like Style)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HPQKNJHVWUWAOR-QBSBYYDBSA-N', E'(1R,4aR,5S,8aR)-5-[(3S)-4-carboxy-3-methyl-butyl]-1,4a-dimethyl-6-methylene-decalin-1-carboxylic acid', E'IUPAC Name (Traditional)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HPQKNJHVWUWAOR-QBSBYYDBSA-N', E'(1R,4aR,5S,8aR)-1,4a-dimethyl-6-methylidene-5-[(3S)-3-methyl-5-oxidanyl-5-oxidanylidene-pentyl]-3,4,5,7,8,8a-hexahydro-2H-naphthalene-1-carboxylic acid', E'IUPAC Name (Systematic)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HPQKNJHVWUWAOR-QBSBYYDBSA-N', E'(1R,4aR,5S,8aR)-5-[(3S)-4-carboxy-3-methylbutyl]-1,4a-dimethyl-6-methylidene-3,4,5,7,8,8a-hexahydro-2H-naphthalene-1-carboxylic acid', E'IUPAC Name (Preferred)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'HPQKNJHVWUWAOR-QBSBYYDBSA-N', E'(1R,4aR,5S,8aR)-5-[(3S)-4-carboxy-3-methyl-butyl]-1,4a-dimethyl-6-methylene-decalin-1-carboxylic acid', E'IUPAC Name (Allowed)', E'Compound_012300001_012325000.sdf.gz');
select insert_external_id(E'HPQKNJHVWUWAOR-QBSBYYDBSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12314311', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'AOTAYHZDDHTRBZ-UHFFFAOYSA-N', E'InChI=1S/C14H20N2O2.ClH/c15-13-7-2-1-6-12(13)14(17)18-10-8-11-5-3-4-9-16-11;/h1-2,6-7,11,16H,3-5,8-10,15H2;1H', 284.7822122327681, 284.129155592, E'C14H21ClN2O2', E'Compound_012300001_012325000.sdf.gz', E'C1CCNC(C1)CCOC(=O)C2=CC=CC=C2N.Cl');
select insert_synonym(E'AOTAYHZDDHTRBZ-UHFFFAOYSA-N', E'2-aminobenzoic acid 2-(2-piperidinyl)ethyl ester;hydrochloride', E'IUPAC Name (CAS-like Style)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'AOTAYHZDDHTRBZ-UHFFFAOYSA-N', E'2-aminobenzoic acid 2-(2-piperidyl)ethyl ester;hydrochloride', E'IUPAC Name (Traditional)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'AOTAYHZDDHTRBZ-UHFFFAOYSA-N', E'2-piperidin-2-ylethyl 2-azanylbenzoate;hydrochloride', E'IUPAC Name (Systematic)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'AOTAYHZDDHTRBZ-UHFFFAOYSA-N', E'2-piperidin-2-ylethyl 2-aminobenzoate;hydrochloride', E'IUPAC Name (Preferred)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'AOTAYHZDDHTRBZ-UHFFFAOYSA-N', E'2-(2-piperidyl)ethyl 2-aminobenzoate;hydrochloride', E'IUPAC Name (Allowed)', E'Compound_012300001_012325000.sdf.gz');
select insert_external_id(E'AOTAYHZDDHTRBZ-UHFFFAOYSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12314347', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'XSUVNTHNQMGPIL-LACSLYJWSA-N', E'InChI=1S/C15H22O3/c1-9-4-5-13(2)11(6-9)18-12-7-10(16)14(13,3)15(12)8-17-15/h6,10-12,16H,4-5,7-8H2,1-3H3/t10-,11-,12-,13+,14-,15+/m1/s1', 250.3339498186953, 250.15689456399997, E'C15H22O3', E'Compound_012300001_012325000.sdf.gz', E'CC1=CC2C(CC1)(C3(C(CC(C34CO4)O2)O)C)C');
select insert_external_id(E'XSUVNTHNQMGPIL-LACSLYJWSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12315016', E'Compound_012300001_012325000.sdf.gz');

select insert_compound(E'RYKWENLBNLTGQK-UHFFFAOYSA-N', E'InChI=1S/C10H11NO2/c11-9-3-1-2-6-7(9)4-5-8(6)10(12)13/h1-3,8H,4-5,11H2,(H,12,13)', 177.20022032213836, 177.078978592, E'C10H11NO2', E'Compound_012300001_012325000.sdf.gz', E'C1CC2=C(C1C(=O)O)C=CC=C2N');
select insert_synonym(E'RYKWENLBNLTGQK-UHFFFAOYSA-N', E'4-amino-2,3-dihydro-1H-indene-1-carboxylic acid', E'IUPAC Name (CAS-like Style)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'RYKWENLBNLTGQK-UHFFFAOYSA-N', E'4-aminoindane-1-carboxylic acid', E'IUPAC Name (Traditional)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'RYKWENLBNLTGQK-UHFFFAOYSA-N', E'4-azanyl-2,3-dihydro-1H-indene-1-carboxylic acid', E'IUPAC Name (Systematic)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'RYKWENLBNLTGQK-UHFFFAOYSA-N', E'4-amino-2,3-dihydro-1H-indene-1-carboxylic acid', E'IUPAC Name (Preferred)', E'Compound_012300001_012325000.sdf.gz');
select insert_synonym(E'RYKWENLBNLTGQK-UHFFFAOYSA-N', E'4-aminoindane-1-carboxylic acid', E'IUPAC Name (Allowed)', E'Compound_012300001_012325000.sdf.gz');
select insert_external_id(E'RYKWENLBNLTGQK-UHFFFAOYSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'12318495', E'Compound_012300001_012325000.sdf.gz');

COMMIT;
