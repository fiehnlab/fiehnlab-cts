select insert_compound(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'InChI=1S/Ca/q+2', 39.96259098);
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Calcium Ion', E'Synonym');
select insert_synonym(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'CA', E'Synonym');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109869', E'11109869');
select insert_external_id(E'BHPQYMZQTOCNFJ-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38459', E'38459.6');

select insert_compound(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'InChI=1S/H2O4S/c1-5(2,3)4/h(H2,1,2,3,4)', 95.95172948000001);
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'SO4', E'Synonym');
select insert_synonym(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Sulfate Ion', E'Synonym');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109874', E'11109874');
select insert_external_id(E'QAOWNCQODCNURD-UHFFFAOYSA-N', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38469', E'38469.2');

select insert_compound(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'InChI=1S/C21H27N7O14P2/c22-17-12-19(25-7-24-17)28(8-26-12)21-16(32)14(30)11(41-21)6-39-44(36,37)42-43(34,35)38-5-10-13(29)15(31)20(40-10)27-3-1-2-9(4-27)18(23)33/h1-4,7-8,10-11,13-16,20-21,29-32H,5-6H2,(H5-,22,23,24,25,33,34,35,36,37)/p+1/t10-,11-,13-,14-,15-,16-,20-,21-/m1/s1', 635.8978459400003);
select insert_synonym(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'NAD', E'Synonym');
select insert_synonym(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'Nicotinamide-Adenine-Dinucleotide', E'Synonym');
select insert_external_id(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109875', E'11109875');
select insert_external_id(E'BAWFJGJZGIEFAR-NNYOXOHSSA-O', E'MMDB', E'http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38469', E'38469.7');

