BEGIN;
select insert_synonym(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'NSC-37274', E'Synonym');
select insert_synonym(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'NSC37274', E'Synonym');
select insert_external_id(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'DTP/NCI', E'http://dtp.nci.nih.gov/', E'37274');
select insert_external_id(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'CAS', E'http://www.cas.org/', E'81957-22-4');
select insert_external_id(E'IVVRRWKNAJCNEZ-UHFFFAOYSA-N', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'93700');
COMMIT;
