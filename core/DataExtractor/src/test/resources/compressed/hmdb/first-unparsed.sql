select insert_compound(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'InChI=1S/C37H67NO13/c1-14-25-37(10,45)30(41)20(4)27(39)18(2)16-35(8,44)32(51-34-28(40)24(38(11)12)15-19(3)47-34)21(5)29(22(6)33(43)49-25)50-26-17-36(9,46-13)31(42)23(7)48-26/h18-26,28-32,34,40-42,44-45H,14-17H2,1-13H3/t18-,19-,20+,21+,22-,23+,24+,25-,26+,28-,29+,30-,31+,32-,34+,35-,36-,37-/m1/s1', 733.9268, 733.461241235, E'C37H67NO13', E'first-unparsed.sdf.gz', E'');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'erythromycin', E'IUPAC Name (Traditional)', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'Erythrocin Stearate', E'Synonym', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'Erythromycin oxime', E'Synonym', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'EM', E'Synonym', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'Erythromycin lactobionate', E'Synonym', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'Erythromycin glucoheptonate', E'Synonym', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'Erythromycin ethylsuccinate', E'Synonym', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'Erythromycin Stearate', E'Synonym', E'first-unparsed.sdf.gz');
select insert_synonym(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'(3R,4S,5S,6R,7R,9R,11R,12R,13S,14R)-6-{[(2S,3R,4S,6R)-4-(dimethylamino)-3-hydroxy-6-methyloxan-2-yl]oxy}-14-ethyl-7,12,13-trihydroxy-4-{[(2R,4R,5S,6S)-5-hydroxy-4-methoxy-4,6-dimethyloxan-2-yl]oxy}-3,5,7,9,11,13-hexamethyl-1-oxacyclotetradecane-2,10-dione', E'IUPAC Name (Preferred)', E'first-unparsed.sdf.gz');
select insert_external_id(E'ULGZDMOVFRHVEP-RWJQBGPGSA-N', E'Human Metabolome Database', E'http://www.hmdb.ca/metabolites//HMDB14344', E'HMDB14344', E'first-unparsed.sdf.gz');

