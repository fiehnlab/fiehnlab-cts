select insert_compound(E'YOZNUFWCRFCGIH-WZHZPDAFSA-K', E'InChI=1S/C62H90N13O14P.Co.H2O/c1-29-20-39-40(21-30(29)2)75(28-70-39)57-52(84)53(41(27-76)87-57)89-90(85,86)88-31(3)26-69-49(83)18-19-59(8)37(22-46(66)80)56-62(11)61(10,25-48(68)82)36(14-17-45(65)79)51(74-62)33(5)55-60(9,24-47(67)81)34(12-15-43(63)77)38(71-55)23-42-58(6,7)35(13-16-44(64)78)50(72-42)32(4)54(59)73-56;;/h20-21,23,28,31,34-37,41,52-53,56-57,76,84H,12-19,22,24-27H2,1-11H3,(H15,63,64,65,66,67,68,69,71,72,73,74,77,78,79,80,81,82,83,85,86);;1H2/q;+3;/p-3/t31-,34-,35-,36-,37+,41-,52-,53-,56-,57+,59-,60+,61+,62+;;/m1../s1', 1346.3551, 1345.567070949, E'C62H89CoN13O15P', E'bad-mol.sdf.gz', E'');
select insert_synonym(E'YOZNUFWCRFCGIH-WZHZPDAFSA-K', E'Hydroxomin', E'Synonym', E'bad-mol.sdf.gz');
select insert_synonym(E'YOZNUFWCRFCGIH-WZHZPDAFSA-K', E'Vitamin B-12b', E'Synonym', E'bad-mol.sdf.gz');
select insert_synonym(E'YOZNUFWCRFCGIH-WZHZPDAFSA-K', E'Hydroxocobalamin', E'IUPAC Name (Preferred)', E'bad-mol.sdf.gz');
select insert_external_id(E'YOZNUFWCRFCGIH-WZHZPDAFSA-K', E'Human Metabolome Database', E'http://www.hmdb.ca/metabolites//HMDB14345', E'HMDB14345', E'bad-mol.sdf.gz');

