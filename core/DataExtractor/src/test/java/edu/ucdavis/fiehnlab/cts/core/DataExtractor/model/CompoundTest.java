/**
 *
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor.model;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.CompoundFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.patterns.PatternHelper;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author dpedrosa
 */
public class CompoundTest {

	/**
	 * @throws java.lang.Exception
	 */
	Compound c1;
	Compound c2;
	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(getClass());

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		c1 = new Compound();
		c1.setInchiCode("InChI=1S/C22H22N2O3/c1-13'-17(14-6-8-16(9-7-14')23(2)3)10-11-24-20(13)'18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H'2,1-3H3,(H,26,27)");
		c1.setInchiKey("ZVEYXXGLPNSLIT-UHFFFAOYSA-N");
		c1.setFormula("C22H22N2O3");
		c1.setMolWeight(123.4);
		c1.setExactMass(124.3);
		c1.setSourceFile("Compound_000000001_000025000.sdf");
		c1.setSmiles("CC(=O)OC1=CC=CC=C1C(=O)O");
		c1.addId(CompoundFields.PUBCHEM_CID, new IdObject(CompoundFields.PUBCHEM_CID, "21104443", CompoundFields.PUBCHEM_BASE_URL));
		c1.addName(new NameObject("IUPAC Name (Allowed)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-quinolizine-3-carboxylic acid"));
		c1.addName(new NameObject("IUPAC Name (CAS-like Style)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-3-quinolizinecarboxylic acid"));
		c1.addName(new NameObject("IUPAC Name (Preferred)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxoquinolizine-3-carboxylic acid"));
		c1.addName(new NameObject("IUPAC Name (Systematic)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxidanylidene-quinolizine-3-carboxylic acid"));
		c1.addName(new NameObject("IUPAC Name (Traditional)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid"));
		c1.addName(new NameObject(CompoundFields.SYNONYMS, "syn1"));
		c1.addName(new NameObject(CompoundFields.SYNONYMS, "syn2"));
		c1.addName(new NameObject(CompoundFields.SYNONYMS, "syn3"));


		c2 = new Compound();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link Compound#Compound()}.
	 */
	@Test
	public void testCompound() {
		/*
		inchikey = ZVEYXXGLPNSLIT-UHFFFAOYSA-N
		inchicode = InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)
		ids:
		 cid = 21104443
		names:
			IUPAC Name (Allowed)		::	1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-quinolizine-3-carboxylic acid
			IUPAC Name (CAS-like Style)	::	1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-3-quinolizinecarboxylic acid
			IUPAC Name (Preferred)		::	1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxoquinolizine-3-carboxylic acid
			IUPAC Name (Systematic)		::	1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxidanylidene-quinolizine-3-carboxylic acid
			IUPAC Name (Traditional)	::	1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid
		*/

		Compound c3 = new Compound();

		// new object not assigned an instance yet.
		assertTrue(c3 != null);

		// new object data should be empty ("" not null)
		assertTrue(c3.getInchiCode().isEmpty());
	}

	/**
	 * Test method for {@link Compound#Compound(String)}.
	 */
	@Test
	public final void testCompoundString() {
		Compound c4 = new Compound("InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)");

		// new object can't be null, but contain no data.
		assertTrue(c4 != null);

		// new object data should be empty except the InChI code
		assertTrue(c4.getInchiCode().length() > 0);
		assertTrue(c4.getInchiKey().isEmpty());
		assertTrue(c4.getIds() instanceof Map);
		assertTrue(c4.getIds().isEmpty());
		assertTrue(c4.getNames() instanceof Map);
		assertTrue(c4.getNames().isEmpty());
	}

	/**
	 * Test method for {@link Compound#getInchiKey()}.
	 */
	@Test
	public final void testGetInchiKey() {
		assertTrue(c1.getInchiKey().equals("ZVEYXXGLPNSLIT-UHFFFAOYSA-N"));
	}

	/**
	 * Test method for {@link Compound#setInchiKey(String)}.
	 */
	@Test
	public final void testSetInchiKey() {
		assertTrue(c2.getInchiKey().isEmpty());
		c2.setInchiKey("ZVEYXXGLPNSLIT-UHFFFAOYSA-N");
		assertTrue(c2.getInchiKey().equals("ZVEYXXGLPNSLIT-UHFFFAOYSA-N"));
	}

	/**
	 * Test method for {@link Compound#getInchiCode()}.
	 */
	@Test
	public final void testGetInchiCode() {
		assertTrue(c1.getInchiCode().equals("InChI=1S/C22H22N2O3/c1-13'-17(14-6-8-16(9-7-14')23(2)3)10-11-24-20(13)'18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H'2,1-3H3,(H,26,27)"));
	}

	/**
	 * Test method for {@link Compound#setInchiCode(java.lang.String)}.
	 */
	@Test
	public final void testSetInchiCode() {
		assertTrue(c2.getInchiCode().isEmpty());
		c2.setInchiCode("InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)");
		assertTrue(c2.getInchiCode().equals("InChI=1S/C22H22N2O3/c1-13-17(14-6-8-16(9-7-14)23(2)3)10-11-24-20(13)18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H2,1-3H3,(H,26,27)"));
	}

	/**
	 * Test method for {@link Compound#getIds()}.
	 */
	@Test
	public final void testGetIds() {
		assertTrue(c1.getIds() instanceof Map);
		assertTrue(c1.getIds().keySet().contains(CompoundFields.PUBCHEM_CID));

		assertTrue(c1.getIds().get(CompoundFields.PUBCHEM_CID).name.equals(CompoundFields.PUBCHEM_CID));
		assertTrue(c1.getIds().get(CompoundFields.PUBCHEM_CID).value.equals("21104443"));
		assertTrue(c1.getIds().get(CompoundFields.PUBCHEM_CID).url.equals(CompoundFields.PUBCHEM_BASE_URL));
	}

	/**
	 * Test method for {@link Compound#setIds(java.util.Map)}.
	 */
	@Test
	public final void testSetIds() {
		assertTrue(c2.getIds().isEmpty());
		c2.setIds(new HashMap<String, IdObject>());
		Map<String, IdObject> testIds = c2.getIds();
		//test c2 has a new id map
		assertTrue(testIds != null);

		// and that map is empty
		assertTrue(testIds.values().isEmpty());

		// test with real values
		testIds = new HashMap<String, IdObject>();

		testIds.put("id 1", new IdObject("type 1", "value 1", "url 1"));
		testIds.put("id 2", new IdObject("type 2", "value 2", "url 2"));
		testIds.put("id 3", new IdObject("type 3", "value 3", "url 3"));

		c2.setIds(testIds);

		assertTrue(c2.getIds().containsKey("id 1"));
		assertTrue(c2.getIds().containsKey("id 2"));
		assertTrue(c2.getIds().containsKey("id 3"));

		IdObject idVal1 = new IdObject("type 1", "value 1", "url 1");
		IdObject idVal2 = new IdObject("type 2", "value 2", "url 2");
		IdObject idVal3 = new IdObject("type 3", "value 3", "url 3");
		assertTrue(c2.getIds().containsValue(idVal1));
		assertTrue(c2.getIds().containsValue(idVal2));
		assertTrue(c2.getIds().containsValue(idVal3));
	}

	/**
	 * Test method for {@link Compound#addId(String, IdObject)}.
	 */
	@Test
	public final void testAddId() {
		c2.addId(CompoundFields.PUBCHEM_CID, new IdObject(CompoundFields.PUBCHEM_CID, "21104443", CompoundFields.PUBCHEM_BASE_URL));
		assertTrue(c2.getIds().keySet().contains(CompoundFields.PUBCHEM_CID));
		assertTrue(c2.getIds().get(CompoundFields.PUBCHEM_CID).name.equals(CompoundFields.PUBCHEM_CID));
		assertTrue(c2.getIds().get(CompoundFields.PUBCHEM_CID).value.equals("21104443"));
		assertTrue(c2.getIds().get(CompoundFields.PUBCHEM_CID).url.equals(CompoundFields.PUBCHEM_BASE_URL));
	}

	/**
	 * Test method for {@link Compound#getIdList()}.
	 */
	@Test
	public final void testGetIdList() {
		assertTrue(c1.getIdList() instanceof Collection);
		assertTrue(c1.getIdList().size() == 1);

		IdObject obj = new IdObject(CompoundFields.PUBCHEM_CID, "21104443", CompoundFields.PUBCHEM_BASE_URL);

		Collection<IdObject> x = c1.getIdList();

		IdObject o = x.iterator().next();

		assertTrue(obj.name.equals(o.name));
		assertTrue(x.contains(o));
	}

	/**
	 * Test method for {@link Compound#getNames()}.
	 */
	@Test
	public final void testGetNames() {
		assertTrue(c1.getNames() instanceof Map);
		// one "synonym" entry and 5 different "Iupac" entries
		assertTrue(c1.getNames().size() == 6);
	}

	/**
	 * Test method for {@link Compound#setNames(java.util.Map)}.
	 */
	@Test
	public final void testSetNames() {
		Map<String, Collection<NameObject>> names = new HashMap<String, Collection<NameObject>>();
		names.put("name1", new HashSet<NameObject>());
		names.get("name1").add(new NameObject("name1", "value 1"));
		names.put("name2", new HashSet<NameObject>());
		names.get("name2").add(new NameObject("name2", "value 2"));
		names.put("syn", new HashSet<NameObject>());
		names.get("syn").add(new NameObject("syn", "svalue 1"));
		names.get("syn").add(new NameObject("syn", "svalue 2"));

		c2.setNames(names);
		assertTrue(c2.getNames().containsKey("name1"));
		assertTrue(c2.getNames().containsKey("name2"));
		assertTrue(c2.getNames().containsKey("syn"));

		assertTrue(c2.getNames().get("name1").size() == 1);
		assertTrue(c2.getNames().get("syn").size() == 2);

		Collection<NameObject> objs = c2.getNames().get("syn");
		assertTrue(objs.contains(new NameObject("syn", "svalue 1")));
		assertTrue(objs.contains(new NameObject("syn", "svalue 2")));
	}

	/**
	 * Test method for {@link Compound#addName(NameObject)}.
	 */
	@Test
	public final void testAddName() {
		Map<String, Collection<NameObject>> nameMap = new HashMap<String, Collection<NameObject>>();
		nameMap = c2.getNames();

		assertTrue(nameMap.isEmpty());
		c2.addName(new NameObject("IUPAC Name (Traditional)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid"));
		c2.addName(new NameObject(CompoundFields.SYNONYMS, "syn1"));
		c2.addName(new NameObject(CompoundFields.SYNONYMS, "syn2"));

		//refresh result name map
		nameMap = c2.getNames();
		// 2 map entries: "Iupac, List[]", "Synonyms, List[]"
		assertTrue(nameMap.size() == 2);
		// one list of names for the the only key in the map
		assertTrue(nameMap.values().size() == 2);
	}

	/**
	 * Test method for {@link Compound#getIupacNames()}.
	 */
	@Test
	public final void testGetIupacNames() {
		Set<String> nameList = new HashSet<String>();
		nameList = c1.getIupacNames();
		assertTrue(nameList instanceof Collection);
		assertTrue(nameList.size() == 5);
		assertTrue(nameList.contains("1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid"));
	}

	/**
	 * Test method for {@link Compound#getSynonymNames()}.
	 */
	@Test
	public final void testGetSynonymNames() {
		Set<String> nameList = new HashSet<String>();
		nameList = c1.getSynonymNames();
		assertTrue(nameList instanceof Collection);
		assertTrue(nameList.size() == 3);
		assertTrue(nameList.contains("syn1"));
		assertTrue(nameList.contains("syn2"));
		assertTrue(nameList.contains("syn3"));
	}

	/**
	 * Test method for {@link Compound#toXML()}.
	 */
	@Test
	public final void testToXML() {
		Document doc = c1.toXML();
		assertTrue(doc instanceof Document);
		assertTrue(doc.getContentSize() > 0);
	}

	/**
	 * Test method for {@link Compound#toString()}.
	 */
	@Test
	public final void testToString() {
		String exp = new StringBuilder().append("ZVEYXXGLPNSLIT-UHFFFAOYSA-N").append(Encoder.newline)
				.append("InChI=1S/C22H22N2O3/c1-13'-17(14-6-8-16(9-7-14')23(2)3)10-11-24-20(13)'18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H'2,1-3H3,(H,26,27)").append(Encoder.newline)
				.append("C22H22N2O3").append(Encoder.newline)
				.append("123.4").append(Encoder.newline)
				.append("124.3").append(Encoder.newline)
				.append("{PubChem CID=PubChem CID: 21104443 (https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi)}").append(Encoder.newline)
				.append("[IUPAC Name (Allowed)=[1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-quinolizine-3-carboxylic acid], ")
				.append("IUPAC Name (CAS-like Style)=[1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-3-quinolizinecarboxylic acid], ")
				.append("IUPAC Name (Preferred)=[1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxoquinolizine-3-carboxylic acid], ")
				.append("IUPAC Name (Systematic)=[1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxidanylidene-quinolizine-3-carboxylic acid], ")
				.append("IUPAC Name (Traditional)=[1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid], ")
				.append("Synonyms=[syn1, syn2, syn3]]").toString();

		assertEquals(exp, c1.toString());
	}

	/**
	 * Test method for {@link Compound#equals(Object)}.
	 */
	@Test
	public final void testEquals() {
		c2.setInchiCode("InChI=1S/C22H22N2O3/c1-13'-17(14-6-8-16(9-7-14')23(2)3)10-11-24-20(13)'18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H'2,1-3H3,(H,26,27)");
		c2.setInchiKey("ZVEYXXGLPNSLIT-UHFFFAOYSA-N");
		c2.setSmiles("CC(=O)OC1=CC=CC=C1C(=O)O");
		c2.setMolWeight(123.4);
		c2.addId(CompoundFields.PUBCHEM_CID, new IdObject(CompoundFields.PUBCHEM_CID, "21104443", CompoundFields.PUBCHEM_BASE_URL));
		c2.addName(new NameObject("IUPAC Name (Allowed)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-quinolizine-3-carboxylic acid"));
		c2.addName(new NameObject("IUPAC Name (CAS-like Style)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-3-quinolizinecarboxylic acid"));
		c2.addName(new NameObject("IUPAC Name (Preferred)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxoquinolizine-3-carboxylic acid"));
		c2.addName(new NameObject("IUPAC Name (Systematic)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxidanylidene-quinolizine-3-carboxylic acid"));
		c2.addName(new NameObject("IUPAC Name (Traditional)", "1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid"));
		c2.addName(new NameObject(CompoundFields.SYNONYMS, "syn1"));
		c2.addName(new NameObject(CompoundFields.SYNONYMS, "syn2"));
		c2.addName(new NameObject(CompoundFields.SYNONYMS, "syn3"));

		assertTrue(c1.equals(c2));
	}

	/**
	 * Test method for {@link Compound#getQuery()}.
	 */
	@Test
	public void testGetQuery() {
		String result = c1.getQuery();

		StringBuilder exp = new StringBuilder();
		exp.append("select insert_compound(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'InChI=1S/C22H22N2O3/c1-13\\'-17(14-6-8-16(9-7-14\\')23(2)3)10-11-24-20(13)\\'18(15-4-5-15)12-19(21(24)25)22(26)27/h6-12,15H,4-5H\\'2,1-3H3,(H,26,27)', 123.4, 124.3, E'C22H22N2O3', E'Compound_000000001_000025000.sdf', E'CC(=O)OC1=CC=CC=C1C(=O)O');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'syn1', E'Synonyms', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'syn2', E'Synonyms', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'syn3', E'Synonyms', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-quinolizine-3-carboxylic acid', E'IUPAC Name (Allowed)', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxo-3-quinolizinecarboxylic acid', E'IUPAC Name (CAS-like Style)', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxoquinolizine-3-carboxylic acid', E'IUPAC Name (Preferred)', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'1-cyclopropyl-8-[4-(dimethylamino)phenyl]-9-methyl-4-oxidanylidene-quinolizine-3-carboxylic acid', E'IUPAC Name (Systematic)', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'1-cyclopropyl-8-[4-(dimethylamino)phenyl]-4-keto-9-methyl-quinolizine-3-carboxylic acid', E'IUPAC Name (Traditional)', E'Compound_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_external_id(E'ZVEYXXGLPNSLIT-UHFFFAOYSA-N', E'PubChem CID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi', E'21104443', E'Compound_000000001_000025000.sdf');").append(Encoder.newline);

		List<String> expLst = Arrays.asList(exp.toString().split(Encoder.newline));
		List<String> resLst = Arrays.asList(result.split(Encoder.newline));

		Collections.sort(expLst);
		Collections.sort(resLst);
		assertEquals(expLst, resLst);
	}

	/**
	 * Test method for {@link Compound#getExactMass()}.
	 */
	@Test
	public void testGetExactMass() {
		assert 124.3 == c1.getExactMass();
	}

	/**
	 * Test method for {@link Compound#setExactMass(Double)}.
	 */
	@Test
	public void testSetExactMass() {
		c2.setExactMass(123.4);

		assert 123.4 == c2.getExactMass();
	}

	/**
	 * Test method for {@link Compound#getSourceFile()}.
	 */
	@Test
	public void testGetSourceFile() {
		assertEquals("Compound_000000001_000025000.sdf", c1.getSourceFile());
	}

	/**
	 * Test method for {@link Compound#setSourceFile(String)}.
	 */
	@Test
	public void testSetSourceFile() {
		c2.setSourceFile("Compound_000000001_000025000.sdf");

		assertEquals("Compound_000000001_000025000.sdf", c2.getSourceFile());
	}

	@Test
	public void testGetSmiles() {
		assertEquals("CC(=O)OC1=CC=CC=C1C(=O)O", c1.getSmiles());
	}

	@Test
	public void testSetGoodLongSmiles() {
		Compound tmp = new Compound();

		// short smiles should be fine
		tmp.setSmiles("CH4");
		assertEquals("CH4", tmp.getSmiles());

		// smiles up to 5 chars are not tested
		tmp.setSmiles("C6H12");
		assertEquals("C6H12", tmp.getSmiles());

		// longer smiles are matches against regex
		tmp.setSmiles("CC(=O)OC1=CC=CC=C1C(=O)O");
		assertEquals("CC(=O)OC1=CC=CC=C1C(=O)O", tmp.getSmiles());
	}

	@Test
	@Ignore("matcher not working correctly for some reason")
	public void testSetBadSmiles() {
		assertTrue("CC(=O)OC1=CC=CC=C1C(=O)O".matches(PatternHelper.SMILES_PATTERN));
		assertFalse("CC(=J)OC1=CC=CC=C1C(=O)O".matches(PatternHelper.SMILES_PATTERN));

		Compound tmp = new Compound();
		tmp.setSmiles("CC(=J)OC1=CC=CC=C1C(=O)O");

		assertEquals("", tmp.getSmiles());
	}
}
