package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.interfaces.IAtomContainer;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.SubstanceFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;

/**
 * TestCase for the reading MDL mol files using one test file.
 *
 * @cdk.module test-io
 * @see org.openscience.cdk.io.MDLReader
 */
public class MyIteratingMDLReaderTest {
	Logger logger = Logger.getLogger(getClass());

	@Test
	public void testExtractFieldData() {
		String exp = "HEM" + Encoder.newline +
				"Protoporphyrin Ix Containing Fe" + Encoder.newline +
				"129048-51-7" + Encoder.newline +
				"413054_ALDRICH" + Encoder.newline +
				"C3518_SIGMA" + Encoder.newline +
				"Fe-40" + Encoder.newline +
				"12310_RIEDEL" + Encoder.newline +
				"CHEBI:18248";
		String result = "";
		
		logger.setLevel(Level.INFO);
		
		File pcfile = new File("src/test/resources/uncompressed/singleSubstance.sdf");
		
		try {
			if (pcfile.exists() && pcfile.canRead()) {
				InputStreamResource isr = new InputStreamResource(new FileInputStream(pcfile));
				MyIteratingMDLReader instance = new MyIteratingMDLReader((InputStream) isr.getRepresentation(), DefaultChemObjectBuilder.getInstance());
	
				while(instance.hasNext()) {
					IAtomContainer subst = instance.next();
					result = subst.getProperty(SubstanceFields.SYNONYM_PROPERTY).toString();
					
					logger.debug(result);
				}
			}
		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + pcfile.getAbsoluteFile());
		}
		
		assertEquals(exp, result);
	}
}
