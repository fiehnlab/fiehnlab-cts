package edu.ucdavis.fiehnlab.cts.core.DataExtractor;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public abstract class ResourceTest extends TestCase {
	
	private Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	public ResourceTest(String name) {
		super(name);
	}

	@Test
	public void testProcess() {
		Resource file = createFile();
		Resource instance = createInstance();

		logger.debug("Representation: " + file.getRepresentation());

		Resource res = instance.process(file);

		// returned resource should be valid
		assertTrue(res.isValid());

		// we don't expect that it's processed with something
		assertTrue(res.isProcessed() == false);

		// file should be processed now
		assertTrue(file.isProcessed());
	}

	protected abstract Resource createFile();

	protected abstract Resource createInstance();
}
