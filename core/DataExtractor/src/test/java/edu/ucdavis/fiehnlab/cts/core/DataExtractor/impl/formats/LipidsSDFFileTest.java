/**
 *
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

/**
 * @author dpedrosa
 */
@RunWith(JUnit4.class)
public class LipidsSDFFileTest extends TestCase {

	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() {
		logger.debug("---------------------------  TEST START  ---------------------------");
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() {
		logger.debug("---------------------------  TEST END  ---------------------------");
	}

	/**
	 * Test method for
	 * {@link PubchemSubstanceFile#internalProcessing(edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public final void internalProcessing() {
		String dest = "src/test/resources/sql-scripts/substances";
		Resource resource;

		CollectionResource<Substance> result;
		File pcfile = null;

		try {
			pcfile = new File(System.getProperty("user.dir") + "/src/test/resources/compressed/lipids/single-fa.sdf.gz");
			resource = new LipidsSDFFile(pcfile.getName());

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				logger.debug("\nresult: " + result.iterator().next() + "\n");
				// make sure it is valid
				assert result.isValid();

				// and got results
				assert !result.isEmpty();

				Substance testLipid = new Substance("FA22:5(7Z,10Z,13Z,16Z,19Z)");
				testLipid.setInchiKey("YUFFSWGQGVEMMI-JLNKQSITSA-N");
				testLipid.setInchiCode("InChI=1S/C22H34O2/c1-2-3-4-5-6-7-8-9-10-11-12-13-14-15-16-17-18-19-20-21-22(23)24/h3-4,6-7,9-10,12-13,15-16H,2,5,8,11,14,17-21H2,1H3,(H,23,24)/b4-3-,7-6-,10-9-,13-12-,16-15-");
				testLipid.setFormula("C22H34O2");
				testLipid.setMolWeight(296.23499958447474);
				testLipid.setExactMass(295.98982924);
				testLipid.addName(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, "FA22:5(7Z,10Z,13Z,16Z,19Z)"));
				testLipid.addName(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, "7Z,10Z,13Z,16Z,19Z-docosapentaenoic acid"));

				Substance res = result.iterator().next();

				assertEquals(testLipid, res);
				assertEquals(testLipid.getFormula(), res.getFormula());

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}
		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage() + " or the file can not be read.");
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}
}
