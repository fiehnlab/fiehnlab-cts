package edu.ucdavis.fiehnlab.cts.core.DataExtractor;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * test for an abstract resource
 * 
 * @author wohlgemuth
 * 
 */
public class AbstractResourceTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * checks if the processing information is linked right
	 */
	@Test
	public void testProcessResource() {

		AbstractResource a = new AbstractResource() {

			@Override
			public Object getRepresentation() {
				return "1";
			}

			@Override
			protected Resource internalProcessing(Resource resource) {
				if (resource == null) {
					return this;
				} else {
					return resource;
				}
			}

			@Override
			public String getQuery() {
				return null;
			}

            @Override
            public String getName() {
                return "";
            }

            @Override
            public void setName(String name) {}
		};

		AbstractResource b = new AbstractResource() {

			@Override
			public Object getRepresentation() {
				return "2";
			}

			@Override
			protected Resource internalProcessing(Resource resource) {
				if (resource == null) {
					return this;
				} else {
					return resource;
				}
			}

			@Override
			public String getQuery() {
				return null;
			}

            @Override
            public String getName() {
                return "";
            }

            @Override
            public void setName(String name) {}
		};

		AbstractResource c = new AbstractResource() {

			@Override
			public Object getRepresentation() {
				return "3";
			}

			@Override
			protected Resource internalProcessing(Resource resource) {
				if (resource == null) {
					return this;
				} else {
					return resource;
				}
			}

			@Override
			public String getQuery() {
				return null;
			}

            @Override
            public String getName() {
                return "";
            }

            @Override
            public void setName(String name) {}
		};

		a.process(b).process(c);

		assertTrue(a.getProcessedWith() == null);
		assertTrue(b.getProcessedWith() == a);
		assertTrue(c.getProcessedWith() == b);

	}

	/**
	 * checks if the processing information is linked right
	 */
	@Test
	public void testProcessResource2() {

		AbstractResource a = new AbstractResource() {

			@Override
			public Object getRepresentation() {
				return "1";
			}

			@Override
			protected Resource internalProcessing(Resource resource) {
				if (resource == null) {
					return this;
				} else {
					return resource;
				}
			}

			@Override
			public boolean isResourceTypeAccepted(Resource resource) {
				return false;
			}

			@Override
			public String getQuery() {
				return null;
			}

            @Override
            public String getName() {
                return "";
            }

            @Override
            public void setName(String name) {}
		};

		AbstractResource b = new AbstractResource() {

			@Override
			public Object getRepresentation() {
				return "2";
			}

			@Override
			protected Resource internalProcessing(Resource resource) {
				if (resource == null) {
					return this;
				} else {
					return resource;
				}
			}

			@Override
			public String getQuery() {
				return null;
			}

            @Override
            public String getName() {
                return "";
            }

            @Override
            public void setName(String name) {}
		};

		try {
			a.process(b);
			fail("this resource should not be able to be processed");
		} catch (Exception e) {
		}

		// this should be fine
		b.process(a);
	}

}
