package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl;

import java.io.File;

import org.junit.*;
import static org.junit.Assert.*;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.xml.JDomResource;

public class FileResourceTest {

	@Test
	public void testInternalProcessing() {

		// create file resource
		Resource resource = new FileResource(new File("src/test/resources/test.xml"));

		assertTrue(resource.isValid());

		// call process on it self should do nothing
		Resource resource2 = resource.process();

		// should be processed, even if it's the same resource
		assertTrue(resource2.isProcessed());

		// simple check that the references are equal
		assertTrue(resource == resource2);

		// cdk the file to xml
		Resource xml = new JDomResource().process(resource2);

		// xml should be a new instance, so not processed
		assertTrue(xml.isProcessed() == false);

		// should be of instance of jdom resource
		assertTrue(xml instanceof JDomResource);

		// cdk the xml to an input stream resource
		Resource stream = new InputStreamResource().process(xml);

		// xml should be processed now
		assertTrue(xml.isProcessed() == true);

		// test if this is true
		assertTrue(stream instanceof InputStreamResource);
	}

}
