/**
 *
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.IdObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.*;

/**
 * @author dpedrosa
 */
public class PubchemSubstanceFileTest {

	private final Logger logger = Logger.getLogger(this.getClass());
	Configuration config;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		logger.debug("---------------------------  TEST START  ---------------------------");
		config = new PropertiesConfiguration("testConfig.properties");
		logger.debug("Configuration: ");

		Iterator kiter = config.getKeys();
		while (kiter.hasNext()) {
			String key = (String) kiter.next();
			logger.debug(key + "= " + config.getProperty(key));
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		logger.debug("---------------------------  TEST END  ---------------------------");
	}

	/**
	 * Test the load of the config file
	 */
	@Test
	public void testConfigLoad() {
		assertNotNull(config);
		List<String> skip = new ArrayList<String>();
		Collections.addAll(skip, config.getStringArray("synonyms.skip"));
		assert 1 <= skip.size();

		StringBuffer msg = new StringBuffer("Config loaded. Synonyms to skip: ");
		for(String s : config.getStringArray("synonyms.skip")) {
			msg.append(s + ",");
		}
		logger.debug(msg.substring(0, msg.length()-1));

		// config should always contain UNK
		assert skip.contains("UNK");
	}

	/**
	 * Test for endless loop bug in generating data processing
	 * happens on flie:Substance_141375001_141400000.sdf.gz
	 * SID:141382403
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testInternalProcessingEndlessLoop() {
		String dest = "src/test/resources/sql-scripts/subst_141375001";
		Resource resource;

		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/subst_141382403.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				for(Substance s : result) {
					logger.debug("Subst: (" + s.getIds().get("Pubchem SID").value + ") -- Names: " + s.getNames());
				}

				// make sure it is valid
				assertTrue(result.isValid());

				// and got results
				assertEquals(6, result.size());

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	/**
	 * Test method for
	 * {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.PubchemSubstanceFile#internalProcessing(edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public final void testInternalProcessing() {
		String dest = "src/test/resources/sql-scripts/substances";
		Resource resource;

		CollectionResource<Substance> result;
		File pcfile = null;
		// FileOutputStream sout = null;
		logger.debug(System.getProperty("user.dir"));
		try {
			pcfile = new File("src/test/resources/compressed/substances/Substance_011100001_011125000.sdf.gz"); // (27 subst)
			// pcfile = new File("src/tet/resources/compressed/substances/Substance_144075001_144100000.sdf.gz"); // (6273 subst)
			// pcfile = new File("src/test/resources/compressed/substances/Substance_144200001_144225000.sdf.gz"); // (5936 subst)
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				// make sure it is valid
				assertTrue(result.isValid());

				// boolean toFile = false;

				File name = new File(dest + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
				if (name.exists()) {
					name.delete();
				}
				FileWriter sqlFile = new FileWriter(name);
				BufferedWriter fw = new BufferedWriter(sqlFile);

				for (Substance s : result) {
					assertFalse(s.getInchiKey().isEmpty());

					for (String e : s.getSynonymNames()) {
						assertFalse(e.isEmpty());
					}

					// boolean toFile = false;
					logger.debug("Substance: " + s);
					fw.append(s.getQuery());
					fw.append(Encoder.newline);
					fw.flush();

					/*
					 * XMLOutputter out = new XMLOutputter(Format.getPrettyFormat()); if (toFile) { File d = new
					 * File(dest); if (!d.exists()) { d.mkdir(); } File sxml = new File(d.getPath() + File.separatorChar
					 * + "substance_" + s.getInchiKey() + ".xml"); sout = new FileOutputStream(sxml);
					 * out.output(s.toXML(), sout); sout.flush(); sout.close(); assertTrue(sxml.exists());
					 * assertTrue(sxml.isFile()); assertTrue(sxml.canRead()); assertTrue(sxml.length() > 0); } else {
					 * out.output(s.toXML(), System.out); }
					 */
				}

				fw.append("COMMIT;" + Encoder.newline);
				fw.flush();
				fw.close();

				/*
				 * if (sout != null) { sout.close(); }
				 */
				Substance testSubst = new Substance("11110513");
				testSubst.setInchiKey("YZTICFPLOXKSQO-RGGAHWMASA-L");
				testSubst.addName(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, "Protoporphyrin Ix Containing Fe"));
				testSubst.addName(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, "HEM"));

				for(Substance s : result) {

					System.out.println("-----------------");
					System.out.println(s);
					System.out.println("-----------------");
				}

				assertTrue(result.contains(testSubst));

				// and got results
				assertEquals(27, result.size());
				// assertEquals(5936, result.size()); // use with 3rd file

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	/**
	 * Test method for time for
	 * {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.PubchemSubstanceFile#internalProcessing(edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource)}
	 */
	@SuppressWarnings("unchecked")
	@Test(timeout = 60000)
	@Ignore("Ignoring timeout test")
	public final void testInternalProcessingForTimeAndMemory() {
		String dest = "src/test/resources/sql-scripts/substances";
		Resource resource;

		CollectionResource<Substance> result;
		File pcfile = null;
		// FileOutputStream sout = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/Substance_160650001_160675000.sdf.gz"); // (FULL 4328 items)
			// pcfile = new File("src/test/resources/compressed/substances/Substance_053775001_053800000.sdf.gz"); // (FULL 19141 items)
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				// make sure it is valid
				assertTrue(result.isValid());

				// boolean toFile = false;

				File name = new File(dest + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
				if (name.exists()) {
					name.delete();
				}
				FileWriter sqlFile = new FileWriter(name);
				BufferedWriter fw = new BufferedWriter(sqlFile);

				fw.write("BEGIN;" + Encoder.newline);
				fw.flush();

				for (Substance s : result) {

					// very bad if inchikey is empty... this should always pass.
					assertFalse(s.getInchiKey().isEmpty());

					for (String e : s.getSynonymNames()) {
						assertFalse(e.isEmpty());
					}

					fw.append(s.getQuery());
					fw.append(Encoder.newline);
					fw.flush();
				}

				fw.append("COMMIT;" + Encoder.newline);
				fw.flush();
				fw.close();

				// and got results
				assertEquals(4328, result.size());
				// assertEquals(19141, result.size());

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage(), e);
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	@Test
	public final void testSqlScriptContent() {
		File source = new File("src/test/resources/sql-scripts");
		File script;

		assertTrue(source.exists());
		assertTrue(source.isDirectory());

		script = new File(source.getPath() + File.separatorChar + "testContent.sql");
		logger.debug("Script name: " + script.getAbsoluteFile());

		assertTrue(script.exists());

		String res = "";
		Scanner fr = null;
		String exp = "BEGIN;\n" +
				"select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Protoporphyrin Ix Containing Fe', E'Synonym');\n" +
				"select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'HEM', E'Synonym');\n" +
				"COMMIT;\n";

		try {
			fr = new Scanner(new FileInputStream(script));

			while (fr.hasNextLine()) {
				res = res.concat(fr.nextLine() + "\n");
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage(), e);
		} finally {
			fr.close();
		}

		assertEquals(exp, res);
	}

	@SuppressWarnings("unchecked")
	@Test
	public final void testSingleSubstanceInternalProcessing() {
		String dest = "src/test/resources/sql-scripts/substances";
		Resource resource;

		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/singleSubstance.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				// make sure it is valid
				assertTrue(result.isValid());

				File name = new File(dest + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
				if (name.exists()) {
					name.delete();
				}
				FileWriter sqlFile = new FileWriter(name);
				BufferedWriter fw = new BufferedWriter(sqlFile);

				for (Substance s : result) {
					assertFalse(s.getInchiKey().isEmpty());

					for (String e : s.getSynonymNames()) {
						assertFalse(e.isEmpty());
					}

//					if (logger.isDebugEnabled()) {
//						logger.debug(s);
//					}
					fw.append(s.getQuery());
					fw.append(Encoder.newline);
					fw.flush();
				}

				fw.append("COMMIT;" + Encoder.newline);
				fw.flush();
				fw.close();

				// and got results
				assertEquals(1, result.size());

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testSwitchedUrlId() {
		Resource resource;
		CollectionResource<Substance> exp = new CollectionResource<Substance>(new ArrayList<Substance>());
		Substance s;

		s = new Substance();
		s.setSid("11109868");
		s.setInchiKey("PWHULOQIROXLJO-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/Mn");
		s.setMolWeight(54.9380451);
		s.addName(new NameObject("Synonym", "Manganese (Ii) Ion"));
		s.addName(new NameObject("Synonym","MN"));
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "11109868", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109868)"));
		s.addId("MMDB", new IdObject("MMDB","38459.6", "http://www.ncbi.nlm.nih.gov/Structure/MMDB/mmdb.shtml"));

		exp.add(s);

		s = new Substance();
		s.setSid("11109869");
		s.setInchiKey("BHPQYMZQTOCNFJ-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/Ca/q+2");
		s.setMolWeight(39.96259098);
		s.addName(new NameObject("Synonym", "Calcium Ion"));
		s.addName(new NameObject("Synonym","CA"));
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "11109869", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109869)"));
		s.addId("MMDB", new IdObject("MMDB","38459.6", "http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38459"));

		exp.add(s);

		s = new Substance();
		s.setSid("11109874");
		s.setInchiKey("QAOWNCQODCNURD-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/H2O4S/c1-5(2,3)4/h(H2,1,2,3,4)");
		s.setMolWeight(95.95172948000001);
		s.addName(new NameObject("Synonym", "SO4"));
		s.addName(new NameObject("Synonym","Sulfate Ion"));
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "11109874", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109874)"));
		s.addId("MMDB", new IdObject("MMDB","38469.2", "http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38469"));

		exp.add(s);

		s = new Substance();
		s.setSid("11109875");
		s.setInchiKey("BAWFJGJZGIEFAR-NNYOXOHSSA-O");
		s.setInchiCode("InChI=1S/C21H27N7O14P2/c22-17-12-19(25-7-24-17)28(8-26-12)21-16(32)14(30)11(41-21)6-39-44(36,37)42-43(34,35)38-5-10-13(29)15(31)20(40-10)27-3-1-2-9(4-27)18(23)33/h1-4,7-8,10-11,13-16,20-21,29-32H,5-6H2,(H5-,22,23,24,25,33,34,35,36,37)/p+1/t10-,11-,13-,14-,15-,16-,20-,21-/m1/s1");
		s.setFormula("C21H28N7O14P2");
		s.setMolWeight(635.8978459400003);
		s.addName(new NameObject("Synonym","NAD"));
		s.addName(new NameObject("Synonym","Nicotinamide-Adenine-Dinucleotide"));
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "11109875", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109875)"));
		s.addId("MMDB", new IdObject("q","38469.7", "http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38469"));

		exp.add(s);

		s = new Substance();
		s.setSid("11109881");
		s.setInchiKey("QTBSBXVTEAMEQO-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)");
		s.setMolWeight(55.98982924);
		s.addName(new NameObject("Synonym","ACT"));
		s.addName(new NameObject("Synonym","Acetate Ion"));
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "11109881", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109881)"));
		s.addId("MMDB", new IdObject("MMDB","38909.3", "http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38909"));

		exp.add(s);

		s = new Substance();
		s.setSid("11109904");
		s.setInchiKey("DHPCBFMFERFZLR-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/C15H9BrO3/c16-14-11-6-5-10(18)7-12(11)15(19)13(14)8-1-3-9(17)4-2-8/h1-7,17-18H");
		s.setMolWeight(306.90308096);
		s.addName(new NameObject("Synonym","789"));
		s.addName(new NameObject("Synonym","3-Bromo-6-Hydroxy-2-(4-Hydroxyphenyl)-1h-Inden-1-One]"));
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "11109904", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11109904)"));
		s.addId("MMDB", new IdObject("MMDB","38485.5", "http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=38485"));

		exp.add(s);

		s = new Substance();
		s.setSid("218563");
		s.setInchiKey("YGDGERXTHJULBJ-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/C13H10ClN3S2/c14-11-6-12(15)17-13(16-11)18-7-9-5-8-3-1-2-4-10(8)19-9/h1-6H,7H2,(H2,15,16,17)");
		s.setMolWeight(307.8216);
		s.setFormula("C13H10ClN3S2");
		s.addName(new NameObject("Synonym","4-Pyrimidinamine, 2-[(benzo[b]thien-2-ylmethyl)thio]-6-chloro-"));
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "218563", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=218563)"));
		s.addId("NIAID", new IdObject("NIAID","122167", "http://chemdb.niaid.nih.gov/CompoundDetails.aspx?AIDSNO=122167"));

		exp.add(s);

		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/switchedSubstance.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				// make sure it is valid
				assertTrue(result.isValid());

				assert 7 == result.size();

				assertArrayEquals(exp.toArray(), result.toArray());
			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	@Test
	public void testMultiSynonyms() {
		Resource resource;
		CollectionResource<Substance> exp = new CollectionResource<Substance>(new ArrayList<Substance>());
		Substance s = new Substance();

		s.setSid("125505");
		s.setInchiKey("QNAYBMKLOCPYGJ-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/C3H7NO2/c1-2(4)3(5)6/h2H,4H2,1H3,(H,5,6)");
		s.setMolWeight(81.99290324);
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "125505", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=125505"));
		s.addId("CAS", new IdObject("CAS", "56-41-7", "http://www.cas.org/"));
		s.addId("DTP/NCI", new IdObject("Human Metabolome Database", "206315", "http://dtp.nci.nih.gov/dtpstandard/servlet/dwindex?searchtype=NSC&outputformat=html&searchlist=206315"));
		s.addName(new NameObject("Synonym", "(S)-Alanine"));
		s.addName(new NameObject("Synonym", ".alpha.-Alanine"));
		s.addName(new NameObject("Synonym", ".alpha.-Aminopropionic acid"));
		s.addName(new NameObject("Synonym", "Alanine"));
//		s.addName(new NameObject("Synonym", "Alanine; L-"));
		s.addName(new NameObject("Synonym", "L(+)-Alanine"));
		s.addName(new NameObject("Synonym", "L-.alpha.-Alanine"));
		s.addName(new NameObject("Synonym", "L-.alpha.-Aminopropionic acid"));
		s.addName(new NameObject("Synonym", "L-2-Aminopropanoic acid"));
		s.addName(new NameObject("Synonym", "L-2-Aminopropionic acid"));
		s.addName(new NameObject("Synonym", "L-Alanine"));
		s.addName(new NameObject("Synonym", "NSC-206315"));
		s.addName(new NameObject("Synonym", "NSC206315"));
		s.addName(new NameObject("Synonym", "Propanoic acid, (S)-"));
		s.addName(new NameObject("Synonym", "Propanoic acid, 2-amino-"));

		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/multi_synonym.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				Substance sub = result.iterator().next();

				// make sure it is valid
				assertTrue(result.isValid());

				assert 1 == result.size();

				assertEquals(s.getNames().size(), sub.getNames().size());

				ArrayList<String> expSyns = new ArrayList<>(s.getSynonymNames());
				Collections.sort(expSyns);

				List<String> resSyns = new ArrayList<>(sub.getSynonymNames());
				Collections.sort(resSyns);

				assertEquals(expSyns, resSyns);

				assert s.getIdList().containsAll(sub.getIdList());
				assert sub.getIdList().containsAll(s.getIdList());

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	@Test
	public void testSynonymAsId() {
		Resource resource;
		CollectionResource<Substance> exp = new CollectionResource<Substance>(new ArrayList<Substance>());
		Substance s = new Substance();

		s.setSid("103574926");
		s.setInchiKey("NBXQCRIMLNZIEK-MRXNPFEDSA-N");
		s.setInchiCode("InChI=1S/C19H20ClF2N3O4S/c1-29-17-7-2-13(11-24-17)12-25(16-10-19(21,22)8-9-23-18(16)26)30(27,28)15-5-3-14(20)4-6-15/h2-7,11,16H,8-10,12H2,1H3,(H,23,26)/t16-/m1/s1");
		s.setMolWeight(438.9266106);
		s.setFormula("C19H20ClF2N3O4S");
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "103574926", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=103574926"));
		s.addId("ChEMBL", new IdObject("ChEMBL", "CHEMBL256794", "https://www.ebi.ac.uk/chembldb/index.php/compound/inspect/CHEMBL256794"));

		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/extid_as_synonym.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				Substance sub = result.iterator().next();
				logger.trace("exp Synonyms:" + s.getSynonymNames() + "\n");
				logger.trace("res Synonyms:" + sub.getSynonymNames() + "\n");
				logger.trace("exp Ids: " + s.getIds() + "\n");
				logger.trace("res Ids: " + sub.getIds() + "\n");
				logger.trace("Query\n" + sub.getQuery() + "\n");

				// make sure it is valid
				assertTrue(result.isValid());

				assert 1 == result.size();

				assertEquals(s, sub);
				assertEquals(s.getFormula(), sub.getFormula());
			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	@Test
	public void testDupedKeggs() {
		CollectionResource<Substance> exp = new CollectionResource<Substance>(new ArrayList<Substance>());
		Substance s = new Substance();

		s.setSid("81053718");
		s.setInchiKey("AAZYJKNISGEWEV-UHFFFAOYSA-N");
		s.setInchiCode("InChI=1S/C10H7ClN2/c11-10-12-7-6-9(13-10)8-4-2-1-3-5-8/h1-7H");
		s.setMolWeight(182.97500068);

		s.addName(new NameObject("Synonym", "2-Chloro-4-phenylpyrimidine"));

		s.addId("Pubchem SID", new IdObject("Pubchem SID", "81053718", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=81053718"));
		s.addId("CAS", new IdObject("CAS", "13036-50-5", "http://www.cas.org/"));
		s.addId("Tyger Scientific", new IdObject("Tyger Scientific", "C67438", "http://www.tygersci.com/phtml/13036-50-5.htm"));


		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/duped_keggs.sdf.gz");
			Resource resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				assertTrue(result.isValid());

				logger.trace("results:" + result.size() + "\n");
				assert 3 == result.size();

				Iterator<Substance> subit = result.iterator();

				int c = 0;
				while (subit.hasNext()) {
					Substance sub = subit.next();
					logger.trace("result (" + ++c + "):\n" + sub + "\n");

					logger.trace("exp Synonyms:" + s.getSynonymNames() + "\n");
					logger.trace("res Synonyms:" + sub.getSynonymNames() + "\n");
					logger.trace("exp Ids: " + s.getIds() + "\n");
					logger.trace("res Ids: " + sub.getIds() + "\n");
					logger.trace("Query\n" + sub.getQuery() + "\n");

					// make sure it is valid

				}
				assertEquals(s, result.iterator().next());
			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	@Test
	public void testSubst126523020() {
		Resource resource;
		CollectionResource<Substance> exp = new CollectionResource<Substance>(new ArrayList<Substance>());
		Substance s = new Substance();

		s.setSid("126523020");
		s.setInchiKey("WQZGKKKJIJFFOK-GASJEMHNSA-N");
		s.setInchiCode("InChI=1S/C6H12O6/c7-1-2-3(8)4(9)5(10)6(11)12-2/h2-11H,1H2/t2-,3-,4+,5-,6?/m1/s1");
		s.setFormula("C6H12O6");
		s.setMolWeight(180.15613399205373);
		s.setExactMass(180.063388104);
		s.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "126523020", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=126523020"));
		s.addId("Human Metabolome Database", new IdObject("Human Metabolome Database", "HMDB00122", "http://hmdb.ca/metabolites/HMDB00122"));
		s.addName(new NameObject("Synonym", "CPC hydrate"));
		s.addName(new NameObject("Synonym", "Grape sugar"));
		s.addName(new NameObject("Synonym", "Glucolin"));
		s.addName(new NameObject("Synonym", "Glucose"));
		s.addName(new NameObject("Synonym", "D(+)-Glucose"));
		s.addName(new NameObject("Synonym", "Cerelose 2001"));
		s.addName(new NameObject("Synonym", "Goldsugar"));
		s.addName(new NameObject("Synonym", "Roferose ST"));
		s.addName(new NameObject("Synonym", "Corn sugar"));
		s.addName(new NameObject("Synonym", "Meritose"));
		s.addName(new NameObject("Synonym", "Cerelose"));
		s.addName(new NameObject("Synonym", "Vadex"));
		s.addName(new NameObject("Synonym", "Clintose L"));
		s.addName(new NameObject("Synonym", "Staleydex 95M"));
		s.addName(new NameObject("Synonym", "(+)-Glucose"));
		s.addName(new NameObject("Synonym", "Tabfine 097(HS)"));
		s.addName(new NameObject("Synonym", "Clearsweet 95"));
		s.addName(new NameObject("Synonym", "D-Glucose"));
		s.addName(new NameObject("Synonym", "Glucodin"));
		s.addName(new NameObject("Synonym", "Dextropur"));
		s.addName(new NameObject("Synonym", "Staleydex 111"));
		s.addName(new NameObject("Synonym", "Dextrose"));
		s.addName(new NameObject("Synonym", "Dextrosol"));
		s.addName(new NameObject("Synonym", "Anhydrous dextrose"));

		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/subst_126523020.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				Substance sub = result.iterator().next();

				List<String> expNames = new ArrayList<>(s.getSynonymNames());
				Collections.sort(expNames);

				List<String> resNames = new ArrayList<>(sub.getSynonymNames());
				Collections.sort(resNames);

				assertEquals(expNames, resNames);
				assert s.getIdList().containsAll(sub.getIdList());
				assert sub.getIdList().containsAll(s.getIdList());

				assertEquals(s.getExactMass(), sub.getExactMass(), 0.000001);
				assertEquals(s.getFormula(), sub.getFormula());
				assertEquals(s.getMolWeight(), sub.getMolWeight(), 0.000001);
				assertEquals(s.getInchiCode(), sub.getInchiCode());
				assertEquals(s.getInchiKey(), sub.getInchiKey());
				assertEquals(s.getSid(), sub.getSid());

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}
	}

	@Test
	public void testSubstWeirdBehavior() {
		Resource resource;
		Substance s = new Substance("126500001");
		s.setInchiKey("ZRVOCNOYAJIAAP-CSKARUKUSA-N");
		s.setInchiCode("InChI=1S/C11H22O2Si/c1-9(12)8-10(2)13-14(6,7)11(3,4)5/h8H,1-7H3/b10-8+");
		s.setFormula("C11H22O2Si");
		s.setExactMass(191.96675577000002);
		s.setMolWeight(192.20228947474007);
		s.addName(new NameObject("Synonym", "4-(TERT-BUTYLDIMETHYLSILOXY)-3-PENTEN-2-ONE"));
		s.addId("Angene International", new IdObject("Angene International", "AG-G-70012", ""));
		s.addId("CAS", new IdObject("CAS", "69404-97-3", "http://www.cas.org/"));
		s.addId("Pubchem SID", new IdObject("Pubchem SID", "126500001", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=126500001"));

		CollectionResource<Substance> result;
		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/substances/subst_weird-behaviour.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Substance>) resource.process(pcxml);

				Substance sub = result.iterator().next();
				logger.trace("\nFound substance:\n" + sub.toString() + "\n");

				assert sub.getIds().values().containsAll(s.getIds().values());
				assert s.getIds().values().containsAll(sub.getIds().values());

				assertEquals(s, sub);

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
			fail();
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
			fail();
		}
	}

	@Test
	public void testGetDataSourceName() {
		PubchemSubstanceFile resource;

		try {
			File pcfile = new File("src/test/resources/compressed/substances/subst_weird-behaviour.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			assertEquals("Human Metabolome Database", resource.getDataSourceName("811", "126523020"));
		} catch (Exception e) {
			logger.error("testGetDataSource found error: " + e.getMessage());
		}
	}

	@Test
	public void testFile291325001_291350000() {
		Resource resource;

		try {
			File pcfile = new File("src/test/resources/compressed/substances/Substance_291325001_291350000.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
			InputStreamResource pcxml = new CompressedResource(gzfile);
			CollectionResource<Substance> result = (CollectionResource<Substance>) resource.process(pcxml);
		} catch (Exception e) {
			logger.debug("ERROR:\n" + e.getMessage());
		}
	}

	@Test
	public void testGetDepositorNameFromPubchem() {
		String type = "substance";
		String dsid = "811";

		PubchemSubstanceFile resource;
		String res = "";

		try {
			File pcfile = new File("src/test/resources/compressed/substances/subst_weird-behaviour.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			CloseableHttpClient client = HttpClients.createDefault();

			res = resource.getDepositorNameFromPubchem(type, dsid, client);

			client.close();
		} catch (Exception e) {
			logger.error("ERROR:\n" + e.getMessage());
			e.printStackTrace();
		}

		assertEquals("MOLI", res);
	}

	@Test
	public void testGetDepositorNameFromPubchem2() {
		String type = "substance";
		String dsid = "11831";

		PubchemSubstanceFile resource;
		String res = "";

		try {
			File pcfile = new File("src/test/resources/compressed/substances/subst_weird-behaviour.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			CloseableHttpClient client = HttpClients.createDefault();
			res = resource.getDepositorNameFromPubchem(type, dsid, client);
			client.close();
		} catch (Exception e) {
			logger.error("ERROR:\n" + e.getMessage());
		}

		assertEquals("KEGG", res);
	}

	@Test
	public void testGetDepositorNameForSubstanceWithWeirdDepositor() {
		String type = "substance";
		String dsid = "10590";
		String sid = "223900001";

		PubchemSubstanceFile resource;
		String res2 = "";

		try {
			File pcfile = new File("src/test/resources/compressed/substances/Substance_weirdDepositor.sdf.gz");
			resource = new PubchemSubstanceFile(pcfile.getName(), config);

			res2 = resource.getDataSourceName(dsid,sid);
		} catch (Exception e) {
			logger.error("ERROR:\n" + e.getMessage() + "\n" + e.getCause() + "\n" + e.getClass().getName());
		}

		assertEquals("NextMove Software", res2);
	}
}
