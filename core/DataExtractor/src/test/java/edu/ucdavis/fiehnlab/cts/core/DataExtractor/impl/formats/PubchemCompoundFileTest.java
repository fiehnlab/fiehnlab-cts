package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Compound;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.IdObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.NameObject;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.*;

public class PubchemCompoundFileTest {

	Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() {
		logger.setLevel(Level.DEBUG);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testInternalProcessing() {
		final int TRANSACTION_BLOCK = 100;
		String dest = "src/test/resources/sql-scripts/compounds";

		CollectionResource<Compound> result;

		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/compounds/Compound_012300001_012325000.sdf.gz");
			//pcfile = new File("src/test/resources/compressed/compounds/Compound_016200001_016225000.sdf.gz"); // use for whole file testing
			Resource resource = new PubchemCompoundFile(pcfile.getName());

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcsdf = new CompressedResource(gzfile);
				result = (CollectionResource<Compound>) resource.process(pcsdf);

				File name = new File(dest + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
				if (name.exists()) {
					name.delete();
				}
				FileWriter sqlFile = new FileWriter(name);
				BufferedWriter fw = new BufferedWriter(sqlFile);

				// make sure it is valid
				assertTrue(result.isValid());

				// and got results
				assertTrue(0 < result.size());

				Compound testComp = new Compound("InChI=1S/C30H50O/c1-25(2)13-14-27(5)15-17-29(7)22-11-9-20-21(10-12-24(31)26(20,3)4)28(22,6)16-18-30(29,8)23(27)19-25/h9,21-24,31H,10-19H2,1-8H3/t21-,22+,23-,24-,27-,28+,29-,30+/m1/s1");
				testComp.setInchiKey("HFSACQSILLSUII-VXRRTDEQSA-N");
				testComp.setMolWeight(426.7174);
				testComp.setExactMass(426.386165);
				testComp.setFormula("C30H50O");
				testComp.setSmiles("CC1(CCC2(CCC3(C4CC=C5C(C4(CCC3(C2C1)C)C)CCC(C5(C)C)O)C)C)C");
				testComp.addId(CompoundFields.PUBCHEM_CID, new IdObject(CompoundFields.PUBCHEM_CID, "12309496", CompoundFields.PUBCHEM_BASE_URL));
				testComp.addName(new NameObject("IUPAC Name (CAS-like Style)", "(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol"));
				testComp.addName(new NameObject("IUPAC Name (Traditional)", "(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol"));
				testComp.addName(new NameObject("IUPAC Name (Systematic)", "(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol"));
				testComp.addName(new NameObject("IUPAC Name (Preferred)", "(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol"));
				testComp.addName(new NameObject("IUPAC Name (Allowed)", "(3R,6aS,6aS,6bR,8aR,12aR,14aR,14bS)-4,4,6a,6b,8a,11,11,14a-octamethyl-1,2,3,6,6a,7,8,9,10,12,12a,13,14,14b-tetradecahydropicen-3-ol"));

				Compound res = null;
				Iterator<Compound> iter = result.iterator();
				while(iter.hasNext()) {
					Compound c = iter.next();
					if(c.getInchiKey().equals(testComp.getInchiKey())) {
						res = c;
						break;
					}
				}

				assertEquals(testComp.getInchiCode(), res.getInchiCode());
				assertEquals(testComp.getFormula(), res.getFormula());
				assertEquals(testComp.getMolWeight(), res.getMolWeight(), 0.05);
				assertEquals(testComp.getExactMass(), res.getExactMass(), 0.00001);

				// initialize file transaction
				fw.write("BEGIN;" + Encoder.newline);
				fw.flush();

				int count = 0;

				for (Compound c : result) {
					if(TRANSACTION_BLOCK == count) {
						logger.trace("new transaction block");
						fw.append("COMMIT;" + Encoder.newline + Encoder.newline);
						fw.write("BEGIN;" + Encoder.newline);
						fw.flush();
					}

					assertFalse(c.getInchiKey().isEmpty());

					for (String e : c.getIupacNames()) {
						assertFalse(e.isEmpty());
					}

					// boolean toFile = false;

					fw.append(c.getQuery());
					fw.append(Encoder.newline);
					fw.flush();
					if (logger.isDebugEnabled()) {
						logger.debug("writing compound " + c.getInchiKey() + " to file.");
					}

				}

				fw.append("COMMIT;" + Encoder.newline);
				fw.flush();
				fw.close();

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		}

	}

	@SuppressWarnings("unchecked")
	@Ignore("The full sdf might takes longer than a minute to process...")
	@Test(timeout = 60000)
	public void testLongRunForTimeAndMemory() {
		long startTime = System.nanoTime();
		final int TRANSACTION_BLOCK = 100;
		String dest = "src/test/resources/sql-scripts/compounds";

		CollectionResource<Compound> result;
		File pcfile = null;

		try {
			pcfile = new File("src/test/resources/compressed/compounds/Compound_016200001_016225000.sdf.gz");
			Resource resource = new PubchemCompoundFile(pcfile.getName());

			if (pcfile.exists() && pcfile.canRead()) {
				GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcfile));
				InputStreamResource pcxml = new CompressedResource(gzfile);
				result = (CollectionResource<Compound>) resource.process(pcxml);

				// make sure it is valid
				assert result.isValid();

				// and got reasults
				assert 23000 == result.size();

				for (Compound c : result) {
					assertFalse(c.getInchiCode().isEmpty());

					for (String e : c.getIupacNames()) {
						assertFalse(e.isEmpty());
					}
				}

				// writing compounds to sql file
				File name = new File(dest + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
				if (name.exists()) {
					name.delete();
				}
				FileWriter sqlFile = new FileWriter(name);
				BufferedWriter fw = new BufferedWriter(sqlFile);
				// initialize file transaction
				fw.write("BEGIN;" + Encoder.newline);
				fw.flush();

				int count = 0;

				for (Compound c : result) {
					if(TRANSACTION_BLOCK == count) {
						logger.trace("new transaction block");
						fw.append("COMMIT;" + Encoder.newline + Encoder.newline);
						fw.write("BEGIN;" + Encoder.newline);
						fw.flush();
					}

					assertFalse(c.getInchiKey().isEmpty());

					for (String e : c.getIupacNames()) {
						assertFalse(e.isEmpty());
					}

					// boolean toFile = false;

					fw.append(c.getQuery());
					fw.append(Encoder.newline);
					fw.flush();
					if (logger.isDebugEnabled()) {
						logger.debug("writing compound " + c.getInchiKey() + " file.");
					}

				}

				fw.append("COMMIT;" + Encoder.newline);
				fw.flush();
				fw.close();

			} else {
				throw new FileNotFoundException(pcfile.getPath());
			}

		} catch (FileNotFoundException e) {
			logger.error("Can't find the file: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Something bad happened: ", e);
		} finally {
			long endTime = System.nanoTime();
			logger.info("time estimate: " + String.valueOf(endTime - startTime));
		}
	}
}
