package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;


/**
 * Created with IntelliJ IDEA.
 * User: diego
 * Date: 10/25/13
 * Time: 1:27 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated
@Ignore(value = "Deprecated main Class")
public class TextNameExportTest extends TestCase {
	TextNameExport app;
	String file = "src/test/resources/pubchem/test"; //Substance_000050001_000075000.sdf.gz
	FileWriter writer;
	private Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() throws Exception {
		File output = new File("testNameList.txt");
		if(output.exists()) {
			output.delete();
			output.createNewFile();
		}
		writer = new FileWriter(output);
		app = new TextNameExport();
	}

	@After
	public void tearDown() throws Exception {
		writer.close();
	}

	@Test
	public void testWriteNames() throws Exception {
		File inputDir = new File(file);
		Integer fileCount = 0;


		if (inputDir.exists() && inputDir.isDirectory()) {
			Resource pcProcessor = null;
			CollectionResource<String> res = null;

			File[] fileList = inputDir.listFiles();

			for (File pcFile : fileList) {
				logger.debug("Processing file: " + pcFile.getName());
				try {
					pcProcessor = new TextNameExport();

					GZIPInputStream gzfile = new GZIPInputStream(new FileInputStream(pcFile));
					InputStreamResource pcxml = new CompressedResource(gzfile);

					res = (CollectionResource<String>) pcProcessor.process(pcxml);

					Iterator it = res.iterator();
					while(it.hasNext()) {
						String line = it.next().toString();
						writer.append(line);
					}
					writer.flush();
				} catch (FileNotFoundException e) {
					System.out.println("Can't find the file: " + pcFile.getAbsolutePath() + "\n" + e);
				} catch (IOException e) {
					System.out.println("Something bad happened trying to read the file: " + pcFile.getName() + "\n" + e);
				}

				fileCount++;
			}
		} else {
			System.out.println("Invalid input directory.");
			assert false;
		}

		logger.debug(TextNameExport.nameCount + " names in " + TextNameExport.inchiCount + " inchikeys in " + fileCount + " file/s.");
	}
}
