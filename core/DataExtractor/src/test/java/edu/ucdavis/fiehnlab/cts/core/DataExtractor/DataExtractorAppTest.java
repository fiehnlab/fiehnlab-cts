/**
 *
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor;

import junit.framework.TestCase;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

/**
 * @author dpedrosa
 */
@RunWith(JUnit4.class)
public class DataExtractorAppTest extends TestCase {

	DataExtractorApp app;
	private Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() {
		app = new DataExtractorApp();

		try {
			app.config = new PropertiesConfiguration("testConfig.properties");
		} catch (ConfigurationException e) {
			logger.error("error loading config file: " + e.getMessage());
		}
	}

	@After
	public void tearDown() {
		app = null;
	}

	@Test
	public void processSmallCompoundFile() {
		app.setResource("compound");

		File infile = new File("src/test/resources/compressed/compounds/Compound_012300001_012325000.sdf.gz");	// small file
		String dest = "src/test/resources/sql-scripts/compounds";

		Long start = System.currentTimeMillis();
		String res = app.processPubchemFile(infile, dest);
		Double delta = (System.currentTimeMillis() - start) / 1000.0;

		logger.info("SQL generation took: " + delta + " seconds.\n");

		assertNotNull(res);
		assertTrue(res.contains("11 resources."));

	}

	@Test
	public final void hasValidArgs() {
		String[] args = { "-r", "compound", "src/test/resources/compressed/compounds/Compound_016200001_016225000.sdf.gz", "src/test/resources/sql-scripts/compounds" };
		Boolean result = false;

		try {
			result = app.hasValidArgs(args);
		} catch(FileNotFoundException | InputMismatchException ex) {
			logger.error(ex.getMessage());
		}

		assertTrue(result);
		assertTrue(app.getInFile().equals("src/test/resources/compressed/compounds/Compound_016200001_016225000.sdf.gz"));
		assertTrue(app.getOutDir().equals("src/test/resources/sql-scripts/compounds"));

		String[] args1 = { "-r", "lipid", "src/test/resources/compressed/lipids/single-fa.sdf.gz", "src/test/resources/sql-scripts/lipids" };
		result = false;

		try {
			result = app.hasValidArgs(args1);
		} catch(FileNotFoundException | InputMismatchException ex) {
			logger.error(ex.getMessage());
		}

		assertTrue(result);
		assertTrue(app.getInFile().equals("src/test/resources/compressed/lipids/single-fa.sdf.gz"));
		assertTrue(app.getOutDir().equals("src/test/resources/sql-scripts/lipids"));
	}

	@Test
	public void processSmallSubstanceFile() {
		app.setResource("substance");

		File infile = new File("src/test/resources/compressed/substances/Substance_011100001_011125000.sdf.gz");	// small file

		String dest = "src/test/resources/sql-scripts/substances/";
		String res = process(infile, dest);

		assertNotNull(res);
		assertTrue(res.contains("27 resources."));
	}

	@Test(expected = FileNotFoundException.class)
	public final void processBadFile() throws Exception {
		String[] args = { "-r", "compound", "filename", "directory" };
		// 2 arguments, sholb be fine
		app.hasValidArgs(args);
	}

	@Test(expected=InputMismatchException.class)
	public final void processNullArgs() throws Exception {
		// no args, should return false
		app.hasValidArgs(null);
	}

	@Test(expected=InputMismatchException.class)
	public final void processBadArgs() throws Exception {
		String args[] = {"dsad"};

		// 1 arg, still false
		app.hasValidArgs(args);
	}

	@Test
	public void processBigCompoundFile() {
		app.setResource("compound");

		File infile = new File("src/test/resources/compressed/compounds/Compound_016200001_016225000.sdf.gz");	// real file
		String dest = "src/test/resources/sql-scripts/compounds";

		Long start = System.currentTimeMillis();
		String res = process(infile, dest);

		assertNotNull(res);
		assertTrue(res.contains("resources."));

	}

	@Test
	public void processBigSubstanceFile() {
		app.setResource("substance");

		File infile = new File("src/test/resources/compressed/substances/Substance_160650001_160675000.sdf.gz");	// real file

		String dest = "src/test/resources/sql-scripts/substances";
		String res;

		res = process(infile, dest);

		assertNotNull(res);
		assertTrue(res.contains("resources."));
	}

	@Test
	public void generateSqlScript() {
		app.setResource("substance");

		File infile = new File(this.getClass().getResource("/compressed/substances/subst_126523020.sdf.gz").getFile());

		String dest = "src/test/resources/sql-scripts/substances/";
		String res = "";

		try {
			Long start = System.currentTimeMillis();
			res = app.processPubchemFile(infile, dest);
			logger.info("RESULTS: " + res);
			Double delta = (System.currentTimeMillis() - start) / 1000.0;

			logger.info("SQL generation took: " + delta + " seconds.\n");
		} catch (NullPointerException e) {
			logger.error("Somthing went wrong..." + e.getMessage(), e);
		}

		File script = new File(this.getClass().getResource("/sql-scripts/substances/subst_126523020.sql").getFile());

		assertTrue(script.exists());
		assertTrue(script.length() > 100);

		String exp2 = "select insert_compound(E'WQZGKKKJIJFFOK-GASJEMHNSA-N', E'InChI=1S/C6H12O6/c7-1-2-3(8)4(9)5(10)6(11)12-2/h2-11H,1H2/t2-,3-,4+,5-,6?/m1/s1', 180.15613399205373, 180.06338810399998, E'C6H12O6', E'subst_126523020.sdf.gz');";
		String exp3 = "select insert_synonym(E'WQZGKKKJIJFFOK-GASJEMHNSA-N', E'Roferose ST', E'Synonym', E'subst_126523020.sdf.gz');";

		List<String> lines = new ArrayList<>();
		try {
			for(String line : Files.readAllLines(Paths.get(script.getPath()), StandardCharsets.UTF_8)) {
				lines.add(line.replaceAll("\\r?\\n", "\n"));
			}
		} catch (IOException e) {
			fail();
		}

		logger.debug("File data:");
		for (String line : lines) {
			logger.debug(line);
		}

		assertEquals("BEGIN;".replaceAll("\\r?\\n", "\n"), lines.get(0).replaceAll("\\r?\\n", "\n"));
		assertEquals("COMMIT;".replaceAll("\\r?\\n", "\n"), lines.get(lines.size() - 1).replaceAll("\\r?\\n", "\n"));
		assertTrue(lines.contains(exp2.replaceAll("\\r?\\n", "\n")));
		assertTrue(lines.contains(exp3.replaceAll("\\r?\\n", "\n")));

		assertNotNull(res);
		assertTrue(res.contains("resources."));
	}

	@Test
	public void processLipidFile() {
		app.setResource("lipid");

		File infile = new File("src/test/resources/compressed/lipids/MGStructures.sdf.gz");	// real file

		String dest = "src/test/resources/sql-scripts/lipids";
		String res;

		res = process(infile, dest);

		assertNotNull(res);
		assertTrue(res.contains("resources."));
	}

	private String process(File infile, String dest) {
		String res = "";
		try {
			Long start = System.currentTimeMillis();
			res = app.processPubchemFile(infile, dest);
			Double delta = (System.currentTimeMillis() - start) / 1000.0;

			logger.info("SQL generation took: " + delta + " seconds.\n");
		} catch (NullPointerException e) {
			logger.error("Somthing went wrong..." + e.getMessage(), e);
		}

		return res;
	}
}
