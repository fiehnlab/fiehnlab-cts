package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import static org.junit.Assert.*;


public class CompressedResourceTest {

	@Test
	public void testInternalProcessing() throws IOException {
		
		//create compressed file resource
		Resource resource = new CompressedResource(new File("src/test/resources/compressed/test.xml.gz"));
		
		assertTrue(resource.isValid());
		
		Resource resource2 = resource.process();
		
		assertTrue(resource == resource2);
		
		assertTrue(resource2.isProcessed());

		// cdk the xml to an input stream resource
		Resource stream = new InputStreamResource().process(resource);

		// xml should be processed now
		assertTrue(resource.isProcessed() == true);

		// test if this is true
		assertTrue(stream instanceof InputStreamResource);
	}
}
