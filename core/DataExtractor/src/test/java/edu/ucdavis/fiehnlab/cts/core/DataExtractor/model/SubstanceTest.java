/**
 *
 */
package edu.ucdavis.fiehnlab.cts.core.DataExtractor.model;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.CompoundFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.CtsXmlFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats.SubstanceFields;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.utils.encoding.Encoder;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * @author dpedrosa
 */
public class SubstanceTest {

	private Logger logger = Logger.getLogger(getClass());
	Substance s1;
	Substance s2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		s1 = new Substance();

		s1.setInchiKey("YZTICFPLOXKSQO-RGGAHWMASA-L");
		s1.setSid("11110513");
		s1.setFormula("C34H36FeN4O4");
		s1.setMolWeight(620.51904);
		s1.setExactMass(123.4);
		s1.setSourceFile("Substance_000000001_000025000.sdf");
		s1.addName(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, "Protoporphyrin Ix Containing Fe"));
		s1.addName(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, "HEM"));
		s1.addName(new NameObject(CtsXmlFields.CTSXML_SYNONYM_ELE, "syn3"));
		s1.addId(SubstanceFields.PUBCHEM_SID, new IdObject(SubstanceFields.PUBCHEM_SID, "11110513", SubstanceFields.PUBCHEM_BASE_URL.concat("11110513")));
		s1.addId("MMDB", new IdObject("MMDB", "0513", ""));

		s2 = new Substance();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#Substance()}.
	 */
	@Test
	public final void testSubstance() {
		/*
		 * Substance Sid: 11110513 InChIKey: YZTICFPLOXKSQO-RGGAHWMASA-L Names: Name: Type: Synonym Value:
		 * Protoporphyrin Ix Containing Fe Name: Type: Synonym Value: HEM Name: Type: Synonym Value: syn3 Ids: Id: Name:
		 * Pubchem SID Value: 11110513 Url: https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi
		 */

		Substance s3 = new Substance();

		// new object not assigned an instance yet.
		assertTrue(s3 != null);

		// new object data should be empty ("" not null)
		assertTrue(s3.getSid().isEmpty());

	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#Substance(java.lang.String)}.
	 */
	@Test
	public final void testSubstanceString() {
		Substance s4 = new Substance("YZTICFPLOXKSQO-RGGAHWMASA-L");

		// new object can't be null.
		assertTrue(s4 != null);

		// new object data should be empty except the InChI key
		assertTrue(s4.getSid().length() > 0);
		assertTrue(s4.getInchiKey().isEmpty());
		assertTrue(s4.getIds() != null);
		assertTrue(s4.getIds().isEmpty());
		assertTrue(s4.getNames() != null);
		assertTrue(s4.getNames().isEmpty());
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#getSid()}.
	 */
	@Test
	public final void testGetSid() {
		assertTrue("11110513".equals(s1.getSid()));
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#setSid(java.lang.String)}.
	 */
	@Test
	public final void testSetSid() {
		assertTrue(s2.getSid().isEmpty());
		s2.setSid("11110513");
		assertTrue("11110513".equals(s2.getSid()));
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#getInchiKey()}.
	 */
	@Test
	public final void testGetInchiKey() {
		assertTrue("YZTICFPLOXKSQO-RGGAHWMASA-L".equals(s1.getInchiKey()));
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#setInchiKey(java.lang.String)}
	 * .
	 */
	@Test
	public final void testSetInchiKey() {
		assertTrue(s2.getInchiKey().isEmpty());
		s2.setInchiKey("YZTICFPLOXKSQO-RGGAHWMASA-L");
		assertTrue("YZTICFPLOXKSQO-RGGAHWMASA-L".equals(s2.getInchiKey()));
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#getNames()}.
	 */
	@Test
	public final void testGetNames() {
		assertTrue(s1.getNames() != null);
		// one "synonyms" entries
		assertTrue(s1.getNames().size() == 1);
		// three entries in "synonyms" map element
		assertTrue(s1.getNames().get("Synonyms").size() == 3);

		assertTrue(s2.getNames().size() == 0);
//		assertTrue(s2.getNames().get("Synonyms").size() == 0);
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#getSynonymNames()}.
	 */
	@Test
	public final void testGetSynonymNames() {
		Set<String> res = s1.getSynonymNames();

		assertFalse(res.isEmpty());

		Set<String> exp = new HashSet<String>();
		exp.add("Protoporphyrin Ix Containing Fe");
		exp.add("syn3");
		exp.add("HEM");

		assertEquals(exp, res);
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#setNames(java.util.Map)}.
	 */
	@Test
	public final void testSetNames() {
		Map<String, Collection<NameObject>> names = new HashMap<String, Collection<NameObject>>();
		names.put("name1", new HashSet<NameObject>());
		names.get("name1").add(new NameObject("name1", "value 1"));
		names.put("name2", new HashSet<NameObject>());
		names.get("name2").add(new NameObject("name2", "value 2"));
		names.put("syn", new HashSet<NameObject>());
		names.get("syn").add(new NameObject("syn", "svalue 1"));
		names.get("syn").add(new NameObject("syn", "svalue 2"));

		s2.setNames(names);
		assertTrue(s2.getNames().containsKey("name1"));
		assertTrue(s2.getNames().containsKey("name2"));
		assertTrue(s2.getNames().containsKey("syn"));

		assertTrue(s2.getNames().get("name1").size() == 1);
		assertTrue(s2.getNames().get("syn").size() == 2);

		Collection<NameObject> objs = s2.getNames().get("syn");
		assertTrue(objs.contains(new NameObject("syn", "svalue 1")));
		assertTrue(objs.contains(new NameObject("syn", "svalue 2")));
	}

	/**
	 * Test method for
	 * {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#addName(NameObject)}.
	 */
	@Test
	public final void testAddName() {
		Map<String, Collection<NameObject>> nameMap = new HashMap<String, Collection<NameObject>>();
		nameMap = s2.getNames();

		assertTrue(nameMap.isEmpty());
		s2.addName(new NameObject(CompoundFields.SYNONYMS, "syn3"));
		s2.addName(new NameObject(CompoundFields.SYNONYMS, "syn1"));
		s2.addName(new NameObject(CompoundFields.SYNONYMS, "syn2"));

		// refresh result name map
		nameMap = s2.getNames();
		// 1 map entry: "Synonyms, List[]"
		assertTrue(nameMap.size() == 1);
		// one list of names for the the only key in the map
		assertTrue(nameMap.values().size() == 1);
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#getIds()}.
	 */
	@Test
	public final void testGetIds() {
		assertTrue(s1.getIds() instanceof HashMap);
		assertTrue(s1.getIds().keySet().contains(SubstanceFields.PUBCHEM_SID));

		assertTrue(s1.getIds().get(SubstanceFields.PUBCHEM_SID).name.equals(SubstanceFields.PUBCHEM_SID));
		assertTrue(s1.getIds().get(SubstanceFields.PUBCHEM_SID).value.equals("11110513"));
		assertTrue(s1.getIds().get(SubstanceFields.PUBCHEM_SID).url.equals("https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110513"));
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#setIds(java.util.Map)}.
	 */
	@Test
	public final void testSetIds() {
		assertTrue(s2.getIds().isEmpty());
		s2.setIds(new HashMap<String, IdObject>());
		Map<String, IdObject> testIds = s2.getIds();

		// test s2 has a new id map
		assertTrue(testIds != null);

		// and that map is empty
		assertTrue(testIds.values().isEmpty());

		// test with real values
		testIds = new HashMap<String, IdObject>();

		testIds.put("id 1", new IdObject("type 1", "value 1", "url 1"));
		testIds.put("id 2", new IdObject("type 2", "value 2", "url 2"));
		testIds.put("id 3", new IdObject("type 3", "value 3", "url 3"));

		s2.setIds(testIds);

		assertTrue(s2.getIds().containsKey("id 1"));
		assertTrue(s2.getIds().containsKey("id 2"));
		assertTrue(s2.getIds().containsKey("id 3"));

		IdObject idVal1 = new IdObject("type 1", "value 1", "url 1");
		IdObject idVal2 = new IdObject("type 2", "value 2", "url 2");
		IdObject idVal3 = new IdObject("type 3", "value 3", "url 3");
		assertTrue(s2.getIds().containsValue(idVal1));
		assertTrue(s2.getIds().containsValue(idVal2));
		assertTrue(s2.getIds().containsValue(idVal3));
	}

	/**
	 * Test method for
	 * {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#addId(java.lang.String, IdObject)}.
	 */
	@Test
	public final void testAddId() {
		// ids should be empty
		assertTrue(s2.getIds().isEmpty());

		s2.addId("id 1", new IdObject("Id 1", "111", "http://localhost"));

		// now it should have 1 map item
		assertTrue(s2.getIds().size() == 1);
		// with key 'id 1'
		assertTrue(s2.getIds().containsKey("id 1"));
		// and value [IdObject] 'Id 1', '111', 'http://localhost'
		assertTrue(s2.getIds().containsValue(new IdObject("Id 1", "111", "http://localhost")));
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#getIdList()}.
	 */
	@Test
	public final void testGetIdList() {
		ArrayList<IdObject> res = s1.getIdList();
		assertFalse(res.isEmpty());

		ArrayList<IdObject> exp = new ArrayList<IdObject>();
		exp.add(new IdObject("Pubchem SID", "11110513", "https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110513"));
		exp.add(new IdObject("MMDB", "0513", ""));

		assertEquals(exp, res);
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#getQuery()}.
	 */
	@Test
	public final void testGetQuery() {
		String res = s1.getQuery();

		StringBuilder exp = new StringBuilder();
		exp.append("select insert_compound(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'', 620.51904, 123.4, E'C34H36FeN4O4', E'Substance_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'HEM', E'Synonym', E'Substance_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Protoporphyrin Ix Containing Fe', E'Synonym', E'Substance_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_synonym(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'syn3', E'Synonym', E'Substance_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_external_id(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'Pubchem SID', E'https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110513', E'11110513', E'Substance_000000001_000025000.sdf');").append(Encoder.newline)
				.append("select insert_external_id(E'YZTICFPLOXKSQO-RGGAHWMASA-L', E'MMDB', E'', E'0513', E'Substance_000000001_000025000.sdf');").append(Encoder.newline);

		List<String> expLst = Arrays.asList(exp.toString().split(Encoder.newline));
		List<String> resLst = Arrays.asList(res.split(Encoder.newline));

		Collections.sort(expLst);
		Collections.sort(resLst);
		assertEquals(expLst, resLst);
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#toString()}.
	 */
	@Test
	public final void testToString() {
		String exp = new StringBuilder().append("11110513").append(Encoder.newline)
				.append("YZTICFPLOXKSQO-RGGAHWMASA-L").append(Encoder.newline)
				.append("").append(Encoder.newline)
				.append("C34H36FeN4O4").append(Encoder.newline)
				.append("620.51904").append(Encoder.newline)
				.append("123.4").append(Encoder.newline)
				.append("[Synonyms=[HEM, Protoporphyrin Ix Containing Fe, syn3]]").append(Encoder.newline)
				.append("{Pubchem SID=Pubchem SID: 11110513 (https://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=11110513), MMDB=MMDB: 0513 ()}").toString();

		assertEquals(exp, s1.toString());
	}

	/**
	 * Test method for {@link edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Substance#equals(java.lang.Object)}.
	 */
	@Test
	public final void testEqualsObject() {
		s2.setInchiKey("YZTICFPLOXKSQO-RGGAHWMASA-L");
		s2.setSid("11110513");
		s2.addName(new NameObject(CompoundFields.SYNONYMS, "HEM"));
		s2.addName(new NameObject(CompoundFields.SYNONYMS, "Protoporphyrin Ix Containing Fe"));
		s2.addName(new NameObject(CompoundFields.SYNONYMS, "syn3"));

		assertTrue(s2.equals(s1));
	}

	/**
	 * Test method for {@link Compound#getExactMass()}.
	 */
	@Test
	public void testGetExactMass() {
		assert 123.4 == s1.getExactMass();
	}

	/**
	 * Test method for {@link Compound#setExactMass(Double)}.
	 */
	@Test
	public void testSetExactMass() {
		s2.setExactMass(124.3);

		assert 124.3 == s2.getExactMass();
	}

	/**
	 * Test method for {@link Compound#getSourceFile()}.
	 */
	@Test
	public void testGetSourceFile() {
		assertEquals("Substance_000000001_000025000.sdf", s1.getSourceFile());
	}

	/**
	 * Test method for {@link Compound#setSourceFile(String)}.
	 */
	@Test
	public void testSetSourceFile() {
		s2.setSourceFile("Substance_000000001_000025000.sdf");

		assertEquals("Substance_000000001_000025000.sdf", s2.getSourceFile());
	}

	// TODO add tests for get/set inchicode and get/set molweight
}
