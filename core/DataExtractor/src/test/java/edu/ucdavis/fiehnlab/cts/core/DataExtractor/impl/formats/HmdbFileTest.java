package edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.formats;

import edu.ucdavis.fiehnlab.cts.core.DataExtractor.Resource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.CollectionResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.CompressedResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.impl.io.InputStreamResource;
import edu.ucdavis.fiehnlab.cts.core.DataExtractor.model.Compound;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class HmdbFileTest {
	Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() {
		logger.setLevel(Level.DEBUG);
	}

	@Test
	public void testInternalProcessing() throws Exception {
		CollectionResource<Compound> result = null;
		String query = "";

		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/hmdb/first-unparsed.sdf.gz");
			File outfile = new File("src/test/resources/compressed/hmdb" + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
			Resource resource = new HmdbFile(pcfile.getName(), outfile);

			if (pcfile.exists() && pcfile.canRead()) {
				InputStreamResource pcsdf = new CompressedResource(pcfile);
				result = (CollectionResource<Compound>) resource.process(pcsdf);
				query = result.getQuery();

				if (logger.isDebugEnabled()) {
					for (Compound cmp : result) {
						logger.debug(query);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		logger.info("size: " + query);
		assert (query != "");
		assert (result != null);
	}

	@Test
	public void testBadMol() throws Exception {
		CollectionResource<Compound> result = null;
		String query = "";

		File pcfile = null;
		try {
			pcfile = new File("src/test/resources/compressed/hmdb/bad-mol.sdf.gz");
			File outfile = new File("src/test/resources/compressed/hmdb" + File.separatorChar + pcfile.getName().substring(0, pcfile.getName().indexOf(".")) + ".sql");
			Resource resource = new HmdbFile(pcfile.getName(), outfile);

			if (pcfile.exists() && pcfile.canRead()) {
				InputStreamResource pcsdf = new CompressedResource(pcfile);
				result = (CollectionResource<Compound>) resource.process(pcsdf);
				query = result.getQuery();

				if (logger.isDebugEnabled()) {
					for (Compound cmp : result) {
						logger.debug(query);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		assert (query != "");
		assert (result != null);
		assert (result.size() == 0);
	}
}