package edu.ucdavis.fiehnlab.chemify.validation.scoring

import edu.ucdavis.fiehnlab.chemify.Scored

/**
 * Created by wohlgemuth on 10/16/14.
 */
public interface InChIScorer {

	/**
	 * scores the first against the 2nd key
	 * @param first
	 * @param second
	 * @return
	 */
	double score(Scored first, Scored second);
}