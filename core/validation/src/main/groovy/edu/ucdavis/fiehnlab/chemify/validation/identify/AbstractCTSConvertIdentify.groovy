package edu.ucdavis.fiehnlab.chemify.validation.identify

import edu.ucdavis.fiehnlab.chemify.Hit
import edu.ucdavis.fiehnlab.chemify.Priority
import edu.ucdavis.fiehnlab.chemify.exception.ChemifyException
import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.identify.AbstractIdentify
import groovy.json.JsonException
import groovy.json.JsonSlurper
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.log4j.Logger

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/25/13
 * Time: 12:16 AM
 * <p/>
 * basic helper class to support more than just one to one conversion. Sub classes will specify what we actually want to cdk
 */
public abstract class AbstractCTSConvertIdentify extends AbstractIdentify {

	transient Logger logger = Logger.getLogger(this.class.simpleName);

	protected AbstractCTSConvertIdentify() {
		super(Priority.LOW);
	}

	protected AbstractCTSConvertIdentify(Priority priority) {
		super(priority);
	}

	/**
	 * from where do we want to cdk
	 *
	 * @return
	 */
	protected abstract String getFromIdentifier();

	/**
	 * where to do we want to cdk
	 *
	 * @return
	 */
	protected String getToIdentifier() {
		return "InChIKey";
	}

	/**
	 * uses the cts cdk service to identify a compounds or an empty list.
	 *
	 * @param searchTerm
	 * @return
	 */
	final protected List<Hit> doIdentify(String searchTerm, String original) throws ChemifyException {

		List<Hit> result = new ArrayList<Hit>();

		CloseableHttpClient client = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(10000)
				.setConnectTimeout(10000)
				.build();

		URL url = new URL("http://cts.fiehnlab.ucdavis.edu/service/convert/Chemical Name/${getToIdentifier()}/$searchTerm");
		URI encodedUrl = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef())

		HttpGet request = new HttpGet(encodedUrl)
		request.addHeader("accept", "application/json")
		request.setConfig(requestConfig);

		CloseableHttpResponse response;
		try {
			response = client.execute(request);

			if (response.getStatusLine().getStatusCode() == 404) {
//				logger.trace("response not ok")
				return result;
			}

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer entity = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				entity.append(line);
			}

			def res
			try {
				//get inchikeys
				res = new JsonSlurper().parseText(entity.toString()).result[0]
			} catch (JsonException e) {
				logger.trace("error parsing: $searchTerm, ${e.getMessage()}")
			}

			res.each { newInchi ->
				if (!result.inchiKey.contains(newInchi)) {
					result.add(new HitImpl(newInchi, this, searchTerm, original))
				}
			}
			response.close();

		} catch (SocketTimeoutException e) {
			logger.trace("waited too long for result identifying: $searchTerm")
		}

		client.close();

		return result;
	}


	public String getDescription() {
		return "this method utilizes the CTS Webservice to identify a search term";
	}
}
