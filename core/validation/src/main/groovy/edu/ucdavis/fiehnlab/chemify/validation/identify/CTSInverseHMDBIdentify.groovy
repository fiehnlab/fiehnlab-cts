package edu.ucdavis.fiehnlab.chemify.validation.identify

import edu.ucdavis.fiehnlab.chemify.Priority

/**
 *
 */
public class CTSInverseHMDBIdentify extends AbstractCTSConvertIdentify {

	@Override
	protected String getToIdentifier() {
		return "Human Metabolome Database"
	}

	@Override
	protected String getFromIdentifier() {
		return "Chemical Name";
	}

	public String getPattern() {
		return "HMDB[0-9]*";
	}

	public CTSInverseHMDBIdentify() {
		super(Priority.HIGH);
	}
}
