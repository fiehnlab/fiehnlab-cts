package edu.ucdavis.fiehnlab.chemify.validation.scoring

import edu.ucdavis.fiehnlab.chemify.*
import edu.ucdavis.fiehnlab.chemify.hit.ScoredHit
import groovy.json.JsonSlurper
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.log4j.Logger

import java.text.DecimalFormat

/**
 * Created by diego on 10/2/14.
 */
class ScoreBiological implements Scoring {
	Logger logger = Logger.getLogger(this.class.simpleName)

	/**
	 * the score methods converts the result into a scored hit to reduce the amount of data output
	 * @param result
	 * @return
	 */
	@Override
	List<Scored> score(List<? extends Hit> hits, String searchTerm) {
		ArrayList<Scored> scoredResult = new ArrayList<Scored>();

		def result = [:]
		Integer max = 0

		/* calculate counts in biological databases for each hit in input
		 * each input comes from a unique searchTerm, and can contain several hits (0 to n)
		 * so we score ALL hits for the searchTerm
		 */
		for (Hit hit : hits) {
			def inchi = hit.inchiKey

			//get bioCount for inchikey
			def test = new JsonSlurper().parseText(bioIdCount(inchi))

			if (test.error == null) {
				result.put(inchi, test.total)
			} else {
				result.put(inchi, 0.0D)
			}

			Integer count = 0
			/* check for total count and if it has KEGG reference
			 * and update count and max accordingly
			 */
			if (test.total > 0) {
				count += test.total
			}

			// increase value if it has KEGG
			if (test.KEGG != null && test.KEEG > 0) {
				count += 10
			}

			// find the max
			if (count >= max) {
				max = count
			}
			result[inchi] = count
		}

		//calculate the relative score (scale score based on max count from all hits)
		for (String inchi : result.keySet()) {
			Double score = result[inchi] / max.doubleValue()

			//in case of Nan or 0/0 we always set it to 0
			if (Double.isNaN(score)) {
				score = 0.0;
			}
			DecimalFormat f = new DecimalFormat("#0.##")
			result[inchi] = f.format(score).toDouble()
		}

		//sort it from highest to lowest
		result = result.sort { -it.value }

		result.each {
			int idx = hits.inchiKey.indexOf(it.key)
			scoredResult.add(new ScoredHit(hits.get(idx), this, it.value))
		}

		return scoredResult
	}


	def bioIdCount(String inchi) {
		def message

		CloseableHttpClient httpclient = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(10000)
				.setConnectTimeout(10000)
				.build();
		HttpGet request = new HttpGet("http://cts.fiehnlab.ucdavis.edu/service/countBiological/$inchi");

		request.setConfig(requestConfig);
		CloseableHttpResponse response = null;

		response = httpclient.execute(request);

		int status = response.getStatusLine().getStatusCode();
		message = response.getStatusLine().getReasonPhrase();

		String jsonstr = response.entity.content.text

		response.close();
		httpclient.close();

		return jsonstr;
	}

	/**
	 * am arbitrary number. The higher the value the higher the priority
	 *
	 * @return
	 */
	@Override
	Priority getPriority() {
		return Priority.HIGH
	}

	@Override
	int compareTo(Prioritize prioritize) {
		return 0
	}

	/**
	 * the description of this object
	 * @return
	 */
	@Override
	String getDescription() {
		return ""
	}
}
