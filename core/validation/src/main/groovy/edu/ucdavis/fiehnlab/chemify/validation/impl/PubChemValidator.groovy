package edu.ucdavis.fiehnlab.chemify.validation.impl

import edu.ucdavis.fiehnlab.chemify.Chemify
import edu.ucdavis.fiehnlab.chemify.configuration.SimpleConfiguration
import edu.ucdavis.fiehnlab.chemify.identify.pubchem.PCNameIdentify
import edu.ucdavis.fiehnlab.chemify.validation.AbstractValidator

/**
 * Created by diego on 9/29/14.
 */
public class PubChemValidator extends AbstractValidator {

	public Chemify getChemify() {
		SimpleConfiguration configuration = new SimpleConfiguration();

		configuration.addAlgorithm(new PCNameIdentify());

		this.configureConfiguration(configuration);

		return new Chemify(configuration);
	}
}