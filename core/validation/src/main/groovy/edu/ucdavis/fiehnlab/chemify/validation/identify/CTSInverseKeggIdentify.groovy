package edu.ucdavis.fiehnlab.chemify.validation.identify

import edu.ucdavis.fiehnlab.chemify.Priority

/**
 * Created by diego on 4/14/14.
 */
public class CTSInverseKeggIdentify extends AbstractCTSConvertIdentify {

	@Override
	protected final String getToIdentifier() {
		return "KEGG";
	}

	@Override
	protected String getFromIdentifier() {
		return "Chemical Name";
	}

	public String getPattern() {
		return "C[0-9]{5}";
	}

	public CTSInverseKeggIdentify() {
		super(Priority.VERY_HIGH);
	}
}
