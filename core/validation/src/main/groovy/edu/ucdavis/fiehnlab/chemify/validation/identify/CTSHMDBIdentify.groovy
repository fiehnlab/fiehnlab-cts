package edu.ucdavis.fiehnlab.chemify.validation.identify

import edu.ucdavis.fiehnlab.chemify.Priority

public class CTSHMDBIdentify extends AbstractCTSConvertIdentify {

	public CTSHMDBIdentify() {
		this(Priority.PATTERN_BASED_APPROACH);
	}

	public CTSHMDBIdentify(Priority priority) {
		super(priority);
	}

	@Override
	protected String getFromIdentifier() {
		return "Human Metabolome Database";
	}
}
