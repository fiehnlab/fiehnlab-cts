package edu.ucdavis.fiehnlab.chemify.validation

import edu.ucdavis.fiehnlab.chemify.validation.impl.*
import org.apache.log4j.Logger

/**
 * Created by diego on 10/2/14.
 */
public class Validator {
	Set<AbstractValidator> validators = new HashSet<AbstractValidator>()
	Map<String, String> keywords = new HashMap<String, String>()
	Logger logger = Logger.getLogger(this.class.simpleName)

	protected void run() {
		Map stats = new HashMap()
		validators.add(new ChemspiderValidator())
		validators.add(new CTSConvertValidator())
		validators.add(new PubChemValidator())

		validators.each { validator ->
			List result = new ArrayList()

			keywords.each { term, expected ->

				result = validator.validate(term, expected)
			}

			stats.put(validator.class.simpleName, validator.getStats())
		}

		stats.each {
//			logger.info(it.value)
		}
	}

}
