package edu.ucdavis.fiehnlab.chemify.validation.scoring

import edu.ucdavis.fiehnlab.chemify.*
import org.apache.log4j.Logger

/**
 * Created by wohlgemuth on 10/16/14.
 */
public abstract class AbstractCombinedScore implements Scoring {
	Logger logger = Logger.getLogger(this.class)
	Scoring[] scorers;

	/**
	 * the score methods converts the result into a scored hit to reduce the amount of data output
	 * @param result
	 * @return
	 */

	@Override
	final List<Scored> score(List<? extends Hit> hits, String searchTerm) {

		List<List<Scored>> results = []

		for (Scoring scorer : scorers) {
			List<Scored> tmpResult = []
			tmpResult = scorer.score(hits, searchTerm)

			//doing the following generates exception since score value must be 0 < n < 1
//				for(Enhanced hit : hits) {
//					tmpResult.add(new ScoredHit(hit, scorer, hit.getEnhancement(((AbstractChemSpiderScoring) scorer).getMetric()).value))
//				}
			results.add(tmpResult)
		}
		return determineBestResult(results);
	}

	/**
	 * takes our incoming list of result lists and computes our final scored list out of it
	 * @param resultSet
	 * @return
	 */
	abstract List<Scored> determineBestResult(List<List<Scored>> resultSet)

	public AbstractCombinedScore(Scoring... scorer) {
		this.scorers = scorer;
	}

	@Override
	int compareTo(Prioritize prioritize) {
		return this.getPriority().compareTo(prioritize.priority)
	}

}