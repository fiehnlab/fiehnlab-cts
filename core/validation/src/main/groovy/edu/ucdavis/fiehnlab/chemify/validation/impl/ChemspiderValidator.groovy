package edu.ucdavis.fiehnlab.chemify.validation.impl

import edu.ucdavis.fiehnlab.chemify.Chemify
import edu.ucdavis.fiehnlab.chemify.configuration.SimpleConfiguration
import edu.ucdavis.fiehnlab.chemify.identify.chemspider.ChemSpiderSearchIdentify
import edu.ucdavis.fiehnlab.chemify.validation.AbstractValidator

/**
 * Created by diego on 9/29/14.
 */
public class ChemspiderValidator extends AbstractValidator {

	public Chemify getChemify() {
		SimpleConfiguration configuration = new SimpleConfiguration();

		configuration.addAlgorithm(new ChemSpiderSearchIdentify());

		this.configureConfiguration(configuration);

		return new Chemify(configuration);
	}
}