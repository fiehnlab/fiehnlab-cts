package edu.ucdavis.fiehnlab.chemify.validation

import edu.ucdavis.fiehnlab.chemify.*
import edu.ucdavis.fiehnlab.chemify.filter.TopNFilter
import edu.ucdavis.fiehnlab.chemify.modify.*
import edu.ucdavis.fiehnlab.chemify.multiplex.*
import edu.ucdavis.fiehnlab.chemify.scoring.NoScoringDone
import edu.ucdavis.fiehnlab.chemify.scoring.chemspider.*
import edu.ucdavis.fiehnlab.chemify.scoring.kegg1.ScoreByKeggPresence
import org.apache.log4j.Logger

/**
 * Created by diego on 9/29/14.
 */
abstract class AbstractValidator {
	protected int total = 0;
	protected int pass = 0;
	protected int unordered = 0;
	protected int fail = 0;

	protected abstract Chemify getChemify()

	Logger logger = Logger.getLogger(this.class)

	public List<? extends Hit> validate(String term, String expected) {
		List<? extends Hit> result = new ArrayList<Hit>()

		long time = 0;

		Chemify chemify = getChemify()

		try {
			long start = System.nanoTime()
			result = chemify.identify(term)
			time = (System.nanoTime() - start) / 1000000
		} catch (Exception e) {
			logger.error("ERROR: exception validating $term", e)
		}

		return result
	}

	protected Configuration configureConfiguration(Configuration configuration) {
		configuration.setSearchTermMinQuality(0.25)

		List<Modify> modifiers = new ArrayList<Modify>()
		modifiers.add(new DeuteratedCompoundModifer())
		modifiers.add(new ISTDModifier())
		modifiers.add(new NISTModifier())
		modifiers.add(new TrimRetentionTimeModifier())
		modifiers.add(new TrimModifier())
		modifiers.add(new ArtifactModifier())
		modifiers.add(new TMSModifier())
		modifiers.add(new MinorModifier())
		modifiers.add(new ZModifier())
		modifiers.add(new GeneralUnderscoreModifier())
		modifiers.add(new NegativeAductModifier())
		modifiers.add(new PositiveAductModifier())
		modifiers.add(new LipidModifier())
		modifiers.add(new PhospholipidModifier())
		modifiers.add(new SterolModifier())
		modifiers.add(new SphingolipidModifier())
		modifiers.add(new ParensStarModifier())
		modifiers.add(new DehydratedModifier())
		modifiers.add(new MeoxModifer())
		configuration.setModifications(modifiers)

		Set<Multiplexer> multiplexers = new HashSet<Multiplexer>()
		multiplexers.add(new SpaceMultiPlexer())
		multiplexers.add(new DashMultiPlexer())
//		multiplexers.add(new AcidSuffixMultiPlexer())
		multiplexers.add(new SugarMultiplexer())
		configuration.setMultiplexer(multiplexers)

		configuration.addFilter(new TopNFilter(10))

//		configuration.setScoringAlgorithms([new NoScoringDone()]);
//		configuration.setScoringAlgorithms([new ScoreBiological(), new NoScoringDone()]);
//		configuration.setScoringAlgorithms([new ScoreByChemSpiderDataSourceCount(), new NoScoringDone()]);
//		configuration.setScoringAlgorithms([new ScoreByChemSpiderReferenceCount(), new NoScoringDone()]);
//		configuration.setScoringAlgorithms([new ScoreByChemSpiderPubMed(), new NoScoringDone()]);

		CombinedScorer combinedChemspiderScorer = new CombinedScorer();
		combinedChemspiderScorer.addScorer(new ScoreByChemSpiderPubMed(1))
		combinedChemspiderScorer.addScorer(new ScoreByChemSpiderReferenceCount(1))
		combinedChemspiderScorer.addScorer(new ScoreByChemSpiderDataSourceCount(1))
		combinedChemspiderScorer.addScorer(new ScoreByKeggPresence())
		configuration.setScoringAlgorithms([combinedChemspiderScorer, new NoScoringDone()])

//		AverageCombinedScore avgScorer = new AverageCombinedScore(
//				new ScoreByKeggPresence(),
//				new ScoreByChemSpiderReferenceCount(),
//				new ScoreByChemSpiderPubMed(),
//				new ScoreByChemSpiderDataSourceCount())
//		configuration.setScoringAlgorithms([avgScorer])

		return configuration
	}

	public String getStats() {
		String message = """\nValidator: ${this.class.simpleName}
\tTotal test cases: $total
\tPassing test cases: $pass (${(0.0 + pass) / total * 100}% of total)
\tResult not top hit: $unordered (${(0.0 + unordered) / total * 100}% of total)
\tFails: $fail (${(0.0 + fail) / total * 100}% of total)
Configuration: ${this.chemify.defaultConfiguration}
"""

		return message
	}
}
