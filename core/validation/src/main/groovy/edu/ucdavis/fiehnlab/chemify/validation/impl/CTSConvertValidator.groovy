package edu.ucdavis.fiehnlab.chemify.validation.impl

import edu.ucdavis.fiehnlab.chemify.Chemify
import edu.ucdavis.fiehnlab.chemify.configuration.SimpleConfiguration
import edu.ucdavis.fiehnlab.chemify.validation.AbstractValidator
import edu.ucdavis.fiehnlab.chemify.validation.identify.CTSInverseKeggIdentify

/**
 * Created by diego on 10/7/14.
 */
public class CTSConvertValidator extends AbstractValidator {
	@Override
	protected Chemify getChemify() {
		SimpleConfiguration configuration = new SimpleConfiguration();

		configuration.addAlgorithm(new CTSInverseKeggIdentify())
//		configuration.addAlgorithm(new CTSInverseHMDBIdentify())
//		configuration.addAlgorithm(new CTSHMDBIdentify(Priority.HIGH));

		this.configureConfiguration(configuration);

		return new Chemify(configuration);
	}
}
