package edu.ucdavis.fiehnlab.chemify.validation.scoring

import edu.ucdavis.fiehnlab.chemify.hit.HitImpl
import edu.ucdavis.fiehnlab.chemify.identify.chemspider.ChemSpiderSearchIdentify
import org.apache.log4j.Logger
import org.junit.Test

public class ScoreBiologicalTest {
	Logger logger = Logger.getLogger(this.class.simpleName)

	@Test
	public void testScore() throws Exception {
		ScoreBiological scorer = new ScoreBiological()
		HitImpl hit1 = new HitImpl("QNAYBMKLOCPYGJ-REOHCLBHSA-N", new ChemSpiderSearchIdentify(), "alanine", "alanine")
		HitImpl hit2 = new HitImpl("QNAYBMKLOCPYGJ-UWTATZPHSA-N", new ChemSpiderSearchIdentify(), "alanine", "alanine")

		def res = scorer.score([hit2, hit1], "alanine")

	}

	@Test
	public void testBioIdCount() throws Exception {
		ScoreBiological scorer = new ScoreBiological()

		def res = scorer.bioIdCount("LFQSCWFLJHTTHZ-UHFFFAOYSA-N")

	}
}
