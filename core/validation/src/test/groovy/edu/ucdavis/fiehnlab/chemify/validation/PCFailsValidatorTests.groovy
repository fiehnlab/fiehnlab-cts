package edu.ucdavis.fiehnlab.chemify.validation

import edu.ucdavis.fiehnlab.chemify.validation.impl.PubChemValidator
import org.junit.Before

/**
 * Created by diego on 10/14/14.
 */
class PCFailsValidatorTests extends ValidatorTest {

	public File getFileName() {
		return new File("src/test/logs/pubchem_validator.tsv")
	}

	@Before
	void setUp() {
		validators.add(new PubChemValidator())
	}

	void initKeywordsListSmallSet() {
		keywords.put("cysteine", "XUJNEKJLAYXESH-UHFFFAOYSA-N")
		keywords.put("3-methoxytyrosine NIST", "PFDUUKDQEHURQC-ZETCQYMHSA-N")
		keywords.put("galactose-6-phosphate", "VFRROHXSMXFLSN-UHFFFAOYSA-N")
		keywords.put("arachidonic acid", "YZXBAPSDXZZRGB-CGRWFSSPSA-N")
		keywords.put("naproxen", "CMWTZPSULFXXJA-UHFFFAOYSA-N")
		keywords.put("N-acetyl-D-tryptophan", "DZTHIGRZJZPRDV-UHFFFAOYSA-N")
		keywords.put("lactobionic acid", "AQKFVNLHZYOMKQ-UHFFFAOYSA-M")
		keywords.put("kynurenine", "YGPSJZOEDVAXAB-QMMMGPOBSA-N")
		keywords.put("2,3-dihydroxybutanoic acid NIST", "LOUGYXZSURQALL-STHAYSLISA-N")
		keywords.put("2-hydroxyhippuric acid", "UOHMMEJUHBCKEE-UHFFFAOYSA-N")
		keywords.put("3-hydroxybutanoic acid", "WHBMMWSBFZVSSR-GSVOUGTGSA-N")
		keywords.put("glyceric acid", "RBNPOMFGQQGHHO-UWTATZPHSA-N")
		keywords.put("sophorose", "HIWPGCMGAMJNRG-BTLHAWITSA-N")
		keywords.put("2-oxogluconic acid NIST", "VBUYCZFBVCCYFD-JJYYJPOSSA-N")
		keywords.put("arabinose", "SRBFZHDQGSBBOR-UHFFFAOYSA-N")
		keywords.put("3,6-anhydrogalactose", "WZYRMLAWNVOIEX-ARQDHWQXSA-N")
		keywords.put("glucose", "GZCGUPFRVQAUEE-SLPGGIOYSA-N")
		keywords.put("indole-3-lactate", "XGILAAMKEQUXLS-UHFFFAOYSA-N")
		keywords.put("pipecolic acid", "HXEACLLIILLPRG-YFKPBYRVSA-N")
		keywords.put("elaidic acid", "LKOVPWSSZFDYPG-MSUUIHNZSA-N")
		keywords.put("2-hydroxybutanoic acid", "WHBMMWSBFZVSSR-VKHMYHEASA-N")
		keywords.put("N-acetyl-D-mannosamine", "MBLBDJOUHNCFQT-WCTZXXKLSA-N")
		keywords.put("2,4-diaminobutyric acid", "OGNSCSPNOLGXSM-VKHMYHEASA-N")
		keywords.put("propane-1,2,3-tricarboxylate NIST", "KQTIIICEAUMSDG-UHFFFAOYSA-N")
		keywords.put("glyoxalurea NIST", "WYLUZALOENCNQU-UHFFFAOYSA-N")
		keywords.put("maltotriose", "RXVWSYJTUUKTEA-CGQAXDJHSA-N")
		keywords.put("inulobiose", "WOHYVFWWTVNXTP-TWOHWVPZSA-N")
	}
}
