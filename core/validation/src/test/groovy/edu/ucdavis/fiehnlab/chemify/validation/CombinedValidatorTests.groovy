package edu.ucdavis.fiehnlab.chemify.validation

import edu.ucdavis.fiehnlab.chemify.validation.impl.ChemspiderValidator
import edu.ucdavis.fiehnlab.chemify.validation.impl.PubChemValidator
import org.junit.Before

/**
 * Created by diego on 10/14/14.
 */
class CombinedValidatorTests extends ValidatorTest {

	@Before
	void setUp() {
		validators.add(new ChemspiderValidator())
		validators.add(new PubChemValidator())
//		validators.add(new CTSConvertValidator())
	}

	public File getFileName() {
		return new File("src/test/logs/combined_validation.tsv")
	}
}
