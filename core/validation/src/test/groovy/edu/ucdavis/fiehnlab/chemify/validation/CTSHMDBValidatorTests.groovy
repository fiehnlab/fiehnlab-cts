package edu.ucdavis.fiehnlab.chemify.validation

import edu.ucdavis.fiehnlab.chemify.validation.impl.CTSConvertValidator
import org.junit.Before

/**
 * Created by diego on 10/14/14.
 */
class CTSHMDBValidatorTests extends ValidatorTest {

	@Before
	void setUp() {
		validators.add(new CTSConvertValidator())
	}

	public File getFileName() {
		return new File("src/test/logs/cts-hmdb_validation.tsv")
	}
}
