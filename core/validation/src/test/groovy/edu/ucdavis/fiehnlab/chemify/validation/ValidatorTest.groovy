package edu.ucdavis.fiehnlab.chemify.validation

import edu.ucdavis.fiehnlab.chemify.Hit
import org.apache.log4j.Logger
import org.junit.*

public abstract class ValidatorTest {
	Set<AbstractValidator> validators = new HashSet<AbstractValidator>()
	Logger logger = Logger.getLogger(this.class)
	Map keywords = new HashMap()
	static List stats = new ArrayList()
	File file
	int total, pass, half, fail, broken = 0;

	protected abstract getFileName()

	@Before
	void setUpClass() {
		file = getFileName()
		if (file.exists()) {
			file.delete()
		}
	}

	@After
	void tearDown() {
		file.withWriter { out ->
			def headers = stats[0].keySet().join("\t")
			out.writeLine(headers)
			stats.each() { item ->
				def line = """${item.get("original")}\t${item.get("modified")}\t${item.get("expected")}\t${item.get("returned")}\t${item.get("status")}\t${
					item.get("algorithm")
				}\t${item.get("all inchis")}"""
				out.writeLine("${line}")
			}
		}
		logger.debug("file saved.")
	}

	@Test
	public void validate() throws Exception {
		List<Hit> result = new ArrayList()

//		initValidatedList()
		initKeywordsList()
//		initKeywordsListSmallSet()
//		initKeywordsListFails1()

		validators.each { AbstractValidator validator ->
			logger.debug("algorythms:")
			validator.chemify.defaultConfiguration.algorithms.each {
				logger.debug(it.class.simpleName)
			}
			logger.debug("multiplexers:")
			validator.chemify.defaultConfiguration.multiplexer.each {
				logger.debug(it.class.simpleName)
			}
			logger.debug("filter size: ${validator.chemify.defaultConfiguration.filters.toArray()[0].limit}")

			keywords.each { String term, String expected ->
				logger.info("Validating $term...")
				result = validator.validate(term, expected)

				def passStat = checkPassing(result, term, expected)

				// original, search, inchi, pass|half\fail
				Map item = new HashMap()
				def inchis = result.inchiKey

				if (result.isEmpty()) {
					stats.add(["original": term, "modified": "not found", "expected": expected, "returned": "not found", "status": passStat, "algorithm": validator.chemify.defaultConfiguration.algorithms, "all inchis": "[]"])
				} else {
					def hit = result[0]
					stats.add(["original": hit.originalTerm, "modified": hit.searchTerm, "expected": expected, "returned": hit.inchiKey, "status": passStat, "algorithm": hit.usedAlgorithm.class.simpleName, "all inchis": inchis])
				}
			}
			stats.add(["original": "", "modified": "", "expected": "", "returned": "", "status": "", "algorithm": "", "all inchis": ""])

			def finalLine = [original: "validator: $validator", modified: "Pass: $pass (${pass / total * 100})", expected: "Half: $half (${half / total * 100})", returned: "Fail: $fail (${fail / total * 100})", status: "Broken: $broken (${broken / total * 100})", algorithm: "Total: $total", "all inchis": ""]
			logger.info(finalLine)
			stats.add(finalLine)
		}
	}

	protected String checkPassing(List<Hit> result, String term, String expected, int time = 0) {
		total++
		if (result.size() < 1) {
			broken++
			logger.debug("Nothing identified\t\t$term\t\t$expected\t\t($time s)")
			return "nothing"
		}
		def partialInchi = result[0].inchiKey[0..result[0].inchiKey.indexOf("-")]
		def partialExp = expected[0..expected.indexOf("-")]
		if (partialInchi.equals(partialExp)) {
			pass++
			logger.debug("PASS\t\t$term\t${result[0].originalTerm}\t$expected\t[${result[0].inchiKey}]\t($time s)")
			return "PASS"
		}

		if (result.size() > 1 && result[1..-1].inchiKey.contains(expected)) {
			half++
			logger.debug("Fail, expected is [${result.inchiKey.indexOf(expected) + 1}] item in results.\t$term\t${result.originalTerm}\t$expected\t[${result.inchiKey}]\t($time s)")
			return "HALF"
		} else {
			fail++
			logger.debug("FAIL, expected is not in results.\t$term\t${result.originalTerm}\t$expected\t[${result.inchiKey}]\t($time s)")
			return "FAIL"
		}
	}

	void initValidatedList() {
		keywords.put("laurate", "POULHZVOKOAJMA-UHFFFAOYSA-N");
		keywords.put("behenate", "UKMSUNONTOPOIO-UHFFFAOYSA-N");
		keywords.put("Malonate", "OFOBLEOULBTSOW-UHFFFAOYSA-N");
		keywords.put("calcitriol", "GMRQFYUYWCNGIN-ZVUFCXRFSA-N");
		keywords.put("timnodonate", "JAZBEHYOTPTENJ-UHFFFAOYSA-N");
		keywords.put("docosahexaenoate", "MBMBGCFOFBJSGT-UHFFFAOYSA-N");
		keywords.put("prostaglandin-b1", "YBHMPNRDOVPQIN-KRWDZBQOSA-N");
		keywords.put("4-Aminobutanoate", "BTCSSZJGUNDROE-UHFFFAOYSA-N");
		keywords.put("3-Oxodecanoyl-CoA", "AZCVXMAPLHSIKY-HSJNEKGZSA-N");
		keywords.put("L-Octanoylcarnitine", "CXTATJFJDMJMIY-ZDUSSCGKSA-N");
		keywords.put("4-maleylacetoacetate", "GACSIVHAIFQKTC-UHFFFAOYSA-N");
		keywords.put("3-(methylthio)propionate", "CAOMCZAIALVUPA-UHFFFAOYSA-N");
		keywords.put("4-fumarylacetoacetate(2-)", "GACSIVHAIFQKTC-UHFFFAOYSA-N");
		keywords.put("11,12-EET", "DXOYQVHGIODESM-UHFFFAOYSA-N");
		keywords.put("14,15-EET", "JBSCUHKPLGKXKH-UHFFFAOYSA-N");
		keywords.put("5alpha-Pregnane-3,20-dione", "XMRPGKVKISIQBV-BJMCWZGWSA-N");
		keywords.put("Androst-5-ene-3beta,17beta-diol", "QADHLRWLCPCEKT-LOVVWNRFSA-N");
		keywords.put("(R)-5-phosphonatomevalonate(3-)", "OKZYCXHTTZZYSK-ZCFIWIBFSA-N");
		keywords.put("4,4-dimethyl-5alpha-cholesta-8,14,24-trien-3beta-ol", "LFQXEZVYNCBVDO-PBJLWWPKSA-N");
		keywords.put("(4R,5S)-4,5,6-trihydroxy-2,3-dioxohexanoate", "GJQWCDSAOUMKSE-STHAYSLISA-N");
	}

	void initKeywordsListSmallSet() {
		keywords.put("glucose", "GZCGUPFRVQAUEE-SLPGGIOYSA-N");
		keywords.put("phosphoric acid", "NBIIXXVUZAFLBC-UHFFFAOYSA-N");
		keywords.put("leucine", "ROHFNLRQFUQHCH-YFKPBYRVSA-N");
		keywords.put("tryptophan", "QIVBCDIJIAJPQS-VIFPVBQESA-N");
		keywords.put("citric acid", "KRKNYBCHXYNGOX-UHFFFAOYSA-N");
		keywords.put("ribose", "HMFHBZSHGGEWLO-SOOFDHNKSA-N");
		keywords.put("proline", "ONIBWKKTOPOVIA-BYPYZUCNSA-N");
		keywords.put("2-hydroxybutanoic acid", "WHBMMWSBFZVSSR-VKHMYHEASA-N");
		keywords.put("phenylalanine", "COLNVLDHVKWLRT-QMMMGPOBSA-N");
		keywords.put("inositol myo-", "CDAISMWEOUEBRE-UHFFFAOYSA-N");
		keywords.put("glutamic acid", "WHUUTDBJXJRKMK-VKHMYHEASA-N");
		keywords.put("N-methylalanine", "GDFAOVXKHJXLEI-VKHMYHEASA-N");
	}

	void initKeywordsList() {
		keywords.put("glucose", "GZCGUPFRVQAUEE-SLPGGIOYSA-N");
		keywords.put("stearic acid", "QIQXTHQIDYTFRH-UHFFFAOYSA-N");
		keywords.put("alanine", "QNAYBMKLOCPYGJ-REOHCLBHSA-N");
		keywords.put("cholesterol", "HVYWMOMLDIMFJA-DPAQBDIFSA-N");
		keywords.put("valine", "KZSNJWFQEVHDMF-BYPYZUCNSA-N");
		keywords.put("glycine", "DHMQDGOQFOQNFH-UHFFFAOYSA-N");
		keywords.put("glutamine", "ZDXPYRJPNDTMRX-VKHMYHEASA-N");
		keywords.put("lactic acid", "JVTAAEKCZFNVCJ-UHFFFAOYSA-N");
		keywords.put("urea", "XSQUKJJJFZCRTK-UHFFFAOYSA-N");
		keywords.put("oxoproline", "ODHCTXKNWHHXJC-VKHMYHEASA-N");
		keywords.put("phosphoric acid", "NBIIXXVUZAFLBC-UHFFFAOYSA-N");
		keywords.put("leucine", "ROHFNLRQFUQHCH-YFKPBYRVSA-N");
		keywords.put("tyrosine", "OUYCCCASQSFEME-QMMMGPOBSA-N");
		keywords.put("glycerol", "PEDCQBHIVMGVHV-UHFFFAOYSA-N");
		keywords.put("1,5-anhydroglucitol", "MPCAJMNYNOGXPB-SLPGGIOYSA-N");
		keywords.put("tryptophan", "QIVBCDIJIAJPQS-VIFPVBQESA-N");
		keywords.put("citric acid", "KRKNYBCHXYNGOX-UHFFFAOYSA-N");
		keywords.put("isoleucine", "AGPKZVBTJJNPAG-WHFBIAKZSA-N");
		keywords.put("uric acid", "LEHOTFFKMJEONL-UHFFFAOYSA-N");
		keywords.put("threonine", "AYFVYJQAPQTCCC-GBXIJSLDSA-N");
		keywords.put("lysine", "KDXKERNSBIXSRK-YFKPBYRVSA-N");
		keywords.put("serine", "MTCFGRXMJLQNBG-REOHCLBHSA-N");
		keywords.put("palmitic acid", "IPCSVZSSVZVIGE-UHFFFAOYSA-N");
		keywords.put("proline", "ONIBWKKTOPOVIA-BYPYZUCNSA-N");
		keywords.put("ornithine", "AHLPHDHHMVZTML-BYPYZUCNSA-N");
		keywords.put("2-hydroxybutanoic acid", "WHBMMWSBFZVSSR-VKHMYHEASA-N");
		keywords.put("phenylalanine", "COLNVLDHVKWLRT-QMMMGPOBSA-N");
		keywords.put("pelargonic acid", "FBUKVWPVBMHYJY-UHFFFAOYSA-N");
		keywords.put("histidine", "HNDVDQJCIGZPNO-YFKPBYRVSA-N");
		keywords.put("inositol myo-", "CDAISMWEOUEBRE-UHFFFAOYSA-N");
		keywords.put("glutamic acid", "WHUUTDBJXJRKMK-VKHMYHEASA-N");
		keywords.put("N-methylalanine", "GDFAOVXKHJXLEI-VKHMYHEASA-N");
		keywords.put("benzoic acid", "WPYMKLBDIGXBTP-UHFFFAOYSA-N");
		keywords.put("lauric acid", "POULHZVOKOAJMA-UHFFFAOYSA-N");
		keywords.put("pentadecanoic acid", "WQEPLUUGTLDZJY-UHFFFAOYSA-N");
		keywords.put("beta-mannosylglycerate", "DDXCFDOPXBPUJC-SAYMMRJXSA-N");
		keywords.put("talose", "GZCGUPFRVQAUEE-KAZBKCHUSA-N");
		keywords.put("iminodiacetic acid", "NBZBKCUXIYYUSX-UHFFFAOYSA-N");
		keywords.put("3-hydroxybutanoic acid", "WHBMMWSBFZVSSR-GSVOUGTGSA-N");
		keywords.put("oleic acid", "ZQPPMHVWECSIRJ-KTKRTIGZSA-N");
		keywords.put("glyceric acid", "RBNPOMFGQQGHHO-UWTATZPHSA-N");
		keywords.put("creatinine", "DDRJAANPRJIHGJ-UHFFFAOYSA-N");
		keywords.put("2-ketoisocaproic acid", "BKAJNAXTPSGJCU-UHFFFAOYSA-N");
		keywords.put("asparagine", "DCXYFEDJOCDNAF-UHFFFAOYSA-N");
		keywords.put("1-monoolein", "RZRNAYUHWVFMIP-KTKRTIGZSA-N");
		keywords.put("cystine", "LEVWYRKDKASIDU-UHFFFAOYSA-N");
		keywords.put("acetophenone NIST", "KWOLFJPFCHCOCG-UHFFFAOYSA-N");
		keywords.put("fructose", "BJHIKXHVCXFQLS-UYFOZJQFSA-N");
		keywords.put("methylhexadecanoic acid", "KEMQGTRYUADPNZ-UHFFFAOYSA-N");
		keywords.put("methionine", "FFEARJCKVFRZRR-BYPYZUCNSA-N");
		keywords.put("2-ketoadipic acid", "FGSBNBBHOZHUBO-UHFFFAOYSA-N");
		keywords.put("tocopherol alpha", "GVJHHUAWPYXKBD-IEOSBIPESA-N");
		keywords.put("capric acid", "GHVNFZFCNZKVNT-UHFFFAOYSA-N");
		keywords.put("arachidic acid", "VKOBVWXKNCXXDE-UHFFFAOYSA-N");
		keywords.put("6-deoxyglucitol NIST", "SKCKOFZKJLZSFA-JGWLITMVSA-N");
		keywords.put("diacetone alcohol NIST", "SWXVUIWOUIDPGS-UHFFFAOYSA-N");
		keywords.put("palmitoleic acid", "SECPZKHBENQXJG-FPLPWBNLSA-N");
		keywords.put("2-hydroxyvaleric acid", "JRHWHSJDIILJAT-UHFFFAOYSA-N");
		keywords.put("hydroxylamine", "AVXURJPOCDRRFD-UHFFFAOYSA-N");
		keywords.put("myristic acid", "TUNFSRHWOTWDNC-UHFFFAOYSA-N");
		keywords.put("methionine sulfoxide", "QEFRNWWLZKMPFJ-YGVKFDHGSA-N");
		keywords.put("nicotinic acid", "PVNIIMVLHYAWGP-UHFFFAOYSA-N");
		keywords.put("aminomalonic acid", "JINBYESILADKFW-UHFFFAOYSA-N");
		keywords.put("3-hydroxypropionic acid", "ALRHLSYJTWAHJZ-UHFFFAOYSA-N");
		keywords.put("linoleic acid", "OYHQOLUKZRVURQ-HZJYTTRNSA-N");
		keywords.put("phenol", "ISWSIDIOOBJBQZ-UHFFFAOYSA-N");
		keywords.put("pseudo uridine", "PTJWIQPHWPFNBW-GBNDHIKLSA-N");
		keywords.put("deoxytetronic acid NIST", "DZAIOXUZHHTJKN-UHFFFAOYSA-N");
		keywords.put("glycolic acid", "AEMRFAOFKBGASW-UHFFFAOYSA-N");
		keywords.put("cysteine", "XUJNEKJLAYXESH-UHFFFAOYSA-N");
		keywords.put("threonic acid", "JPIJQSOTBSSVTP-UHFFFAOYSA-N");
		keywords.put("caprylic acid", "WWZKQHOCKIZLMA-UHFFFAOYSA-N");
		keywords.put("aspartic acid", "CKLJMWTZIZZHCS-REOHCLBHSA-N");
		keywords.put("4-hydroxyproline", "PMMYEEVYMWASQN-UHFFFAOYSA-N");
//		keywords.put("fucose + rhamnose", "PNNNRSAQSRJVSB-DPYQTVNSSA-N");
		keywords.put("ethanolamine", "HZAXFHJVJLSVMW-UHFFFAOYSA-N");
		keywords.put("2-deoxyerythritol NIST", "ARXKVVRQIIOZGF-UHFFFAOYSA-N");
		keywords.put("parabanic acid NIST", "ZFLIKDUSUDBGCD-UHFFFAOYSA-N");
		keywords.put("citrulline", "RHGKLRLOHDJJDR-BYPYZUCNSA-N");
		keywords.put("xylose", "PYMYPHUHKUWMLA-VPENINKCSA-N");
		keywords.put("isothreonic acid", "JPIJQSOTBSSVTP-GBXIJSLDSA-N");
		keywords.put("pyruvic acid", "LCTONWCANYUPML-UHFFFAOYSA-N");
		keywords.put("heptadecanoic acid NIST", "KEMQGTRYUADPNZ-UHFFFAOYSA-N");
		keywords.put("indole-3-acetate", "SEOVTRFCIGRIMH-UHFFFAOYSA-N");
		keywords.put("elaidic acid", "LKOVPWSSZFDYPG-MSUUIHNZSA-N");
		keywords.put("arachidonic acid", "YZXBAPSDXZZRGB-CGRWFSSPSA-N");
		keywords.put("gamma-tocopherol", "QUEDXNHFTDJVIY-DQCZWYHMSA-N");
		keywords.put("erythritol", "UNXHWFMMPAWVPI-ZXZARUISSA-N");
		keywords.put("succinic acid", "KDYFGRWQOYBRFD-UHFFFAOYSA-N");
		keywords.put("glycerol-alpha-phosphate", "AWUCVROLDVIAJX-UHFFFAOYSA-N");
		keywords.put("isocitric acid", "ODBLHEXUDAPZAU-ZAFYKAAXSA-N");
		keywords.put("behenic acid", "UKMSUNONTOPOIO-UHFFFAOYSA-N");
		keywords.put("benzylalcohol", "WVDDGKGOMKODPV-UHFFFAOYSA-N");
		keywords.put("indole-3-lactate", "XGILAAMKEQUXLS-UHFFFAOYSA-N");
		keywords.put("uracil", "ISAKRJDGNUQOIC-UHFFFAOYSA-N");
		keywords.put("methanolphosphate", "CAAULPUQFIIOTL-UHFFFAOYSA-N");
		keywords.put("glyoxalurea NIST", "WYLUZALOENCNQU-UHFFFAOYSA-N");
		keywords.put("adipic acid", "WNLRTRBMVRJNCN-UHFFFAOYSA-N");
		keywords.put("sorbitol", "FBPFZTCFMRRESA-JGWLITMVSA-N");
		keywords.put("phosphoethanolamine", "SUHOOTKUPISOBE-UHFFFAOYSA-N");
		keywords.put("trans-4-hydroxyproline", "PMMYEEVYMWASQN-DMTCNVIQSA-N");
		keywords.put("glucuronic acid", "AEMOLEFTQBMNLQ-AQKNRBDQSA-N");
		keywords.put("maleimide", "PEEHTFAAVSWFBL-UHFFFAOYSA-N");
		keywords.put("lignoceric acid", "QZZGJDVWLFXDLK-UHFFFAOYSA-N");
		keywords.put("propane-1,3-diol NIST", "YPFDHNVEDLHUCE-UHFFFAOYSA-N");
		keywords.put("propane-1,2,3-tricarboxylate NIST", "KQTIIICEAUMSDG-UHFFFAOYSA-N");
		keywords.put("2-hydroxyglutaric acid", "HWXBTNAVRSUOJR-UHFFFAOYSA-N");
		keywords.put("hypoxanthine", "FDGQSTZJBFJUBT-UHFFFAOYSA-N");
		keywords.put("gluconic acid", "RGHNJXZEOKUKBD-SQOUGZDYSA-N");
		keywords.put("shikimic acid", "JXOHGGNKMLTUBP-HSUXUTPPSA-N");
		keywords.put("ribitol", "HEBKCHPVOIAQTA-UHFFFAOYSA-N");
		keywords.put("galactonic acid", "RGHNJXZEOKUKBD-MGCNEYSASA-N");
		keywords.put("glucose-1-phosphate", "HXXFSFRBOHSIMQ-VFUOTHLCSA-N");
		keywords.put("arabinose", "SRBFZHDQGSBBOR-UHFFFAOYSA-N");
		keywords.put("arabinose", "SRBFZHDQGSBBOR-UHFFFAOYSA-N");
		keywords.put("malic acid", "BJEPYKJPYRNKOW-REOHCLBHSA-N");
		keywords.put("maltose", "GUBGYTABKSRVRQ-QUYVBRFLSA-N");
		keywords.put("alloxanoic acid", "XXGFYEUQDGVMOU-UHFFFAOYSA-N");
		keywords.put("dihydroabietic acid", "BTAURFWABMSODR-UHFFFAOYSA-N");
		keywords.put("trehalose", "HDTRYLNUVZCQOY-LIZSDCNHSA-N");
		keywords.put("threitol", "UNXHWFMMPAWVPI-QWWZWVQMSA-N");
		keywords.put("dodecane", "SNRUBQQJIBEYMU-UHFFFAOYSA-N");
		keywords.put("levoglucosan", "TWNIBLMWSKIRAT-VFUOTHLCSA-N");
		keywords.put("2,4-diaminobutyric acid", "OGNSCSPNOLGXSM-VKHMYHEASA-N");
		keywords.put("fumaric acid", "VZCYOOQTPOCHFL-OWOJBTEDSA-N");
		keywords.put("hydroxycarbamate NIST", "DRAJWRKLRBNJRQ-UHFFFAOYSA-N");
		keywords.put("tartaric acid", "FEWJPZIEWOKRBE-UHFFFAOYSA-N");
		keywords.put("octadecanol", "GLDOVTGHNKAZLK-UHFFFAOYSA-N");
		keywords.put("glycero-guloheptose NIST", "YPZMPEPLWKRVLD-PJEQPVAWSA-N");
		keywords.put("conduritol beta epoxide", "ZHMWOVGZCINIHW-SPHYCDKFSA-N");
		keywords.put("putrescine", "KIDHWZJUCRJVML-UHFFFAOYSA-N");
		keywords.put("1-hexadecanol", "BXWNKGSJHAJOGX-UHFFFAOYSA-N");
		keywords.put("salicylic acid", "YGSDEFSMJLZEOE-UHFFFAOYSA-N");
		keywords.put("phytol", "BOTWFXYSPFMFNR-RTBURBONSA-N");
		keywords.put("xylitol", "HEBKCHPVOIAQTA-NGQZWQHPSA-N");
		keywords.put("glycerol-3-galactoside", "NHJUPBDCSOGIKX-VGPGGAHRSA-N");
		keywords.put("pyrrole-2-carboxylic acid", "WRHZVMBBRYBTKZ-UHFFFAOYSA-N");
		keywords.put("alpha ketoglutaric acid", "KPGXRSRHYNQIFN-UHFFFAOYSA-N");
		keywords.put("2-deoxytetronic acid", "DZAIOXUZHHTJKN-UHFFFAOYSA-N");
		keywords.put("1-monostearin", "VBICKXHEKHSIBG-UHFFFAOYSA-N");
		keywords.put("dodecanol", "LQZZUXJYWNFBMV-UHFFFAOYSA-N");
		keywords.put("GABA", "BTCSSZJGUNDROE-UHFFFAOYSA-N");
		keywords.put("trihydroxypyrazine NIST", "DZMNWPSOYTUVJQ-UHFFFAOYSA-N");
		keywords.put("terephtalic acid", "KKEYFWRCBNTPAC-UHFFFAOYSA-N");
		keywords.put("ribonic acid", "QXKAIJAYHKCRRA-BXXZVTAOSA-N");
		keywords.put("5-methoxytryptamine", "JTEJPPKMYBDEMY-UHFFFAOYSA-N");
		keywords.put("biuret", "OHJMTUPIZMNBFR-UHFFFAOYSA-N");
		keywords.put("triethanolamine", "GSEJCLTVZPLZKY-UHFFFAOYSA-N");
		keywords.put("inulobiose", "WOHYVFWWTVNXTP-TWOHWVPZSA-N");
		keywords.put("cellobiotol", "VQHSOMBJVWLPSR-WELRSGGNSA-N");
		keywords.put("kynurenine", "YGPSJZOEDVAXAB-QMMMGPOBSA-N");
		keywords.put("1-monopalmitin", "QHZLMUACJMDIAE-UHFFFAOYSA-N");
		keywords.put("UDP-glucuronic acid", "HDYANYHVCAPMJV-LXQIFKJMSA-N");
		keywords.put("inositol allo-", "CDAISMWEOUEBRE-UHFFFAOYSA-N");
		keywords.put("uridine", "DRTQHJPVMGBUCF-XVFCMESISA-N");
		keywords.put("3-aminoisobutyric acid", "QCHPKSFMDHPSNR-UHFFFAOYSA-N");
		keywords.put("pyrophosphate", "XPPKVPWEQAFLFU-UHFFFAOYSA-N");
		keywords.put("tocopherol beta NIST", "WGVKWNUPNGFDFJ-CBIUGAAKSA-N");
		keywords.put("2,3-dihydroxybutanoic acid NIST", "LOUGYXZSURQALL-STHAYSLISA-N");
		keywords.put("5-hydroxynorvaline NIST", "CZWARROQQFCFJB-UHFFFAOYSA-N");
		keywords.put("glycyl proline", "KZNQNBZMBZJQJO-YFKPBYRVSA-N");
		keywords.put("enolpyruvate NIST", "LCTONWCANYUPML-UHFFFAOYSA-N");
		keywords.put("pyrazine 2,5-dihydroxy  NIST", "USHRADXTTYICIJ-UHFFFAOYSA-N");
		keywords.put("sucrose", "CZMRCDWAGMRECN-UGDNZRGBSA-N");
//		keywords.put("beta-glycerolphosphate", "DHCLVCXQIBBOPH-UHFFFAOYSA-N");  // duplicate of line 327
		keywords.put("4-hydroxybutyric acid", "SJZRECIVHVDYJC-UHFFFAOYSA-N");
		keywords.put("3-phosphoglycerate", "OSJPPGNTCRNQQC-UHFFFAOYSA-N");
		keywords.put("N-acetylaspartic acid", "OTCCIMWXFLJLIA-BYPYZUCNSA-N");
		keywords.put("isobutene glycol NIST", "BTVWZWFKMIUSGS-UHFFFAOYSA-N");
		keywords.put("phenylethylamine", "BHHGXPLMPWCGHP-UHFFFAOYSA-N");
		keywords.put("cysteine-glycine", "ZUKPVRWZDMRIEO-VKHMYHEASA-N");
		keywords.put("quinic acid", "AAWZDTNXLSGCEK-LNVDRNJUSA-N");
		keywords.put("arabitol", "HEBKCHPVOIAQTA-QWWZWVQMSA-N");
		keywords.put("pipecolic acid", "HXEACLLIILLPRG-YFKPBYRVSA-N");
		keywords.put("1,2,4-benzenetriol", "GGNQRNBDZQJCCN-UHFFFAOYSA-N");
		keywords.put("saccharic acid", "DSLZVSRJTYRBFB-LLEIAEIESA-N");
		keywords.put("idonic acid NIST", "RGHNJXZEOKUKBD-SKNVOMKLSA-N");
		keywords.put("3-methoxytyrosine NIST", "PFDUUKDQEHURQC-ZETCQYMHSA-N");
		keywords.put("lactobionic acid", "JYTUSYBCFIZPBE-AMTLMPIISA-N");    // sodium salt is "AQKFVNLHZYOMKQ-UHFFFAOYSA-M"
		keywords.put("cellobiose", "GUBGYTABKSRVRQ-UHFFFAOYSA-N");
		keywords.put("taurine", "XOAAWQZATWQOTB-UHFFFAOYSA-N");
		keywords.put("naproxen", "CMWTZPSULFXXJA-UHFFFAOYSA-N");
		keywords.put("glutaric acid", "JFCQEDHGNNZCLN-UHFFFAOYSA-N");
		keywords.put("glycerol-beta-phosphate", "DHCLVCXQIBBOPH-UHFFFAOYSA-N"); // duplicate of line 308
		keywords.put("N-acetyl-D-mannosamine", "MBLBDJOUHNCFQT-WCTZXXKLSA-N");
		keywords.put("aconitic acid", "GTZCVFVGUGFEME-UHFFFAOYSA-N");
		keywords.put("O-acetylserine", "VZXPDPZARILFQX-BYPYZUCNSA-N");
		keywords.put("maltotriose", "RXVWSYJTUUKTEA-CGQAXDJHSA-N");
		keywords.put("stigmasterol", "HCXVJBMSMIARIN-PHZDYDNGSA-N");
		keywords.put("pyrogallol", "WQGWDDDVZFFDIG-UHFFFAOYSA-N");
		keywords.put("2-oxogluconic acid NIST", "VBUYCZFBVCCYFD-JJYYJPOSSA-N");
		keywords.put("2,3,5-trihydroxypyrazine NIST", "YMRCYAMOHVNOSM-UHFFFAOYSA-N");
		keywords.put("2-aminoadipic acid", "OYIFNHCXNCRBQI-UHFFFAOYSA-N");
		keywords.put("2-monopalmitin", "BBNYCLAREVXOSG-UHFFFAOYSA-N");
		keywords.put("epsilon-caprolactam", "JBKVHLHDHHXQEQ-UHFFFAOYSA-N");
		keywords.put("citramalic acid", "XFTRTWQBIOMVPK-UHFFFAOYSA-N");
		keywords.put("beta-alanine", "UCMIRNVEIXFBKS-UHFFFAOYSA-N");
		keywords.put("azelaic acid", "BDJRBEYXGGNYIS-UHFFFAOYSA-N");
		keywords.put("N-acetyl-D-tryptophan", "DZTHIGRZJZPRDV-UHFFFAOYSA-N");
		keywords.put("3,6-anhydrogalactose", "WZYRMLAWNVOIEX-ARQDHWQXSA-N");
		keywords.put("paracetamol", "RZVAJINKPMORJF-UHFFFAOYSA-N");
		keywords.put("adenosine-5-phosphate", "UDMBCSSLTHHNCD-KQYNXXCUSA-N");
		keywords.put("lathosterol NIST", "IZVFFXVYBHFIHY-SKCNUYALSA-N");
		keywords.put("glucoheptulose", "HSNZZMHEPUFJNZ-UHFFFAOYSA-N");
		keywords.put("mevalonic acid NIST", "KJTLQQUUPVSXIM-ZCFIWIBFSA-N");
		keywords.put("ribose", "HMFHBZSHGGEWLO-SOOFDHNKSA-N");
		keywords.put("chlorogenic acid", "CWVRJTMFETXNAD-JUHZACGLSA-N");
		keywords.put("allantoic acid", "NUCLJNSWZCHRKL-UHFFFAOYSA-N");
		keywords.put("sophorose", "HIWPGCMGAMJNRG-BTLHAWITSA-N");
		keywords.put("galactose-6-phosphate", "VFRROHXSMXFLSN-UHFFFAOYSA-N");
		keywords.put("formononetin", "HKQYGTCOTHHOMP-UHFFFAOYSA-N");
		keywords.put("adenosine", "OIRDTQYFTABQOQ-KQYNXXCUSA-N");
		keywords.put("2-hydroxyhippuric acid", "UOHMMEJUHBCKEE-UHFFFAOYSA-N");
	}

	void initKeywordsListFails1() {
		keywords.put("glucose", "GZCGUPFRVQAUEE-SLPGGIOYSA-N");
		keywords.put("phosphoric acid", "NBIIXXVUZAFLBC-UHFFFAOYSA-N");
		keywords.put("leucine", "ROHFNLRQFUQHCH-YFKPBYRVSA-N");
		keywords.put("tryptophan", "QIVBCDIJIAJPQS-VIFPVBQESA-N");
		keywords.put("citric acid", "KRKNYBCHXYNGOX-UHFFFAOYSA-N");
		keywords.put("serine", "MTCFGRXMJLQNBG-REOHCLBHSA-N");
		keywords.put("proline", "ONIBWKKTOPOVIA-BYPYZUCNSA-N");
		keywords.put("2-hydroxybutanoic acid", "WHBMMWSBFZVSSR-VKHMYHEASA-N");
		keywords.put("phenylalanine", "COLNVLDHVKWLRT-QMMMGPOBSA-N");
		keywords.put("inositol myo-", "CDAISMWEOUEBRE-UHFFFAOYSA-N");
		keywords.put("glutamic acid", "WHUUTDBJXJRKMK-VKHMYHEASA-N");
		keywords.put("N-methylalanine", "GDFAOVXKHJXLEI-VKHMYHEASA-N");
		keywords.put("beta-mannosylglycerate", "DDXCFDOPXBPUJC-SAYMMRJXSA-N");
		keywords.put("iminodiacetic acid", "NBZBKCUXIYYUSX-UHFFFAOYSA-N");
		keywords.put("3-hydroxybutanoic acid", "WHBMMWSBFZVSSR-GSVOUGTGSA-N");
		keywords.put("glyceric acid", "RBNPOMFGQQGHHO-UWTATZPHSA-N");
		keywords.put("2-ketoisocaproic acid", "BKAJNAXTPSGJCU-UHFFFAOYSA-N");
		keywords.put("asparagine", "DCXYFEDJOCDNAF-UHFFFAOYSA-N");
		keywords.put("1-monoolein", "RZRNAYUHWVFMIP-KTKRTIGZSA-N");
		keywords.put("cystine", "LEVWYRKDKASIDU-UHFFFAOYSA-N");
		keywords.put("fructose", "BJHIKXHVCXFQLS-UYFOZJQFSA-N");
		keywords.put("methylhexadecanoic acid", "KEMQGTRYUADPNZ-UHFFFAOYSA-N");
		keywords.put("methionine", "FFEARJCKVFRZRR-BYPYZUCNSA-N");
		keywords.put("tocopherol alpha", "GVJHHUAWPYXKBD-IEOSBIPESA-N");
		keywords.put("6-deoxyglucitol NIST", "SKCKOFZKJLZSFA-JGWLITMVSA-N");
		keywords.put("palmitoleic acid", "SECPZKHBENQXJG-FPLPWBNLSA-N");
		keywords.put("2-hydroxyvaleric acid", "JRHWHSJDIILJAT-UHFFFAOYSA-N");
		keywords.put("methionine sulfoxide", "QEFRNWWLZKMPFJ-YGVKFDHGSA-N");
		keywords.put("aminomalonic acid", "JINBYESILADKFW-UHFFFAOYSA-N");
		keywords.put("3-hydroxypropionic acid", "ALRHLSYJTWAHJZ-UHFFFAOYSA-N");
		keywords.put("pseudo uridine", "PTJWIQPHWPFNBW-GBNDHIKLSA-N");
		keywords.put("deoxytetronic acid NIST", "DZAIOXUZHHTJKN-UHFFFAOYSA-N");
		keywords.put("threonic acid", "JPIJQSOTBSSVTP-UHFFFAOYSA-N");
		keywords.put("4-hydroxyproline", "PMMYEEVYMWASQN-UHFFFAOYSA-N");
//		keywords.put("fucose + rhamnose", "PNNNRSAQSRJVSB-DPYQTVNSSA-N");
		keywords.put("xylose", "PYMYPHUHKUWMLA-VPENINKCSA-N");
		keywords.put("isothreonic acid", "JPIJQSOTBSSVTP-GBXIJSLDSA-N");
		keywords.put("elaidic acid", "LKOVPWSSZFDYPG-MSUUIHNZSA-N");
		keywords.put("arachidonic acid", "YZXBAPSDXZZRGB-CGRWFSSPSA-N");
		keywords.put("gamma-tocopherol", "QUEDXNHFTDJVIY-DQCZWYHMSA-N");
		keywords.put("isocitric acid", "ODBLHEXUDAPZAU-ZAFYKAAXSA-N");
		keywords.put("indole-3-lactate", "XGILAAMKEQUXLS-UHFFFAOYSA-N");
		keywords.put("methanolphosphate", "CAAULPUQFIIOTL-UHFFFAOYSA-N");
		keywords.put("glyoxalurea NIST", "WYLUZALOENCNQU-UHFFFAOYSA-N");
		keywords.put("phosphoethanolamine", "SUHOOTKUPISOBE-UHFFFAOYSA-N");
		keywords.put("glucuronic acid", "AEMOLEFTQBMNLQ-AQKNRBDQSA-N");
		keywords.put("propane-1,3-diol NIST", "YPFDHNVEDLHUCE-UHFFFAOYSA-N");
		keywords.put("propane-1,2,3-tricarboxylate NIST", "KQTIIICEAUMSDG-UHFFFAOYSA-N");
		keywords.put("2-hydroxyglutaric acid", "HWXBTNAVRSUOJR-UHFFFAOYSA-N");
		keywords.put("gluconic acid", "RGHNJXZEOKUKBD-SQOUGZDYSA-N");
		keywords.put("ribitol", "HEBKCHPVOIAQTA-UHFFFAOYSA-N");
		keywords.put("galactonic acid", "RGHNJXZEOKUKBD-MGCNEYSASA-N");
		keywords.put("arabinose", "SRBFZHDQGSBBOR-UHFFFAOYSA-N");
		keywords.put("arabinose", "SRBFZHDQGSBBOR-UHFFFAOYSA-N");
		keywords.put("malic acid", "BJEPYKJPYRNKOW-REOHCLBHSA-N");
		keywords.put("alloxanoic acid", "XXGFYEUQDGVMOU-UHFFFAOYSA-N");
		keywords.put("dihydroabietic acid", "BTAURFWABMSODR-UHFFFAOYSA-N");
		keywords.put("2,4-diaminobutyric acid", "OGNSCSPNOLGXSM-VKHMYHEASA-N");
		keywords.put("fumaric acid", "VZCYOOQTPOCHFL-OWOJBTEDSA-N");
		keywords.put("hydroxycarbamate NIST", "DRAJWRKLRBNJRQ-UHFFFAOYSA-N");
		keywords.put("glycero-guloheptose NIST", "YPZMPEPLWKRVLD-PJEQPVAWSA-N");
		keywords.put("conduritol beta epoxide", "ZHMWOVGZCINIHW-SPHYCDKFSA-N");
		keywords.put("phytol", "BOTWFXYSPFMFNR-RTBURBONSA-N");
		keywords.put("xylitol", "HEBKCHPVOIAQTA-NGQZWQHPSA-N");
		keywords.put("glycerol-3-galactoside", "NHJUPBDCSOGIKX-VGPGGAHRSA-N");
		keywords.put("2-deoxytetronic acid", "DZAIOXUZHHTJKN-UHFFFAOYSA-N");
		keywords.put("trihydroxypyrazine NIST", "DZMNWPSOYTUVJQ-UHFFFAOYSA-N");
		keywords.put("terephtalic acid", "KKEYFWRCBNTPAC-UHFFFAOYSA-N");
		keywords.put("ribonic acid", "QXKAIJAYHKCRRA-BXXZVTAOSA-N");
		keywords.put("5-methoxytryptamine", "JTEJPPKMYBDEMY-UHFFFAOYSA-N");
		keywords.put("inulobiose", "WOHYVFWWTVNXTP-TWOHWVPZSA-N");
		keywords.put("kynurenine", "YGPSJZOEDVAXAB-QMMMGPOBSA-N");
		keywords.put("1-monopalmitin", "QHZLMUACJMDIAE-UHFFFAOYSA-N");
//		keywords.put("inositol allo-", "CDAISMWEOUEBRE-UHFFFAOYSA-N");
		keywords.put("3-aminoisobutyric acid", "QCHPKSFMDHPSNR-UHFFFAOYSA-N");
		keywords.put("pyrophosphate", "XPPKVPWEQAFLFU-UHFFFAOYSA-N");
		keywords.put("tocopherol beta NIST", "WGVKWNUPNGFDFJ-CBIUGAAKSA-N");
		keywords.put("2,3-dihydroxybutanoic acid NIST", "LOUGYXZSURQALL-STHAYSLISA-N");
		keywords.put("enolpyruvate NIST", "LCTONWCANYUPML-UHFFFAOYSA-N");
		keywords.put("pyrazine 2,5-dihydroxy  NIST", "USHRADXTTYICIJ-UHFFFAOYSA-N");
		keywords.put("sucrose", "CZMRCDWAGMRECN-UGDNZRGBSA-N");
//		keywords.put("beta-glycerolphosphate", "DHCLVCXQIBBOPH-UHFFFAOYSA-N");  // duplicate of line 454
		keywords.put("4-hydroxybutyric acid", "SJZRECIVHVDYJC-UHFFFAOYSA-N");
		keywords.put("3-phosphoglycerate", "OSJPPGNTCRNQQC-UHFFFAOYSA-N");
		keywords.put("cysteine-glycine", "ZUKPVRWZDMRIEO-VKHMYHEASA-N");
		keywords.put("quinic acid", "AAWZDTNXLSGCEK-LNVDRNJUSA-N");
		keywords.put("pipecolic acid", "HXEACLLIILLPRG-YFKPBYRVSA-N");
		keywords.put("1,2,4-benzenetriol", "GGNQRNBDZQJCCN-UHFFFAOYSA-N");
		keywords.put("idonic acid NIST", "RGHNJXZEOKUKBD-SKNVOMKLSA-N");
		keywords.put("3-methoxytyrosine NIST", "PFDUUKDQEHURQC-ZETCQYMHSA-N");
		keywords.put("lactobionic acid", "AQKFVNLHZYOMKQ-UHFFFAOYSA-M");
		keywords.put("cellobiose", "GUBGYTABKSRVRQ-UHFFFAOYSA-N");
		keywords.put("naproxen", "CMWTZPSULFXXJA-UHFFFAOYSA-N");
		keywords.put("glutaric acid", "JFCQEDHGNNZCLN-UHFFFAOYSA-N");
		keywords.put("glycerol-beta-phosphate", "DHCLVCXQIBBOPH-UHFFFAOYSA-N"); // duplicate of line 441
		keywords.put("N-acetyl-D-mannosamine", "MBLBDJOUHNCFQT-WCTZXXKLSA-N");
		keywords.put("aconitic acid", "GTZCVFVGUGFEME-UHFFFAOYSA-N");
		keywords.put("O-acetylserine", "VZXPDPZARILFQX-BYPYZUCNSA-N");
		keywords.put("maltotriose", "RXVWSYJTUUKTEA-CGQAXDJHSA-N");
		keywords.put("stigmasterol", "HCXVJBMSMIARIN-PHZDYDNGSA-N");
		keywords.put("2-oxogluconic acid NIST", "VBUYCZFBVCCYFD-JJYYJPOSSA-N");
		keywords.put("2,3,5-trihydroxypyrazine NIST", "YMRCYAMOHVNOSM-UHFFFAOYSA-N");
		keywords.put("2-monopalmitin", "BBNYCLAREVXOSG-UHFFFAOYSA-N");
		keywords.put("citramalic acid", "XFTRTWQBIOMVPK-UHFFFAOYSA-N");
		keywords.put("N-acetyl-D-tryptophan", "DZTHIGRZJZPRDV-UHFFFAOYSA-N");
		keywords.put("3,6-anhydrogalactose", "WZYRMLAWNVOIEX-ARQDHWQXSA-N");
		keywords.put("glucoheptulose", "HSNZZMHEPUFJNZ-UHFFFAOYSA-N");
		keywords.put("mevalonic acid NIST", "KJTLQQUUPVSXIM-ZCFIWIBFSA-N");
		keywords.put("ribose", "HMFHBZSHGGEWLO-SOOFDHNKSA-N");
		keywords.put("sophorose", "HIWPGCMGAMJNRG-BTLHAWITSA-N");
		keywords.put("galactose-6-phosphate", "VFRROHXSMXFLSN-UHFFFAOYSA-N");
		keywords.put("2-hydroxyhippuric acid", "ONJSZLXSECQROL-UHFFFAOYSA-N");
	}

}