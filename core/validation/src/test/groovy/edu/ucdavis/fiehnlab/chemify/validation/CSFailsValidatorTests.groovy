package edu.ucdavis.fiehnlab.chemify.validation

import edu.ucdavis.fiehnlab.chemify.validation.impl.ChemspiderValidator
import org.junit.Before

/**
 * Created by diego on 10/14/14.
 */
class CSFailsValidatorTests extends ValidatorTest {

	@Before
	void setUp() {
		validators.add(new ChemspiderValidator())
	}

	public File getFileName() {
		return new File("src/test/logs/chemspider_validation.tsv")
	}

	void initKeywordsListSmallSet() {
		keywords.put("phosphoethanolamine", "SUHOOTKUPISOBE-UHFFFAOYSA-N");
		keywords.put("quinic acid", "AAWZDTNXLSGCEK-LNVDRNJUSA-N");
		keywords.put("pyrophosphate", "XPPKVPWEQAFLFU-UHFFFAOYSA-N");
		keywords.put("pyrazine 2,5-dihydroxy", "USHRADXTTYICIJ-UHFFFAOYSA-N");
		keywords.put("enolpyruvate", "LCTONWCANYUPML-UHFFFAOYSA-N");
		keywords.put("galactose-6-phosphate", "VFRROHXSMXFLSN-UHFFFAOYSA-N");
		keywords.put("4-hydroxybutyric acid", "SJZRECIVHVDYJC-UHFFFAOYSA-N");
		keywords.put("inositol allo-", "Not Found");
		keywords.put("beta-mannosylglycerate", "DDXCFDOPXBPUJC-SAYMMRJXSA-N");
		keywords.put("naproxen", "CMWTZPSULFXXJA-UHFFFAOYSA-N");
		keywords.put("leucine", "ROHFNLRQFUQHCH-YFKPBYRVSA-N");
		keywords.put("hydroxycarbamate", "DRAJWRKLRBNJRQ-UHFFFAOYSA-N");
		keywords.put("lactobionic acid", "AQKFVNLHZYOMKQ-UHFFFAOYSA-M");
		keywords.put("2,3-dihydroxybutanoic acid NIST", "LOUGYXZSURQALL-STHAYSLISA-N");
		keywords.put("beta-glycerolphosphate", "DHCLVCXQIBBOPH-UHFFFAOYSA-N");          //synonym of glycerol-beta-phosphate (line 64)
		keywords.put("ribitol", "HEBKCHPVOIAQTA-UHFFFAOYSA-N");
		keywords.put("2-hydroxyhippuric acid", "UOHMMEJUHBCKEE-UHFFFAOYSA-N");
		keywords.put("xylitol", "HEBKCHPVOIAQTA-NGQZWQHPSA-N");
		keywords.put("3-hydroxybutanoic acid", "WHBMMWSBFZVSSR-GSVOUGTGSA-N");
		keywords.put("glyceric acid", "RBNPOMFGQQGHHO-UWTATZPHSA-N");
		keywords.put("sophorose", "HIWPGCMGAMJNRG-BTLHAWITSA-N");
		keywords.put("terephtalic acid", "KKEYFWRCBNTPAC-UHFFFAOYSA-N");
		keywords.put("arabinose", "SRBFZHDQGSBBOR-UHFFFAOYSA-N");
		keywords.put("3,6-anhydrogalactose", "WZYRMLAWNVOIEX-ARQDHWQXSA-N");
		keywords.put("indole-3-lactate", "XGILAAMKEQUXLS-UHFFFAOYSA-N");
		keywords.put("cellobiose", "GUBGYTABKSRVRQ-UHFFFAOYSA-N");
		keywords.put("fucose + rhamnose", "PNNNRSAQSRJVSB-DPYQTVNSSA-N");
		keywords.put("cysteine-glycine", "ZUKPVRWZDMRIEO-VKHMYHEASA-N");
		keywords.put("elaidic acid", "LKOVPWSSZFDYPG-MSUUIHNZSA-N");
		keywords.put("2-hydroxybutanoic acid", "WHBMMWSBFZVSSR-VKHMYHEASA-N");
		keywords.put("phytol", "BOTWFXYSPFMFNR-RTBURBONSA-N");
		keywords.put("ribonic acid", "QXKAIJAYHKCRRA-BXXZVTAOSA-N");
		keywords.put("4-hydroxyproline", "PMMYEEVYMWASQN-UHFFFAOYSA-N");
		keywords.put("O-acetylserine", "VZXPDPZARILFQX-BYPYZUCNSA-N");
		keywords.put("N-methylalanine", "GDFAOVXKHJXLEI-VKHMYHEASA-N");
		keywords.put("tocopherol beta", "WGVKWNUPNGFDFJ-CBIUGAAKSA-N");
		keywords.put("methanolphosphate", "CAAULPUQFIIOTL-UHFFFAOYSA-N");
		keywords.put("phosphoric acid", "NBIIXXVUZAFLBC-UHFFFAOYSA-N");
		keywords.put("2-hydroxyglutaric acid", "HWXBTNAVRSUOJR-UHFFFAOYSA-N");
		keywords.put("trihydroxypyrazine", "DZMNWPSOYTUVJQ-UHFFFAOYSA-N");
		keywords.put("3-hydroxypropionic acid", "ALRHLSYJTWAHJZ-UHFFFAOYSA-N");
		keywords.put("propane-1,2,3-tricarboxylate", "KQTIIICEAUMSDG-UHFFFAOYSA-N");
		keywords.put("propane-1,3-diol", "YPFDHNVEDLHUCE-UHFFFAOYSA-N");
		keywords.put("glucuronic acid", "AEMOLEFTQBMNLQ-AQKNRBDQSA-N");
		keywords.put("glycerol-beta-phosphate", "DHCLVCXQIBBOPH-UHFFFAOYSA-N");     //synonym of beta-glycerolphosphate (line 34)
		keywords.put("methylhexadecanoic acid", "KEMQGTRYUADPNZ-UHFFFAOYSA-N");
		keywords.put("isothreonic acid", "JPIJQSOTBSSVTP-GBXIJSLDSA-N");
		keywords.put("malic acid", "BJEPYKJPYRNKOW-REOHCLBHSA-N");
		keywords.put("glycerol-3-galactoside", "NHJUPBDCSOGIKX-VGPGGAHRSA-N");
		keywords.put("tocopherol alpha", "GVJHHUAWPYXKBD-IEOSBIPESA-N");
	}
}
