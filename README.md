# Chemical Translation Service #

Publicly available chemical information including structures, chemical names, chemical synonyms, database identifiers, molecular mass data were downloaded from different databases and combined into a single internal repository for compound-specific, structure-based cross references.

Molfile (MOL) to InChI Code/Key converters (vs. 1.0.2) and InChI code to InChI key converters were integrated into the system to allow extra types of conversions, e.g. by chemical names, structures, database identifiers.

This database allows executing queries at far higher speed compared to web-based applications that query external repositories in real time. 

For accessibility we use a web GUI and a REST-based application-programming interface was implemented for automated access.

## Contact ##
[Diego Pedrosa](mailto:dpedrosa@ucdavis.edu)

[Gert Wohlgemuth](mailto:berlinguyinca@ucdavis.edu)

## Release ##
2017/09 - CTS v2.6.15
2017/04 - CTS v2.6.14
2017/02 - CTS v2.6.13
2016/11 - CTS v2.6.12

### Tools ###

* IntelliJ
* [Java profiler](http://www.ej-technologies.com/products/jprofiler/overview.html)
